using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace CUSTOR.OTRLS.API.Authorization
{
    public class ManagementServicesAuthorizationRequirement : IAuthorizationRequirement
    {
        public ManagementServicesAuthorizationRequirement(string operationName)
        {

            this.OperationName = operationName;
        }

        public string OperationName { get; private set; }
    }

    public class ViewReadOnlyDataAuthorizationHandler : AuthorizationHandler<ManagementServicesAuthorizationRequirement, string>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ManagementServicesAuthorizationRequirement requirement, string targetUserId)
        {
            //if ((context.User.IsInRole("MOTI Management")) && context.User.HasClaim(ClaimConstants.Permission, ApplicationPermissions.ViewReadOnlyData))
            if ((context.User.IsInRole("Site Management")))
                context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
