namespace CUSTOR.ICERS.API.Authorization
{
    public class Policies
    {


        //User Managment
        public const string ViewAllUsersPolicy = "View All Users";

        public const string ManageAllUsersPolicy = "Manage All Users";

        //Role Managment
        public const string ViewAllRolesPolicy = "View All Roles";

        public const string ViewRoleByRoleNamePolicy = "View Role by RoleName";
        public const string ManageAllRolesPolicy = "Manage All Roles";
        public const string AssignAllowedRolesPolicy = "Assign Allowed Roles";


        //Super Administration
        public const string ManageSettingsPolicy = "Manage Settings";
        public const string ManageLookupsPolicy = "Manage Lookup Data";
        public const string ManageAdministratorsPolicy = "Manage Site Administrators";

        //Recognition Management
        public const string ManageRecognitionsPolicy = "Manage Recognition";
        public const string ManageInjunctionsPolicy = "Manage Injunctions";
        public const string ManageCancellationsPolicy = "Cancel Recognition";
        public const string AddRecongnition = "Add Recognition";
        public const string ViewRecognition = "View Recognition";
        public const string UpateRecognition = "Edit Recognition";

        //For Finance
        public const string ViewAuthorizedPayment = "View Authorized Payment";
        public const string RecivePayment = "Recive Payment";
        public const string CancelAuthorizedPayment = "Cancel Authorized Payment Order";

        //Apporval
        public const string Prepare = "Prepare";
        public const string Approver_One = "Prepare";
        public const string Approver_Two = "Prepare";


        //Management Services Policy
        public const string ViewReadOnlyData = "View Read-only Data";

        // Recognition management policy

        // Trade Excution
        public const string ViewFinancialReport = "View Financial Reports";
        public const string ViewMemberTradeExecution = "View Member Trade Execution";
        public const string ViewMemberClientInformation = "View Member Client Information";
        public const string CreateTradeExecution = "Create Trade Execution";
        public const string CreateFinancialReport = "Create Financial Report";
        public const string CreateClientInformation = "Create Client Information";
        public const string CreateMemberViolation = "Create Member Violation";
        public const string ViewMemberViolation = "View Member Violation";
        public const string Checker = "Checker";
        public const string Approver_one = "Approver One";
        public const string Approver_two = "Approver Two";
        public const string Reporter = "Reporter";

        public const string MemberClientInformation = "MemberClientInformation";
        public const string MemberFinance = "MemberFinance";
        public const string MemberTradeExecution = "MemberTradeExecution";
        public const string OffSiteMoniteringTeamLeader = "OffSiteMoniteringTeamLeader";
        public const string OffSiteMoniteringDeptDirector = "OffSiteMoniteringDeptDirector";
        public const string OffSiteMoniteringView = "OffSiteMoniteringView";
        //public const string

        //End Trade Excution
        /*Start Settlement Bank */
        public const string SettlementBankOfficer = "SettlementBankOfficer";
        public const string SettlementBankTeamLeader = "SettlementBankTeamLeader";
        public const string SettlementBankDepartmentHead = "SettlementBankDepartmentHead";
        /* End Settlement Bank */


        /* adminstrative tirbunal */

        public const string ManageCases = "Manage Tirbunal Cases";

        /* end of adminstrative tribunal */

        /* law enforcement */

        public const string ManageLawEnforcementCase = "Manage Law Enforcement Case";

        /* end of law enforcement */


        /*
         * servillance
         */
        public const string ViewServilanceViolation = "View Servilance Violation";
        /*Start ECX Report */
        public const string SettlementGuarantee = "SettlementGuarantee";
        public const string ServiceLevelAgreement = "ServiceLevelAgreement";
        /* End ECX Report */

    }

    /// <summary>
    /// Operation Policy to allow adding, viewing, updating and deleting general or specific user records.
    /// </summary>
    public static class AccountManagementOperations
    {
        public const string CreateOperationName = "Create";
        public const string ReadOperationName = "Read";
        public const string UpdateOperationName = "Update";
        public const string DeleteOperationName = "Delete";

        public static UserAccountAuthorizationRequirement Create = new UserAccountAuthorizationRequirement(CreateOperationName);
        public static UserAccountAuthorizationRequirement Read = new UserAccountAuthorizationRequirement(ReadOperationName);
        public static UserAccountAuthorizationRequirement Update = new UserAccountAuthorizationRequirement(UpdateOperationName);
        public static UserAccountAuthorizationRequirement Delete = new UserAccountAuthorizationRequirement(DeleteOperationName);
    }
}
