using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.API.ModelValidationAttribute;
using CUSTOR.Helpers;
using CUSTOR.ICERS.API.Authorization;
using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.Security;
using CUSTOR.SecurityViewModels;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [ServiceFilter(typeof(ModelValidationAttribute))]
    [EnableCors("CorsPolicy")]
    public class AccountController : Controller
    {
        private const string GetUserByIdActionName = "GetUserById";
        private const string GetRoleByIdActionName = "GetRoleById";
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _config;
        private readonly ECEADbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(IMapper mapper,IAccountManager accountManager, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IConfiguration config, ECEADbContext ctx, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _accountManager = accountManager;
            _authorizationService = authorizationService;
            _userManager = userManager;
            _config = config;
            _context = ctx;
            _httpContextAccessor = httpContextAccessor;

        }

        [HttpGet("users/me")]
        [Produces(typeof(UserViewModel))]
        public async Task<IActionResult> GetCurrentUser()
        {
            return await GetUserByUserName(User.Identity.Name);
        }

        [HttpGet("users/{id}", Name = GetUserByIdActionName)]
        [Produces(typeof(UserViewModel))]
        public async Task<IActionResult> GetUserById(string id)
        {
            if (!(await _authorizationService.AuthorizeAsync(User, id, AccountManagementOperations.Read))
                .Succeeded)
                return new ChallengeResult();

            var userVM = await GetUserViewModelHelper(id);

            if (userVM != null)
                return Ok(userVM);
            return NotFound(id);
        }

        [HttpGet("users/username/{userName}")]
        [Produces(typeof(UserViewModel))]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {

            var appUser = await _accountManager.GetUserByUserNameAsync(userName);

            if (!(await _authorizationService.AuthorizeAsync(User, appUser?.Id ?? "",
                AccountManagementOperations.Read)).Succeeded)
                return new ChallengeResult();

            if (appUser == null)
                return NotFound(userName);

            return await GetUserById(appUser.Id);
        }

        [HttpGet("users")]
        [Produces(typeof(List<UserViewModel>))]
        //[AllowAnonymous]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        public IActionResult GetUsers()
        {
            return  GetUsers(-1, -1);
        }

        [HttpGet("users/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<UserViewModel>))]

        //[AllowAnonymous]


        public IActionResult GetUsers(int page, int pageSize)
         {
            var users = (from u in _context.Users
                         let query = (from ur in _context.UserRoles
                                      where ur.UserId.Equals(u.Id)
                                      join r in _context.Roles on ur.RoleId equals r.Id
                                      orderby r.Name
                                      select r.Name)
                         orderby u.UserName 
                         select new 
                         {
                             Id = u.Id,
                             UserName = u.UserName,
                             FullName = u.FullName,
                             PhoneNumber = u.PhoneNumber,
                             Email = u.Email,
                             IsEnabled = u.IsEnabled,
                             Roles = query.ToList()
                         })
               .ToList();


            return Ok(users);
        }

        [HttpPut("users/me")]
        public async Task<IActionResult> UpdateCurrentUser([FromBody] UserEditViewModel user)
        {
            return await UpdateUser(Utilities.GetUserId(User), user);
        }

        [HttpPut("users/{id}")]
        //[Authorize(Policies.ManageAllUsersPolicy)]
        public async Task<IActionResult> UpdateUser(string id, [FromBody] UserEditViewModel user)
        {
            try
            {
             

                var appUser = await _accountManager.GetUserByIdAsync(id);
                var currentRoles =
                    appUser != null ? (await _accountManager.GetUserRolesAsync(appUser)).ToArray() : null;

                var manageUsersPolicy =
                    _authorizationService.AuthorizeAsync(User, id, AccountManagementOperations.Update);
                var assignRolePolicy = _authorizationService.AuthorizeAsync(User,
                    Tuple.Create(user.Roles, currentRoles), Policies.AssignAllowedRolesPolicy);

                //if ((await Task.WhenAll(manageUsersPolicy, assignRolePolicy)).Any(r => !r.Succeeded))
                //  return new ChallengeResult();

                if (ModelState.IsValid)
                {
                    if (user == null)
                        return BadRequest($"{nameof(user)} cannot be null");

                    if (!string.IsNullOrWhiteSpace(user.Id) && id != user.Id)
                        return BadRequest("Conflicting user id in parameter and model data");

                    if (appUser == null)
                        return NotFound(id);

                    if (Utilities.GetUserId(User) == id && string.IsNullOrWhiteSpace(user.CurrentPassword))
                    {
                        if (!string.IsNullOrWhiteSpace(user.NewPassword))
                            return BadRequest("Current password is required when changing your own password");

                        if (appUser.UserName != user.UserName)
                            return BadRequest("Current password is required when changing your own username");
                    }

                    var isValid = true;

                    if (Utilities.GetUserId(User) == id &&
                        (appUser.UserName != user.UserName || !string.IsNullOrWhiteSpace(user.NewPassword)))
                        if (!await _accountManager.CheckPasswordAsync(appUser, user.CurrentPassword))
                        {
                            isValid = false;
                            AddErrors(new[] { "The username/password couple is invalid." });
                        }

                    if (isValid)
                    {
                        _mapper.Map<UserViewModel, ApplicationUser>(user, appUser);

                        var result = await _accountManager.UpdateUserAsync(appUser, user.Roles);
                        if (result.Item1)
                        {
                            if (!string.IsNullOrWhiteSpace(user.NewPassword))
                            {
                                if (!string.IsNullOrWhiteSpace(user.CurrentPassword))
                                    result = await _accountManager.UpdatePasswordAsync(appUser, user.CurrentPassword,
                                        user.NewPassword);
                                else
                                    result = await _accountManager.ResetPasswordAsync(appUser, user.NewPassword);
                            }

                            if (result.Item1)
                                return NoContent();
                        }

                        AddErrors(result.Item2);
                    }
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    var s = message;
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                var s = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        [HttpPatch("users/me")]
        public async Task<IActionResult> UpdateCurrentUser([FromBody] JsonPatchDocument<UserPatchViewModel> patch)
        {
            return await UpdateUser(Utilities.GetUserId(User), patch);
        }

        [HttpPatch("users/{id}")]
        public async Task<IActionResult> UpdateUser(string id, [FromBody] JsonPatchDocument<UserPatchViewModel> patch)
        {
            if (!(await _authorizationService.AuthorizeAsync(User, id, AccountManagementOperations.Update))
                .Succeeded)
                return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (patch == null)
                    return BadRequest($"{nameof(patch)} cannot be null");

                var appUser = await _accountManager.GetUserByIdAsync(id);

                if (appUser == null)
                    return NotFound(id);

                var userPVM = _mapper.Map<UserPatchViewModel>(appUser);
                patch.ApplyTo(userPVM, ModelState);

                if (ModelState.IsValid)
                {
                    _mapper.Map(userPVM, appUser);

                    var result = await _accountManager.UpdateUserAsync(appUser);
                    if (result.Item1)
                        return NoContent();

                    AddErrors(result.Item2);
                }
            }

            return BadRequest(ModelState);
        }


        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeOrResetPassword([FromBody] UserEditViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                throw new Exception("Error - Please make sure that the provided email is correct");
                ;
            }



            if (!string.Equals(user.UserName, model.UserName, StringComparison.CurrentCultureIgnoreCase))
                return BadRequest("Error - The account credentials you provided are incorrect!");

            //var _emailSender = new EmailSendGrid(_config);
            //var _emailSender = new EmailService(_emailConfiguration);

            if (model.CurrentPassword == "resetme") //this means Reset Passowrd is requested
            {
                //var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                //var callbackUrl = new Uri(Url.Link("ConfirmResetRoute",
                //    new {userId = user.Id, token = code, newPass = model.NewPassword}));
                //await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                //    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");

                var token = _userManager.GeneratePasswordResetTokenAsync(user).Result;
                await _userManager.ResetPasswordAsync(user, token, model.NewPassword);
                // await _emailSender.SendEmailAsync(user.Email, "Confirm Password Reset", strMessage);
            }
            else
            {
                if (!await _userManager.CheckPasswordAsync(user, model.CurrentPassword))
                    return BadRequest("Error - The old password  you provided is incorrect!");
                await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
            }

            return Ok();
        }


        //[HttpGet]
        //[Route("[action]/{userEmail}")]
        //[AllowAnonymous]
        ////[ValidateAntiForgeryToken]
        //public async Task<IActionResult> ForgotPassword(string userEmail)
        //{
        //    //var _emailSender = new EmailSendGrid(_config);
        //    //var _emailSender = new EmailService(_emailConfiguration);
        //    //if (ModelState.IsValid)
        //    //{
        //    var user = await _userManager.FindByEmailAsync(userEmail);
        //    if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
        //        return BadRequest("Error - Your account is not confirmed!");

        //    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
        //    //var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
        //    var callbackUrl = new Uri(Url.Link("ConfirmResetRoute", new { userId = user.Id, token = code }));
        //    EmailMessage emailMessage = new EmailMessage();
        //    EmailAddress reciecverEmailAddress = new EmailAddress();
        //    EmailAddress senderEmailAddress = new EmailAddress();
        //    reciecverEmailAddress.Name = user.FullName;
        //    reciecverEmailAddress.Address = user.Email;
        //    senderEmailAddress.Name = _config["EmailSenderName"];
        //    senderEmailAddress.Address = _config["EmailSenderAddress"];
        //    emailMessage.ToAddresses.Add(reciecverEmailAddress);
        //    emailMessage.FromAddresses.Add(senderEmailAddress);
        //    emailMessage.Content = $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>";
        //    emailMessage.Subject = "Reset Password";
        //    _emailSender.Send(emailMessage);
        //    //await _emailSender.SendEmailAsync(userEmail, "Reset Password",
        //    //    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");


        //    //return CreatedAtAction(GetUserByIdActionName, new { id = user.Id }, user); // ????
        //    return Ok();
        //    //}
        //}

        [HttpGet]
        [Route("ConfirmReset", Name = "ConfirmResetRoute")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmReset(string userId, string token, string newPass)
        {
            //to-do proper error handling required
            //if (userId == null || code == null)
            //{
            //    return RedirectToAction(nameof(HomeController.Index), "Home");
            //}
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null) throw new ApplicationException($"Unable to load user with ID '{userId}'.");

                var result = await _userManager.ResetPasswordAsync(user, token, newPass);
                if (result.Succeeded)
                {
                    var strHomeUrl = _config["ReturnURL"];
                    var strRedirect = new Uri(strHomeUrl).ToString();
                    var strMessage = EmailTemplates.GetResetConfirmationMessage(strRedirect);
                    return Content(strMessage, "text/html");
                }

                return Ok(); //???
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                var strHomeUrl = _config["ReturnURL"];
                var strRedirect = new Uri(strHomeUrl).ToString();
                var strMessage = EmailTemplates.GetConfirmationMessage(strRedirect);
                return Content(strMessage, "text/html");
            }

            throw new ApplicationException($"Unable to confirm email for user with ID '{userId}'.");
        }

        [HttpGet("isEmailRegisterd/{email}")]
        [AllowAnonymous]
        public async Task<bool> IsEmailRegisterd(string email)
        {
            var appUser = await _accountManager.GetUserByEmailAsync(email);


            if (appUser == null)
                return false;
            return true;
        }

        //[HttpGet("isTinRegisterd/{tin}")]
        //[AllowAnonymous]
        //public async Task<bool> IsTinRegisterd(string tin)
        //{
        //    //var investor = await _accountManager.GetTin(tin);


        //    //if (investor == null)
        //    //    return false;
        //    return true;
        //}

        [Produces("application/json")]
        [HttpPost("users")]
        // [Authorize(Policies.ManageAllUsersPolicy)]
        public async Task<IActionResult> SelfRegister([FromBody] UserEditViewModel user)
        {

            try
            {
                //if (!(await _authorizationService.AuthorizeAsync(this.User, Tuple.Create(user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
                //    return new ChallengeResult();

                if (!string.IsNullOrEmpty(user.Tin))
                {

                    var exisitingUser = _context.Users.Where(u => u.Tin == user.Tin).FirstOrDefault();
                    if (exisitingUser != null)
                    {
                        ModelState.AddModelError("Tin",
                            "A record with the provided TIN  exists!");
                        return BadRequest(ModelState);
                    }
                }

                //var _emailSender = new EmailSendGrid(_config);
                // var _emailSender = new EmailService(_emailConfiguration);

                if (ModelState.IsValid)
                {
                    if (user == null)
                        return BadRequest($"{nameof(user)} cannot be null");

                    var appUser = _mapper.Map<ApplicationUser>(user);
                    appUser.CreatedDate = DateTime.Now;
                    //get current user inf
                    var currentUser = await _userManager.GetUserAsync(User);
                    if (currentUser != null)
                    {
                        appUser.CreatedUserId = currentUser.Id;
                        appUser.CreatedBy = currentUser.UserName;
                    }

                    var result = await _accountManager.CreateUserAsync(appUser, user.Roles, user.NewPassword);
                    if (result.Item1)
                    {
                        var userVM = await GetUserViewModelHelper(appUser.Id);
                        try
                        {
                            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
                            //var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute",
                            //    new { userId = appUser.Id, token = code }));
                            //var strUser = user.FullName;
                            //var strMessage = EmailTemplates.GetConfirmationEmail(strUser, callbackUrl.ToString());
                            //EmailMessage emailMessage = new EmailMessage();
                            //EmailAddress reciecverEmailAddress = new EmailAddress();
                            //EmailAddress senderEmailAddress = new EmailAddress();
                            //reciecverEmailAddress.Name = user.FullName;
                            //reciecverEmailAddress.Address = user.Email;
                            //senderEmailAddress.Name = _config["EmailSenderName"];
                            //senderEmailAddress.Address = _config["EmailSenderAddress"];
                            //emailMessage.ToAddresses.Add(reciecverEmailAddress);
                            //emailMessage.FromAddresses.Add(senderEmailAddress);
                            //emailMessage.Content = strMessage;
                            //emailMessage.Subject = "Confirm your account";

                            ////await _emailSender.SendEmailAsync(user.Email, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                            //// await _emailSender.SendEmailAsync(user.Email, "Confirm your account", strMessage);
                            //_emailSender.Send(emailMessage);
                        }
                        catch (Exception ex)
                        {
                            var s = ex.Message;
                        }

                        return CreatedAtAction(GetUserByIdActionName, new { id = userVM.Id }, userVM);
                    }

                    AddErrors(result.Item2);
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    var s = message;
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                var s = ex.Message;
                return BadRequest("Network error, please try again later");
            }
        }

        [HttpPost("users/add")]
        public async Task<IActionResult> Register([FromBody] UserEditViewModel user)
        {
            try
            {
                var _emailSender = new EmailSendGrid(_config);
                if (!(await _authorizationService.AuthorizeAsync(User, Tuple.Create(user.Roles, new string[] { }),
                    Policies.AssignAllowedRolesPolicy)).Succeeded)
                    return new ChallengeResult();

                if (ModelState.IsValid)
                {
                    if (user == null)
                        return BadRequest($"{nameof(user)} cannot be null");

                    var appUser = _mapper.Map<ApplicationUser>(user);

                    var result = await _accountManager.CreateUserAsync(appUser, user.Roles, user.NewPassword);
                    if (result.Item1)
                    {
                        var userVM = await GetUserViewModelHelper(appUser.Id);
                        try
                        {
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
                            var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute",
                                new { userId = appUser.Id, token = code }));
                            var strUser = user.FullName;
                            var strMessage = EmailTemplates.GetConfirmationEmailAdmin(strUser, callbackUrl.ToString(),
                                user.NewPassword);

                            //await _emailSender.SendEmailAsync(user.Email, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                            await _emailSender.SendEmailAsync(user.Email, "Confirm your account", strMessage);
                        }
                        catch (Exception ex)
                        {
                            var s = ex.Message;
                        }

                        return CreatedAtAction(GetUserByIdActionName, new { id = userVM.Id }, userVM);
                    }

                    AddErrors(result.Item2);
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    var s = message;
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                var s = ex.Message;
                return null;
            }
        }

        [HttpDelete("users/{id}")]
        [Produces(typeof(UserViewModel2))]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (!(await _authorizationService.AuthorizeAsync(User, id, AccountManagementOperations.Delete)).Succeeded)
                return new ChallengeResult();

            //if (!await _accountManager.TestCanDeleteUserAsync(id))
            //    return BadRequest("User cannot be deleted. Delete all orders associated with this user and try again");

            UserViewModel2 userVM = null;
            var appUser = await _accountManager.GetUserByIdAsync(id);

            if (appUser != null)
                userVM = await GetUserViewModelHelper(appUser.Id);

            if (userVM == null)
                return NotFound(id);

            var result = await _accountManager.DeleteUserAsync(appUser);
            if (!result.Item1)
                throw new Exception("The following errors occurred whilst deleting user: " +
                                    string.Join(", ", result.Item2));

            return Ok(userVM);
        }

        [HttpPut("users/unblock/{id}")]
        //[Authorize(Policies.ManageAllUsersPolicy)]
        public async Task<IActionResult> UnblockUser(string id)
        {
            var appUser = await _accountManager.GetUserByIdAsync(id);

            if (appUser == null)
                return NotFound(id);

            appUser.LockoutEnd = null;
            var result = await _accountManager.UpdateUserAsync(appUser);
            if (!result.Item1)
                throw new Exception("The following errors occurred whilst unblocking user: " +
                                    string.Join(", ", result.Item2));

            return NoContent();
        }

        [HttpGet("users/me/preferences")]
        [Produces(typeof(string))]
        public async Task<IActionResult> UserPreferences()
        {
            var userId = Utilities.GetUserId(User);
            var appUser = await _accountManager.GetUserByIdAsync(userId);

            if (appUser != null)
                return Ok(appUser.Configuration);
            return NotFound(userId);
        }

        [HttpPut("users/me/preferences")]
        public async Task<IActionResult> UserPreferences([FromBody] string data)
        {
            var userId = Utilities.GetUserId(User);
            var appUser = await _accountManager.GetUserByIdAsync(userId);

            if (appUser == null)
                return NotFound(userId);

            appUser.Configuration = data;
            var result = await _accountManager.UpdateUserAsync(appUser);
            if (!result.Item1)
                throw new Exception("The following errors occurred whilst updating User Configurations: " +
                                    string.Join(", ", result.Item2));

            return NoContent();
        }

        [HttpGet("roles/{id}", Name = GetRoleByIdActionName)]
        [Produces(typeof(RoleViewModel))]
        public async Task<IActionResult> GetRoleById(string id)
        {
            var appRole = await _accountManager.GetRoleByIdAsync(id);
            if (!(await _authorizationService.AuthorizeAsync(User, appRole?.Name ?? "", Policies.ViewRoleByRoleNamePolicy)).Succeeded)

                return new ChallengeResult();

            if (appRole == null)
                return NotFound(id);

            return await GetRoleByName(appRole.Name);
        }

        [HttpGet("roles/name/{name}")]
        [Produces(typeof(RoleViewModel))]
        public async Task<IActionResult> GetRoleByName(string name)
        {
            if (!(await _authorizationService.AuthorizeAsync(User, name, Policies.ViewRoleByRoleNamePolicy)).Succeeded)
                return new ChallengeResult();

            var roleVM = await GetRoleViewModelHelper(name);

            if (roleVM == null)
                return NotFound(name);

            return Ok(roleVM);
        }

        [HttpGet("roles")]
        [Produces(typeof(List<RoleViewModel>))]
        public async Task<IActionResult> GetRoles()
        {
            return await GetRoles(-1, -1);
        }

        [HttpGet("roles/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<RoleViewModel2>))]
        public async Task<IActionResult> GetRoles(int page, int pageSize)
        {

            var users = (from u in _context.Roles
                         let query = (from ur in _context.RoleClaims
                                      where ur.RoleId.Equals(u.Id)
                                      orderby ur.ClaimValue
                                      select ur.ClaimValue)
                         orderby u.Name descending
                         select new RoleViewModel2
                         {
                             Id = u.Id,
                             Name = u.Name,
                             Description = u.Description,
                             Permissions = query.ToList()
                         })
              .ToList();


            return Ok(users);

        }

        [HttpPut("roles/{id}")]
        //[Authorize(Policies.ManageAllRolesPolicy)]
        public async Task<IActionResult> UpdateRole(string id, [FromBody] RoleViewModel role)
        {
            if (ModelState.IsValid)
            {
                if (role == null)
                    return BadRequest($"{nameof(role)} cannot be null");

                if (!string.IsNullOrWhiteSpace(role.Id) && id != role.Id)
                    return BadRequest("Conflicting role id in parameter and model data");

                var appRole = await _accountManager.GetRoleByIdAsync(id);

                if (appRole == null)
                    return NotFound(id);

                _mapper.Map(role, appRole);

                var result =
                    await _accountManager.UpdateRoleAsync(appRole, role.Permissions?.Select(p => p.Value).ToArray());
                if (result.Item1)
                    return NoContent();

                AddErrors(result.Item2);
            }

            return BadRequest(ModelState);
        }

        [HttpPost("roles")]
        //[Authorize(Policies.ManageAllRolesPolicy)]
        public async Task<IActionResult> CreateRole([FromBody] RoleViewModel role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (role == null)
                        return BadRequest($"{nameof(role)} cannot be null");

                    var appRole = _mapper.Map<ApplicationRole>(role);

                    var result =
                        await _accountManager.CreateRoleAsync(appRole, role.Permissions?.Select(p => p.Value).ToArray());
                    if (result.Item1)
                    {
                        var roleVM = await GetRoleViewModelHelper(appRole.Name);
                        return CreatedAtAction(GetRoleByIdActionName, new { id = roleVM.Id }, roleVM);
                    }

                    AddErrors(result.Item2);
                }

                return BadRequest(ModelState);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpDelete("roles/{id}")]
        [Produces(typeof(RoleViewModel))]
        public async Task<IActionResult> DeleteRole(string id)
        {
            if (!await _accountManager.TestCanDeleteRoleAsync(id))
                return BadRequest("Role cannot be deleted. Remove all users from this role and try again");

            RoleViewModel roleVM = null;
            var appRole = await _accountManager.GetRoleByIdAsync(id);

            if (appRole != null)
                roleVM = await GetRoleViewModelHelper(appRole.Name);

            if (roleVM == null)
                return NotFound(id);

            var result = await _accountManager.DeleteRoleAsync(appRole);
            if (!result.Item1)
                throw new Exception("The following errors occurred whilst deleting role: " +
                                    string.Join(", ", result.Item2));

            return Ok(roleVM);
        }

        [HttpGet("permissions")]
        [Produces(typeof(List<PermissionViewModel>))]
        //[Authorize(Authorization.Policies.ViewAllRolesPolicy)]
        public IActionResult GetAllPermissions()
        {
            return Ok(_mapper.Map<List<PermissionViewModel>>(ApplicationPermissions.AllPermissions));
        }

        private async Task<UserViewModel2> GetUserViewModelHelper(string userId)
        {
            var users = (from u in _context.Users
                         let query = (from ur in _context.UserRoles
                                      where ur.UserId.Equals(userId)
                                      join r in _context.Roles on ur.RoleId equals r.Id
                                      select r.Name)
                         select new UserViewModel2
                         {
                             Id = u.Id,
                             
                             UserName = u.UserName,
                             FullName = u.FullName,
                             PhoneNumber = u.PhoneNumber,
                             Email = u.Email,
                             IsEnabled = u.IsEnabled,
                             Roles = query.ToArray()
                         })
                 .FirstOrDefault();


            return users;
        }

        private async Task<RoleViewModel> GetRoleViewModelHelper(string roleName)
        {
            var role = await _accountManager.GetRoleLoadRelatedAsync(roleName);
            if (role != null)
                return _mapper.Map<RoleViewModel>(role);

            return null;
        }

        private void AddErrors(IEnumerable<string> errors)
        {
            foreach (var error in errors) ModelState.AddModelError(string.Empty, error);
        }
    }
}
