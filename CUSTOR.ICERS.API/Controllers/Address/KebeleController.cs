﻿using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers.Address
{
    [Route("api/kebele")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class KebeleController : Controller
    {
        private readonly KebeleRepository _kebeleRepo;
        private readonly IMapper _mapper;

        public KebeleController(KebeleRepository kebeleRepository, IMapper mapper)
        {
            _kebeleRepo = kebeleRepository;
            _mapper = mapper;
        }


        [HttpGet("{lang}/{id}")]
        public async Task<IEnumerable<StaticData>> GetKebeleByWoredId(string lang, int id)
        {
            return await _kebeleRepo.GetKebelesByWoredaId(lang, id);
        }

        [HttpGet]
        [Route("getKebelesByWoreda")]
        public async Task<IEnumerable<StaticData>> GetKebelesByWoreda(string lang, int woredaID)
        {
            return await _kebeleRepo.GetKebelesByWoredaId(lang, woredaID);
        }


        [HttpPost]
        public async Task<IActionResult> AddKebele([FromBody] KebeleDTO postedKebele)
        {
            var kebele = _mapper.Map<KebeleDTO, Kebele>(postedKebele);
            var kebeleDTO = await _kebeleRepo.AddKebele(kebele);
            return new OkObjectResult(kebeleDTO);
        }

        [HttpGet("GetKebelesByParentId/{woredaId}")]
        public async Task<IActionResult> GetKebelesByParentId(int woredaId)
        {
            var kebeles = await _kebeleRepo.GetKebelesByParentId(woredaId);
            var KebeleDTO = _mapper.Map<List<Kebele>, List<KebeleDTO>>(kebeles);
            return new OkObjectResult(KebeleDTO);
        }

        [HttpDelete("{KebeleId}")]
        public async Task<IActionResult> DeleteKebelesAsync(int kebeleId)
        {
            var bKebeleDTO = await _kebeleRepo.DeleteKebele(kebeleId);

            return new OkObjectResult(bKebeleDTO);

        }

        [HttpPut]
        public async Task<IActionResult> UpdateKebelesAsync([FromBody] KebeleDTO postedKebele)
        {
            var kebele = _mapper.Map<KebeleDTO, Kebele>(postedKebele);
            var kebeleDTO = await _kebeleRepo.UpdateKebele(kebele);
            return new OkObjectResult(kebeleDTO);
        }

        [HttpGet("{kebeleId}")]
        public async Task<IActionResult> GetKebele(int kebeleId)
        {
            var Kebele = await _kebeleRepo.GetKebele(kebeleId);
            var KebeleDTO = _mapper.Map<Kebele, KebeleDTO>(Kebele);
            return new OkObjectResult(KebeleDTO);
        }

    }
}
