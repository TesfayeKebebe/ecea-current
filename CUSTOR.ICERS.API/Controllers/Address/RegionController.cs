﻿using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.WEBAPI.Controllers.Address
{
    [Route("api/region")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class RegionController : ControllerBase
    {
        private readonly RegionRepository _regionRepo;
        private readonly IMapper _mapper;
        public RegionController(RegionRepository regionRepository, IMapper mapper)
        {
            _regionRepo = regionRepository;
            _mapper = mapper;
        }


        [HttpGet("{lang}")]
        public async Task<IEnumerable<StaticData>> GetRegions(string lang)
        {
            return await _regionRepo.GetRegions(lang);
        }

        [HttpGet("GetRegion/{regionId}")]
        public async Task<IActionResult> GetRegion(int regionId)
        {
            var RegionDTO = await _regionRepo.GetRegion(regionId);

            return new OkObjectResult(RegionDTO);
        }

        [HttpPost]
        public async Task<IActionResult> AddRegion([FromBody] RegionDTO postedRegion)
        {
            var region = _mapper.Map<RegionDTO, Region>(postedRegion);
            var regionDTO = await _regionRepo.AddRegion(region);
            return new OkObjectResult(regionDTO);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateRegionsAsync([FromBody] RegionDTO postedRegion)
        {
            var region = _mapper.Map<RegionDTO, Region>(postedRegion);
            var regionDTO = await _regionRepo.UpdateRegion(region);
            return new OkObjectResult(regionDTO);
        }
        [HttpDelete("{RegionId}")]
        public async Task<IActionResult> DeleteRegionsAsync(int regionId)
        {
            var bRegionDTO = await _regionRepo.DeleteRegion(regionId);

            return new OkObjectResult(bRegionDTO);

        }

        [HttpGet]
        public async Task<IActionResult> GetRegionsWithNoCache()
        {
            var regionList = await _regionRepo.GetRegionsWithNoCache();
            var regions = _mapper.Map<List<RegionDTO>>(regionList);
            return new OkObjectResult(regions);
        }

        [HttpGet("GetNonCacheRegion/{lang}")]
        public async Task<IActionResult> GetNonCacheRegion(string lang)
        {
            var regionList = await _regionRepo.GetNonCacheRegion(lang);
            // var regions = _mapper.Map<List<RegionDTO>>(regionList);
            return new OkObjectResult(regionList);
        }
    }
}
