﻿using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers.Address
{
    [Route("api/woreda")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class WoredaController : Controller
    {
        private readonly WoredaRepository _woredaRepo;
        private readonly IMapper _mapper;

        public WoredaController(WoredaRepository woredaRepository, IMapper mapper)
        {
            _woredaRepo = woredaRepository;
            _mapper = mapper;
        }

        [HttpGet("{lang}")]
        public async Task<IEnumerable<StaticData2>> GetWoreda(string lang)
        {
            return await _woredaRepo.GetWoredas(lang);
        }

        [HttpPost]
        public async Task<IActionResult> AddWoreda([FromBody] WoredaDTO postedWoreda)
        {
            var woreda = _mapper.Map<WoredaDTO, Woreda>(postedWoreda);
            var woredaDTO = await _woredaRepo.AddWoreda(woreda);
            return new OkObjectResult(woredaDTO);
        }

        [HttpGet("GetWoredas")]
        public async Task<IActionResult> GetWoredas()
        {
            var woredaList = await _woredaRepo.GetWoredas();
            var woredas = _mapper.Map<List<WoredaDTO>>(woredaList);
            return new OkObjectResult(woredas);
        }

        [HttpGet("GetWoredasByParentId/{zoneId}")]
        public async Task<IActionResult> GetWoredasByParentId(int zoneId)
        {
            var woredas = await _woredaRepo.GetWoredasByParentId(zoneId);
            var WoredaDTO = _mapper.Map<List<Woreda>, List<WoredaDTO>>(woredas);
            return new OkObjectResult(WoredaDTO);
        }

        [HttpGet("GetWoredasByParentIdAndLang/{zoneId}/{lang}")]
        public async Task<IActionResult> GetWoredasByParentIdAndLang(int zoneId, string lang)
        {
            var woredas = await _woredaRepo.GetWoredasByParentIdAndLang(zoneId, lang);
            // var WoredaDTO = _mapper.Map<List<Woreda>, List<WoredaDTO>>(woredas);
            return new OkObjectResult(woredas);
        }

        [HttpDelete("{WoredaId}")]
        public async Task<IActionResult> DeleteWoredasAsync(int woredaId)
        {
            var bWoredaDTO = await _woredaRepo.DeleteWoreda(woredaId);

            return new OkObjectResult(bWoredaDTO);
        }

        [HttpGet("GetWoredaById/{woredaId}")]
        public async Task<IActionResult> GetWoreda(int woredaId, bool bIsActive)
        {
            var Woreda = await _woredaRepo.GetWoreda(woredaId);
            var WoredaDTO = _mapper.Map<Woreda, WoredaDTO>(Woreda);
            return new OkObjectResult(WoredaDTO);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateWoredasAsync([FromBody] WoredaDTO postedWoreda)
        {
            var woreda = _mapper.Map<WoredaDTO, Woreda>(postedWoreda);
            var woredaDTO = await _woredaRepo.UpdateWoreda(woreda);
            return new OkObjectResult(woredaDTO);
        }

    }
}
