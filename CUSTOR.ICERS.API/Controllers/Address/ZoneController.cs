using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;



namespace CUSTOR.ICERS.WEBAPI.Controllers.Address
{
    [Route("api/zone")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ZoneController : Controller
    {
        private readonly ZoneRepository _zoneRepo;
        private readonly IMapper _mapper;

        public ZoneController(ZoneRepository zoneRepo, IMapper mapper)
        {
            _zoneRepo = zoneRepo;
            _mapper = mapper;
        }


        [HttpGet("{lang}")]
        public async Task<IEnumerable<StaticData2>> GetZone(string lang)
        {
            return await _zoneRepo.GetZones(lang);
        }

        [HttpGet("GetZones")]
        public async Task<IActionResult> GetZones()
        {
            var zoneList = await _zoneRepo.GetZones();
            var zones = _mapper.Map<List<ZoneDTO>>(zoneList);
            return new OkObjectResult(zones);
        }

        [HttpGet("GetZonesByParentIdAndLang/{regionId}/{lang}")]
        public async Task<IActionResult> GetZonesByParentIdAndLang(int regionId, string lang)
        {
            var zone = await _zoneRepo.GetZonesByParentIdAndLang(regionId, lang);
            // var zoneDTO = _mapper.Map<List<Zone>, List<ZoneDTO>>(zone);
            return new OkObjectResult(zone);
        }

        [HttpGet("GetZonesByParentId/{regionId}")]
        public async Task<IActionResult> GetZonesByParentId(int regionId)
        {
            var zone = await _zoneRepo.GetZonesByParentId(regionId);
            var zoneDTO = _mapper.Map<List<Zone>, List<ZoneDTO>>(zone);
            return new OkObjectResult(zone);
        }

        [HttpPost]
        public async Task<IActionResult> AddZone([FromBody] ZoneDTO postedZone)
        {
            var zone = _mapper.Map<ZoneDTO, Zone>(postedZone);
            var zoneDTO = await _zoneRepo.AddZone(zone);
            return new OkObjectResult(zoneDTO);
        }

        [HttpGet]
        public async Task<IActionResult> GetZonesWithNoCache()
        {
            var zoneList = await _zoneRepo.GetZonesWithNoCache();
            var zones = _mapper.Map<List<ZoneDTO>>(zoneList);
            return new OkObjectResult(zones);
        }

        [HttpDelete("{ZoneId}")]
        public async Task<IActionResult> DeleteZonesAsync(int zoneId)
        {
            var bZoneDTO = await _zoneRepo.DeleteZone(zoneId);

            return new OkObjectResult(bZoneDTO);

        }

        [HttpGet("GetZoneByZoneId/{zoneId}")]
        public async Task<IActionResult> GetZone(int zoneId)
        {
            var Zone = await _zoneRepo.GetZone(zoneId);
            var ZoneDTO = _mapper.Map<Zone, ZoneDTO>(Zone);
            return new OkObjectResult(ZoneDTO);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateZonesAsync([FromBody] ZoneDTO postedZone)
        {
            var zone = _mapper.Map<ZoneDTO, Zone>(postedZone);
            var zoneDTO = await _zoneRepo.UpdateZone(zone);
            return new OkObjectResult(zoneDTO);
        }
    }
}
