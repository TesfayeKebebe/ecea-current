using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers
{
    [Route("api/Audit")]
    [ApiController]
    public class AuditController : ControllerBase
    {
        private readonly AuditRepository _auditRepository;
        public AuditController(AuditRepository auditRepository)
        {
            _auditRepository = auditRepository;
        }
        [HttpGet]
        // [Authorize]
        public async Task<List<AuditDMV>> GetAuditByFilteration(string tableName, string userName, string ActionId, string start, string end)
        {
            return await _auditRepository.GetAuditByCriteria(tableName, userName, ActionId, start, end);
        }
        [HttpGet("Tables")]
        //[Authorize]
        public async Task<List<TableInformation>> GetAllTables()
        {
            return await _auditRepository.GetAllTables();
        }

    }
}
