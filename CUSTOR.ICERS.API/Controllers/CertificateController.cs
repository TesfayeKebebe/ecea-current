﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers
{
    [Route("api/certificate")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CertificateController: Controller
    {
        private readonly CertificateRepository _certificateRepository;
        public CertificateController(CertificateRepository certificateRepository)
        {
            _certificateRepository = certificateRepository;
        }
        [HttpPost]
        [Authorize]
        public async Task<int> PostCertificate(CertificateDTO postedCertificate)
        {
            return await _certificateRepository.PostCertificate(postedCertificate);
        }
    }
}
