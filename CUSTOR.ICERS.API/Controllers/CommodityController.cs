﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers
{

    [Route("api/commodity")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CommodityController
    {
        private readonly CommodityRepository _commodityRepository;

        public CommodityController(CommodityRepository commodityRepository)
        {
            _commodityRepository = commodityRepository;
        }

        [HttpGet("GetAllCommodity/{lang}")]
        public async Task<List<StaticData>> GetAllCommodity(string lang)
        {
            return await _commodityRepository.GetAllCommodity(lang);
        }
        [HttpPost]
        [Route("Create")]
        public async Task<int> Create(CommodityDTO reportPeriod)
        {
            return await _commodityRepository.Create(reportPeriod);
        }
        [HttpPost]
        [Route("Update")]
        public async Task<int> Update(CommodityDTO reportPeriod)
        {
            return await _commodityRepository.Update(reportPeriod);
        }
        [HttpPost]
        [Route("Delete")]
        public async Task<int> Delete(CommodityDTO reportPeriod)
        {
            return await _commodityRepository.Delete(reportPeriod);
        }
        [HttpGet]
        [Route("GetCommodityById/{reportPeriodId}")]
        public async Task<Commodity> GetCommodityById(int reportPeriodId)
        {
            return await _commodityRepository.GetCommodityById(reportPeriodId);
        }
        [HttpGet]
        [Route("GetAllCommodityList/{lang}")]
        public async Task<List<CommodityDTO>> GetAllCommodityList(string lang)
        {
            return await _commodityRepository.GetAllCommodityList(lang);
        }
    }
}
