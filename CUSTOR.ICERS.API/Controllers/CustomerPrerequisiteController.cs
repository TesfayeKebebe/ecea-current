using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers
{
    [Route("api/customerprerequisite")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CustomerPrerequisiteController
    {
        private readonly CustomerPrerequisiteRepository _customerprerequisiteRepo;

        public CustomerPrerequisiteController(CustomerPrerequisiteRepository customerprerequisiteRepo)
        {
            _customerprerequisiteRepo = customerprerequisiteRepo;
        }

        [HttpPost]
        public async Task<int> PostCustomerPrerequisite([FromBody] CustomerPrerequisiteDTO[] postedCustomerPrerequisite)
        {
            return await _customerprerequisiteRepo.PostCustomerPrerequisite(postedCustomerPrerequisite);
        }

        [HttpGet("{saId}/{serPreId}")]
        public async Task<CustomerPrerequisite> GetCustomerPrerequisite(Guid saId, int serPreId)
        {
            return await _customerprerequisiteRepo.GetCustomerPrerequisite(saId, serPreId);
        }

        [HttpPost("prerequisitedoc")]
        public async Task<CustomerPrerequisite> UploadPrerequisiteDocument([FromForm] PrerequisteDocumentDTO documentTobeUploaded)
        {
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Documents");
            string fileType = documentTobeUploaded.FileType;

            string extension = fileType == "application/pdf" ? ".pdf" : fileType == "image/jpeg" ? ".jpeg" : ".png";
            string fileName = Path.Combine(documentTobeUploaded.ServiceApplicationId.ToString() + "_" + documentTobeUploaded.ServicePrerequisiteId + extension);

            var filePath = Path.Combine(pathToSave, fileName);


            return await _customerprerequisiteRepo.UploadPrerequisiteDocument(documentTobeUploaded, fileName, filePath, pathToSave);
        }

        [HttpDelete("{servicePrerequisiteId}/{fileName}/{serviceApplicationId}")]
        public async Task<CustomerPrerequisite> DeletePrerequisiteWithDocument([FromRoute] int servicePrerequisiteId, string fileName, Guid serviceApplicationId)
        {
            return await _customerprerequisiteRepo.DeletePrerequisiteWithDocument(servicePrerequisiteId, fileName, serviceApplicationId);
        }

        [HttpDelete("{servicePrerequisiteId}/{serviceApplicaitonId}")]
        public async Task<bool> DeletePrerequisite([FromRoute] int servicePrerequisiteId, Guid serviceApplicaitonId)
        {
            return await _customerprerequisiteRepo.DeletePrerequisite(servicePrerequisiteId, serviceApplicaitonId);
        }
    }
}
