using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Dashboard;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReportRepo;
using CUSTOR.ICERS.Core.EntityLayer.Dashboard;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Dashboard
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class EceaDashboardController : ControllerBase
    {
        private readonly RecognationDashboardRepository recognationDashboard;
        private readonly OversightReportRepo _oversightReportRepo;

        public EceaDashboardController(RecognationDashboardRepository recognationDashboard, OversightReportRepo _oversightReportRepo)
        {
            this.recognationDashboard = recognationDashboard;
            this._oversightReportRepo = _oversightReportRepo;
        }
        [HttpGet("getTotalCountInRecognation")]
        public async Task<List<DashboardModel>> GetTotalCountInRecognation()
        {
            return await recognationDashboard.GetTotalCountInRecognation();
        }
    [HttpGet("getCommon")]
    public async Task<CommonModel> GetCommon()
    {
      return await recognationDashboard.GetCommon();
    }

    [HttpGet("getOversightFollowuprReportByDecision")]
        public async Task<List<OversightDashbordDataByDecision>> GetOversightFollowupReportByDecision()
        {
            return await _oversightReportRepo.GetFollowupReportByFinalDecision();
        }
        [HttpGet("law/getTotalCountInLawCase")]
        public async Task<List<DashboardLawModel>> GetTotalCountInLawCase()
        {
            return await recognationDashboard.GetTotalCountInLawCase();
        }

        [HttpGet("at/getTotalCountInAtCase")]
        public async Task<List<DashboardAtModel>> GetTotalCountInAtCase()
        {
            return await recognationDashboard.GetTotalCountInAtCase();
        }
    }
}
