﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.DataChanges
{
    [Route("api/addresschange")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class AddressChangeController
    {
        private readonly AddressChangeRepository _addressChangeRepository;


        public AddressChangeController(AddressChangeRepository addressChangeRepository)
        {
            _addressChangeRepository = addressChangeRepository;
        }


        [HttpPost]
        public async Task<IActionResult> SaveAddressChange(AddressChangeDTO addressChangeData)
        {
            return await _addressChangeRepository.SaveAddressChange(addressChangeData);
        }

        [HttpPost("postMangerAddressChange")]
        public async Task<bool> SaveManagerAddressChange(AddressChangeDTO addressChangeData)
        {
            return await _addressChangeRepository.SaveManagerAddressChange(addressChangeData);
        }


        [HttpGet("{serviceApplicationId}")]
        public async Task<AddressChangeDTO> GetOrganizationAddressChangeData([FromRoute] Guid serviceApplicationId)
        {
            return await _addressChangeRepository.GetOrganizationAddressChangeData(serviceApplicationId);
        }

        [HttpPost("{serviceApplicationId}/{exchangeActorId}")]
        public async Task<bool> SaveApprovedAddressChange([FromRoute] Guid serviceApplicationId, Guid exchangeActorId)
        {
            return await _addressChangeRepository.SaveApprovedAddressChange(serviceApplicationId, exchangeActorId);
        }
    }
}
