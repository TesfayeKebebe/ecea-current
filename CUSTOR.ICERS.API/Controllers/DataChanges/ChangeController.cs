﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers.DataChanges
{
    [Route("api/change")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ChangeController : Controller
    {
        private readonly ChangeRepository _changeRepository;

        public ChangeController(ChangeRepository changeRepository)
        {
            _changeRepository = changeRepository;
        }

        //[HttpPost]
        //public async Task<bool> SaveAddressChange(AddressChangeDTO addressChangeData)
        //{
        //    return _changeRepository.SaveAddressChange();
        //}
    }
}
