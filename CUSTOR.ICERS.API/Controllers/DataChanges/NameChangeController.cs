﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.DataChanges
{
    [Route("api/namechange")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class NameChangeController : Controller
    {
        private readonly NameChangeRepository _nameChangeRepository;
        public NameChangeController(NameChangeRepository nameChangeRepository)
        {
            _nameChangeRepository = nameChangeRepository;
        }

        [HttpGet("{exchangeActorId}")]
        public async Task<AddressChangeDTO> GetCustomerName([FromRoute] Guid exchangeActorId)
        {
            return await _nameChangeRepository.GetCustomerName(exchangeActorId);
        }

        [HttpPost]
        public async Task<IActionResult> SaveNameChange(AddressChangeDTO nameChangeData)
        {
            return await _nameChangeRepository.SaveNameChange(nameChangeData);
        }

        [HttpGet("getNameChangeData/{serviceApplicationId}")]
        public async Task<AddressChangeDTO> GetNameChangeData([FromRoute] Guid serviceApplicationId)
        {
            return await _nameChangeRepository.GetNameChangeData(serviceApplicationId);
        }

        [HttpPost("{serviceApplicationId}/{exchangeActorId}")]
        public async Task<bool> SaveApprovedNameChange([FromRoute] Guid serviceApplicationId, Guid exchangeActorId)
        {
            return await _nameChangeRepository.SaveApprovedNameChange(serviceApplicationId, exchangeActorId);
        }
    }
}
