﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.OTRLS.Core;
using CUSTOR.OTRLS.Core.DataAccessLayer;
using CUSTOR.OTRLS.Core.EntityLayer.ECX;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.OTRLS.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Ecex")]
    //[ServiceFilter(typeof(ApiExceptionFilter))]
    //[EnableCors("CorsPolicy")]
    public class EcexController : ControllerBase
    {
        private readonly EceRepository ecxRepository;
        private readonly IHostingEnvironment host;
        public EcexController(EceRepository rep, IHostingEnvironment host)
        {
            ecxRepository = rep;
            this.host = host;
        }
        [HttpGet]
        public async Task<List<EcexDTo>> GetAll(string start, string end = null, string sessionId = null, string ProductType = null)
        {
            return await ecxRepository.GetEcx(start, end, sessionId, ProductType);
        }
        [HttpGet("ecex/Product")]
        public async Task<List<EcexProductDto>> GetProduct()
        {
            return await ecxRepository.GetProduct();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IEnumerable<EXE>> GetById(int id)
        {
            return await ecxRepository.GetById(id);
        }
        //[HttpGet("amharic/current/date")]
        //public IActionResult AmharicCurrentDate()
        //{

        //    var response = EthiopicCalender.EthiopicDateTime.GetEthiopicDate(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        //    return new ObjectResult(response);
        //}

    }
}