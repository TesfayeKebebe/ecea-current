using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommodityLookupController : ControllerBase
    {
    public readonly CommodityLookupRepository _commodityLookupRepository;
    public CommodityLookupController(CommodityLookupRepository commodityLookupRepository)
    {
      _commodityLookupRepository = commodityLookupRepository;
    }
    /// <summary>
    /// get commodity lookup
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<CommodityLookupVM>>  getCommodityLookUp()
    {
      return await _commodityLookupRepository.GetCommodityLookUp();
    }
    }
}
