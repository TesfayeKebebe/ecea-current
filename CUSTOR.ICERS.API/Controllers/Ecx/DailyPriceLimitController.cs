using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyPriceLimitController : ControllerBase
    {
    public readonly DailyPriceLimitRepository _dailyPriceLimitRepository;
    public DailyPriceLimitController(DailyPriceLimitRepository dailyPriceLimitRepository)
    {
      _dailyPriceLimitRepository = dailyPriceLimitRepository;
    }
    /// <summary>
    /// get daily price limit by date range
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<DailyPriceLimitVM>> GetDailyPriceLimits(DateTime from, DateTime to)
    {
      return await _dailyPriceLimitRepository.getDailyPriceLimit(from, to);
    }
  }
}
