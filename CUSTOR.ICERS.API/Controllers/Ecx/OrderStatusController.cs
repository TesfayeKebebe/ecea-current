using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.X509;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderStatusController : ControllerBase
    {
    public readonly OrderStatusRepository _orderStatusRepository;
    public OrderStatusController(OrderStatusRepository orderStatusRepository)
    {
      _orderStatusRepository = orderStatusRepository;
    }
    /// <summary>
    /// get order status
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<OrderStatusVM>>  GetOrderStatusVM()
    {
      return await _orderStatusRepository.GetOrderStatus();
    }
    }
}
