using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderValidityController : ControllerBase
    {
    public readonly OrderValidityRepository _orderValidityRepository;
    public OrderValidityController(OrderValidityRepository orderValidityRepository)
    {
      _orderValidityRepository = orderValidityRepository;
    }
    /// <summary>
    /// get order validities
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<OrderValidityVM>> GetOrderValidities()
    {
      return await _orderValidityRepository.GetOrderValidities();
    }
  }
}
