using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionDataController : ControllerBase
    {
    public readonly SessionDataRepository _sessionDataRepository;
    public SessionDataController(SessionDataRepository sessionDataRepository)
    {
      _sessionDataRepository = sessionDataRepository;
    }
    /// <summary>
    /// get session data by date range
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
       [HttpGet]
    public async Task<List<SessionDataVM>> GetSessionDatas(DateTime from, DateTime to)
    {
      return await _sessionDataRepository.GetSessionDatas(from, to);
    }
    }
}
