using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionStatusController : ControllerBase
    {
    public readonly SessionStatusRepository _sessionStatusRepository;
    public SessionStatusController(SessionStatusRepository sessionStatusRepository)
    {
      _sessionStatusRepository = sessionStatusRepository;
    }
    /// <summary>
    /// get session status
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<SessionStatusVM>>  GetSessionStatus()
    {
      return await _sessionStatusRepository.GetSessionStatus();
    }
    }
}
