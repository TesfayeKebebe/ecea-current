using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubSessionController : ControllerBase
    {
    public readonly SubSessionRepository _subSessionRepository;
    public SubSessionController(SubSessionRepository subSessionRepository)
    {
      _subSessionRepository = subSessionRepository;
    }
    /// <summary>
    /// get sub session by date range
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<SubSessionVM>>  GetSubSessions(DateTime from, DateTime to)
    {
      return await _subSessionRepository.GetSubSessions(from, to);
    }
    }
}
