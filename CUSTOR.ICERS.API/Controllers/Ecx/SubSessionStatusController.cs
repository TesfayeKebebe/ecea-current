using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubSessionStatusController : ControllerBase
    {
    public readonly SubSessionStatusRepository _subSessionStatusRepository;
    public SubSessionStatusController(SubSessionStatusRepository subSessionStatusRepository)
    {
      _subSessionStatusRepository = subSessionStatusRepository;

    }
    /// <summary>
    /// get subsession status
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<SubSessionStatusVM>> GetSubSessionStatus()
    {
      return await _subSessionStatusRepository.GetSubSessionStatus();
    }
  }
}
