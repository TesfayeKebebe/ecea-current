using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradeDataController : ControllerBase
    {
    public readonly TradeDataRepository _tradeDataRepository;
    public TradeDataController(TradeDataRepository tradeDataRepository)
    {
      _tradeDataRepository = tradeDataRepository;
    }
    /// <summary>
    /// get trade data by date range
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<TradeDataVM>> GetTradeDatas(DateTime from , DateTime to)
    {
      return await _tradeDataRepository.GetTradeDatas(from,to);
    }
    }
}
