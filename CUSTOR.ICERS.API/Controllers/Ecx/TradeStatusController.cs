using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradeStatusController : ControllerBase
    {
    public readonly TradeStatusRepository _tradeStatusRepository;
    public TradeStatusController(TradeStatusRepository  tradeStatusRepository)
    {
      _tradeStatusRepository = tradeStatusRepository;
    }
    [HttpGet]
    public async Task<List<TradeStatusVM>> GetTradeStatus()
    {
      return  await _tradeStatusRepository.getTradeStatus();
    }
    }
}
