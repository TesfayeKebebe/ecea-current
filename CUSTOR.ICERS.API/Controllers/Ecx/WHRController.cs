using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class WHRController : ControllerBase
    {
    public readonly WHRRepository _wHRRepository;
    public WHRController(WHRRepository wHRRepository)
    {
      _wHRRepository = wHRRepository;
    }
    [HttpGet]
    public async Task<List<WHRVM>> GetWHRs()
    {
      return await _wHRRepository.GetWHRs();
    }
  }
}
