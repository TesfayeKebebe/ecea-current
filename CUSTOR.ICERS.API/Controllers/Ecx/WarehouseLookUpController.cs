using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Ecx
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseLookUpController : ControllerBase
    {
    public readonly WarehouseLookUpRepository _warehouseLookUpRepository;
    public WarehouseLookUpController(WarehouseLookUpRepository warehouseLookUpRepository)
    {
      _warehouseLookUpRepository = warehouseLookUpRepository;
    }
    /// <summary>
    /// get warehouse lookup
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<WarehouseVM>> GetWarehouses()
    {
      return await _warehouseLookUpRepository.getWareHouses();
    }
    }
}
