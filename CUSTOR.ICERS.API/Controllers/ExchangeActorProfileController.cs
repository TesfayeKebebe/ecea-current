﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers
{
    [Route("api/exchangeactorprofile")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ExchangeActorProfileController : Controller
    {
        private readonly ExchangeActorProfileRepository _exchangeActorProfileRepository;
        public ExchangeActorProfileController(ExchangeActorProfileRepository exchangeActorProfileRepository)
        {
            _exchangeActorProfileRepository = exchangeActorProfileRepository;
        }
        // GET: /<controller>/
        // 
        [HttpGet("{lang}")]
        public async Task<List<ExchangeActorProfile>> GetExchageActorProfile(string lang)
        {
            /*
             * PageNumber = 1
             * PageCount = 4
             */
            return await _exchangeActorProfileRepository.GetExchangeActroProfile(lang);
        }

        [HttpGet]
        [Route("customerorganizationinfo/{lang}/{exchangeActorId}/{customerTypeId}")]
        public async Task<IActionResult> GetCustomerOrganizationInfo([FromRoute] string lang, Guid exchangeActorId, int customerTypeId)
        {
            var queryResult = await _exchangeActorProfileRepository.GetCustomerOrganizationInfo(lang, exchangeActorId, customerTypeId);

            var result = new OkObjectResult(queryResult);

            return result;
        }

        [HttpGet]
        [Route("managerinfo/{lang}/{exchangeActorId}")]
        public async Task<ActionResult<IEnumerable<ManagerProfileVM>>> GetManagerInfo([FromRoute] string lang, Guid exchangeActorId)
        {
            var queryResult = await _exchangeActorProfileRepository.GetOwnerMangerInfo(lang, exchangeActorId);

            var result = new OkObjectResult(queryResult);

            return result;
        }

        [HttpGet]
        [Route("represntativeinfo/{lang}/{exchangeActorId}")]
        public async Task<ActionResult<IEnumerable<RepresntativeVM>>> GetRepresntativeInfo([FromRoute] string lang, Guid exchangeActorId)
        {
            var queryResult = await _exchangeActorProfileRepository.GetRepresentativeProfile(lang, exchangeActorId);

            var result = new OkObjectResult(queryResult);

            return result;
        }

    }
}
