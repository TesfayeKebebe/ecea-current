using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    /// <summary>
    /// Helper API
    /// CAUnit|Case administration unit stands for (CaseApplication,CaseInvestigation,Case,CaseTracking)
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CAUnitController : ControllerBase
    {
        private readonly CAUnitRepository _cAUnitRepository;

        public CAUnitController(CAUnitRepository cAUnitRepository)
        {
            _cAUnitRepository = cAUnitRepository;
        }

        /// <summary>
        /// Get general information about case administration unit 
        /// CAUnit|Case administration unit stands for (CaseApplication,CaseInvestigation,Case,CaseTracking)
        /// </summary>
        /// <param name="caUnitId"></param>
        /// <param name="unitType"></param>
        /// <returns></returns>
        [HttpGet("{caUnitId}/{unitType}")]
        public async Task<CAUnitInfo> GetInfo(Guid caUnitId, CaseAdministrationUnitType unitType)
        {
            return await _cAUnitRepository.GetCAUnitInfo(caUnitId, unitType);
        }

    }
}
