using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CaseApplicationController : ControllerBase
    {
        private readonly CaseApplicationRepository _applicationRepo;
        private readonly CaseFeedbackRepository _feedbackRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public CaseApplicationController(CaseApplicationRepository applicationRepo, CaseFeedbackRepository feedbackRepo, UserManager<ApplicationUser> userManager)
        {
            _applicationRepo = applicationRepo;
            _feedbackRepo = feedbackRepo;
            _userManager = userManager;
        }

        /// <summary>
        /// Create new case application
        /// </summary>
        /// <param name="postedApplication"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<bool> PostCaseApplication([FromBody] CaseApplicationEditDTO postedApplication)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedApplication.CreatedUserId = Guid.Parse(user.Id);
            return await _applicationRepo.Create(postedApplication);
        }
        [HttpPost("SaveCase")]
        [Authorize]
        public async Task<CaseApplicationPartyDTO> PostCaseApplication([FromBody] CaseApplicationPartyDTO caseApplicationDTO) 
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            caseApplicationDTO.CreatedUserId = Guid.Parse(user.Id);
            CaseApplicationPartyDTO ca = null;

            try
            {
                ca = await _applicationRepo.SaveCaseApplication(caseApplicationDTO);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex.Message);
            }
            return ca;
        }

        /// <summary>
        /// Update case application
        /// </summary>
        /// <param name="postedApplication"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public async Task<bool> UpdateCaseApplication([FromBody] CaseApplicationEditDTO postedApplication)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedApplication.UpdatedUserId = Guid.Parse(user.Id);
            return await _applicationRepo.Update(postedApplication);
        }

        /// <summary>
        /// Get Application Detail for Edit
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet("{lang}/{caseApplicationId}")]
        public async Task<CaseApplicationEditDTO> GetCaseApplicationById(string lang, Guid caseApplicationId)
        {
            return await _applicationRepo.GetCaseApplicationById(lang, caseApplicationId);
        }
        /// <summary>
        /// Get Application Detail for Edit
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet("{lang}/{caseApplicationId}/party")]
        public async Task<CaseApplicationPartyDTO> getCaseApplication(string lang, Guid caseApplicationId)
        {
            return await _applicationRepo.getCaseApplication(lang, caseApplicationId);
        }

        /// <summary>
        /// Get Application Detail for Display
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDetail/{lang}/{caseApplicationId}")]
        public async Task<CaseApplicationDTO> GetCaseApplicationDetailById(string lang, Guid caseApplicationId)
        {
            return await _applicationRepo.GetCaseApplicationDetailById(lang, caseApplicationId);
        }
        /// <summary>
        /// Get Application Detail for Display
        /// </summary> 
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrint/Detail")]
        public async Task<LetterContent> GetCaseApplicationPrintDetailById(Guid caseApplicationId)
        {
            return await _applicationRepo.GetCaseApplicationPrintDetailById(caseApplicationId);
        }
        /// <summary>
        /// Get Application Detail for Display
        /// </summary>
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrint/Warning/Detail")]
        public async Task<LetterContent> GetCaseApplicationWarningPrintDetailById(Guid caseApplicationId)
        {
            return await _applicationRepo.GetCaseApplicationWarningPrintDetailById(caseApplicationId);
        }
        /// <summary>
        /// Get Application Detail for Display
        /// </summary> 
        /// <param name="caseApplicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrint/Terminated/Closed/Detail")]
        public async Task<LetterContent> GetCaseApplicationTerminatedOrClosedPrintDetailById(Guid caseApplicationId)
        {
            return await _applicationRepo.GetCaseApplicationTerminatedOrClosedPrintDetailById(caseApplicationId);
        }
        [HttpGet]
        public async Task<PagedResult<CaseApplicationListModel>> SearchCaseApplication([FromQuery] CaseApplicationSearchCriteriaModel searchQueryParameters)
        {
            return await _applicationRepo.SearchApplication(searchQueryParameters);
        }
        [HttpGet("investigationList")]
        [Authorize]
        public async Task<PagedResult<CaseApplicationListModel>> SearchInvestigationCaseApplication([FromQuery] CaseApplicationSearchCriteriaModel searchQueryParameters)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
             
            return await _applicationRepo.SearchInvestigationApplication(searchQueryParameters, Guid.Parse(user.Id));
        }
        [HttpGet("Officer/DataList")]
        [Authorize]
        public async Task<PagedResult<CaseApplicationListModel>> SearchOfficerInvestigationCaseApplication([FromQuery] CaseApplicationSearchCriteriaModel searchQueryParameters)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            return await _applicationRepo.SearchOfficerInvestigationApplication(searchQueryParameters, Guid.Parse(user.Id));
        }
        [HttpGet("rejectedList")]
        public async Task<PagedResult<CaseApplicationListModel>> SearchRejectedListCaseApplication([FromQuery] CaseApplicationSearchCriteriaModel searchQueryParameters)
        {
            return await _applicationRepo.SearchRejectedListApplication(searchQueryParameters);
        }

    }
}
