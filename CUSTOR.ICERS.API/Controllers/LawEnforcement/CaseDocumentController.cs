using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CaseDocumentController : ControllerBase
    {
        private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".jpg", ".jpeg", ".png" };
        private readonly bool SaveDocumentsToDisc = false;
        private readonly IHostingEnvironment _host;
        private readonly CaseDocumentRepository _documentRepo;
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _userManager;
        public CaseDocumentController(IHostingEnvironment host, IConfiguration configuration, CaseDocumentRepository documentRepo, UserManager<ApplicationUser> userManager)
        {
            _documentRepo = documentRepo;
            _host = host;
            _configuration = configuration;
            _userManager = userManager;
        }

        /// <summary>
        /// Attach new on Case Application/Investigation/Court-Day 
        /// </summary>
        /// <param name="postedDocument"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Upload")]
        [Authorize]
        public async Task<bool> PostCaseDocument([FromForm] CaseDocumentDTO postedDocument)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedDocument.CreatedUserId = Guid.Parse(user.Id);
            postedDocument.CaseDocumentId = Guid.NewGuid();

            return await _documentRepo.Create(postedDocument);
        }

        /// <summary>
        /// Update Case Document
        /// </summary>
        /// <param name="postedCaseDocument"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdateCaseDocument([FromForm] CaseDocumentDTO postedCaseDocument)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseDocument.UpdatedUserId = Guid.Parse(user.Id);
            return await _documentRepo.Update(postedCaseDocument);
        }

        /// <summary>
        /// Get Case Document
        /// </summary>
        /// <param name="caseDocumentId"></param>
        /// <returns></returns>
        [HttpGet("{caseDocumentId}")]
        public async Task<CaseDocumentDTO> GetCasePartyForEdit(Guid caseDocumentId)
        {
            return await _documentRepo.GetCaseDocumentById(caseDocumentId);
        }
        /// <summary>
        /// Get all documents attached at Case Application/Investigation/Court-Day 
        /// </summary>
        /// <param name="caUnitId">Case AdministrationId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllByCAUnit/{caUnitId}")]
        public async Task<IEnumerable<CaseDocumentDTO>> GetAllByCAUnitId(Guid caUnitId)
        {
            return await _documentRepo.GetAllByCAUnitId(caUnitId);
        }

        private async Task<CaseDocumentDTO> SaveFileToDisc(CaseDocumentDTO postedDocument)
        {
            var filesData = postedDocument.ActualFile;
            //Guid fileId = Guid.Empty;
            bool isValid = false;
            if (!(filesData == null ||
                filesData.Length == 0 ||
                filesData.Length > 10 * 1024 * 1024 ||
                !ACCEPTED_FILE_TYPES.Any(s => s == Path.GetExtension(filesData.FileName).ToLower())
                )
                )
            {
                isValid = true;
            }

            var uploadFilesPath = Path.Combine(_host.WebRootPath, "uploads");
            if (isValid)
            {
                if (!Directory.Exists(uploadFilesPath))
                    Directory.CreateDirectory(uploadFilesPath);
                var fileId = postedDocument.CaseDocumentId.HasValue && !postedDocument.CaseDocumentId.Equals(Guid.Empty) ? postedDocument.CaseDocumentId : Guid.NewGuid();
                var fileName = fileId.ToString() + Path.GetExtension(filesData.FileName);
                var filePath = Path.Combine(uploadFilesPath, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await filesData.CopyToAsync(stream);
                    postedDocument.CaseDocumentId = fileId;
                    var fileUrl = _configuration["ApplicationUrl"] + "/uploads/" + fileName;
                    postedDocument.FileName = postedDocument.FileName == null ? fileName : postedDocument.FileName;
                    postedDocument.Url = fileUrl;
                    postedDocument.FileLocation = filePath;
                }
            }

            return postedDocument;
        }

    }
}
