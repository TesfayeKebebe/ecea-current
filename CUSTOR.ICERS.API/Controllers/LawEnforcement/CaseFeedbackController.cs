using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CaseFeedbackController : ControllerBase
    {
        private readonly CaseFeedbackRepository _feedbackRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public CaseFeedbackController(CaseFeedbackRepository feedbackRepo, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _feedbackRepo = feedbackRepo;
        }

        /// <summary>
        /// Add new feedback
        /// </summary>
        /// <param name="postedCaseFeedback"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<bool> PostCaseFeedback([FromBody] CaseFeedbackDTO postedCaseFeedback)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseFeedback.CreatedUserId = Guid.Parse(user.Id);
            return await _feedbackRepo.Create(postedCaseFeedback, Guid.Parse(user.Id));
        }

        /// <summary>
        /// Update Feedback
        /// </summary>
        /// <param name="postedCaseFeedback"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdateCaseFeedback([FromBody] CaseFeedbackDTO postedCaseFeedback)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //postedCaseFeedback.UpdatedUserId = Guid.Parse(user.Id);
            postedCaseFeedback.CreatedUserId = Guid.Parse(user.Id);
            return await _feedbackRepo.Update(postedCaseFeedback, Guid.Parse(user.Id));
        }

        /// <summary>
        /// Update Feedback
        /// </summary>
        /// <param name="postedCaseFeedback"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Assign")]
        [Authorize]
        public async Task<bool> AssignCaseFeedback([FromBody] CaseFeedbackDTO postedCaseFeedback)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseFeedback.UpdatedUserId = Guid.Parse(user.Id);
            postedCaseFeedback.CreatedUserId = Guid.Parse(user.Id);
            return await _feedbackRepo.Assign(postedCaseFeedback, Guid.Parse(user.Id));
        }
        /// <summary>
        /// Update Feedback
        /// </summary>
        /// <param name="postedCaseFeedback"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("AssignProsececution")]
        [Authorize]
        public async Task<bool> AssignProsececutionCaseFeedback([FromBody] CaseFeedbackDTO postedCaseFeedback)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //postedCaseFeedback.UpdatedUserId = Guid.Parse(user.Id);
            postedCaseFeedback.CreatedUserId = Guid.Parse(user.Id);
            return await _feedbackRepo.AssignProsececution(postedCaseFeedback, Guid.Parse(user.Id));
        }

        /// <summary>
        /// Get Feedback
        /// </summary>
        /// <param name="caseFeedbackId"></param>
        /// <returns></returns>
        [HttpGet("{caseFeedbackId}")]
        public async Task<CaseFeedbackDTO> GetCaseFeedback(int caseFeedbackId)
        {
            return await _feedbackRepo.GetCaseFeedbackById(caseFeedbackId);
        }

        /// <summary>
        /// Get All Application/Preliminary investigation state feedbacks
        /// </summary>
        /// <param name="caUnitId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("GetAllPreliminaryFeedbacks/{caUnitId}")]
        public async Task<CaseFeedbackListDTO> GetAllPreliminaryFeedbacks(Guid caUnitId)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var currentLoggedId = user?.Id.ToString();
            return await _feedbackRepo.GetAllPreliminaryFeedbacks(caUnitId, currentLoggedId);
        }
        /// <summary>
        /// Get All Full-flegded investigation state feedbacks
        /// </summary>
        /// <param name="caseInvestigationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("GetAllInvestigationFeedbacks/{caseInvestigationId}")]
        public async Task<CaseFeedbackListDTO> GetAllInvestigationFeedbacks(Guid caseInvestigationId)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var currentLoggedId = user?.Id.ToString();
            return await _feedbackRepo.GetAllInvestigationFeedbacks(caseInvestigationId, currentLoggedId);
        }
        [HttpGet("Caseexpertfeedback")]
        public async Task<CaseFeedbackDTO> GetCaseexpertfeedbackSummurys(Guid caseInvestigation)
        {
            return await _feedbackRepo.GetCaseexpertfeedbackSummurys(caseInvestigation);
        }
    }
}
