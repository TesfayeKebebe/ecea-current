using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CaseInvestigationController : ControllerBase
    {
        private readonly CaseInvestigationRepository _invRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public CaseInvestigationController(CaseInvestigationRepository invRepo, UserManager<ApplicationUser> userManager)
        {
            _invRepo = invRepo;
            _userManager = userManager;
        }

        /// <summary>
        /// Add/Start new investigation
        /// </summary>
        /// <param name="postedInvestigation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<CaseInvestigationDTO> PostCaseInvestigation([FromBody] CaseInvestigationDTO postedInvestigation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedInvestigation.CreatedUserId = Guid.Parse(user.Id);
            return await _invRepo.Create(postedInvestigation);
        }

        /// <summary>
        /// Update Investigation
        /// </summary>
        /// <param name="postedInvestigation"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdateCaseInvestigation([FromBody] CaseInvestigationDTO postedInvestigation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedInvestigation.UpdatedUserId = Guid.Parse(user.Id);
            return await _invRepo.Update(postedInvestigation);
        }

        /// <summary>
        /// Get Investigation
        /// </summary>
        /// <param name="caseInvestigationId"></param>
        /// <returns></returns>
        [HttpGet("{caseInvestigationId}")]
        public async Task<CaseInvestigationDTO> GetInvestigation(Guid caseInvestigationId)
        {
            return await _invRepo.GetCaseInvestigationById(caseInvestigationId);
        }
        [HttpGet]
        public async Task<PagedResult<CaseInvestigationListModel>> SearchCaseInvestigation([FromQuery] CaseInvestigationSearchCriteriaModel searchQueryParameters)
        {
            return await _invRepo.SearchInvestigation(searchQueryParameters);
        }
    }
}
