﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]

    public class CaseLookupController : ControllerBase
    {
        private readonly CaseLookUpRepository _lookupRepo;
        private readonly CaseStatusRepository _statusRepo;
        private readonly UserManager<ApplicationUser> _userManager;

            public CaseLookupController(CaseLookUpRepository lookupRepo, CaseStatusRepository statusRepo, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _lookupRepo = lookupRepo;
            _statusRepo = statusRepo;
        }

        [HttpGet("{lang}/{groupCode}")]
        public async Task<IEnumerable<CaseLookUpDTO>> GetLookups(string lang, string groupCode)
        {
            return await _lookupRepo.GetAllByCategory(lang, groupCode);
        }
        [HttpGet("{lang}/{lookupTypeId}")]
        public async Task<IEnumerable<CaseLookupDTO5>> GetLookups(string lang, int lookupTypeId)
        {
            return await _lookupRepo.GetLookups(lang, lookupTypeId);
        }
        [HttpDelete("delete/{id}")]
        public async Task<bool> DeleteLookupValue(int id)
        {
            return await _lookupRepo.DeleteLookUpValue(id);
        }
        [HttpDelete("deleteLookupType/{id}")]
        public async Task<bool> DeleteLookupType(int id)
        {
            return await _lookupRepo.DeleteLookUpType(id);
        }

        [HttpPost("SaveLookup")]
        public async Task<bool> SaveLookup([FromBody] CaseLookupDTO4 lookup)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (lookup.CaseLookUpId == 0)
            {
                lookup.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                lookup.UpdatedUserId = Guid.Parse(user.Id);
            }
            return await _lookupRepo.SaveLookup(lookup);
        }
        [HttpPost("SaveLookupType")]
        public async Task<bool> SaveLookupType([FromBody] CaseLookupTypeDTO lookup)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (lookup.CaseLookUpTypeId == 0)
            {
                lookup.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                lookup.UpdatedUserId = Guid.Parse(user.Id);
            }
            return await _lookupRepo.SaveLookupType(lookup);
        }
        [HttpGet("{lookupTypeId}")]
        public async Task<List<CaseLookupDTO4>> GetLookupsById(int lookupTypeId)
        {
            return await _lookupRepo.GetLookupsById(lookupTypeId);
        }
        [HttpGet("GetAllLookUpType")]
        public async Task<List<CaseLookupTypeDTO>> GetAllLookUpType()
        {
            return await _lookupRepo.GetAllLookupTypeData();
        }
        [HttpGet]
        [Route("GetAllStatusTypes/{lang}")]
        public async Task<IEnumerable<CaseLookUpDTO>> GetAllStatusTypes(string lang)
        {
            return await _statusRepo.GetAll(lang);
        }

        [HttpGet]
        [Route("GetStatusTypeByMainCategory/{lang}/{groupCode}")]
        public async Task<IEnumerable<CaseLookUpDTO>> GetAllStatusByMainCategory(string lang, string groupCode)
        {
            return await _statusRepo.GetAllByMainCategory(lang, groupCode);
        }

        [HttpGet]
        [Route("GetStatusTypeBySubCategory/{lang}/{groupCode}")]
        public async Task<IEnumerable<CaseLookUpDTO>> GetAllStatusBySubCategory(string lang, string groupCode)
        {
            return await _statusRepo.GetAllBySubCategory(lang, groupCode);
        }

        [HttpGet]
        [Route("GetStatusForApplication/{lang}/{caseApplicationId}")]
        public async Task<IEnumerable<CaseLookUpDTO>> GetAllStatusByCaseApplication([FromRoute] string lang, [FromRoute] Guid caseApplicationId)
        {
            return await _statusRepo.GetAllForCaseApplication(lang, caseApplicationId);
        }

    }
}