using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto;
using CUSTOR.ICERS.Core.Enum;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CasePartyController : ControllerBase
    {
        private readonly CasePartyRepository _partyRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public CasePartyController(CasePartyRepository partyRepo, UserManager<ApplicationUser> userManager)
        {
            _partyRepo = partyRepo;
            _userManager = userManager;
        }

        /// <summary>
        /// Create new case witness or collaborator
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="postedCaseParty"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create/{caseId}")]
        [Authorize]
        public async Task<bool> PostCaseParty([FromRoute] Guid caseId, [FromBody] CasePartyEditDTO postedCaseParty)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseParty.CreatedUserId = Guid.Parse(user.Id);
            return await _partyRepo.Create(caseId, postedCaseParty);
        }

        /// <summary>
        /// Create new case witness or collaborator
        /// </summary>
        /// <param name="partyDTO"></param>
        /// <returns></returns> ///{caseId} Guid caseId,
        [HttpPost("Create/SaveParty")] 
        [Authorize]
        public async Task<PartyDTO> PostPlaintiffCaseParty([FromBody] PartyDTO partyDTO)
        { 
            PartyDTO ca = null;
            var user = await _userManager.GetUserAsync(HttpContext.User);
            partyDTO.CreatedUserId = Guid.Parse(user.Id);
            //partyParty.CreatedUserId = Guid.Parse(user.Id);
            try
            {
                ca = await _partyRepo.SavePlaintiffCaseParty(partyDTO);
            }
            catch (Exception ex)
            {
                throw new ApiException(ex.Message);
            }
            return ca;
        }
        /// <summary>
        /// Update Suspect|Plaintiff|Witness|Collaborator
        /// </summary>
        /// <param name="postedCaseParty"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdateCaseParty([FromBody] CasePartyEditDTO postedCaseParty)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseParty.UpdatedUserId = Guid.Parse(user.Id);
            return await _partyRepo.Update(postedCaseParty);
        }
        /// <summary>
        /// Get Application Detail for Edit
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        [HttpGet("{lang}/{partyId}/party")]
        public async Task<PartyDTO> getCaseParty(string lang, Guid partyId)
        {
            return await _partyRepo.getCaseParty(lang, partyId);
        }

        /// <summary>
        /// Get Case Suspect|Plaintiff|Witness|Collaborator for Edit
        /// </summary>
        /// <param name="casePartyId"></param>
        /// <returns></returns>
        [HttpGet("{casePartyId}")]
        public async Task<CasePartyEditDTO> GetCasePartyForEdit(Guid casePartyId)
        {
            return await _partyRepo.GetCasePartyById(casePartyId);
        }

        [HttpGet]
        [Route("GetAllByCategory/{caUnitId}/{partyCategory}/{caunitType}")]
        public async Task<IEnumerable<CasePartyDTO>> GetAllByCategory(Guid caUnitId, CasePartyCategory partyCategory, CaseAdministrationUnitType caunitType)
        {
            return await _partyRepo.GetAllByCategory(caUnitId, partyCategory, caunitType);
        }

        /// <summary>
        /// Add new case party hearings at court session
        /// </summary>
        /// <param name="postedCaseParty"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CourtSession/")]
        [Authorize]
        public async Task<bool> PostCasePartyAtCourtSession([FromBody] CasePartyTrackingDTO postedCaseParty)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseParty.CreatedUserId = Guid.Parse(user.Id);
            return await _partyRepo.CreateAtCourtSession(postedCaseParty);
        }

        /// <summary>
        /// Update case party hearings at court session
        /// </summary>
        /// <param name="postedCaseParty"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("CourtSession/")]
        [Authorize]
        public async Task<bool> PutCasePartyAtCourtSession([FromBody] CasePartyTrackingDTO postedCaseParty)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseParty.UpdatedUserId = Guid.Parse(user.Id);
            return await _partyRepo.UpdateAtCourtSession(postedCaseParty);
        }
        [HttpGet]
        [Route("CourtSession/GetAllAtCourtSession/{caUnitId}/{partyCategory}")]
        public async Task<IEnumerable<CasePartyDTO>> GetAllAtCourtSession(Guid caUnitId, CasePartyCategory partyCategory)
        {
            return await _partyRepo.GetAllAtCourtSession(caUnitId, partyCategory);
        }

        [HttpGet]
        [Route("CourtSession/{courtSessionId}/{casePartyId}")]
        public async Task<CasePartyTrackingDTO> GetPartyAtCourtSession(Guid courtSessionId, Guid casePartyId)
        {
            return await _partyRepo.GetPartyAtCourtSession(courtSessionId, casePartyId);
        }
        private readonly string AT_WITNESS_SUMMONS_TEMPLATE = $"fdsa";
    }
}
