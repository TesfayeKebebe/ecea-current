using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
  [Route("api/[controller]")]
  [ApiController]
  [ServiceFilter(typeof(ApiExceptionFilter))]
  [EnableCors("CorsPolicy")]
  public class CasePartyDocumentController : ControllerBase
  {
    private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".jpg", ".jpeg", ".png" };
    private readonly CasePartyDocumentRepository _casePartyDocumentRepo;
    private readonly UserManager<ApplicationUser> _userManager;
    public CasePartyDocumentController(CasePartyDocumentRepository applicationRepo, CaseFeedbackRepository feedbackRepo, UserManager<ApplicationUser> userManager)
    {
      _casePartyDocumentRepo = applicationRepo;
      _userManager = userManager;
    }
    [HttpPost]
    [Authorize]
    public async Task<bool> create([FromForm] CasePartyDocumentDto postedAttachment)
    {
      var user = await _userManager.GetUserAsync(HttpContext.User);
      postedAttachment.CreatedUserId = Guid.Parse(user.Id);
      return await _casePartyDocumentRepo.Create(postedAttachment);
    }
    [HttpPut]
    [Authorize]
    public async Task<bool>  Put([FromForm] CasePartyDocumentDto putAttachment)
    {
      var user = await _userManager.GetUserAsync(HttpContext.User);
      putAttachment.UpdatedUserId = Guid.Parse(user.Id);
      return await _casePartyDocumentRepo.Update(putAttachment);
    }
    [HttpGet]
    public async Task<List<CasePartyDocumentDto>> getCasePartyDocument(Guid CaseApplicationId, string lang)
    {
      return await _casePartyDocumentRepo.GetByCaseApplicationId(CaseApplicationId, lang);
    }
  }
}
