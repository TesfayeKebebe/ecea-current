using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Enum;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CasePartyInvestigationController : ControllerBase
    {
        private readonly CasePartyInvestigationRepository _invRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public CasePartyInvestigationController(CasePartyInvestigationRepository invRepo, UserManager<ApplicationUser> userManager)
        {
            _invRepo = invRepo;
            _userManager = userManager;
        }

        /// <summary>
        /// Add new case party investigation
        /// </summary>
        /// <param name="postedInvestigation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<bool> PostPartyInvestigation([FromBody] CasePartyInvestigationDTO postedInvestigation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedInvestigation.CreatedUserId = Guid.Parse(user.Id);
            return await _invRepo.Create(postedInvestigation);
        }

        /// <summary>
        /// Update party investigation
        /// </summary>
        /// <param name="postedInvestigation"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdatePartyInvestigation([FromBody] CasePartyInvestigationDTO postedInvestigation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedInvestigation.UpdatedUserId = Guid.Parse(user.Id);
            return await _invRepo.Update(postedInvestigation);
        }

        /// <summary>
        /// Get Party Investigation
        /// </summary>
        /// <param name="partyInvestigationId"></param>
        /// <returns></returns>
        [HttpGet("{partyInvestigationId}")]
        public async Task<CasePartyInvestigationDTO> GetPartyInvestigationById(int partyInvestigationId)
        {
            return await _invRepo.GetPartyInvestigationById(partyInvestigationId);
        }

        /// <summary>
        /// Get All Case parties for investigation
        /// </summary>
        /// <param name="caseInvestigationId"></param>
        /// <param name="partyCategory"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllByPartyCategory/{caseInvestigationId}/{partyCategory}")]
        public async Task<List<CasePartyInvestigationListModel>> GetAllByPartyCategory(Guid caseInvestigationId, CasePartyCategory partyCategory)
        {
            return await _invRepo.GetAllByPartyCategory(caseInvestigationId, partyCategory);
        }

        /// <summary>
        /// Get All Case party investigations
        /// </summary>
        /// <param name="casePartyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllPartyInvestigations/{casePartyId}")]
        public async Task<List<CasePartyInvestigationDTO>> GetAllPartyInvestigations(Guid casePartyId)
        {
            return await _invRepo.GetAllPartyInvestigations(casePartyId);
        }

    }
}
