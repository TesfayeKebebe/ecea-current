using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CaseTribunalFeedBackController : Controller
    {
        private readonly CaseTribunalFeedBackRepository _caseTribunalFeedBackRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _host;
        public CaseTribunalFeedBackController(CaseTribunalFeedBackRepository caseTribunalFeedBackRepository, IConfiguration configuration, IHostingEnvironment host, UserManager<ApplicationUser> userManager)
        {
            _configuration = configuration;
            _host = host;
            _caseTribunalFeedBackRepository = caseTribunalFeedBackRepository;
            _userManager = userManager;

        }
        /// <summary>
        /// create case tribunal feed back
        /// </summary>
        /// <param name="caseTribunalFeedBackDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CaseFeedBackCreate")]
        [Authorize]
        public async Task<CaseTribunalFeedBackDto> PostCaseTribunal([FromBody]  CaseTribunalFeedBackDto caseTribunalFeedBackDto)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            caseTribunalFeedBackDto.CreatedUserId = Guid.Parse(user?.Id);
            return await _caseTribunalFeedBackRepository.Create(caseTribunalFeedBackDto);
        }
        [HttpPut]
        [Route("CaseFeedBackEdit")]
        [Authorize]
        public async Task<CaseTribunalFeedBackDto> put([FromBody] CaseTribunalFeedBackDto caseTribunalFeedBackDto)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            caseTribunalFeedBackDto.UpdatedUserId = Guid.Parse(user.Id);
            return await _caseTribunalFeedBackRepository.Update(caseTribunalFeedBackDto);
        }
        [HttpGet]
        public async Task<List<CaseTribunalFeedBackDetail>> GetByCaseId(Guid CaseInvestigationId, string lang)
        {
            return await _caseTribunalFeedBackRepository.GetCaseTribunal(CaseInvestigationId, lang);
        }
    [HttpGet("JudgeFeedBack")]
    public async Task<List<CaseTribunalFeedBackDetail>> GetJudgeFeedbackByCaseId(Guid CaseInvestigationId, string lang)
    {
      return await _caseTribunalFeedBackRepository.GetCaseTribunalJudge(CaseInvestigationId, lang);
    }
    [HttpGet("CaseLookUpDropDown")]
        public async Task<List<CaseLookUpDropDown>> GetLookup(string lang)
        {
            return await _caseTribunalFeedBackRepository.GetCaseLookUp(lang);
        }

    }
}
