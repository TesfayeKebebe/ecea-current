﻿using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    public class LawDashboardController : Controller
    { 
        private readonly ECEADbContext _ctx;

        public LawDashboardController(ECEADbContext ctx)
        {
            _ctx = ctx;
        }
        [HttpGet("byCaseStatusType")] 
        public IActionResult ByCaseStatusType()
        {
            var appCase = _ctx.CaseApplication.Include(o => o.CaseStatusType).ToList();
            var groupedResult = appCase
                .GroupBy(r => r.CaseStatusType.CaseStatusTypeId)
                .ToList()
                .Select(grp => new
                {
                    CaseStatusTypeName = _ctx.CaseStatusType.Find(grp.Key).DescriptionAmh,
                    TotalCase = _ctx.CaseStatusType.Find(grp.Key).DescriptionAmh.Count()
                }).OrderByDescending(r => r.TotalCase)
                // .Take(n)
                .ToList();

            return Ok(groupedResult);
        }

        [HttpGet("caseApplication/byCaseType")] 
        public IActionResult ByCaseType()
        {
            var appCase = _ctx.CaseApplication.Include(o => o.CaseType).ToList();
            var groupedResult = appCase
                .GroupBy(r => r.CaseType.LookupId)
                .ToList()
                .Select(grp => new
                {
                    CaseTypeName = _ctx.Lookup.Find(grp.Key).DescriptionAmh,
                    TotalCase = _ctx.Lookup.Find(grp.Key).DescriptionAmh.Count()
                }).OrderByDescending(r => r.TotalCase)
                // .Take(n)
                .ToList();

            return Ok(groupedResult);
        }

        // GET api/caseApplicationpageNumber/pageSize
        [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult Get(int pageIndex, int pageSize)
        {
            var data = _ctx.CaseApplication.Include(o => o.CaseStatusType).OrderByDescending(c => c.ApplicationDate);
            var page = new PaginatedResponse<CaseApplication>(data, pageIndex, pageSize);

            var totalCount = data.Count();
            var totalPages = Math.Ceiling((double)totalCount / pageSize);

            var response = new
            {
                Page = page,
                TotalPages = totalPages
            };

            return Ok(response);
        }
    }
}