using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class LegalCaseController : ControllerBase
    {
        private readonly LegalCaseRepository _legalRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public LegalCaseController(LegalCaseRepository legalRepo, UserManager<ApplicationUser> userManager)
        {
            _legalRepo = legalRepo;
            _userManager = userManager;
        }

        /// <summary>
        /// Add new administrative tribunal file
        /// </summary>
        /// <param name="postedCaseFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<LegalCaseDTO> PostLegalCase([FromBody] LegalCaseDTO postedCaseFile)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseFile.CreatedUserId = Guid.Parse(user.Id);
            return await _legalRepo.Create(postedCaseFile);
        }

        /// <summary>
        /// Update administrative tribunal file
        /// </summary>
        /// <param name="postedCaseFile"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Update")]
        [Authorize]
        public async Task<bool> UpdateLegalCase([FromBody] LegalCaseDTO postedCaseFile)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseFile.UpdatedUserId = Guid.Parse(user.Id);
            return await _legalRepo.Update(postedCaseFile);
        }

        /// <summary>
        /// Get administrative tribunal file
        /// </summary>
        /// <param name="legalCaseId"></param>
        /// <returns></returns>
        [HttpGet("{legalCaseId}")]
        public async Task<LegalCaseDTO> GetLegalCase(Guid legalCaseId)
        {
            return await _legalRepo.GetLegalCaseById(legalCaseId);
        }
        [HttpGet]
        public async Task<PagedResult<LegalCaseListModel>> SearchLegalCase([FromQuery] LegalCaseSearchCriteriaModel searchQueryParameters)
        {
            return await _legalRepo.SearchLegalCase(searchQueryParameters);
        }

        /// <summary>
        /// Add new case appeal
        /// </summary>
        /// <param name="postedCaseFile"></param>
        /// <param name="parentCaseId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateAppeal/{parentCaseId}")]
        [Authorize]
        public async Task<LegalCaseDTO> PostCaseAppeal([FromBody] LegalCaseDTO postedCaseFile, Guid parentCaseId)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedCaseFile.CreatedUserId = Guid.Parse(user.Id);
            return await _legalRepo.CreateAppeal(postedCaseFile, parentCaseId);
        }
    }
}
