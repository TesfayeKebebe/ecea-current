using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class LegalCaseTrackingController : ControllerBase
    {
        private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".avi", ".mp4", ".mp3" };
        private readonly IHostingEnvironment _host;
        private readonly CaseDocumentRepository _documentRepo;
        private readonly IConfiguration _configuration;
        private readonly CaseTrackingRepository _trackingRepo;
        private readonly CaseAppointmentRepository _apptRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public LegalCaseTrackingController(CaseTrackingRepository trackingRepo, CaseAppointmentRepository apptRepo, IHostingEnvironment host, IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _trackingRepo = trackingRepo;
            _apptRepo = apptRepo;
            _host = host;
            _configuration = configuration;
            _userManager = userManager;
        }

        /// <summary>
        /// Add new case appointment
        /// </summary>
        /// <param name="postedAppointment"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Appointment")]
        [Authorize]
        public async Task<AppointmentDTO> PostAppointment([FromBody] AppointmentDTO postedAppointment)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);


            postedAppointment.CreatedUserId = Guid.Parse(user.Id);
            return await _apptRepo.Create(postedAppointment);
        }

        /// <summary>
        /// Update case appointment
        /// </summary>
        /// <param name="postedAppointment"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Appointment")]
        [Authorize]
        public async Task<AppointmentDTO> UpdateAppointment([FromBody] AppointmentDTO postedAppointment)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedAppointment.UpdatedUserId = Guid.Parse(user.Id);
            return await _apptRepo.Update(postedAppointment);
        }

        /// <summary>
        /// Get case appointment by id
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        [HttpGet("Appointment/{appointmentId}")]
        public async Task<AppointmentDTO> GetAppointment(Guid appointmentId)
        {
            return await _apptRepo.GetByAppointmentId(appointmentId);
        }

        /// <summary>
        /// Get all appointments on a case
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Appointment/{caseId}/{pageIndex}/{pageSize}")]
        public async Task<PagedResult<AppointmentDTO>> GetAllCaseAppointments([FromRoute] Guid caseId, [FromRoute] int pageIndex, [FromRoute] int pageSize)
        {
            return await _apptRepo.GetAllCaseAppointments(caseId, pageIndex, pageSize);
        }

        /// <summary>
        /// Get court decision by appointment
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        [HttpGet("CourtSession/{appointmentId}")]
        public async Task<CaseTrackingDTO> GetDecisionByAppointment(Guid appointmentId)
        {
            return await _trackingRepo.GetDecisionByAppointmentId(appointmentId);
        }
    [HttpGet("CourtSession/History/Appointement")]
    public async Task<CaseTrackingDTO> GetDecisionByHistoryAppointment(Guid appointmentId)
    {
      return await _trackingRepo.GetDecisionByHistoryAppointmentId(appointmentId);
    }
    [HttpGet("CourtSeession/Details/History")]
         public async Task<CaseTrackingDTO>  getCourtDecisionHistory(Guid ParantCaseId)
    {
      return await _trackingRepo.GetDecisionHistoryByCaseParantId(ParantCaseId);
    }
        /// <summary>
        /// Add New court hearing day activity
        /// </summary>
        /// <param name="postedCaseFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CourtSession")]
       [Authorize]
    public async Task<CaseDecisionTrackingDTO> NewCourtSession([FromBody] CaseDecisionTrackingDTO postedCaseFile)
        {
      var user = await _userManager.GetUserAsync(HttpContext.User);

      postedCaseFile.CreatedUserId = Guid.Parse(user.Id);
      return await _trackingRepo.NewCourtSessionDecision(postedCaseFile);
        }

        [HttpPut]
        [Route("CourtSession")]
        [Authorize]
        public async Task<CaseDecisionTrackingDTO> UpdateSession([FromBody] CaseDecisionTrackingDTO postedCaseFile)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedCaseFile.UpdatedUserId = Guid.Parse(user.Id);
      return await _trackingRepo.UpdateCourtSessionDecision(postedCaseFile);
        }
    [HttpPut]
    [Route("CourtSession/Details")]
    [Authorize]
    public async Task<CaseDecisionTrackingDTO> UpdateSessionDetails([FromBody] CaseDecisionTrackingDTO postedCaseFile)
    {
      var user = await _userManager.GetUserAsync(HttpContext.User);

      postedCaseFile.UpdatedUserId = Guid.Parse(user.Id);
      return await _trackingRepo.UpdateCourtSessionDecisionDetails(postedCaseFile);
    }
    /// <summary>
    /// Get all court sessions
    /// </summary>
    /// <param name="caseId"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    /// <returns></returns>
        [HttpGet]
        [Route("CourtSession/{caseId}/{pageIndex}/{pageSize}")]
        public async Task<PagedResult<CaseTrackingDTO>> GetAllCourSessions([FromRoute] Guid caseId, [FromRoute] int pageIndex, [FromRoute] int pageSize)
        {
            return await _trackingRepo.GetAllCourtSessions(caseId, pageIndex, pageSize);
        }
        /// <summary>
        /// Get all court sessions
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>RequestSizeLimit 
        [HttpGet]
    [Route("CourtSession/{caseId}/{pageIndex}/{pageSize}/History")]
    public async Task<PagedResult<CaseTrackingDTO>> GetAllCourSessionsHistorys([FromRoute] Guid caseId, [FromRoute] int pageIndex, [FromRoute] int pageSize)
    {
      return await _trackingRepo.GetAllCourtSessionsHistory(caseId, pageIndex, pageSize);
    }
    /// <summary>
    /// Get case tracking activity by id
    /// </summary>
    /// <param name="caseTrackingId"></param>
    /// <returns></returns>
    [HttpGet("{caseTrackingId}")]
        public async Task<CaseDecisionTrackingDTO> GetLegalCaseActivity(Guid caseTrackingId)
        {
            return await _trackingRepo.GetCaseTrackingById(caseTrackingId);
        }
        /// <summary>
        /// Search case tracking activity
        /// </summary>
        /// <param name="searchQueryParameters"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResult<CaseTrackingDTO>> SearchCaseTracking([FromQuery] CaseTrackingSearchCriteriaModel searchQueryParameters)
        {
            return await _trackingRepo.SearchCaseTracking(searchQueryParameters);
        }

        private async Task<string> SaveFileToDisc(CaseTrackingVideoDTO postedDocument)
        {
            var filesData = postedDocument.ActualFile;
            //Guid fileId = Guid.Empty;
            bool isValid = false;
            if (!(filesData == null ||
                filesData.Length == 0 ||
                //filesData.Length > 10 * 1024 * 1024 ||
                !ACCEPTED_FILE_TYPES.Any(s => s == Path.GetExtension(filesData.FileName).ToLower())
                )
                )
            {
                isValid = true;
            }


            string pathToSave = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploads");
           
            if (isValid)
            {
                if (!Directory.Exists(pathToSave))
                    Directory.CreateDirectory(pathToSave);
                var fileId = postedDocument.CaseDecisionTrackingId;
                var fileName = fileId.ToString() + Path.GetExtension(filesData.FileName);
                var filePath = Path.Combine(pathToSave, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await filesData.CopyToAsync(stream);
                    var fileUrl = _configuration["ApplicationUrl"] + "/uploads/" + fileName;
                    postedDocument.Url = fileUrl;
                }
            }

            return postedDocument.Url;
        }
        /// <summary>
        /// Add New court hearing day activity
        /// </summary>
        /// <param name="postedCaseFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CourtSession/Upload")]
        [DisableRequestSizeLimit]

        public async Task<string> CourtSessionUpload([FromForm] CaseTrackingVideoDTO postedCaseFile)
        {
            postedCaseFile.Url = "";
            var fileLoc = await SaveFileToDisc(postedCaseFile);
            postedCaseFile.Url = fileLoc;
            if (!string.IsNullOrEmpty(postedCaseFile.Url))
                await _trackingRepo.UpdateUrl(postedCaseFile);
            return fileLoc;
        }
        [HttpGet("CaseParty/Appointment")]
        public async Task<List<CasePartyAppointment>> GetCaseParty(Guid caseId, string lang)
        {
            return await _apptRepo.GetCaseParty(caseId, lang);
        }
        [HttpGet("Party/AppointmentPrint")]
        public async Task<LetterContent> GetAppointmentByParty(Guid casepartyId, Guid Cauid,int partyCategory)
        {
            return await _apptRepo.GetLastAppointed(casepartyId, Cauid, partyCategory);
        }
    }
}


