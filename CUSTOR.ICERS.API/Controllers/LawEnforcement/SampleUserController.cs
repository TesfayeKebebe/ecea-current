using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.LawEnforcement
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SampleUserController : ControllerBase
    {
        private readonly SampleUserRepository _userRepo;

        public SampleUserController(SampleUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        //[HttpGet]
        //[Route("GetAllUsers/{lang}")]
        //public async Task<IEnumerable<SampleUserDTO>> GetAllUsers(string lang)
        //{
        //    return await _userRepo.GetAllUsers(lang);
        //}
        [HttpGet]
        [Route("GetUsersByRole/{roleName}/{lang}")]
        public async Task<IEnumerable<SampleUserDTO>> GetUsersByRole(string roleName, string lang)
        {
            return await _userRepo.GetUsersByRole(roleName, lang);
        }
    }
}
