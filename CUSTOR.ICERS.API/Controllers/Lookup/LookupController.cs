﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.Security;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers.Lookup
{
    [Route("api/lookup")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class LookupController : ControllerBase
    {
        private readonly LookupRepository _lookupRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public LookupController(LookupRepository lookupRepo, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _lookupRepo = lookupRepo;
        }

        [HttpGet("{lang}/{lookupTypeId}")]
        public async Task<IEnumerable<LookupDTO>> GetLookups(string lang, int lookupTypeId)
        {
            return await _lookupRepo.GetLookups(lang, lookupTypeId);
        }
        [HttpDelete("delete/{id}")]
        public async Task<bool> DeleteLookupValue(int id)
        {
            return await _lookupRepo.DeleteLookUpValue(id);
        }
        [HttpDelete("deleteLookupType/{id}")]
        public async Task<bool> DeleteLookupType(int id)
        {
            return await _lookupRepo.DeleteLookUpType(id);
        }
        [HttpPost("SaveLookup")]
        public async Task<bool> SaveLookup([FromBody] LookupDTO4 lookup)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (lookup.LookupTypeId == 0)
            {
                lookup.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                lookup.UpdatedUserId = Guid.Parse(user.Id);
            }
            return await _lookupRepo.SaveLookup(lookup);
        }
        [HttpPost("SaveLookupType")]
        public async Task<bool> SaveLookupType([FromBody] LookupTypeDTO lookup)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (lookup.LookupTypeId == 0)
            {
                lookup.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                lookup.UpdatedUserId = Guid.Parse(user.Id);
            }

            return await _lookupRepo.SaveLookupType(lookup);
        }
        [HttpGet("{lookupTypeId}")]
        public async Task<List<LookupDTO4>> GetLookupsById(int lookupTypeId)
        {
            return await _lookupRepo.GetLookupsById(lookupTypeId);
        }
        [HttpGet("GetAllLookUpType")]
        public async Task<List<LookupTypeDTO>> GetAllLookUpType()
        {
            return await _lookupRepo.GetAllLookupTypeData();
        }
        //[HttpGet("GetLookupData/{lang}/{lookupTypeId}")]
        //public async Task<IEnumerable<LookupDTO>> GetLookupData(string lang, Guid lookupTypeId)
        //{
        //    return await _lookupRepo.GetLookupData(lang, lookupTypeId);
        //}

    }
}
