﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers.Lookup
{
    [Route("api/nationalities")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class NationalitiesController : ControllerBase
    {

        private readonly NationalitiesRepository _nationalitiesRepo;

        public NationalitiesController(NationalitiesRepository nationalitiesRepo)
        {
            _nationalitiesRepo = nationalitiesRepo;
        }


        [HttpGet("{lang}")]
        public async Task<IEnumerable<NationalityDTO>> GetNationality(string lang)
        {
            return await _nationalitiesRepo.GetNationality(lang);
        }
    }
}
