﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.WEBAPI.Controllers.Lookup
{
    [Route("api/prerequisite")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]

    public class PrerequisiteController : Controller
    {
        private readonly PrerequisiteRepository _prerequisiteRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public PrerequisiteController(PrerequisiteRepository prerequisiteRepo, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _prerequisiteRepo = prerequisiteRepo;
        }

        [HttpPost("SavePrerequisite")]
        public async Task<Prerequisite> SavePrerequisite([FromBody] Prerequisite postedPrerequisite)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedPrerequisite.CreatedUserId = Guid.Parse(user.Id);
            return await _prerequisiteRepo.SavePrerequisite(postedPrerequisite);
        }

        [HttpGet("GetAllPrerequisites")]
        public PagedResult<Prerequisite> GetAllPrerequisites([FromQuery] QueryParameters queryParameters)
        {
            return _prerequisiteRepo.GetAllPrerequisites(queryParameters);
        }

        [HttpGet("GetAllPrerequisiteByService")]
        public PagedResult<ServicePrerequisteDTO> GetAllPrerequisiteByService([FromQuery] ServicePrerequisiteQueryParameters queryParameters)
        {
            return _prerequisiteRepo.GetAllPrerequisiteByService(queryParameters);
        }

        [HttpGet("GetAllPrerequisitesNotAssighendToService")]
        public PagedResult<Prerequisite> GetAllPrerequisitesNotAssighendToService([FromQuery] ServicePrerequisiteQueryParameters queryParameters)
        {
            return _prerequisiteRepo.GetAllPrerequisitesNotAssighendToService(queryParameters);
        }

        [HttpGet("GetPrerequisite/{prerequisiteId}")]
        public async Task<Prerequisite> GetPrerequisite(int prerequisiteId)
        {
            return await _prerequisiteRepo.GetPrerequisite(prerequisiteId);
        }

        [HttpPut("UpdatePrerequisite")]
        public async Task<Prerequisite> UpdatePrerequisite([FromBody] Prerequisite postedPrerequisite)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedPrerequisite.UpdatedUserId   = Guid.Parse(user.Id);
            return await _prerequisiteRepo.UpdatePrerequisite(postedPrerequisite);
        }

        [HttpDelete("{PrerequisiteId}")]
        public async Task<bool> Delete(int PrerequisiteId)
        {
            return await _prerequisiteRepo.DeletePrerequisite(PrerequisiteId);
        }
    }
}
