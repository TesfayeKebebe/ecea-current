﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.Notification;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Notification
{
    [Route("api/notification")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class NotificationController : Controller
    {
        private readonly NotificationRepository _notificationRepository;

        public NotificationController(NotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }


        [HttpGet]
        public async Task<PagedResult<NotificationViewModel>> GetNotification([FromQuery] NotificationQueryParameters queryParameters)
        {
            return await _notificationRepository.GetNotifiation(queryParameters);
        }

        [HttpGet]
        [Route("getNotificationViolation")]
        public async Task<PagedResult<MemberTradeViolationView>> GetNotificationViolation([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetNotifiationTradeExecution(notificationQuery);
        }
        [HttpGet]
        [Route("getNotifiationTradeExecutionReport")]
        public async Task<PagedResult<MemberClientTradeViewModel>> GetNotifiationTradeExecutionReport([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetNotifiationTradeExecutionReport(notificationQuery);
        }
        [HttpGet]
        [Route("bankTransactionNotification")]
        public async Task<PagedResult<MemberTradeViolationView>> GetNotifiationBankTransaction([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetNotifiationBankTransaction(notificationQuery);
        }

        [HttpGet("getRecognitionExpirationNotification/{lang}")]
        public async Task<List<RecognitionExpirNotificationViewModel>> GetRecognitionExpirationNotification([FromRoute] string lang)
        {
            return await _notificationRepository.GetRecognitionExpirationNotification(lang);
        }
        [HttpGet]
        [Route("getInjunctionNotifications/{lang}/{userRoleId}")]
        public async Task<List<PendingInjunctionViewDTO>> GetInjunctionNotifications(string lang, int userRoleId)
        {
            return await _notificationRepository.GetInjunctionNotifications(lang, userRoleId);
        }

        [HttpGet]
        [Route("getTradeExecutionNotifiationReport")]
        public async Task<PagedResult<MemberClientTradeViewModel>> GetTradeExecutionNotifiationReport([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetTradeExecutionNotifiationReport(notificationQuery);
        }
        [HttpGet]
        [Route("getFinanceNotifiationReport")]
        public async Task<PagedResult<MemberClientTradeViewModel>> GetFinanceNotifiationReport([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetFinanceNotifiationReport(notificationQuery);
        }
        [HttpGet]
        [Route("getClientInformationNotifiationReport")]
        public async Task<PagedResult<MemberClientTradeViewModel>> GetClientInformationNotifiationReport([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetClientInformationNotifiationReport(notificationQuery);
        }
        //[HttpGet]
        //[Route("expiringInjunctions")]
        //public async Task<PagedResult<PendingInjunctionViewDTO>> GetInjunctionsTobeLifted([FromQuery] InjunctionQueryParameters QueryParameters)
        //{
        //    return await _notificationRepository.GetInjunctionsTobeLifted(QueryParameters);
        //}
        [HttpGet]
        [Route("getAnnualAuditNotifiationReport")]
        public async Task<PagedResult<MemberFinancialAuditorNotificationView>> GetAnnualAuditNotifiationReport([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetAnnualAuditNotifiationReport(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteNotifiation")]
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteNotifiation([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetOffSiteNotifiation(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteFinancialNotifiation")]
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteFinancialNotifiation([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetOffSiteFinancialNotifiation(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteTradeNotifiation")]
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteTradeNotifiation([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetOffSiteTradeNotifiation(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteClientNotifiation")]
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteClientNotifiation([FromQuery] NotificationQueryParameters notificationQuery)
        {
            return await _notificationRepository.GetOffSiteClientNotifiation(notificationQuery);
        }
        [HttpGet]
        [Route("expiringInjunctions")]
        public async Task<PagedResult<PendingInjunctionViewDTO>> GetInjunctionsTobeLifted([FromQuery] InjunctionQueryParameters QueryParameters)
        {
            return await _notificationRepository.GetInjunctionsTobeLifted(QueryParameters);
        }
        [HttpGet]
        [Route("getNotifiationViolationList")]
        public async Task<PagedResult<MemberTradeViolationView>> GetNotifiationViolationList([FromQuery] NotificationQueryParameters QueryParameters)
        {
            return await _notificationRepository.GetNotifiationViolationList(QueryParameters);
        }
    }
}
