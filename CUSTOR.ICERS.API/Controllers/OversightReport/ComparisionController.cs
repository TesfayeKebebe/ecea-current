﻿using CUSTOR.API.ExceptionFilter;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReport;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;

namespace CUSTOR.ICERS.API.Controllers.OversightReport
{
    [Route("api/comparision")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ComparisionController : Controller
    {
        private readonly ComparisionRepository _comparisionRepo;
        public ComparisionController(ComparisionRepository _repository)
        {
            _comparisionRepo = _repository;
        }

        [HttpGet]
        [Route("monthly/{FirstYear}/{FirstMonth}/{SecondYear}/{SecondMonth}/{type}/{lang}")]
        public async Task<List<ComparisonData>> GetMonthlyComparisionData(int FirstYear, int FirstMonth, int SecondYear, int SecondMonth, int type, string lang)
        {
            return await _comparisionRepo.GetMonthlyComparisionData(FirstYear, FirstMonth, SecondYear, SecondMonth,type, lang);
        }
        [HttpGet]
        [Route("quarterly/{FirstYear}/{Quarter1}/{SecondYear}/{Quarter2}/{Type}/{Language}")]
        public async Task<List<ComparisonData>> GetQuarterlyComparisionData(int FirstYear, int Quarter1, int SecondYear, int Quarter2, int Type, string Language)
        {
            return await _comparisionRepo.GetQuarterlyComparisionData(FirstYear, Quarter1, SecondYear, Quarter2, Type, Language);
        }

        [HttpGet]
        [Route("semister/{FirstYear}/{Semister1}/{SecondYear}/{Semister2}/{Type}")]
        public async Task<List<ComparisonData>> GetSemisterComparisionData(int FirstYear, int Semister1, int SecondYear, int Semister2,int Type)
        {
            return await _comparisionRepo.GetSemisterComparisionData(FirstYear, Semister1, SecondYear, Semister2, Type);
        }
        [HttpGet]
        [Route("nineMonth/{FirstYear}/{order1}/{SecondYear}/{Order2}/{Type}")]
        public async Task<List<ComparisonData>> GetNineMonthComparisionData(int FirstYear, int order1, int SecondYear, int Order2,int Type)
        {
            return await _comparisionRepo.GetNineMonthComparisionData(FirstYear, order1, SecondYear, Order2,Type);
        }

        [HttpGet]
        [Route("annual/{FirstYear}/{SecondYear}/{type}/{lang}")]
        public async Task<List<ComparisonData>> GetAnnualComparisionData(int FirstYear, int SecondYear,int type, string lang)
        {
            return await _comparisionRepo.GetAnnualComparisionData(FirstYear, SecondYear,type, lang);
        }
        [HttpGet]
        [Route("bycheckList/{FirstYear}/{SecondYear}/{Type}/{Language}")]
        public async Task<List<OversightReportMOnthlySummaryModel>> GetCheckListComparisionData(int FirstYear, int SecondYear, int Type, string Language)
        {
            return await _comparisionRepo.GetCheckListComparisionData(FirstYear, SecondYear, Type, Language);
        }
    }
}
