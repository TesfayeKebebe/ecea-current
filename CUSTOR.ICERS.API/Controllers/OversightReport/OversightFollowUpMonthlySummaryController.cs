﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReport;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.OversightReport
{
    [Authorize]
    [Route("api/OversightFollowUpMonthlySummaryController")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OversightFollowUpMonthlySummaryController : ControllerBase
    {
        private readonly OversightMonthlySummaryrepo _summaryRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public OversightFollowUpMonthlySummaryController(OversightMonthlySummaryrepo _oversightSummuryRepo, UserManager<ApplicationUser> userManager)
        {
            _summaryRepo = _oversightSummuryRepo;
            _userManager = userManager;

        }

        [HttpPost]
        [Route("submitMonthlySummary")]
        public async Task<int> SummerizeReports(MonthlyOnsiteFollowupSummary PostedData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            PostedData.CreatedUserId = Guid.Parse(user.Id);
            return await _summaryRepo.PostMonthlyReport(PostedData);
        }
        [HttpPatch]
        [Route("SummitSummaryForleader")]
        public async Task<bool> sendForAppproval([FromBody] SendForApprovalModel  UpdatedData)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);

            return await _summaryRepo.SendForAppproval(UpdatedData);
        }

        [HttpPatch]
        [Route("approveReport")]
        public async Task<bool> ApproveSummaryReport([FromBody] SendForApprovalModel UpdatedData)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);

            return await _summaryRepo.ApproveMOnthlySummary(UpdatedData);
        }

        [HttpPatch]
        [Route("decide")]
        public async Task<bool> MakeDecision(ApprovalModel decision)
        {
            return await _summaryRepo.MakeDecisionOnSingleFollowUp(decision);
        }


        [HttpGet]
        [Route("checkIfexists/{year}/{month}/{criteria}")]
        public async Task<MonthlyOnsiteFollowupSummary> CheckIfExists(int year, int month, int criteria)
        {
            return await _summaryRepo.CheckMonthlyReportByCriteria(year, month, criteria);
        }
        [HttpGet]
        [Route("CompareExecution/{FirstCriteria}/{secondCriteria}/{unit}")]
        public async Task<List<ComparisonData>> CompareExecution( int FirstCriteria, int secondCriteria,string unit)
        {
            return await _summaryRepo.CompareExecution(FirstCriteria, secondCriteria,  unit); 
        }

        [HttpGet]
        [Route("getMonthlyReport")]
        public async Task<PagedResult<FollowupSummaryViewModel>> GetMonthlyReport([FromQuery] FilterCriteria  filterCriteria)
        {
            return await _summaryRepo.GetMonthlyReport(filterCriteria); 
        }
        [HttpGet]
        [Route("getlastMonthReportstatus/{Year}/{Month}/{Type}")]
        public Task<List<MonthlySummaryModel>> GetlastMonthReportstatus(int Year, int Month,int Type) {
            return  _summaryRepo.GetlastMonthReportstatus(Year, Month, Type);
        }

    }
}
