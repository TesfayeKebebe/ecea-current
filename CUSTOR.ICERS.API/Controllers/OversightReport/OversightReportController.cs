﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReportRepo;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.OversightReport
{
    [Authorize]
    [Route("api/OversightReport")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OversightReportController : Controller
    {
        private readonly OversightReportRepo _OversightReportRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public OversightReportController(OversightReportRepo _oversightReportRepo, UserManager<ApplicationUser> userManager)
        {
            _OversightReportRepo = _oversightReportRepo;
            _userManager = userManager;
        }

        [HttpGet]
        [Route("all/{lang}")]
        public async Task<List<ReportSearchResult>> Reports(string lang)
        {
            return await _OversightReportRepo.ReportLsit(lang);
        }
        [HttpGet]
        [Route("getIcompleteReportDetails/{id}")]
        public async Task<OversightFollowUpReport> GetIcompleteReportDetails([FromRoute] int id)
        {
            return await _OversightReportRepo.GetIcompleteReportDetails(id);
        }

        [HttpGet]
        [Route("{lang}/{id}")]
        public async Task<OversightReportMasterDataViewModel> ReportDetails([FromRoute] string lang, int id)
        {
            return await _OversightReportRepo.GetReportDetails(lang, id);
        }
        [HttpGet]
        [Route("getReportDetailsPercriteria/{id}/{lang}")]
        public async Task<List<ReportDetaitPerCriteriaDTO>> GetReportDetailsPercriteria([FromRoute] int id, string lang)
        {
            return await _OversightReportRepo.GetReportDetailsPerCritera(id, lang);
        }

        [HttpGet]
        [Route("GetMonthlySumary/{lang}/{from}/{to}/{type}")]
        public async Task<PagedResult<OversightReportMOnthlySummaryModel>> GetMonthlySumary([FromRoute] string lang, DateTime from,
            DateTime to, int type, [FromQuery] SearchScheduleModel filter)
        {
            return await _OversightReportRepo.GetMonthlySummary(lang, from, to, type,filter);
        }

        [HttpGet]
        [Route("countEvaluations/{followupId}")]
        public async Task<int> CountCompletedEvaluations(int followupId)
        {
            return await _OversightReportRepo.GetExecutedEvaluationsCount(followupId);
        }
        /// <summary>
        /// Get Dashboard data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("dashboardData")]
        public async Task<OversightReportDashboardDataViewModel> GetDashboardData()
        {
            return await _OversightReportRepo.GetDashboardData();
        }

        /// <summary>
        /// get current status of selected followupId
        /// </summary>
        /// <param name="followupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("CurentStatus/{followupId}")]
        public async Task<bool?> CurentStatus(int followupId)
        {

            return await _OversightReportRepo.GetCurrentStatus(followupId);
        }
        [HttpGet]
        [Route("getEvaluations/{followupId}")]
        public async Task<OversightReportDetail> GetEvaluations(int followupId)
        {
            return await _OversightReportRepo.GetDetailsByFollowUpId(followupId);
        }

        [HttpGet]
        [Route("incomplete/{lang}/{type}")]
        public async Task<PagedResult<OversightReportViewModel>> GetIncompleteEvaluations(string lang, int type,
            [FromQuery] SearchScheduleModel filter)
        {
            return await _OversightReportRepo.GetIncompleteEvaluations(lang, type, filter);
        }

        /// <summary>
        /// calculate total weight using followup
        /// </summary>
        /// <param name="followupId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("TotalWeight/{followupId}")]
        public async Task<float> GetTotalWeight(int followupId)
        {
            return await _OversightReportRepo.GetTotalweight(followupId);
        }
        /// <summary>
        /// Search oversight report using Date of inspection and selected exchange actor during adding new report
        /// </summary>
        /// <param name="Criteria"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("search")]
        public async Task<OversightReportViewModel> Searchreport([FromBody] OversightReportSearchCriteria Criteria)
        {
            return await _OversightReportRepo.SearchReports(Criteria);
        }


        /// <summary>
        /// Search oversight followup reports by Date range, Decisions
        /// </summary>
        /// <param name="Criteria"></param>
        /// <returns>
        /// General reports
        /// </returns>

        [HttpPost]
        [Route("generalSearch")]
        public async Task<PagedResult<OversightReportMasterData>> Searchreport([FromBody] OvsersightReportGeneralSearchCriteria Criteria)
        {
            return await _OversightReportRepo.GeneralSearchReports(Criteria);
        }
        /// <summary>
        /// Add new oversight
        /// </summary>
        /// <param name="masterdata"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public async Task<ReportStatusResult> SaveOvesightReportData([FromBody] OversightReportDTO masterdata)
        {
            return await _OversightReportRepo.SaveOversightReport(masterdata);
        }

        [HttpGet]
        [Route("getLastRecord/{exchangeActorId}/{lang}")]
        public async Task<OversightReportDecisionRecord> GetLastRecord(Guid exchangeActorId, string lang)
        {
            return await _OversightReportRepo.GetLastEvaluationDecision(exchangeActorId, lang);
        }
        /// <summary>
        /// complete oversight report
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>

        [HttpPut]
        [Route("markAsComplete")]
        public async Task<bool> MarkAsComplete([FromBody] CompletedoversitReportData data)
        {
            return await _OversightReportRepo.MarkReportComplete(data);
        }

        [HttpPut]
        [Route("editFollowupBycheckList")]
        public async Task<bool> EditFollowupBycheckList([FromBody] OversightReportDetail data)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            data.UpdatedUserId = Guid.Parse(user.Id);
            return await _OversightReportRepo.EditFollowupBycheckList(data);
        }

        [HttpPut]
        [Route("editFollowup")]
        public async Task<bool> EditFollowup([FromBody] OversightFollowUpReport data)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            data.UpdatedUserId = Guid.Parse(user.Id);
            return await _OversightReportRepo.EditFollowup(data);
        }

        [HttpGet]
        [Route("getFollowupFinalDecisionDetails/{FollowupId}")]
        public async Task<OversiteFollowUpFinalDecisionModel> GetFinalDecisionOfOversiteFollowup(int FollowupId)
        {
            return await _OversightReportRepo.GetFinalDecisionOfOversiteFollowup(FollowupId);
        }

        [HttpPost]
        [Route("markAsOfficeClosed")]
        public async Task<int> ReportAsOfficeIsClosed([FromBody] OfficeClosedData data)
        {
            return await _OversightReportRepo.ReportAsOfficeIsClosed(data);
        }



        /// <summary>
        /// Get report Detail of specific criteria from detail table
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("checkDetail/")]
        public async Task<List<OversightReportDetail>> CheckDetails([FromBody] OversightDetailReportSearchCriteria criteria)
        {
            return await _OversightReportRepo.CheckDetailReport(criteria);
        }

        [HttpGet]
        [Route("getEvaluationsCountByDate")]
        public async Task<PagedResult<EvaluationsByDate>> EvaluationsCountByDate([FromQuery] PaginateModel model)
        {
            return await _OversightReportRepo.GetEvaluationsCountByDate(model);
        }
        [HttpGet]
        [Route("getEvaluationDetails/{id}/{lang}")]
        public async Task<List<OversightReportDetailViewModel>> GetEvaluationDetails(int id,string lang)
        {
            return await _OversightReportRepo.GetEvaluationDetails(id, lang);
        }

    }
}