﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReportRepo;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers
{
    [Authorize]
    [Route("api/OversightReportCriteriaController")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OversightReportCriteriaController : Controller
    {

        private readonly OversightReportRepo _OversightReportRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public OversightReportCriteriaController(OversightReportRepo _oversightReportRepo, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _OversightReportRepo = _oversightReportRepo;
        }

        [HttpGet("{exchangeActortype}/{type}/{lang}")]
        public async Task<List<Oversight_report_criteria_viewModel>> GetOversightReportCriteria(int exchangeActortype, int type, string lang)
        {
            return await _OversightReportRepo.GetCriteria(exchangeActortype, type,lang);
        }
        [HttpGet("type/{type}/{lang}")]
        public async Task<List<Oversight_report_criteria_viewModel>> GetOversightReportCriteria(int type, string lang)
        {
            return await _OversightReportRepo.GetCriteriaByType( type, lang);
        }


        [HttpGet]
        public async Task<List<Oversight_report_criteriaDTO>> GetallOversightReportCriteria()
        {
            return await _OversightReportRepo.GetallCriteria();
        }
        [HttpPost]
        public async Task<int> SaveCriteria(Oversight_report_criteria PostedData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            PostedData.CreatedUserId = Guid.Parse(user.Id);
            return await _OversightReportRepo.PostOversightReportCriteria(PostedData);
        }
        [HttpPost("{UpdateCriteria}")]
        public async Task<bool> UpdateCriteria(Oversight_report_criteria PostedData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            PostedData.UpdatedUserId = Guid.Parse(user.Id);

            return await _OversightReportRepo.UpdateCriteriaAsync(PostedData);
        }
        [HttpDelete("{id}")]
        public async Task<bool> DeleteCriteria(int id)
        {
            return await _OversightReportRepo.Deletecriteria(id);
        }


    }
}
