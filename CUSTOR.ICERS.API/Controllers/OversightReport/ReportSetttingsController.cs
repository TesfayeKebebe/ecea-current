﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReport;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.OversightReport
{
    [Route("api/OversightReportSetting")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ReportSetttingsController : Controller
    {
        private readonly OversightReportSettingRepository osrSettingRepo;
        public ReportSetttingsController(OversightReportSettingRepository osRepo)
        {
            osrSettingRepo = osRepo;
        }
        [HttpGet]
        public async Task<List<OversightReportSettingDTO>> GetDecisionTypes()
        {
            return await osrSettingRepo.GetDecisonTypes();
        }

        [HttpPost]

        public async Task<bool> SaveDecisionType(OversightReportSetting data)
        {
            return await osrSettingRepo.AddDecisonType(data);
        }

    }
}