﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReport;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.OversightReport
{
    [Route("api/schedule")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ScheduleController : Controller
    {

        private readonly ScheduleRepository _scheduleRepository;
        public ScheduleController(ScheduleRepository _oversightScheduleRepo)
        {
            _scheduleRepository = _oversightScheduleRepo;
        }

        [HttpPost]
        [Route("addSchedule")]
        public async Task<int> SaveSchedule([FromBody] OversightSchedule[] schedule)
        {
            return await _scheduleRepository.UploadSchedule(schedule);

        }
        [HttpPost]
        [Route("updateSchedule")]
        public async Task<int> UpdateSchedule([FromBody] OversightSchedule[] schedule)
        {
            return await _scheduleRepository.UpdateSchedule(schedule);

        }
        [HttpPut]
        [Route("UpdateSingleSchedule")]
        public async Task<bool> UpdateSingleSchedule([FromBody] OversightSchedule schedule)
        {
            return await _scheduleRepository.UpdateSingleSchedule(schedule);

        }
        [HttpGet]
        [Route("getSchedule")]
        public async Task<PagedResult<OversightSchedule>> GetSchedule([FromQuery] PaginateModel _paginateModel)
        {
            return await _scheduleRepository.GetSchedule(_paginateModel);
        }
        [HttpDelete]
        [Route("DeleteSchedule/{id}")]
        public async Task<bool> DeleteScheduleById(int id)
        {
            return await _scheduleRepository.DeleteSchedule(id);
        }

        [HttpGet]
        [Route("getCurrentMonthSchedule")]
        public async Task<PagedResult<OversightSchedule>> getCurrentMonthSchedule([FromQuery] SearchScheduleModel filter )
        {
            return await _scheduleRepository.GetCurrentMonthSchedule(filter); 
        }

        [HttpGet]
        [Route("SearchSchedule")]
        public async Task<PagedResult<OversightSchedule>> GetScheduleByMonth([FromQuery] SearchScheduleModel _scheduleModel)
        {
            return await _scheduleRepository.GetScheduleByMonth(_scheduleModel);
        }

        [HttpGet]
        [Route("CountScheduleByMonths")]
        public async Task<List<SearchScheduleByMonthsModel>> GetScheduleCountByMonth([FromQuery] SearchScheduleModel _scheduleModel)
        {
            return await _scheduleRepository.GetScheduleCountByMonth(_scheduleModel);
        }

        [HttpGet]
        [Route("getMonthlySchedule")]
        public async Task<PagedResult<SearchScheduleByMonthsModel>> GetScheduleByMonthly([FromQuery] SearchScheduleModel _scheduleModel)
        { 
            return await _scheduleRepository.GetScheduleByMonthly(_scheduleModel);
        }
        
    }
}