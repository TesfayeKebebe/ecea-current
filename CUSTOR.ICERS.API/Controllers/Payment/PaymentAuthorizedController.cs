﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Payment
{
    [Route("api/paymentauthorize")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class PaymentAuthorizedController : Controller
    {
        private readonly PaymentAuthorizedRepository _paymetnAuthorizedRepo;

        public PaymentAuthorizedController(PaymentAuthorizedRepository paymentAuthorizedRepository)
        {
            _paymetnAuthorizedRepo = paymentAuthorizedRepository;
        }

        [HttpGet("{serviceApplicationId}")]
        [Authorize]
        public async Task<IEnumerable<PaymentAuthorizationStatusDTO>> GetPaymentAuthorizationStatus(Guid serviceApplicationId)
        {
            return await _paymetnAuthorizedRepo.GetPaymentAuthorizationStatus(serviceApplicationId);
        }

        [HttpGet]
        [Authorize]
        public async Task<PagedResult<AuthorizedPaymentOrderSearchDTO>> SearchAuthorizedPaymentOrder([FromQuery] PaymentQueryParameters customerQueryParameters)
        {
            return await _paymetnAuthorizedRepo.SearchAuthorizedPaymentOrder(customerQueryParameters);
        }

        [HttpPost]
        [Authorize]
        public async Task<bool> PostPaymentAuthorization([FromBody] ServiceApplicationDTO postedPaymentAuthorization)
        {
            return await _paymetnAuthorizedRepo.PostPaymentAuthorize(postedPaymentAuthorization);
        }

    }
}
