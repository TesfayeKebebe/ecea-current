﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Payment
{
    [Route("api/paymentorder")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class PaymentOrderController : Controller
    {
        private readonly PaymentOrderRepository _paymentOrderRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public PaymentOrderController(PaymentOrderRepository paymentOrderRepository, UserManager<ApplicationUser> userManager)
        {
            _paymentOrderRepository = paymentOrderRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<Guid> PostPaymentOrder([FromBody] PaymentOrderDTO postedPaymentOrder)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedPaymentOrder.UpdatedUserId = Guid.Parse(user?.Id);

            return await _paymentOrderRepository.PostPaymentOrder(postedPaymentOrder);
        }
    }
}