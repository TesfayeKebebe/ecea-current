﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Payment
{
    [Route("api/receipt")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ReceiptController : Controller
    {
        private readonly ReceiptRepository _receiptRepository;

        public ReceiptController(ReceiptRepository receiptRepository)
        {
            _receiptRepository = receiptRepository;
        }

        [HttpPost]
        public async Task<int> PostReceiptData(ReceiptDTO postedReceipt)
        {
            return await _receiptRepository.PostReceiptData(postedReceipt);
        }

        //[HttpGet("")]
        //public async Task<List<ReceiptDataDTO>> GetReceiptData()
        //{
        //    return await _receiptRepository.GetReceipt();
        //}
    }
}
