﻿
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers
{
    [Route("api/cancellation")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CancellationController : Controller
    {

        private readonly CancellationRepository _cancellationRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public CancellationController(CancellationRepository cancellationRepository, UserManager<ApplicationUser> userManager)
        {
            _cancellationRepository = cancellationRepository;
            _userManager = userManager;

        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostCancellationGeneralInfo([FromBody] CancellationDTO postedCancellation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedCancellation.CreatedUserId = Guid.Parse(user.Id);

            return await _cancellationRepository.PostCancellationGeneralInfo(postedCancellation);
        }

        [HttpPost("finalcancellation")]
        [Authorize]
        public async Task<bool> PostFinalCancellation([FromBody] CancellationDTO finalCancellation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            finalCancellation.UpdatedUserId = Guid.Parse(user.Id);

            return await _cancellationRepository.PostFinalCancellation(finalCancellation);
        }

        [HttpGet("{lang}/{serviceApplicationId}")]
        public async Task<CancellationDTO> GetCancellationDataByServiceApp(string lang, Guid serviceApplicationId)
        {
            return await _cancellationRepository.GetCancellationDataWithServiceApp(lang, serviceApplicationId);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateCancellation([FromBody] CancellationDTO updatedCancellationData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedCancellationData.UpdatedUserId = Guid.Parse(user.Id);

            return await _cancellationRepository.UpdateCancellation(updatedCancellationData);

        }

        [HttpGet("{exchangeActorId}")]
        public async Task<ServiceStatusDTO> GetCancellationStatus([FromRoute] Guid exchangeActorId)
        {
            return await _cancellationRepository.GetExchangeActorCancellationStatus(exchangeActorId);
        }
    }
}
