﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Recognition
{
    [Route("api/injunction")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class InjunctionController : Controller
    {
        private readonly InjunctionRepository _injunctionRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public InjunctionController(InjunctionRepository injunctionRepo, UserManager<ApplicationUser> userManager)
        {
            _injunctionRepo = injunctionRepo;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> SaveInjunction([FromBody] InjunctionInfoDTO postedInjunction)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedInjunction.CreatedUserId = Guid.Parse(user?.Id);
            return await _injunctionRepo.PostInjunction(postedInjunction);
        }

        //[HttpPost]
        //public async Task<int> SaveInjunction(InjunctionInfoDTOForDataEntry postedInjunction)
        //{
        //    return await _injunctionRepo.PostInjunctionfordataEntery(postedInjunction);
        //}
        [HttpGet]
        [Route("currentStatus/{customer_id}")]
        public async Task<int> GetCustomerStatus(Guid customer_id)
        {
            return await _injunctionRepo.GetCurrentStatus(customer_id);
        }
        [HttpGet]
        [Route("getActiveInjunctions")]
        [Authorize]
        public async Task<PagedResult<InjunctionViewDTO>> GetInjunction([FromQuery] InjunctionQueryParameters QueryParameters)
        {
            return await _injunctionRepo.GetInjunctions(QueryParameters);
        }

        [HttpGet]
        [Route("getAllCancellation")]
        [Authorize]
        public async Task<PagedResult<CancellationViewDTO>> GetCancellation([FromQuery] InjunctionQueryParameters QueryParameters)
        {
            return await _injunctionRepo.GetCancellations(QueryParameters);
        }

        [HttpGet]
        [Route("getLifted")]
        [Authorize]
        public async Task<PagedResult<InjunctionViewDTO>> GetLiftedInjunctions([FromQuery] InjunctionQueryParameters QueryParameters)
        {
            return await _injunctionRepo.GetLiftedInjunctions(QueryParameters);
        }

        [HttpGet]
        [Route("getPendingInjunctions")]
        [Authorize]
        public async Task<PagedResult<PendingInjunctionViewDTO>> PendingInjunctions([FromQuery] InjunctionQueryParameters QueryParameters)
        {
            return await _injunctionRepo.GetPendingInjunctions(QueryParameters);
        }

        [HttpGet("{lang}/{id}")]
        public async Task<InjunctionViewDTO> GetInjunctionById(string lang, int id)
        {
            return await _injunctionRepo.GetInjunctionById(lang, id);
        }

        [HttpGet("print/{lang}/{id}")]
        public async Task<LetterContent> GetInjunctionPrintById(string lang, int id)
        {
            return await _injunctionRepo.GetInjunctionPrintById(lang, id);
        }

        [HttpGet("print/lifting/{lang}/{id}")]
        public async Task<LetterContent> GetLiftingInjunctionPrintById(string lang, int id)
        {
            return await _injunctionRepo.GetLiftingInjunctionPrintById(lang, id);
        }

        [HttpGet("print/cancelled/{lang}/{id}")]
        public async Task<LetterContent> GetCancelledPrintById(string lang, int id)
        {
            return await _injunctionRepo.GetCancelledPrintById(lang, id);
        }

        [HttpGet("getExpiredRenewals/{lang}")]
        public async Task<List<ExchangeActor>> GetExpiredRenewals(string lang)
        {
            return await _injunctionRepo.GetExpiredRenewals(lang);
        }

        [HttpGet]
        [Route("history/{customer_id}/{lang}")]
        public async Task<List<InjunctionViewDTO>> GetInjunctionHistoryByCustomer([FromRoute] Guid customer_id, string lang)
        {
            return await _injunctionRepo.GetInjunctionHistory(customer_id, lang);
        }

        [HttpPut("update")]
        public async Task<bool> UpdateInjunction(InjunctionInfoDTO updatedInfo)
        {
            return await _injunctionRepo.UpdateInjunctionInfo(updatedInfo);
        }

        [HttpPut]
        [Route("lift")]
        public async Task<bool> LiftInjunction(InjunctionLiftDTO injunctionLiftInfo)
        {
            return await _injunctionRepo.LiftInjunction(injunctionLiftInfo);
        }

        [HttpDelete("{id}")]
        public async Task<bool> DeleteInjunction(int id)
        {
            return await _injunctionRepo.DeleteInjunction(id);
        }
        [HttpPut("approve")]
        public async Task<bool> Approve(InjunctionApprovalDTO approvalData)
        {
            return await _injunctionRepo.ApproveOrDeclineInjunction(approvalData);
        }
        [HttpGet("expiringInjunctions")]
        public async Task<PagedResult<InjunctionViewDTO>> GetExpiringInjunctions([FromQuery] InjunctionQueryParameters parameters)
        {
            return await _injunctionRepo.GetExpiringInjunctions(parameters);
        }
    }
}
