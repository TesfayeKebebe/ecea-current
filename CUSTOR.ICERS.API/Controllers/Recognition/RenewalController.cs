﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Recognition
{
    [Route("api/renewal")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class RenewalController : Controller
    {
        private readonly RenewalRepository _renewalRepository;
        private readonly UserManager<ApplicationUser> _userManager;


        public RenewalController(RenewalRepository renewalRepository, UserManager<ApplicationUser> userManager)
        {
            _renewalRepository = renewalRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostRenewalGeneralInfo([FromBody]  RenewalDTO postedRenewal)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedRenewal.CreatedUserId = Guid.Parse(user.Id);

            return await _renewalRepository.PostRenewalGeneralInfo(postedRenewal);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateRenewal([FromBody] RenewalDTO updatedRenewalData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedRenewalData.UpdatedUserId = Guid.Parse(user.Id);

            return await _renewalRepository.UpdateRenewal(updatedRenewalData);

        }

        [HttpGet("{lang}/{serviceApplicationId}")]
        public async Task<RenewalDTO> GetRenewalDataByServiceAppId(string lang, Guid serviceApplicationId)
        {
            return await _renewalRepository.GetRenewalDataByServiceAppId(lang, serviceApplicationId);
        }
    }
}
