﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers.Recognition
{
    [Route("api/replacement")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ReplacementController : Controller
    {

        private readonly ReplacementRepository _replacementRepository;
        private readonly UserManager<ApplicationUser> _userManager;


        public ReplacementController(ReplacementRepository replacementRepository, UserManager<ApplicationUser> userManager)
        {
            _replacementRepository = replacementRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostReplacementGeneralInfo([FromBody] ReplacementDTO postedReplacement)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedReplacement.CreatedUserId = Guid.Parse(user.Id);

            return await _replacementRepository.PostReplacementGeneralInfo(postedReplacement);
        }

        [HttpGet("{lang}/{serviceApplicationId}")]
        public async Task<ReplacementDTO> GetReplacementDataByServiceAppId(string lang, Guid serviceApplicationId)
        {
            return await _replacementRepository.GetReplacementDataByServiceAppId(lang, serviceApplicationId);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateReplacment([FromBody] ReplacementDTO updatedReplacmentData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedReplacmentData.UpdatedUserId = Guid.Parse(user.Id);

            return await _replacementRepository.UpdateReplacment(updatedReplacmentData);

        }

    }
}
