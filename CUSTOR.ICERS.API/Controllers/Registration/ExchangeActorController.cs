﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.DataAccessLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers
{
    [Route("api/exchangeactor")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ExchangeActorController : Controller
    {
        private readonly ExchangeActorRepository _customerRepo;
        private readonly ReprsentativeTypeRepository _representativeTypeRepository;
        private readonly MemberProductRepository _memberProductRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public ExchangeActorController(ExchangeActorRepository customerRepo, ReprsentativeTypeRepository reprsentativeTypeRepository, MemberProductRepository memberProductRepository, UserManager<ApplicationUser> userManager)
        {
            _customerRepo = customerRepo;
            _representativeTypeRepository = reprsentativeTypeRepository;
            _memberProductRepository = memberProductRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostExchnageActor([FromBody] ExchangeActorGeneralInfoDTO postedCustomer)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedCustomer.CreatedUserId = Guid.Parse(user?.Id);

            return await _customerRepo.PostExchnageActor(postedCustomer);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateCustomer([FromBody] ExchangeActorGeneralInfoDTO updatedCustomer)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedCustomer.UpdatedUserId = Guid.Parse(user?.Id);

            await _memberProductRepository.DeleteMemberProduct(updatedCustomer.ExchangeActorId);
            await _memberProductRepository.DeleteCustomerBussinessField(updatedCustomer.ExchangeActorId);

            await _representativeTypeRepository.DeleteMemeberRepresentativeType(updatedCustomer.ExchangeActorId);

            return await _customerRepo.UpdateCustomer(updatedCustomer);
        }
        [HttpGet("{lang}/{serviceApplicationId}")]
        public async Task<ExchangeActorGeneralInfoDTO> GetCustomerById(string lang, Guid serviceApplicationId)
        {
            return await _customerRepo.GetCustomerById(lang, serviceApplicationId);
        }

        [HttpGet("GetServiceByExchangeActor")]
        public async Task<ExchangeActorServicesDto> GetServiceByExchangeActor(Guid ExchangeActorId, string lang)
        {
            return await _customerRepo.GetServiceByExchangeActor(ExchangeActorId, lang);
        }

        [HttpGet("getCustomerAmendomentById/{lang}/{exchangeActorId}")]
        public async Task<ExchangeActorGeneralInfoDTO> GetCustomerAmendomentById(string lang, Guid exchangeActorId)
        {
            return await _customerRepo.GetCustomerAmendomentById(lang, exchangeActorId);
        }
     
        [HttpPut("updateCustomerAmendment")]
        [Authorize]
        public async Task<IActionResult> UpdateCustomerAmendment([FromBody] ExchangeActorGeneralInfoDTO updatedCustomer)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedCustomer.UpdatedUserId = Guid.Parse(user?.Id);

            return await _customerRepo.UpdateExchangeActorAmendment(updatedCustomer);
        }


        [HttpGet]
        public async Task<PagedResult<CusteromSearchDTO>> SearchCustomer([FromQuery] CustomerQueryParameters customerQueryParameters)
        {
            return await _customerRepo.SearchCustomer(customerQueryParameters);
        }


        /// <summary>
        /// Get Exchange Actor Recognition Information
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="ExchangeActorId"></param>
        /// <returns></returns>

        [HttpGet("getRecognitionType/{lang}/{ExchangeActorId}")]
        public async Task<CustomerRecognitionListDTO> GetCustomerRecogntioinInfo([FromRoute] string lang, Guid ExchangeActorId)
        {
            return await _customerRepo.GetCustomerRecogntioinInfo(lang, ExchangeActorId);
        }

        /// <summary>
        /// Get Exchange Actor General Information
        /// </summary>
        /// <param name="ExchangeActorId"></param>
        /// <returns></returns>
        [HttpGet("getExchangeActorGeneralInfo/{ExchangeActorId}")]
        public async Task<ExchangeActorGeneralInfoForPRofile> GetExchangeActorGeneralInfo([FromRoute] Guid ExchangeActorId)
        {

            return await _customerRepo.GetExchangeActorGeneralInfo(ExchangeActorId);
        }

        /// <summary>
        /// Get Exchange Actor Address Information
        /// </summary>
        /// <param name="ExchangeActorId"></param>
        /// <returns></returns>
        [HttpGet("getExchangeActorAddress/{ExchangeActorId}")]
        public async Task<AddressChangeDTO> GetExchangeActorAddress([FromRoute] Guid ExchangeActorId)
        {
            return await _customerRepo.GetExchangeActorAddress(ExchangeActorId);
        }

        [HttpGet("getMemberRepresentative/{serviceApplicationId}")]
        public async Task<IEnumerable<StaticData6>> GetMemberReprsentative([FromRoute] Guid serviceApplicationId)
        {
            return await _customerRepo.GetMemebrReprsentative(serviceApplicationId);
        }
        [HttpGet("getAuditByCriteria")]
        public Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            return  _customerRepo.GetAuditByCriteria(table, exchangeActorId,actionId);
        }

    }

}
