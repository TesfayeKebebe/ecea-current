﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers
{
    [Route("api/ownermanager")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OwnerManagerController : Controller
    {
        private readonly OwnerManagerRepository _customerlegalbodyRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public OwnerManagerController(OwnerManagerRepository customerlegalbodyRepo, UserManager<ApplicationUser> userManager)
        {
            _customerlegalbodyRepo = customerlegalbodyRepo;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> PostCustomerLegalBody([FromBody] OwnerManagerDTO postedcustomerlegalbody)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedcustomerlegalbody.CreatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.PostCustomerLegalBody(postedcustomerlegalbody);
        }
        [HttpPost("postAmendCustomerLegalBody")]
        [Authorize]
        public async Task<int> PostAmendCustomerLegalBody([FromBody] OwnerManagerDTO postedcustomerlegalbody)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedcustomerlegalbody.CreatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.PostAmendCustomerLegalBody(postedcustomerlegalbody);
        }

        [HttpGet("{ExchangeActorId}")]
        public async Task<OwnerManagerDTO> GetCustomerLegalBodyById(Guid ExchangeActorId)
        {
            return await _customerlegalbodyRepo.GetCustomerLegalBodyById(ExchangeActorId);
        }

        [HttpPut]
        [Authorize]
        public async Task<bool> UpdateCustomerLegalBody([FromBody] OwnerManagerDTO updatedCustomerLegalBody)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedCustomerLegalBody.UpdatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.UpdateCustomerLegalBody(updatedCustomerLegalBody);
        }
        [HttpPut("amendmentCustomerLegalBody")]
        [Authorize]
        public async Task<bool> AmendmentCustomerLegalBody([FromBody] OwnerManagerDTO updatedCustomerLegalBody)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedCustomerLegalBody.UpdatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.AmendmentCustomerLegalBody(updatedCustomerLegalBody);
        }
        // Yemane
        [HttpGet]
        [Route("GetCustomerLegalBodyByCustomerId/{customerId}/{lang}")]
        public async Task<IEnumerable<StaticData8>> GetCustomerLegalBodyByCustomerId(Guid customerId, string lang)
        {
            return await _customerlegalbodyRepo.GetCustomerLegalBodyByCustomerId(customerId, lang);
        }

        /// <summary>
        /// Get Owner / Manager Address Information
        /// </summary>
        /// <param name="OwnerManagerId"></param>
        /// <returns></returns>
        [HttpGet("getOwnerManagerAddress/{OwnerManagerId}/{lang}")]
        public async Task<AddressDTO> GetOwnerManagerAddress([FromRoute] Guid OwnerManagerId, string lang)
        {
            return await _customerlegalbodyRepo.GetOwnerManagerAddress(OwnerManagerId, lang);
        }

        [HttpGet("getOwners/{organizationId}/{lang}")]
        public async Task<IEnumerable<OwnerViewModel>> GetOwnersByOrganizationId([FromRoute] Guid organizationid, string lang)
        {
            var ownerToReturn = await _customerlegalbodyRepo.GetOwnersByOrganizationId(organizationid, lang);

            return ownerToReturn;
        }

        [HttpGet("getOwnerById/{ownerId}")]
        public async Task<OwnerDTO> GetOwnerByOwnerId([FromRoute] int ownerId)
        {
            var ownerToReturn = await _customerlegalbodyRepo.GetOwnerById(ownerId);

            return ownerToReturn;
        }

        [HttpDelete("{ownerId}")]
        public async Task<bool> DeleteOwner([FromRoute] int ownerId)
        {
            return await _customerlegalbodyRepo.DeleteOwner(ownerId);
        }

        [HttpPut("updateOwner")]
        [Authorize]
        public async Task<bool> UpdateOwner([FromBody] OwnerManagerDTO owner)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            owner.UpdatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.UpdateOwner(owner);
        }
        [HttpPut("amendmentOwner")]
        [Authorize]
        public async Task<bool> AmendmentOwner([FromBody] OwnerManagerDTO owner)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            owner.UpdatedUserId = Guid.Parse(user?.Id);

            return await _customerlegalbodyRepo.AmendmentOwner(owner);
        }
        [HttpGet("getManagerByManagerId/{exchangeActorId}/{lang}")]
        public async Task<OwnerDataChangeVM> GetManagerInfoForDataChange([FromRoute] Guid exchangeActorId, string lang)
        {
            return await _customerlegalbodyRepo.GetManagerInforForDataChange(exchangeActorId, lang);
        }
        [HttpGet("getOwnerAuditByCriteria")]
        public Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            return _customerlegalbodyRepo.GetAuditByCriteria(table, exchangeActorId, actionId);
        }
        [HttpPost("{exchangeActorId}")]
        public async Task<IActionResult> AddUploadeManager(Guid exchangeActorId)
        {
            var excelFile = Request.Form.Files[0];
            var result = await _customerlegalbodyRepo.UploadOwners(excelFile, exchangeActorId);
            return new OkObjectResult(result);
        }
    }
}
