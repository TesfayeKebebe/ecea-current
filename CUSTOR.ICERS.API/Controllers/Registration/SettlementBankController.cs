﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Registration
{
    [Route("api/settlementbank")] 
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SettlementBankController : Controller
    {

        private readonly SettlementBankRepository _settlementBankRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public SettlementBankController(SettlementBankRepository settlementBankRepository, UserManager<ApplicationUser> userManager)
        {
            _settlementBankRepository = settlementBankRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> PostSettlementBankContact([FromBody] SettlementBankContactDTO settlementBankContact)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            settlementBankContact.CreatedUserId = Guid.Parse(user?.Id);

            return await _settlementBankRepository.PostSettlementBanContact(settlementBankContact);
        }


        [HttpGet("{lang}/{exchangeActorId}")]
        [Authorize]
        public async Task<IEnumerable<OwnerViewModel>> GetSettlementBankContactByOrganizationId([FromRoute]  string lang, Guid exchangeActorId)
        {
            var settlmentBankContact = await _settlementBankRepository.GetSettlementBankContact(lang, exchangeActorId);

            return settlmentBankContact;
        }

        [HttpGet("getSettlementBankContact/{contactId}")]
        public async Task<OwnerDTO> GetOwnerByOwnerId([FromRoute] int contactId)
        {
            var ownerToReturn = await _settlementBankRepository.GetSettlementBankContactById(contactId);

            return ownerToReturn;
        }

        [HttpPut]
        [Authorize]
        public async Task<bool> UpdateOwner([FromBody] SettlementBankContactDTO settlementContact)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            settlementContact.UpdatedUserId = Guid.Parse(user?.Id);

            return await _settlementBankRepository.UpdateSettlementBankContact(settlementContact);
        }


        [HttpDelete("{contactId}")]
        [Authorize]
        public async Task<bool> DeleteSettlementContact([FromRoute] int contactId)
        {
            return await _settlementBankRepository.DeleteSettlementBankContact(contactId);
        }
    }
}
