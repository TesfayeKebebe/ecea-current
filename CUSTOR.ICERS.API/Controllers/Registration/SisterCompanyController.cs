﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Registration
{
    [Route("api/sistercompany")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SisterCompanyController: ControllerBase
    {
        private readonly SisterCompanyRepository _sisterCompanyRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public SisterCompanyController(SisterCompanyRepository sisterCompanyRepository, UserManager<ApplicationUser> userManager)
        {
            _sisterCompanyRepository = sisterCompanyRepository;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> PostSisterCompany([FromBody] SisterCompanyPostDTO postedSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedSisterCompany.CreatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.PostSisterCompany(postedSisterCompany);
        }
        [HttpPost("postAmendmentSisterCompany")]
        [Authorize]
        public async Task<int> PostAmendmentSisterCompany([FromBody] SisterCompanyPostDTO postedSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.PostAmendmentSisterCompany(postedSisterCompany);
        }

        [HttpPost("SaveEcxMemberSisterCompnay")]
        [Authorize]
        public async Task<IActionResult> SaveEcxMemberSisterCompnay([FromBody] EcxMemberSistetCompanyDTO ecxMemberSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            ecxMemberSisterCompany.CreatedUserId = Guid.Parse(user?.Id);
            var reponse = await _sisterCompanyRepository.SaveEcxMemberSisterCompnay(ecxMemberSisterCompany);

            return reponse;
        }
        [HttpPost("postAmendmentEcxMemberSisisterCompany")]
        [Authorize]
        public async Task<IActionResult> PostAmendmentEcxMemberSisisterCompany([FromBody] EcxMemberSistetCompanyDTO ecxMemberSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            ecxMemberSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            var reponse = await _sisterCompanyRepository.PostAmendmentEcxMemberSisisterCompany(ecxMemberSisterCompany);

            return reponse;
        }
        [HttpGet("{Id}")]
        public async Task<SisterCompanyPostDTO> GetSisterCompanyById(int Id)
        {
            return await _sisterCompanyRepository.GetSisterCompanyById(Id);
        }

        [HttpGet("GetAllSisterCompanyByActorId/{exchanegActorId}/{lang}")]
        public async Task<List<SisterCompanyGetDTO>> GetAllSisterCompanyByActorId(Guid exchanegActorId, string lang)
        {
            return await _sisterCompanyRepository.GetAllSisterCompany(exchanegActorId, lang);
        }

        // Soft delete
        [HttpDelete("{Id}/{isEcxMember}")]
        public async Task<bool> DeleteSisterCompany([FromRoute] int Id, bool isEcxMember)
        {
            return await _sisterCompanyRepository.DeletSisterCompany(Id, isEcxMember);
        }


        [HttpPut]
        [Authorize]
        public async Task<bool> UpdateSisterCompany([FromBody] SisterCompanyPostDTO updatedSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            updatedSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.UpdateSisterCompany(updatedSisterCompany);
        }

        [HttpPut("UpdateEcxMemberSisterCompany")]
        [Authorize]
        public async Task<IActionResult> UpdateEcxMemberSisterCompany([FromBody] EcxMemberSistetCompanyDTO updatedEcxMemberSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            updatedEcxMemberSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.UpdateEcxMemberSisterCompany(updatedEcxMemberSisterCompany);
        }
        [HttpPut("amendmentSisterCompany")]
        [Authorize]
        public async Task<bool> AmendmentSisterCompany([FromBody] SisterCompanyPostDTO updatedSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            updatedSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.AmendmentSisterCompany(updatedSisterCompany);
        }

        [HttpPut("amendmentEcxMemberSisterCompany")]
        [Authorize]
        public async Task<IActionResult> AmendmentEcxMemberSisterCompany([FromBody] EcxMemberSistetCompanyDTO updatedEcxMemberSisterCompany)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            updatedEcxMemberSisterCompany.UpdatedUserId = Guid.Parse(user?.Id);
            return await _sisterCompanyRepository.AmendmentEcxMemberSisterCompany(updatedEcxMemberSisterCompany);
        }
        [HttpGet("getSisterAuditByCriteria")]
        public Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            return _sisterCompanyRepository.GetAuditByCriteria(table, exchangeActorId, actionId);
        }
    }
}
