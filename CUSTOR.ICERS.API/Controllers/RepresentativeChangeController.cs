﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers
{
    [Route("api/representativeChange")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class RepresentativeChangeController : Controller
    {
        private readonly RepresentativeChangeRepository _representativeChangeRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public RepresentativeChangeController(RepresentativeChangeRepository representativeChangeRepository, UserManager<ApplicationUser> userManager)
        {
            _representativeChangeRepository = representativeChangeRepository;
            _userManager = userManager;
        }


        [HttpPost]
        [Authorize]
        public async Task<IActionResult> postRepresentativeChangeData([FromBody] RepresentativeChangeDTO representativeChangeData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            representativeChangeData.CreatedUserId = Guid.Parse(user.Id);

            return await _representativeChangeRepository.postRepresentativeChangeData(representativeChangeData);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> updateRepresentativeChange([FromBody] RepresentativeChangeDTO representativeChangeData)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            representativeChangeData.UpdatedUserId = Guid.Parse(user.Id);

            return await _representativeChangeRepository.updateRepresentativeChange(representativeChangeData);
        }

        [HttpGet]
        [Route("{lang}/{serviceApplicationId}")]
        public async Task<RepresentativeViewModel> getRepresentativeChangeData(string lang, Guid serviceApplicationId)
        {
            return await _representativeChangeRepository.getRepresentativeChangeData(lang, serviceApplicationId);
        }


        [HttpPost]
        [Route("changeRepresentative")]
        [Authorize]
        public async Task<bool> updateDelgatorCompnay([FromBody] ServiceApplicationDTO representativeInfo)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            representativeInfo.UpdatedUserId = Guid.Parse(user.Id);

            return await _representativeChangeRepository.updateDelgatorCompnay(representativeInfo);
        }
    }
}
