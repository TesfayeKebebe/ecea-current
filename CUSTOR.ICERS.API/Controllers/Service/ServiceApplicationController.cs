﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.Service;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.WEBAPI.Controllers
{
    [Route("api/serviceapplication")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ServiceApplicationController : Controller
    {
        private readonly ServiceApplicationRepository _serviceApplicationRepo;
        private readonly UserManager<ApplicationUser> _userManager;


        public ServiceApplicationController(ServiceApplicationRepository serviceApplicationRepo, UserManager<ApplicationUser> userManager)
        {
            _serviceApplicationRepo = serviceApplicationRepo;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<ActionResult<ServiceApplicationDTO>> PostServiceApplicationStatus([FromBody] ServiceApplicationDTO postedServiceApplicationStatus)
        {
            return await _serviceApplicationRepo.PostServiceApplicationStatus(postedServiceApplicationStatus);
        }

        [HttpGet("{ExchangeActorId}/{serviceApplicationId}")]
        public async Task<List<ServiceApplicationDTO>> GetServiceApplicationByCusteomerAndServiceId(int ExchangeActorId, Guid serviceApplicationId)
        {
            return await _serviceApplicationRepo.GetServiceApplicationById(ExchangeActorId, serviceApplicationId);
        }

        [HttpPut]
        [Authorize]
        public async Task<ServiceApplicationDTO> UpdateServiceApplicationState([FromBody] ServiceApplicationDTO updatedServiceApplication)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            updatedServiceApplication.UpdatedUserId = Guid.Parse(user?.Id);

            return await _serviceApplicationRepo.UpdateServiceApplicaitonState(updatedServiceApplication);
        }

        [HttpGet]
        public async Task<IEnumerable<ServiceApplicationDTO>> GetDraftApplication()
        {
            return await _serviceApplicationRepo.GetDraftedApplication();
        }

        [HttpGet("SearchServiceApplication")]
        public async Task<PagedResult<ServiceApplicationSearchDTO>> Searchserviceapplication([FromQuery] ServiceApplicationQueryParameters queryparameters)
        {
            return await _serviceApplicationRepo.SearchServiceApplication(queryparameters);
        }


        [HttpGet("{serviceApplicationId}")]
        public async Task<ServiceApplicationDTO> GetServiceApplication(Guid serviceApplicationId)
        {
            return await _serviceApplicationRepo.GetServiceApplication(serviceApplicationId);
        }

    }
}
