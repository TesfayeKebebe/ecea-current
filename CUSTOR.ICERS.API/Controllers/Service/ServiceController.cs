﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Service;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers.Service
{
    [Route("api/service")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ServiceController
    {
        private readonly ServiceRepository _serviceRepo;

        public ServiceController(ServiceRepository serviceRepo)
        {
            _serviceRepo = serviceRepo;
        }

        [HttpGet("{lang}")]
        public async Task<List<LookupDTO3>> GetAllServiceType(string lang)
        {
            return await _serviceRepo.GetAllServiceType(lang);
        }
    }
}
