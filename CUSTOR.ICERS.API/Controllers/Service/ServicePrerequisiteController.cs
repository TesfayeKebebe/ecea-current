﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Service;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.API.Controllers.Service
{
    [Route("api/serviceprerequisite")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ServicePrerequisiteController
    {
        private readonly ServicePrerequisiteRepository _servicePrerequisiteRepo;
        public ServicePrerequisiteController(ServicePrerequisiteRepository servicePrerequisiteRepo)
        {
            _servicePrerequisiteRepo = servicePrerequisiteRepo;
        }

        [HttpGet("{lang}/{sid}/{customerType}")]
        public async Task<List<ServicePrerequisiteWithDocumentDTO>> GetServicePrerequiste(string lang, int sid, int customerType)
        {
            return await _servicePrerequisiteRepo.GetServicePrerequisite(lang, sid, customerType);
        }

        [HttpPost]
        public async Task<ServicePrerequisite> PostServicePrerequisite(ServicePrerequisite servicePrerequisite)
        {
            //var user = await _userManager.GetUserAsync(User);
            return await _servicePrerequisiteRepo.AddPrerequisiteForService(servicePrerequisite);
        }

        [HttpDelete("{ServicePrerequisiteId}")]
        public async Task<bool> DeleteServicePrerequisite(int ServicePrerequisiteId)
        {
            return await _servicePrerequisiteRepo.Delete(ServicePrerequisiteId);
        }

        [HttpPut("{ServicePrerequisiteId}/{IsRequired}/{IsActive}/{IsDocument}")]
        public async Task<bool> Put(int ServicePrerequisiteId, bool IsRequired, bool IsActive, bool IsDocument)
        {
            return await _servicePrerequisiteRepo.EditPrerequistiteRequirment(ServicePrerequisiteId, IsRequired, IsActive, IsDocument);
        }

        /***
         * Get service prerequiste that is Document Type include the uploaded file to be seen using ExchangeActorId and
         */

        [HttpGet("getattacheddocument/{exchangeActorId}/{serviceId}/{IsDocument}")]
        public async Task<List<UploadedDocumentVM>> GetAttachedDocumentOfService([FromRoute] Guid exchangeActorId, int serviceId, bool isDocument)
        {
            return await _servicePrerequisiteRepo.GetAttachedDocumentOfService(exchangeActorId, serviceId, isDocument);
        }

    }
}
