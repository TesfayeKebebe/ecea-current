﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Service;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Service
{
    [Route("api/servicetariff")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]

    public class ServiceTariffController : Controller
    {
        private readonly ServiceTariffRepository _serviceTariffRepository;
        private readonly UserManager<ApplicationUser> _userManager;

  
            public ServiceTariffController(ServiceTariffRepository serviceTariffRepository, UserManager<ApplicationUser> userManager)
        {
                _userManager = userManager;
                _serviceTariffRepository = serviceTariffRepository;
        }

        [HttpGet("{serviceId}")]
        //public async Task<decimal> GetServiceTotoalCost(int serviceId)
        //{
        //    return await _serviceTariffRepository.GetServieTariffTotalCost(serviceId);
        //}
        [HttpPost("saveserviceTariff")]
        public async Task<bool> SaveserviceTariff(ServicetariffDTO serviceTariff)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (serviceTariff.ServiceTariffId == 0)
            {
                serviceTariff.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                serviceTariff.UpdatedUserId = Guid.Parse(user.Id);
            }

            return await _serviceTariffRepository.SaveserviceTariff(serviceTariff);
        }
        [HttpGet("getallserviceTariff/{lang}")]
        public async Task<List<AllServiceTariffDTO>> getallserviceTariff(string lang)
        {
            return await _serviceTariffRepository.GetAllServiceTariff(lang);
        }

        [HttpGet("{lang}/{serviceId}/{exchangeActorTypeId}")]
        public async Task<List<ServiceTariffDTO>> 
            GetServiceTariffByServiceId(string lang, int serviceId, int exchangeActorTypeId)
        {
            return await _serviceTariffRepository.GetServiceTariffByServiecID(lang, serviceId, exchangeActorTypeId);
        }
        [HttpDelete("{id}")]
        public async Task<bool> DeleteTariff(int id)
        {
            return await _serviceTariffRepository.DeleteTariff(id);
        }
    }
}
