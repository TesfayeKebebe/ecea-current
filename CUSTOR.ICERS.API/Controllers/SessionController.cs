﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.EntityLayer.ECX;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Session")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly SessionRepositry sessionRepository;
        private readonly IHostingEnvironment host;
        public SessionController(SessionRepositry rep, IHostingEnvironment hosting )
        {
            this.host = hosting;
            this.sessionRepository = rep;
        }
        [HttpGet]
        public async Task<List<SessionDTo>> getAll()
        {
            return await sessionRepository.GetSession();
        }
    }
}