﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class BankOverSightViolationDetailController : ControllerBase
    {
        private readonly BankOverSightViolationDetailRepo violationDetailRepo;
        private readonly UserManager<ApplicationUser> userManager;

        public BankOverSightViolationDetailController(BankOverSightViolationDetailRepo _violationDetailRepo,
                                                      UserManager<ApplicationUser> _userManager)
        {
           violationDetailRepo = _violationDetailRepo;
            userManager = _userManager;
        }
        [HttpGet("getBankViolationRecordById/{id}")]
        public async Task<BankOverSightViolationDetail> GetBankViolationRecordById(int id)
        {
            return await violationDetailRepo.GetBankViolationRecordById(id);
        }
        [HttpGet("getListOfViolationById/{exchangeActorId}/{lang}")]
        public async Task<List<BankOverSightViolationViewModels>> GetListOfViolationById(Guid exchangeActorId, string lang)
        {
            return await violationDetailRepo.GetListOfViolationById(exchangeActorId, lang);
        }
        [HttpGet("getBankViolationListById/{exchangeActorId}/{lang}")]
        public async Task<List<BankOverSightViolationViewModels>> GetBankViolationListById(int exchangeActorId, string lang)
        {
            return await violationDetailRepo.GetBankViolationListById(exchangeActorId, lang);
        }
        [HttpPost("updateBankViolation")]
        [Authorize]
        public async Task<int> UpdateBankViolation(BankOverSightViolationDetailDTO bankViolation)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            bankViolation.UpdatedUserId = Guid.Parse(user.Id);
            return await violationDetailRepo.UpdateBankViolation(bankViolation);
        }
        [HttpPost]
        [Route("deleteBankViolation")]
        [Authorize]
        public async Task<int> DeleteBankViolation(BankOverSightViolationDetail bankViolation)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            bankViolation.UpdatedUserId = Guid.Parse(user.Id);
            return await violationDetailRepo.DeleteBankViolation(bankViolation);
        }
        [HttpGet]
        [Route("getBankViolationSearchByCriteria")]
        public async Task<List<BankOverSightViolationViewModels>> GetBankViolationSearchByCriteria([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await violationDetailRepo.GetBankViolationSearchByCriteria(clientTrade);
        }
    }
}
