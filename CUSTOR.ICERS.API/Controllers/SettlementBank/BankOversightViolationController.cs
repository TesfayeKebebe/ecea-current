﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.Security;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class BankOversightViolationController : ControllerBase
    {
        private readonly BankOverSightViolationRepository sightViolationRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public BankOversightViolationController(BankOverSightViolationRepository _sightViolationRepository,
                                                UserManager<ApplicationUser> _userManager)
        {
            sightViolationRepository = _sightViolationRepository;
            userManager = _userManager;
        }
        [HttpPost]
        [Route("createViolationRecord")]
        [Authorize]
        public async Task<int> CreateViolationRecord(BankOversightViolationDTO tradeExcution)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            tradeExcution.CreatedUserId = Guid.Parse(user.Id);
            return await sightViolationRepository.CreateViolationRecord(tradeExcution);
        }
    }
}
