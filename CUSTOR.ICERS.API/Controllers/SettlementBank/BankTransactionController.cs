﻿using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.API.Authorization;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CUSTOR.Oversight.UI.Controllers
{


    // [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class BankTransactionController : ControllerBase
    {
        private readonly BankTransactionRepository transRepository;
        private readonly IMapper mapper;
        public BankTransactionController(BankTransactionRepository transRepo, IMapper _mapper)
        {
            transRepository = transRepo;
            mapper = _mapper;
        }

        [HttpGet("GetAllTrans")]
        public async Task<IActionResult> GetAllBankTransactionsAsync([FromQuery] QueryParameters queryParameters)
        {
            PagedResult<BankTransactionDTO> result = await transRepository.GetAllBankTransactionsAsync(queryParameters);
            return new OkObjectResult(result);
        }
        [HttpGet("GetFilteredTrans")]
        public async Task<IActionResult> GetAllBankTransactionsAsync([FromQuery] QueryParameters queryParameters, string bankName)
        {
            PagedResult<BankTransactionDTO> result = await transRepository.GetAllBankTransactionsAsync(queryParameters, bankName);
            return new OkObjectResult(result);
        }
        [HttpGet("GetAllBankTransactionList")]
        public async Task<List<BankTransaction>> GetAllBankTransactionList(string memberCode, string memberType, string accountType, string SiteCode, string start, string end)

        {

            return await transRepository.GetAllBankTransactionList(memberCode, memberType, accountType, SiteCode, start, end);
            // return new OkObjectResult(summary);
        }
        [HttpGet("GetUploadSummary/{bankCode}")]
        //[Authorize(Policies.ManageAdministratorsPolicy)]
        public async Task<IActionResult> GetUploadSummary(string bankCode)
        {

            var summary = await transRepository.GetUploadSummaryAsync(bankCode);
            return new OkObjectResult(summary);
        }

        [Authorize(Policies.ManageAdministratorsPolicy)]
        [HttpPost("{siteCode}")]
        public async Task<IActionResult> AddBankTransactionsAsync(string siteCode)
        {
            var xmlFile = Request.Form.Files[0];
            if (!xmlFile.ContentType.Equals("application/xml") && !xmlFile.ContentType.Equals("text/xml"))
                return BadRequest();
            var result = await transRepository.UploadTransaction(xmlFile, siteCode);
            return new OkObjectResult(result);


            //if (xmlFile.ContentType.Equals("application/xml") || xmlFile.ContentType.Equals("text/xml"))
            //{
            //    try
            //    {
            //        using (var fileStream = new FileStream(fileToSave, FileMode.Create))
            //        {
            //            await xmlFile.CopyToAsync(fileStream);
            //            fileStream.Dispose();
            //            XDocument xDoc = XDocument.Load(fileToSave);
            //            //to-do change to utc
            //            string dateFormat = "m/d/yyyy";
            //            int i = xDoc.Descendants("BankTransaction").ToList().Count;
            //            List<BankTransaction> transList = xDoc.Descendants("transaction").Select(xNode =>
            //            new BankTransaction
            //            {
            //                //Description = xNode.Element("MemberTransactionDescription").Value,
            //                AccountHolderName = xNode.Element("AccountHolderName").Value,
            //                AccountNo = xNode.Element("AccountNumber").Value,
            //                AccountType = xNode.Element("AccountType").Value,
            //                MemberType = xNode.Element("MemberType").Value,
            //                BranchName = xNode.Element("BranchName").Value,
            //                Credit = Convert.ToDecimal(xNode.Element("Credit").Value),
            //                Debit = Convert.ToDecimal(xNode.Element("Debit").Value),
            //                Balance = Convert.ToDecimal(xNode.Element("Balance").Value),
            //                BBF = Convert.ToDecimal(xNode.Element("BBF").Value),
            //                TransactionDate = DateTime.ParseExact(xNode.Element("TransactionDate").Value, dateFormat, CultureInfo.InvariantCulture),
            //                Reference = xNode.Element("Reference").Value,
            //                UnAuthorized = true,
            //                IsThroughUpload =true,
            //               SiteCode =siteCode,
            //                CreatedUserId = "Abebe", //to be changed
            //                CreatedDateTime = DateTime.UtcNow //to be changed

            //        }).ToList();

            //            int n=transList.Count;
            //             await transRepository.AddBankTransactions(transList, siteCode, xmlFile.FileName, n);
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        string s = e.Message;
            //        throw new Exception(e.Message);
            //    }

        }


        [HttpDelete("DeleteUpload/{id}")]
        public async Task<IActionResult> DeleteBankTransactionsAsync(int uploadId)
        {
            var transDTO = await transRepository.DeleteBankTransactionAsync(uploadId);
            if (transDTO == null)
            {
                throw new ApiException("Record with the supplied ID was not found", (int)HttpStatusCode.NotFound);
            }
            return new OkObjectResult(transDTO);

        }
        [HttpGet("getBankTransactionClearing")]
        public async Task<List<ClearingSettlementBank>> GetBankTransactionClearing(string SiteCode, string start, string end)
        {
            return await transRepository.GetListClearingSettlementBank(SiteCode, start, end);
        }

        [HttpGet("getTotalMemberPayInBankList")]
        public async Task<List<ClearingSettlementBank>> GetTotalMemberPayInBankList(string siteCode, string start, string end)
        {
            return await transRepository.GetTotalMemberPayInBankList(siteCode,start,end);
        }
        [HttpGet("getTotalPayInList")]
        public async Task<List<ClearingSettlementBank>> GetTotalPayInList(string siteCode, string start, string end)
        {
            return await transRepository.GetTotalPayInList(siteCode,start,end);
        }
        [HttpGet("getTotalClientPayInList")]
        public async Task<List<ClearingSettlementBank>> GetTotalClientPayInList(string siteCode, string start, string end)
        {
            return await transRepository.GetTotalClientPayInList(siteCode,start,end);
        }
        [HttpGet("getNonMembersPayInList")]
        public async Task<List<ClearingSettlementBank>> GetNonMembersPayInList(string siteCode, string start, string end)
        {
            return await transRepository.GetNonMembersPayInList(siteCode,start,end);
        }


        [HttpGet("getMembersTotalPayOut")]
        public async Task<List<ClearingSettlementBank>> GetMembersTotalPayOut(string siteCode, string start, string end)
        {
            return await transRepository.GetMembersTotalPayOut(siteCode,start,end);
        }
        [HttpGet("getClientTotalPayOut")]
        public async Task<List<ClearingSettlementBank>> GetClientTotalPayOut(string siteCode, string start, string end)
        {
            return await transRepository.GetClientTotalPayOut(siteCode,start,end);
        }
        [HttpGet("getNonMembersTotalPayOut")]
        public async Task<List<ClearingSettlementBank>> GetNonMembersTotalPayOut(string siteCode, string start, string end)
        {
            return await transRepository.GetNonMembersTotalPayOut(siteCode,start,end);
        }
        [HttpGet("getTotalPayOut")]
        public async Task<List<ClearingSettlementBank>> GetTotalPayOut(string siteCode, string start, string end)
        {
            return await transRepository.GetTotalPayOut(siteCode,start,end);
        }
        [HttpGet("getDifferencePayInPayOut")]
        public async Task<List<ClearingSettlementBank>> GetDifferencePayInPayOut(string siteCode, string start, string end)
        {
            return await transRepository.GetDifferencePayInPayOut(siteCode,start,end);
        }
        [HttpGet("getAllBankTransactionReportYear")]
        public List<AllBankTransactionReportView> GetAllBankTransactionReportYear([FromQuery]SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetAllBankTransactions(searchBankTransaction);
        }
        [HttpGet("getAllUnauthorizedBankTransactions")]
        public List<UnAuthorizedBankTransaction> GetAllUnauthorizedBankTransactions([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetAllUnauthorizedBankTransactions(searchBankTransaction);
        }
        [HttpGet("getBankTransactionMonthlyReportList")]
        public List<BankTransactionMonthlyReport> GetBankTransactionMonthlyReportList([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetBankTransactionMonthlyReportList(searchBankTransaction);
        }
        [HttpGet("getBankTransactionTopTen")]
        public List<BankTransactionTopTen> GetBankTransactionTopTen([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetBankTransactionTopTen(searchBankTransaction);
        }
        [HttpGet("getViolationTransaction")] 
        public List<ViolationBankTransaction> GetViolationTransaction([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetViolationTransaction(searchBankTransaction);
        }
        [HttpGet("getDailyBankTransactionList")] 
        public async Task<List<BankTransactionViewModel>> GetDailyBankTransactionList([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return await transRepository.GetDailyBankTransactionList(searchBankTransaction);
        }
        [HttpGet("getDailySettlementTransactionReport")]
        public List<DailySettlementTransactionReport> GetDailySettlementTransactionReport([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return  transRepository.GetDailySettlementTransactionReport(searchBankTransaction);
        }
        [HttpGet("getSettlementTransactionReportSummery")]
        public List<SettlementTransactionReportSummery> GetSettlementTransactionReportSummery([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return transRepository.GetSettlementTransactionReportSummery(searchBankTransaction);
        }
    }
}