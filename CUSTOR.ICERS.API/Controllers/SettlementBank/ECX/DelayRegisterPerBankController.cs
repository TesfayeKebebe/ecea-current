﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using CUSTOR.ICERS.DAL.Repository.Ecx;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.ECX
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class DelayRegisterPerBankController : ControllerBase
    {
        private readonly DelayRegisterPerBankRepository _delayRegisterPer;

        public DelayRegisterPerBankController(DelayRegisterPerBankRepository delayRegisterPer)
        {
            _delayRegisterPer = delayRegisterPer;
        }

        [HttpGet("getDelayRegisterPerBankList")]
        public async Task<List<DelayRegisterPerBankDTO>> GetDelayRegisterPerBankList([FromQuery] SearchEcxParameters qp)
        {
            return await _delayRegisterPer.GetDelayRegisterPerBankList(qp);
        }
    }
}
