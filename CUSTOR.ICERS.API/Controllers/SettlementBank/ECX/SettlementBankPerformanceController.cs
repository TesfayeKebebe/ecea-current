﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using CUSTOR.ICERS.DAL.Repository.Ecx;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.ECX
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SettlementBankPerformanceController : ControllerBase
    {
        private readonly SettlementBankPerformanceRepository _bankPerformanceRepository;

        public SettlementBankPerformanceController(SettlementBankPerformanceRepository bankPerformanceRepository)
        {
            _bankPerformanceRepository = bankPerformanceRepository;
        }

        [HttpGet("getAllSettleBankPerformanceList")]
        public async Task<List<SettlementBankPerformanceDTO>> GetAllSettleBankPerformanceList([FromQuery] SearchEcxParameters qp)
        {
            return await _bankPerformanceRepository.GetAllSettleBankPerformanceList(qp);
        }
    }
}
