﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using CUSTOR.ICERS.DAL.Repository.Ecx;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.Oversight.API.Controllers.ECX
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SettlementGuarnatteFundController : ControllerBase
    {
        private readonly SettlementGuarnatteFundRepository _guarnatteFundRepository;

        public SettlementGuarnatteFundController(SettlementGuarnatteFundRepository guarnatteFundRepository)
        {
            _guarnatteFundRepository = guarnatteFundRepository;
        }
        [HttpGet("getAllSettlementGuaranteeFundList")]
        public async Task<List<SettlementGuarnatteFundDTO>> GetAllSettlementGuaranteeFundList([FromQuery] SearchEcxParameters qp)
        {
            return await _guarnatteFundRepository.GetAllSettlementGuaranteeFundList(qp);
        }

        [HttpGet("getBankGuaranteeFundList")]
        public async Task<List<GuaranteeFundViewModel>> GetBankGuaranteeFundList([FromQuery] SearchEcxParameters qp)
        {
            return await _guarnatteFundRepository.GetBankGuaranteeFundList(qp);
        }
    }
}
