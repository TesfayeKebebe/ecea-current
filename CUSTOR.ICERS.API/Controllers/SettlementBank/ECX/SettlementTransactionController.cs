﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank.ECX
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SettlementTransactionController : ControllerBase
    {
        private readonly SettlementTransactionRepository _settlementTransaction;

        public SettlementTransactionController(SettlementTransactionRepository settlementTransaction)
        {
            _settlementTransaction = settlementTransaction;
        }
        [HttpGet("getSettlementTransactionMontlyReport")]
        public List<SettlementTransactionMonthlyView> GetSettlementTransactionMontlyReport([FromQuery] SearchSqlQueryParms searchParms)
        {
            return  _settlementTransaction.GetSettlementTransactionMontlyReport(searchParms);
        }
        [HttpGet("getSettlementTransactionDailyReport")]
        public List<SettlementTransactionDailyView> GetSettlementTransactionDailyReport([FromQuery] SearchSqlQueryParms searchParms)
        {
            return _settlementTransaction.GetSettlementTransactionDailyReport(searchParms);
        }
    }
}
