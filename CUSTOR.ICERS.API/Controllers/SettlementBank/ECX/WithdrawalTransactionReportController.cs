﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank.ECX
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class WithdrawalTransactionReportController : ControllerBase
    {
        private readonly WithdrawalTransactionReportRepository _withdrawalTransactionReport;

        public WithdrawalTransactionReportController(WithdrawalTransactionReportRepository withdrawalTransactionReport)
        {
            _withdrawalTransactionReport = withdrawalTransactionReport;
        }
        [HttpGet("getWithdrawalTransactionMontlyReport")]
        public List<SettlementTransactionMonthlyView> GetWithdrawalTransactionMontlyReport([FromQuery] SearchSqlQueryParms searchParms)
        {
            return _withdrawalTransactionReport.GetWithdrawalTransactionMontlyReport(searchParms);
        }
        [HttpGet("getWithdrawalTransactionDailyReport")]
        public List<SettlementTransactionDailyView> GetWithdrawalTransactionDailyReport([FromQuery] SearchSqlQueryParms searchParms)
        {
            return _withdrawalTransactionReport.GetWithdrawalTransactionDailyReport(searchParms);
        }
    }
}
