﻿using System.Collections.Generic;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SettlementAccountController : ControllerBase
    {
        private readonly SettlementAccountRepository settlementAccount;

        public SettlementAccountController(SettlementAccountRepository _settlementAccount)
        {
            settlementAccount = _settlementAccount;
        }
        [HttpGet("getBankSettlementAccountList")]
        public List<SettlementAccountDTO> GetBankSettlementAccountList([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return settlementAccount.GetBankSettlementAccountList(searchBankTransaction);
        }
    }
}
