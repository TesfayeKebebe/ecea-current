﻿using AutoMapper;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.API.Authorization;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.Oversight.DAL;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.Oversight.UI.Controllers
{
    //[ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class SiteController : ControllerBase
    {
        private readonly SiteRepository siteRepository;
        private readonly IMapper _mapper;
        public SiteController(SiteRepository siteRepo, IMapper mapper)
        {
            siteRepository = siteRepo;
            _mapper = mapper;

        }

        [HttpGet("GetAllTrans")]
        public async Task<IActionResult> GetAllSitesAsync([FromQuery] QueryParameters queryParameters)
        {
            var query = await siteRepository.GetAllSitesAsync(queryParameters);
            var result = _mapper.Map<IEnumerable<Site>, IEnumerable<SiteDTO>>(query);
            var pagedResult = new PagedResult<SiteDTO>()
            {
                Items = result,
                ItemsCount = result.Count()
            };

            return new OkObjectResult(pagedResult);
        }


        [HttpGet("{siteCode}")]
        public async Task<IActionResult> FindSiteAsync(string siteCode)
        {
            var site = await siteRepository.GetSiteAsync(siteCode);
            var siteDTO = _mapper.Map<Site, SiteDTO>(site);
            return new OkObjectResult(siteDTO);
        }
        [HttpGet("GetSites/{lang}")]
        public async Task<IEnumerable<StaticData7>> GetSites(string lang)
        {
            return await siteRepository.GetSitesList(lang, true);
        }
        [HttpGet("GetActiveSites")]
        [Authorize(Policies.ViewAllRolesPolicy)]
        public async Task<IActionResult> GetActiveSites()
        {
            var siteList = await siteRepository.GetAllSitesList();
            var sites = _mapper.Map<List<SiteDTO>>(siteList);
            return new OkObjectResult(sites);
        }
        [HttpPost]
        public async Task<IActionResult> AddSitesAsync([FromBody] SiteDTO postedSite)
        {
            var site = _mapper.Map<SiteDTO, Site>(postedSite);
            var bSiteDTO = await siteRepository.AddSite(site);
            return new OkObjectResult(bSiteDTO);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSitesAsync([FromBody] SiteDTO siteDTO)
        {
            var site = _mapper.Map<SiteDTO, Site>(siteDTO);
            var bSiteDTO = await siteRepository.UpdateSite(site);
            return new OkObjectResult(bSiteDTO);
        }
        [HttpDelete("{siteId}")]
        public async Task<IActionResult> DeleteSitesAsync(string siteId)
        {
            var bSiteDTO = await siteRepository.DeleteSiteAsync(siteId);

            return new OkObjectResult(bSiteDTO);

        }
    }
}