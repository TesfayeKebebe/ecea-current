﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.SettlementBank
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class SvgSettlementAccountController : ControllerBase
    {
        private readonly SvgSettlementAccountRepository settlementAccountRepository;

        public SvgSettlementAccountController(SvgSettlementAccountRepository settlementAccountRepository)
        {
            this.settlementAccountRepository = settlementAccountRepository;
        }
        [HttpGet("getBankSVGandSettlementAccountList")]
        public List<SvgSettlementAccountViewModel> GetBankSVGandSettlementAccountList([FromQuery] SearchBankTransactionCriteria searchBankTransaction)
        {
            return settlementAccountRepository.GetBankSVGandSettlementAccountList(searchBankTransaction);
        }
    }
}
