﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Trade;
using CUSTOR.ICERS.Core.EntityLayer.Trade;
using CUSTOR.ICERS.Core.EntityLayer.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Trade
{
    [Route("api/trade")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeController : ControllerBase
    {

        private readonly TradeRepository _tradeRepository;

        public TradeController(TradeRepository tradeRepository)
        {
            _tradeRepository = tradeRepository;
        }

        [HttpGet]
        public IEnumerable<TradeViewModel> GetTradeInformations([FromQuery] TradeSearchCritira tradeSearchCriteria)
        {
            return _tradeRepository.GetTradeInformations(tradeSearchCriteria);
        }
    }
}
