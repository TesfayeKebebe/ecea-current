﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class AnnualBudgetCloserController : ControllerBase
    {
        private readonly AnnualBudgetCloserRepository annualBudgetCloser;
        private readonly UserManager<ApplicationUser> userManager;

        public AnnualBudgetCloserController(AnnualBudgetCloserRepository annualBudgetCloser,
                                            UserManager<ApplicationUser> userManager)
        {
            this.annualBudgetCloser = annualBudgetCloser;
            this.userManager = userManager;
        }
        [HttpPost("create")]
        public async Task<int> Create(AnnualBudgetCloserNewDTO annualBudget)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            annualBudget.CreatedUserId = Guid.Parse(user.Id);
            return await annualBudgetCloser.Create(annualBudget);
        }
        [HttpPost("update")]
        public async Task<int> Update(AnnualBudgetCloserNewDTO annualBudget)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            annualBudget.UpdatedUserId = Guid.Parse(user.Id);
            return await annualBudgetCloser.Update(annualBudget);
        }
        [HttpGet("getAnnualBudgetCloserId/{annualBudgetId}")]
        public async Task<AnnualBudgetCloser> GetAnnualBudgetCloserId(int annualBudgetId)
        {
            return await annualBudgetCloser.GetAnnualBudgetCloserId(annualBudgetId);
        }
        [HttpGet("getAnnualBudgetCloserList/{lang}")]
        public async Task<List<AnnualBudgetCloserView>> GetAnnualBudgetCloserList(string lang)
        {
            return await annualBudgetCloser.GetAnnualBudgetCloserList(lang);
        }
        [HttpGet("getAnnualBudgetCloser/{lang}")]
        public async Task<List<AnnualBudgetCloserViewModel>> GetAnnualBudgetCloser(string lang)
        {
            return await annualBudgetCloser.GetAnnualBudgetCloser(lang);
        }

        [HttpDelete("{id}")]
        public async Task<bool> DeleteCriteria(int id)
        {
            return await annualBudgetCloser.DeleteAnnualBuget(id);
        }

    }
}
