using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.Registration;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{

    // [Authorize]

    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ClientInformationController : ControllerBase
    {
        private readonly MemberClientInformationRepo clientInformationRepo;
        private readonly MemberProductRepository _memberProductRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public ClientInformationController(MemberClientInformationRepo _clientInformationRepo,
                                            MemberProductRepository memberProductRepository,
                                            UserManager<ApplicationUser> userManager)
        {
            clientInformationRepo = _clientInformationRepo;
            _memberProductRepository = memberProductRepository;
            _userManager = userManager;
        }     

        //[HttpPost]
        //[Route("CreateMemberClinetReg")]
        //public async Task<ResultResponse> CreateMemberClinetReg(MemberClientInformation memberClient)
        //{
        //    return await clientInformationRepo.CreateMemberClinetReg(memberClient);
        //}
        [HttpGet]
        [Route("GetClientInfoListById")]
        public async Task<PagedResult<ClientInformationViewModel>> GetClientInfoListById([FromQuery] ClientTradeQueryParameters queryParameters)
        {
            return await clientInformationRepo.GetClientInfoListById(queryParameters);
        }
        [HttpGet]
        [Route("getAllClientInformationList")]
        public async Task<List<ClientInformationViewModel>> GetAllClientInformationList([FromQuery] ClientTradeQueryParameters lang)
        {
            return await clientInformationRepo.GetAllClientInformationList(lang);
        }
        [HttpGet]
        [Route("GetClientInformationList")]
        //  [Authorize(Policies.ViewMemberClientInformation)]
        public async Task<PagedResult<ClientInformationViewModel>> GetClientInformationList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await clientInformationRepo.GetClientInformationList(clientTradeQueryParameters);
        }

        [HttpGet]
        [Route("GetClintInformationById/{clietInformationId}")]
        public async Task<ClientInformationViewModel> GetClintInformationById(int clietInformationId)
        {
            return await clientInformationRepo.GetClintInformationById(clietInformationId);
        }
        [HttpPost]
        [Route("UpdateMemberClientInformation")]
        [Authorize]
        public async Task<int> UpdateMemberClientInformation(MemberClientInformationDTO memberClientInformation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            memberClientInformation.CreatedUserId = Guid.Parse(user.Id);
            await _memberProductRepository.DeleteClientProduct(memberClientInformation.MemberClientInformationId);
            return await clientInformationRepo.UpdateMemberClientInformation(memberClientInformation);
        }
        [HttpPost]
        [Route("DeleteMemberClientInformation")]
        public async Task<Guid> DeleteMemberClientInformation(MemberClientInformationDTO memberClientInformation)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            memberClientInformation.CreatedUserId = Guid.Parse(user.Id);
            return await clientInformationRepo.DeleteMemberClientInformation(memberClientInformation);
        }

        [HttpPost]
        [Route("SaveClientInformation")]
        public async Task<Guid> SaveClientInformation(MemberClientInformationDTO memberClientInformation)
        {
            return await clientInformationRepo.SaveClientInformation(memberClientInformation);
        }

        [HttpGet]
        [Route("GetMemberClientInformation/{customerId}/{lang}")]
        public async Task<IEnumerable<StaticData3>> GetMemberClientInformation(Guid customerId, string lang)
        {
            return await clientInformationRepo.GetClientInformationByCustomerId(customerId, lang);
        }
        [HttpGet]
        [Route("GetClientFullNameList")]
        public async Task<List<ClientInformationViewModel>> GetClientFullNameList([FromQuery] ClientTradeQueryParameters tradeQueryParameters, Guid exchangeActor)
        {
            return await clientInformationRepo.GetClientFullName(tradeQueryParameters, exchangeActor);
        }
        [HttpGet]
        [Route("getMembersTotalNoOfClient")]
        public async Task<PagedResult<MembersTotalClient>> GetMembersTotalNoOfClient([FromQuery] ClientTradeQueryParameters tradeQueryParameters)
        {
            return await clientInformationRepo.GetMembersTotalNoOfClient(tradeQueryParameters);
        }
        [HttpGet]
        [Route("getTotalNoListOfClientInformation")]
        public List<TradeExecutionAmountViewModel> getTotalNoListOfClientInformation([FromQuery] TradeExecutionOrderSearchQuery tradeQueryParameters)
        {
            return  clientInformationRepo.GetTotalNoListOfClientInformation(tradeQueryParameters);
        }
        [HttpGet]
        [Route("getMembersClientInformationList")]
        public PagedResult<ClientInformationViewModel> GetMembersClientInformationList([FromQuery] ClientTradeQueryParameters tradeQueryParameters)
        {
            return clientInformationRepo.GetMembersClientInformationList(tradeQueryParameters);
        }
        [HttpGet]
        [Route("getListOfBasicClientInformation")]
        public PagedResult<ClientInformationViewModel> GetListOfBasicClientInformation([FromQuery] ClientTradeQueryParameters tradeQueryParameters)
        {
            return clientInformationRepo.GetListOfBasicClientInformation(tradeQueryParameters);
        }

    }
}
