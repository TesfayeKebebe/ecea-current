﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ClientInformationReminderController : ControllerBase
    {
        private readonly ClientInformationReminderRepo clientInformation;
        private readonly UserManager<ApplicationUser> userManager;

        public ClientInformationReminderController(ClientInformationReminderRepo _clientInformation,
                                                   UserManager<ApplicationUser> userManager)
        {
            clientInformation = _clientInformation;
            this.userManager = userManager;
        }

        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<int> Create(ClientInformationReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.CreatedUserId = Guid.Parse(user.Id);
            return await clientInformation.Create(financialReport);
        }
        [HttpPost]
        [Route("Update")]
        [Authorize]
        public async Task<int> Update(ClientInformationReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.UpdatedUserId = Guid.Parse(user.Id);
            return await clientInformation.Update(financialReport);
        }
        [HttpPost]
        [Route("Delete")]
        [Authorize]
        public async Task<int> Delete(ClientInformationReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.UpdatedUserId = Guid.Parse(user.Id);
            return await clientInformation.Delete(financialReport);
        }
        [HttpGet]
        [Route("GetClientInformationReminderId/{financialReportId}")]
        public async Task<ClientInformationReminder> GetClientInformationReminderId(int financialReportId)
        {
            return await clientInformation.GetClientInformationReminderId(financialReportId);
        }
        [HttpGet]
        [Route("GetClientInformationReminderList/{lang}")]
        public async Task<List<ClientInformationReminderDTO>> GetClientInformationReminderList(string lang)
        {
            return await clientInformation.GetClientInformationReminderList(lang);
        }
    }
}