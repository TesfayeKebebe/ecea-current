﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class CommodityGradeController : ControllerBase
    {
        private readonly CommodityGradeRepository commodityGrade;
        private readonly UserManager<ApplicationUser> userManager;

        public CommodityGradeController(CommodityGradeRepository _commodityGrade,
                                        UserManager<ApplicationUser> userManager)
        {
            commodityGrade = _commodityGrade;
            this.userManager = userManager;
        }
        [HttpGet]
        [Route("GetCommodityGradeById/{commodityTypeId}/{lang}")]
        public async Task<IEnumerable<StaticData2>> GetCommodityGradeById(int commodityTypeId, string lang)
        {
            return await commodityGrade.GetCommodityGradeById(commodityTypeId, lang);
        }

        [HttpGet]
        [Route("GetCommodityGradeByIda/{commoditygradeId}/{lang}")]
        public async Task<CommodityGradeByIdDTO> GetCommodityGradeByIda(int commoditygradeId, string lang)
        {
            return await commodityGrade.GetCommodityGradeByIda(commoditygradeId, lang);
        }
        [HttpPost]
        [Route("create")]
        [Authorize]
        public async Task<int> Create(CommodityGradeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.CreatedUserId = Guid.Parse(user.Id);
            return await commodityGrade.Create(commodity);
        }
        [HttpPost]
        [Route("update")]
        [Authorize]
        public async Task<int> Update(CommodityGradeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.UpdatedUserId = Guid.Parse(user.Id);
            return await commodityGrade.Update(commodity);
        }
        [HttpPost]
        [Route("delete")]
        [Authorize]
        public async Task<int> Delete(CommodityGradeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.UpdatedUserId = Guid.Parse(user.Id);
            return await commodityGrade.Delete(commodity);
        }


        [HttpGet]
        [Route("getCommodityById/{reportPeriodId}")]
        public async Task<CommodityGrade> GetCommodityById(int reportPeriodId)
        {
            return await commodityGrade.GetCommodityById(reportPeriodId);
        }

        [HttpGet]
        [Route("getAllCommodityGradeList/{lang}")]
        public async Task<List<CommodityGradeDTO>> GetAllCommodityGradeList(string lang)
        {
            return await commodityGrade.GetAllCommodityList(lang);
        }
    }
}