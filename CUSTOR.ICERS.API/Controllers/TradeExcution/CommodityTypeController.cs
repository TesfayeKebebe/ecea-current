﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class CommodityTypeController : ControllerBase
    {
        private readonly CommodityTypeRepositoory _typeRepositoory;
        private readonly UserManager<ApplicationUser> userManager;

        public CommodityTypeController(CommodityTypeRepositoory typeRepositoory,
                                       UserManager<ApplicationUser> userManager)
        {
            _typeRepositoory = typeRepositoory;
            this.userManager = userManager;
        }

        [HttpGet()]
        [Route("GetCommodityTypeById/{commodityId}/{lang}")]
        public async Task<IEnumerable<StaticData2>> GetCommodityTypeById(int commodityId, string lang)
        {
            return await _typeRepositoory.GetCommodityTypeById(commodityId, lang);
        }
        [HttpPost]
        [Route("create")]
        public async Task<int> Create(CommodityTypeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.CreatedUserId = Guid.Parse(user.Id);
            return await _typeRepositoory.Create(commodity);
        }
        [HttpPost]
        [Route("update")]
        public async Task<int> Update(CommodityTypeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.UpdatedUserId = Guid.Parse(user.Id);
            return await _typeRepositoory.Update(commodity);
        }
        [HttpPost]
        [Route("delete")]
        public async Task<int> Delete(CommodityTypeDTO commodity)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            commodity.UpdatedUserId = Guid.Parse(user.Id);
            return await _typeRepositoory.Delete(commodity);
        }
        [HttpGet]
        [Route("getCommodityById/{reportPeriodId}")]
        public async Task<CommodityType> GetCommodityById(int reportPeriodId)
        {
            return await _typeRepositoory.GetCommodityById(reportPeriodId);
        }
        [HttpGet]
        [Route("getAllCommodityList/{lang}")]
        public async Task<List<CommodityTypeDTO>> GetAllCommodityList(string lang)
        {
            return await _typeRepositoory.GetAllCommodityList(lang);
        }
    }
}
