﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ExchangeActorFinanicialController : ControllerBase
    {
        private readonly ExchangeActorFinanicialRepo actorFinanicialRepo;
        private readonly UserManager<ApplicationUser> userManager;

        public ExchangeActorFinanicialController(ExchangeActorFinanicialRepo actorFinanicialRepo,
                                                 UserManager<ApplicationUser> userManager)
        {
            this.actorFinanicialRepo = actorFinanicialRepo;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<int> Create(ExchangeActorFinanicialDTO exchangeActorFinanicial)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            exchangeActorFinanicial.CreatedUserId = Guid.Parse(user.Id);
            return await actorFinanicialRepo.Create(exchangeActorFinanicial);
        }
        [HttpPost]
        [Route("Update")]
        [Authorize]
        public async Task<int> Update(ExchangeActorFinanicialDTO finanicialDTO)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            finanicialDTO.CreatedUserId = Guid.Parse(user.Id);
            return await actorFinanicialRepo.Update(finanicialDTO);
        }
        [HttpPost]
        [Route("Delete")]
        [Authorize]
        public async Task<int> Delete(ExchangeActorFinanicialDTO actorFinanicial)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            actorFinanicial.CreatedUserId = Guid.Parse(user.Id);
            return await actorFinanicialRepo.Delete(actorFinanicial);
        }
        [HttpGet]
        [Route("GetFinancialAmountById/{financialId}")]
        public async Task<ExchangeActorFinanicial> GetFinancialAmountById(int financialId)
        {
            return await actorFinanicialRepo.GetFinancialAmountById(financialId);
        }
        [HttpGet]
        [Route("GetAllFinancialList")]
        public async Task<List<ExchangeActorFinanicialDTO>> GetAllFinancialList()
        {
            return await actorFinanicialRepo.GetAllFinancialList();
        }
        [HttpGet]
        [Route("GetListOfCustomerType/{lang}")]
        public async Task<List<FinancialPerformanceViewModel>> GetListOfCustomerType(string lang)
        {
            return await actorFinanicialRepo.GetListOfCustomerType(lang);
        }
        [HttpGet]
        [Route("GetFinancialPerformanceById/{id}")]
        public async Task<StaticData5> GetFinancialPerformanceById(int id)
        {
            return await actorFinanicialRepo.GetFinancialPerformanceById(id);
        }
    }
}