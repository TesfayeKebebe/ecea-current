﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]

    public class FinancialAuditorController : ControllerBase
    {
        private readonly MemberFinancialAuditorRepository financialAuditorRepository;
        private readonly IHostingEnvironment host;
        private readonly IConfiguration Configuration;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".jpg", ".jpeg", ".png" };
        public FinancialAuditorController(MemberFinancialAuditorRepository financialAuditorRepository,
                                          IHostingEnvironment host,
                                          IConfiguration configuration,
                                          UserManager<ApplicationUser> userManager)
        {
            this.financialAuditorRepository = financialAuditorRepository;
            this.host = host;
            Configuration = configuration;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("CreateUpload")]
        [Authorize]
        public async Task<Guid> CreateUpload([FromForm]MemberFinancialAuditorDTO memberFinancial)
        {
            try
            {
                var user = await userManager.GetUserAsync(HttpContext.User);
                memberFinancial.CreatedUserId = Guid.Parse(user.Id);
                var  result = await financialAuditorRepository.CreateUpload(memberFinancial);
                return result;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
         [HttpGet("getMemberFinancialAnnualAuditor")]
          public async Task<PagedResult<MemberFinancialAuditorView>> GetMemberFinancialAnnualAuditor([FromQuery]NotificationQueryParameters notificationQuery)
        {
            return await financialAuditorRepository.GetExechangeActorAnnulAuditor(notificationQuery);
        }

        [HttpGet("getMemberAnnulAuditor/{ExchangeActorId}")]
        public async Task<PagedResult<MemberFinancialAuditorView>> GetMemberAnnulAuditor([FromQuery] NotificationQueryParameters notificationQuery,Guid ExchangeActorId)
        {
            return await financialAuditorRepository.GetMemberAnnulAuditor(notificationQuery,ExchangeActorId);
        }
        [HttpGet("getAllMemberFinancialAuditedList")]
        public async Task<PagedResult<MemberFinancialAuditorView>> GetAllMemberFinancialAuditedList([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return await financialAuditorRepository.GetAllMemberFinancialAuditedList(notificationQuery);
        }
        [HttpPost("UpdateFinancialAuditor")]
        [Authorize]
        public async Task<int> UpdateFinancialAuditor(MemberFinancialAuditorDTO memberFinancial)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberFinancial.UpdatedUserId = Guid.Parse(user.Id);
            return await financialAuditorRepository.UpdateFinancialAuditor(memberFinancial);
        }
        [HttpGet("getOnTimeReportFinancialAuditor")]
        public async Task<List<MemberFinancialAuditorViewModel>> GetOnTimeReportFinancialAuditor([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return await financialAuditorRepository.GetOnTimeReportFinancialAuditor(notificationQuery);
        }
        [HttpGet("getNotMemberFinancialAuditor")]
        public List<MemberFinancialAuditorViewModel> GetNotMemberFinancialAuditor([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return  financialAuditorRepository.GetNotMemberFinancialAuditor(notificationQuery);
        }
        [HttpGet("getNetWorthDiffBetweenMemberAuditor")]
        public List<NetWorthBetweenMemberAuditor> GetNetWorthDiffBetweenMemberAuditor([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return financialAuditorRepository.GetNetWorthDiffBetweenMemberAuditor(notificationQuery);
        }
        [HttpGet("getLateFinancialAuditorList")]
        public async Task<List<MemberFinancialAuditorLateViewModel>> GetLateFinancialAuditorList([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return await financialAuditorRepository.GetLateFinancialAuditorList(notificationQuery);
        }
        [HttpGet]
        [Route("getMemberAnnualAuditerById/{memberClientId}")]
        public async Task<MemberFinancialAuditorView> GetMemberAnnualAuditerById(Guid memberClientId)
        {
            return await financialAuditorRepository.GetMemberAnnualAuditerById(memberClientId);
        }
        [HttpGet("getActivSuspendedCanceled")]
        public List<MemberFinancialAuditorViewModel> GetActivSuspendedCanceled([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return  financialAuditorRepository.GetActivSuspendedCanceled(notificationQuery);
        }
        [HttpGet("getTotalQuantyOfExchangeactor")]
        public List<TotalQuantyOfExchangeactor> GetTotalQuantyOfExchangeactor([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return financialAuditorRepository.GetTotalQuantyOfExchangeactor(notificationQuery);
        }
    }
}
