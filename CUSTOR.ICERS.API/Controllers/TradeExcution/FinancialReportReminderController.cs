﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class FinancialReportReminderController : ControllerBase
    {
        private readonly FinancialReportReminderRepo financialReportReminder;
        private readonly UserManager<ApplicationUser> userManager;

        public FinancialReportReminderController(FinancialReportReminderRepo _financialReportReminder,
                                                 UserManager<ApplicationUser> userManager)
        {
            financialReportReminder = _financialReportReminder;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<int> Create(FinancialReportReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.CreatedUserId = Guid.Parse(user.Id);
            return await financialReportReminder.Create(financialReport);
        }
        [HttpPost]
        [Route("Update")]
        [Authorize]
        public async Task<int> Update(FinancialReportReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.UpdatedUserId = Guid.Parse(user.Id);
            return await financialReportReminder.Update(financialReport);
        }
        [HttpPost]
        [Route("Delete")]
        [Authorize]
        public async Task<int> Delete(FinancialReportReminderDTO financialReport)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            financialReport.UpdatedUserId = Guid.Parse(user.Id);
            return await financialReportReminder.Delete(financialReport);
        }
        [HttpGet]
        [Route("GetFinancialReportReminderId/{financialReportId}")]
        public async Task<FinancialReportReminder> GetFinancialReportReminderId(int financialReportId)
        {
            return await financialReportReminder.GetFinancialReportReminderId(financialReportId);
        }
        [HttpGet]
        [Route("GetFinancialReportReminderList/{lang}")]
        public async Task<List<FinancialReportReminderDTO>> GetFinancialReportReminderList(string lang)
        {
            return await financialReportReminder.GetFinancialReportReminderList(lang);
        }
    }
}