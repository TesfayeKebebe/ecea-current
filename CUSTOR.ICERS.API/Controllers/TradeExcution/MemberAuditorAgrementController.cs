﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberAuditorAgrementController : ControllerBase
    {
        private readonly MemberAuditorRepository _memberAuditorRepository;
        public MemberAuditorAgrementController(
           MemberAuditorRepository memberAuditorRepository)
        {
            _memberAuditorRepository = memberAuditorRepository;
        }
        [HttpGet("getMemberAuditoragreementList")]
        public async Task<PagedResult<MemberAuditorAgreementView>> GetAllMemberAuditor([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await _memberAuditorRepository.GetAllMemberAuditorAgrement(notificationQuery);
        }
    }
}
