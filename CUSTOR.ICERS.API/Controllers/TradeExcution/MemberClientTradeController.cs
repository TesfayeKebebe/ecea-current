﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberClientTradeController : ControllerBase
    {
        private readonly MemberClientTradeRepository _clientTradeRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public MemberClientTradeController(MemberClientTradeRepository clientTradeRepository, UserManager<ApplicationUser> userManager)
        {
            _clientTradeRepository = clientTradeRepository;
            this.userManager = userManager;
        }

        [HttpPost]
        [Route("CreateMemberClinetReg")]
        [Authorize]
        // [Authorize(Policies.CreateClientInformation)]
        public async Task<int> CreateMemberClinetReg(TradeExcutionReportDTO memberClient)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClient.CreatedUserId = Guid.Parse(user.Id);
            return await _clientTradeRepository.CreateMemberClinetReg(memberClient);
        }

        [HttpPost]
        [Route("CreateMemberTradeDetail")]
        [Authorize]
        //[Authorize(Policies.CreateTradeExecution)]
        public async Task<int> CreateMemberTradeDetail(TradeExcutionReportDTO memberClient)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClient.CreatedUserId = Guid.Parse(user.Id);
            return await _clientTradeRepository.CreateMemberTradeDetail(memberClient);
        }
        [HttpPost]
        [Route("CreateMemberFinancial")]
        // [Authorize(Policies.CreateFinancialReport)]
        public async Task<int> CreateMemberFinancial(TradeExcutionReportDTO memberClient)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClient.CreatedUserId = Guid.Parse(user.Id);
            return await _clientTradeRepository.CreateMemberFinancial(memberClient);
        }
        [HttpPost]
        [Route("UpdateMemberTradeDetail")]
        public async Task<int> UpdateMemberTradeDetail(TradeExcutionReportDTO memberClient)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClient.UpdatedUserId = Guid.Parse(user.Id);
            return await _clientTradeRepository.UpdateMemberTradeDetail(memberClient);
        }
        [HttpGet]
        [Route("GetAllMemberClientList/{lang}")]
        public async Task<List<StaticData>> GetAllMemberClientList(string lang)
        {
            return await _clientTradeRepository.GetAllMemberClientList(lang);
        }
        [HttpGet]
        [Route("GetAllMemberClient/{memberClientTradeId}")]
        public async Task<List<MemberClientTradeDTO>> GetAllMemberClient(int memberClientTradeId)
        {
            return await _clientTradeRepository.GetAllMemberClient(memberClientTradeId);
        }

        [HttpGet]
        [Route("GetTradeExcutionNotAccomplished")]
        // [Authorize(Policies.ViewMemberTradeExecution)]
        public async Task<PagedResult<MemberClientTradeDTO>> GetTradeExcutionNotAccomplished([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetTradeExcutionNotAccomplished(clientTradeQueryParameters);
        }

        [HttpGet]
        [Route("GetMemberClientById/{memberClientId}")]
        public async Task<MemberTradeExcutionDTO> GetMemberClientById(int memberClientId)
        {
            return await _clientTradeRepository.GetMemberClientById(memberClientId);
        }

        [HttpGet]
        [Route("GetAllTardeExcutionReport")]
        public async Task<PagedResult<MemberTradeExcutionDTO>> GetAllTardeExcutionReport([FromQuery] ClientTradeQueryParameters clientTradeQuery)
        {
            return await _clientTradeRepository.GetAllTardeExcutionReport(clientTradeQuery);
        }
        [HttpGet]
        [Route("GetTradeNotReportList")]
        public List<MemberTradeExecutionViewModel> GetTradeNotReportList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return  _clientTradeRepository.GetMemberTardeNotReportList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getFinancialNotReportList")]
        public List<MemberTradeExecutionViewModel> GetFinancialNotReportList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return  _clientTradeRepository.GetMemberFinancialNotReportList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("GetLateMemberTardeList")]
        public async Task<List<MemberTradeLateReportView>> GetLateMemberTardeList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetLateMemberTardeList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getLateMemberFinancialList")]
        public async Task<List<MemberTradeLateReportView>> GetLateMemberFinancialList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetLateMemberFinancialList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getMemberFinancialOnTimeReportList")]
        public async Task<List<MemberClientTradeDTO>> GetMemberFinancialOnTimeReportList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetMemberFinancialOnTimeReportList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getMemberTradeOnTimeReportList")]
        public async Task<List<MemberClientTradeDTO>> GetMemberTradeOnTimeReportList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetMemberTradeOnTimeReportList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getListNotAccomplishedTradeComparative")]
        public  List<ComparisonOffSiteMonitoring> GetListNotAccomplishedTradeComparative([FromQuery] ComparativeQueryParameterSearch clientTradeQueryParameters)
        {
            return  _clientTradeRepository.GetListNotAccomplishedTradeComparative(clientTradeQueryParameters);
        }


        [HttpGet]
        [Route("getMemberClientInformationOnTimeReport")]
        public async Task<List<MemberClientTradeDTO>> GetMemberClientInformationOnTimeReport([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetMemberClientInformationOnTimeReport(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getLateReportedMemberClientList")]
        public async Task<List<MemberTradeLateReportView>> GetLateReportedMemberClientList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await _clientTradeRepository.GetLateReportedMemberClientList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getNotReportMemberClientList")]
        public List<MemberTradeExecutionViewModel> GetNotReportMemberClientList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return _clientTradeRepository.GetNotReportMemberClientList(clientTradeQueryParameters);
        }
        [HttpGet("getActivSuspendedCanceled")]
        public List<MemberFinancialAuditorViewModel> GetActivSuspendedCanceled([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return _clientTradeRepository.GetActivSuspendedCanceled(notificationQuery);
        }
        [HttpGet("getTotalQuantyOfExchangeactor")]
        public List<TotalQuantyOfExchangeactor> GetTotalQuantyOfExchangeactor([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return _clientTradeRepository.GetTotalQuantyOfExchangeactor(notificationQuery);
        }

        [HttpGet("getMemberClientActivSuspendedCanceled")]
        public List<MemberFinancialAuditorViewModel> GetMemberClientActivSuspendedCanceled([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return _clientTradeRepository.GetMemberClientActivSuspendedCanceled(notificationQuery);
        }
        [HttpGet("getMemberClientTotalQuantyOfExchangeactor")]
        public List<TotalQuantyOfExchangeactor> GetMemberClientTotalQuantyOfExchangeactor([FromQuery] ExchangeActorStatusSearchQuery notificationQuery)
        {
            return _clientTradeRepository.GetMemberClientTotalQuantyOfExchangeactor(notificationQuery);
        }
    }
}
