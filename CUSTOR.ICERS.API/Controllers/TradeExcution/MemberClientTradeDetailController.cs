﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberClientTradeDetailController : ControllerBase
    {
        private readonly MemberClientTradeDetailRepo tradeDetailRepo;
        private readonly UserManager<ApplicationUser> userManager;

        public MemberClientTradeDetailController(MemberClientTradeDetailRepo _tradeDetailRepo, UserManager<ApplicationUser> userManager)
        {
            tradeDetailRepo = _tradeDetailRepo;
            this.userManager = userManager;
        }
        [HttpGet]
        [Route("GetAllBoughtSelfTrade")]
        // [Authorize(Policies.ViewMemberTradeExecution)]
        public async Task<PagedResult<BoughtSelfTradeExecutionDTO>> GetAllBoughtSelfTrade([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            PagedResult<BoughtSelfTradeExecutionDTO> tradeExcution = await this.tradeDetailRepo.GetAllBoughtSelfTrade(clientTradeQueryParameters);
            return tradeExcution;
        }

        [HttpGet]
        [Route("GetAllSoldSelfTrade")]
        public async Task<List<SoldSelfTradeExecutionDTO>> GetAllSoldSelfTrade([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<SoldSelfTradeExecutionDTO> tradeExcution = await tradeDetailRepo.GetAllSoldSelfTrade(clientTradeQueryParameters);
            return tradeExcution;
        }

        [HttpGet]
        [Route("getTradeExcutionById")]
        public async Task<PagedResult<MemberTradeDetailViewModel>> GetTradeExcutionById([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await tradeDetailRepo.GetTradeExcutionById(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("GetTradeExcutionDetailById/{memberClientTradeId}")]
        public async Task<MemberTradeDetailViewModel> GetTradeExcutionDetailById(int memberClientTradeId)
        {
            return await tradeDetailRepo.GetTradeExcutionDetailById(memberClientTradeId);
        }

        [HttpPost]
        [Route("UpdateTradeExcution")]
        [Authorize]
        public async Task<int> UpdateTradeExcution(MemberClientTradeDetailDTO memberClientTradeId)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClientTradeId.UpdatedUserId = Guid.Parse(user.Id);
            return await tradeDetailRepo.UpdateTradeExcution(memberClientTradeId);
        }

        [HttpPost]
        [Route("DeleteTradeExcution")]
        [Authorize]
        public async Task<int> DeleteTradeExcution(MemberClientTradeDetailDTO memberClientTradeDetail)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberClientTradeDetail.UpdatedUserId = Guid.Parse(user.Id);
            return await tradeDetailRepo.DeleteTradeExcution(memberClientTradeDetail);
        }
        [HttpGet]
        [Route("getTradeAccomplishedComparatives")]
        public List<ComparisonOffSiteMonitoring> GetTradeAccomplishedComparatives([FromQuery] ComparativeQueryParameterSearch clientTradeQueryParameters)
        {
            return  tradeDetailRepo.GetTradeAccomplishedComparatives(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getListOfTradeExecutionOreder")]
        public List<TradeExecutionOrderViewModel> GetListOfTradeExecutionOreder([FromQuery] TradeExecutionOrderSearchQuery clientTradeQueryParameters)
        {
            return tradeDetailRepo.GetListOfTradeExecutionOreder(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getAllTradeExecutionList")]
        public PagedResult<MembersTradeExecutionReportPeriod> GetAllTradeExecutionList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return tradeDetailRepo.GetAllTradeExecutionList(clientTradeQueryParameters);
        }
        [HttpGet]
        [Route("getAllMembersTradeExecutionList")]
        public async Task<PagedResult<BoughtSelfTradeExecutionDTO>> GetAllMembersTradeExecutionList([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await tradeDetailRepo.GetAllMembersTradeExecutionList(clientTradeQueryParameters);
        }
    }
}
