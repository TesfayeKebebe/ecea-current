﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
//using AutoMapper.Configuration;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberTradeEvidenceController : ControllerBase
    {

        private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".jpg", ".jpeg", ".png" };
        private readonly MemberTradeEvidenceRepository evidenceRepository;
        private readonly IHostingEnvironment host;
        private readonly IConfiguration Configuration;
        // string message = "";

        public MemberTradeEvidenceController(
            MemberTradeEvidenceRepository _evidenceRepository,
            IHostingEnvironment host, 
            IConfiguration configuration)
        {
            evidenceRepository = _evidenceRepository;
            this.host = host;
            this.Configuration = configuration;
        }

        private async Task<int> MultipleUpload(List<IFormFile> filesData)
        {
            if (filesData != null)
            {
                foreach (var photo in filesData)
                {
                    if (!ACCEPTED_FILE_TYPES.Any(s => s == Path.GetExtension(photo.FileName).ToLower()))
                    {
                        return 0;
                    }
                    var uploadFilesPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
                    if (!Directory.Exists(uploadFilesPath))
                        Directory.CreateDirectory(uploadFilesPath);
                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(photo.FileName);
                    var filePath = Path.Combine(uploadFilesPath, fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await photo.CopyToAsync(stream);
                    }
                }

            }
            return 1;
        }
        private async Task<int> UploadFiles(IFormFile filesData)
        {
            if (filesData == null)
            {
                return 0;
            }
            if (filesData.Length == 0)
            {
                return 0;
            }
            if (filesData.Length > 10 * 1024 * 1024)
            {
                return 0;                
            }

            if (!ACCEPTED_FILE_TYPES.Any(s => s == Path.GetExtension(filesData.FileName).ToLower()))
            {
                return 0;
                
            }

            var uploadFilesPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");

            if (!Directory.Exists(uploadFilesPath))
                Directory.CreateDirectory(uploadFilesPath);

            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(filesData.FileName);
            var filePath = Path.Combine(uploadFilesPath, fileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await filesData.CopyToAsync(stream);
            }

            return 1;
            
        }
        [HttpPost]
        [Route("UploadTradeEvidence")]
        public int UploadTradeEvidence([FromForm]AttachmentPostModel attachmentModel)
        {
            //try
            //{
            //    var filesData = attachmentModel.ActualFile;
            //    var result = await MultipleUpload(filesData);

            //    var uploadFilesPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
            //    var fileName = "";
            //    var filePath = Path.Combine(uploadFilesPath, fileName);
            //    var fileUrl = Configuration["ApplicationUrl"] + "/uploads/" + fileName;
            //    var attachment = new MemberTradeEvidence
            //    {
            //        EvidenceContent = attachmentModel.AttachmentContent,
            //        CreatedDateTime = DateTime.Now,
            //        CreatedUserId = new Guid(),
            //        FileLocation = filePath,
            //        Remark = "Remark",
            //        FileType = 1,
            //        Url = fileUrl,
            //        IsActive = true,
            //        IsDeleted = false,
            //        UpdatedDateTime = DateTime.Now,
            //        UpdatedUserId = new Guid(),
            //        MemberClientTradeId = attachmentModel.ParentId,
            //        AuditorId = attachmentModel.AuditorId
            //    };
            //    result = await evidenceRepository.Create(attachment);
            //    return 1;

            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
            return 0;
        }
        [HttpGet]
        [Route("GetMemberEvidenceTrade/{tradeExcutionId}")]
       public async Task<List<AttachmentViewModel>> GetMemberEvidenceTrade(int tradeExcutionId)
        {
            return await evidenceRepository.GetAllByTradeEvidenceId(tradeExcutionId);

        }
        [HttpGet]
        [Route("GetListOfAuditor/{lang}")]
        public async Task<List<StaticData4>> GetListOfAuditor(string lang)
        {
            return await evidenceRepository.GetListOfAuditors(lang);
        }
    }
    }