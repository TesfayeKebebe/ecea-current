﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberTradeFinancialController : ControllerBase
    {
        private readonly MemberTradeFinancialRepository financialRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public MemberTradeFinancialController(MemberTradeFinancialRepository _financialRepository, UserManager<ApplicationUser> userManager)
        {
            financialRepository = _financialRepository;
            this.userManager = userManager;
        }
        [HttpGet]
        [Route("GetFinancialListById/{id}")]
        public async Task<List<MemberTradeFinancialViewModel>> GetFinancialListById(int id)
        {
            return await financialRepository.GetFinancialListById(id);
        }

        [HttpGet]
        [Route("GetMemberFinancialReport")]
        // [Authorize(Policies.ViewFinancialReport)]
        public async Task<PagedResult<MemberTradeFinancialViewModel>> GetMemberFinancialReport([FromQuery] ClientTradeQueryParameters clientTradeQueryParameters)
        {
            return await financialRepository.GetMemberFinancialReport(clientTradeQueryParameters);
        }

        [HttpPost]
        [Route("UpdateMemberFinancial")]
        [Authorize]
        public async Task<int> UpdateMemberFinancial(MemberTradeFinancialDTO memberTradeFinancial)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberTradeFinancial.CreatedUserId = Guid.Parse(user.Id);
            return await financialRepository.UpdateMemberFinancial(memberTradeFinancial);
        }
        [HttpPost]
        [Route("DeleteMemberFinancialTrade")]
        [Authorize]
        public async Task<int> DeleteMemberFinancialTrade(MemberTradeFinancialDTO memberTradeFinancial)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberTradeFinancial.CreatedUserId = Guid.Parse(user.Id);
            return await financialRepository.DeleteMemberFinancialTrade(memberTradeFinancial);
        }
        [HttpGet]
        [Route("GetMemberFinacialTradeById/{financialTradeId}")]
        public async Task<MemberTradeFinancial> GetMemberFinacialTradeById(int financialTradeId)
        {
            return await financialRepository.GetMemberFinacialTradeById(financialTradeId);
        }
        [HttpGet]
        [Route("getMemberQuaterlyFinacialTradeById")]
        public async Task<MemberTradeFinancial> GetMemberQuaterlyFinacialTradeById([FromQuery] ClientTradeQueryParameters financialTrade)
        {
            return await financialRepository.GetMemberQuaterlyFinacialTradeById(financialTrade);
        }
        [HttpGet]
        [Route("getMemberAnnuallyFinacialTradeById")]
        public async Task<MemberTradeFinancial> GetMemberAnnuallyFinacialTradeById([FromQuery] ClientTradeQueryParameters financialTrade)
        {
            return await financialRepository.GetMemberAnnuallyFinacialTradeById(financialTrade);
        }
        [HttpGet]
        [Route("GetListOfTradeFinancial")]
        public async Task<List<MemberTradeFinancialViewModel>> GetListOfTradeFinancial([FromQuery]ClientTradeQueryParameters clientTrade)
        {
            return await financialRepository.GetListOfTradeFinancial(clientTrade);
        }
        [HttpPost]
        [Route("UpdateLowBalanceExchangeActor")]
        public async Task<int> UpdateLowBalanceExchangeActor(MemberTradeFinancial tradeFinancial)
        {
            return await financialRepository.UpdateLowFinancialBalanceStatus(tradeFinancial);
        }
        [HttpGet]
        [Route("getMemberAnnualFinancialReport")]
        public async Task<List<MemberTradeFinancialViewModel>> GetMemberAnnualFinancialReport([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await financialRepository.GetMemberAnnualFinancialReport(clientTrade);
        }
        [HttpGet]
        [Route("getAnnualFinancialPerformanceReport")]
        public async Task<List<MemberTradeFinancialViewModel>> GetAnnualFinancialPerformanceReport([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await financialRepository.GetAnnualFinancialPerformanceReport(clientTrade);
        }
        [HttpGet]
        [Route("getQuartrlyFinancialReportList")]
        public async Task<List<MemberTradeFinancialViewModel>> GetQuartrlyFinancialReportList([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await financialRepository.GetQuartrlyFinancialReportList(clientTrade);
        }
        [HttpGet]
        [Route("getAnnualFinancialReportList")]
        public async Task<List<MemberTradeFinancialViewModel>> GetAnnualFinancialReportList([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await financialRepository.GetAnnualFinancialReportList(clientTrade);
        }
        [HttpGet]
        [Route("getQuarterlyFinancialComparativeList")]
        public List<ExchangeActorFinacialComparative> GetQuarterlyFinancialComparativeList([FromQuery] FinancialReportSearchQuery clientTrade)
        {
            return  financialRepository.GetQuarterlyFinancialComparativeList(clientTrade);
        }
        [HttpGet]
        [Route("getAnnualyFinancialComparativeList")]
        public List<ExchangeActorFinacialComparative> GetAnnualyFinancialComparativeList([FromQuery] FinancialReportSearchQuery clientTrade)
        {
            return financialRepository.GetAnnualyFinancialComparativeList(clientTrade);
        }
        [HttpGet]
        [Route("getQuarterFinancialNetWorthCalculationList")]
        public List<FinancialNetWorthCalculation> GetQuarterFinancialNetWorthCalculationList([FromQuery] FinancialReportSearchQuery clientTrade)
        {
            return financialRepository.GetQuarterFinancialNetWorthCalculationList(clientTrade);
        }
        [HttpGet]
        [Route("getAnnualFinancialNetWorthCalculationList")]
        public List<FinancialNetWorthCalculation> GetAnnualFinancialNetWorthCalculationList([FromQuery] FinancialReportSearchQuery clientTrade)
        {
            return financialRepository.GetAnnualFinancialNetWorthCalculationList(clientTrade);
        }
    }
}