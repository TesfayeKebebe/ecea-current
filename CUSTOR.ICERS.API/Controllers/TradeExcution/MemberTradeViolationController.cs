﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberTradeViolationController : ControllerBase
    {
        private readonly MemberTradeViolationRepository violationRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public MemberTradeViolationController(MemberTradeViolationRepository _violationRepository, 
                                                 UserManager<ApplicationUser> userManager)
        {
            violationRepository = _violationRepository;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("createViolationRecord")]
        [Authorize]
        public async Task<int> CreateViolationRecord(MemberTradeViolationDTO tradeExcution)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            tradeExcution.CreatedUserId = Guid.Parse(user.Id);
            return await violationRepository.CreateViolationRecord(tradeExcution);
        }
    }
}