﻿using System;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OffSiteMonitoringController : ControllerBase
    {
        private readonly OffSiteMonitoringRepository offSiteMonitoring;
        private readonly UserManager<ApplicationUser> userManager;

        public OffSiteMonitoringController(OffSiteMonitoringRepository offSiteMonitoring, UserManager<ApplicationUser> userManager)
        {
            this.offSiteMonitoring = offSiteMonitoring;
            this.userManager = userManager;
        }
        [HttpPost("createOffSite")]
        [Authorize]
        public async Task<int> CreateOffSite(OffSiteMonitoringReportDTO offSite)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            offSite.CreatedUserId = Guid.Parse(user.Id);
            return await offSiteMonitoring.CreateOffSite(offSite);
        }
    }
}
