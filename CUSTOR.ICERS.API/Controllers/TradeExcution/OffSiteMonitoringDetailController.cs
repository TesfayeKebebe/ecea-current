﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class OffSiteMonitoringDetailController : ControllerBase
    {
        private readonly OffSiteMonitoringDetailRepository offSiteMonitoringDetail;
        private readonly UserManager<ApplicationUser> userManager;

        public OffSiteMonitoringDetailController(OffSiteMonitoringDetailRepository offSiteMonitoringDetail,
                                                  UserManager<ApplicationUser> userManager)
        {
            this.offSiteMonitoringDetail = offSiteMonitoringDetail;
            this.userManager = userManager;
        }
        [HttpGet("getOffSiteReportViewList")]
        public async Task<List<ExchangeActorOffSiteReportView>> GetOffSiteReportViewList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await offSiteMonitoringDetail.GetOffSiteReportViewList(notificationQuery);
        }
        [HttpGet("getExchangeActorOffSiteViolationList")]
        public async Task<PagedResult<ExchangeActorOffSiteReportView>> GetExchangeActorOffSiteViolationList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await offSiteMonitoringDetail.GetExchangeActorOffSiteViolationList(notificationQuery);
        }
        [HttpGet("getExchangeActorAnnualViolationList")]
        public async Task<PagedResult<ExchangeActorOffSiteReportView>> GetExchangeActorAnnualViolationList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await offSiteMonitoringDetail.GetExchangeActorAnnualViolationList(notificationQuery);
        }
        [HttpPost]
        [Route("updateExchangeActorOffSiteStatus")]
        [Authorize]
        public async Task<Guid> UpdateExchangeActorOffSiteStatus(OffSetMonitoringWarningStatus postModel)
        {
            return await offSiteMonitoringDetail.UpdateExchangeActorOffSiteStatus(postModel.ExchangeActorId,postModel.ReportTypeId, postModel.ReportPeriodId,
                                                                                   postModel.Year, postModel.ReasonTypeId, postModel.StatusViewRecord);
        }
        [HttpGet]
        [Route("getTotalMembersWarningStatus")]
        public async Task<int> GetTotalMembersWarningStatus(OffSetMonitoringWarningStatus postModel)
        {
            return await offSiteMonitoringDetail.GetTotalMembersWarningStatus(postModel.ExchangeActorId, postModel.ReportTypeId,
                                                                                  postModel.ReportPeriodId, postModel.Year, postModel.ReasonTypeId);
        }
        [HttpGet]
        [Route("getOffSiteMonitoringComprisonList")]
        public List<ComparisonOffSiteMonitoring> GetOffSiteMonitoringComprisonList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.OffSiteMonitoringComprisonList(offSite);
        }
        [HttpGet]
        [Route("getDuplicateReportingViolationList")]
       public List<DepulicateMemberReportingViolation> GetDuplicateReportingViolationList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetDuplicateReportingViolation(offSite);
        }
        [HttpGet]
        [Route("getDuplicateReportingAnnualViolation")]
        public List<DepulicateMemberReportingViolation> GetDuplicateReportingAnnualViolation([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetDuplicateReportingAnnualViolation(offSite);
        }
        [HttpGet]
        [Route("getReportAccomplishResponsibilityList")]
        public List<ReportAccomplishedResposiblity> GetReportAccomplishResponsibilityList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetReportAccomplishResponsibilityList(offSite);
        }

        [HttpGet]
        [Route("getListOfExchangeActorReportList")]
        public List<MemberAnnualFinancialAuditorViewModel> GetListOfExchangeActorReportList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetListOfExchangeActorReportList(offSite);
        }
        [HttpGet]
        [Route("getNoOfExchangeactorReportStatus")]
        public List<TradeExecutionAmountViewModel> GetNoOfExchangeactorReportStatus([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetNoOfExchangeactorReportStatus(offSite);
        }
        [HttpGet]
        [Route("getQuarterlyFinancialNoOfExchangeactorReport")]
        public List<TradeExecutionAmountViewModel> GetQuarterlyFinancialNoOfExchangeactorReport([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetQuarterlyFinancialNoOfExchangeactorReport(offSite);
        }
        [HttpGet]
        [Route("getQuarterlyTradeNoOfExchangeactorReport")]
        public List<TradeExecutionAmountViewModel> GetQuarterlyTradeNoOfExchangeactorReport([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetQuarterlyTradeNoOfExchangeactorReport(offSite);
        }
        [HttpGet]
        [Route("getQuarterlyClientNoOfExchangeactorReport")]
        public List<TradeExecutionAmountViewModel> GetQuarterlyClientNoOfExchangeactorReport([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetQuarterlyClientNoOfExchangeactorReport(offSite);
        }

        [HttpGet]
        [Route("getReportClientAccomplishResponsibilityList")]
        public List<ReportAccomplishedResposiblity> GetReportClientAccomplishResponsibilityList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetReportClientAccomplishResponsibilityList(offSite);
        }
        [HttpGet]
        [Route("getDuplicateReportingClientViolation")]
        public List<DepulicateMemberReportingViolation> GetDuplicateReportingClientViolation([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.GetDuplicateReportingClientViolation(offSite);
        }
        [HttpGet]
        [Route("offSiteMonitoringClientComprisonList")]
        public List<ComparisonOffSiteMonitoring> OffSiteMonitoringClientComprisonList([FromQuery] ComparativeQueryParameterSearch offSite)
        {
            return offSiteMonitoringDetail.OffSiteMonitoringClientComprisonList(offSite);
        }
    }
}
