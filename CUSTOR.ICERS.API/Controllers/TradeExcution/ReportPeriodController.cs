﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ReportPeriodController : ControllerBase
    {
        private readonly ReportPeriodRepository periodRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public ReportPeriodController(ReportPeriodRepository _periodRepository,
                                      UserManager<ApplicationUser> userManager)
        {
            periodRepository = _periodRepository;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("Create")]
        [Authorize]
        public async Task<int> Create(ReportPeriodSettingViewModel reportPeriod)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            reportPeriod.CreatedUserId = Guid.Parse(user.Id);
            return await periodRepository.Create(reportPeriod);
        }
        [HttpGet]
        [Route("GetAllReportPeriod/{lang}")]
        public async Task<IEnumerable<ReportPeriodDTO>> GetAllReportPeriod(string lang)
        {
            return await periodRepository.GetAllReportPeriod(lang);
        }
        [HttpGet]
        [Route("GetAllReportPeriodList/{lang}")]
        public async Task<List<ReportPeriodViewModel>> GetAllReportPeriodList(string lang)
        {
            return await periodRepository.GetAllReportPeriodList(lang);
        }

        [HttpGet]
        [Route("GetReportPeriodById/{reportPeriodId}")]
        public async Task<ReportPeriod> GetReportPeriodById(int reportPeriodId)
        {
            return await periodRepository.GetReportPeriodById(reportPeriodId);
        }
        [HttpPost]
        [Route("Update")]
        [Authorize]
        public async Task<int> Update(ReportPeriodSettingViewModel reportPeriod)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            reportPeriod.UpdatedUserId = Guid.Parse(user.Id);
            return await periodRepository.Update(reportPeriod);
        }
        [HttpPost]
        [Route("Delete")]
        [Authorize]
        public async Task<int> Delete(ReportPeriodViewModel reportPeriod)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            reportPeriod.UpdatedUserId = Guid.Parse(user.Id);
            return await periodRepository.Delete(reportPeriod);
        }
    }
}