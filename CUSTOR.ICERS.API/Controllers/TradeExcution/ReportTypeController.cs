﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class ReportTypeController : ControllerBase
    {
        private readonly ReportTypeRepository _reportTypeRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public ReportTypeController(ReportTypeRepository reportTypeRepository,
                                    UserManager<ApplicationUser> userManager)
        {
            _reportTypeRepository = reportTypeRepository;
            this.userManager = userManager;
        }
        [HttpGet("{lang}")]
        public async Task<IEnumerable<ReportTypeDTO>> GetReportTypes(string lang)
        {
            return await _reportTypeRepository.GetReportTypes(lang);
        }
        [HttpGet("getallreporttypes")]
        public async Task<IEnumerable<ReportTypeviewDTO>> GetAllReportTypes()
        {
            return await _reportTypeRepository.GetallReportTypes();
        }
        [HttpPost]
        [Route("Create")]
        public async Task<int> Create(ReportTypeviewDTO ReportType)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            ReportType.CreatedUserId = Guid.Parse(user.Id);
            return await _reportTypeRepository.Create(ReportType);
        }
      
        [HttpGet]
        [Route("GetReportTypeById/{ReportTypeId}")]
        public async Task<ReportType> GetReportTypeById(int ReportTypeId)
        {
            return await _reportTypeRepository.GetReportTypeById(ReportTypeId);
        }
        [HttpPost]
        [Route("Update")]
        public async Task<int> Update(ReportTypeviewDTO ReportType)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            ReportType.UpdatedUserId = Guid.Parse(user.Id);
            return await _reportTypeRepository.Update(ReportType);
        }
        [HttpPost]
        [Route("Delete")]
        public async Task<int> Delete(ReportTypeviewDTO ReportType)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            ReportType.UpdatedUserId = Guid.Parse(user.Id);
            return await _reportTypeRepository.Delete(ReportType);
        }
    }
}