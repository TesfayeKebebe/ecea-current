﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeExcutionStatusController : ControllerBase
    {
        private readonly TradeExcutionStatusTypeRepo statusTypeRepo;

        public TradeExcutionStatusController(TradeExcutionStatusTypeRepo _statusTypeRepo)
        {
            statusTypeRepo = _statusTypeRepo;
        }
        [HttpGet("{lang}")]
        public async Task<IEnumerable<TradeExcutionStatusTypeDTO>> GetExcutionStatusList(string lang)
        {
            return await statusTypeRepo.GetExcutionStatusList(lang);
        }
    }
}