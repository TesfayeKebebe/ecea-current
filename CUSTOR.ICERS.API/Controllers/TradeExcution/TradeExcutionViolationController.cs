﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeExcutionViolationController : ControllerBase
    {
        private readonly TradeExcutionViolationRecordeRepo tradeExcutionViolation;
        private readonly UserManager<ApplicationUser> userManager;

        public TradeExcutionViolationController(TradeExcutionViolationRecordeRepo _tradeExcutionViolation,
                                                 UserManager<ApplicationUser> userManager)
        {
            tradeExcutionViolation = _tradeExcutionViolation;
            this.userManager = userManager;
        }
        [HttpPost]
        [Route("CreateViolationRecord")]
        [Authorize]
        public async Task<Guid> CreateViolationRecord(TradeExcutionViolationRecordDTO tradeExcution)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            tradeExcution.CreatedUserId = Guid.Parse(user.Id);
            return await tradeExcutionViolation.CreateViolationRecord(tradeExcution);
        }
        [HttpPost]
        [Route("UpdateViolationRecordStatus")]
        [Authorize]
        public async Task<Guid> UpdateViolationRecordStatus(ViolationRecordStatusChangeDTO postModel)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            postModel.CreatedUserId = Guid.Parse(user.Id);
            return await tradeExcutionViolation.UpdateViolationRecordStatus(postModel.ExchangeActorId);
        }
        [HttpGet]
        [Route("GetListOfViolationById/{exchangeActorId}/{lang}")]
        public async Task<List<TradeExcutionViolationRecordView>> GetListOfViolationById(int exchangeActorId, string lang)
        {
            return await tradeExcutionViolation.GetListOfViolationById(exchangeActorId, lang);
        }
        [HttpGet]
        [Route("getAllViolation/{memberclientId}/{lang}")]
        public async Task<List<TradeExcutionViolationRecordView>> GetAllViolation(Guid memberclientId, string lang)
        {
            return await tradeExcutionViolation.GetListOfViolatedMembers(memberclientId, lang);
        }
        [HttpGet]
        [Route("getMembersViolationDecisionList/{memberclientId}/{lang}")]
        public async Task<List<TradeExcutionViolationRecordView>> GetMembersViolationDecisionList(Guid memberclientId, string lang)
        {
            return await tradeExcutionViolation.GetMembersViolationDecisionList(memberclientId, lang);
        }
        [HttpGet]
        [Route("getListOfExchangeActor/{lang}")]
        public async Task<List<StaticData4>> GetListOfExchangeActor(string lang)
        {
            return await tradeExcutionViolation.GetListOfExchangeActor(lang);
        }
        [HttpGet]
        [Route("getMemberViolationById")]
        public async Task<int> GetMemberViolationById(int memberViolationId)
        {
            return await tradeExcutionViolation.GetMemberViolationById(memberViolationId);
        }
        [HttpGet]
        [Route("getTradeViolationRecordById/{memberViolationId}")]
        public async Task<TradeExcutionViolationRecord> GetTradeViolationRecordById(int memberViolationId)
        {
            return await tradeExcutionViolation.GetTradeViolationRecordById(memberViolationId);
        }
        [HttpGet]
        [Route("getMembersOffSiteDecisionList")]
        public async Task<List<TradeExcutionViolationRecordView>> GetMembersOffSiteDecisionList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await tradeExcutionViolation.GetMembersOffSiteDecisionList(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteDecisionPreparedList")]
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionPreparedList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await tradeExcutionViolation.GetOffSiteDecisionPreparedList(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteDecisionOfficerList")]
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionOfficerList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await tradeExcutionViolation.GetOffSiteDecisionOfficerList(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteDecisionApproverList")]
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionApproverList([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await tradeExcutionViolation.GetOffSiteDecisionApproverList(notificationQuery);
        }
        [HttpGet]
        [Route("getOffSiteTeamLeaderDecision")]
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteTeamLeaderDecision([FromQuery] OffSiteMonitoringQueryParameters notificationQuery)
        {
            return await tradeExcutionViolation.GetOffSiteTeamLeaderDecision(notificationQuery);
        }
        [HttpPost]
        [Route("updateMemberViolation")]
        [Authorize]
        public async Task<int> UpdateMemberViolation(TradeExcutionViolationRecordDTO memberViolationId)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberViolationId.CreatedUserId = Guid.Parse(user.Id);
            return await tradeExcutionViolation.UpdateMemberViolation(memberViolationId);
        }
        [HttpPost]
        [Route("DeleteMemberViolation")]
        [Authorize]
        public async Task<int> DeleteMemberViolation(TradeExcutionViolationRecordDTO memberViolationId)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            memberViolationId.CreatedUserId = Guid.Parse(user.Id);
            return await tradeExcutionViolation.DeleteTradeViolation(memberViolationId);
        }
        [HttpGet]
        [Route("getMemberViolationBySearch")]
        public async Task<List<TradeExcutionViolationRecordView>> GetMemberViolationBySearch([FromQuery]ClientTradeQueryParameters clientTrade)
        {
            return await tradeExcutionViolation.GetMemberViolationByCriteria(clientTrade);
        }

        [HttpGet]
        [Route("getMemberViolationTotalNumber")]
        public async Task<int> GetMemberViolationTotalNumber([FromQuery] ClientTradeQueryParameters clientTrade)
        {
            return await tradeExcutionViolation.GetMemberViolationTotalNumber(clientTrade);
        }
    }
}