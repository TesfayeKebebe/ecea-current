﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class UnitMeasurementController : ControllerBase
    {
        private readonly UnitMeasurementRepository measurementRepository;

        public UnitMeasurementController(UnitMeasurementRepository _measurementRepository)
        {
            measurementRepository = _measurementRepository;
        }
        [HttpGet("{lang}")]
        public async Task<IEnumerable<StaticData>> GetUserMeasurementList(string lang)
        {
            return await measurementRepository.GetUserMeasurementList(lang);
        }
    }
}