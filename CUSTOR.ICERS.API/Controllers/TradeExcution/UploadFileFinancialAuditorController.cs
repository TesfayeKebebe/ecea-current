﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class UploadFileFinancialAuditorController : ControllerBase
    {
        private readonly FinancialAuditoredFileUploadRepository financialAuditored;
        private readonly UserManager<ApplicationUser> userManager;

        public UploadFileFinancialAuditorController(FinancialAuditoredFileUploadRepository financialAuditored, UserManager<ApplicationUser> userManager)
        {
            this.financialAuditored = financialAuditored;
            this.userManager = userManager;
        }
        [HttpGet("getListOfAuditoredUploadFile/{memberFinancialAuditorId}")]
        public async Task<List<FinancialAuditoredFileUploadView>> GetListOfAuditoredUploadFile(string memberFinancialAuditorId)
        {
            return await financialAuditored.ListUploadFileOfAuditored(memberFinancialAuditorId);
        }
        [HttpGet("getOneUploadFileOfAuditored/{memberFinancialAuditorId}")]
        public async Task<List<FinancialAuditoredFileUploadView>> OneUploadFileOfAuditored(int memberFinancialAuditorId)
        {
            return await financialAuditored.OneUploadFileOfAuditored(memberFinancialAuditorId);
        }
        [HttpGet("getAllAnnualAuditedDocument")]
        public async Task<PagedResult<AuditedAnnualDocument>> GetAllAnnualAuditedDocument([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return await financialAuditored.GetAllAnnualAuditedDocument(notificationQuery);
        }
        [HttpGet("getMemberAnnualFileUploadReport")]
        public async Task<List<AuditedAnnualDocument>> GetMemberAnnualFileUploadReport([FromQuery] ClientTradeQueryParameters notificationQuery)
        {
            return await financialAuditored.GetMemberAnnualFileUploadReport(notificationQuery);
        }
        [HttpPost]
        [Route("deleteAuditorFileUpload")]
        [Authorize]
        public async Task<int> DeleteAuditorFileUpload(FinancialAuditoredFileUploadDTO fileUpload)
        {
            var user = await userManager.GetUserAsync(HttpContext.User);
            fileUpload.CreatedUserId = Guid.Parse(user.Id);
            return await financialAuditored.DeleteAuditorFileUpload(fileUpload);
        }
    }
}
