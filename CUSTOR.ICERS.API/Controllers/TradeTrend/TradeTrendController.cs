﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.DataAccessLayer.Trade;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Trade;
using CUSTOR.ICERS.Core.EntityLayer.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Trade
{
    [Route("api/tradetrend")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeTrendController : ControllerBase
    {

        private readonly TradeTrendRepository _tradeRepository;

        public TradeTrendController(TradeTrendRepository tradeRepository)
        {
            _tradeRepository = tradeRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<TradeTrend>> GetTradeInformations()
        {
            return await _tradeRepository.GetTradeTrend();
        }
        [HttpGet("GetFilteredStock")]
        public async Task<IActionResult> GetFilteredStock([FromQuery] TrendQueryParameters queryParameters)
        {
            IEnumerable<TradeTrend> result = await _tradeRepository.GetFilteredStockAsync(queryParameters);
            return new OkObjectResult(result);
        }
        //[HttpGet("GetFilteredStockWithPaging")]
        //public async Task<IActionResult> GetFilteredStockWithPaging([FromQuery] TrendQueryParameters queryParameters)
        //{
        //    PagedResult<TradeTrend> result = await _tradeRepository.GetFilteredStockWithPagingAsync(queryParameters);
        //    return new OkObjectResult(result);
        //}
    }
}
