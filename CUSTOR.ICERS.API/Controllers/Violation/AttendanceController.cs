using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
//using DevExpress.Data;
//using DevExpress.Office.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        public readonly AttendanceRepository _attendanceRepository;
        public AttendanceController(AttendanceRepository attendanceRepository)
        {
            _attendanceRepository = attendanceRepository;
        }
        [HttpGet]
        public async Task<List<AttendanceVM>> GetAttendances(DateTime From, DateTime To , bool IsTrade, Guid commodityId, string CommodityGradeId=null)
    {
            return await _attendanceRepository.getAttendance(From, To,IsTrade, CommodityGradeId, commodityId);
        }
        [HttpGet("getAttendanceBuyerSellerList")]
        public async Task<List<AttendanceBuyerSellerViewModel>> GetAttendanceBuyerSellerList(DateTime From, DateTime To, bool IsTrade,Guid CommodityId)
        {
            return await _attendanceRepository.GetAttendanceBuyerSellerList(IsTrade,From, To, CommodityId);
        }
        [HttpGet("getAttendanceVolumeValueList")]
        public async Task<List<AttendanceVolumeValueRatioView>> GetAttendanceVolumeValueList(bool IsTrade, DateTime From, DateTime To,Guid CommodityId)
        {
            return await _attendanceRepository.GetAttendanceVolumeValueList(IsTrade,From, To, CommodityId);
        }
        [HttpGet("getAttendanceRatioGraphList")]
        public async Task<List<AttendanceRatio>> GetAttendanceRatioGraphList(bool IsTrade, DateTime From, DateTime To,Guid CommodityId)
        {
            return await _attendanceRepository.GetAttendanceRatioGraphList(IsTrade, From, To, CommodityId);
        }
    }
}
