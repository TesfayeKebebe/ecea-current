using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuctionController : ControllerBase
    {
    private readonly AuctionRepository _auctionRepository;
    public AuctionController(AuctionRepository auctionRepository)
    {
      _auctionRepository = auctionRepository;
    }
    [HttpGet]
    public async Task<List<AuctionVM>> getbidoffers(DateTime From, DateTime To, bool IsAllOrder, decimal Price, string type)
    {
      return await _auctionRepository.getbidoffers(From, To, IsAllOrder, Price, type);
    }
    }
}
