using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class BidOfferRecordController : ControllerBase
    {
    private readonly BidOfferRecordRepository _repository;
    public BidOfferRecordController(BidOfferRecordRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<List<EcxOrderVm>> GetBidOfferRecords(DateTime from, DateTime to, bool IsAllOrder, decimal Price, string Type)
    {
      return await _repository.getbidoffers(from, to, IsAllOrder, Price, Type);
    }
    [HttpGet("Investigation")]
    public async Task<List<EcxOrderVm>>  GetInvestigation(DateTime from, DateTime to)
    {
      return await _repository.getOrderInvestigation(from, to);
    }
    }
}
