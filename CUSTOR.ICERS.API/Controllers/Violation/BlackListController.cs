﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation.Penalty;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/blacklist")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class BlackListController : Controller
    {
        private readonly BlackListRepository _BlackListRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public BlackListController(BlackListRepository BlackListRepo, UserManager<ApplicationUser> userManager)
        {
            _BlackListRepo = BlackListRepo;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> SaveBlackList([FromBody] BlackListDto postedBlackList)
        { 
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedBlackList.CreatedUserId = Guid.Parse(user?.Id);
            return await _BlackListRepo.PostBlackList(postedBlackList);
        }

        [HttpGet]
        [Authorize]
        public async Task<PagedResult<BlackListViewDto>> GetBlackList([FromQuery] BlackListQueryParameters QueryParameters)
        {
            return await _BlackListRepo.GetBlackLists(QueryParameters);
        }

        [HttpGet("{lang}/{id}")]
        public async Task<BlackListViewDto> GetBlackListById(string lang, int id)
        {
            return await _BlackListRepo.GetBlackListById(lang, id);
        }

    }
}