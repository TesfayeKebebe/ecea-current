using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonViolationController : ControllerBase
    {
    private readonly CommonViolationRepository _repository;
    public CommonViolationController(CommonViolationRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<List<EcxTradeVM>> GetOrderAndTrades(DateTime from, DateTime to, decimal VolumLimit, int percentage, bool IsAllTrade, string Type)
    {
      return await _repository.getTrade(from, to, VolumLimit, percentage, IsAllTrade, Type);
    }
    [HttpGet("Investigation")]
    public async Task< List<EcxTradeVM>> GetTradeInvestagation(DateTime from, DateTime to)
    {
      return await _repository.getTradeInvestigation(from, to);
    }
    [HttpGet("GetById")]
    public async Task<CommonViolationDetail> GetByOrders(DateTime From, DateTime To, Guid OrderId, decimal VolumLimit, int percentage, string type)
    {
      return  await _repository.getOrdersTradeById(From, To, OrderId, VolumLimit, percentage, type);
    }
    [HttpPut]
    public async Task<bool> updateViolationStatus(ViolationStatus violation)
    {
      return await _repository.updateViolationStatus(violation);
    }
      [HttpGet("ViolationList")]
     public async Task<ViolationType> getViolationList(string violationId)
    {
      return await _repository.GetViolationStatus(violationId);
    }
  }
}
