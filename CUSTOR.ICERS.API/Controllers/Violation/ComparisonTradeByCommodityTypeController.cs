﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ComparisonTradeByCommodityTypeController : ControllerBase
    {
        private readonly ComparisonTradeByCommodityTypeRepo tradeByCommodityTypeRepo;

        public ComparisonTradeByCommodityTypeController(ComparisonTradeByCommodityTypeRepo tradeByCommodityTypeRepo)
        {
            this.tradeByCommodityTypeRepo = tradeByCommodityTypeRepo;
        }
        [HttpGet("getComparisonTradeByCommodityTypeList")]
        public async Task<List<ComparisonTradeByCommodityTypeView>> GetComparisonTradeByCommodityTypeList([FromQuery] ComparisonSearchSqlQuery comparisonSearchSql)
        {
            return await tradeByCommodityTypeRepo.GetComparisonTradeByCommodityTypeList(comparisonSearchSql);
        }
        [HttpGet("getTradeComparisonByCommodityGradeList")]
        public async Task<List<TradeComparisonByCommodityGradeView>> GetTradeComparisonByCommodityGradeList([FromQuery] ComparisonSearchSqlQuery comparisonSearchSql)
        {
            return await tradeByCommodityTypeRepo.GetTradeComparisonByCommodityGradeList(comparisonSearchSql);
        }
    }
}
