using System.Collections.Generic;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractMarketSharesController : ControllerBase
    {
        private readonly ContractMarketSharesRepository _repository;

        public ContractMarketSharesController(ContractMarketSharesRepository repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<List<MarketShareDetail>> MarketShareDetails([FromQuery] ContratMarketParamter searchParameter)
        {
            return await _repository.MarketShareDetails(searchParameter);
        }
        [HttpGet("warehouse")]
        public async Task<List<WarehouseMarketShareDetail>> WarehouseMarketShareDetails([FromQuery] ContratMarketParamter searchParameter)
        {
            return await _repository.WarehouseMarketShareDetails(searchParameter);
        }
        [HttpGet("warehouse/commodit")]
        public async Task<List<WarehouseMarketShareDetail>> CommodityWarehouseMarketShareDetails([FromQuery] ContratMarketParamter searchParameter)
        {
            return await _repository.CommodityWarehouseMarketShareDetails(searchParameter);
        }
        [HttpGet("marketshare/commodity")]
        public async Task<List<MarketShareDetail>> CommodityContractMarketShareDetails([FromQuery] ContratMarketParamter searchParameter)
        {
            return await _repository.CommodityMarketShareDetails(searchParameter);
        }
    }
}
