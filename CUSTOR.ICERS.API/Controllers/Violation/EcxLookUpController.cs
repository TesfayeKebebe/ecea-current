using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class EcxLookUpController : ControllerBase
    {
        private readonly EcxLookUpRepository _lookUpRepository;

        public EcxLookUpController(EcxLookUpRepository lookUpRepository)
        {
            _lookUpRepository = lookUpRepository;
        }
        [HttpGet("getListOfCommodityGrade")]
        public async Task<List<StaticData4>> GetListOfCommodityGrade()
        {
            return await _lookUpRepository.GetListOfCommodityGrade();
        }
        [HttpGet("getListOfWarehouse")]
        public async Task<List<StaticData4>> GetListOfWarehouse()
        {
            return await _lookUpRepository.GetListOfWarehouse();
        }
        [HttpGet("getListOfCommodity")]
        public async Task<List<StaticData4>> GetListOfCommodity()
        {
            return await _lookUpRepository.GetListOfCommodity();
        }
    [HttpGet("GetCommodityGrade")]
    public async Task<List<CommodityGradeLookUp>> GetGradeLookUps()
    {
      return await _lookUpRepository.getCommoditGrade();
    }
    }
}
