using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlashCrashController : ControllerBase
    {
    private readonly FlashCrashRepository _repository;

    public FlashCrashController(FlashCrashRepository repository)
    {
     _repository = repository;
    }
    [HttpGet]
    public async Task<List<FlashCrashDetail>> GetFlashCrashes(Guid? CommodityGrade, DateTime TradeDate)
    {
      return await _repository.FlashCrashes(CommodityGrade, TradeDate);
    }
    }
}
