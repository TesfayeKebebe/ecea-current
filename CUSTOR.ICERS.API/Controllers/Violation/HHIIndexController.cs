﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class HHIIndexController : ControllerBase
    {
        private readonly HHIIndexRepository _hIIndexRepository;

        public HHIIndexController(HHIIndexRepository hIIndexRepository)
        {
            _hIIndexRepository = hIIndexRepository;
        }
        [HttpGet("getSellerHHIIndexList")]
        public async Task<List<HHIIndexViewModel>> GetSellerHHIIndexList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetSellerHHIIndexList(searchSqlQuery);
        }
        [HttpGet("getBuyerHHIIndexList")]
        public async Task<List<HHIIndexViewModel>> GetBuyerHHIIndexList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetBuyerHHIIndexList(searchSqlQuery);
        }
        [HttpGet("getSellerMarketShareList")]
        public async Task<List<TopMarketShareViewModel>> GetSellerMarketShareList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetSellerMarketShareList(searchSqlQuery);
        }
        [HttpGet("getBuyerMarketShareList")]
        public async Task<List<TopMarketShareViewModel>> GetBuyerMarketShareList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetBuyerMarketShareList(searchSqlQuery);
        }
        [HttpGet("getSellerMarketShareListGraph")]
        public async Task<List<TopMarketShareViewModel>> GetSellerMarketShareListGraph([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetSellerMarketShareListGraph(searchSqlQuery);
        }
        [HttpGet("getBuyerMarketShareListGraph")]
        public async Task<List<TopMarketShareViewModel>> GetBuyerMarketShareListGraph([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _hIIndexRepository.GetBuyerMarketShareListGraph(searchSqlQuery);
        }
    }
}
