﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class MemberNotRenewedController : ControllerBase
    {
        private readonly MemberNotRenewedRepository renewedRepository;

        public MemberNotRenewedController(MemberNotRenewedRepository renewedRepository)
        {
            this.renewedRepository = renewedRepository;
        }
        [HttpGet]
        public async Task<List<NotRegistered>> GetMembersNotRenewedList()
        {
            return await renewedRepository.GetMembersNotRenewedList();
        }
    }
}
