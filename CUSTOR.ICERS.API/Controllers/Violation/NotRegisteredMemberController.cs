using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotRegisteredMemberController : ControllerBase
    {
    private readonly NotRegisteredMemberRepository _notRegisteredMemberRepo;

    public NotRegisteredMemberController(NotRegisteredMemberRepository notRegisteredMemberRepository)
    {
      _notRegisteredMemberRepo = notRegisteredMemberRepository;
    }
    [HttpGet]
    public async Task<List<NotRegistered>> GetNotRegistereds(DateTime From, DateTime To)
    {
      return await _notRegisteredMemberRepo.GetNotRegistered(From,To);
    }
    }
}
