﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation.Penalty;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/penalty")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class PenaltyController : Controller
    {
        private readonly PenaltyRepository _penaltyRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public PenaltyController(PenaltyRepository penaltyRepo, UserManager<ApplicationUser> userManager)
        {
            _penaltyRepo = penaltyRepo;
            _userManager = userManager;
        }

        [HttpPost]
        [Authorize]
        public async Task<int> SavePenalty([FromBody] PenaltyDto postedPenalty)
        { 
            var user = await _userManager.GetUserAsync(HttpContext.User);
            postedPenalty.CreatedUserId = Guid.Parse(user?.Id);
            return await _penaltyRepo.PostPenalty(postedPenalty);
        }

        [HttpGet]
        [Authorize]
        public async Task<PagedResult<PenaltyViewDto>> GetPenalty([FromQuery] PenaltyQueryParameters QueryParameters)
        {
            return await _penaltyRepo.GetPenaltys(QueryParameters);
        }

        [HttpGet("{lang}/{id}")]
        public async Task<PenaltyViewDto> GetPenaltyById(string lang, int id)
        {
            return await _penaltyRepo.GetPenaltyById(lang, id);
        }

    }
}