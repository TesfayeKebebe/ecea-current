using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceAndQuantitySummuryController : ControllerBase
    {
    private readonly PriceAndQuantitySummuryRepository _repository;

    public PriceAndQuantitySummuryController(PriceAndQuantitySummuryRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<summuryOfQuantityPriceDetail>   PriceAndQuantitySummuries(DateTime From, DateTime To)
    {
      return await _repository.PriceAndQuantities(From, To);
    }
    }
}
