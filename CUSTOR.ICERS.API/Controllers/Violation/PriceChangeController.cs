using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceChangeController : ControllerBase
    {
    public readonly PriceChangeRepository _priceChangeRepository;
    public PriceChangeController(PriceChangeRepository priceChangeRepository)
    {
      _priceChangeRepository = priceChangeRepository;
    }
    /// <summary>
    /// get price changes by date range
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<PriceChangeVM>>  PriceChanges(DateTime from, DateTime to)
    {
      return await _priceChangeRepository.getPriceChange(from, to);
    }
    }
}
