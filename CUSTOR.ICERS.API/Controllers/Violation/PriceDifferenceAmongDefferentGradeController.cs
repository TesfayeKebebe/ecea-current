﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class PriceDifferenceAmongDefferentGradeController : ControllerBase
    {
        private readonly PriceDifferenceAmongDefferentGradeRepo _priceDifferenceAmongDefferent;

        public PriceDifferenceAmongDefferentGradeController(PriceDifferenceAmongDefferentGradeRepo priceDifferenceAmongDefferent)
        {
            _priceDifferenceAmongDefferent = priceDifferenceAmongDefferent;
        }
        [HttpGet]
        public async Task<List<PriceDifferenceAmongDiffGradeView>> GetPriceDifferenceAmongGradeList([FromQuery]PriceDifferenceSqlSearch searchParms)
        {
            return await _priceDifferenceAmongDefferent.GetPriceDifferenceAmongGradeList(searchParms);
        }
        }
}
