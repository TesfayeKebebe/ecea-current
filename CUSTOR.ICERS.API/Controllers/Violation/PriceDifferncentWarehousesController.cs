using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceDifferncentWarehousesController : ControllerBase
    {
    private readonly PriceDifferncentWarehousesRepository _repository;

    public PriceDifferncentWarehousesController(PriceDifferncentWarehousesRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<List<WarehouseDetail>> GetWarehouseDetails(DateTime From, DateTime To, string WareHouseFrom, string WareHouseTo, decimal Price,string Grade)
    {
      return await _repository.getWareHouse(From, To, WareHouseFrom, WareHouseTo, Price, Grade);
    }
    [HttpGet("WareHouse")]
    public async Task<List<WareHouse>> GetWareHouses()
    {
      return await _repository.getAllWareHouse();
    }
    }
}
