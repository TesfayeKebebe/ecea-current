﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class ReceiptHoldingSellOrderController : ControllerBase
    {
        private readonly ReceiptHoldingSellOrderRepository _receiptHoldingSellOrder;

        public ReceiptHoldingSellOrderController(ReceiptHoldingSellOrderRepository receiptHoldingSellOrder)
        {
            _receiptHoldingSellOrder = receiptHoldingSellOrder;
        }
        [HttpGet]
        public async Task<ReceiptHoldingSellOrderView> GetReceiptHoldingSellOrderList([FromQuery] SearchParmsQuery searchParms)
        {
            return await _receiptHoldingSellOrder.GetReceiptHoldingSellOrderList(searchParms);
        }
        [HttpGet("getReceiptHoldingSellOrderGraph")]
        public async Task<List<ReceiptHoldingSellOderGraph>> GetReceiptHoldingSellOrderGraph([FromQuery] SearchParmsQuery searchParms)
        {
            return await _receiptHoldingSellOrder.GetReceiptHoldingSellOrderGraph(searchParms);
        }
    }
}
