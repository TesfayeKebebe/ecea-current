using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class SisterCompanyViolationController : ControllerBase
    {
    private readonly SisterCompanyViolationRepository _sisterCompanyViolationRepository;

    public SisterCompanyViolationController(SisterCompanyViolationRepository sisterCompanyViolationRepository)
    {
      _sisterCompanyViolationRepository = sisterCompanyViolationRepository;
    }
    [HttpGet]
    public async Task<List<SisterCompanyViolation>> GetSisterViolation(DateTime From, DateTime To)
    {
      return await _sisterCompanyViolationRepository.GetSisterCompanyViolations(From,To);
    }
  }
}
