using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class SuspendedExchangeactorController : ControllerBase
    {
        private readonly SuspendedExchangeactorRepository suspendedRepository;

        public SuspendedExchangeactorController(SuspendedExchangeactorRepository suspendedRepository)
        {
            this.suspendedRepository = suspendedRepository;            
        }
        [HttpGet]
        public async Task<List<NotRegistered>> GetSuspendedExchangeActorList()
        {
            return await suspendedRepository.GetSuspendedExchangeActorList();
        }
    }
}
