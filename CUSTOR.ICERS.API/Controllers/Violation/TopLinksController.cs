using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TopLinksController : ControllerBase
    {
        private readonly TopLinkRepository _topLinkRepository;

        public TopLinksController(TopLinkRepository topLinkRepository)

        {
            _topLinkRepository = topLinkRepository;
        }
        [HttpGet("getTopLinkList")]
        public async Task<List<TopLinkViewModel>> GetTopLinkList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _topLinkRepository.GetTopLinkList(searchSqlQuery);
        }
        [HttpGet("getTopLinkClientToClientList")]
        public async Task<List<TopLinkViewModel>> GetTopLinkClientToClientList([FromQuery] HHISearchSqlQuery searchSqlQuery)
        {
            return await _topLinkRepository.GetTopLinkClientToClientList(searchSqlQuery);
        }
    }
}
