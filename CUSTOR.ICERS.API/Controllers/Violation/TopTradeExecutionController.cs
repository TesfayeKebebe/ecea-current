﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TopTradeExecutionController : ControllerBase
    {
        private readonly TopTradeExecutionRepository _executionRepository;

        public TopTradeExecutionController(TopTradeExecutionRepository executionRepository)
        {
            _executionRepository = executionRepository;
        }
        [HttpGet("getTopTradeExecutionList")]
        public async Task<List<TopTradeExecution>> GetTopTradeExecutionList([FromQuery] TopSearchSQLParms searchParms)
        {
            return await _executionRepository.GetTopTradeExecutionList(searchParms);
        }

    }
}
