using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeyondPriceAndVolumeController : ControllerBase
    {
    public readonly BeyondPriceAndVolumeRepository _repository;
    public BeyondPriceAndVolumeController(BeyondPriceAndVolumeRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<List<PriceVolumeLimitDetail>> getPriceVolumeLimits(DateTime From, DateTime To, decimal VolumLimit, int percentage)
    {
      return await _repository.getTradePriceWithVolum(From, To, VolumLimit, percentage);
    }
  }
}
