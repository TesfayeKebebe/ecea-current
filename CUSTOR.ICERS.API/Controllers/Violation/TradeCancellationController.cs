﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeCancellationController : ControllerBase
    {
        private readonly TradeCancellationRepository _tradeCancellation;

        public TradeCancellationController(TradeCancellationRepository tradeCancellation)
        {
            _tradeCancellation = tradeCancellation;
        }
        [HttpGet("getTradeCancellationList")]
        public async Task<List<TradeCancellationView>> GetTradeCancellationList([FromQuery] TradeCancellationSqlQuery sqltradeCancellation)
        {
            return await _tradeCancellation.GetTradeCancellationList(sqltradeCancellation);
        }
        [HttpGet("getTradeCancellationFigureList")]
        public async Task<List<TradeCancellationFigure>> GetTradeCancellationFigureList([FromQuery] TradeCancellationSqlQuery sqltradeCancellation)
        {
            return await _tradeCancellation.GetTradeCancellationFigureList(sqltradeCancellation);
        }
    }
}
