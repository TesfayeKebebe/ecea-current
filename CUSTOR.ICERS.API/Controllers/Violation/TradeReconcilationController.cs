﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeReconcilationController : ControllerBase
    {
        private readonly TradeReconciliationRepository _tradeReconciliation;

        public TradeReconcilationController(TradeReconciliationRepository tradeReconciliation)
        {
            _tradeReconciliation = tradeReconciliation;
        }
        [HttpGet("getTradeReconcilationList")]
        public async Task<List<ComparingTradeReconciliation>> GetTradeReconcilationList([FromQuery] SearchParmsQuery searchParms)
        {
            return await _tradeReconciliation.GetTradeReconcilationList(searchParms);
        }
        [HttpGet("getTradeFigureList")]
        public List<TradeReconcilationFigureViewModel> GetTradeFigureList([FromQuery] SearchParmsQuery searchParms)
        {
            return _tradeReconciliation.GetTradeFigureList(searchParms);
        }
        [HttpGet("getTradeExecutionFigureList")]
        public List<TradeReconcilationFigureViewModel> GetTradeExecutionFigureList([FromQuery] SearchParmsQuery searchParms)
        {
            return _tradeReconciliation.GetTradeExecutionFigureList(searchParms);
        }
        [HttpGet("getTradeExecutionList")]
        public List<TradeExecutionView> GetTradeExecutionList([FromQuery] SearchParmsQuery searchParms)
        {
            return _tradeReconciliation.GetTradeExecutionList(searchParms);
        }
        [HttpGet("getTradeReconcilationDifferenceList")]
        public List<TradeReconcilationFigureViewModel> GetTradeReconcilationDifferenceList([FromQuery] SearchParmsQuery searchParms)
        {
            return _tradeReconciliation.GetTradeReconcilationDifferenceList(searchParms);
        }
    }
}
