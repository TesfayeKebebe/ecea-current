﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class TradeSummaryController : ControllerBase
    {
        private readonly TradeSummaryRepository _summaryRepository;

        public TradeSummaryController(TradeSummaryRepository summaryRepository)
        {
            _summaryRepository = summaryRepository;
        }
        [HttpGet("getTradeSummaryList")]
        public async Task<List<TradeSummaryViewModel>> GetTradeSummaryList([FromQuery] SearchParmsQuery searchParms)
        {
            return await _summaryRepository.GetTradeSummaryList(searchParms);
        }
    }
}
