using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradeVsSalesController : ControllerBase
    {
    private readonly TradeVsSalesRepository _repository;
    public TradeVsSalesController(TradeVsSalesRepository repository)
    {
      _repository = repository;
    }
    [HttpGet]
    public async Task<SalesVsTradeDetails> GetTrades(DateTime From, DateTime To, decimal Ratio, decimal MaximumRatio)
    {
      return await _repository.TradeVsSales(From, To, Ratio, MaximumRatio);
    }
    }
}
