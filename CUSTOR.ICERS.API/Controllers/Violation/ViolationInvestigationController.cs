using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViolationInvestigationController : ControllerBase
    {
    private readonly ViolationInvestigationRepository _repository;

    public ViolationInvestigationController(ViolationInvestigationRepository repository)
    {
      _repository = repository;
    }
    [HttpPost]
    public bool post([FromBody] ViolationInvestigationDto investigationDto)
    {
      return _repository.Create(investigationDto);
    }
    [HttpPut]
    public bool Update([FromBody] ViolationInvestigationDto investigationDto)
    {
      return _repository.Update(investigationDto);
    }
    [HttpGet]
    public async Task<List<ViolationRecordCount>>  GetViolation(Guid ViolationId, string Type)
    {
      return await _repository.GetViolation(ViolationId,Type);
    }
    [HttpGet("Investigation")]
    public async Task<List<ViolationInvestigationDetail>> GetVInvestigation(Guid ViolationId)
    {
      return await _repository.GetInvestigations(ViolationId);
    }
  }
}
