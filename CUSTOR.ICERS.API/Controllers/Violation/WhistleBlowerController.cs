﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.WhistleBlowerReport;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.Violation
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class WhistleBlowerController : ControllerBase
    {

        private readonly WhistleBlowerReportRepository _whistleBlowerReportRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        // GET: api/WhistleBlower

        public WhistleBlowerController(WhistleBlowerReportRepository whistleBlowerReportRepository, UserManager<ApplicationUser> userManager)
        {
            _whistleBlowerReportRepository = whistleBlowerReportRepository;
            _userManager = userManager;
        }
 

        // GET: api/WhistleBlower/5
        [HttpGet("{lang}")]
        public async Task<List<WhistleBlowerListDTO>> GetListOfWhistleBlowerReports(string lang)
        {
            return await _whistleBlowerReportRepository.GetAllWhisterBlowerReport(lang);
        }
        [HttpGet("getAllSearchWhisterBlowerList")]
        public async Task<List<WhistleBlowerListDTO>> GetAllSearchWhisterBlowerList([FromQuery] SearchSqlBlowersQuery searchSqlBlowers)
        {
            return await _whistleBlowerReportRepository.GetAllSearchWhisterBlowerList(searchSqlBlowers);
        }
        // POST: api/WhistleBlower
        [HttpPost]
        public async Task<int> PostNewWhistleBlowerReport([FromBody]  WhistleBlowerDTO postedRenewal)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedRenewal.CreatedUserId = Guid.Parse(user.Id);

            return await _whistleBlowerReportRepository.PostNewWhisterBlowerReport(postedRenewal);
        }

        // PUT: api/WhistleBlower/5
        [HttpPut]
        public async Task<int> UpdateWhistleBlowerReport([FromBody]  WhistleBlowerDTO postedRenewal)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedRenewal.UpdatedUserId = Guid.Parse(user.Id);

            return await _whistleBlowerReportRepository.UpdateWhisterBlowerReport(postedRenewal);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<bool> DeleteReport(int id)
        {
            WhistleBlowerDTO postedRenewal = new WhistleBlowerDTO();
            postedRenewal.Id = id;
            var user = await _userManager.GetUserAsync(HttpContext.User);

            postedRenewal.UpdatedUserId = Guid.Parse(user.Id);

            return await _whistleBlowerReportRepository.DeleteReport(postedRenewal);
        }
    }
}
