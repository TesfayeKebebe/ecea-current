﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.DataAccessLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.WEBAPI.Controllers
{
    [Route("api/Warehouse")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class WarehouseController : ControllerBase
    { 

        private readonly WarehouseRepository _warehouseRepo;

        public WarehouseController(WarehouseRepository warehouseRepo)
        {
            _warehouseRepo = warehouseRepo;
        }


       
        [HttpGet("allWarehouse")]
        public async Task<IEnumerable<ClWarehousesDTO>> GetWarehouse()
        {
            return await _warehouseRepo.GetWarehouse(); 
        }
        [HttpGet("allCommodity")]
        public async Task<List<ClCommodityDTO>> GetCommodityes()
        {
            return await _warehouseRepo.GetAllCommodites();
        }
        

       [HttpGet("AllCommodityGrade")]
        public async Task<List<ClCommodityDTO>> GetAllCommodityGrade()
        {
            return await _warehouseRepo.GetAllCommoditesGrade();
        }
        [HttpGet("ServiceLevelAgrement")]
        public async Task<IEnumerable<TblslaDTO>> GetWSLA(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetWSLA(start, end, commodityId);
        }

        [HttpGet("SlaByWarehouse")]
        public async Task<TblslaReportBywarehouseDTO> GetSLAReportByWarehouseReport(string start, string end, Guid? commodityId, Guid? warehouseId)
        {
            return await _warehouseRepo.GetSLAReportByWarehouse(start, end, commodityId, warehouseId);
        }

        [HttpGet("allWORAGRN")]
        public async Task<IEnumerable<TblGrnsDTO>> GetWORAGRN(string start, string end, int status)
        {
            return await _warehouseRepo.GetGRNReport(start, end, status);
        }

        //[HttpGet("allIPosition")]
        //public async Task<IEnumerable<EcxInventoryPositionDTO>> GetWarehouseInventoryPosition(string start, string end)
        //{
        //    return await _warehouseRepo.GetWarehouseInventoryPosition(start, end);
        //}

        [HttpGet]
        [Route("ExpiredPickuNotices")]
        public async Task<IEnumerable<TblPickupNoticesDTO>> GetallExpiredPickuNotices(string start, string end, int status)
        {
            return await _warehouseRepo.GetExpiredPickupNotices(start,end,status);
        }

        [HttpGet]
        [Route("ExtendedPickuNotices")]
        public async Task<IEnumerable<TblExtendPunexpiryDateDTO>> GetExtendedPickuNotices(string start, string end)
        {
            return await _warehouseRepo.GetExtendedPickupNotices(start, end);
        }

        [HttpGet]
        [Route("ExtendedPickupNoticeByClientName")]
        public async Task<IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO>> GetExtendedPickupNoticeByClientName(string start, string end)
        {
            return await _warehouseRepo.GetExtendedPickupNoticesByClientName(start, end);
        }


        [HttpGet]
        [Route("ExtendedPickupNoticesBywarhouse")]
        public async Task<IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO>> GetExtendedPickupNoticesBywarhouse(string start, string end)
        {
            return await _warehouseRepo.GetExtendedPickupNoticesBywarhouse(start, end);
        }

        [HttpGet]
        [Route("Reconcilation")]
        public async Task<IEnumerable<ReconcilationDTO>> GetReconcilationReport(string start, string end)
        {
            return await _warehouseRepo.GetReconcilationReport(start, end);
        }


        [HttpGet]
        [Route("ReconcilationBywarehouse")]
        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationBywarehouse(string start, string end,Guid? commodityId)
        {
            return await _warehouseRepo.GetReconcilationReportByWarehouse(start, end, commodityId);
        }

        [HttpGet]
        [Route("ReconcilationByClientName")]
        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationByClientName(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetReconcilationReportByClientName(start, end,commodityId);
        }

        [HttpGet]
        [Route("ReconcilationByCommdityGrade")]
        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationByCommodityGrade(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetReconcilationReportByCommodityGrade(start, end, commodityId);
        }
        //Warehouse Operations Report –Delivery Notice (DN) Received – Goods Issue Note (GIN) Approval controller

        [HttpGet("ReconcilationByRemark")]
        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationByRemark(string start, string end, Guid? commodityId, Guid? warehouseId)
        {
            return await _warehouseRepo.GetReconcilationReportByRemark(start, end, commodityId, warehouseId);
        }
        [HttpGet]
        [Route("WORDNGIN")]
        public async Task<IEnumerable<TblGinsDTO>> GetallWORDNGIN(string start, string end)
        {
            return await _warehouseRepo.GetWORDNGIN(start, end);
        }


        [HttpGet]
        [Route("GinIssuedBywarehouse")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinIssuedBywarehouse(string start, string end)
        {
            return await _warehouseRepo.GetGINByWarehouse(start, end);
        }

        [HttpGet]
        [Route("ginByCommdityGrade")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> getGINByCommdityGrade(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetGINByCommodityGrade(start, end, commodityId);
        }

        [HttpGet]
        [Route("GinIssuedByClientName")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinIssuedByClientName(string start, string end)
        {
            return await _warehouseRepo.GetGINByClientName(start, end);
        }

        [HttpGet]
        [Route("GinCancledByClientName")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinCancledByClientName(string start, string end)
        {
            return await _warehouseRepo.GetGinCancledByClientName(start, end);
        }

        [HttpGet]
        [Route("GinCancledByWarehouseName")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinCancledByWarehouseName(string start, string end)
        {
            return await _warehouseRepo.GetGinCancledByWarehouseName(start, end);
        }

        [HttpGet]
        [Route("GinCancledByCommodityGrade")]
        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinCancledByCommodityGrade(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetGINCancledByCommodityGrade(start, end, commodityId);
        }
        //Warehouse Operations Report –Delivery Notice (DN) Received – Goods Issue Note (GIN) Approval controller

        [HttpGet("Arrival")]
        public async Task<IEnumerable<TblArrivalsDTO>> GetArrival(string start, string end)
        {
            return await _warehouseRepo.GetArrival(start, end);
        }

        [HttpGet("ArrivalByWarehouseAndCommodity")]
        public async Task<IEnumerable<TblArrivalsBywarehouseandcommodityDTO>> GetArrivalByWarehouseAndCommodity(string start, string end)
        {
            return await _warehouseRepo.GetArrivalByCommodityAndWarehouse(start, end);
        }
        [HttpGet("ArrivalByCommodity")]
        public async Task<IEnumerable<TblArrivalsBywarehouseandcommodityDTO>> GetArrivalByCommodity(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetArrivalByCommodity(start, end, commodityId);
        }

        [HttpGet("ExtendedGrnByCommodity")]
        public async Task<IEnumerable<TblExtendedGrnsDTO>> GetExtendedGrnByCommodity(string start, string end)
        {
            return await _warehouseRepo.GetExtendedNumberofGRNCommodit(start, end);
        }

        [HttpGet("ExtendedGrnByWarehouse")]
        public async Task<IEnumerable<TblExtendedGrnsDTO>> GetExtendedGrnByWarehouse(string start, string end, Guid? commodityId)
        {
            return await _warehouseRepo.GetExtendedNumberofGRNByWarehouseAndCommodity(start, end, commodityId);
        }


    }
}
