﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.WorkFlowProcess;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CUSTOR.ICERS.API.Controllers.WorkFlowProcess
{
    [Route("api/workflow")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    public class WorkFlowController : Controller
    {
        private readonly WorkFlowRepository _workFlowRepo;


        public WorkFlowController(WorkFlowRepository workFlowRepo)
        {
            _workFlowRepo = workFlowRepo;
        }

        [HttpPost]
        [Route("postRecognitionWorkFlow")]
        [Authorize]
        public async Task<bool> PostRecognitionWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostRecognitionWorkFlow(workFLow);
        }
        [HttpPost]
        [Route("postMemberViolationWorkFlow")]
        public async Task<bool> PostMemberViolationWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostMemberViolationWorkFlow(workFLow);
        }

        [HttpPost]
        [Route("postMemberClientWorkFlow")]
        public async Task<bool> PostMemberClientWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostMemberClientWorkFlow(workFLow);
        }

        [HttpPost]
        [Route("PostInjunctionWorkFlow")]
        public async Task<bool> PostInjunctionWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostInjunctionWorkFlow(workFLow);
        }

        [HttpGet("getTradeExecutionWorkFlow/{memberTradeId}/{lang}")]
        public async Task<List<WorkFlowViewModel>> GetTradeExecutionWorkFlow(int memberTradeId, string lang)
        {
            return await _workFlowRepo.GetTradeExecutionWorkFlow(memberTradeId, lang);
        }
        [HttpGet("getTradeViolationWorkFlow/{violationId}/{lang}")]
        public async Task<List<WorkFlowViewModel>> GetTradeViolationWorkFlow(int violationId, string lang)
        {
            return await _workFlowRepo.GetTradeViolationWorkFlow(violationId, lang);
        }
        [HttpGet("getServiceApplicationWorkFlow/{serviceApplicationId}/{lang}")]
        public async Task<List<WorkFlowViewModel>> GetServiceApplicationWorkFlow(Guid serviceApplicationId, string lang)
        {
            return await _workFlowRepo.GetServiceApplicationWorkFlow(serviceApplicationId, lang);
        }
        [HttpGet("getOffSiteMonitoringWorkFlow/{offSiteMonitoring}/{lang}")]
        public async Task<List<WorkFlowViewModel>> GetOffSiteMonitoringWorkFlow(int offSiteMonitoring, string lang)
        {
            return await _workFlowRepo.GetOffSiteMonitoringWorkFlow(offSiteMonitoring, lang);
        }
        [HttpPost]
        [Route("postOffSiteMonitoringWorkFlow")]
        public async Task<bool> PostOffSiteMonitoringWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostOffSiteMonitoringWorkFlow(workFLow);
        }
        [HttpPost]
        [Route("postBankOverSightViolationWorkFlow")]
        public async Task<bool> PostBankOverSightViolationWorkFlow(WorkFlowDTO workFLow)
        {
            return await _workFlowRepo.PostBankOverSightViolationWorkFlow(workFLow);
        }
        [HttpGet("getBankViolationDecisionWorkFlow/{bankViolationId}/{lang}")]
        public async Task<List<WorkFlowViewModel>> GetBankViolationDecisionWorkFlow(int bankViolationId, string lang)
        {
            return await _workFlowRepo.GetBankViolationDecisionWorkFlow(bankViolationId, lang);
        }
    }
}
