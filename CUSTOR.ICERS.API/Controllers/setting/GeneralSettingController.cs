﻿using CUSTOR.API.ExceptionFilter;
using CUSTOR.ICERS.Core.DataAccessLayer.Setting;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Setting;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.setting
{
    [Route("api/generalSetting")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    [EnableCors("CorsPolicy")]
    [Authorize]
    public class GeneralSettingController : Controller
    {
        private readonly GeneralSettingRepository _generalSettingRepository;
        private readonly UserManager<ApplicationUser> _userManager;

            public GeneralSettingController(GeneralSettingRepository generalSettingRepository, UserManager<ApplicationUser> userManager)
        {
                _userManager = userManager;

                _generalSettingRepository = generalSettingRepository;
        }

        [HttpPost]
        public async Task<bool> UpdateSetting([FromBody] GeneralSettingDTO setting)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);
            setting.UpdatedUserId = Guid.Parse(user.Id);
            
            return await _generalSettingRepository.UpdateSetting(setting);
        }

        [HttpPost("renewal/{id}")]
        public async Task<bool> UpdateSettingPeriodicDuration([FromBody] PeriodicDurationSettingDTO setting,int id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (setting.ID == 0)
            {
                setting.UpdatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                setting.UpdatedUserId = Guid.Parse(user.Id);
            }

            return await _generalSettingRepository.UpdateSettingPeriodicDuration(setting,id);
        }
        [HttpGet]
        public async Task<GeneralSetting> GetSetting()
        {
            return await _generalSettingRepository.GetSetting();
        }

        [HttpGet("renewal")]
        public async Task<PeriodicDurationSetting> GetRenewalSetting()
        {
            return await _generalSettingRepository.GetRenewalSetting();
        }

        [HttpPost("SaveTotalCapital")]
        public async Task<bool> SaveTotalCapital(RecoginationTotalCapitalDTO totalcapital)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (totalcapital.Id == 0)
            {
                totalcapital.CreatedUserId = Guid.Parse(user.Id);
            }
            else
            {
                totalcapital.UpdatedUserId = Guid.Parse(user.Id);
            }

            return await _generalSettingRepository.Savetotalcapital(totalcapital);
        }
        [HttpGet("getallTotalCapital/{lang}")]
        public async Task<List<AllRecoginationTotalCapitalDTO>> getallTotalCapital(string lang)
        {
            return await _generalSettingRepository.getallTotalCapital(lang);
        }

        //[HttpGet("{lang}/{exchangeActorTypeId}")]
        //public async Task<List<RecoginationTotalCapitalDTO>> GetServiceCapitalById(string lang,int exchangeActorTypeId)
        //{
        //    return await _generalSettingRepository.GetServiceCapitalById(lang, exchangeActorTypeId);
        //}
        [HttpDelete("{id}")]
        public async Task<bool> DeleteCapital(int id)
        {
            return await _generalSettingRepository.DeleteCapital(id);
        }
    }
}