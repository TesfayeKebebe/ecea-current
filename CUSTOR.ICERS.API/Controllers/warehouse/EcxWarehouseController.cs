﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.DataAccessLayer.Warehouse;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.API.Controllers.warehouse
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcxWarehouseController : ControllerBase
    {

        private readonly EcxWarehouseRepository _warehouseRepo;

        public EcxWarehouseController(EcxWarehouseRepository warehouseRepo)
        {
            _warehouseRepo = warehouseRepo;
        }
        [HttpGet]
        [Route("Warehouse")]
        public async Task<bool> CreateWarehouse()
        {
            return await _warehouseRepo.CreateWarehouse();
        }
        [HttpGet]
        [Route("GrnReport")]
        public async Task<bool> CreateGrnReport()
        {
            return await _warehouseRepo.CreateGrnReport();
        }
        //[HttpGet]
        //[Route("CreateArrivalsReport")]
        //public async Task<bool> CreateArrivalsReport(TblArrivals reportPeriod)
        //{
        //    return await _warehouseRepo.CreateArrivalsReport(reportPeriod);
        //}

        //[HttpGet]
        //[Route("CreateGINReport")]
        //public async Task<bool> CreateGINReport(TblGins reportPeriod)
        //{
        //    return await _warehouseRepo.CreateGINReport(reportPeriod);
        //}
    }
}