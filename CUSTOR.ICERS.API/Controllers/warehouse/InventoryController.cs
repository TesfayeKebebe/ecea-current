﻿using CUSTOR.ICERS.Core.DataAccessLayer.Warehouse;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.warehouse
{
    [Route("api/InventoryController")]
    [EnableCors("CorsPolicy")]
    [ApiController]

    public class InventoryController : ControllerBase
    {
        public readonly InventoryRepository _inventoryRepository;
        public InventoryController(InventoryRepository inventoryRepository)
        {
            _inventoryRepository = inventoryRepository;
        }
        [HttpGet]
        [Route("getLeftOverData/{lang}")]
        public async Task<IEnumerable<SampleLeftOver>> GetLeftOverData(string lang)
        {
            return await _inventoryRepository.GetLeftOverData(lang);
        }
        [HttpGet]
        [Route("getExcessData/{lang}")]
        public async Task<IEnumerable<ExcessProduct>> GetExcessData(string lang)
        {
            return await _inventoryRepository.GetExcessProducts(lang);
        }
        [HttpGet]
        [Route("getShortFallData/{lang}")]
        public async Task<IEnumerable<ShortFallDTO>> GetShortFallData(string lang)
        {
            return await _inventoryRepository.GetShortFallData(lang);
        }
    }
}
