﻿using CUSTOR.ICERS.Core.DataAccessLayer.Warehouse;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.warehouse
{
    [ApiController]
    [EnableCors("CorsPolicy")]
    [Route("api/tradeTransaction")]
    public class TransactionController : ControllerBase
    {
        public readonly TransactionRepository _transactionRepository;
        public TransactionController(TransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        [HttpGet]
        [Route("transactionStatusOne/{lang}")]
        public async Task<IEnumerable<EcexTradeDataForECEA>> GetTransactionStatusOne(string lang)
        {
            return await _transactionRepository.GetTransactionStatusOne(lang);
        }

        [HttpGet]
        public async Task<List<TransactionStatusTwo>> GetTransactionStatusTwo()
        {
            return await _transactionRepository.GetTransactionStatusTwo();
        }
    }
}
