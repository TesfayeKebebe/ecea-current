﻿using CUSTOR.Helpers;
using CUSTOR.ICERS.API.Authorization;
using CUSTOR.ICERS.API.IdentityServer;
using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.DataAccessLayer;
using CUSTOR.ICERS.Core.DataAccessLayer.Dashboard;
using CUSTOR.ICERS.Core.DataAccessLayer.DataChanges;
using CUSTOR.ICERS.Core.DataAccessLayer.Ecx;
using CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement;
using CUSTOR.ICERS.Core.DataAccessLayer.Lookup;
using CUSTOR.ICERS.Core.DataAccessLayer.Notification;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReport;
using CUSTOR.ICERS.Core.DataAccessLayer.OversightReportRepo;
using CUSTOR.ICERS.Core.DataAccessLayer.Payment;
using CUSTOR.ICERS.Core.DataAccessLayer.Recognition;
using CUSTOR.ICERS.Core.DataAccessLayer.Registration;
using CUSTOR.ICERS.Core.DataAccessLayer.Service;
using CUSTOR.ICERS.Core.DataAccessLayer.Setting;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank;
using CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank.Ecx;
using CUSTOR.ICERS.Core.DataAccessLayer.Trade;
using CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation.Penalty;
using CUSTOR.ICERS.Core.DataAccessLayer.Warehouse;
using CUSTOR.ICERS.Core.DataAccessLayer.WhistleBlowerReport;
using CUSTOR.ICERS.Core.DataAccessLayer.WorkFlowProcess;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.DAL.Repository.Ecx;
using CUSTOR.OTRLS.Core.DataAccessLayer;
using CUSTOR.Oversight.DAL;
using CUSTOR.Security;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                          builder => builder.WithOrigins("http://localhost:4200")
                          .AllowAnyMethod()
                          .AllowAnyHeader()
                          .AllowCredentials());
            });
        }
        public static void ConfigureRepositoryService(this IServiceCollection services)
        {
            services.AddScoped<WarehouseRepository>();
            services.AddScoped<RegionRepository>();
            services.AddScoped<ZoneRepository>();
            services.AddScoped<WoredaRepository>();
            services.AddScoped<KebeleRepository>();
            services.AddScoped<ServiceApplicationRepository>();
            services.AddScoped<NationalitiesRepository>();
            services.AddScoped<ExchangeActorRepository>();
            services.AddScoped<LookupRepository>();
            services.AddScoped<PrerequisiteRepository>();
            services.AddScoped<ServiceRepository>();
            services.AddScoped<OwnerManagerRepository>();
            services.AddScoped<CustomerPrerequisiteRepository>();
            services.AddScoped<PaymentAuthorizedRepository>();
            services.AddScoped<ReprsentativeTypeRepository>();
            services.AddScoped<ServiceTariffRepository>();
            services.AddScoped<PaymentOrderRepository>();
            services.AddScoped<ReceiptRepository>();
            services.AddScoped<CertificateRepository>();
            services.AddScoped<CommodityRepository>();
            services.AddScoped<SisterCompanyRepository>();
            services.AddScoped<MemberProductRepository>();
            services.AddScoped<InjunctionRepository>();
            services.AddScoped<CancellationRepository>();
            services.AddScoped<RenewalRepository>();
            services.AddScoped<ReplacementRepository>();
            services.AddScoped<ExchangeActorProfileRepository>();
            services.AddScoped<TransactionRepository>();
            services.AddScoped<InventoryRepository>();
            services.AddScoped<NameChangeRepository>();




            // TODO check unused repo
            services.AddScoped<ServicePrerequisiteRepository>();

            //start Trade Excution
            services.AddScoped<CommodityTypeRepositoory>();
            services.AddScoped<MemberClientTradeRepository>();
            services.AddScoped<ReportTypeRepository>();
            services.AddScoped<MemberTradeEvidenceRepository>();
            services.AddScoped<CommodityGradeRepository>();
            services.AddScoped<MemberClientInformationRepo>();
            services.AddScoped<MemberClientTradeDetailRepo>();
            services.AddScoped<UnitMeasurementRepository>();
            services.AddScoped<MemberTradeFinancialRepository>();
            services.AddScoped<TradeExcutionStatusTypeRepo>();
            services.AddScoped<ReportPeriodRepository>();
            services.AddScoped<TradeExcutionViolationRecordeRepo>();
            services.AddScoped<ExchangeActorFinanicialRepo>();
            services.AddScoped<FinancialReportReminderRepo>();
            services.AddScoped<ClientInformationReminderRepo>();
            services.AddScoped<MemberTradeViolationRepository>();
            services.AddScoped<BankTransactionRepository>();
            services.AddScoped<SiteRepository>();
            services.AddScoped<MemberFinancialAuditorRepository>();
            services.AddScoped<FinancialAuditoredFileUploadRepository>();
            services.AddScoped<OffSiteMonitoringRepository>();
            services.AddScoped<OffSiteMonitoringDetailRepository>();
            services.AddScoped<AnnualBudgetCloserRepository>();
            services.AddScoped<BankOverSightViolationRepository>();
            services.AddScoped<BankOverSightViolationDetailRepo>();


            // end Trade Excution
            //Start Bank Settlement

            // start oversight followup
            services.AddScoped<OversightReportRepo>();
            services.AddScoped<OversightMonthlySummaryrepo>();
            services.AddScoped<ScheduleRepository>();
            services.AddScoped<ComparisionRepository>();
            services.AddScoped<SvgSettlementAccountRepository>();
            services.AddScoped<SettlementAccountRepository>();

            //start Case Related Repositories
            services.AddScoped<SampleUserRepository>();
            services.AddScoped<CaseLookUpRepository>();
            services.AddScoped<CaseStatusRepository>();
            services.AddScoped<CaseApplicationRepository>();
            services.AddScoped<CasePartyRepository>();
            services.AddScoped<CaseFeedbackRepository>();
            services.AddScoped<CaseInvestigationRepository>();
            services.AddScoped<CasePartyInvestigationRepository>();
            services.AddScoped<CaseDocumentRepository>();
            services.AddScoped<LegalCaseRepository>();
            services.AddScoped<CAUnitRepository>();
            services.AddScoped<CaseAppointmentRepository>();
            services.AddScoped<CaseTrackingRepository>();
            services.AddScoped<OversightReportSettingRepository>();
            services.AddScoped<IEmailer, Emailer>();
            services.AddScoped<IEmailSender, EmailSendGrid>();
            services.AddScoped<IAccountManager, AccountManager>();
            // Auth Handlers
            services.AddSingleton<IAuthorizationHandler, ViewUserAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, ManageUserAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, ViewRoleAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, AssignRolesAuthorizationHandler>();
            services.AddScoped<IAccountManager, AccountManager>();

            services.AddScoped<GeneralSettingRepository>();
            //services.AddSingleton<GeneralSettinginstant>();
            //end Case Related Repositories

            // work flow repository
            services.AddScoped<WorkFlowRepository>();
            services.AddScoped<NotificationRepository>();
            // end of work flow repository
            //rep change service
            services.AddScoped<RepresentativeChangeRepository>();
            services.AddScoped<AuditRepository>();
            services.AddScoped<CaseTribunalFeedBackRepository>();
            services.AddScoped<CasePartyDocumentRepository>();
            services.AddScoped<NotRegisteredMemberRepository>();
            services.AddScoped<SisterCompanyViolationRepository>();
            services.AddScoped<BucketingRepository>();
            services.AddScoped<MemberNotRenewedRepository>();
            services.AddScoped<MatchingTradeOrderRepository>();
            services.AddScoped<SuspendedExchangeactorRepository>();
            //end of rep change service
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //audit

            //end of rep change service
            //DashBoard
            services.AddScoped<RecognationDashboardRepository>();
            //End Dashboard

            // data change service
            services.AddScoped<AddressChangeRepository>();
            // end of data change service

            // end of owner service
            services.AddScoped<EceRepository>();
            services.AddScoped<SessionRepositry>();
            services.AddScoped<TradeRepository>();
            services.AddScoped<CaseAppealDocumentRepository>();
            //ecx repository
            services.AddScoped<WhistleBlowerReportRepository>();
            services.AddScoped<CommodityLookupRepository>();
            services.AddScoped<DailyPriceLimitRepository>();
            services.AddScoped<OrderStatusRepository>();
            services.AddScoped<OrderValidityRepository>();
            services.AddScoped<SessionDataRepository>();
            services.AddScoped<SessionStatusRepository>();
            services.AddScoped<SubSessionRepository>();
            services.AddScoped<TradeDataRepository>();
            services.AddScoped<TradeStatusRepository>();
            services.AddScoped<WarehouseLookUpRepository>();
            services.AddScoped<WHRRepository>();
            services.AddScoped<SubSessionStatusRepository>();
            services.AddScoped<PriceChangeRepository>();
            services.AddScoped<AttendanceRepository>();
            services.AddScoped<BeyondPriceAndVolumeRepository>();
            services.AddScoped<NewRequestAfterPreOpenRepository>();
            services.AddScoped<PrearrangedTradeRepository>();
            services.AddScoped<MarkingTheCloseRepository>();
            services.AddScoped<BidOfferRecordRepository>();
            services.AddScoped<CommonViolationRepository>();
            services.AddScoped<MemberAuditorRepository>();
            services.AddScoped<SettlementBankRepository>();
            services.AddScoped<PriceDifferncentWarehousesRepository>();
            services.AddScoped<TradeVsSalesRepository>();
            services.AddScoped<TradeReconciliationRepository>();
            services.AddScoped<ReceiptHoldingSellOrderRepository>();
            services.AddScoped<EcxLookUpRepository>();
            services.AddScoped<SettlementGuarnatteFundRepository>();
            services.AddScoped<SettlementBankPerformanceRepository>();
            services.AddScoped<DelayRegisterPerBankRepository>();
            services.AddScoped<SalesTradeCommodityRepository>();
            services.AddScoped<TopTradeExecutionRepository>();
            services.AddScoped<SettlementTransactionRepository>();
            services.AddScoped<WithdrawalTransactionReportRepository>();
            services.AddScoped<HHIIndexRepository>();
            services.AddScoped<TopLinkRepository>();
            services.AddScoped<TradeSummaryRepository>();
            services.AddScoped<TradeTrendRepository>();
            services.AddScoped<PriceAndQuantitySummuryRepository>();
            services.AddScoped<ContractMarketSharesRepository>();
            //penalty
            services.AddScoped<PenaltyRepository>();
            services.AddScoped<ComparisonTradeByCommodityTypeRepo>();
            services.AddScoped<BlackListRepository>();
            services.AddScoped<BuyerSellerRatioRepository>();
            services.AddScoped<TradeSummaryByCommodityRepository>();
            services.AddScoped<FlashCrashRepository>();
            services.AddScoped<TradeCancellationRepository>();
            services.AddScoped<ViolationInvestigationRepository>();
            services.AddScoped<PriceDifferenceAmongDefferentGradeRepo>();
            services.AddScoped<AuctionRepository>();
        }
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "ICERS API", Version = "v1" });
            });
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options => { });

        }
        public static void RegisterAppServices(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>()
              .AddEntityFrameworkStores<ECEADbContext>()
              .AddDefaultTokenProviders();

            // Configure Identity options and password complexity here
            services.Configure<IdentityOptions>(options =>
            {
                // User settings
                options.User.RequireUniqueEmail = true;

                //// Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;

                //// Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
            });
            var applicationUrl = configuration["IServer:IServerUrl"].ToString();
            services.AddIdentityServer(options =>
            {
                options.IssuerUri = applicationUrl;
            })
             .AddDeveloperSigningCredential()
             .AddInMemoryPersistedGrants()
             .AddInMemoryIdentityResources(IdentityServerConfig.GetIdentityResources())
             .AddInMemoryApiResources(IdentityServerConfig.GetApiResources())
             .AddInMemoryClients(IdentityServerConfig.GetClients())
             .AddAspNetIdentity<ApplicationUser>()
             .AddProfileService<ProfileService>();




            // .TrimEnd('/');

            services.AddAuthentication(
                IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = applicationUrl;
                    options.SupportedTokens = SupportedTokens.Jwt;
                    options.RequireHttpsMetadata = false; // Note: Set to true in production
                    options.ApiName = IdentityServerConfig.ApiName;
                    //options.NameClaimType = JwtClaimTypes.Subject;
                });
        }
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config["ConnectionStrings:DefaultConnection"];
            var ecxLookUpConnectionString = config["ConnectionStrings:ECXLookUpConnection"];
            var ecxCentralDepositoryConnectionString = config["ConnectionStrings:ECXCentralDepositoryConnection"];
            var ecxWarhouseApplicationVersion2ConnectionString = config["ConnectionStrings:ECXWarhouseApplicationVersion2Connection"];
            var eCXTradeConnectionConnectionString = config["ConnectionStrings:ECXTradeConnection"];
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<ECEADbContext>(options =>
            {
                options.UseSqlServer(connectionString, b => b.MigrationsAssembly(migrationsAssembly));
                options.EnableSensitiveDataLogging(true);

            });
            services.AddDbContext<ECXTradeDbContext>(options =>
            {
                options.UseSqlServer(eCXTradeConnectionConnectionString);
                options.EnableSensitiveDataLogging(true);

            });
            services.AddDbContext<ECXLookupDbContext>(options =>
            {
                options.UseSqlServer(ecxLookUpConnectionString, b => b.MigrationsAssembly(migrationsAssembly));
                options.EnableSensitiveDataLogging(true);

            });
            services.AddDbContext<EcxdbCentralDepositoryDbContext>(options =>
            {
                options.UseSqlServer(ecxCentralDepositoryConnectionString, b => b.MigrationsAssembly(migrationsAssembly));
                options.EnableSensitiveDataLogging(true);

            });
            services.AddDbContext<ECXWarehouseApplicationVersion2DbContext>(options =>
            {
                options.UseSqlServer(ecxWarhouseApplicationVersion2ConnectionString, b => b.MigrationsAssembly(migrationsAssembly));
                options.EnableSensitiveDataLogging(true);

            });


        }
        public static void RegisterPolicyServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser()
                    .Build();

                options.AddPolicy(Policies.AssignAllowedRolesPolicy,
                        policy => policy.Requirements.Add(new AssignRolesAuthorizationRequirement()));

                options.AddPolicy(Policies.ViewAllUsersPolicy,
                        policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ViewUsers));
                options.AddPolicy(Policies.ManageAllUsersPolicy,
                        policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ManageUsers));
                options.AddPolicy(Policies.ViewAllRolesPolicy,
                        policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ViewRoles));
                options.AddPolicy(Policies.ViewRoleByRoleNamePolicy,
                        policy => policy.Requirements.Add(new ViewRoleAuthorizationRequirement()));
                options.AddPolicy(Policies.ManageAllRolesPolicy,
                        policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ManageRoles));
                options.AddPolicy(Policies.ManageAllRolesPolicy,
                    policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ManageRoles));

                /* Start trade execution permissions       */
                options.AddPolicy(Policies.ViewFinancialReport,
                       policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ManageFinancialReports));
                options.AddPolicy(Policies.ViewMemberTradeExecution, policy => policy.RequireClaim(ApplicationPermissions.ViewTradeExecution));
                options.AddPolicy(Policies.ViewMemberClientInformation, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ViewMemberClientInformation));
                options.AddPolicy(Policies.CreateClientInformation, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.CreateClientInformation));
                options.AddPolicy(Policies.CreateFinancialReport, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.CreateFinancialReport));
                options.AddPolicy(Policies.CreateMemberViolation, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.CreateMemberViolation));
                options.AddPolicy(Policies.CreateTradeExecution, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.CreateTradeExecution));
                options.AddPolicy(Policies.ViewMemberViolation, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ViewMemberViolation));


                options.AddPolicy(Policies.MemberTradeExecution, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.MemberTradeExecution));
                options.AddPolicy(Policies.MemberFinance, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.MemberFinance));
                options.AddPolicy(Policies.MemberClientInformation, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.MemberClientInformation));
                options.AddPolicy(Policies.OffSiteMoniteringTeamLeader, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.OffSiteMoniteringTeamLeader));
                options.AddPolicy(Policies.OffSiteMoniteringDeptDirector, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.OffSiteMoniteringDeptDirector));
                options.AddPolicy(Policies.OffSiteMoniteringView, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.OffSiteMoniteringView));
                /*Start Settlement Bank */
                options.AddPolicy(Policies.SettlementBankOfficer, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.SettlementBankOfficer));
                options.AddPolicy(Policies.SettlementBankTeamLeader, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.SettlementBankTeamLeader));
                options.AddPolicy(Policies.SettlementBankDepartmentHead, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.SettlementBankDepartmentHead));

                /* End Settlement Bank */

                /* end trade execution permissions  */


                // Super Administrator Permissions
                options.AddPolicy(Policies.ManageAdministratorsPolicy,
                  policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.ManageSiteAdministrators));
                options.AddPolicy(Policies.ManageLookupsPolicy,
                        policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.ManageLookups));

                // Recognition permission
                options.AddPolicy(Policies.AddRecongnition,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.AddRecognitions));

                options.AddPolicy(Policies.ViewRecognition,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.ViewRecognition));

                options.AddPolicy(Policies.UpateRecognition,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.UpateRecognition));

                //For finance
                options.AddPolicy(Policies.ViewAuthorizedPayment,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.ViewAuthorizedPayment));

                options.AddPolicy(Policies.RecivePayment,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.RecivePayment));

                options.AddPolicy(Policies.CancelAuthorizedPayment,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.CancelAuthorizedPaymentOrder));

                // for approval
                options.AddPolicy(Policies.Prepare,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.Prepare));
                options.AddPolicy(Policies.Reporter, policy => policy.RequireClaim(ClaimConstants.Permission, ApplicationPermissions.Reporter));

                /* start of law enforcment policy defnition */
                options.AddPolicy(Policies.ManageLawEnforcementCase,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.ManageLawEnforcementCase));

                /* end of law enforcement plocy definition*/

                /* start of adminstrative tirubunal policy defnition */
                options.AddPolicy(Policies.ManageCases,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.ManageCases));

                /* end of amdinstrative tribunal plocy definition*/
                options.AddPolicy(Policies.SettlementGuarantee,
                    policy => policy.RequireClaim(ClaimConstants.Permission,
                    ApplicationPermissions.SettlementGuarantee));
                options.AddPolicy(Policies.ServiceLevelAgreement,
                   policy => policy.RequireClaim(ClaimConstants.Permission,
                   ApplicationPermissions.ServiceLevelAgreement));

            });
        }
    }
}
