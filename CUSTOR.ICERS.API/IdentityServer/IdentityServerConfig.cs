using CUSTOR.Security;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.IdentityServer
{

    public class IdentityServerConfig
    {
        public const string ApiName = "icers_api";
        public const string OffLineAccess = "offline_access";
        public const string ApiFriendlyName = "ICERS API";
        public const string icersClientID = "icers_spa";
        public const string SwaggerClientID = "swaggerui";
        public const string OversightClientId = "icers_oversight";

        // Identity resources (used by UserInfo endpoint).
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Phone(),
                new IdentityResources.Email(),
                new IdentityResource(ScopeConstants.Roles, new List<string> { JwtClaimTypes.Role })
            };
        }

        // Api resources.
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource(ApiName) {
                    UserClaims = {
                        JwtClaimTypes.Name,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.PhoneNumber,
                        JwtClaimTypes.Role,
                        ClaimConstants.Permission,

                    }
                },
                new ApiResource(OffLineAccess) {
                    UserClaims = {
                        JwtClaimTypes.Name,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.PhoneNumber,
                        JwtClaimTypes.Role,
                        ClaimConstants.Permission,

                    }
                }
            };
        }

        // Clients want to access resources.
        public static IEnumerable<Client> GetClients()
        {
            // Clients credentials.
            return new List<Client>
            {
                // http://docs.identityserver.io/en/release/reference/client.html.
                new Client
                {
                    ClientId = IdentityServerConfig.icersClientID,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword, // Resource Owner Password Credential grant.
                    AllowAccessTokensViaBrowser = true,
                    RequireClientSecret = false, // This client does not need a secret to request tokens from the token endpoint.
                    AllowedCorsOrigins = { "http://127.0.0.1:4200", "http://localhost:4200" },

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId, // For UserInfo endpoint.
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Phone,
                        IdentityServerConstants.StandardScopes.Email,
                        ScopeConstants.Roles,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                    },
                    AllowOfflineAccess = true, // For refresh token.
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    AccessTokenLifetime = 7200, // Lifetime of access token in seconds.
                    IdentityTokenLifetime = 7200,
                    AbsoluteRefreshTokenLifetime = 0,
                    SlidingRefreshTokenLifetime = 2592000,
                    UpdateAccessTokenClaimsOnRefresh=true

                // AccessTokenLifetime = 30=, // Lifetime of access token in seconds.
                },

                new Client
                {
                    DeviceCodeLifetime=889,
                    ClientId = SwaggerClientID,
                    ClientName = "Icers UI",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowAccessTokensViaBrowser = true,
                    RequireClientSecret = false,
                     AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId, // For UserInfo endpoint.
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Phone,
                        IdentityServerConstants.StandardScopes.Email,
                        ScopeConstants.Roles,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                       ApiName
                    },
                    AllowOfflineAccess = true, // For refresh token.
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    AccessTokenLifetime = 7200, // Lifetime of access token in seconds.
                    IdentityTokenLifetime = 7200,
                    AuthorizationCodeLifetime=8999,
                    SlidingRefreshTokenLifetime = 2592000,
                    UpdateAccessTokenClaimsOnRefresh=true,
                },
                new Client
                {
                    ClientId = IdentityServerConfig.OversightClientId,
                    ClientName = "Icers UI",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword, // Resource Owner Password Credential grant.
                    AllowAccessTokensViaBrowser = true,
                    RequireClientSecret = false, // This client does not need a secret to request tokens from the token endpoint.
                    AllowedCorsOrigins = { "http://127.0.0.1:4200", "http://localhost:4200" },

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId, // For UserInfo endpoint.
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Phone,
                        IdentityServerConstants.StandardScopes.Email,
                        ScopeConstants.Roles,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                    },
                    AllowOfflineAccess = true, // For refresh token.
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    AccessTokenLifetime = 7200, // Lifetime of access token in seconds.
                    IdentityTokenLifetime = 7200,
                    AbsoluteRefreshTokenLifetime = 0,
                    SlidingRefreshTokenLifetime = 2592000,
                    UpdateAccessTokenClaimsOnRefresh=true

                // AccessTokenLifetime = 30=, // Lifetime of access token in seconds.
                },

            };
        }
    }
}
