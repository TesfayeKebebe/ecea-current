﻿namespace CUSTOR.Security
{
    public class ClaimViewModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}