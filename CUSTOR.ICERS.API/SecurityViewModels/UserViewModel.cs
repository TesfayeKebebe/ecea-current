using CUSTOR.Helpers;
using CUSTOR.ICERS.Core.Security;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.SecurityViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(200, MinimumLength = 2, ErrorMessage = "Username must be between 2 and 200 characters")]
        public string UserName { get; set; }

        public string FullName { get; set; }

        public string Tin { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(200, ErrorMessage = "Email must be at most 200 characters")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        

        public string PhoneNumber { get; set; }

        public string Configuration { get; set; }

        public bool IsEnabled { get; set; }
        public bool IsExisting { get; set; }

        public bool IsLockedOut { get; set; }

        [MinimumCount(1, ErrorMessage = "Roles cannot be empty")]
        public List<string> Roles { get; set; }
    }
    public class UsersViewModel
    {
        [Display(Name = "User")]
        public ApplicationUser User { get; set; }

        [Display(Name = "Roles")]
        public string Roles { get; set; }
    }
    public class UserViewModel2
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(200, MinimumLength = 2, ErrorMessage = "Username must be between 2 and 200 characters")]
        public string UserName { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(200, ErrorMessage = "Email must be at most 200 characters")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }
        public string Tin { get; set; }
        public string PhoneNumber { get; set; }

        public string Configuration { get; set; }

        public bool IsEnabled { get; set; }
        public bool IsExisting { get; set; }

        public bool IsLockedOut { get; set; }

        [MinimumCount(1, ErrorMessage = "Roles cannot be empty")]
        public string[] Roles { get; set; }
    }

}
