using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.Common
{
  public static class Constants
  {
    //Violations Constant
    #region
    public const string IsNotRegistered = "IsNotRegistered";
    public const string HasNewRequestAfterPreOpen = "HasNewRequestAfterPreOpen";
    public const string HasMarkingToClose = "HasMarkingToClose";
    public const string HasCancelled = "HasCancelled";
    public const string HasSuspended = "HasSuspended";
    public const string HasNotRenewed = "HasNotRenewed";
    public const string HasBucketing = "HasBucketing";
    public const string HasPreArrangedDuringOpen =  "HasPreArrangedDuringOpen";
    public const string HasFrontRunning = "HasFrontRunning";
    public const string HasAbnormalTrading = "HasAbnormalTrading";
    public const string HasPriceChange = "HasPriceChange";
    public const string HasPreArragedTrade = "HasPreArragedTrade";
    public const string IsMatchTrade = "IsMatchTrade";
    public const string HasSisterCompany = "HasSisterCompany";
    public const string SellerNotRegisterd = "SellerNotRegisterd";
    public const string BuyerNotRegisterd = "BuyerNotRegisterd";
    public const string HasPriceVolumeBeyondLimit = "HasPriceVolumeBeyondLimit";
    public const string HasPriceLimit = "HasPriceLimit";
    #endregion
    //Queries constant
    #region
    public const string Order = "Order";
    public const string PrearrangedDuringOpen = "PrearrangedDuringOpen";
    public const string ExchangeActorCode = "ExchangeActorCode";
    public const string Cancel = "Cancel";
    public const string SuspendedMember = "SuspendedMember";
    public const string NotRenewedMember = "NotRenewedMember";
    public const string Bucketing = "Bucketing";
    public const string LastSession = "LastSession";
    public const string WeightAveragePrice = "WeightAveragePrice";
    public const string PriceLimitForCommodity = "PriceLimitForCommodity";
    public const string TradeByDate = "TradeByDate";
    public const string ClosingAndOpenningPrice = "ClosingAndOpenningPrice";
    public const string OrderByMemberId = "OrderByMemberId";
    public const string SisterCompanyViolationByCode = "SisterCompanyViolationByCode";
    public const string PriceRange = "PriceRange";
    public const string FrontRunningBySeller = "FrontRunningBySeller";
    public const string FrontRunningByBuyer = "FrontRunningByBuyer";
    public const string TradeBeyondPrice = "TradeBeyondPrice";
    public const string PreviousSession = "previousSession";
    public const string CommonsTrade = "CommonsTrade";
    public const string TradeViolationRecord = "TradeViolationRecord";
    public const string GetAuction = "GetAuction";
    public const string AttendanceOfTrade = "AttendanceOfTrade";
    public const string AttendanceOfTradeWithCommodity = "AttendanceOfTradeWithCommodity";
    public const string AttendanceByDate = "AttendanceByDate";
    public const string BuyerSellerRatio = "BuyerSellerRatio";
    public const string AttendanceVolumeValueRatio = "AttendanceVolumeValueRatio";
    public const string TradeAttendanceVolumeValueRatio = "TradeAttendanceVolumeValueRatio";
    public const string OrderAttendanceRatioGraph = "OrderAttendanceRatioGraph";
    public const string TradeAttendanceRatioGraph = "TradeAttendanceRatioGraph";
    public const string AuctionViolationRecordByDate = "AuctionViolationRecordByDate";
    public const string GetTradeByDate = "GetTradeByDate";
    public const string TradeViolationRecordByDate = "TradeViolationRecordByDate";
    public const string BidOfferRecordById = "BidOfferRecordById";
    public const string BidOfferRecordBySession = "BidOfferRecordBySession";
    public const string ViolationRecordByViolationId = "ViolationRecordByViolationId";
    public const string PreGetPrearrangedDuringOpen = "preGetPrearrangedDuringOpen";
    public const string AuctionSellerById = "AuctionSellerById";
    public const string AuctionBuyerById = "AuctionBuyerById";
    public const string AuctiontViolationRecordByViolationId = "AuctiontViolationRecordByViolationId";
    public const string GetInvestigationByDate = "GetInvestigationByDate";
    public const string ComparisonTradeByCommodityType = "ComparisonTradeByCommodityType";
    public const string ComparisonTradeByCommodityGrade = "ComparisonTradeByCommodityGrade";
    public const string BuyedCommodity = "BuyedCommodity";
    public const string BuyedCommodityWarehouse = "BuyedCommodityWarehouse";
    public const string BuyedCommodityForMarketShare = "BuyedCommodityForMarketShare";
    public const string ItemForSales = "ItemForSales";
    public const string BuyedCommodityWarehouseForMarketShare = "BuyedCommodityWarehouseForMarketShare";
    public const string BuyerMemberMarketShare = "BuyerMemberMarketShare";
    public const string SellerMemberMarketShare = "SellerMemberMarketShare";
    public const string TopBuyerMarketShare = "TopBuyerMarketShare";
    public const string TopSellerMarketShare = "TopSellerMarketShare";
    public const string OpenedSession = "OpenedSession";
    public const string OnlineEcxOrderByDate = "OnlineEcxOrderByDate";
    public const string SummuryOfPriceAndQuantity = "SummuryOfPriceAndQuantity";
    public const string SummuryOfPriceAndQuantityByCommodityClass = "SummuryOfPriceAndQuantityByCommodityClass";
    public const string SummuryOfPriceAndQuantityByCommodity = "SummuryOfPriceAndQuantityByCommodity";
    public const string GetPriceByDate = "GetPriceByDate";
    public const string GetPrice = "GetPrice";
    public const string PriceDifferenceAmongDifferentGrade = "PriceDifferenceAmongDifferentGrade";
    public const string PriceDifferenceBetweenWarehouse = "PriceDifferenceBetweenWarehouse";
    public const string PriceDifferenceBetweenWarehouses = "PriceDifferenceBetweenWarehouses";
    public const string AllWareHouse = "AllWareHouse";
    public const string ReceiptHoldingVsSellOrder = "ReceiptHoldingVsSellOrder";
    public const string ReceiptHoldingvsSellOrderGraph = "ReceiptHoldingvsSellOrderGraph";
    public const string TradeVsSalesCommodity = "TradeVsSalesCommodity";
    public const string TopLink = "TopLink";
    public const string TopLinkClientToClient = "TopLinkClientToClient";
    public const string TopGenerateByComodity = "TopGenerateByComodity";
    public const string TopGenerateByComodityGrade = "TopGenerateByComodityGrade";
    public const string TopGenerateBuyerExchangeActor = "TopGenerateBuyerExchangeActor";
    public const string TopGenerateSellerExchangeActor = "TopGenerateSellerExchangeActor";
    public const string TopGenerateWarehouse = "TopGenerateWarehouse";
    public const string TradeCancellation = "TradeCancellation";
    public const string TradeCancellationFigure = "TradeCancellationFigure";
    public const string TradeReconciliation = "TradeReconciliation";
    public const string TradeReconcilationFigure = "TradeReconcilationFigure";
    public const string TradeSummaryByCommodity = "TradeSummaryByCommodity";
    public const string TradeSummary = "TradeSummary";
    public const string SalesVsTradeable = "SalesVsTradeable";
    public const string GetViolation = "GetViolation";
    public const string TradeViolationRecords = "TradeViolationRecords";
    #endregion
  }
}
