﻿using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.Common
{
    public class PagedResult<T>
    {
        public IEnumerable<T> Items { get; set; }

        public int ItemsCount { get; set; }
        //public static implicit operator PagedResult<T>(ICERS.Core.Common.PagedResult<BankTransactionDTO> v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
