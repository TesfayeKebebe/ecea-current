using OfficeOpenXml.FormulaParsing.LexicalAnalysis.TokenSeparatorHandlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.Common
{
 public  class QueryConfiguration
  {
    public static string GetQuery(string Name)
    {
      string query = "";
      switch (Name)
      {
        case Constants.Order:
          query = "exec dbo.pGetBidOfferRecord {0},{1}";
          break;
        case Constants.PrearrangedDuringOpen:
          query = "exec dbo.preGetPrearrangedDuringOpen {0},{1},{2}";
          break;
        case Constants.ExchangeActorCode:
          query = "exec dbo.prGetExchangeActorCode {0}";
          break;
        case Constants.Cancel:
          query = "exec dbo.preGetCancel { 0}";
          break;
        case Constants.SuspendedMember:
          query = "exec dbo.preGetSuspendedMember {0}";
          break;
        case Constants.NotRenewedMember:
          query = "exec dbo.preNotRenewedMember {0},{1}";
          break;
        case Constants.Bucketing:
          query = "exec dbo.preGetBacketing {0},{1},{2},{3}";
          break;
        case Constants.LastSession:
          query = "exec dbo.pGetLastSession {0},{1},{2}";
          break;
        case Constants.WeightAveragePrice:
          query = "exec dbo.pGetWeightAveragePrice {0},{1},{2}";
          break;
        case Constants.PriceLimitForCommodity:
          query = "exec dbo.pGetPriceLimitForCommodity {0},{1},{2}";
          break;
        case Constants.TradeByDate:
          query = "exec dbo.pGetTradeByDate {0},{1}";
          break;
        case Constants.ClosingAndOpenningPrice:
          query = "exec dbo.getClosingAndOpenningPrice {0},{1},{2},{3}";
          break;
        case Constants.OrderByMemberId:
          query = "exec dbo.GetOrderByMemberId {0}";
          break;
        case Constants.SisterCompanyViolationByCode:
          query = "exec dbo.prGetSisterCompanyViolationByCode {0},{1}";
          break;
        case Constants.PriceRange:
          query = "exec dbo.pPriceRange {0},{1},{2}";
          break;
        case Constants.FrontRunningBySeller:
          query = "exec dbo.pGetFrontRunningBySeller {0},{1},{2},{3}";
          break;
        case Constants.FrontRunningByBuyer:
          query = "exec dbo.pGetFrontRunningByBuyer {0},{1},{2},{3}";
          break;
        case Constants.TradeBeyondPrice:
          query = "exec dbo.prGetTradeBeyondPrice {0},{1}";
          break;
        case Constants.PreviousSession:
          query = "exec dbo.pGetpreviousSession {0},{1}";
          break;
        case Constants.CommonsTrade:
          query = "exec dbo.prGetCommonsTradeById {0}";
          break;
        case Constants.TradeViolationRecord:
          query = "exec dbo.pGetTradeViolationRecordByViolationId {0}";
          break;
        case Constants.GetAuction:
          query = "exec dbo.pGetAuction {0},{1}";
          break;
        case Constants.AttendanceOfTradeWithCommodity:
          query = "exec dbo.pGetAttendanceOfTrade {0},{1}, {2},{3}";
          break;
        case Constants.AttendanceOfTrade:
          query = "exec dbo.pGetAttendanceOfTrade {0},{1},{2}";
          break;
        case Constants.AttendanceByDate:
          query = "exec dbo.pGetAttendanceByDate {0},{1},{2},{3}";
          break;
        case Constants.BuyerSellerRatio:
          query = "exec dbo.pGetBuyerSellerRatio {0},{1},{2},{3}";
          break;
        case Constants.AttendanceVolumeValueRatio:
          query = "exec dbo.pGetAttendanceVolumeValueRatio {0},{1},{2}";
          break;
        case Constants.TradeAttendanceVolumeValueRatio:
          query = "exec dbo.pTradeAttendanceVolumeValueRatio {0},{1},{2}";
          break;
        case Constants.OrderAttendanceRatioGraph:
          query = "exec dbo.pGetOrderAttendanceRatioGraph {0},{1},{2}";
          break;
        case Constants.TradeAttendanceRatioGraph:
          query = "exec dbo.pGetTradeAttendanceRatioGraph {0},{1},{2}";
          break;
        case Constants.AuctionViolationRecordByDate:
          query = "exec dbo.pGetAuctionViolationRecordByDate {0},{1}";
          break;
        case Constants.GetTradeByDate:
          query = "exec dbo.GetTradeByDate {0},{1}";
          break;
        case Constants.TradeViolationRecordByDate:
          query = "exec dbo.pGetTradeViolationRecordByDate {0},{1}";
          break;
        case Constants.BidOfferRecordById:
          query = "exec dbo.pGetBidOfferRecordById {0}";
          break;
        case Constants.BidOfferRecordBySession:
          query = "exec dbo.pGetBidOfferRecordBySession {0},{1}";
          break;
        case Constants.ViolationRecordByViolationId:
          query = "exec dbo.pGetViolationRecordByViolationId {0}";
          break;
        case Constants.PreGetPrearrangedDuringOpen:
          query = "exec dbo.preGetPrearrangedDuringOpen {0},{1},{2}";
          break;
        case Constants.AuctionSellerById:
          query = "exec dbo.pGetAuctionSellerById {0}";
          break;
        case Constants.AuctionBuyerById:
          query = "exec dbo.pGetAuctionBuyerById {0}";
          break;
        case Constants.AuctiontViolationRecordByViolationId:
          query = "exec dbo.pGeAuctiontViolationRecordByViolationId {0}";
          break;
        case Constants.GetInvestigationByDate:
          query = QueryConfiguration.GetQuery(Constants.GetInvestigationByDate);
          break;
        case Constants.ComparisonTradeByCommodityType:
          query = QueryConfiguration.GetQuery(Constants.ComparisonTradeByCommodityType);
          break;
        case Constants.ComparisonTradeByCommodityGrade:
          query = QueryConfiguration.GetQuery(Constants.ComparisonTradeByCommodityGrade);
          break;
        case Constants.BuyedCommodity:
          query = QueryConfiguration.GetQuery(Constants.BuyedCommodity);
          break;
        case Constants.BuyedCommodityWarehouse:
          query = QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouse);
          break;
        case Constants.BuyedCommodityForMarketShare:
          query = QueryConfiguration.GetQuery(Constants.BuyedCommodityForMarketShare);
          break;
        case Constants.ItemForSales:
          query = QueryConfiguration.GetQuery(Constants.ItemForSales);
          break;
        case Constants.BuyedCommodityWarehouseForMarketShare:
          query = QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouseForMarketShare);
          break;
        case Constants.BuyerMemberMarketShare:
          query = QueryConfiguration.GetQuery(Constants.BuyerMemberMarketShare);
          break;
        case Constants.SellerMemberMarketShare:
          query = QueryConfiguration.GetQuery(Constants.SellerMemberMarketShare);
          break;
        case Constants.TopBuyerMarketShare:
          query = QueryConfiguration.GetQuery(Constants.TopBuyerMarketShare);
          break;
        case Constants.TopSellerMarketShare:
          query = QueryConfiguration.GetQuery(Constants.TopSellerMarketShare);
          break;
        case Constants.OpenedSession:
          query = QueryConfiguration.GetQuery(Constants.OpenedSession);
          break;
        case Constants.OnlineEcxOrderByDate:
          query = QueryConfiguration.GetQuery(Constants.OnlineEcxOrderByDate);
          break;
        case Constants.SummuryOfPriceAndQuantity:
          query = QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantity);
          break;
        case Constants.SummuryOfPriceAndQuantityByCommodityClass:
          query = QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantityByCommodityClass);
          break;
        case Constants.SummuryOfPriceAndQuantityByCommodity:
          query = QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantityByCommodity);
          break;
        case Constants.GetPriceByDate:
          query = QueryConfiguration.GetQuery(Constants.GetPriceByDate);
          break;
        case Constants.GetPrice:
          query = QueryConfiguration.GetQuery(Constants.GetPrice);
          break;
        case Constants.PriceDifferenceAmongDifferentGrade:
          query = QueryConfiguration.GetQuery(Constants.PriceDifferenceAmongDifferentGrade);
          break;
        case Constants.PriceDifferenceBetweenWarehouse:
          query = QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouse);
          break;
        case Constants.PriceDifferenceBetweenWarehouses:
          query = QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouses);
          break;
        case Constants.AllWareHouse:
          query = QueryConfiguration.GetQuery(Constants.AllWareHouse);
          break;
        case Constants.ReceiptHoldingVsSellOrder:
          query = QueryConfiguration.GetQuery(Constants.ReceiptHoldingVsSellOrder);
          break;
        case Constants.ReceiptHoldingvsSellOrderGraph:
          query = QueryConfiguration.GetQuery(Constants.ReceiptHoldingvsSellOrderGraph);
          break;
        case Constants.TradeVsSalesCommodity:
          query = QueryConfiguration.GetQuery(Constants.TradeVsSalesCommodity);
          break;
        case Constants.TopLink:
          query = QueryConfiguration.GetQuery(Constants.TopLink);
          break;
        case Constants.TopLinkClientToClient:
          query = QueryConfiguration.GetQuery(Constants.TopLinkClientToClient);
          break;
        case Constants.TopGenerateByComodity:
          query = QueryConfiguration.GetQuery(Constants.TopGenerateByComodity);
          break;
        case Constants.TopGenerateByComodityGrade:
          query = QueryConfiguration.GetQuery(Constants.TopGenerateByComodityGrade);
          break;
        case Constants.TopGenerateBuyerExchangeActor:
          query = QueryConfiguration.GetQuery(Constants.TopGenerateBuyerExchangeActor);
          break;
        case Constants.TopGenerateSellerExchangeActor:
          query = QueryConfiguration.GetQuery(Constants.TopGenerateSellerExchangeActor);
          break;
        case Constants.TopGenerateWarehouse:
          query = QueryConfiguration.GetQuery(Constants.TopGenerateSellerExchangeActor);
          break;
        case Constants.TradeCancellation:
          query = QueryConfiguration.GetQuery(Constants.TradeCancellation);
          break;
        case Constants.TradeCancellationFigure:
          query = QueryConfiguration.GetQuery(Constants.TradeCancellationFigure);
          break;
        case Constants.TradeReconciliation:
          query = QueryConfiguration.GetQuery(Constants.TradeReconciliation);
          break;
        case Constants.TradeReconcilationFigure:
          query = QueryConfiguration.GetQuery(Constants.TradeReconcilationFigure);
          break;
        case Constants.TradeSummaryByCommodity:
          query = QueryConfiguration.GetQuery(Constants.TradeSummaryByCommodity);
          break;
        case Constants.SalesVsTradeable:
          query = QueryConfiguration.GetQuery(Constants.SalesVsTradeable);
          break;
        case Constants.GetViolation:
          query = QueryConfiguration.GetQuery(Constants.GetViolation);
          break;
        case Constants.TradeViolationRecords:
          query = QueryConfiguration.GetQuery(Constants.TradeViolationRecords);
          break;

      }
      return query;
    }

  }
}
