﻿using Microsoft.Extensions.Configuration;
using System;
using System.Globalization;

namespace CUSTOR.ICERS.Core.Common
{
    public class Settings
    {
        public int ExpirationPeriod = 9000;
        public Settings(IConfiguration configuration)
        {
            int expirationPeriod;
            if (Int32.TryParse(configuration["Caching:ExpirationPeriod"], NumberStyles.Any, NumberFormatInfo.InvariantInfo, out expirationPeriod))
            {
                ExpirationPeriod = expirationPeriod;
            }
        }
    }
}
