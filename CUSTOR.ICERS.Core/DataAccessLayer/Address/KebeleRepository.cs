﻿using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class KebeleRepository
    {

        private readonly ECEADbContext _context;
        private readonly IDistributedCache _distributedCache;
        private readonly Settings _settings;

        public KebeleRepository(ECEADbContext context, IDistributedCache distributedCache, IConfiguration configuration)
        {
            _context = context;
            _distributedCache = distributedCache;
            _settings = new Settings(configuration);
        }

        // Get Kebele based on woreda id from server cache if there is no cache get from the database
        // and store for other time
        public async Task<IEnumerable<StaticData>> GetKebelesByWoredaId(string lang, int woredaId)
        {
            IEnumerable<StaticData> kebeles = null;

            string cacheKey = "Kebeles: " + woredaId;

            try
            {
                var cachedKebeles = await _distributedCache.GetStringAsync(cacheKey);

                if (cachedKebeles != null)
                {
                    kebeles = JsonConvert.DeserializeObject<IEnumerable<StaticData>>(cachedKebeles);
                }
                else
                {
                    kebeles = await _context.Kebele
                        .Where(k => k.WoredaId == woredaId && k.IsActive == true && k.IsDeleted == false)
                        .Select(k => new StaticData
                        {
                            Id = k.KebeleId,
                            Description = (lang == "et") ? k.DescriptionAmh : k.DescriptionEng
                        })
                        .ToListAsync();

                    DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));

                    await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(kebeles),
                        cacheOptions);
                }

                return kebeles;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> AddKebele(Kebele kebele)
        {
            _context.Kebele.Add(kebele);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Kebele>> GetKebelesByParentId(int woredaId)
        {
            var kebeles = await _context.Kebele
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.WoredaId == woredaId)
            .ToListAsync();
            return kebeles;
        }

        public async Task<bool> DeleteKebele(int kebeleId)
        {
            Kebele kebele = await GetKebele(kebeleId);

            if (kebele != null)
            {
                _context.Kebele.Remove(kebele);

                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<Kebele> GetKebele(int kebeleId)
        {
            var kebele = await _context.Kebele
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.KebeleId == kebeleId)
            .FirstOrDefaultAsync();
            return kebele;
        }

        public async Task<bool> UpdateKebele(Kebele kebele)
        {
            try
            {
                _context.Update(kebele);
                _context.Entry(kebele).Property(x => x.KebeleId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        //public async Task<Kebele> GetKebele(int kebeleId)
        //{
        //    var kebele = await _context.Kebele
        //    .Where(s => s.IsActive == true && s.IsDeleted == false && s.KebeleId == kebeleId)
        //    .FirstOrDefaultAsync();
        //    return kebele;
        //}
    }
}
