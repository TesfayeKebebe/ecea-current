﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class RegionRepository
    {
        private readonly ECEADbContext _context;
        private readonly IDistributedCache _distributedCache;
        private readonly Settings _settings;
        private readonly IMapper _mapper;

        public RegionRepository(ECEADbContext context, IDistributedCache distributedCache, IConfiguration configuration, IMapper mapper)
        {
            _context = context;
            _settings = new Settings(configuration);
            _distributedCache = distributedCache;
            _mapper = mapper;
        }

        // Get Region from server cache if there is no cache get from the database
        // and store for other time
        public async Task<IEnumerable<StaticData>> GetRegions(string lang)
        {
            IEnumerable<StaticData> regions = null;

            string cacheKey = "Regions";

            try
            {
                var chachedRegions = await _distributedCache.GetStringAsync(cacheKey);

                if (chachedRegions != null)
                {
                    regions = JsonConvert.DeserializeObject<IEnumerable<StaticData>>(chachedRegions);
                }
                else
                {
                    regions = await _context.Region
                        .Where(r => r.IsActive == true && r.IsDeleted == false)
                        .Select(r => new StaticData
                        {
                            Id = r.RegionId,
                            Description = (lang == "et") ? r.DescriptionAmh : r.DescriptionEng
                        })
                        .ToListAsync();

                    DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));

                    await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(regions),
                        cacheOptions);
                }

                return regions;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<Region> GetRegion(int regionId)
        {
            try
            {
                var region = await _context.Region
                .Where(s => s.IsActive == true && s.IsDeleted == false && s.RegionId == regionId)
                .FirstOrDefaultAsync();

                var RegionDTO = _mapper.Map<Region, RegionDTO>(region);

                return region;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
        public async Task<bool> UpdateRegion(Region region)
        {
            try
            {
                _context.Update(region);
                //context.Entry(region).Property(x => x.RegionId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }
        public async Task<bool> AddRegion(Region region)
        {
            try
            {
                _context.Add(region);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
        public async Task<bool> DeleteRegion(int regionId)
        {
            Region region = await GetRegion(regionId);

            try
            {
                if (region != null)
                {
                    _context.Region.Remove(region);

                    await _context.SaveChangesAsync();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
        public async Task<IEnumerable<Region>> GetRegionsWithNoCache()
        {
            try
            {
                var regions = await _context.Region
                            .Where(r => r.IsActive == true && r.IsDeleted == false)
                            .ToListAsync();


                return regions;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<IEnumerable<StaticData>> GetNonCacheRegion(string lang)
        {
            try
            {
                var regions = await _context.Region
                       .Where(r => r.IsActive == true && r.IsDeleted == false)
                       .Select(r => new StaticData
                       {
                           Id = r.RegionId,
                           Description = (lang == "et") ? r.DescriptionAmh : r.DescriptionEng
                       })
                       .ToListAsync();


                return regions;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
    }
}
