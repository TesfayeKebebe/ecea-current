﻿using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class WoredaRepository
    {
        private readonly ECEADbContext _context;
        private readonly IDistributedCache _distributedCache;
        private readonly Settings _settings;

        public WoredaRepository(ECEADbContext context, IDistributedCache distributedCache, IConfiguration configuration)
        {
            _context = context;
            _distributedCache = distributedCache;
            _settings = new Settings(configuration);
        }

        // Get Woreda from server cache if there is no cache get from the database
        // and store for other time
        public async Task<IEnumerable<StaticData2>> GetWoredas(string lang)
        {
            IEnumerable<StaticData2> woredas = null;

            string cacheKey = "Woredas";

            try
            {
                var cachedWoredas = await _distributedCache.GetStringAsync(cacheKey);

                if (cachedWoredas != null)
                {
                    woredas = JsonConvert.DeserializeObject<IEnumerable<StaticData2>>(cachedWoredas);
                }
                else
                {
                    woredas = await _context.Woreda
                        .Where(w => w.IsActive == true && w.IsDeleted == false)
                        .Select(w => new StaticData2
                        {
                            Id = w.WoredaId,
                            ParentId = w.ZoneId,
                            Description = (lang == "et") ? w.DescriptionAmh : w.DescriptionEng
                        })
                        .AsNoTracking().ToListAsync();

                    DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));

                    await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(woredas), cacheOptions);
                }

                return woredas;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> AddWoreda(Woreda woreda)
        {
            _context.Woreda.Add(woreda);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<Woreda>> GetWoredas()
        {
            var regions = await _context.Woreda
            .Where(r => r.IsActive == true && r.IsDeleted == false)
            .ToListAsync();
            return regions;
        }

        public async Task<List<Woreda>> GetWoredasByParentId(int zoneId)
        {
            var woredas = await _context.Woreda
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.ZoneId == zoneId)
            .ToListAsync();
            return woredas;
        }

        public async Task<List<StaticData>> GetWoredasByParentIdAndLang(int zoneId, string lang)
        {
            var woredas = await _context.Woreda
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.ZoneId == zoneId)
            .Select(w => new StaticData
            {
                Id = w.WoredaId,
                Description = lang == "et" ? w.DescriptionAmh : w.DescriptionEng

            }).AsNoTracking()
            .ToListAsync();
            return woredas;
        }

        public async Task<bool> DeleteWoreda(int woredaId)
        {
            Woreda woreda = await GetWoreda(woredaId);

            if (woreda != null)
            {
                _context.Woreda.Remove(woreda);

                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<Woreda> GetWoreda(int woredaId)
        {
            var woredas = await _context.Woreda
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.WoredaId == woredaId)
            .FirstOrDefaultAsync();
            return woredas;
        }

        public async Task<bool> UpdateWoreda(Woreda woreda)
        {
            try
            {
                _context.Update(woreda);
                _context.Entry(woreda).Property(x => x.WoredaId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

    }
}
