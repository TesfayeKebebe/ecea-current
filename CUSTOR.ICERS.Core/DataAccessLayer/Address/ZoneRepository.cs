﻿using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class ZoneRepository
    {
        private readonly ECEADbContext context;
        private readonly IDistributedCache distributedCache;
        private readonly Settings settings;

        public ZoneRepository(ECEADbContext _context, IDistributedCache _distributedCache, IConfiguration _configuration)
        {
            context = _context;
            distributedCache = _distributedCache;
            settings = new Settings(_configuration);
        }

        // Get Zone from server cache if there is no cache get from the database
        // and store for other time
        public async Task<IEnumerable<StaticData2>> GetZones(string lang)
        {
            IEnumerable<StaticData2> zones = null;

            string cacheKey = "Zones";

            try
            {
                var cachedZones = await distributedCache.GetStringAsync(cacheKey);

                if (cachedZones != null)
                {
                    zones = JsonConvert.DeserializeObject<IEnumerable<StaticData2>>(cachedZones);
                }
                else
                {
                    zones = await context.Zone
                        .Where(z => z.IsActive == true && z.IsDeleted == false)
                        .Select(z => new StaticData2
                        {
                            Id = z.ZoneId,
                            ParentId = z.RegionId,
                            Description = (lang == "et") ? z.DescriptionAmh : z.DescriptionEng
                        })
                        .AsNoTracking().ToListAsync();

                    DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(settings.ExpirationPeriod));

                    await distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(zones), cacheOptions);
                }

                return zones;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<Zone>> GetZones()
        {
            var regions = await context.Zone
            .Where(r => r.IsActive == true && r.IsDeleted == false)
            .ToListAsync();
            return regions;
        }

        public async Task<List<StaticData>> GetZonesByParentIdAndLang(int regionId, string lang)
        {
            var zones = await context.Zone
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.RegionId == regionId)
            .Select(z => new StaticData
            {
                Id = z.ZoneId,
                Description = (lang == "et") ? z.DescriptionAmh : z.DescriptionEng
            }).AsNoTracking()
            .ToListAsync();

            return zones;

        }

        public async Task<List<Zone>> GetZonesByParentId(int regionId)
        {
            try
            {
                var zones = await context.Zone
                .Where(s => s.IsActive == true && s.IsDeleted == false && s.RegionId == regionId)
                .ToListAsync();

                return zones;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> AddZone(Zone zone)
        {
            context.Zone.Add(zone);
            await context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<Zone>> GetZonesWithNoCache()
        {
            var regions = await context.Zone
            .Where(r => r.IsActive == true && r.IsDeleted == false)
            .ToListAsync();
            return regions;
        }

        public async Task<bool> DeleteZone(int zoneId)
        {
            Zone zone = await GetZone(zoneId);

            if (zone != null)
            {
                context.Zone.Remove(zone);

                await context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<Zone> GetZone(int zoneId)
        {
            var zone = await context.Zone
            .Where(s => s.IsActive == true && s.IsDeleted == false && s.ZoneId == zoneId)
            .FirstOrDefaultAsync();
            return zone;
        }

        public async Task<bool> UpdateZone(Zone zone)
        {
            try
            {
                context.Update(zone);
                // context.Entry(zone).Property(x => x.ZoneId).IsModified = false;
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }
    }
}
