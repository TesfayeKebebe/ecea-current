using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class AuditRepository
    {
        private readonly ECEADbContext _context;
        public List<AuditDMV> ListOfAudit { get; set; }

        // private readonly  IHttpContextAccessor _host;
        //public AuditRepository()
        //{

        //}
        public AuditRepository(ECEADbContext context)
        {
            _context = context;

        }



        //public static Audit CreateOdit(AuditDTO oditDTO)
        //{
        //  Audit odit = new Audit();
        //  odit.ActionResult = oditDTO.ActionResult;
        //  odit.CreatedDate = DateTime.Now;
        //  odit.Id = Guid.NewGuid();
        //  odit.Message = oditDTO.Message;
        //  odit.UserFullName = UserInformation.FullName;
        //  odit.UserId = oditDTO.UserId;
        //  return odit;
        //}

        //public async Task<List<AuditDMV>> GetOditData(string start, string end, string userName, string table)
        //{

        //  List<AuditDMV> addedOdit = new List<AuditDMV>();
        //  if (start !=    "null" && end !=    "null" && ActionId !=    "null" && ActionId!= "undefined" && table != "undefined" && table !=    null)
        //  {
        //    var oditList = await _context.Odit.Where(x => x.CreatedDate >= Convert.ToDateTime(start)).ToListAsync();
        //    return addedOdit = oditList.Select(o => new AuditDMV()
        //    {
        //      UserFullName = o.UserFullName

        //    }).ToList();
        //  }
        //  else
        //  {
        //    var oditList = await _context.Odit.Where(x => x.CreatedDate >= DateTime.Now).ToListAsync();
        //    return addedOdit = oditList.Select(o => new AuditDMV()
        //    {
        //      UserFullName = o.UserFullName,
        //      ActionResult = Enum.GetName(typeof(ActionResult), o.ActionResult),
        //      CreatedDate = EthiopicDateTime.GetEthiopicDate(o.CreatedDate.Day, o.CreatedDate.Month, o.CreatedDate.Year),
        //      Message = $"{ Enum.GetName(typeof(ActionResult), o.ActionResult) } {o.Message}"
        //    }).ToList();
        //  }

        //}

        public async Task<List<AuditDMV>> GetAuditByCriteria(string table, string userName, string ActionId, string start, string end)
        {
            if (start != "null" && end != "null" && ActionId != "null" && ActionId != "undefined" && table != null && table != "undefined" && userName != null && userName != "undefined")
            {

                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionBy_UserId ={1} and ActionType = {2} and  convert(date,ActionDateTime,111)= {3} and   convert(date,ActionDateTime,111) <= {4}", table, userName, ActionId, start, end).ToListAsync();

            }
            else if (start != "null" && end != "null" && ActionId != "null" && ActionId != "undefined" && table != null && table != "undefined")
            {

                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionType = {1} and ( convert(date,ActionDateTime,111)= {2} and    convert(date,ActionDateTime,111) <= {3})", table, ActionId, start, end).ToListAsync();

            }
            else if (start != "null" && end != "null" && ActionId != "null" && ActionId != "undefined" && userName != null && userName != "undefined")
            {

                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionBy_UserId={0} and ActionType = {1} and  convert(date,ActionDateTime,111)= {2} and   convert(date,ActionDateTime,111) <= {3}", userName, ActionId, start, end).ToListAsync();

            }
            else if (start != "null" && end != "null" && table != null && table != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and   convert(date,ActionDateTime,111)= {1} and   convert(date,ActionDateTime,111) <= {2}", table, start, end).ToListAsync();

            }
            else if (userName != null && userName != "undefined" && table != null && table != "undefined" && ActionId != "null" && ActionId != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionBy_UserId = {1} and ActionType = {2}  ", table, userName, ActionId).ToListAsync();

            }
      else if (userName != null && userName != "undefined" && table != null && table != "undefined" && ActionId != "null" && ActionId != "undefined" && start != "null")
      {
        ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionBy_UserId = {1} and ActionType = {2}  and  convert(date,ActionDateTime,111) = {3}  ", table, userName, ActionId, start).ToListAsync();

      }
      else if (table != null && table != "undefined" && ActionId != "null" && ActionId != "undefined" && start != "null")
      {
        ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0}  and ActionType = {1}  and  convert(date,ActionDateTime,111) = {2}  ", table,  ActionId, start).ToListAsync();

      }
      else if (start != "null" && end != "null" && userName != null && userName != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionBy_UserId={0} and   convert(date,ActionDateTime,111) = {1} ", userName, start).ToListAsync();

            }
            else if (start != "null" && end != "null" && ActionId != "null" && ActionId != "undefined")

            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where  ActionType = {0} and  convert(date,ActionDateTime,111)= {1} ", ActionId, start).ToListAsync();

            }

      
            else if (ActionId != "null" && ActionId != "undefined" && table != null && table != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionType = {1} ", table, ActionId).ToListAsync();

            }
            else if (userName != null && userName != "undefined" && table != null && table != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName,ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} and ActionBy_UserId = {1} ", table, userName).ToListAsync();

            }
            else if (start != "null" && ActionId != "null" && ActionId != "undefined")
            {
        ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionType = {0} and  convert(date,ActionDateTime,111) = {1} ", ActionId, start).ToListAsync();

            }
      else if (userName != null && userName != "undefined" && ActionId != "null" && ActionId != "undefined")
      {
        ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionType = {0} and ActionBy_UserId = {1} ", ActionId, userName).ToListAsync();

      }
      else if (start != "null" && userName != null && userName != "undefined")
      {
 

        ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionBy_UserId = {0} and  convert(date,ActionDateTime,111)= {1} ", ActionId, start).ToListAsync();

      }
      else if (start != "null" && end != "null")


      {
        try
        {
          ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where   convert(date,ActionDateTime,111)= {0} and   convert(date,ActionDateTime,111) <= {1}", start, end).ToListAsync();

        }
        catch (Exception ex)
        {
          throw new Exception(ex.Message);

        }

      }
      else if (table != null && table != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where TableName={0} ", table).ToListAsync();


            }
            else if (userName != null && userName != "undefined")
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where ActionBy_UserId={0} ", userName).ToListAsync();


            }
            else if (ActionId != "null" && ActionId != "undefined")
            {
        try
        {
          ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where  ActionType = {0} ", ActionId).ToListAsync();

        }
        catch (Exception ex)
        {

          throw new Exception(ex.Message);
        }
            
            }
            else if (start != "null")
            {
        try
        {
          ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName,  ActionDateTime,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where   convert(date,ActionDateTime,111)= {0}", start).ToListAsync();

        }
        catch (Exception ex )
        {
          throw new Exception(ex.Message);
         
        }
   
            }
            else
            {
                ListOfAudit = await _context.AuditViewModel.FromSqlRaw(@"select TableName, ColumnName,
ActionBy_UserId as UserFullName, ActionDateTime ,alt.ActionTypeDesc as ActionDescription,Audit_Description, OldValue, OldValue_Decode,
NewValue, NewValue_Decode from Audit.AuditLogData ald
join Audit.AuditLogActionType alt on alt.ActionTypeId = ald.ActionType where   convert(date,ActionDateTime,111) = {0} ", DateTime.Now.Date).ToListAsync();

            }
            return ListOfAudit;
        }
        public async Task<List<TableInformation>> GetAllTables()
        {
            return await _context.TableInformationModel.FromSqlRaw(@"SELECT TABLE_NAME as TableName FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA ='dbo'").ToListAsync();
        }
    }
}
