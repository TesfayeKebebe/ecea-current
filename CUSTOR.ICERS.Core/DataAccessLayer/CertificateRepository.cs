﻿using AutoMapper;
using CUSTOR.ICERS.Core.DataAccessLayer.Setting;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class CertificateRepository
    {

        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly GeneralSettingRepository _generalSettingRepository;

        public CertificateRepository(ECEADbContext context, IMapper mapper, GeneralSettingRepository generalSettingRepository)
        {
            _generalSettingRepository = generalSettingRepository;
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostCertificate(CertificateDTO postedCertificate)
        {

            ServiceApplication serviceApplication = await _context.ServiceApplication.SingleOrDefaultAsync(sa =>
            sa.IsActive == true && sa.IsDeleted == false && sa.ServiceApplicationId == postedCertificate.ServiceApplicationId);

            ExchangeActor exchangeActor = await _context.ExchangeActor.SingleOrDefaultAsync(ea => ea.IsDeleted == false && ea.ExchangeActorId == postedCertificate.ExchangeActorId);


            if (serviceApplication == null || exchangeActor == null)
            {
                return 0;
            }

            int serviceApplicationNextStep = (int)RecognitionServiceApplicaitonSteps.Certeficate;

            // fro service application
            serviceApplication.UpdatedDateTime = DateTime.Now;
            serviceApplication.IsFinished = true;
            serviceApplication.NextStep = serviceApplicationNextStep;
            serviceApplication.CurrentStep = serviceApplicationNextStep;
            serviceApplication.Status = (int)ServiceApplicaitonStatus.Completed; //finsished
            serviceApplication.ServiceEndDateTime = DateTime.Now;
            serviceApplication.UpdatedUserId = postedCertificate.CreatedUserId;


            // for certifcate
            Certificate certeficate = _mapper.Map<Certificate>(postedCertificate);

            certeficate.IssuedDateTime = DateTime.Now;
            certeficate.ExpireDateTime = getExpireDateOfCustomerRecognition(exchangeActor, certeficate);
            certeficate.CreatedUserId = postedCertificate.CreatedUserId;


            // set exchange actor status to Active
            exchangeActor.Status = (int)ExchangeActorStatus.Active;
            exchangeActor.IssueDateTime = certeficate.IssuedDateTime;
            exchangeActor.ExpireDateTime = certeficate.ExpireDateTime;
            exchangeActor.UpdatedUserId = postedCertificate.UpdatedUserId;


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Certificate.Add(certeficate);
                    _context.SaveChanges();
                    transaction.Commit();

                    return certeficate.CertificateId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }


        private DateTime getExpireDateOfCustomerRecognition(ExchangeActor exchangeActor, Certificate certificate)
        {
            PeriodicDurationSettingDTO generalSetting = _generalSettingRepository.GetGeneralSetting();

            DateTime recognitionExpirationTime = DateTime.Now;

            if (certificate.ServiceId == (int)ServiceType.Renewal)
            {

                if (exchangeActor.CustomerTypeId == (int)CustomerType.Representative || exchangeActor.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative)
                {
                    ExchangeActor delegator = (from ea in _context.ExchangeActor where ea.ExchangeActorId == exchangeActor.DelegatorId select ea).AsNoTracking().FirstOrDefault();

                    recognitionExpirationTime = delegator.ExpireDateTime.GetValueOrDefault();

                }
                else
                {
                    if (generalSetting.RenewalDurationUnit == (int)Enum.DaysMesurementUnit.Year)
                    {
                        recognitionExpirationTime = recognitionExpirationTime.AddYears(Convert.ToInt32(generalSetting.RenewalDuration));
                    }
                    else if (generalSetting.RenewalDurationUnit == (int)Enum.DaysMesurementUnit.Month)
                    {
                        recognitionExpirationTime = recognitionExpirationTime.AddMonths(Convert.ToInt32(generalSetting.RenewalDuration));
                    }
                    else
                    {
                        recognitionExpirationTime = recognitionExpirationTime.AddDays(Convert.ToInt32(generalSetting.RenewalDuration));
                    }
                }
            }
            else if ((exchangeActor.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative) || (exchangeActor.CustomerTypeId == (int)CustomerType.Representative))
            {
                ExchangeActor delegator = (from ea in _context.ExchangeActor where ea.ExchangeActorId == exchangeActor.DelegatorId select ea).AsNoTracking().FirstOrDefault();

                recognitionExpirationTime = delegator.ExpireDateTime.GetValueOrDefault();

            }
            else
            {
                if (generalSetting.RenewalDurationUnit == (int)Enum.DaysMesurementUnit.Year)
                {
                    recognitionExpirationTime = recognitionExpirationTime.AddYears(Convert.ToInt32(generalSetting.RenewalDuration));
                }
                else if (generalSetting.RenewalDurationUnit == (int)Enum.DaysMesurementUnit.Month)
                {
                    recognitionExpirationTime = recognitionExpirationTime.AddMonths(Convert.ToInt32(generalSetting.RenewalDuration));
                }
                else
                {
                    recognitionExpirationTime = recognitionExpirationTime.AddDays(Convert.ToInt32(generalSetting.RenewalDuration));
                }
            }

            return recognitionExpirationTime;
        }
    }
}
