﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class CommodityRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper mapper;

        public CommodityRepository(ECEADbContext context, IMapper _mapper)
        {
            _context = context;
            mapper = _mapper;
        }

        public async Task<List<StaticData>> GetAllCommodity(string lang)
        {
            List<StaticData> commodityList = null;
            try
            {
                commodityList = await _context.Commodity
                    .Select(c => new StaticData
                    {
                        Id = c.CommodityId,
                        Description = lang == "et" ? c.DescriptionAmh : c.DesciptionEng
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return commodityList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<int> Create(CommodityDTO commodity)
        {
            try
            {
                Commodity newCommodity = mapper.Map<Commodity>(commodity);
                _context.Add(newCommodity);
                await _context.SaveChangesAsync();
                return newCommodity.CommodityId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<int> Update(CommodityDTO reportPeriod)
        {
            try
            {
                var commodity = await _context.Commodity
                    .Where(x => x.CommodityId == reportPeriod.CommodityId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (commodity != null)
                {
                    var updateReportPeriod = mapper.Map(reportPeriod, commodity);
                    _context.Entry(updateReportPeriod).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                return commodity.CommodityId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Delete(CommodityDTO commodity)
        {
            try
            {
                var commodityDelet = await _context.Commodity
                                    .Where(x => x.CommodityId == commodity.CommodityId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (commodityDelet != null)
                {
                    var deleteReportPeriod = mapper.Map(commodity, commodityDelet);
                    _context.Entry(deleteReportPeriod).State = EntityState.Deleted;
                    await _context.SaveChangesAsync();
                }
                return commodityDelet.CommodityId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<CommodityDTO>> GetAllCommodityList(string lang)
        {
            try
            {
                var listOfReportPeriod = await _context.Commodity
                    .Select(n => new CommodityDTO
                    {
                        CommodityId = n.CommodityId,
                        DescriptionAmh = lang == "et" ? n.DescriptionAmh : n.DesciptionEng,
                        CommodityUnitDesc = lang == "et" ? n.UnitMeasurement.DescriptionAmh : n.UnitMeasurement.DescriptionEng,
                        PriceTollerance = n.PriceTollerance,
                        WeightedAverage = n.WeightedAverage
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return listOfReportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Commodity> GetCommodityById(int commodityId)
        {
            try
            {
                var reportPrd = await _context.Commodity
                    .Where(x => x.CommodityId == commodityId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
