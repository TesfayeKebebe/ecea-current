using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class CustomerPrerequisiteRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;


        public CustomerPrerequisiteRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        public async Task<int> PostCustomerPrerequisite(CustomerPrerequisiteDTO[] postedCustomerPrerequisite)
        {

            try
            {
                List<CustomerPrerequisite> customerPrerequisite = null;

                customerPrerequisite = _mapper.Map<CustomerPrerequisiteDTO[], List<CustomerPrerequisite>>(postedCustomerPrerequisite);

                _context.CustomerPrerequisite.AddRange(customerPrerequisite);

                await _context.SaveChangesAsync();

                return 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<CustomerPrerequisite> GetCustomerPrerequisite(Guid saId, int serPreId)
        {
            try
            {
                var customerPrerequisite = await _context.CustomerPrerequisite.FirstOrDefaultAsync(cp => cp.ServiceApplicationId == saId && cp.ServicePrerequisiteId == serPreId);

                return customerPrerequisite;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<CustomerPrerequisite> UploadPrerequisiteDocument(PrerequisteDocumentDTO documentTobeUploaded, string fileName, string filePath, string pathToSave)
        {
            try
            {
                CustomerPrerequisite uploadedprerequisite = null;

                var existingdocument = await _context.CustomerPrerequisite.FirstOrDefaultAsync(d => d.ServicePrerequisiteId == documentTobeUploaded.ServicePrerequisiteId && d.ServiceApplicationId == documentTobeUploaded.ServiceApplicationId);

                if (existingdocument == null)
                {
                    uploadedprerequisite = new CustomerPrerequisite()
                    {
                        ServicePrerequisiteId = documentTobeUploaded.ServicePrerequisiteId,
                        ServiceApplicationId = documentTobeUploaded.ServiceApplicationId,
                        FileName = fileName,
                        Title = documentTobeUploaded.Name

                    };

                    _context.CustomerPrerequisite.Add(uploadedprerequisite);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    existingdocument = _mapper.Map(uploadedprerequisite, existingdocument);
                    uploadedprerequisite = existingdocument;
                    await _context.SaveChangesAsync();
                }

                if (!Directory.Exists(pathToSave))
                    Directory.CreateDirectory(pathToSave);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await documentTobeUploaded.File.CopyToAsync(stream);
                }

                return uploadedprerequisite;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<CustomerPrerequisite> DeletePrerequisiteWithDocument(int servicePrerequisiteId, string fileName, Guid serviceApplicationId)
        {

            try
            {

                var document = await _context.CustomerPrerequisite.SingleOrDefaultAsync(dp => dp.ServicePrerequisiteId == servicePrerequisiteId && dp.ServiceApplicationId == serviceApplicationId);
                _context.CustomerPrerequisite.Remove(document);
                await _context.SaveChangesAsync();

                // Remove the file
                string deletionPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Documents");
                string filePath = Path.Combine(deletionPath, fileName);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                return document;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<bool> DeletePrerequisite(int servicePrerequisiteId, Guid serviceApplicationId)
        {

            try
            {

                var document = await _context.CustomerPrerequisite.SingleOrDefaultAsync(dp => dp.ServicePrerequisiteId == servicePrerequisiteId && dp.ServiceApplicationId == serviceApplicationId);
                if (document != null)
                {
                    _context.CustomerPrerequisite.Remove(document);
                    await _context.SaveChangesAsync();

                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
    }
}
