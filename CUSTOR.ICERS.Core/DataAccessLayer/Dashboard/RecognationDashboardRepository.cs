using CUSTOR.ICERS.Core.EntityLayer.Dashboard;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Dashboard
{
    public class RecognationDashboardRepository
    {
        private readonly ECEADbContext context;
        private readonly OversightReportRepo.OversightReportRepo _OversightReportRepo;


        public RecognationDashboardRepository(ECEADbContext _context, OversightReportRepo.OversightReportRepo _oversightReportRepo )
        {
            context = _context;
            _OversightReportRepo = _oversightReportRepo;

        }
        public async Task<List<DashboardModel>> GetTotalCountInRecognation()
        {
            List<DashboardModel> recognationDashBoard = new List<DashboardModel>();
            try
            {
                var notFinisedCancellation = await (from ove in context.Cancellation
                                                    join details in context.ServiceApplication
                                                    on ove.ServiceApplicationId equals details.ServiceApplicationId
                                                    select details).ToListAsync();
                var notFinisedRenewal = await (from ove in context.Renewal
                                                    join details in context.ServiceApplication
                                                    on ove.ServiceApplicationId equals details.ServiceApplicationId
                                                    select details).ToListAsync();
                var notFinisedReplacment = await (from ove in context.Replacement
                                                    join details in context.ServiceApplication
                                                    on ove.ServiceApplicationId equals details.ServiceApplicationId
                                                    select details).ToListAsync();
                var totalRenewal = context.Renewal.Count(x=>x.RenewalId==x.RenewalId);
                var totalCancellation =context.Cancellation.Count(x => x.CancellationId == x.CancellationId);
                var totalReplacment = context.Replacement.Count(x => x.ReplacementId == x.ReplacementId);
        var listOfActive =  context.ExchangeActor.Where(x => x.Status == 20).Count();
                var listOfApprovedInjection =  context.ExchangeActor.Where(x => x.Status == 31).Count();
                var listCancellation =  context.ExchangeActor.Where(x => x.Status == 64).Count();
                var listOfDraftInjection =  context.ExchangeActor.Where(x => x.Status == 114).Count();
                var listPreCancellation =  context.ExchangeActor.Where(x => x.Status == 64).Count();
                var listOfNewCase =  context.CaseApplication.Where(x => x.CaseStatusTypeId == 1).Count();

                DashboardModel dashboardModel = new DashboardModel();
                dashboardModel.TotalActiveExchangactor = listOfActive;
                dashboardModel.TotalApprovedInjection = listOfApprovedInjection;
                dashboardModel.TotalCancellation = listCancellation;
                dashboardModel.TotalNonApprovedInjection = listCancellation;
                dashboardModel.TotalPrecancellation = listPreCancellation;
                dashboardModel.notFinisedCancellation = notFinisedCancellation.Count(x => x.Status == 64);               
                dashboardModel.notFinisedRenewal = notFinisedRenewal.Count(x => x.Status == 64);
                dashboardModel.notFinisedReplacment = notFinisedReplacment.Count(x => x.Status == 64);
                dashboardModel.totalCancellation = totalCancellation;
                dashboardModel.totalRenewal = totalRenewal;
                dashboardModel.totalReplacment = totalReplacment;
                dashboardModel.totalNewCase = listOfNewCase;

                recognationDashBoard.Add(dashboardModel);
                return recognationDashBoard;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    public async Task<CommonModel> GetCommon()
    {
      try
      {
      
  
        var listOfActive = context.ExchangeActor.Where(x => x.Status == 20).Count();
        var listOfNewCase = context.CaseApplication.Where(x => x.CaseStatusTypeId == 1).Count();

        CommonModel dashboardModel = new CommonModel();
        dashboardModel.Recognition = listOfActive;
        dashboardModel.Case = listOfNewCase;
        dashboardModel.OverSite = context.OversightFollowUpReport
                                             .Select(f => f.FollowUpId).Count();
        return dashboardModel;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public async Task<List<DashboardLawModel>> GetTotalCountInLawCase()
        {
            List<DashboardLawModel> lawDashBoard = new List<DashboardLawModel>();
            try
            {
                //law enforcment CaseStatusTypeId
                var listOfNewCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 1);
                var listOfReporterClosedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 2);
                var listOfInvestigatorClosedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 5);
                var listOfInvestigatorHeadSelectedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 6);
                var listOfInvestigatorHeadClosedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 7);
                var listOfLawEnDirectorClosedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 10);
                var listOfLawEnDirectorIncompleteCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 11);
                var listOfInvestigationSelectedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 12);
                var listOfLawEnMainDirectorClosedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 10);
                var listOfLawEnMainDirectorIncompleteCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 11);
                var listOfCaseTeamIncompleteCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 26);
                var listOfCaseTeamApprovedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 27);

                DashboardLawModel dashboardLawModel = new DashboardLawModel();
                dashboardLawModel.TotalNewCase = listOfNewCase;
                dashboardLawModel.TotalReporterClosedCase = listOfReporterClosedCase; 
                dashboardLawModel.TotalInvestigatorClosedCase = listOfInvestigatorClosedCase;
                dashboardLawModel.TotalInvestigatorHeadSelectedCase = listOfInvestigatorHeadSelectedCase;
                dashboardLawModel.TotalInvestigatorHeadClosedCase = listOfInvestigatorHeadClosedCase;
                dashboardLawModel.TotalLawEnDirectorClosedCase = listOfLawEnDirectorClosedCase;
                dashboardLawModel.TotalLawEnDirectorIncompleteCase = listOfLawEnDirectorIncompleteCase;
                dashboardLawModel.TotalInvestigationSelectedCase = listOfInvestigationSelectedCase;
                dashboardLawModel.TotalCaseTeamIncompleteCase = listOfCaseTeamIncompleteCase;
                dashboardLawModel.TotalCaseTeamApprovedCase = listOfCaseTeamApprovedCase;
                lawDashBoard.Add(dashboardLawModel);
                return lawDashBoard;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    //public async Task<List<CaseStatusModel>> GetLawEnforcementCaseStatus()
    //{
    //  List<CaseStatusModel> lawDashBoard = new List<CaseStatusModel>();
    //  try
    //  {
    //    //law enforcment CaseStatusTypeId
    //    var listOfNewCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 1);
    //    var listOfReporterClosedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 2);
    //    var listOfInvestigatorClosedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 5);
    //    var listOfInvestigatorHeadSelectedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 6);
    //    var listOfInvestigatorHeadClosedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 7);
    //    var listOfLawEnDirectorClosedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 10);
    //    var listOfLawEnDirectorIncompleteCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 11);
    //    var listOfInvestigationSelectedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 12);
    //    var listOfLawEnMainDirectorClosedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 10);
    //    var listOfLawEnMainDirectorIncompleteCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 11);
    //    var listOfCaseTeamIncompleteCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 26);
    //    var listOfCaseTeamApprovedCase = context.CaseApplication.Count(x => x.CaseStatusTypeId == 27);

    //    CaseStatusModel dashboardLawModel = new CaseStatusModel();
        
    //    dashboardLawModel.TotalNewCase = listOfNewCase;
    //    dashboardLawModel.TotalReporterClosedCase = listOfReporterClosedCase;
    //    dashboardLawModel.TotalInvestigatorClosedCase = listOfInvestigatorClosedCase;
    //    dashboardLawModel.TotalInvestigatorHeadSelectedCase = listOfInvestigatorHeadSelectedCase;
    //    dashboardLawModel.TotalInvestigatorHeadClosedCase = listOfInvestigatorHeadClosedCase;
    //    dashboardLawModel.TotalLawEnDirectorClosedCase = listOfLawEnDirectorClosedCase;
    //    dashboardLawModel.TotalLawEnDirectorIncompleteCase = listOfLawEnDirectorIncompleteCase;
    //    dashboardLawModel.TotalInvestigationSelectedCase = listOfInvestigationSelectedCase;
    //    dashboardLawModel.TotalCaseTeamIncompleteCase = listOfCaseTeamIncompleteCase;
    //    dashboardLawModel.TotalCaseTeamApprovedCase = listOfCaseTeamApprovedCase;
    //    lawDashBoard.Add(dashboardLawModel);
    //    return lawDashBoard;
    //  }
    //  catch (Exception ex)
    //  {
    //    throw new Exception(ex.Message);
    //  }
    //}

    public async Task<List<DashboardAtModel>> GetTotalCountInAtCase() 
        {
            List<DashboardAtModel> atDashBoard = new List<DashboardAtModel>();
            try
            {
                var listOfRegistrarApproved =  context.CaseApplication.Count(x => x.CaseStatusTypeId== 29);
                var listOfRegistrarClosed =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 30);
                var listOfAppointment =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 31);
                var listOfCreatedCase =  context.CaseApplication.Count(x => x.CaseStatusTypeId == 34);

                DashboardAtModel dashboardAtModel = new DashboardAtModel();
                dashboardAtModel.TotalRegistrarApproved = listOfRegistrarApproved;
                dashboardAtModel.TotalRegistrarClosed = listOfRegistrarClosed;
                dashboardAtModel.TotalAppointment = listOfAppointment;
                dashboardAtModel.TotalCreatedCase = listOfCreatedCase;
                atDashBoard.Add(dashboardAtModel);
                return atDashBoard;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
