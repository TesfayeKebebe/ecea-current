﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace CUSTOR.ICERS.Core.DataAccessLayer.DataChanges
{
    public class AddressChangeRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public AddressChangeRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IActionResult> SaveAddressChange(AddressChangeDTO addressChangeData)
        {

            var submitedAddressChangeData = await FillterChangedData(addressChangeData);

            // create serviceapplication

            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = (int)DataChangeServiceApplicaiton.ChangeInformation,
                NextStep = (int)DataChangeServiceApplicaiton.ChangePrerequiste,
                ServiceId = (int)ServiceType.AddressChange,
                IsFinished = false,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                CustomerTypeId = addressChangeData.CustomerTypeId,
                ExchangeActorId = addressChangeData.ExchangeActorId
            };

            submitedAddressChangeData.ServiceApplicationId = newServiceApplication.ServiceApplicationId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    DataChange addressChange = null;

                    addressChange = _mapper.Map<DataChange>(submitedAddressChangeData);


                    _context.ServiceApplication.Add(newServiceApplication);

                    _context.DataChange.Add(addressChange);

                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return new ObjectResult(_mapper.Map<ServiceApplicationDTO>(newServiceApplication));


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }


        public async Task<bool> SaveManagerAddressChange(AddressChangeDTO addressChangeData)
        {

            OwnerManager manager = await _context.OwnerManager.FirstOrDefaultAsync(man => man.ExchangeActorId == addressChangeData.ExchangeActorId && man.IsManager == true); // add isOwner = true

            if (manager == null)
            {
                return false;
            }

            // update all address related values
            //manager.Email = addressChangeData.Email;
            //manager.RegionId = addressChangeData.RegionId;
            //manager.ZoneId = addressChangeData.ZoneId;
            //manager.WoredaId = addressChangeData.WoredaId;
            //manager.KebeleId = addressChangeData.KebeleId;
            //manager.MobileNo = addressChangeData.MobileNo;
            //manager.Tel = addressChangeData.Tel;
            //manager.Fax = addressChangeData.FaxNo;
            //manager.Pobox = addressChangeData.Pobox;
            //manager.HouseNo = addressChangeData.HouseNo;
            

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    DataChange addressChange = null;

                    addressChange = _mapper.Map<DataChange>(addressChangeData);

                    _context.DataChange.Add(addressChange);

                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        public async Task<AddressChangeDTO> GetOrganizationAddressChangeData(Guid serviceApplicationId)
        {
            try
            {

                var queryResult = await (from sa in _context.ServiceApplication
                                  join dc in _context.DataChange on sa.ServiceApplicationId equals dc.ServiceApplicationId
                                  join ea in _context.ExchangeActor on sa.ExchangeActorId equals ea.ExchangeActorId
                                  where sa.ServiceApplicationId == serviceApplicationId
                                  select new AddressChangeDTO
                                  {
                                      RegionId = dc.RegionId == null ? ea.RegionId : dc.RegionId,
                                      OldRegionId = ea.RegionId,
                                      ZoneId = dc.ZoneId == null ? ea.ZoneId : dc.ZoneId,
                                      OldZoneId = ea.ZoneId,
                                      WoredaId = dc.WoredaId == null ? ea.WoredaId : dc.WoredaId,
                                      OldWoredaId = ea.WoredaId,
                                      KebeleId = dc.KebeleId == null ? ea.KebeleId : dc.KebeleId,
                                      OldKebeleId = dc.KebeleId,
                                      HouseNo = dc.HouseNo == null ? ea.HouseNo : dc.HouseNo,
                                      OldHouseNo = ea.HouseNo,
                                      Email = dc.Email == null ? ea.Email : dc.Email,
                                      OldEmail = ea.Email,
                                      Tel = dc.Tel == null ? ea.Tel : dc.Tel,
                                      OldTel = ea.Tel,
                                      MobileNo = dc.MobileNo == null ? ea.MobileNo : dc.MobileNo,
                                      OldMobileNo = ea.MobileNo,
                                      FaxNo = dc.FaxNo == null ? ea.FaxNo : dc.FaxNo,
                                      OldFaxNo = ea.FaxNo,
                                      Pobox = dc.Pobox == null ? ea.Pobox : dc.Pobox,
                                      OldPobox = ea.Pobox,
                                      OrganizationAreaNameEng = dc.OrganizationAreaNameEng == null ? ea.OrganizationAreaNameEng : dc.OrganizationAreaNameEng,
                                      OldOrganizationAreaNameEng = ea.OrganizationAreaNameEng,
                                      OrganizationAreaNameAmh = dc.OrganizationAreaNameAmh == null ? ea.OrganizationAreaNameAmh : dc.OrganizationAreaNameAmh,
                                      OldOrganizationAreaNameAmh = ea.OrganizationAreaNameAmh,
                                      OldRegion = ea.Region.DescriptionAmh,
                                      OldZone = ea.Zone.DescriptionAmh,
                                      OldWoreda = ea.Woreda.DescriptionAmh,
                                      OldKebele = ea.Kebele.DescriptionAmh
                                  }).FirstOrDefaultAsync();

                return queryResult;

            }catch(Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<AddressChangeDTO> FillterChangedData(AddressChangeDTO addressChangeData)
        {

            
            ExchangeActor exchangeActorAddress = null;

            OwnerManager ownerManagerAddress = null;

            if(addressChangeData.DataChangeTypeId == (int)DataChangeType.OrganizationAddressChange || addressChangeData.DataChangeTypeId == (int)DataChangeType.FainacialInsititutionAddressChange)
            {
                exchangeActorAddress = await _context.ExchangeActor.FindAsync(addressChangeData.ExchangeActorId);

                if(exchangeActorAddress.Email != addressChangeData.Email)
                {
                    addressChangeData.OldEmail = exchangeActorAddress.Email;
                } else
                {
                    addressChangeData.Email = null;
                }

                if (exchangeActorAddress.RegionId != addressChangeData.RegionId)
                {
                    addressChangeData.OldRegionId = exchangeActorAddress.RegionId;
                }
                else
                {
                    addressChangeData.RegionId = null;
                }

                if (exchangeActorAddress.WoredaId != addressChangeData.WoredaId)
                {
                    addressChangeData.OldWoredaId = exchangeActorAddress.WoredaId;
                }
                else
                {
                    addressChangeData.WoredaId = null;
                }


                if (exchangeActorAddress.ZoneId != addressChangeData.ZoneId)
                {
                    addressChangeData.OldZoneId = exchangeActorAddress.ZoneId;
                }
                else
                {
                    addressChangeData.ZoneId = null;
                }

                if (exchangeActorAddress.KebeleId != addressChangeData.KebeleId)
                {
                    addressChangeData.OldKebeleId = exchangeActorAddress.KebeleId;
                }
                else
                {
                    addressChangeData.KebeleId = null;
                }


                if (exchangeActorAddress.MobileNo != addressChangeData.MobileNo)
                {
                    addressChangeData.OldMobileNo = exchangeActorAddress.MobileNo;
                }
                else
                {
                    addressChangeData.MobileNo = null;
                }

                if (exchangeActorAddress.Tel != addressChangeData.Tel)
                {
                    addressChangeData.OldTel = exchangeActorAddress.Tel;
                }
                else
                {
                    addressChangeData.Tel = null;
                }

                if (exchangeActorAddress.FaxNo != addressChangeData.FaxNo)
                {
                    addressChangeData.OldFaxNo = exchangeActorAddress.FaxNo;
                }
                else
                {
                    addressChangeData.FaxNo = null;
                }

                if (exchangeActorAddress.Pobox != addressChangeData.Pobox)
                {
                    addressChangeData.OldPobox = exchangeActorAddress.Pobox;
                }
                else
                {
                    addressChangeData.Pobox = null;
                }
              

                if (exchangeActorAddress.HouseNo != addressChangeData.HouseNo)
                {
                    addressChangeData.OldHouseNo = exchangeActorAddress.HouseNo;
                }
                else
                {
                    addressChangeData.HouseNo = null;
                }
            } 


            if(addressChangeData.DataChangeTypeId == (int)DataChangeType.OrganizationAddressChange)
            {
                if (exchangeActorAddress.OrganizationAreaNameEng.Trim() != addressChangeData.OrganizationAreaNameEng.Trim())
                {
                    addressChangeData.OldOrganizationAreaNameEng = exchangeActorAddress.OrganizationAreaNameEng;
                }
                else
                {
                    addressChangeData.OrganizationAreaNameEng = null;
                }

                if (exchangeActorAddress.OrganizationAreaNameAmh.Trim() != addressChangeData.OrganizationAreaNameAmh.Trim())
                {
                    addressChangeData.OldOrganizationAreaNameAmh = exchangeActorAddress.OrganizationAreaNameAmh;
                }
                else
                {
                    addressChangeData.OrganizationAreaNameAmh = null;
                }
            }
            

            if(addressChangeData.DataChangeTypeId == (int)DataChangeType.FainacialInsititutionAddressChange)
            {
                if (exchangeActorAddress.HeadOffice.Trim() != addressChangeData.HeadOffice.Trim())
                {
                    addressChangeData.OldHeadOffice = exchangeActorAddress.HeadOffice;
                }
                else
                {
                    addressChangeData.HeadOffice = null;
                }

                if (exchangeActorAddress.HeadOfficeAmh.Trim() != addressChangeData.HeadOfficeAmh.Trim())
                {
                    addressChangeData.OldHeadOfficeAmh = exchangeActorAddress.HeadOfficeAmh;
                }
                else
                {
                    addressChangeData.HeadOfficeAmh = null;
                }
            }
            
            if(addressChangeData.DataChangeTypeId == (int)DataChangeType.ManagerAddressChange)
            {

                ownerManagerAddress = await _context.OwnerManager.FindAsync(addressChangeData.ExchangeActorId); // check if it is manager

                if(ownerManagerAddress.Email != addressChangeData.Email)
                {
                    addressChangeData.OldEmail = ownerManagerAddress.Email;
                }else
                {
                    addressChangeData.Email = null;
                }

                if (ownerManagerAddress.RegionId != addressChangeData.RegionId)
                {
                    addressChangeData.OldRegionId = ownerManagerAddress.RegionId;
                }
                else
                {
                    addressChangeData.RegionId = null;
                }

                if (ownerManagerAddress.ZoneId != addressChangeData.ZoneId)
                {
                    addressChangeData.OldZoneId = ownerManagerAddress.ZoneId;
                }
                else
                {
                    addressChangeData.ZoneId = null;
                }

                if (ownerManagerAddress.WoredaId != addressChangeData.WoredaId)
                {
                    addressChangeData.OldWoredaId = ownerManagerAddress.WoredaId;
                }
                else
                {
                    addressChangeData.WoredaId = null;
                }

                if (ownerManagerAddress.KebeleId != addressChangeData.KebeleId)
                {
                    addressChangeData.OldKebeleId = ownerManagerAddress.KebeleId;
                }
                else
                {
                    addressChangeData.KebeleId = null;
                }

                if (ownerManagerAddress.MobileNo != addressChangeData.MobileNo)
                {
                    addressChangeData.OldMobileNo = ownerManagerAddress.MobileNo;
                }
                else
                {
                    addressChangeData.MobileNo = null;
                }

                if (ownerManagerAddress.Tel != addressChangeData.Tel)
                {
                    addressChangeData.OldTel = ownerManagerAddress.Tel;
                }
                else
                {
                    addressChangeData.Tel = null;
                }

                if (ownerManagerAddress.Fax != addressChangeData.FaxNo)
                {
                    addressChangeData.OldFaxNo = ownerManagerAddress.Fax;
                }
                else
                {
                    addressChangeData.FaxNo = null;
                }

                if (ownerManagerAddress.Pobox != addressChangeData.Pobox)
                {
                    addressChangeData.OldPobox = ownerManagerAddress.Pobox;
                }
                else
                {
                    addressChangeData.Pobox = null;
                }

                if (ownerManagerAddress.HouseNo != addressChangeData.HouseNo)
                {
                    addressChangeData.OldHouseNo = ownerManagerAddress.HouseNo;
                }
                else
                {
                    addressChangeData.HouseNo = null;
                }

            }

            return addressChangeData;
        }

        public async Task<bool> SaveApprovedAddressChange(Guid serviceApplicationId, Guid exchangeActorId)
        {
            var changeRequest = await (from sa in _context.ServiceApplication
                                            join dc in _context.DataChange on sa.ServiceApplicationId equals dc.ServiceApplicationId
                                            join ea in _context.ExchangeActor on sa.ExchangeActorId equals ea.ExchangeActorId
                                            where sa.ServiceApplicationId == serviceApplicationId
                                            select new AddressChangeDTO
                                            {
                                                RegionId = dc.OldRegionId != dc.RegionId ? dc.RegionId : ea.RegionId,
                                                ZoneId = dc.OldZoneId != dc.ZoneId ?  dc.ZoneId : ea.ZoneId,
                                                WoredaId = dc.OldWoredaId != dc.WoredaId ? dc.WoredaId : ea.WoredaId,
                                                KebeleId = dc.OldKebeleId != dc.KebeleId ? dc.KebeleId : ea.KebeleId,
                                                HouseNo =  dc.OldHouseNo != dc.HouseNo ? dc.HouseNo : ea.HouseNo,
                                                Email = dc.OldEmail != dc.Email ? dc.Email : ea.Email,
                                                Tel = dc.OldTel != dc.Tel ? dc.Tel : ea.Tel,
                                                MobileNo = dc.OldMobileNo != dc.MobileNo ? dc.MobileNo : ea.MobileNo,
                                                FaxNo =  dc.OldFaxNo != dc.FaxNo ? dc.FaxNo : ea.FaxNo,
                                                Pobox = dc.OldPobox != dc.Pobox ?  dc.Pobox : ea.Pobox,
                                                OrganizationAreaNameEng = dc.OldOrganizationAreaNameEng != dc.OrganizationAreaNameEng ? dc.OrganizationAreaNameEng : ea.OrganizationAreaNameEng,
                                                OrganizationAreaNameAmh = dc.OldOrganizationAreaNameAmh != dc.OrganizationAreaNameAmh ? dc.OrganizationAreaNameAmh: ea.OrganizationAreaNameAmh,
                                            }).AsNoTracking()
                                              .FirstOrDefaultAsync();


            //var currentAddress = await (from ea in _context.ExchangeActor
            //                                where ea.ExchangeActorId == exchangeActorId
            //                                select new AddressChangeDTO
            //                                {
            //                                    RegionId = ea.RegionId,
            //                                    ZoneId = ea.ZoneId,
            //                                    WoredaId = ea.WoredaId,
            //                                    KebeleId = ea.KebeleId,
            //                                    HouseNo = ea.HouseNo,
            //                                    Email = ea.Email,
            //                                    Tel = ea.Tel,
            //                                    MobileNo = ea.MobileNo,
            //                                    FaxNo = ea.FaxNo,
            //                                    Pobox = ea.Pobox,
            //                                    OrganizationAreaNameEng = ea.OrganizationAreaNameEng,
            //                                    OrganizationAreaNameAmh = ea.OrganizationAreaNameAmh,
            //                                }).FirstOrDefaultAsync();

            ExchangeActor currentAddress = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == exchangeActorId).FirstOrDefaultAsync();




            currentAddress.RegionId = changeRequest.RegionId;
            currentAddress.ZoneId = changeRequest.ZoneId;
            currentAddress.WoredaId = changeRequest.WoredaId;
            currentAddress.KebeleId = changeRequest.KebeleId;
            currentAddress.HouseNo = changeRequest.HouseNo;
            currentAddress.Email = changeRequest.Email;
            currentAddress.Tel = changeRequest.Tel;
            currentAddress.MobileNo = changeRequest.MobileNo;
            currentAddress.FaxNo = changeRequest.FaxNo;
            currentAddress.Pobox = changeRequest.Pobox;
            currentAddress.OrganizationAreaNameAmh = changeRequest.OrganizationAreaNameAmh;
            currentAddress.OrganizationAreaNameEng = changeRequest.OrganizationAreaNameEng;

            // update service application data

            ServiceApplication serviceApplication = await _context.ServiceApplication.Where(sa => sa.ServiceApplicationId == serviceApplicationId).FirstOrDefaultAsync();

            int serviceApplicationNextStep = (int)DataChangeServiceApplicaiton.ChangePrerequiste;

            serviceApplication.UpdatedDateTime = DateTime.Now;
            serviceApplication.Status = (int)ServiceApplicaitonStatus.Completed;
            serviceApplication.ServiceEndDateTime = DateTime.Now;
            serviceApplication.IsFinished = true;
            serviceApplication.NextStep = serviceApplicationNextStep;
            serviceApplication.CurrentStep = serviceApplicationNextStep;


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    // _context.Entry(currentAddress).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        
    }
}
