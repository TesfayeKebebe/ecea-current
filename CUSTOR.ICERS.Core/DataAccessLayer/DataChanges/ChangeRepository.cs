﻿using AutoMapper;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.DataChanges
{
    public class ChangeRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ChangeRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public  async Task<bool> SaveAddressChange()
        {

            /*
             * steps to save address change value
             * 1. prepare the model
             * 2. save both at the change table and exchagne actor table (use transaction)
             * 
             * 
             */

            return false;
        }

        // TODO Chnage this one to query parmetter 
        // Refactor the search to work with language selection type
        //public async Task<List<ChangeDTO>> GetAllDataChange()
        //{
        //    List<ChangeDTO> dataChangeList = null;

        //    try
        //    {
        //        dataChangeList = await (from change in _context.DataChanges
        //                                join exchangeActor in _context.ExchangeActor on change.ExchangeActorId equals exchangeActor.ExchangeActorId
        //                                join lookup in _context.Lookup on change.ChangeType equals lookup.LookupId
        //                                where change.IsActive == true && change.IsDeleted == false
        //                                select new ChangeDTO
        //                                {
        //                                    FullName = exchangeActor.OwnerManager.FirstNameAmh + " " + exchangeActor.OwnerManager.FatherNameAmh + " " +
        //                                    exchangeActor.OwnerManager.GrandFatherNameAmh,
        //                                    ChangeType = change.ChangeType,
        //                                    IsApproved = change.IsApproved,
        //                                    StartDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(change.CreatedDateTime).ToString()),
        //                                    ExchangeActorId = exchangeActor.ExchangeActorId,
        //                                    DataChangeId = change.DataChangeId,
        //                                    ChangeTypeDescription = lookup.DescriptionAmh
        //                                })
        //                                .AsNoTracking()
        //                                .ToListAsync();

        //        return dataChangeList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.InnerException.ToString());
        //    }
        //}
    }
}
