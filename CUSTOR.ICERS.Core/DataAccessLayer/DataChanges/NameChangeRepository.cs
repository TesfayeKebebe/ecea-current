﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using CUSTOR.ICERS.Core.EntityLayer.Registration;

namespace CUSTOR.ICERS.Core.DataAccessLayer.DataChanges
{
    public class NameChangeRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public NameChangeRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<AddressChangeDTO> GetCustomerName(Guid exchangeActorId)
        {

            
                AddressChangeDTO companyName = await (from ea in _context.ExchangeActor
                                                      where ea.ExchangeActorId == exchangeActorId
                                                      select new AddressChangeDTO
                                                      {
                                                          OrganizationNameAmh = ea.OrganizationNameAmh,
                                                          OrganizationNameEng = ea.OrganizationNameEng,
                                                          OldOrganizationNameAmh = ea.OrganizationNameAmh,
                                                          OldOrganizationNameEng = ea.OrganizationNameEng

                                                      }).AsNoTracking()
                                                        .FirstOrDefaultAsync();

                return companyName;
        }

        public async Task<IActionResult> SaveNameChange(AddressChangeDTO nameChangeData)
        {

            var exchangeActorName = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == nameChangeData.ExchangeActorId).AsNoTracking().FirstOrDefaultAsync();

            if (exchangeActorName.OrganizationNameAmh != nameChangeData.OrganizationNameAmh)
            {
                nameChangeData.OldOrganizationNameAmh = exchangeActorName.OrganizationNameAmh;
            } else
            {
                nameChangeData.OrganizationNameAmh = null;
            }

            if(exchangeActorName.OrganizationNameEng != nameChangeData.OrganizationNameEng)
            {
                nameChangeData.OldOrganizationNameEng = exchangeActorName.OrganizationNameEng;
            } else
            {
                nameChangeData.OrganizationNameEng = null;
            }


            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = (int)DataChangeServiceApplicaiton.ChangeInformation,
                NextStep = (int)DataChangeServiceApplicaiton.ChangePrerequiste,
                ServiceId = (int)ServiceType.CompanyNameChange,
                IsFinished = false,
                ApprovalStatus = 0,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                CustomerTypeId = nameChangeData.CustomerTypeId,
                ExchangeActorId = nameChangeData.ExchangeActorId
            };

            nameChangeData.ServiceApplicationId = newServiceApplication.ServiceApplicationId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    DataChange nameChange = _mapper.Map<DataChange>(nameChangeData);

                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.DataChange.Add(nameChange);

                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return new ObjectResult(_mapper.Map<ServiceApplicationDTO>(newServiceApplication));
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        public async Task<AddressChangeDTO> GetNameChangeData(Guid serviceApplicationId)
        {
            var queryResult = await (from sa in _context.ServiceApplication
                                     join dc in _context.DataChange on sa.ServiceApplicationId equals dc.ServiceApplicationId
                                     join ea in _context.ExchangeActor on sa.ExchangeActorId equals ea.ExchangeActorId
                                     where sa.ServiceApplicationId == serviceApplicationId
                                     select new AddressChangeDTO
                                     {
                                         OrganizationNameAmh = dc.OrganizationNameAmh == null ? ea.OrganizationNameAmh : dc.OrganizationNameAmh,
                                         OldOrganizationNameAmh = ea.OrganizationNameAmh,
                                         OrganizationNameEng = dc.OrganizationNameEng == null ? ea.OrganizationNameEng : dc.OrganizationNameEng,
                                         OldOrganizationNameEng = ea.OrganizationNameEng
                                     }).AsNoTracking()
                                       .FirstOrDefaultAsync();

            return queryResult;
        }

        public async Task<bool> SaveApprovedNameChange(Guid serviceApplicationId, Guid exchangeActorId)
        {
            //try
            //{
                // get change request
                var changeRequest = await (from sa in _context.ServiceApplication
                                           join dc in _context.DataChange on sa.ServiceApplicationId equals dc.ServiceApplicationId
                                           join ea in _context.ExchangeActor on sa.ExchangeActorId equals ea.ExchangeActorId
                                           where sa.ServiceApplicationId == serviceApplicationId
                                           select new AddressChangeDTO
                                           {
                                               OrganizationNameAmh = dc.OldOrganizationNameAmh != dc.OrganizationNameAmh ? dc.OrganizationNameAmh : ea.OrganizationNameAmh,
                                               OrganizationNameEng = dc.OldOrganizationNameEng != dc.OrganizationNameEng ? dc.OrganizationNameEng : ea.OrganizationNameEng
                                           }).AsNoTracking()
                                             .FirstOrDefaultAsync();

                // get current exchange actor name information and update the value

                ExchangeActor currentName = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == exchangeActorId).FirstOrDefaultAsync();

                currentName.OrganizationNameEng = changeRequest.OrganizationNameEng;
                currentName.OrganizationNameAmh = changeRequest.OrganizationNameAmh;

                // get service application and update the value
                ServiceApplication serviceApplication = await _context.ServiceApplication.Where(sa => sa.ServiceApplicationId == serviceApplicationId).FirstOrDefaultAsync();

                int serviceApplicationNextStep = (int)DataChangeServiceApplicaiton.Reprint;
            // serviceApplication.Status = (int)ServiceApplicaitonStatus.Approved;
                serviceApplication.ApprovalFinished = true;
                serviceApplication.UpdatedDateTime = DateTime.Now;
                serviceApplication.Status = (int)ServiceApplicaitonStatus.Completed;
                serviceApplication.ServiceEndDateTime = DateTime.Now;
                serviceApplication.IsFinished = true;
                serviceApplication.NextStep = serviceApplicationNextStep;
                serviceApplication.CurrentStep = serviceApplicationNextStep;

                // update the tables using transation

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;

                    }catch(Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }
                }

            //}catch(Exception ex)
            //{
            //    throw new Exception(ex.InnerException.ToString());
            //}
        }
    }
}
