
using CUSTOR.ICERS.Core;
using CUSTOR.OTRLS.Core.EntityLayer.ECX;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CUSTOR.OTRLS.Core.DataAccessLayer
{
    public class EceRepository
    {
        ECEADbContext context;
        public EceRepository(ECEADbContext oTRLSDbContext)
        {
            context = oTRLSDbContext;
        }
        public async Task<List<EcexDTo>> GetEcx(string start, string end, string sessionId, string ProductType)
        {
            List<EcexDTo> addedEcex = new List<EcexDTo>();
            var ecxexisteddata = await context.EXE.Where(c => c.Date.HasValue && c.Date.Value.Day == DateTime.Now.Day).ToListAsync();
            if (!ecxexisteddata.Any())
            {
                var ecex = await context.EXE.ToListAsync();
                foreach (var sav in ecex)
                {
                    sav.Date = DateTime.Now;
                }
                await context.SaveChangesAsync();
            }
            if (start != "null" && end != "null" && sessionId != "null" && sessionId != "undefined" && ProductType != "undefined" && ProductType != null)
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date >= Convert.ToDateTime(start) && c.Date < Convert.ToDateTime(end) && c.SessionId == sessionId && c.ProductType == ProductType).ToListAsync();
                ;
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();

            }
            else if (start != "null" && end != "null" && sessionId != "null" && sessionId != "undefined")
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date >= Convert.ToDateTime(start) && c.Date < Convert.ToDateTime(end) && c.SessionId == sessionId).ToListAsync();
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                        EXEId =c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();
            }
            else if (start != "null" && end != "null" && ProductType != "undefined" && ProductType != null)
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date >= Convert.ToDateTime(start) && c.Date < Convert.ToDateTime(end) && c.ProductType == ProductType).ToListAsync();
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();

            }
            else if (start != "null" && end != "null")
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date >= Convert.ToDateTime(start) && c.Date <= Convert.ToDateTime(end)).ToListAsync();
                try
                {
                    return addedEcex = ecxdata.Select(c => new EcexDTo()
                    {
                        EXEId = c.EXEId,
                        ClosingPrice = c.ClosingPrice,
                        ProductType = c.ProductType,
                        Date = c.Date.Value.ToString("dd/MM/yyyy"),
                        TradeNo = c.TradeNo,
                        WRNo = c.WRNo,
                        ConsignmentType = c.ConsignmentType,
                        IsNewModel = c.IsNewModel,
                        Grade = c.Grade,
                        Symbol = c.Symbol,
                        BuyRepID = c.BuyRepID,
                        BuyRepName = c.BuyRepName,
                        BuyMemberId = c.BuyMemberId,
                        BuyMemberName = c.BuyMemberName,
                        BuyMemberTIN = c.BuyMemberTIN,
                        BuyClientId = c.BuyClientId,
                        SellRepID = c.SellRepID,
                        SellRepName = c.SellRepName,
                        SellMemberId = c.SellMemberId,
                        SellMemberName = c.SellMemberName,
                        SellMemberTIN = c.SellMemberTIN,
                        SellClientId = c.SellClientId,
                        SellClientName = c.SellClientName,
                        SellClientTIN = c.SellClientTIN,
                        Lot = c.Lot,
                        Quintal = c.Quintal,
                        UnitPrice = c.UnitPrice,
                        ValueInBirrSTD = c.ValueInBirrSTD,
                        Bags = c.Bags,
                        VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                        Warehouse = c.Warehouse,
                        PY = c.PY,
                        IsOnline = c.IsOnline,
                        OpeningPrice = c.OpeningPrice,
                        PrevClosingprice = c.PrevClosingprice,
                        RangeinPercent = c.RangeinPercent,
                        LowerLimitPrice = c.LowerLimitPrice,
                        UpperLimitPrice = c.UpperLimitPrice
                    }).ToList();
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
            else if (start != "null" && sessionId != "null" && ProductType != null && ProductType != "undefined")
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date == Convert.ToDateTime(start) && c.SessionId == sessionId && c.ProductType == ProductType).ToListAsync();
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();
            }
            else if (start != "null" && ProductType != null && ProductType != "undefined")
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date == Convert.ToDateTime(start) && c.ProductType == ProductType).ToListAsync();
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();
            }
            else if (start != "null" && sessionId != "null")
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date == Convert.ToDateTime(start) && c.SessionId == sessionId).ToListAsync();
                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();
            }
            else
            {
                var ecxdata = await context.EXE.Where(c => c.Date.HasValue && c.Date.Value.Day == DateTime.Now.Day).ToListAsync();

                return addedEcex = ecxdata.Select(c => new EcexDTo()
                {
                    EXEId = c.EXEId,
                    ClosingPrice = c.ClosingPrice,
                    ProductType = c.ProductType,
                    Date = c.Date.Value.ToString("dd/MM/yyyy"),
                    TradeNo = c.TradeNo,
                    WRNo = c.WRNo,
                    ConsignmentType = c.ConsignmentType,
                    IsNewModel = c.IsNewModel,
                    Grade = c.Grade,
                    Symbol = c.Symbol,
                    BuyRepID = c.BuyRepID,
                    BuyRepName = c.BuyRepName,
                    BuyMemberId = c.BuyMemberId,
                    BuyMemberName = c.BuyMemberName,
                    BuyMemberTIN = c.BuyMemberTIN,
                    BuyClientId = c.BuyClientId,
                    SellRepID = c.SellRepID,
                    SellRepName = c.SellRepName,
                    SellMemberId = c.SellMemberId,
                    SellMemberName = c.SellMemberName,
                    SellMemberTIN = c.SellMemberTIN,
                    SellClientId = c.SellClientId,
                    SellClientName = c.SellClientName,
                    SellClientTIN = c.SellClientTIN,
                    Lot = c.Lot,
                    Quintal = c.Quintal,
                    UnitPrice = c.UnitPrice,
                    ValueInBirrSTD = c.ValueInBirrSTD,
                    Bags = c.Bags,
                    VolumeKGUsingSTD = c.VolumeKGUsingSTD,
                    Warehouse = c.Warehouse,
                    PY = c.PY,
                    IsOnline = c.IsOnline,
                    OpeningPrice = c.OpeningPrice,
                    PrevClosingprice = c.PrevClosingprice,
                    RangeinPercent = c.RangeinPercent,
                    LowerLimitPrice = c.LowerLimitPrice,
                    UpperLimitPrice = c.UpperLimitPrice
                }).ToList();
            }
        }

        public async Task<IEnumerable<EXE>> GetById(int id)
        {
            var ecxdata = await context.EXE.Where(c => c.EXEId == id).ToListAsync();
            return ecxdata;

        }
        public async Task<List<EcexProductDto>> GetProduct()
        {
            List<EcexProductDto> addedEcex = new List<EcexProductDto>();
            addedEcex = await context.EXE.Select(e => new EcexProductDto()
            {
                ProductType = e.ProductType
            }).Distinct().ToListAsync();
            return addedEcex;
        }
    }
}
