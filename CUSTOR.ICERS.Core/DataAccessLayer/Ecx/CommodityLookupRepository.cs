using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
//using DevExpress.DataAccess.Sql;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
  public class CommodityLookupRepository
  {
    private readonly ECEADbContext _context;

    public CommodityLookupRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<CommodityLookupVM>>  GetCommodityLookUp()
    {
      try
      {
        List<CommodityLookupVM> listOfCommodityLookUp = new List<CommodityLookupVM>();
        listOfCommodityLookUp = await _context.CommodityLookupVM.FromSqlRaw("EXECUTE dbo.spCommodityLookupForECEA").ToListAsync();
        return listOfCommodityLookUp;
      }
      catch (Exception ex)
      {

        throw new Exception( ex.Message);
      }

    }
  }
}
