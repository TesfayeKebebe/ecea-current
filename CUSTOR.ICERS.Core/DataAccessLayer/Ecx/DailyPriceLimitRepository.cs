using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class DailyPriceLimitRepository
  {
    private readonly ECEADbContext _context;

    public DailyPriceLimitRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<DailyPriceLimitVM>> getDailyPriceLimit(DateTime from, DateTime to)
    {
      try
      {
        List<DailyPriceLimitVM> listPriceLimit = new List<DailyPriceLimitVM>();
        listPriceLimit = await _context.DailyPriceLimitVM.FromSqlRaw("EXECUTE dbo.spDailyPriceLimitforECEA {0}, {1}", from, to).ToListAsync();
        return listPriceLimit;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
   
    }
  }
}
