using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class OrderStatusRepository
  {
    private readonly ECEADbContext _context;

    public OrderStatusRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<OrderStatusVM>>  GetOrderStatus()
    {
      try
      {
        List<OrderStatusVM> listOrderStatus = new List<OrderStatusVM>();
        listOrderStatus = await _context.OrderStatusVM.FromSqlRaw("exec dbo.spOrderStatusLookupForECEA").ToListAsync();
        return listOrderStatus;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }
  }
}
