using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class OrderValidityRepository
  {
    private readonly ECEADbContext _context;

    public OrderValidityRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<OrderValidityVM>> GetOrderValidities()
    {
      try
      {
        List<OrderValidityVM> listOrderValidities = new List<OrderValidityVM>();
        listOrderValidities = await _context.OrderValidityVM.FromSqlRaw("exec dbo.spOrderValidityLookupForECEA").ToListAsync();
        return listOrderValidities;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
  }
}
