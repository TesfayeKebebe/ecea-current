using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class SessionDataRepository
  {
    private readonly ECEADbContext _context;

    public SessionDataRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<SessionDataVM>>  GetSessionDatas(DateTime from, DateTime to)
    {
      try
      {
        List<SessionDataVM> listOfSessionDatas = new List<SessionDataVM>();
        listOfSessionDatas = await _context.SessionDataVM.FromSqlRaw("exec dbo.spSessionDataForECEA {0}, {1}", from, to).ToListAsync();
        return listOfSessionDatas;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
     
    }
  }
}
