using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class SessionStatusRepository
  {
    private readonly ECEADbContext _context;

    public SessionStatusRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<SessionStatusVM>> GetSessionStatus()
    {
      try
      {
        List<SessionStatusVM> listOfSessionStatus = new List<SessionStatusVM>();
        listOfSessionStatus = await _context.SessionStatusVM.FromSqlRaw("exec dbo.spSessionStatusLookupForECEA").ToListAsync();
        return listOfSessionStatus;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
  }
}
