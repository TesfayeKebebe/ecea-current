using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class SubSessionRepository
  {
    private readonly ECEADbContext _context;

    public SubSessionRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<SubSessionVM>>  GetSubSessions(DateTime from, DateTime to)
    {
      try
      {
        List<SubSessionVM> listOfSubSessions = new List<SubSessionVM>();
        listOfSubSessions = await _context.SubSessionVM.FromSqlRaw(" exec dbo.spSubSessionDataForECEA {0},{1}", from, to).ToListAsync();
        return listOfSubSessions;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
     
    }

  }
}
