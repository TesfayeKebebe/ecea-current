using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public  class SubSessionStatusRepository
  {
    public readonly ECEADbContext _context;

    public SubSessionStatusRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<SubSessionStatusVM>>  GetSubSessionStatus()
    {
      try
      {
        List<SubSessionStatusVM> listOfSubSessionStatus = new List<SubSessionStatusVM>();
        listOfSubSessionStatus = await _context.SubSessionStatusVM.FromSqlRaw("exec dbo.spSubSessionStatusLookupForECEA").ToListAsync();
        return listOfSubSessionStatus;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    
    }
  }
}
