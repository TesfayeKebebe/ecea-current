using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class TradeDataRepository
  {
    private readonly ECEADbContext _context;

    public TradeDataRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<TradeDataVM>>  GetTradeDatas(DateTime from, DateTime to)
    {
      try
      {
        List<TradeDataVM> LisTradeDatas = new List<TradeDataVM>();
        LisTradeDatas = await _context.TradeDataVM.FromSqlRaw("exec dbo.spTradeDataForECEA {0},{1}", from, to).ToListAsync();
        return LisTradeDatas;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
     
    }
  }
}
