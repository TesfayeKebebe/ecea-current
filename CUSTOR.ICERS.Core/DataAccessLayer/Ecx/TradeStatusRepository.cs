using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class TradeStatusRepository
  {
    private readonly ECEADbContext _context;

    public TradeStatusRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<TradeStatusVM>> getTradeStatus()
    {
      try
      {
        List<TradeStatusVM> ListOfTradeStatus = new List<TradeStatusVM>();
        ListOfTradeStatus = await _context.TradeStatusVM.FromSqlRaw("exec dbo.spTradeStatusLookupForECEA").ToListAsync();
        return ListOfTradeStatus;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
      
    }
  }
}
