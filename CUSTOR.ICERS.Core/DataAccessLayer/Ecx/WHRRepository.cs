using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class WHRRepository
  {
    private readonly ECEADbContext _context;
    public WHRRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<WHRVM>> GetWHRs()
    {
      try
      {
        List<WHRVM> wHRVMs = new List<WHRVM>();
        wHRVMs = await _context.WHRVM.FromSqlRaw("exec dbo.spWHRLookupForECEA").ToListAsync();
        return wHRVMs;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
      
    }
  }
}
