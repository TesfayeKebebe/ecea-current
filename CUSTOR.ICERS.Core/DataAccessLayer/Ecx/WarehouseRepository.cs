using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Ecx
{
 public class WarehouseLookUpRepository
  {
    private readonly ECEADbContext _context;

    public WarehouseLookUpRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<WarehouseVM>> getWareHouses()
    {
      try
      {
        List<WarehouseVM> warehouses = new List<WarehouseVM>();
        warehouses = await _context.WarehouseVM.FromSqlRaw("exec dbo.spWarehouseLookupForECEA").ToListAsync();
        return warehouses;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
  }
}
