﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class ExchangeActorProfileRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ExchangeActorProfileRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<ExchangeActorProfile>> GetExchangeActroProfile(string lang)
        {
            try
            {

                /*
                 * Search for customer profile based on the give search criteria
                 * Get search items result
                 * Count the number of total search result
                 * Send for Angular App and display the result on the syncfusion table
                 * 
                 * 
                 * 
                 * FullName = om.ManagerTypeId == 117 : (lang == "et" ? om.Owner.OwnerFullNameAmh : om.Owner.OwnerFullNameEng ) : (lang == "et" ? om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh
                                                                        : om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng),
                 * 
                 */

                List<ExchangeActorProfile> customerProfile = await (from ea in _context.ExchangeActor
                                                                    join om in _context.OwnerManager
                                                                    on new {ea.ExchangeActorId, IsManager = true} equals new {om.ExchangeActorId, om.IsManager} into owManager
                                                                    // ea.ExchangeActorId equals om.ExchangeActorId 
                                                                    from ownMan in owManager.DefaultIfEmpty()
                                                                    // where ownMan.IsManager == true
                                                                    select new ExchangeActorProfile
                                                                    {
                                                                        FullName = (lang == "et") ? ownMan.FirstNameAmh + " " + ownMan.FatherNameAmh + " " + ownMan.GrandFatherNameAmh : 
                                                                        ownMan.FirstNameEng + " " + ownMan.FatherNameEng + " " + ownMan.GrandFatherNameEng,
                                                                        EcxCode = ea.Ecxcode,
                                                                        ExpireDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ea.ExpireDateTime.GetValueOrDefault()).ToString()) :
                                                                        ea.ExpireDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                                                        IssueDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ea.IssueDateTime.GetValueOrDefault()).ToString()) :
                                                                        ea.IssueDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                                                        MemberType = lang == "et" ? ea.CustomerType.DescriptionAmh : ea.CustomerType.DescriptionEng,
                                                                        OrganizationName = lang == "et" ? ea.OrganizationNameAmh : ea.OrganizationNameEng,
                                                                        CustomerStatus = lang == "et" ? ea.ExchangeActorStatus.DescriptionAmh : ea.ExchangeActorStatus.DescriptionEng,
                                                                        MemberCategory = lang == "et" ? ea.MemberCategory.DescriptionAmh : ea.MemberCategory.DescriptionEng,
                                                                        SatusCode = ea.Status,
                                                                        ExchangeActorId = ea.ExchangeActorId,
                                                                        CustomerTypeId = ea.CustomerTypeId
                                                                    }).AsNoTracking()
                                                                      .ToListAsync();


                                                                    return customerProfile;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }


        public async Task<CustomerOrganizationVM> GetCustomerOrganizationInfo(string lang, Guid exchangeActorId, int customerTypeId)
        {
                var data = await (from ea in _context.ExchangeActor
                                  join sa in _context.ServiceApplication on ea.ServiceApplicationId equals sa.ServiceApplicationId
                                  join om in _context.OwnerManager on new {ea.ExchangeActorId, IsManager = true } equals new { om.ExchangeActorId, om.IsManager} into ownMan
                                  from ownerManager in ownMan.DefaultIfEmpty()
                                  join ct in _context.Lookup on ea.CustomerTypeId equals ct.LookupId
                                  join bf in _context.Lookup on ea.BuisnessFiledId equals bf.LookupId into businessFiled
                                  from busFiled in businessFiled.DefaultIfEmpty()
                                  join mc in _context.Lookup on ea.MemberCategoryId equals mc.LookupId into memberCategory
                                  from memberCat in memberCategory.DefaultIfEmpty()
                                  join ot in _context.Lookup on ea.OrganizationTypeId equals ot.LookupId into organizationType
                                  from orgType in organizationType.DefaultIfEmpty()
                                  join r in _context.Region on ea.RegionId equals r.RegionId into region
                                  from reig in region.DefaultIfEmpty()
                                  join z in _context.Zone on ea.ZoneId equals z.ZoneId into zone
                                  from zon in zone.DefaultIfEmpty()
                                  join w in _context.Woreda on ea.WoredaId equals w.WoredaId into woreda
                                  from wor in woreda.DefaultIfEmpty()
                                  join k in _context.Kebele on ea.KebeleId equals k.KebeleId into keble
                                  from keb in keble.DefaultIfEmpty()
                                  where ea.ExchangeActorId == exchangeActorId && sa.Status == (int)ServiceApplicaitonStatus.Completed
                                  select new CustomerOrganizationVM
                                  {
                                    OrganizationName = lang == "et" ? ea.OrganizationNameAmh : ea.OrganizationNameEng,
                                    CustomerType = lang == "et" ? ct.DescriptionAmh : ct.DescriptionEng,
                                    Address = lang == "et" ? (reig.DescriptionAmh + "/" + zon.DescriptionAmh + "/" + wor.DescriptionAmh + "/" + keb.DescriptionAmh) :
                                    (reig.DescriptionEng + "/" + zon.DescriptionEng + "/" + wor.DescriptionEng + "/" + keb.DescriptionEng),
                                    EcxCode = ea.Ecxcode,
                                    MemberCategory = lang == "et" ? memberCat.DescriptionAmh : memberCat.DescriptionEng,
                                    OrganizationType = lang == "et" ? orgType.DescriptionAmh : orgType.DescriptionEng,
                                    Photo  = ownerManager.Photo,
                                    Products = (from mp in _context.MemberProduct
                                                join com in _context.Commodity on mp.CommodityId equals com.CommodityId
                                                where mp.ExchangeActorId == exchangeActorId
                                                select new ProducNameVM
                                                {
                                                    Name = lang == "et" ? com.DescriptionAmh : com.DesciptionEng
                                                }).ToList(),
                                   BusinessFiled = (from buissinessfields in _context.CustomerBussinessFiled
                                                    join bf in _context.Lookup 
                                                    on buissinessfields.BusinessFiledId equals bf.LookupId
                                                    where buissinessfields.ExchangeActorId == exchangeActorId
                                                                select new CustomerBusinessFiledNameVM
                                                                {
                                                                    BusinessFiled = lang == "et" ?  bf.DescriptionAmh: bf.DescriptionEng
                                                                }).ToList(),
                                    Email = ea.Email,
                                    MobileNo = ea.MobileNo,
                                    Tel = ea.Tel
                                  }).AsNoTracking().SingleAsync();
                return data;
        }

        public async Task<List<ManagerProfileVM>> GetOwnerMangerInfo(string lang, Guid exchagneactorid)
        {
            try
            {
                var data = await (from om in _context.OwnerManager
                            join sa in _context.ServiceApplication on om.ExchangeActorId equals sa.ExchangeActorId
                            join gender in _context.Lookup on om.GenderId equals gender.LookupId
                            join el in _context.Lookup on om.EducationalLevelId equals el.LookupId into educationalLevel
                            from eduLevel in educationalLevel.DefaultIfEmpty()
                            join mt in _context.Lookup on om.ManagerTypeId equals mt.LookupId into mangerType
                            from manType in mangerType.DefaultIfEmpty()
                            where om.ExchangeActorId == exchagneactorid && sa.Status == (int)ServiceApplicaitonStatus.Completed && sa.ServiceId == (int)ServiceType.Recognition
                                  select new ManagerProfileVM
                            {
                                FullName = lang == "et" ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh) : 
                                (om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng),
                                Gender = lang == "et" ? gender.DescriptionAmh : gender.DescriptionEng,
                                EducationalLevel = lang == "et" ? eduLevel.DescriptionAmh : eduLevel.DescriptionEng,
                                Email = om.Email,
                                IsManager = om.IsManager,
                                Mobile = om.MobileNo,
                                Tel = om.Tel,
                                Photo = om.Photo,
                                ManagerType = lang == "et" ? manType.DescriptionAmh : manType.DescriptionEng
                            })
                            .OrderByDescending(s=>s.IsManager ==true)
                            .AsNoTracking()
                              .ToListAsync();

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<List<RepresntativeVM>> GetRepresentativeProfile(string lang, Guid exchangeActorId)
        {
            try
            {
                var data = await (from ea in _context.ExchangeActor
                                  join om in _context.OwnerManager on ea.ExchangeActorId equals om.ExchangeActorId
                                  join gender in _context.Lookup on om.GenderId equals gender.LookupId
                                  join educationLevel in _context.Lookup on om.EducationalLevelId equals educationLevel.LookupId
                                  where ea.DelegatorId == exchangeActorId
                                  select new RepresntativeVM
                                  {
                                      FullName = lang == "et" ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh) : 
                                      (om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng),
                                      Gender = lang == "et" ? gender.DescriptionAmh : gender.DescriptionEng,
                                      EducationalLevel = lang == "et" ? educationLevel.DescriptionAmh : educationLevel.DescriptionEng,
                                      Tel = om.Tel,
                                      MobileNo = om.MobileNo,
                                      Status = (lang == "et" &&DateTime.Now.Date.CompareTo(ea.ExpireDateTime.Value.AddMonths(4)) < 0) ? "በሥራ ላይ" 
                                      : (lang == "et" && DateTime.Now.Date.CompareTo(ea.ExpireDateTime.Value.AddMonths(4)) > 0) ? "ተቋርጧል" 
                                      : (lang == "en" && DateTime.Now.Date.CompareTo(ea.ExpireDateTime.Value.AddMonths(4)) > 0) ? "Terminated"
                                      :"Active",
                                      IssuedDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ea.ExpireDateTime.GetValueOrDefault()).ToString()) :
                                                                        ea.ExpireDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                      //IssuedDate = 

                                  }).AsNoTracking()
                                    .ToListAsync();

                return data;

            }catch(Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
       
    }
}
