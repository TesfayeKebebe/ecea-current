using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CAUnitRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CAUnitRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CAUnitInfo> GetCAUnitInfo(Guid caUnitId, CaseAdministrationUnitType unitType)
        {
            CAUnitInfo result = null;
            switch (unitType)
            {
                case CaseAdministrationUnitType.Application:
                    result = await _context.CaseApplication
                        .Where(u => u.CaseApplicationId == caUnitId && u.IsDeleted == false)
                        .Include(u => u.CaseStatusType).ThenInclude(s => s.Category)
                        .Include(u => u.CaseStatusType).ThenInclude(s => s.StateCategory)
                        .Select(u => new CAUnitInfo
                        {
                            CaseApplicationId = u.CaseApplicationId.ToString(),
                            Status = new CaseLookUpDTO
                            {
                                Code = u.CaseStatusType.TypeCode,
                                Id = u.CaseStatusTypeId,
                                GroupCode = u.CaseStatusType.Category.TypeCode,
                                SubGroupCode = u.CaseStatusType.StateCategory.TypeCode,
                            }
                        })
                        .AsNoTracking()
                        .FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.Investigation:
                    result = await _context.CaseInvestigation
                        .Where(u => u.CaseInvestigationId == caUnitId && u.IsDeleted == false)
                        .Include(u => u.Case)
                        .Include(u => u.CaseApplication)
                        .Include(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.Category)
                        .Include(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.StateCategory)
                        .Select(u => new CAUnitInfo
                        {
                            CaseApplicationId = u.CaseApplicationId.ToString(),
                            InvestigationId = u.CaseInvestigationId.ToString(),
                            LegalCaseId = u.Case != null ? u.Case.CaseId.ToString() : "",
                            Status = new CaseLookUpDTO
                            {
                                Code = u.CaseApplication.CaseStatusType.TypeCode,
                                Id = u.CaseApplication.CaseStatusTypeId,
                                GroupCode = u.CaseApplication.CaseStatusType.Category.TypeCode,
                                SubGroupCode = u.CaseApplication.CaseStatusType.StateCategory.TypeCode,
                            }
                        })
                        .AsNoTracking()
                        .FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.Case:
                    result = await _context.Case
                        .Where(u => u.CaseId == caUnitId && u.IsDeleted == false)
                        .Include(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication)
                        .Include(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.Category)
                        .Include(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.StateCategory)
                        .Select(u => new CAUnitInfo
                        {
                            CaseApplicationId = u.CaseInvestigation.CaseApplicationId.ToString(),
                            InvestigationId = u.CaseInvestigationId.ToString(),
                            LegalCaseId = u.CaseId.ToString(),
                            Status = new CaseLookUpDTO
                            {
                                Code = u.CaseInvestigation.CaseApplication.CaseStatusType.TypeCode,
                                Id = u.CaseInvestigation.CaseApplication.CaseStatusTypeId,
                                GroupCode = u.CaseInvestigation.CaseApplication.CaseStatusType.Category.TypeCode,
                                SubGroupCode = u.CaseInvestigation.CaseApplication.CaseStatusType.StateCategory.TypeCode,
                            }
                        })
                        .AsNoTracking()
                        .FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.CaseTracking:
                    result = await _context.CaseDecisionTracking
                       .Where(u => u.CaseDecisionTrackingId == caUnitId && u.IsDeleted == false)
                       .Include(u => u.Case).ThenInclude(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication)
                       .Include(u => u.Case).ThenInclude(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.Category)
                       .Include(u => u.Case).ThenInclude(u => u.CaseInvestigation).ThenInclude(u => u.CaseApplication).ThenInclude(u => u.CaseStatusType).ThenInclude(s => s.StateCategory)
                       .Select(u => new CAUnitInfo
                       {
                           CaseApplicationId = u.Case.CaseInvestigation.CaseApplicationId.ToString(),
                           InvestigationId = u.Case.CaseInvestigationId.ToString(),
                           LegalCaseId = u.CaseId.ToString(),
                           CaseTrackingId = u.CaseDecisionTrackingId.ToString(),
                           CAUnitType = unitType,
                           Status = new CaseLookUpDTO
                           {
                               Code = u.Case.CaseInvestigation.CaseApplication.CaseStatusType.TypeCode,
                               Id = u.Case.CaseInvestigation.CaseApplication.CaseStatusTypeId,
                               GroupCode = u.Case.CaseInvestigation.CaseApplication.CaseStatusType.Category.TypeCode,
                               SubGroupCode = u.Case.CaseInvestigation.CaseApplication.CaseStatusType.StateCategory.TypeCode,
                           }
                       })
                       .AsNoTracking()
                       .FirstOrDefaultAsync();
                    break;
                default:
                    break;
            }
            return result;
        }

    }
}
