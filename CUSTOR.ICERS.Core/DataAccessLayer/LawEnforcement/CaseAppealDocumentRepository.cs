using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
 public class CaseAppealDocumentRepository
  {
    private readonly ECEADbContext _context;

    private readonly IMapper _mapper;
    public CaseAppealDocumentRepository(ECEADbContext context, IMapper mapper)
    {
      _context = context;
      _mapper = mapper;
    }
    public async Task<bool> Create(CaseAppealDocumentDto postedAttachment)
    {
      var pathToSave = getUploadPath();

      string extension = getFileExtension(postedAttachment);
      postedAttachment.Id = Guid.NewGuid().ToString();
      string fileName = postedAttachment.Id + extension;

      string filePath = Path.Combine(pathToSave, fileName);

      postedAttachment.FileName = fileName;

      try
      {


        DateTime dateTime = DateTime.Now;
        CaseAppealDocument caseDocument = _mapper.Map<CaseAppealDocument>(postedAttachment);
        caseDocument.CreatedDateTime = dateTime;
        caseDocument.UpdatedDateTime = dateTime;


        _context.CaseAppealDocument.Add(caseDocument);
        _context.SaveChanges();


        // save to disk
        if (!Directory.Exists(pathToSave))
          Directory.CreateDirectory(pathToSave);

        using (var stream = new FileStream(filePath, FileMode.Create))
        {
          await postedAttachment.ActualFile.CopyToAsync(stream);
        }


        return true;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }

    }
    //public async Task<bool> Update(CaseAppealDocumentDto updatedAttachment)
    //{
    //  var oldDocumentUnit = await _context.CaseAppealDocument.Where(ca => ca.Id == updatedAttachment.Id).FirstOrDefaultAsync();

    //  if (oldDocumentUnit == null)
    //    return false;

    //  DateTime dateTime = DateTime.Now;

    //  var pathToSave = getUploadPath();

    //  string extension = getFileExtension(updatedAttachment);

    //  // for updating
    //  string fileName = updatedAttachment.FileName + extension;

    //  // for deletion
    //  string todeleteFileName = oldDocumentUnit.FileName;

    //  string filePath = Path.Combine(pathToSave, fileName);

    //  string deletionPath = Path.Combine(pathToSave, todeleteFileName);

    //  try
    //  {
    //    // delete existing file with the given name
    //    if (File.Exists(deletionPath))
    //    {
    //      File.Delete(deletionPath);
    //    }


    //    oldDocumentUnit.UpdatedDateTime = dateTime;
    //    oldDocumentUnit.AttachmentContent = updatedAttachment.AttachmentContent;
    //    oldDocumentUnit.FileName = fileName;
    //    oldDocumentUnit.UpdatedUserId = updatedAttachment.UpdatedUserId;


    //    await _context.SaveChangesAsync();
    //    return true;
    //  }
    //  catch (Exception ex)
    //  {
    //    throw new Exception(ex.InnerException.ToString());
    //  }
    //}
    //public async Task<CaseAppealDocumentDto> GetCaseDocumentById(Guid caseDocumentId)
    //{
    //  CaseAppealDocumentDto result = null;
    //  var caseDocument = await _context.CaseAppealDocument.Where(u => u.CaseId == caseDocumentId && u.IsDeleted == false)
    //      .AsNoTracking().FirstOrDefaultAsync();
    //  if (caseDocument != null)
    //  {
    //    result = _mapper.Map<CaseAppealDocumentDto>(caseDocument);
    //  }
    //  return result;
    //}



    private string getUploadPath()
    {
      return Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
    }
    private string getFileExtension(CaseAppealDocumentDto documentInformation)
    {
      return documentInformation.ActualFile.ContentType == "application/pdf" ? ".pdf" : documentInformation.ActualFile.ContentType == "image/jpeg" ? ".jpeg" : ".png";
    }

  }
}
