﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseApplicationRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CaseApplicationRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(CaseApplicationEditDTO postedApplication)
        {
            if (postedApplication.Plaintiff.PartyType == CasePartyType.Sole)
                postedApplication.Plaintiff.ExchangeActorId = null;
            if (postedApplication.Plaintiff.ExchangeActor != null)
            {
                postedApplication.Plaintiff.ExchangeActor = null;
                //postedApplication.Plaintiff.ExchangeActor.ExchangeActorId = null;
            }
            if (postedApplication.Suspect.ExchangeActor != null)
            {
                postedApplication.Suspect.ExchangeActor = null;
                //postedApplication.Suspect.ExchangeActor.ExchangeActorId = null;
            }
            CaseApplication caseApplication = _mapper.Map<CaseApplication>(postedApplication);
            DateTime dateTime = DateTime.Now;
            caseApplication.CreatedDateTime = dateTime;
            var suspectId = Guid.NewGuid();
            var plaintiffId = Guid.NewGuid();

            caseApplication.Suspect.CasePartyId = suspectId;
            caseApplication.Suspect.PartyCategory = (int)CasePartyCategory.Suspect;
            caseApplication.Suspect.CreatedDateTime = dateTime;
            caseApplication.Suspect.UpdatedDateTime = dateTime;

            caseApplication.Plaintiff.CasePartyId = plaintiffId;
            caseApplication.Plaintiff.PartyCategory = (int)CasePartyCategory.Plaintiff;
            caseApplication.Plaintiff.CreatedDateTime = dateTime;
            caseApplication.Plaintiff.UpdatedDateTime = dateTime;

            CAUnit cAUnit = new CAUnit
            {
                CAUnitId = Guid.NewGuid(),
                CreatedDateTime = dateTime,
                UpdatedDateTime = dateTime
            };
            caseApplication.CaseApplicationId = cAUnit.CAUnitId;
            caseApplication.Suspect.CAUnitId = cAUnit.CAUnitId;
            caseApplication.Plaintiff.CAUnitId = cAUnit.CAUnitId;
            caseApplication.CaseStatusTypeId = 1;
            caseApplication.SuspectId = suspectId;
            caseApplication.PlaintiffId = plaintiffId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.CAUnit.Add(cAUnit);
                    _context.CaseParty.Add(caseApplication.Plaintiff);
                    _context.CaseParty.Add(caseApplication.Suspect);
                    _context.CaseApplication.Add(caseApplication);


                    _context.SaveChanges();
                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }
        }

        public async Task<CaseApplicationPartyDTO> SaveCaseApplication(CaseApplicationPartyDTO postedCase)
        { 
            bool isNew = postedCase.CaseApplicationId is null; // New or Update?
            
            DateTime dateTime = DateTime.Now;
            
            // We should always use transaction for operations involving two or more entities
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    // // map DTO to case party entity 
                    //CaseParty caseParty = CaseApplicationHelper.GetCaseParty(postedCase);
                    
                    if (!isNew)
                    {
                        //.Include(ex => ex.ExchangeActor).ThenInclude(mn=>mn.OwnerManager)
                        var oldParty = await _context.CaseParty
                            .FirstOrDefaultAsync(cp => cp.CasePartyId.ToString() 
                            == postedCase.CasePartyId);
                        if (postedCase.PartyType == 2)
                            postedCase.ExchangeActorId = null;
                        if (postedCase.PartyType == 3)
                            postedCase.ExchangeActorId = null;
                       
                        if (postedCase.ExchangeActor != null)
                        {
                            postedCase.ExchangeActorId = postedCase.ExchangeActor.ExchangeActorId;
                            postedCase.ExchangeActor = null;
                        }
                        CAUnit cAUnit = null;
                        cAUnit = await _context.CAUnit
                            .FirstOrDefaultAsync(cau => cau.CAUnitId == new Guid(postedCase.CaseApplicationId));
                        if (cAUnit is null)
                        {
                            CAUnit cAUnitNew = new CAUnit
                            {
                                CAUnitId = Guid.NewGuid(),
                                CreatedDateTime = dateTime,
                                UpdatedDateTime = dateTime
                            };
                            _context.CAUnit.Add(cAUnitNew);
                            postedCase.CAUnitId = cAUnitNew.CAUnitId.ToString();
                            oldParty = _mapper.Map(postedCase, oldParty);

                            //_context.SaveChanges();
                        }
                        else {
                            postedCase.IsDeleted = false;
                            postedCase.IsActive = true;
                            postedCase.CreatedDateTime = oldParty.CreatedDateTime;
                            postedCase.UpdatedDateTime = oldParty.UpdatedDateTime;
                            oldParty = _mapper.Map(postedCase, oldParty);
                            //_context.SaveChanges();
                        }
                        
                       var capp = await _context.CaseApplication
                            .FirstOrDefaultAsync(cap => cap.CaseApplicationId.ToString() == postedCase.CaseApplicationId);
                        
                        if (capp.CAUnit.CAUnitId != null)
                        {
                            postedCase.IsDeleted = false;
                            postedCase.IsActive = true;
                            postedCase.CreatedDateTime = capp.CreatedDateTime;
                            postedCase.UpdatedDateTime = capp.UpdatedDateTime;
                            postedCase.UpdatedUserId = capp.UpdatedUserId;

                            capp = _mapper.Map(postedCase, capp);

                            //_context.SaveChanges();
                        }
                        _context.SaveChanges();

                        //Guid caseAppGuid = new Guid(postedCase.CaseApplicationId);
                    }
                    else
                    {
                        CaseApplication ca = CaseApplicationHelper.GetCaseApplication(postedCase);
                        //CaseParty caseParty = CaseApplicationHelper.GetCaseParty(postedCase);
                        if (postedCase.PartyType == 2)
                            postedCase.ExchangeActorId = null;
                        if (postedCase.PartyType == 3)
                            postedCase.ExchangeActorId = null;

                        if (postedCase.ExchangeActor != null)
                        {
                            postedCase.ExchangeActorId = postedCase.ExchangeActor.ExchangeActorId;
                            postedCase.ExchangeActor = null;
                        }
                        CAUnit cAUnit = new CAUnit
                        {
                            CAUnitId = Guid.NewGuid(),
                            CreatedDateTime = dateTime,
                            UpdatedDateTime = dateTime
                        };
                        _context.CAUnit.Add(cAUnit);
                        var partyId = Guid.NewGuid();
                        postedCase.CasePartyId = partyId.ToString();
                        postedCase.CAUnitId = cAUnit.CAUnitId.ToString();
                        postedCase.PartyCategory = (int)CasePartyCategory.Suspect;
                        CaseParty newCaseParty = _mapper.Map<CaseParty>(postedCase);
                         
                        _context.CaseParty.Add(newCaseParty); 
                        //_context.SaveChanges();
                        // then save Case Application entity
                        ca.CreatedDateTime = dateTime;
                        ca.SuspectId = newCaseParty.CasePartyId; 
                        //ca.PlaintiffId = Guid.Empty;
                        ca.CaseApplicationId = cAUnit.CAUnitId;
                        ca.CaseStatusTypeId = 1;
                        _context.CaseApplication.Add(ca);
                        _context.SaveChanges();
                        postedCase.CaseApplicationId = cAUnit.CAUnitId.ToString();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }

                transaction.Commit();
                return postedCase;
            }
        }

        public async Task<bool> Update(CaseApplicationEditDTO updatedApplication)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var oldApplication = await _context.CaseApplication
                       .Include(c => c.Plaintiff)
                       .Include(c => c.Suspect)
                       .AsNoTracking()
                       .FirstOrDefaultAsync(c =>
                       c.CaseApplicationId == updatedApplication.CaseApplicationId);
                    if (oldApplication == null) {
                        return false;
                    } 
                        var modifiedApplication = _mapper.Map(updatedApplication, oldApplication);
                        modifiedApplication.CaseStatusTypeId = 1;
                        modifiedApplication.Plaintiff.CAUnitId = oldApplication.Plaintiff.CAUnitId;
                        modifiedApplication.Plaintiff.PartyCategory = oldApplication.Plaintiff.PartyCategory;
                        modifiedApplication.Plaintiff.PartyType = oldApplication.Plaintiff.PartyType;

                        modifiedApplication.Suspect.CAUnitId = oldApplication.Suspect.CAUnitId;
                        modifiedApplication.Suspect.PartyCategory = oldApplication.Suspect.PartyCategory;
                        modifiedApplication.Suspect.PartyType = oldApplication.Suspect.PartyType;
                        modifiedApplication.CreatedUserId = oldApplication.CreatedUserId;
                        _context.Entry(modifiedApplication).State = EntityState.Modified;
                        _context.Entry(modifiedApplication.Suspect).State = EntityState.Modified;
                        _context.Entry(modifiedApplication.Plaintiff).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }
        public async Task<PagedResult<CaseApplicationListModel>> SearchApplication(CaseApplicationSearchCriteriaModel queryParameters)
        {
            try
            { 
                var dataResult = await (from c in _context.CaseApplication//.Include(cs => cs.CaseStatusType)

                                        from pt in _context.CaseParty
                                        .Where(d=>d.CasePartyId == c.SuspectId).DefaultIfEmpty()
                                        
                                         from lex in _context.ExchangeActor
                                        .Where(ex=>ex.ExchangeActorId== pt.ExchangeActorId)
                                        .DefaultIfEmpty()
                                        join cst in _context.CaseStatusType
                                        on c.CaseStatusTypeId equals cst.CaseStatusTypeId

                                        join ct in _context.Lookup
                                          on c.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on c.DepartmentId equals dt.LookupId
                                         
                                        where (
                                          (queryParameters.CaseType == null || (c.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.Department == null || (c.DepartmentId == queryParameters.Department)) &&
                                          (queryParameters.StatusType == null || (c.CaseStatusTypeId == queryParameters.StatusType)) &&
                                           (queryParameters.From == null || c.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                            (queryParameters.To == null || c.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          c.IsDeleted == false
                                          && (c.CaseStatusType.StateCategoryId == 16 || c.CaseStatusType.StateCategoryId == 17
                                                                      //|| (c.CaseStatusType.CategoryId == 23 && c.CaseStatusType.StateCategoryId == 18)
                                          )
                                          )
                                        orderby (c.CreatedDateTime) descending

                                        select new CaseApplicationListModel
                                        {
                                            ApplicationDate = c.ApplicationDate,
                                            EventDate = c.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.EventDate).ToString()),
                                            CaseApplicationId = c.CaseApplicationId,
                                            SuspectId = c.SuspectId,
                                            PlaintiffId = c.PlaintiffId,
                                            CaseStatusTypeId = c.CaseStatusTypeId,
                                            CaseStatusCode = c.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            CreatedDate = c.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.CreatedDateTime).ToString()),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = c.Description,
                                            ApplicationPartyType = pt.PartyType,
                                            SuspectFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            SuspectFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectOrganizationName = pt.OrganizationNameEng,
                                            SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                            ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng,
                                            //CustomerTypeId = pt.ExchangeActor.CustomerTypeId,
                                            UpdatedDate = c.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.UpdatedDateTime).ToString()),

                                        })
                                    .Paging(queryParameters.PageSize, queryParameters.PageIndex)
                                    .AsNoTracking()
                                    .ToListAsync();
                foreach (var data in dataResult)
                {
                    if (data.SuspectId != null)
                    {
                        var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectId);
                        if (sus != null)
                        {
                            if (sus?.ExchangeActorId != null)
                            {
                                var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
                                //CustomerTypeId == 63 || sueA.CustomerTypeId == 120
                                if (sueA.DelegatorId != null)
                                {
                                    var sueD = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sueA.DelegatorId).FirstOrDefaultAsync();
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sueA.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueD?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueD?.OrganizationNameAmh;
                                }
                                else
                                {
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueA?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
                                }
                            }
                            else
                            {
                                data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
                                data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
                                data.SuspectOrganizationName = sus?.OrganizationNameEng;
                                data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
                            }

                        }

                    }
                    else if (data.PlaintiffId != null)
                    {
                        var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintiffId);

                        if (plaintIff.ExchangeActorId != null)
                        {
                            var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
                            var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                            data.PlaintiffFullNameAmh = (sueA?.OrganizationNameAmh != null) ? sueA?.OrganizationNameAmh : seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                        }
                        else
                        {
                            data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh != null) ? plaintIff?.OrganizationNameAmh : plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (plaintIff?.OrganizationNameEng != null) ? plaintIff?.OrganizationNameEng : plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
                        }
                    }
                }
                return new PagedResult<CaseApplicationListModel>()
                {
                    Items = _mapper.Map<List<CaseApplicationListModel>>(dataResult),
                    ItemsCount = dataResult.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<CaseApplicationListModel>> SearchInvestigationApplication(CaseApplicationSearchCriteriaModel queryParameters, Guid currentUserId)
        {
            try
            {
                var dataResult = await (from c in _context.CaseApplication
                                        from pt in _context.CaseParty
                                        .Where(d => d.CasePartyId == c.SuspectId).DefaultIfEmpty()
 
                                        from lex in _context.ExchangeActor
                                        .Where(ex => ex.ExchangeActorId == pt.ExchangeActorId)
                                        .DefaultIfEmpty() 

                                        join cst in _context.CaseStatusType
                                       on c.CaseStatusTypeId equals cst.CaseStatusTypeId
                                        join ct in _context.Lookup
                                          on c.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on c.DepartmentId equals dt.LookupId
                                         
                                        where (
                                          (queryParameters.CaseType == null || (c.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.Department == null || (c.DepartmentId == queryParameters.Department)) &&
                                          (queryParameters.StatusType == null || (c.CaseStatusTypeId == queryParameters.StatusType)) &&
                                           (queryParameters.From == null || c.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                            (queryParameters.To == null || c.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          c.IsDeleted == false && c.UpdatedUserId == currentUserId 
                                          && (c.CaseStatusType.StateCategoryId == 16 || c.CaseStatusType.StateCategoryId == 17
                                           ))
                                        orderby (c.CreatedDateTime) descending

                                        select new CaseApplicationListModel
                                        {
                                            ApplicationDate = c.ApplicationDate,
                                            EventDate = c.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.EventDate).ToString()),
                                            CaseApplicationId = c.CaseApplicationId,
                                            SuspectId = c.SuspectId,
                                            PlaintiffId = c.PlaintiffId,
                                            CaseStatusTypeId = c.CaseStatusTypeId,
                                            CaseStatusCode = c.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            CreatedDate = c.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.CreatedDateTime).ToString()),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = c.Description,
                                            ApplicationPartyType = pt.PartyType,
                                            SuspectFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            SuspectFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectOrganizationName = pt.OrganizationNameEng,
                                            SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                            ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng,

                                            UpdatedDate = c.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.UpdatedDateTime).ToString()),

                                        })
                                    .Paging(queryParameters.PageSize, queryParameters.PageIndex)
                                    .AsNoTracking()
                                    .ToListAsync();
                foreach (var data in dataResult)
                {
                    if (data.SuspectId != null)
                    {
                        var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectId);
                        if (sus != null)
                        {
                            if (sus?.ExchangeActorId != null)
                            {
                                var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
                                //CustomerTypeId == 63 || sueA.CustomerTypeId == 120
                                if (sueA.DelegatorId != null)
                                {
                                    var sueD = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sueA.DelegatorId).FirstOrDefaultAsync();
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sueA.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueD?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueD?.OrganizationNameAmh;
                                }
                                else
                                {
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueA?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
                                }
                            }
                            else
                            {
                                data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
                                data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
                                data.SuspectOrganizationName = sus?.OrganizationNameEng;
                                data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
                            }

                        }

                    }
                    else if (data.PlaintiffId != null)
                    {
                        var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintiffId);

                        if (plaintIff.ExchangeActorId != null)
                        {
                            var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
                            var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                            data.PlaintiffFullNameAmh = (sueA?.OrganizationNameAmh != null) ? sueA?.OrganizationNameAmh : seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                        }
                        else
                        {
                            data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh != null) ? plaintIff?.OrganizationNameAmh : plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (plaintIff?.OrganizationNameEng != null) ? plaintIff?.OrganizationNameEng : plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
                        }
                    }
                }
                return new PagedResult<CaseApplicationListModel>()
                {
                    Items = _mapper.Map<List<CaseApplicationListModel>>(dataResult),
                    ItemsCount = dataResult.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<CaseApplicationListModel>> SearchOfficerInvestigationApplication(CaseApplicationSearchCriteriaModel queryParameters, Guid currentUserId)
        {
            try
            {
                var dataResult = await (from c in _context.CaseApplication
                                        from pt in _context.CaseParty
                                        .Where(d => d.CasePartyId == c.SuspectId).DefaultIfEmpty()

                                        from lex in _context.ExchangeActor
                                        .Where(ex => ex.ExchangeActorId == pt.ExchangeActorId)
                                        .DefaultIfEmpty()

                                        join cst in _context.CaseStatusType
                                        on c.CaseStatusTypeId equals cst.CaseStatusTypeId
                                        join ct in _context.Lookup
                                          on c.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on c.DepartmentId equals dt.LookupId

                                        where (
                                          (queryParameters.CaseType == null || (c.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.Department == null || (c.DepartmentId == queryParameters.Department)) &&
                                          (queryParameters.StatusType == null || (c.CaseStatusTypeId == queryParameters.StatusType)) &&
                                           (queryParameters.From == null || c.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                            (queryParameters.To == null || c.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          c.IsDeleted == false && c.UpdatedUserId == currentUserId
                                          )
                                        orderby (c.CreatedDateTime) descending

                                        select new CaseApplicationListModel
                                        {
                                            ApplicationDate = c.ApplicationDate,
                                            EventDate = c.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.EventDate).ToString()),
                                            CaseApplicationId = c.CaseApplicationId,
                                            SuspectId = c.SuspectId,
                                            PlaintiffId = c.PlaintiffId,
                                            CaseStatusTypeId = c.CaseStatusTypeId,
                                            CaseStatusCode = c.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            CreatedDate = c.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.CreatedDateTime).ToString()),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = c.Description,
                                            ApplicationPartyType = pt.PartyType,
                                            SuspectFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            SuspectFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectOrganizationName = pt.OrganizationNameEng,
                                            SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                            ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng,

                                            UpdatedDate = c.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.UpdatedDateTime).ToString()),

                                        })
                                    .Paging(queryParameters.PageSize, queryParameters.PageIndex)
                                    .AsNoTracking()
                                    .ToListAsync();
                foreach (var data in dataResult)
                {
                    if (data.SuspectId != null)
                    {
                        var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectId);
                        if (sus != null)
                        {
                            if (sus?.ExchangeActorId != null)
                            {
                                var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
                                //CustomerTypeId == 63 || sueA.CustomerTypeId == 120
                                if (sueA.DelegatorId != null)
                                {
                                    var sueD = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sueA.DelegatorId).FirstOrDefaultAsync();
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sueA.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueD?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueD?.OrganizationNameAmh;
                                }
                                else
                                {
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueA?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
                                }
                            }
                            else
                            {
                                data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
                                data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
                                data.SuspectOrganizationName = sus?.OrganizationNameEng;
                                data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
                            }

                        }

                    }
                    else if (data.PlaintiffId != null)
                    {
                        var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintiffId);

                        if (plaintIff.ExchangeActorId != null)
                        {
                            var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
                            var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                            data.PlaintiffFullNameAmh = (sueA?.OrganizationNameAmh != null) ? sueA?.OrganizationNameAmh : seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                        }
                        else
                        {
                            data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh != null) ? plaintIff?.OrganizationNameAmh : plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (plaintIff?.OrganizationNameEng != null) ? plaintIff?.OrganizationNameEng : plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
                        }
                    }
                }
                return new PagedResult<CaseApplicationListModel>()
                {
                    Items = _mapper.Map<List<CaseApplicationListModel>>(dataResult),
                    ItemsCount = dataResult.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<CaseApplicationListModel>> SearchRejectedListApplication(CaseApplicationSearchCriteriaModel queryParameters)
        {
            try
            {
                var dataResult = await (from c in _context.CaseApplication
                                        from pt in _context.CaseParty
                                        .Where(d => d.CasePartyId == c.SuspectId).DefaultIfEmpty()

                                         from lex in _context.ExchangeActor
                                        .Where(ex => ex.ExchangeActorId == pt.ExchangeActorId)
                                        .DefaultIfEmpty()
                                        
                                        join cst in _context.CaseStatusType
                                        on c.CaseStatusTypeId equals cst.CaseStatusTypeId
                                        join ct in _context.Lookup
                                        on c.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on c.DepartmentId equals dt.LookupId
                                         
                                        where (
                                          (queryParameters.CaseType == null || (c.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.Department == null || (c.DepartmentId == queryParameters.Department)) &&
                                          (queryParameters.StatusType == null || (c.CaseStatusTypeId == queryParameters.StatusType)) &&
                                           (queryParameters.From == null || c.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                            (queryParameters.To == null || c.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          c.IsDeleted == false && (c.CaseStatusTypeId == 35)
                                          )
                                        orderby (c.CreatedDateTime) descending

                                        select new CaseApplicationListModel
                                        {
                                            ApplicationDate = c.ApplicationDate,
                                            EventDate = c.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.EventDate).ToString()),
                                            CaseApplicationId = c.CaseApplicationId,
                                            CaseStatusCode = c.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            CreatedDate = c.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.CreatedDateTime).ToString()),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = c.Description,
                                            ApplicationPartyType = pt.PartyType,
                                            SuspectFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            SuspectFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectOrganizationName = pt.OrganizationNameEng,
                                            SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                            ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng,

                                            UpdatedDate = c.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.UpdatedDateTime).ToString()),

                                        })
                                    .Paging(queryParameters.PageSize, queryParameters.PageIndex)
                                    .AsNoTracking()
                                    .ToListAsync();

                return new PagedResult<CaseApplicationListModel>()
                {
                    Items = _mapper.Map<List<CaseApplicationListModel>>(dataResult),
                    ItemsCount = dataResult.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<CaseApplicationEditDTO> GetCaseApplicationById(string lang, Guid caseApplicationId)
        {
            try
            {
                CaseApplicationEditDTO data = null;
                var result = await _context.CaseApplication.Where(c => c.CaseApplicationId == caseApplicationId && c.CAUnit.IsDeleted == false)
                    .Include(c => c.CaseType)
                    .Include(c => c.Department)
                    .Include(c => c.CAUnit)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Region)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Zone)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Woreda)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Kebele)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Nationality)
                    .Include(c => c.CaseStatusType)
                    .FirstOrDefaultAsync();
                if (result != null)
                {
                    data = _mapper.Map<CaseApplicationEditDTO>(result);
                    if (result.Suspect.ExchangeActor != null)
                    {
                        data.Suspect.ExchangeActor = new ExchangeActorCasePartyModel
                        {
                            Ecxcode = result.Suspect.ExchangeActor.Ecxcode,
                            Tin = result.Suspect.ExchangeActor.Tin,
                            ExchangeActorId = result.Suspect.ExchangeActorId.Value,
                            OrganizationName = lang == "et" ? result.Suspect.ExchangeActor.OrganizationNameAmh : result.Suspect.ExchangeActor.OrganizationNameEng,
                            FullName = lang == "et" ? (result.Suspect.ExchangeActor.OwnerManager.FirstNameAmh + " " + result.Suspect.ExchangeActor.OwnerManager.FatherNameAmh + " " + result.Suspect.ExchangeActor.OwnerManager.GrandFatherNameAmh)
                                              : (result.Suspect.ExchangeActor.OwnerManager.FirstNameEng + " " + result.Suspect.ExchangeActor.OwnerManager.FatherNameEng + " " + result.Suspect.ExchangeActor.OwnerManager.GrandFatherNameEng)

                        };
                    }
                    //else { data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyModel(); }
                    if (result.Plaintiff.ExchangeActor != null)
                    {
                        data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyModel
                        {
                            Ecxcode = result.Plaintiff.ExchangeActor.Ecxcode,
                            Tin = result.Plaintiff.ExchangeActor.Tin,
                            ExchangeActorId = result.Plaintiff.ExchangeActorId.Value,
                            OrganizationName = lang == "et" ? result.Plaintiff.ExchangeActor.OrganizationNameAmh : result.Plaintiff.ExchangeActor.OrganizationNameEng,
                            FullName = lang == "et" ? (result.Plaintiff.ExchangeActor.OwnerManager.FirstNameAmh + " " + result.Plaintiff.ExchangeActor.OwnerManager.FatherNameAmh + " " + result.Plaintiff.ExchangeActor.OwnerManager.GrandFatherNameAmh)
                                              : (result.Plaintiff.ExchangeActor.OwnerManager.FirstNameEng + " " + result.Plaintiff.ExchangeActor.OwnerManager.FatherNameEng + " " + result.Plaintiff.ExchangeActor.OwnerManager.GrandFatherNameEng)

                        };
                    }
                    //else
                    //{
                    //    data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyModel();
                    //}
                }
                return data;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<CaseApplicationPartyDTO> getCaseApplication(string lang, Guid caseId)
        {  
            CaseApplication caseApp = null;
            CaseParty cprty = null;
            caseApp = await _context.CaseApplication.Include(c => c.CAUnit).Include(c => c.Suspect).Include(c => c.Plaintiff).ThenInclude(e => e.ExchangeActor)
                .FirstOrDefaultAsync(ca => ca.CaseApplicationId == caseId);

            cprty = await _context.CaseParty.Include(u => u.ExchangeActor).ThenInclude(o => o.OwnerManager)
                .FirstOrDefaultAsync(a => a.CasePartyId == caseApp.SuspectId);

            return CaseApplicationHelper.GetCaseApplicationDTO(caseApp, cprty);

        }

        public async Task<CaseApplicationDTO> GetCaseApplicationDetailById(string lang, Guid caseApplicationId)
        {
            try
            {
                CaseApplicationDTO data = null;
                var result = await _context.CaseApplication.Where(c => c.CaseApplicationId == caseApplicationId && c.CAUnit.IsDeleted == false)
                    .Include(c => c.CaseType)
                    .Include(c => c.Department)
                    .Include(c => c.CAUnit)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Region)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Zone)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Woreda)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Kebele)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Nationality)
                    .Include(c => c.CaseStatusType)
                    .FirstOrDefaultAsync();
                if (result != null)
                {
                    data = _mapper.Map<CaseApplicationDTO>(result);
                    if (result.Suspect.ExchangeActor != null)
                    {
                        data.Suspect.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Suspect?.ExchangeActor?.Ecxcode,
                            Tin = result.Suspect?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Suspect?.ExchangeActorId.Value,
                            OrganizationNameAmh = result.Suspect?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Suspect?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Suspect?.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else {
                        if(result.Plaintiff?.ExchangeActor != null) {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                            if (result.Plaintiff.ExchangeActor.OwnerManager != null) 
                            {
                                var exchangerActor = _context.ExchangeActor.Where(x => x.ExchangeActorId == result.Plaintiff.ExchangeActorId)
                                                     .Include(ex => ex.OwnerManager).FirstOrDefaultAsync();
                            }
                        }
                    }
                    if (result.Plaintiff?.ExchangeActor != null)
                    {
                        data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Plaintiff?.ExchangeActor?.Ecxcode,
                            Tin = result.Plaintiff?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Plaintiff?.ExchangeActorId.Value,
                            OrganizationNameAmh = result.Plaintiff?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Plaintiff?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    data.ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ApplicationDate).ToString());
                    data.EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.EventDate).ToString());
                    data.CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString());
                    data.UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString());
                }
                return data;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }

        //print start
        public async Task<LetterContent> GetCaseApplicationPrintDetailById(Guid caseApplicationId)
        {
            try
            {
                CaseApplicationDTO data = null;
                List<CaseParty> listOfSuspect = new List<CaseParty>();
                CaseParty suspect = new CaseParty();
                suspect = null;
                var plaintIffName = "";
                var EtCurrentDate = "";
                var suspectName = "";
                var PlaintIffAdress = "";
                var SuspectAddress = "";    
            
                var result = await _context.CaseApplication.Where(c => c.CaseApplicationId == caseApplicationId && c.CAUnit.IsDeleted == false)
                    .Include(c => c.CaseType)
                    .Include(c => c.Department)
                    .Include(c => c.CAUnit)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Region)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Zone)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Woreda)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Kebele)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Nationality)
                    .Include(c => c.CaseStatusType)
                    .FirstOrDefaultAsync();
                var caseParties = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).ToList();
                // start
                var caseAppl = _context.Case.Where(x => x.CaseApplicationId == result.CAUnit.CAUnitId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
                var caseApplications = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == result.CAUnit.CAUnitId);
                var region = "";
                var zone = "";
                var woreda = "";
                var kebele = "";
                var plaintIff = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).FirstOrDefault(x => x.PartyCategory == 2);
                listOfSuspect = caseParties.Where(x => x.PartyCategory != 4 && x.PartyCategory != 2).ToList();
                if (plaintIff != null)
                {
                    if (plaintIff.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == plaintIff.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (customerInformation.RegionId > 0)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (customerInformation.ZoneId > 0)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (customerInformation.WoredaId > 0)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (customerInformation.KebeleId > 0)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                            else
                            {
                                plaintIffName = exca?.OrganizationNameAmh;
                                EtCurrentDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString());
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                        }
                    }
                    else
                    {
                        if (plaintIff.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == plaintIff.RegionId)?.DescriptionAmh;
                        if (plaintIff.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == plaintIff.ZoneId)?.DescriptionAmh;
                        if (plaintIff.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == plaintIff.WoredaId)?.DescriptionAmh;
                        if (plaintIff.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == plaintIff.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        plaintIffName = (!string.IsNullOrWhiteSpace(plaintIff?.OrganizationNameAmh)) ? plaintIff?.OrganizationNameAmh : $"{ plaintIff?.FirstNameAmh } {plaintIff.FatherNameAmh} {plaintIff.GrandFatherNameAmh}";
                    }

                }
                if (listOfSuspect.Any())
                {
                    foreach (var ssu in listOfSuspect)
                    {
                        if (ssu.ExchangeActorId != null)
                        {
                            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == ssu.ExchangeActorId);

                            if (exca != null)
                            {
                                if (exca?.DelegatorId != null)
                                {
                                    var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";

                                    }
                                    else
                                    {
                                        suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + exca?.OrganizationNameAmh;
                                    }
                                    else
                                    {
                                        suspectName = exca?.OrganizationNameAmh;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(suspectName))
                            {
                                suspectName = suspectName + "," + ((!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}");
                            }
                            else
                            {
                                suspectName = (!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}";
                            }
                        }
                    }

                }
                if (suspect != null)
                {
                    if (suspect?.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == suspect.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{kebele}";
                            }
                            else
                            {
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                                suspectName = exca?.OrganizationNameAmh;
                            }
                        }
                    }
                    else
                    {
                        if (suspect.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == suspect.RegionId)?.DescriptionAmh;
                        if (suspect.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == suspect.ZoneId)?.DescriptionAmh;
                        if (suspect.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == suspect.WoredaId)?.DescriptionAmh;
                        if (suspect.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == suspect.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        SuspectAddress = $"{region} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        suspectName = (!string.IsNullOrEmpty(suspect?.OrganizationNameAmh)) ? suspect?.OrganizationNameAmh : $"{ suspect?.FirstNameAmh } {suspect.FatherNameAmh} {suspect.GrandFatherNameAmh}";
                    }

                } 
                // end
                
                if (result != null)
                {
                    data = _mapper.Map<CaseApplicationDTO>(result);
                    if (result.Suspect?.ExchangeActor != null)
                    {
                        data.Suspect.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Suspect?.ExchangeActor?.Ecxcode,
                            Tin = result.Suspect?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Suspect.ExchangeActorId.Value,
                            OrganizationNameAmh = result.Suspect?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Suspect?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Suspect.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    if (result.Plaintiff?.ExchangeActor != null)
                    {
                        data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Plaintiff?.ExchangeActor?.Ecxcode,
                            Tin = result.Plaintiff?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Plaintiff?.ExchangeActorId.Value,
                            OrganizationNameAmh = plaintIffName = result.Plaintiff?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Plaintiff?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    data.ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ApplicationDate).ToString());
                    data.EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.EventDate).ToString());
                    data.CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString());
                    data.UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString());
                }
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "InvestigationLetter").FirstOrDefaultAsync();
                var LetterDateRep = letterContent.BodyHeader.Replace(@"ቀን __________", "ቀን	፡ " + "<strong>" + EtCurrentDate + "</strong>");
                var caseDate = "";
                var caseType = "";
                //var caseName = ""; 
                if (caseApplications != null)
                {
                    caseDate = EthiopicDateTime.GetEthiopicDate(caseApplications.ApplicationDate.Day, caseApplications.ApplicationDate.Month, caseApplications.ApplicationDate.Year);
                    caseType = _context.Lookup.FirstOrDefault(x => x.LookupId == caseApplications.CaseTypeId)?.DescriptionAmh;
                    //caseName = caseApplications?.?.OrganizationNameAmh;
                }
                var caseDateRep = LetterDateRep.Replace(@"ሂደት __________", "ሂደት " + "<strong>" + caseDate + "</strong>");
                var suspectRep = caseDateRep.Replace(@"ሔለን1 ________________ ", " " + "<strong>" + suspectName + " </strong> ");
                var caseType0Rep = suspectRep.Replace(@"የግብይት ፈፃሚ ______________", " " + caseType);
                var suspect2Rep = caseType0Rep.Replace(@"ሔለን2 ___________  ", " " + "<strong>" + suspectName + " </strong> ");
                var caseTypeRep = suspect2Rep.Replace(@"የ2010______  ", " " + caseType);
                //var caseType2Rep = caseTypeRep.Replace(@"ጥሰቱ ዓይነት፡- ____________________ ", "ጥሰቱ ዓይነት፡- " + caseType);
                var suspect3Rep = caseTypeRep.Replace(@"ሔለን3____ ", " " + suspectName);
                var caseType3Rep = suspect3Rep.Replace(@"የ20102_ ", " " + caseType);
                var suspect4Rep = caseType3Rep.Replace(@"ሔለን4_______ ", " " + suspectName);
                var suspect5Rep = suspect4Rep.Replace(@"ሔለን5_______   ", " " + suspectName);
                var suspectNameRep = suspect5Rep.Replace(@"ሔለን6_______  ", " " + suspectName);
                
                LetterContent letter = new LetterContent();
                letter.BodyHeader = suspectNameRep;
                return letter;
                //end of letter contente
            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }
        //end of print

        //Warning print start
        public async Task<LetterContent> GetCaseApplicationWarningPrintDetailById(Guid caseApplicationId)
        {
            try
            {
                CaseApplicationDTO data = null;
                List<CaseParty> listOfSuspect = new List<CaseParty>();
                CaseParty suspect = new CaseParty();
                suspect = null;
                var plaintIffName = "";
                var EtCurrentDate = "";
                var suspectName = "";
                var PlaintIffAdress = "";
                var SuspectAddress = "";

                var result = await _context.CaseApplication.Where(c => c.CaseApplicationId == caseApplicationId && c.CAUnit.IsDeleted == false)
                    .Include(c => c.CaseType)
                    .Include(c => c.Department)
                    .Include(c => c.CAUnit)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Region)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Zone)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Woreda)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Kebele)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Nationality)
                    .Include(c => c.CaseStatusType)
                    .FirstOrDefaultAsync();
                var caseParties = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).ToList();
                // start
                var caseAppl = _context.Case.Where(x => x.CaseApplicationId == result.CAUnit.CAUnitId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
                var caseApplications = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == result.CAUnit.CAUnitId);
                var region = "";
                var zone = "";
                var woreda = "";
                var kebele = "";
                var plaintIff = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).FirstOrDefault(x => x.PartyCategory == 2);
                listOfSuspect = caseParties.Where(x => x.PartyCategory != 4 && x.PartyCategory != 2).ToList();
                if (plaintIff != null)
                {
                    if (plaintIff.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == plaintIff.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (customerInformation.RegionId > 0)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (customerInformation.ZoneId > 0)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (customerInformation.WoredaId > 0)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (customerInformation.KebeleId > 0)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                            else
                            {
                                plaintIffName = exca?.OrganizationNameAmh;
                                EtCurrentDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString());
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                        }
                    }
                    else
                    {
                        if (plaintIff.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == plaintIff.RegionId)?.DescriptionAmh;
                        if (plaintIff.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == plaintIff.ZoneId)?.DescriptionAmh;
                        if (plaintIff.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == plaintIff.WoredaId)?.DescriptionAmh;
                        if (plaintIff.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == plaintIff.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        plaintIffName = (!string.IsNullOrWhiteSpace(plaintIff?.OrganizationNameAmh)) ? plaintIff?.OrganizationNameAmh : $"{ plaintIff?.FirstNameAmh } {plaintIff.FatherNameAmh} {plaintIff.GrandFatherNameAmh}";
                    }

                }
                if (listOfSuspect.Any())
                {
                    foreach (var ssu in listOfSuspect)
                    {
                        if (ssu.ExchangeActorId != null)
                        {
                            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == ssu.ExchangeActorId);

                            if (exca != null)
                            {
                                if (exca?.DelegatorId != null)
                                {
                                    var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";

                                    }
                                    else
                                    {
                                        suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + exca?.OrganizationNameAmh;
                                    }
                                    else
                                    {
                                        suspectName = exca?.OrganizationNameAmh;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(suspectName))
                            {
                                suspectName = suspectName + "," + ((!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}");
                            }
                            else
                            {
                                suspectName = (!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}";
                            }
                        }
                    }

                }
                if (suspect != null)
                {
                    if (suspect?.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == suspect.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{kebele}";
                            }
                            else
                            {
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                                suspectName = exca?.OrganizationNameAmh;
                            }
                        }
                    }
                    else
                    {
                        if (suspect.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == suspect.RegionId)?.DescriptionAmh;
                        if (suspect.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == suspect.ZoneId)?.DescriptionAmh;
                        if (suspect.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == suspect.WoredaId)?.DescriptionAmh;
                        if (suspect.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == suspect.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        SuspectAddress = $"{region} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        suspectName = (!string.IsNullOrEmpty(suspect?.OrganizationNameAmh)) ? suspect?.OrganizationNameAmh : $"{ suspect?.FirstNameAmh } {suspect.FatherNameAmh} {suspect.GrandFatherNameAmh}";
                    }

                }
                // end

                if (result != null)
                {
                    data = _mapper.Map<CaseApplicationDTO>(result);
                    if (result.Suspect?.ExchangeActor != null)
                    {
                        data.Suspect.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Suspect?.ExchangeActor?.Ecxcode,
                            Tin = result.Suspect?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Suspect.ExchangeActorId.Value,
                            OrganizationNameAmh = result.Suspect?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Suspect?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Suspect.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    if (result.Plaintiff?.ExchangeActor != null)
                    {
                        data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Plaintiff?.ExchangeActor?.Ecxcode,
                            Tin = result.Plaintiff?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Plaintiff?.ExchangeActorId.Value,
                            OrganizationNameAmh = plaintIffName = result.Plaintiff?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Plaintiff?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    data.ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ApplicationDate).ToString());
                    data.EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.EventDate).ToString());
                    data.CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString());
                    data.UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString());
                }
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "InvestigationWarningLetter").FirstOrDefaultAsync();
                var LetterDateRep = letterContent.BodyHeader.Replace(@"ቀን __________", "ቀን	፡ " + "<strong>" + EtCurrentDate + "</strong>");
                var caseDate = "";
                var caseType = "";
                //var caseName = ""; 
                if (caseApplications != null)
                {
                    caseDate = EthiopicDateTime.GetEthiopicDate(caseApplications.ApplicationDate.Day, caseApplications.ApplicationDate.Month, caseApplications.ApplicationDate.Year);
                    caseType = _context.Lookup.FirstOrDefault(x => x.LookupId == caseApplications.CaseTypeId)?.DescriptionAmh;
                    //caseName = caseApplications?.?.OrganizationNameAmh;
                }
                var caseDateRep = LetterDateRep.Replace(@"ሂደት __________", "ሂደት " + "<strong>" + caseDate + "</strong>");
                var suspectRep = caseDateRep.Replace(@"ሔለን1 ________________ ", " " + "<strong>" + suspectName + " </strong> ");
                var caseType0Rep = suspectRep.Replace(@"የግብይት ፈፃሚ ______________", " " + caseType);
                var suspect2Rep = caseType0Rep.Replace(@"ሔለን2 ___________  ", " " + "<strong>" + suspectName + " </strong> ");
                var caseTypeRep = suspect2Rep.Replace(@"የ2010______  ", " " + caseType);
                //var caseType2Rep = caseTypeRep.Replace(@"ጥሰቱ ዓይነት፡- ____________________ ", "ጥሰቱ ዓይነት፡- " + caseType);
                var suspect3Rep = caseTypeRep.Replace(@"ሔለን3____ ", " " + suspectName);
                var caseType3Rep = suspect3Rep.Replace(@"የ20102_ ", " " + caseType);
                var suspect4Rep = caseType3Rep.Replace(@"ሔለን4_______ ", " " + suspectName);
                var suspect5Rep = suspect4Rep.Replace(@"ሔለን5_______   ", " " + suspectName);
                var suspectNameRep = suspect5Rep.Replace(@"ሔለን6_______  ", " " + suspectName);

                LetterContent letter = new LetterContent();
                letter.BodyHeader = suspectNameRep;
                return letter;
                //end of letter contente
            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }

        //Warning print end

        //TerminatedOrClosed start
        public async Task<LetterContent> GetCaseApplicationTerminatedOrClosedPrintDetailById(Guid caseApplicationId)
        {
            try
            {
                CaseApplicationDTO data = null;
                List<CaseParty> listOfSuspect = new List<CaseParty>();
                CaseParty suspect = new CaseParty();
                suspect = null;
                var plaintIffName = "";
                var EtCurrentDate = "";
                var suspectName = "";
                var PlaintIffAdress = "";
                var SuspectAddress = "";

                var result = await _context.CaseApplication.Where(c => c.CaseApplicationId == caseApplicationId && c.CAUnit.IsDeleted == false)
                    .Include(c => c.CaseType)
                    .Include(c => c.Department)
                    .Include(c => c.CAUnit)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Suspect).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.CustomerType)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.ExchangeActor).ThenInclude(c => c.OwnerManager)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Region)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Zone)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Woreda)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Kebele)
                    .Include(c => c.Plaintiff).ThenInclude(c => c.Nationality)
                    .Include(c => c.CaseStatusType)
                    .FirstOrDefaultAsync();
                var caseParties = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).ToList();
                // start
                var caseAppl = _context.Case.Where(x => x.CaseApplicationId == result.CAUnit.CAUnitId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
                var caseApplications = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == result.CAUnit.CAUnitId);
                var region = "";
                var zone = "";
                var woreda = "";
                var kebele = "";
                var plaintIff = _context.CaseParty.Where(x => x.CAUnitId == result.CAUnit.CAUnitId).FirstOrDefault(x => x.PartyCategory == 2);
                listOfSuspect = caseParties.Where(x => x.PartyCategory != 4 && x.PartyCategory != 2).ToList();
                if (plaintIff != null)
                {
                    if (plaintIff.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == plaintIff.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (customerInformation.RegionId > 0)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (customerInformation.ZoneId > 0)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (customerInformation.WoredaId > 0)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (customerInformation.KebeleId > 0)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                            else
                            {
                                plaintIffName = exca?.OrganizationNameAmh;
                                EtCurrentDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString());
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                            }
                        }
                    }
                    else
                    {
                        if (plaintIff.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == plaintIff.RegionId)?.DescriptionAmh;
                        if (plaintIff.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == plaintIff.ZoneId)?.DescriptionAmh;
                        if (plaintIff.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == plaintIff.WoredaId)?.DescriptionAmh;
                        if (plaintIff.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == plaintIff.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        plaintIffName = (!string.IsNullOrWhiteSpace(plaintIff?.OrganizationNameAmh)) ? plaintIff?.OrganizationNameAmh : $"{ plaintIff?.FirstNameAmh } {plaintIff.FatherNameAmh} {plaintIff.GrandFatherNameAmh}";
                    }

                }
                if (listOfSuspect.Any())
                {
                    foreach (var ssu in listOfSuspect)
                    {
                        if (ssu.ExchangeActorId != null)
                        {
                            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == ssu.ExchangeActorId);

                            if (exca != null)
                            {
                                if (exca?.DelegatorId != null)
                                {
                                    var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";

                                    }
                                    else
                                    {
                                        suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrWhiteSpace(suspectName))
                                    {
                                        suspectName = suspectName + "," + exca?.OrganizationNameAmh;
                                    }
                                    else
                                    {
                                        suspectName = exca?.OrganizationNameAmh;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(suspectName))
                            {
                                suspectName = suspectName + "," + ((!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}");
                            }
                            else
                            {
                                suspectName = (!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}";
                            }
                        }
                    }

                }
                if (suspect != null)
                {
                    if (suspect?.ExchangeActorId != null)
                    {
                        var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == suspect.ExchangeActorId);

                        if (exca != null)
                        {
                            if (exca?.DelegatorId != null)
                            {
                                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{kebele}";
                            }
                            else
                            {
                                if (exca.RegionId != null)
                                    region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                                if (exca.ZoneId != null)
                                    zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                                if (exca.WoredaId != null)
                                    woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                                if (exca.KebeleId != null)
                                    kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                                if (region != "")
                                    region = "ክልል:- " + region;
                                if (zone != "")
                                    zone = "ዞን :- " + zone;
                                if (woreda != "")
                                    woreda = "ወረዳ :- " + woreda;
                                if (kebele != "")
                                    kebele = "ቀበሌ:- " + kebele;
                                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                                suspectName = exca?.OrganizationNameAmh;
                            }
                        }
                    }
                    else
                    {
                        if (suspect.RegionId != null)
                            region = _context.Region.FirstOrDefault(d => d.RegionId == suspect.RegionId)?.DescriptionAmh;
                        if (suspect.ZoneId != null)
                            zone = _context.Zone.FirstOrDefault(z => z.ZoneId == suspect.ZoneId)?.DescriptionAmh;
                        if (suspect.WoredaId != null)
                            woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == suspect.WoredaId)?.DescriptionAmh;
                        if (suspect.KebeleId != null)
                            kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == suspect.KebeleId)?.DescriptionAmh;
                        if (region != "")
                            region = "ክልል:- " + region;
                        if (zone != "")
                            zone = "ዞን :- " + zone;
                        if (woreda != "")
                            woreda = "ወረዳ :- " + woreda;
                        if (kebele != "")
                            kebele = "ቀበሌ:- " + kebele;
                        SuspectAddress = $"{region} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                        suspectName = (!string.IsNullOrEmpty(suspect?.OrganizationNameAmh)) ? suspect?.OrganizationNameAmh : $"{ suspect?.FirstNameAmh } {suspect.FatherNameAmh} {suspect.GrandFatherNameAmh}";
                    }

                }
                // end

                if (result != null)
                {
                    data = _mapper.Map<CaseApplicationDTO>(result);
                    if (result.Suspect?.ExchangeActor != null)
                    {
                        data.Suspect.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Suspect?.ExchangeActor?.Ecxcode,
                            Tin = result.Suspect?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Suspect.ExchangeActorId.Value,
                            OrganizationNameAmh = result.Suspect?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Suspect?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Suspect?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Suspect?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Suspect?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Suspect.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    if (result.Plaintiff?.ExchangeActor != null)
                    {
                        data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO
                        {
                            Ecxcode = result.Plaintiff?.ExchangeActor?.Ecxcode,
                            Tin = result.Plaintiff?.ExchangeActor?.Tin,
                            ExchangeActorId = result.Plaintiff?.ExchangeActorId.Value,
                            OrganizationNameAmh = plaintIffName = result.Plaintiff?.ExchangeActor?.OrganizationNameAmh,
                            OrganizationNameEng = result.Plaintiff?.ExchangeActor?.OrganizationNameEng,
                            FullNameAmh = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameAmh + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                            FullNameEng = result.Plaintiff?.ExchangeActor?.OwnerManager?.FirstNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.FatherNameEng + " " + result.Plaintiff?.ExchangeActor?.OwnerManager?.GrandFatherNameEng
                        };
                    }
                    else
                    {
                        if (result.Plaintiff?.ExchangeActor != null)
                        {
                            data.Plaintiff.ExchangeActor = new ExchangeActorCasePartyDTO();
                        }
                    }
                    data.ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ApplicationDate).ToString());
                    data.EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.EventDate).ToString());
                    data.CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString());
                    data.UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString());
                }
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "InvestigationTerminatedOrClosedLetter").FirstOrDefaultAsync();
                var LetterDateRep = letterContent.BodyHeader.Replace(@"ቀን __________", "ቀን	፡ " + "<strong>" + EtCurrentDate + "</strong>");
                var caseDate = "";
                var caseType = "";
                //var caseName = ""; 
                if (caseApplications != null)
                {
                    caseDate = EthiopicDateTime.GetEthiopicDate(caseApplications.ApplicationDate.Day, caseApplications.ApplicationDate.Month, caseApplications.ApplicationDate.Year);
                    caseType = _context.Lookup.FirstOrDefault(x => x.LookupId == caseApplications.CaseTypeId)?.DescriptionAmh;
                    //caseName = caseApplications?.?.OrganizationNameAmh;
                }
                var caseDateRep = LetterDateRep.Replace(@"ሂደት __________", "ሂደት " + "<strong>" + caseDate + "</strong>");
                var suspectRep = caseDateRep.Replace(@"ሔለን1 ________________ ", " " + "<strong>" + suspectName + " </strong> ");
                var caseType0Rep = suspectRep.Replace(@"የግብይት ፈፃሚ ______________", " " + caseType);
                var suspect2Rep = caseType0Rep.Replace(@"ሔለን2 ___________  ", " " + "<strong>" + suspectName + " </strong> ");
                var caseTypeRep = suspect2Rep.Replace(@"የ2010______  ", " " + caseType);
                //var caseType2Rep = caseTypeRep.Replace(@"ጥሰቱ ዓይነት፡- ____________________ ", "ጥሰቱ ዓይነት፡- " + caseType);
                var suspect3Rep = caseTypeRep.Replace(@"ሔለን3____ ", " " + suspectName);
                var caseType3Rep = suspect3Rep.Replace(@"የ20102_ ", " " + caseType);
                var suspect4Rep = caseType3Rep.Replace(@"ሔለን4_______ ", " " + suspectName);
                var suspect5Rep = suspect4Rep.Replace(@"ሔለን5_______   ", " " + suspectName);
                var suspectNameRep = suspect5Rep.Replace(@"ሔለን6_______  ", " " + suspectName);

                LetterContent letter = new LetterContent();
                letter.BodyHeader = suspectNameRep;
                return letter;
                //end of letter contente
            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }

        //TerminatedOrClosed end
    }
}
