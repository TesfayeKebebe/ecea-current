using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
  public class CaseAppointmentRepository
  {
    private readonly ECEADbContext _context;

    private readonly IMapper _mapper;

    public CaseAppointmentRepository(ECEADbContext context, IMapper mapper)
    {
      _context = context;
      _mapper = mapper;


    }

    public async Task<AppointmentDTO> Create(AppointmentDTO postedAppointment)
    {
      var caseApplication = await _context.CaseApplication
          .Where(ca => ca.CaseInvestigation.Case.CaseId == postedAppointment.CaseId && ca.IsDeleted == false
          && (ca.CaseStatusTypeId == 29 || ca.CaseStatusTypeId == 34 || ca.CaseStatusTypeId == 31)
          )
          .AsNoTracking().FirstOrDefaultAsync();
      if (caseApplication == null)
        return null;

      DateTime dateTime = DateTime.Now;
      var caseAppointment = _mapper.Map<CaseAppointment>(postedAppointment);
      caseAppointment.CreatedDateTime = dateTime;
      caseAppointment.UpdatedDateTime = dateTime;
      caseAppointment.AppointmentDate = caseAppointment.AppointmentDate.AddDays(1);
      caseAppointment.IsActive = true;
      caseAppointment.IsDeleted = false;
      caseAppointment.CaseAppointmentId = Guid.NewGuid();
      caseApplication.CaseStatusTypeId = 31;
      var lastAppointment = _context.Case.FirstOrDefault(x => x.CaseId == postedAppointment.CaseId);
      lastAppointment.LastAppointmentId = caseAppointment.CaseAppointmentId;
      using (var transaction = await _context.Database.BeginTransactionAsync())
      {
        try
        {
          _context.Entry(caseApplication).State = EntityState.Modified;
          _context.CaseAppointment.Add(caseAppointment);


          _context.SaveChanges();
          transaction.Commit();
          postedAppointment.CaseAppointmentId = caseAppointment.CaseAppointmentId;


          return postedAppointment;

        }
        catch (Exception ex)
        {
          transaction.Rollback();
          throw new Exception(ex.InnerException.ToString());
        }
      }
    }

    public async Task<AppointmentDTO> Update(AppointmentDTO postedAppointment)
    {
      //var oldAppointments = await _context.CaseAppointment
      //    .Where(ca => ca.CaseId == postedAppointment.CaseId && ca.AppointmentDate>postedAppointment.AppointmentDate  && ca.IsDeleted == false
      //    )
      //    .AsNoTracking().ToListAsync();
      //if (oldAppointments.Count > 0)
      //    return null;
      var oldAppointment = await _context.CaseAppointment.Where(ap => ap.CaseAppointmentId == postedAppointment.CaseAppointmentId).AsNoTracking().FirstOrDefaultAsync();
      if (oldAppointment == null)
        return null;
      DateTime dateTime = DateTime.Now;
      var caseAppointment = _mapper.Map<CaseAppointment>(postedAppointment);
      caseAppointment.CreatedDateTime = oldAppointment.CreatedDateTime;
      caseAppointment.UpdatedDateTime = oldAppointment.UpdatedDateTime;
      caseAppointment.IsActive = oldAppointment.IsActive;
      caseAppointment.IsDeleted = oldAppointment.IsDeleted;
      caseAppointment.CreatedUserId = oldAppointment.CreatedUserId;
      caseAppointment.CaseId = oldAppointment.CaseId;
      if (oldAppointment.AppointmentDate <= caseAppointment.AppointmentDate.AddDays(1))
      {
        caseAppointment.AppointmentDate = caseAppointment.AppointmentDate.AddDays(1);
      }
      //var lastAppointment = _context.Case.FirstOrDefault(x => x.CaseApplicationId == oldAppointment.CaseId);
      //lastAppointment.LastAppointmentId = caseAppointment.CaseAppointmentId;
      using (var transaction = await _context.Database.BeginTransactionAsync())
      {
        try
        {
          _context.Entry(caseAppointment).State = EntityState.Modified;


          _context.SaveChanges();
          transaction.Commit();
          return postedAppointment;

        }
        catch (Exception ex)
        {
          transaction.Rollback();
          throw new Exception(ex.InnerException.ToString());
        }
      }
    }
    public async Task<AppointmentDTO> GetByAppointmentId(Guid appointmentId)
    {
      try
      {
        var dataResult = await (from ap in _context.CaseAppointment
                                join apt in _context.Lookup
                                  on ap.AppointmentTypeId equals apt.LookupId
                                where (
                                  ap.CaseAppointmentId == appointmentId &&
                                  ap.IsDeleted == false
                                  )
                                select new AppointmentDTO
                                {
                                  CaseId = ap.CaseId,
                                  AppointmentDate = ap.AppointmentDate,
                                  AppointmentTime = ap.AppointmentTime,
                                  AppointmentType = _mapper.Map<LookUpDisplayView>(ap.AppointmentType),
                                  AppointmentTypeId = ap.AppointmentTypeId,
                                  CaseAppointmentId = ap.CaseAppointmentId,
                                  AppointmentReason = ap.AppointmentReason,
                                  AmhAppointmentReason = ap.AmhAppointmentReason,
                                  Location = ap.Location,
                                  AmhOtherRemark = ap.AmhOtherRemark,
                                  EngOtherRemark = ap.EngOtherRemark

                                })
                            .AsNoTracking()
                            .FirstOrDefaultAsync();
        return dataResult;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }

    public async Task<PagedResult<AppointmentDTO>> GetAllCaseAppointments(Guid caseId, int pageIndex, int pageSize)
    {
      try
      {
        var dataResult = await (from ap in _context.CaseAppointment
                                join apt in _context.Lookup
                                  on ap.AppointmentTypeId equals apt.LookupId
                                where (
                                  ap.CaseId == caseId &&
                                  ap.IsDeleted == false
                                  )
                                orderby (ap.CreatedDateTime) descending

                                select new AppointmentDTO
                                {
                                  CaseId = ap.CaseId,
                                  AppointmentDate = ap.AppointmentDate,
                                  AppointmentTime = ap.AppointmentTime,
                                  AppointmentType = _mapper.Map<LookUpDisplayView>(ap.AppointmentType),
                                  AppointmentTypeId = ap.AppointmentTypeId,
                                  CaseAppointmentId = ap.CaseAppointmentId,
                                  AppointmentReason = ap.AppointmentReason,
                                  AmhAppointmentReason = ap.AmhAppointmentReason,
                                  Location = ap.Location,
                                  AmhOtherRemark = ap.AmhOtherRemark,
                                  EngOtherRemark = ap.EngOtherRemark
                                })
                                    .AsNoTracking()
                                    .ToListAsync();


        dataResult.AsQueryable().Paging(pageSize, pageIndex);

        return new PagedResult<AppointmentDTO>()
        {
          Items = _mapper.Map<List<AppointmentDTO>>(dataResult),
          ItemsCount = dataResult.Count()
        };

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }
    public async Task<LetterContent> GetLastAppointed(Guid casepartyId, Guid Cauid, int PartyCategory)
    {
      try
      {
        List<CaseParty> listOfSuspect = new List<CaseParty>();
        CaseParty suspect = new CaseParty();
        CaseParty witness = new CaseParty();
        suspect = null;
        witness = null;
var plaintIffName = "";
        var suspectName = "";
        var PlaintIffAdress = "";
        var SuspectAddress = "";
        var witnessName = "";
        var caseParties = _context.CaseParty.Where(x => x.CAUnitId == Cauid).ToList();
        var plaintIff = caseParties.FirstOrDefault(x => x.PartyCategory == 2);

        if (PartyCategory==4)
        {
          witness = caseParties.FirstOrDefault(x => x.PartyCategory == PartyCategory && x.CasePartyId==casepartyId);
          listOfSuspect = caseParties.Where(x => x.PartyCategory != 4 && x.PartyCategory!=2).ToList();
        }
        else
        {
          suspect = caseParties.FirstOrDefault(x => x.CasePartyId == casepartyId);

        }

        var caseAppl = _context.Case.Where(x => x.CaseApplicationId == Cauid).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
        var caseApplications = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == Cauid);
        var caseAppoints = await _context.CaseAppointment.Where(x => x.CaseId == caseAppl.CaseId).ToListAsync();
        var textAnswer = caseAppoints.Where(x => x.AppointmentTypeId == 54).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
        var others = caseAppoints.FirstOrDefault(x => x.CaseAppointmentId == caseAppl.LastAppointmentId);
        var region = "";
        var zone = "";
        var woreda = "";
        var kebele = "";
        if (plaintIff != null)
        {
          if (plaintIff.ExchangeActorId != null)
          {
            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == plaintIff.ExchangeActorId);

            if (exca != null)
            {
              if (exca.DelegatorId != null)
              {
                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                if (customerInformation.RegionId > 0)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                if (customerInformation.ZoneId > 0)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                if (customerInformation.WoredaId > 0)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                if (customerInformation.KebeleId > 0)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
              }
              else
              {
                plaintIffName = exca?.OrganizationNameAmh;
                if (exca.RegionId != null)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                if (exca.ZoneId != null)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                if (exca.WoredaId != null)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                if (exca.KebeleId != null)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
              }

            }
          }
          else
          {
            if (plaintIff.RegionId != null)
              region = _context.Region.FirstOrDefault(d => d.RegionId == plaintIff.RegionId)?.DescriptionAmh;
            if (plaintIff.ZoneId != null)
              zone = _context.Zone.FirstOrDefault(z => z.ZoneId == plaintIff.ZoneId)?.DescriptionAmh;
            if (plaintIff.WoredaId != null)
              woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == plaintIff.WoredaId)?.DescriptionAmh;
            if (plaintIff.KebeleId != null)
              kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == plaintIff.KebeleId)?.DescriptionAmh;
            if (region != "")
              region = "ክልል:- " + region;
            if (zone != "")
              zone = "ዞን :- " + zone;
            if (woreda != "")
              woreda = "ወረዳ :- " + woreda;
            if (kebele != "")
              kebele = "ቀበሌ:- " + kebele;
            PlaintIffAdress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
            plaintIffName = (!string.IsNullOrWhiteSpace(plaintIff?.OrganizationNameAmh)) ? plaintIff?.OrganizationNameAmh : $"{ plaintIff?.FirstNameAmh } {plaintIff.FatherNameAmh} {plaintIff.GrandFatherNameAmh}";
          }

        }
        if(listOfSuspect.Any())
        {
          foreach(var ssu in listOfSuspect)
          {
            if (ssu.ExchangeActorId != null)
            {
              var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == ssu.ExchangeActorId);


              if (exca != null)
              {
                if (exca.DelegatorId != null)
                {
                  var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                     if(!string.IsNullOrWhiteSpace(suspectName))
                  {
                    suspectName = suspectName + "," + $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";

                  }
                     else
                  {
                    suspectName =  $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                  }
                
                }
                else
                {
                 
                  if (!string.IsNullOrWhiteSpace(suspectName))
                  {
                    suspectName = suspectName + "," + exca?.OrganizationNameAmh;
                  }
                  else
                  {
                    suspectName = exca?.OrganizationNameAmh;
                  }
                
                }

              }
            }
            else
            {
              
              if (!string.IsNullOrWhiteSpace(suspectName))
              {
                suspectName = suspectName + "," +( (!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}");

              }
              else
              {
                suspectName = (!string.IsNullOrEmpty(ssu?.OrganizationNameAmh)) ? ssu?.OrganizationNameAmh : $"{ ssu?.FirstNameAmh } {ssu.FatherNameAmh} {ssu.GrandFatherNameAmh}";
              }
            }
          }
         
        }
        if (suspect != null)
        {
          if (suspect.ExchangeActorId != null)
          {
            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == suspect.ExchangeActorId);


            if (exca != null)
            {
              if (exca.DelegatorId != null)
              {
                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                suspectName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                if (exca.RegionId != null)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                if (exca.ZoneId != null)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                if (exca.WoredaId != null)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                if (exca.KebeleId != null)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{kebele}";
              }
              else
              {
                if (exca.RegionId != null)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                if (exca.ZoneId != null)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                if (exca.WoredaId != null)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                if (exca.KebeleId != null)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                suspectName = exca?.OrganizationNameAmh;
              }

            }
          }
          else
          {
            if (suspect.RegionId != null)
              region = _context.Region.FirstOrDefault(d => d.RegionId == suspect.RegionId)?.DescriptionAmh;
            if (suspect.ZoneId != null)
              zone = _context.Zone.FirstOrDefault(z => z.ZoneId == suspect.ZoneId)?.DescriptionAmh;
            if (suspect.WoredaId != null)
              woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == suspect.WoredaId)?.DescriptionAmh;
            if (suspect.KebeleId != null)
              kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == suspect.KebeleId)?.DescriptionAmh;
            if (region != "")
              region = "ክልል:- " + region;
            if (zone != "")
              zone = "ዞን :- " + zone;
            if (woreda != "")
              woreda = "ወረዳ :- " + woreda;
            if (kebele != "")
              kebele = "ቀበሌ:- " + kebele;
            SuspectAddress = $"{region} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
            suspectName = (!string.IsNullOrEmpty(suspect?.OrganizationNameAmh))?suspect?.OrganizationNameAmh: $"{ suspect?.FirstNameAmh } {suspect.FatherNameAmh} {suspect.GrandFatherNameAmh}";
          }

        }
        if (witness != null)
        {
          if (witness.ExchangeActorId != null)
          {
            var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == witness.ExchangeActorId);


            if (exca != null)
            {
              if (exca.DelegatorId != null)
              {
                var customerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                witnessName = $"{ customerInformation.FirstNameAmh} { customerInformation.FatherNameAmh} { customerInformation.GrandFatherNameAmh}";
                if (exca.RegionId != null)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == customerInformation.RegionId)?.DescriptionAmh;
                if (exca.ZoneId != null)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == customerInformation.ZoneId)?.DescriptionAmh;
                if (exca.WoredaId != null)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == customerInformation.WoredaId)?.DescriptionAmh;
                if (exca.KebeleId != null)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == customerInformation.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{kebele}";
              }
              else
              {
                if (exca.RegionId != null)
                  region = _context.Region.FirstOrDefault(d => d.RegionId == exca.RegionId)?.DescriptionAmh;
                if (exca.ZoneId != null)
                  zone = _context.Zone.FirstOrDefault(z => z.ZoneId == exca.ZoneId)?.DescriptionAmh;
                if (exca.WoredaId != null)
                  woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == exca.WoredaId)?.DescriptionAmh;
                if (exca.KebeleId != null)
                  kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == exca.KebeleId)?.DescriptionAmh;
                if (region != "")
                  region = "ክልል:- " + region;
                if (zone != "")
                  zone = "ዞን :- " + zone;
                if (woreda != "")
                  woreda = "ወረዳ :- " + woreda;
                if (kebele != "")
                  kebele = "ቀበሌ:- " + kebele;
                SuspectAddress = $"{region}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
                witnessName = exca?.OrganizationNameAmh;
              }

            }
          }
          else
          {
            if (witness.RegionId != null)
              region = _context.Region.FirstOrDefault(d => d.RegionId == witness.RegionId)?.DescriptionAmh;
            if (witness.ZoneId != null)
              zone = _context.Zone.FirstOrDefault(z => z.ZoneId == witness.ZoneId)?.DescriptionAmh;
            if (witness.WoredaId != null)
              woreda = _context.Woreda.FirstOrDefault(z => z.WoredaId == witness.WoredaId)?.DescriptionAmh;
            if (witness.KebeleId != null)
              kebele = _context.Kebele.FirstOrDefault(x => x.KebeleId == witness.KebeleId)?.DescriptionAmh;
            if (region != "")
              region = "ክልል:- " + region;
            if (zone != "")
              zone = "ዞን :- " + zone;
            if (woreda != "")
              woreda = "ወረዳ :- " + woreda;
            if (kebele != "")
              kebele = "ቀበሌ:- " + kebele;
            SuspectAddress = $"{region} {"&nbsp; &nbsp; &nbsp; &nbsp;"}{zone}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {woreda}{"&nbsp; &nbsp; &nbsp; &nbsp;"} {kebele}";
            witnessName = (!string.IsNullOrEmpty(witness?.OrganizationNameAmh)) ? witness?.OrganizationNameAmh : $"{ witness?.FirstNameAmh } {witness.FatherNameAmh} {witness.GrandFatherNameAmh}";
          }

        }

        // LetterContent content = new LetterContent();
        var content = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "AppointmentLetter").FirstOrDefaultAsync();      
        var plaintIffRep = content.BodyHeader.Replace(@"ከሳሽ _________________________", "ከሳሽ:" + "<strong>" + plaintIffName + "</strong>");
        if (listOfSuspect.Any())
        {
          var suspectRep = plaintIffRep.Replace(@"ተከሳሽ____________________", "ተከሳሽ: " + "<strong>" + suspectName + " </strong> " + "<br/>" + "ምስክር: " + "<strong>" + witnessName + " </strong> ");
          var dateRep = suspectRep.Replace(@"ቀን፦", "ቀን:-" + EthiopicDateTime.GetEthiopicDate(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
          var fileNomRep = dateRep.Replace(@"የመዝገብ ቁጥር፦", "የመዝገብ ቁጥር:-" + caseAppl?.FileNo);
          var plaintIffAddressRep = fileNomRep.Replace(@"አድራሻ<strong>_____________________________</strong>", "አድራሻ: " + "<strong>" + PlaintIffAdress + "</strong>");
          var suspectAddressRep = plaintIffAddressRep.Replace(@"የተከሳሽ <strong>_____________________________</strong>", "አድራሻ: " + "<strong>" + SuspectAddress + "</strong>");
          var caseDate = "";
          var caseType = "";
          if (caseApplications != null)
          {
            caseDate = EthiopicDateTime.GetEthiopicDate(caseApplications.ApplicationDate.Day, caseApplications.ApplicationDate.Month, caseApplications.ApplicationDate.Year);
            caseType = _context.Lookup.FirstOrDefault(x => x.LookupId == caseApplications.CaseTypeId)?.DescriptionAmh;
          }
          var PlaintNameRep = suspectAddressRep.Replace(@"ከሳሽ ጥቅምት 10 ቀን 2010 ዓም", "ከሳሽ " + caseDate + " ዓ.ም");
          var suspectNameRep = PlaintNameRep.Replace(@"ጥስት", "<strong>" + caseType + "</strong>");
          var answerRep = suspectNameRep;
          var othersRep = answerRep;
          if (textAnswer != null && others != null)
          {
            answerRep = suspectNameRep.Replace(@"አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ", "አያይዘዉ " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(textAnswer.AppointmentTime) + " ");
            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(others.AppointmentDate.Day, others.AppointmentDate.Month, others.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(others.AppointmentTime) + " ");

          }
          else if (textAnswer == null && others != null)
          {
            answerRep = suspectNameRep.Replace(@" ልዚህ የሚሰጡትን መልስ በፅሁፍ አዘገአጅተዉ ማስረጃ ካልዎት አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ስዓት ለችሎት ሬጅስትራር ቦታ----- እንዲያቀርቡ እንደዚሁም ", " ");

            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(others.AppointmentDate.Day, others.AppointmentDate.Month, others.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(others.AppointmentTime) + " ");

          }
          else if (textAnswer != null && others == null)
          {
            answerRep = suspectNameRep.Replace(@"አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ", "አያይዘዉ " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(textAnswer.AppointmentTime) + " ");
            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም  በ" + Convert.ToString(textAnswer.AppointmentTime));
          }

          var locationRep = othersRep.Replace(@"ቦታ-----", " " + "<strong>" + others?.Location + "</strong>" + " ");

          LetterContent letter = new LetterContent();
          letter.BodyHeader = locationRep;
          return letter;
        }
        else
        {
          var suspectRep = plaintIffRep.Replace(@"ተከሳሽ____________________", "ተከሳሽ: " + "<strong>" + suspectName + " </strong> ");
          var dateRep = suspectRep.Replace(@"ቀን፦", "ቀን:-" + EthiopicDateTime.GetEthiopicDate(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
          var fileNomRep = dateRep.Replace(@"የመዝገብ ቁጥር፦", "የመዝገብ ቁጥር:-" + caseAppl?.FileNo);
          var plaintIffAddressRep = fileNomRep.Replace(@"አድራሻ<strong>_____________________________</strong>", "አድራሻ: " + "<strong>" + PlaintIffAdress + "</strong>");
          var suspectAddressRep = plaintIffAddressRep.Replace(@"የተከሳሽ <strong>_____________________________</strong>", "አድራሻ: " + "<strong>" + SuspectAddress + "</strong>");
          var caseDate = "";
          var caseType = "";
          if (caseApplications != null)
          {
            caseDate = EthiopicDateTime.GetEthiopicDate(caseApplications.ApplicationDate.Day, caseApplications.ApplicationDate.Month, caseApplications.ApplicationDate.Year);
            caseType = _context.Lookup.FirstOrDefault(x => x.LookupId == caseApplications.CaseTypeId)?.DescriptionAmh;
          }
          var PlaintNameRep = suspectAddressRep.Replace(@"ከሳሽ ጥቅምት 10 ቀን 2010 ዓም", "ከሳሽ " + caseDate + " ዓ.ም");
          var suspectNameRep = PlaintNameRep.Replace(@"ጥስት", "<strong>" + caseType + "</strong>");
          var answerRep = suspectNameRep;
          var othersRep = answerRep;
          if (textAnswer != null && others != null)
          {
            answerRep = suspectNameRep.Replace(@"አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ", "አያይዘዉ " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(textAnswer.AppointmentTime) + " ");
            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(others.AppointmentDate.Day, others.AppointmentDate.Month, others.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(others.AppointmentTime) + " ");

          }
          else if (textAnswer == null && others != null)
          {
            answerRep = suspectNameRep.Replace(@" ልዚህ የሚሰጡትን መልስ በፅሁፍ አዘገአጅተዉ ማስረጃ ካልዎት አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ስዓት ለችሎት ሬጅስትራር ቦታ----- እንዲያቀርቡ እንደዚሁም ", " ");

            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(others.AppointmentDate.Day, others.AppointmentDate.Month, others.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(others.AppointmentTime) + " ");

          }
          else if (textAnswer != null && others == null)
          {
            answerRep = suspectNameRep.Replace(@"አያይዘዉ ጥቅምት 20 ቀን 2010 ዓም በ330 ", "አያይዘዉ " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም በ" + Convert.ToString(textAnswer.AppointmentTime) + " ");
            othersRep = answerRep.Replace(@"ጥቅምት 21 ቀን 2010 ዓም 400 ", " " + EthiopicDateTime.GetEthiopicDate(textAnswer.AppointmentDate.Day, textAnswer.AppointmentDate.Month, textAnswer.AppointmentDate.Year) + " ዓ.ም  በ" + Convert.ToString(textAnswer.AppointmentTime));
          }

          var locationRep = othersRep.Replace(@"ቦታ-----", " " + "<strong>" + others?.Location + "</strong>" + " ");

          LetterContent letter = new LetterContent();
          letter.BodyHeader = locationRep;
          return letter;
        }
       
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<CasePartyAppointment>> GetCaseParty(Guid caseId, string lang)
    {
      List<CasePartyAppointment> addedCaseParties = new List<CasePartyAppointment>();
      var caseApp = await _context.Case.FirstOrDefaultAsync(x => x.CaseId == caseId && x.LastAppointmentId != null);
      if (caseApp != null)
      {
        var caseApplication = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == caseApp.CaseApplicationId);
        if (caseApplication != null)
        {
          var caseParties = _context.CaseParty.Where(x => x.CAUnitId == caseApplication.CaseApplicationId && x.PartyCategory != 2).ToList();
          foreach (var party in caseParties)
          {
            CasePartyAppointment casePartyDTO = new CasePartyAppointment();
            switch (party.PartyCategory)
            {
              case 1:
                casePartyDTO.Category = "ተከሳሽ";
                break;
              case 3:
                casePartyDTO.Category = "ተከሳሽ";
                break;
              case 4:
                casePartyDTO.Category = "ምስክር";
                break;
            }

            casePartyDTO.PartyCategory = party.PartyCategory;

            if (party.ExchangeActorId != null)
            {
              var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == party.ExchangeActorId);
              if (exca.DelegatorId != null)
              {
                var ownerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);
                casePartyDTO.Name = (lang == "et") ? $"{ownerInformation?.FirstNameAmh} {ownerInformation?.FatherNameAmh} {ownerInformation?.GrandFatherNameAmh}" : $"{ownerInformation?.FirstNameEng} {ownerInformation?.FatherNameEng} {ownerInformation?.GrandFatherNameEng}";

              }
              else
              {
                casePartyDTO.Name = (lang == "et") ? exca?.OrganizationNameAmh : exca.OrganizationNameEng;

              }
            }
            else
            {
              if (!string.IsNullOrWhiteSpace(party.OrganizationNameAmh))
              {
                casePartyDTO.Name = (lang == "et") ? $"{ party?.OrganizationNameAmh } " : $"{ party?.OrganizationNameEng }";
              }
              else
              {
                casePartyDTO.Name = (lang == "et") ? $"{ party?.FirstNameAmh } {party.FatherNameAmh} {party.GrandFatherNameAmh}" : $"{ party?.FirstNameEng } {party.FatherNameEng} {party.GrandFatherNameEng}";

              }

            }
            casePartyDTO.CasePartyId = party.CasePartyId;
            casePartyDTO.CAUnitId = party.CAUnitId;

            addedCaseParties.Add(casePartyDTO);

          }

        }
      }
      return addedCaseParties;
    }

  }

}
