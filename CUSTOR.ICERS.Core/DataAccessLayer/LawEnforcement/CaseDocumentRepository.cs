using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseDocumentRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CaseDocumentRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(CaseDocumentDTO postedAttachment)
        {
            var pathToSave = getUploadPath();

            string extension = getFileExtension(postedAttachment);

            string fileName = postedAttachment.FileName + extension;

            string filePath = Path.Combine(pathToSave, fileName);

            postedAttachment.FileName = fileName;

            try
            {
                var caUnit = await _context.CAUnit.Where(ca => ca.CAUnitId == postedAttachment.CAUnitId && ca.IsDeleted == false).FirstOrDefaultAsync();
                if (caUnit == null)
                    return false;

                DateTime dateTime = DateTime.Now;
                CaseDocument caseDocument = _mapper.Map<CaseDocument>(postedAttachment);
                caseDocument.CreatedDateTime = dateTime;
                caseDocument.UpdatedDateTime = dateTime;


                _context.CaseDocument.Add(caseDocument);
                _context.SaveChanges();


                // save to disk
                if (!Directory.Exists(pathToSave))
                    Directory.CreateDirectory(pathToSave);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await postedAttachment.ActualFile.CopyToAsync(stream);
                }


                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
        public async Task<bool> Update(CaseDocumentDTO updatedAttachment)
        {
            var oldDocumentUnit = await _context.CaseDocument.Where(ca => ca.CaseDocumentId == updatedAttachment.CaseDocumentId && ca.IsDeleted == false).FirstOrDefaultAsync();

            if (oldDocumentUnit == null)
                return false;

            DateTime dateTime = DateTime.Now;

            var pathToSave = getUploadPath();

            string extension = getFileExtension(updatedAttachment);

            // for updating
            string fileName = updatedAttachment.FileName + extension;

            // for deletion
            string todeleteFileName = oldDocumentUnit.FileName;

            string filePath = Path.Combine(pathToSave, fileName);

            string deletionPath = Path.Combine(pathToSave, todeleteFileName);

            try
            {
                // delete existing file with the given name
                if (File.Exists(deletionPath))
                {
                    File.Delete(deletionPath);
                }


                oldDocumentUnit.UpdatedDateTime = dateTime;
                oldDocumentUnit.AttachmentContent = updatedAttachment.AttachmentContent;
                oldDocumentUnit.FileName = fileName;


                await _context.SaveChangesAsync();

                // save update data
                if (!Directory.Exists(pathToSave))
                    Directory.CreateDirectory(pathToSave);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await updatedAttachment.ActualFile.CopyToAsync(stream);
                }

                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
        public async Task<CaseDocumentDTO> GetCaseDocumentById(Guid caseDocumentId)
        {
            CaseDocumentDTO result = null;
            var caseDocument = await _context.CaseDocument.Where(u => u.CaseDocumentId == caseDocumentId && u.IsDeleted == false)
                .AsNoTracking().FirstOrDefaultAsync();
            if (caseDocument != null)
            {
                result = _mapper.Map<CaseDocumentDTO>(caseDocument);
            }
            return result;
        }

        public async Task<IEnumerable<CaseDocumentDTO>> GetAllByCAUnitId(Guid caUnitId)
        {
            var result = new List<CaseDocumentDTO>();
            var caseDocuments = await _context.CaseDocument
                    .Where(u => u.CAUnitId == caUnitId && u.IsDeleted == false)
                .AsNoTracking().ToListAsync();
            result = _mapper.Map<List<CaseDocumentDTO>>(caseDocuments);
            return result;
        }

        private string getUploadPath()
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
        }
        private string getFileExtension(CaseDocumentDTO documentInformation)
        {
            return documentInformation.ActualFile.ContentType == "application/pdf" ? ".pdf" : documentInformation.ActualFile.ContentType == "image/jpeg" ? ".jpeg" : ".png";
        }

    }
}
