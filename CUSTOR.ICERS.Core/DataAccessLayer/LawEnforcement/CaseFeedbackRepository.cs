using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseFeedbackRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CaseFeedbackRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(CaseFeedbackDTO postedFeedback, Guid userId)
        {
            DateTime dateTime = DateTime.Now;
            var feedBack = _mapper.Map<CaseFeedback>(postedFeedback);
            feedBack.CreatedDateTime = dateTime;
            feedBack.UpdatedDateTime = dateTime;
            feedBack.CreatedUserId = userId;
            feedBack.FeedbackDate = dateTime;
            feedBack.EmployeeId = postedFeedback.CreatedUserId.ToString();
            var caseApplication = await _context.CaseApplication.Where(ca => (ca.CaseApplicationId == postedFeedback.CAUnitId || ca.CaseInvestigation.CaseInvestigationId == postedFeedback.CAUnitId) && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            
            if (caseApplication == null)
                return false;
            caseApplication.CaseStatusTypeId = postedFeedback.CaseStatusTypeId;
            
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(caseApplication).State = EntityState.Modified;
                    _context.CaseFeedback.Add(feedBack);

                    _context.SaveChanges();
                    transaction.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<bool> Update(CaseFeedbackDTO updatedFeedback, Guid userId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var caseApplication = await _context.CaseApplication.Where(ca => (ca.CaseApplicationId == updatedFeedback.CAUnitId || ca.CaseInvestigation.CaseInvestigationId == updatedFeedback.CAUnitId) && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
                    if (caseApplication == null)
                        return false;

                    var oldFeedback = await _context.CaseFeedback.Where(ca => ca.CaseFeedbackId == updatedFeedback.CaseFeedbackId && ca.IsDeleted == false).FirstOrDefaultAsync();
                    //CaseFeedback caseFeedback = null;

                    if (oldFeedback == null)
                        return false;
                    if (updatedFeedback.CaseStatusTypeId != 0)
                    {
                        //caseApplication.CaseStatusTypeId = updatedFeedback.CaseStatusTypeId;
                        caseApplication.CreatedUserId = updatedFeedback.CreatedUserId;
                        //caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId;
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.FeedbackTypeId = oldFeedback.FeedbackTypeId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.EmployeeId = updatedFeedback.UpdatedUserId.ToString();
                        //updatedFeedback.UpdatedUserId = new Guid(updatedFeedback.EmployeeId);
                    }
                    else
                    {
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        updatedFeedback.DateOfCaseInvestigation = updatedFeedback.DateOfCaseInvestigation;
                        updatedFeedback.DateOfProsecution = updatedFeedback.DateOfProsecution;
                        updatedFeedback.EmployeeId = updatedFeedback.EmployeeId;
                        //caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId; //CreatedUserId
                    }

                    DateTime dateTime = DateTime.Now;
                    updatedFeedback.CreatedDateTime = oldFeedback.CreatedDateTime;
                    updatedFeedback.UpdatedDateTime = dateTime;
                    updatedFeedback.IsActive = true;
                    updatedFeedback.IsDeleted = false;
                    //updatedFeedback.CaseFeedbackId = oldFeedback.CaseFeedbackId;
                    //var modifiedFeedBack = _mapper.Map<CaseFeedback>(updatedFeedback);

                    _context.Entry(caseApplication).State = EntityState.Modified;
                    //_context.Entry(updatedFeedback).State=EntityState.Modified;

                    oldFeedback = _mapper.Map(updatedFeedback, oldFeedback);

                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
                transaction.Commit();
                return true;
            } 
        }
        public async Task<bool> Assign(CaseFeedbackDTO updatedFeedback, Guid userId)
        {
            DateTime cdateTime = DateTime.Now;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var caseApplication = await _context.CaseApplication.Where(ca => (ca.CaseApplicationId == updatedFeedback.CAUnitId || ca.CaseInvestigation.CaseInvestigationId == updatedFeedback.CAUnitId) && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
                    if (caseApplication == null)
                        return false;
                    if (updatedFeedback.CaseInvestigationNo != null)
                    {
                        CaseInvestigation caseInvestigation = new CaseInvestigation
                        {
                            CaseInvestigationId = Guid.NewGuid(),
                            FileNo = updatedFeedback.CaseInvestigationNo,
                            CaseApplicationId = caseApplication.CaseApplicationId,
                            OpeningDate = cdateTime,
                            CreatedDateTime = cdateTime,
                            UpdatedDateTime = cdateTime
                        };
                        CAUnit iCAUnit = new CAUnit
                        {
                            CAUnitId = Guid.NewGuid(),
                            CreatedDateTime = cdateTime,
                            UpdatedDateTime = cdateTime
                        };
                        caseInvestigation.CaseInvestigationId = iCAUnit.CAUnitId;
                        _context.CAUnit.Add(iCAUnit);
                        _context.CaseInvestigation.Add(caseInvestigation);
                    }

                    var oldFeedback = await _context.CaseFeedback.Where(ca => ca.CaseFeedbackId == updatedFeedback.CaseFeedbackId && ca.IsDeleted == false).FirstOrDefaultAsync();
                    //CaseFeedback caseFeedback = null;

                    if (oldFeedback == null)
                        return false;
                    if (updatedFeedback.CaseStatusTypeId != 0)
                    {
                        //caseApplication.CaseStatusTypeId = updatedFeedback.CaseStatusTypeId;
                        caseApplication.CreatedUserId = updatedFeedback.CreatedUserId;
                        caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId;
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.FeedbackTypeId = oldFeedback.FeedbackTypeId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.EmployeeId = updatedFeedback.UpdatedUserId.ToString();
                        updatedFeedback.UpdatedUserId = new Guid(updatedFeedback.EmployeeId);
                    }
                    else
                    {
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        updatedFeedback.DateOfCaseInvestigation = updatedFeedback.DateOfCaseInvestigation;
                        updatedFeedback.DateOfProsecution = updatedFeedback.DateOfProsecution;
                        updatedFeedback.UpdatedUserId = new Guid(updatedFeedback.EmployeeId);
                        caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId; //CreatedUserId
                    }

                    DateTime dateTime = DateTime.Now;
                    updatedFeedback.CreatedDateTime = oldFeedback.CreatedDateTime;
                    updatedFeedback.UpdatedDateTime = dateTime;
                    updatedFeedback.IsActive = true;
                    updatedFeedback.IsDeleted = false;
                    //updatedFeedback.CaseFeedbackId = oldFeedback.CaseFeedbackId;
                    //var modifiedFeedBack = _mapper.Map<CaseFeedback>(updatedFeedback);

                    _context.Entry(caseApplication).State = EntityState.Modified;
                    //_context.Entry(updatedFeedback).State=EntityState.Modified;

                    oldFeedback = _mapper.Map(updatedFeedback, oldFeedback);

                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
                transaction.Commit();
                return true;
            }
        }
        public async Task<bool> AssignProsececution(CaseFeedbackDTO updatedFeedback, Guid userId)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var caseApplication = await _context.CaseApplication.Where(ca => (ca.CaseApplicationId == updatedFeedback.CAUnitId || ca.CaseInvestigation.CaseInvestigationId == updatedFeedback.CAUnitId) && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
                    if (caseApplication == null)
                        return false;

                    var oldFeedback = await _context.CaseFeedback.Where(ca => ca.CaseFeedbackId == updatedFeedback.CaseFeedbackId && ca.IsDeleted == false).FirstOrDefaultAsync();
                    //CaseFeedback caseFeedback = null;

                    if (oldFeedback == null)
                        return false;
                    if (updatedFeedback.CaseStatusTypeId != 0)
                    {
                        //caseApplication.CaseStatusTypeId = updatedFeedback.CaseStatusTypeId;
                        caseApplication.CreatedUserId = updatedFeedback.CreatedUserId;
                        //caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId;
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.FeedbackTypeId = oldFeedback.FeedbackTypeId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.EmployeeId = updatedFeedback.UpdatedUserId.ToString();
                        //updatedFeedback.UpdatedUserId = new Guid(updatedFeedback.EmployeeId);
                    }
                    else
                    {
                        updatedFeedback.CAUnitId = oldFeedback.CAUnitId;
                        updatedFeedback.CreatedUserId = userId;
                        updatedFeedback.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        updatedFeedback.DateOfCaseInvestigation = updatedFeedback.DateOfCaseInvestigation;
                        updatedFeedback.DateOfProsecution = updatedFeedback.DateOfProsecution;
                        updatedFeedback.EmployeeId = updatedFeedback.EmployeeId;
                        //caseApplication.UpdatedUserId = updatedFeedback.UpdatedUserId; //CreatedUserId
                    }

                    DateTime dateTime = DateTime.Now;
                    updatedFeedback.CreatedDateTime = oldFeedback.CreatedDateTime;
                    updatedFeedback.UpdatedDateTime = dateTime;
                    updatedFeedback.IsActive = true;
                    updatedFeedback.IsDeleted = false;
                    //updatedFeedback.CaseFeedbackId = oldFeedback.CaseFeedbackId;
                    //var modifiedFeedBack = _mapper.Map<CaseFeedback>(updatedFeedback);

                    _context.Entry(caseApplication).State = EntityState.Modified;
                    //_context.Entry(updatedFeedback).State=EntityState.Modified;

                    oldFeedback = _mapper.Map(updatedFeedback, oldFeedback);

                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
                transaction.Commit();
                return true;
            }
        }
        public async Task<CaseFeedbackDTO> GetCaseFeedbackById(int caseFeedbackId)
        {
            CaseFeedbackDTO result = null;
            
            var caseFeedback = await _context.CaseFeedback.Where(u => u.CaseFeedbackId == caseFeedbackId && u.IsDeleted == false)
                .Include(u => u.Employee)
                .Include(u => u.FeedbackType)
                .AsNoTracking().FirstOrDefaultAsync();
            if (caseFeedback != null)
            {
                result = _mapper.Map<CaseFeedbackDTO>(caseFeedback);
                if (caseFeedback.CAUnitId != null)
                {
                    var caseApplication = await _context.CaseApplication
                        .Where(ca => (ca.CaseApplicationId == caseFeedback.CAUnitId || ca.CaseInvestigation.CaseInvestigationId == caseFeedback.CAUnitId) &&
                        ca.IsDeleted == false)
                        .Include(u => u.CaseStatusType).ThenInclude(u => u.RequiredFeedbackType).AsNoTracking().FirstOrDefaultAsync();
                    result.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                }
            }
            return result;
        }
        public async Task<CaseFeedbackListDTO> GetAllPreliminaryFeedbacks(Guid caUnitId, string currentLoggedId)
        {
            try
            {
                var result = new CaseFeedbackListDTO();
                var caseApplication = await _context.CaseApplication
                    .Where(u => u.CaseApplicationId == caUnitId)
                .Include(u => u.CaseStatusType).ThenInclude(u => u.RequiredFeedbackType)
                .AsNoTracking().FirstOrDefaultAsync();

                if (caseApplication == null)
                    return result;
                var savedFeedbacks = await _context.CaseFeedback.Where(f => f.CAUnitId == caUnitId)
                    .Include(u => u.Employee)
                    .Include(u => u.FeedbackType).ToListAsync();
                if (savedFeedbacks.Count > 0)
                {
                    result.Feedbacks = _mapper.Map<List<CaseFeedbackDTO>>(savedFeedbacks);
                    var FirstFeedbacksData = result.Feedbacks.First();
                     
                    foreach (var feedBack in result.Feedbacks)
                    {
                        var user = _context.Users.FirstOrDefault(x => x.Id == FirstFeedbacksData.CreatedUserId.ToString());
                         
                        feedBack.FullUserName = user?.FullName;
                        feedBack.FullEmployeeName = FirstFeedbacksData.Employee.FullName;
                        if (feedBack.UpdatedUserId.ToString() == currentLoggedId)
                        {
                            feedBack.CurrentLoggedId = true;
                        }
                        feedBack.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        feedBack.CaseFeedbackId = feedBack.CaseFeedbackId;
                    } 
                    //result.Feedbacks = _mapper.Map<List<CaseFeedbackDTO>>(result.Feedbacks);
                }

                if (caseApplication.CaseStatusType.RequiredFeedbackTypeId != null)
                {
                    var currentFeedback = new CaseFeedbackDTO
                    {
                        CAUnitId = caUnitId,
                        FeedbackTypeId = caseApplication.CaseStatusType.RequiredFeedbackTypeId.Value,
                        FeedbackType = new LookUpDisplayView
                        {
                            DescriptionAmh = caseApplication.CaseStatusType.RequiredFeedbackType.DescriptionAmh,
                            DescriptionEng = caseApplication.CaseStatusType.RequiredFeedbackType.DescriptionEng
                        },
                        CaseStatusTypeId = caseApplication.CaseStatusTypeId
                    };
                    var currentExits = result.Feedbacks.FirstOrDefault(f => f.FeedbackTypeId == currentFeedback.FeedbackTypeId && f.CAUnitId == caUnitId);
                    if (currentExits == null)
                        result.Feedbacks.Add(currentFeedback);
                    
                    result.CurrentFeedback = currentFeedback;

                }
                return result;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
          
        }

        public async Task<CaseFeedbackListDTO> GetAllInvestigationFeedbacks(Guid caseInvestigationId, string currentLoggedId)
        {
            try
            {
                var result = new CaseFeedbackListDTO();

                var caseApplication = await _context.CaseApplication
                    .Where(u => u.CaseInvestigation.CaseInvestigationId == caseInvestigationId)
                .Include(u => u.CaseStatusType).ThenInclude(u => u.RequiredFeedbackType)
                .AsNoTracking().FirstOrDefaultAsync();
                if (caseApplication == null)
                    return result;
                var savedFeedbacks = await _context.CaseFeedback.Where(f => f.CAUnitId == caseInvestigationId)
                    .Include(u => u.Employee)
                    .Include(u => u.FeedbackType).ToListAsync();
                if (savedFeedbacks.Count > 0)
                {
                    result.Feedbacks = _mapper.Map<List<CaseFeedbackDTO>>(savedFeedbacks);
                    var NumberOfFeedback = result.Feedbacks.Count();
                    if (NumberOfFeedback >= 3)
                    {
                        var FirstFeedbacksData = result.Feedbacks[2];
                        foreach (var feedBack in result.Feedbacks)
                        {
                            var user = _context.Users.FirstOrDefault(x => x.Id == FirstFeedbacksData.CreatedUserId.ToString());
                            feedBack.FullUserName = user?.FullName;
                            feedBack.FullEmployeeName = FirstFeedbacksData.Employee.FullName;
                            if (feedBack.UpdatedUserId.ToString() == currentLoggedId)
                            {
                                feedBack.CurrentLoggedId = true;
                            }
                            feedBack.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        }
                    }
                    else
                    {
                        foreach (var feedBack in result.Feedbacks)
                        {
                            if (feedBack.UpdatedUserId.ToString() == currentLoggedId)
                            {
                                feedBack.CurrentLoggedId = true;
                            }
                            feedBack.CaseStatusTypeId = caseApplication.CaseStatusTypeId;
                        }
                    }
                }
                if (caseApplication.CaseStatusType.RequiredFeedbackTypeId != null)
                {
                    var currentFeedback = new CaseFeedbackDTO
                    {
                        CAUnitId = caseInvestigationId,
                        FeedbackTypeId = caseApplication.CaseStatusType.RequiredFeedbackTypeId.Value,
                        FeedbackType = new LookUpDisplayView
                        {
                            DescriptionAmh = caseApplication.CaseStatusType.RequiredFeedbackType.DescriptionAmh,
                            DescriptionEng = caseApplication.CaseStatusType.RequiredFeedbackType.DescriptionEng
                        }
                    };
                    var currentExits = result.Feedbacks.FirstOrDefault(f => f.FeedbackTypeId == currentFeedback.FeedbackTypeId && f.CAUnitId == caseInvestigationId);
                    if (currentExits == null)
                        result.Feedbacks.Add(currentFeedback);

                    result.CurrentFeedback = currentFeedback;

                }
                return result;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public async Task<CaseFeedbackDTO> GetCaseexpertfeedbackSummurys(Guid caseInvestigationId)
        {
            try
            {
                //  LetterContent content = new LetterContent();
                //  content = await _context.LetterContent.FromSql(@"select BodyHeadser, Category from LetterContent where Category={0} ", "CaseExpertLetter").FirstOrDefaultAsync();
                //  var investigationFileNo = await _context.CaseInvestigation.FirstOrDefaultAsync(x => x.CaseInvestigationId == caseInvestigationId);
                var savedFeedbacks = await _context.CaseFeedback.Where(f => f.CAUnitId == caseInvestigationId && f.FeedbackTypeId == 32).OrderByDescending(x => x.CreatedDateTime).FirstOrDefaultAsync(); ;
                //var header=  content.BodyHeader.Replace(@"ም 5", savedFeedbacks?.Summary);
                //  var date= header.Replace(@"ቀን:-", "ቀን:-" + EthiopicDateTime.GetEthiopicDate(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
                //  var contents= date.Replace(@"የመዝገብ ቁጥር:-", "የመዝገብ ቁጥር:-" + investigationFileNo?.FileNo);

                CaseFeedbackDTO caseFeedbackDTO = new CaseFeedbackDTO();
                caseFeedbackDTO.Summary = savedFeedbacks?.Summary;
                return caseFeedbackDTO;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }


    }
}
