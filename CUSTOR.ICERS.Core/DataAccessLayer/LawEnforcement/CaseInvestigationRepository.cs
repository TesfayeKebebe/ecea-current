using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseInvestigationRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CaseInvestigationRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CaseInvestigationDTO> Create(CaseInvestigationDTO postedInvestigation)
        {
            var caseApplication = await _context.CaseApplication.Where(ca => ca.CaseApplicationId == postedInvestigation.CaseApplicationId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            if (caseApplication == null)
                return null;
            DateTime dateTime = DateTime.Now;
            var investigation = _mapper.Map<CaseInvestigation>(postedInvestigation);
            investigation.CreatedDateTime = dateTime;
            investigation.UpdatedDateTime = dateTime;

            CAUnit cAUnit = new CAUnit
            {
                CAUnitId = Guid.NewGuid(),
                CreatedDateTime = dateTime,
                UpdatedDateTime = dateTime
            };

            investigation.CaseInvestigationId = cAUnit.CAUnitId;


            caseApplication.CaseStatusTypeId = 13;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(caseApplication).State = EntityState.Modified;
                    _context.CAUnit.Add(cAUnit);
                    _context.CaseInvestigation.Add(investigation);


                    _context.SaveChanges();
                    transaction.Commit();
                    postedInvestigation.CaseInvestigationId = investigation.CaseInvestigationId;
                    return postedInvestigation;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<bool> Update(CaseInvestigationDTO updatedInvestigation)
        {
            var oldInvestigation = await _context.CaseInvestigation.Where(ca => ca.CaseInvestigationId == updatedInvestigation.CaseInvestigationId && ca.CaseApplicationId == updatedInvestigation.CaseApplicationId && ca.IsDeleted == false).FirstOrDefaultAsync();
            if (oldInvestigation == null)
                return false;

            DateTime dateTime = DateTime.Now;
            var modifiedInvestigation = _mapper.Map<CaseInvestigation>(updatedInvestigation);
            modifiedInvestigation.CreatedDateTime = dateTime;
            modifiedInvestigation.UpdatedDateTime = dateTime;
            var caseApplication = await _context.CaseApplication.Where(ca => ca.CaseApplicationId == updatedInvestigation.CaseApplicationId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            caseApplication.CaseStatusTypeId = 13;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    oldInvestigation = _mapper.Map(modifiedInvestigation, oldInvestigation);
                    //_context.Entry(modifiedInvestigation).State = EntityState.Modified;
                    _context.Entry(caseApplication).State = EntityState.Modified;
                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<CaseInvestigationDTO> GetCaseInvestigationById(Guid caseInvestigationId)
        {
            CaseInvestigationDTO result = null;
            var caseInvestigation = await _context.CaseInvestigation.Where(u => u.CaseInvestigationId == caseInvestigationId && u.IsDeleted == false)
                .AsNoTracking().FirstOrDefaultAsync();
            if (caseInvestigation != null)
            {
                result = _mapper.Map<CaseInvestigationDTO>(caseInvestigation);
                result.CaseApplicationId = caseInvestigation.CaseApplicationId;
                result.OpeningDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(result.OpeningDate).ToString());
            }
            return result;
        }
        public async Task<PagedResult<CaseInvestigationListModel>> SearchInvestigation(CaseInvestigationSearchCriteriaModel queryParameters)
        {
            try
            {              
                var dataResult = await (from inv in _context.CaseInvestigation
                                        join cg in _context.CaseApplication
                                         on inv.CaseApplicationId equals cg.CaseApplicationId into k
                                        from ca in k.DefaultIfEmpty()

                                        from pt in _context.CaseParty
                                        .Where(d => d.CasePartyId == ca.SuspectId).DefaultIfEmpty()
                                         from lex in _context.ExchangeActor
                                        .Where(ex => ex.ExchangeActorId == pt.ExchangeActorId)
                                        .DefaultIfEmpty()
                                        join cst in _context.CaseStatusType
                                       on ca.CaseStatusTypeId equals cst.CaseStatusTypeId 
                                        join ct in _context.Lookup
                                          on ca.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on ca.DepartmentId equals dt.LookupId
                                        where (
                                          (queryParameters.CaseType == null || (ca.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.Department == null || (ca.DepartmentId == queryParameters.Department)) &&
                                          (queryParameters.StatusType == null || (ca.CaseStatusTypeId == queryParameters.StatusType)) &&
                                          (queryParameters.FileNo == null || (inv.FileNo.Contains(queryParameters.FileNo))) &&
                                           (queryParameters.From == null || inv.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                            (queryParameters.To == null || inv.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          inv.IsDeleted == false && cst.StateCategoryId == 18
                                          )
                                        orderby (ca.CreatedDateTime) descending

                                        select new CaseInvestigationListModel
                                        {
                                            ApplicationDate = ca.ApplicationDate,
                                            EventDate = ca.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.EventDate).ToString()),
                                            CaseApplicationId = ca.CaseApplicationId,
                                            SuspectId = ca.SuspectId,
                                            PlaintiffId = ca.PlaintiffId,
                                            CaseStatusCode = ca.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            CreatedDate = inv.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(inv.CreatedDateTime).ToString()),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = ca.Description,
                                            ApplicationPartyType = pt.PartyType,
                                            PlaintiffFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            PlaintiffFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectFullNameAmh = pt.FirstNameAmh + " " + pt.FatherNameAmh + " " + pt.GrandFatherNameAmh,
                                            SuspectFullName = pt.FirstNameEng + " " + pt.FatherNameEng + " " + pt.GrandFatherNameEng,
                                            SuspectOrganizationName = pt.OrganizationNameEng,
                                            SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                            ExchangeActorId = pt.ExchangeActorId,
                                            ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng, 
                                            UpdatedDate = inv.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(inv.UpdatedDateTime).ToString()),
                                            OpeningDate = inv.OpeningDate,
                                            OpeningDateET = EthiopicDateTime.GetEthiopicDate(inv.OpeningDate).ToString(),
                                            CaseInvestigationId = inv.CaseInvestigationId,
                                            FileNo = inv.FileNo,
                                            CaseStatusTypeId = ca.CaseStatusTypeId,
                                            CaseTypeId = ca.CaseTypeId
                                        })
                                    .AsNoTracking()
                                    .ToListAsync();
                foreach (var data in dataResult)
                {
                    if (data.SuspectId != null)
                    {
                        var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectId);
                        if (sus != null)
                        {
                            if (sus?.ExchangeActorId != null)
                            {
                                var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
                                //CustomerTypeId == 63 || sueA.CustomerTypeId == 120
                                if (sueA.DelegatorId != null)
                                {
                                    var sueD = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sueA.DelegatorId).FirstOrDefaultAsync();
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sueA.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueD?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueD?.OrganizationNameAmh;
                                }
                                else
                                {
                                    var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                                    data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                                    data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                                    data.SuspectOrganizationName = sueA?.OrganizationNameEng;
                                    data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
                                }
                            }
                            else
                            {
                                data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
                                data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
                                data.SuspectOrganizationName = sus?.OrganizationNameEng;
                                data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
                            }

                        }

                    }
                    else if (data.PlaintiffId != null)
                    {
                        var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintiffId);

                        if (plaintIff.ExchangeActorId != null)
                        {
                            var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
                            var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                            data.PlaintiffFullNameAmh = (sueA?.OrganizationNameAmh != null) ? sueA?.OrganizationNameAmh : seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                        }
                        else
                        {
                            data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh != null) ? plaintIff?.OrganizationNameAmh : plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
                            data.PlaintiffFullName = (plaintIff?.OrganizationNameEng != null) ? plaintIff?.OrganizationNameEng : plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
                        }
                    }
                }
                var pendingApplications = await GetPendingApplications();
                dataResult.AddRange(pendingApplications);

                dataResult.AsQueryable().Paging(queryParameters.PageSize, queryParameters.PageIndex);

                return new PagedResult<CaseInvestigationListModel>()
                {
                    Items = _mapper.Map<List<CaseInvestigationListModel>>(dataResult),
                    ItemsCount = dataResult.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<CaseInvestigationListModel>> GetPendingApplications()
        {
            var pendingApplications = await (from ca in _context.CaseApplication
                                             join cst in _context.CaseStatusType
                                            on ca.CaseStatusTypeId equals cst.CaseStatusTypeId
                                             join ct in _context.Lookup
                                               on ca.CaseTypeId equals ct.LookupId
                                             join dt in _context.Lookup
                                             on ca.DepartmentId equals dt.LookupId
                                             from pt in _context.CaseParty
                                             .Where(d => d.CasePartyId == ca.SuspectId).DefaultIfEmpty()
                                              
                                             from lex in _context.ExchangeActor
                                             .Where(ex => ex.ExchangeActorId == pt.ExchangeActorId)
                                             .DefaultIfEmpty() 
                                                 //join pt in _context.CaseParty
                                                 //on ca.PlaintiffId equals pt.CasePartyId
                                             where (ca.CaseStatusTypeId == 12)
                                             orderby (ca.CreatedDateTime) descending
                                             select new CaseInvestigationListModel
                                             {
                                                 CaseStatusTypeId = ca.CaseStatusTypeId,
                                                 CaseTypeId = ca.CaseTypeId,
                                                 FileNo = "",
                                                 ApplicationDate = ca.ApplicationDate,
                                                 EventDate = ca.EventDate,
                                                 ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.ApplicationDate).ToString()),
                                                 EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.EventDate).ToString()),
                                                 CaseApplicationId = ca.CaseApplicationId,
                                                 CaseStatusCode = ca.CaseStatusType.TypeCode,
                                                 CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                                 CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                                 CreatedDate = ca.CreatedDateTime,
                                                 CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.CreatedDateTime).ToString()),
                                                 Department = _mapper.Map<LookUpDisplayView>(dt),
                                                 Description = ca.Description,
                                                 ApplicationPartyType = pt.PartyType,
                                                 SuspectOrganizationName = pt.OrganizationNameEng,
                                                 SuspectOrganizationNameAmh = pt.OrganizationNameAmh,
                                                 ExchangeActorId = pt.ExchangeActorId,
                                                 ExchangeActorOrganizationName = pt.ExchangeActor.OrganizationNameAmh,
                                                 ExchangeActorOrganizationNameEng = pt.ExchangeActor.OrganizationNameEng, 
                                                 UpdatedDate = ca.UpdatedDateTime,
                                                 UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.UpdatedDateTime).ToString()),
                                             }).AsNoTracking().ToListAsync();
            return pendingApplications;
        }
    }
}
