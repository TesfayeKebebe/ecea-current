﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseLookUpRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public CaseLookUpRepository(ECEADbContext context, IMapper mapper, IDistributedCache distributedCache)
        {
            _context = context;
            _mapper = mapper;
            _distributedCache = distributedCache;

        }

        public async Task<List<CaseLookUpDTO>> GetAllByCategory(string lang, string groupCode)
        {
            try
            {
                List<CaseLookUpDTO> lookups = null;

                lookups = await _context.CaseLookUp
                    .Where(lookUp => lookUp.Category.TypeCode == groupCode)
                    .Select(lk => new CaseLookUpDTO
                    {
                        Id = lk.CaseLookUpId,
                        Description = (lang == "et") ? lk.DescriptionAmh : lk.DescriptionEng,
                        Code = lk.TypeCode,
                        GroupCode = lk.Category.TypeCode
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return lookups;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<CaseLookUp> GetSingleById(int typeId)
        {
            try
            {
                var lookup = await _context.CaseLookUp.FirstOrDefaultAsync(c => c.CaseLookUpId == typeId);
                return lookup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CaseLookUp> GetSingleByCode(string typeCode)
        {
            try
            {
                var lookup = await _context.CaseLookUp.FirstOrDefaultAsync(c => c.TypeCode == typeCode);
                return lookup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<CaseLookupDTO5>> GetLookups(string lang, int lookupTypeId)
        {

            IEnumerable<CaseLookupDTO5> lookups = null;
            string cacheKey = "Lookups:" + lookupTypeId + lang;

            try
            {
                var cachedLookups = await _distributedCache.GetStringAsync(cacheKey);
                if (cachedLookups != null)
                {
                    lookups = JsonConvert.DeserializeObject<IEnumerable<CaseLookupDTO5>>(cachedLookups);
                }
                else
                {
                    lookups = await _context.CaseLookUp
                        .Where(l => l.CategoryId == lookupTypeId && l.IsActive == true && l.IsDeleted == false)
                        .Select(l => new CaseLookupDTO5
                        {
                            CaseLookUpId = l.CaseLookUpId,
                            Description = (lang == "et") ? l.DescriptionAmh : l.DescriptionEng
                        }).AsNoTracking().ToListAsync();


                }

                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<bool> SaveLookup(CaseLookupDTO4 updatedSetting)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    string updated = updatedSetting.DescriptionEng.ToUpper().Replace(' ', '_').Replace('-', '_');
                    updatedSetting.TypeCode = updated;
                    var oldSetting = await _context.CaseLookUp.FirstOrDefaultAsync(clb => clb.CategoryId == updatedSetting.CategoryId && clb.CaseLookUpId == updatedSetting.CaseLookUpId);

                    if (oldSetting == null)
                    {
                        var newCommodity = _mapper.Map<CaseLookUp>(updatedSetting);
                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.IsDeleted = oldSetting.IsDeleted;
                        updatedSetting.IsActive = oldSetting.IsActive;
                        updatedSetting.CreatedUserId = oldSetting.CreatedUserId;
                        updatedSetting.UpdatedDateTime = DateTime.Now;
                        updatedSetting.CreatedDateTime = oldSetting.CreatedDateTime;
                        var updateReportPeriod = _mapper.Map(updatedSetting, oldSetting);
                        _context.Entry(updateReportPeriod).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }
        public async Task<bool> SaveLookupType(CaseLookupTypeDTO updatedSetting)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    string updated = updatedSetting.DescriptionEng.ToUpper().Replace(' ', '_').Replace('-', '_');
                    updatedSetting.TypeCode = updated;
                    var oldLookup = await _context.CaseLookUpType.FirstOrDefaultAsync(clb => clb.CaseLookUpTypeId == updatedSetting.CaseLookUpTypeId);

                    if (oldLookup == null)
                    {
                        var newCommodity = _mapper.Map<CaseLookUpType>(updatedSetting);
                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.IsDeleted = oldLookup.IsDeleted;
                        updatedSetting.IsActive = oldLookup.IsActive;
                        updatedSetting.CreatedUserId = oldLookup.CreatedUserId;
                        updatedSetting.UpdatedDateTime = DateTime.Now;
                        updatedSetting.CreatedDateTime = oldLookup.CreatedDateTime;
                        var updateReportPeriod = _mapper.Map(updatedSetting, oldLookup);
                        _context.Entry(updateReportPeriod).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        public async Task<List<CaseLookupDTO4>> GetLookupsById(int lookupTypeId)
        {


            try
            {

                var lookups = await _context.CaseLookUp
                        .Where(l => l.CategoryId == lookupTypeId && l.IsDeleted == false)
                        .Select(l => new CaseLookupDTO4
                        {
                            CaseLookUpId = l.CaseLookUpId,
                            DescriptionEng = l.DescriptionEng,
                            DescriptionAmh = l.DescriptionAmh
                        }).AsNoTracking().ToListAsync();

                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        // get lookup data using guid
        //public async Task<IEnumerable<LookupDTO>> GetLookupData(string lang, Guid lookupTypeId)
        //{

        //    IEnumerable<LookupDTO> lookups = null;
        //    string cacheKey = "Lookups:" + lookupTypeId + lang;

        //    try
        //    {
        //        var cachedLookups = await _distributedCache.GetStringAsync(cacheKey);
        //        if (cachedLookups != null)
        //        {
        //            lookups = JsonConvert.DeserializeObject<IEnumerable<LookupDTO>>(cachedLookups);
        //        }
        //        else
        //        {
        //            lookups = await _context.Lookup
        //                .Where(l => l.LookupTypeId == lookupTypeId && l.IsActive == true && l.IsDeleted == false)
        //                .Select(l => new LookupDTO
        //                {
        //                    Id = l.LookupId,
        //                    Description = (lang == "et") ? l.DescriptionAmh : l.DescriptionEng
        //                }).AsNoTracking().ToListAsync();

        //            DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
        //                .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));
        //            await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(lookups),
        //                cacheOptions);
        //        }

        //        return lookups;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //}

        public async Task<List<CaseLookupTypeDTO>> GetAllLookupTypeData()
        {


            try
            {

                var lookups = await _context.CaseLookUpType.Where(l => l.IsDeleted == false)
                  .Select(n => new CaseLookupTypeDTO
                  {
                      CaseLookUpTypeId = n.CaseLookUpTypeId,
                      DescriptionAmh = n.DescriptionAmh,
                      DescriptionEng = n.DescriptionEng,
                  })
                  .AsNoTracking()
                  .ToListAsync();




                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> DeleteLookUpValue(int lookupid)
        {
            try
            {
                var oldInjunction = await _context.CaseLookUp.FirstOrDefaultAsync(lo =>
                    lo.CaseLookUpId == lookupid);
                if (oldInjunction != null)
                {
                    oldInjunction.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteLookUpType(int lookuptypeid)
        {
            try
            {
                var oldInjunction = await _context.CaseLookUpType.FirstOrDefaultAsync(lo =>
                    lo.CaseLookUpTypeId == lookuptypeid);
                if (oldInjunction != null)
                {
                    oldInjunction.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<CaseLookUpType> GetLookUpTypeByCode(string typeCode)
        {
            try
            {
                var lookup = await _context.CaseLookUpType.FirstOrDefaultAsync(c => c.TypeCode == typeCode);
                return lookup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
