using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
  public class CasePartyDocumentRepository
  {
    private readonly ECEADbContext _context;

    private readonly IMapper _mapper;
    public CasePartyDocumentRepository(ECEADbContext context, IMapper mapper)
    {
      _context = context;
      _mapper = mapper;
    }
    public async Task<bool> Create(CasePartyDocumentDto postedAttachment)
    {
      var pathToSave = getUploadPath();

      string extension = getFileExtension(postedAttachment);
      postedAttachment.Id = Guid.NewGuid().ToString();
      string fileName = postedAttachment.Id + extension;

      string filePath = Path.Combine(pathToSave, fileName);

      postedAttachment.FileName = fileName;

      try
      {


        DateTime dateTime = DateTime.Now;
        CasePartyDocument caseDocument = _mapper.Map<CasePartyDocument>(postedAttachment);
        caseDocument.CreatedDateTime = dateTime;
        caseDocument.UpdatedDateTime = dateTime;


        _context.CasePartyDocument.Add(caseDocument);
        _context.SaveChanges();


        // save to disk
        if (!Directory.Exists(pathToSave))
          Directory.CreateDirectory(pathToSave);

        using (var stream = new FileStream(filePath, FileMode.Create))
        {
          await postedAttachment.ActualFile.CopyToAsync(stream);
        }


        return true;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }

    }
    public async Task<bool> Update(CasePartyDocumentDto updatedAttachment)
    {
      var oldDocumentUnit = await _context.CasePartyDocument.Where(ca => ca.Id == updatedAttachment.Id).FirstOrDefaultAsync();

      if (oldDocumentUnit == null)
        return false;

      DateTime dateTime = DateTime.Now;

      var pathToSave = getUploadPath();

      string extension = getFileExtension(updatedAttachment);

      // for updating
      string fileName = updatedAttachment.FileName + extension;

      // for deletion
      string todeleteFileName = oldDocumentUnit.FileName;

      string filePath = Path.Combine(pathToSave, fileName);

      string deletionPath = Path.Combine(pathToSave, todeleteFileName);

      try
      {
        // delete existing file with the given name
        if (File.Exists(deletionPath))
        {
          File.Delete(deletionPath);
        }


        oldDocumentUnit.UpdatedDateTime = dateTime;
        oldDocumentUnit.AttachmentContent = updatedAttachment.AttachmentContent;
        oldDocumentUnit.FileName = fileName;
        oldDocumentUnit.UpdatedUserId = updatedAttachment.UpdatedUserId;


        await _context.SaveChangesAsync();
        return true;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }



    public async Task<List<CasePartyDocumentDto>> GetByCaseApplicationId(Guid CaseApplicationId, string lang)
    {
      List<CasePartyDocumentDto> addedCasePartyDocument = new List<CasePartyDocumentDto>();
      var caseParties = await _context.CaseParty.Where(x => x.CAUnitId == CaseApplicationId).ToListAsync();
      if (caseParties.Any())
      {
        foreach (var casePa in caseParties)
        {
          var casePartyDoc = _context.CasePartyDocument.Where(x => x.CasePartyId == casePa.CasePartyId).ToList();
          if (casePartyDoc.Any())
          {
            foreach (var cpd in casePartyDoc)
            {
              CasePartyDocumentDto casePartyDocumentDto = new CasePartyDocumentDto();
              if (casePa.ExchangeActorId != null)
              {
                var exca = _context.ExchangeActor.FirstOrDefault(x => x.ExchangeActorId == casePa.ExchangeActorId);
                if (exca.DelegatorId != null)
                {
                  var ownerInformation = _context.OwnerManager.FirstOrDefault(x => x.ExchangeActorId == exca.ExchangeActorId);

                  casePartyDocumentDto.FullName = (lang == "et") ? $"{ownerInformation?.FirstNameAmh} {ownerInformation?.FatherNameAmh} {ownerInformation?.GrandFatherNameAmh}" : $"{ownerInformation?.FirstNameEng} {ownerInformation?.FatherNameEng} {ownerInformation?.GrandFatherNameEng}";
                }
                else
                {
                  if (!string.IsNullOrWhiteSpace(exca.OrganizationNameAmh) || !string.IsNullOrWhiteSpace(exca.OrganizationAreaNameEng))
                  {
                    casePartyDocumentDto.FullName = (lang == "et") ? $"{exca.OrganizationNameAmh}" : $"{ exca.OrganizationAreaNameEng}";

                  }
                  else
                  {
                    casePartyDocumentDto.FullName = (lang == "et") ? exca?.OrganizationNameAmh : exca.OrganizationNameEng;

                  }
                }
              }
              else
              {
                if (!string.IsNullOrWhiteSpace(casePa.OrganizationNameAmh) || !string.IsNullOrWhiteSpace(casePa.OrganizationNameEng))
                {
                  casePartyDocumentDto.FullName = (lang == "et") ? $"{casePa.OrganizationNameAmh}" : $"{ casePa.OrganizationNameEng}";

                }
                else
                {
                  casePartyDocumentDto.FullName = (lang == "et") ? $"{casePa?.FirstNameAmh} {casePa?.FatherNameAmh} {casePa?.GrandFatherNameAmh}" : $"{casePa?.FirstNameEng} {casePa?.FatherNameEng} {casePa?.GrandFatherNameEng}";

                }
              }
              casePartyDocumentDto.FileName = cpd.FileName;
              casePartyDocumentDto.AttachmentContent = cpd.AttachmentContent;
              casePartyDocumentDto.CasePartyId = cpd.CasePartyId;
              addedCasePartyDocument.Add(casePartyDocumentDto);

            }
          }
        }
      }
      return addedCasePartyDocument;
    }
    private string getUploadPath()
    {
      return Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
    }
    private string getFileExtension(CasePartyDocumentDto documentInformation)
    {
      return documentInformation.ActualFile.ContentType == "application/pdf" ? ".pdf" : documentInformation.ActualFile.ContentType == "image/jpeg" ? ".jpeg" : ".png";
    }
  }
}
