using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CasePartyInvestigationRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CasePartyInvestigationRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(CasePartyInvestigationDTO postedInvestigation)
        {
            DateTime dateTime = DateTime.Now;
            var partyInvestigation = _mapper.Map<CasePartyInvestigation>(postedInvestigation);
            partyInvestigation.CreatedDateTime = dateTime;
            partyInvestigation.UpdatedDateTime = dateTime;
            partyInvestigation.InvestigatorId = postedInvestigation?.CreatedUserId.ToString();
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.CasePartyInvestigation.Add(partyInvestigation);

                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<bool> Update(CasePartyInvestigationDTO updatedInvestigation)
        {
            var oldInvestigation = await _context.CasePartyInvestigation.Where(ca => ca.CasePartyInvestigationId == updatedInvestigation.CasePartyInvestigationId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            if (oldInvestigation == null)
                return false;

            updatedInvestigation.CasePartyId = oldInvestigation.CasePartyId;
            DateTime dateTime = DateTime.Now;
            updatedInvestigation.InvestigatorId = 1;
            var modifiedInvestigation = _mapper.Map<CasePartyInvestigation>(updatedInvestigation);
            modifiedInvestigation.CreatedDateTime = oldInvestigation.CreatedDateTime;
            modifiedInvestigation.UpdatedDateTime = dateTime;
            modifiedInvestigation.IsActive = oldInvestigation.IsActive;
            modifiedInvestigation.IsDeleted = oldInvestigation.IsDeleted;
            modifiedInvestigation.CreatedUserId = oldInvestigation.CreatedUserId;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(modifiedInvestigation).State = EntityState.Modified;

                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<CasePartyInvestigationDTO> GetPartyInvestigationById(int casePartyInvestigationId)
        {
            CasePartyInvestigationDTO result = null;
            var casePartyInvestigation = await _context.CasePartyInvestigation.Where(u => u.CasePartyInvestigationId == casePartyInvestigationId && u.IsDeleted == false)
                .Include(u => u.Investigator)
                .AsNoTracking().FirstOrDefaultAsync();
            if (casePartyInvestigation != null)
            {
                result = _mapper.Map<CasePartyInvestigationDTO>(casePartyInvestigation);
            }
            return result;
        }
        public async Task<List<CasePartyInvestigationListModel>> GetAllByPartyCategory(Guid caseInvestigationId, CasePartyCategory partyCategory)
        {
            try
            {
                bool isSuspect = (partyCategory == CasePartyCategory.Collaborator || partyCategory == CasePartyCategory.Suspect);
                var result = new List<CasePartyInvestigationListModel>();
                var caseInvestigation = await _context.CaseInvestigation
                        .Where(u => u.CaseInvestigationId == caseInvestigationId)
                        .Include(u => u.CaseApplication)
                        .AsNoTracking().FirstOrDefaultAsync();

                if (caseInvestigation == null)
                {
                    return result;
                }
                var partyInv = await _context.CaseParty
                        .Where(p => (p.CAUnitId == caseInvestigation.CaseInvestigationId || p.CAUnitId == caseInvestigation.CaseApplicationId)

                        && (isSuspect && new List<int> { 1, 3 }.Contains(p.PartyCategory) || (!isSuspect && p.PartyCategory == (int)partyCategory))
                        && p.IsDeleted == false)
                        .OrderBy(m => m.PartyCategory)
                        .Include(u => u.ExchangeActor).ThenInclude(u => u.OwnerManager)
                        .Include(u => u.Region)
                        .Include(u => u.Zone)
                        .Include(u => u.Woreda)
                        .Include(u => u.Kebele)
                        .Include(u => u.Nationality)
                    .Include(p => p.CasePartyInvestigation).ThenInclude(u => u.Investigator).AsNoTracking().ToListAsync();
                if (partyInv.Count > 0)
                    result = _mapper.Map<List<CasePartyInvestigationListModel>>(partyInv);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<CasePartyInvestigationDTO>> GetAllPartyInvestigations(Guid casePartyId)
        {
            var result = new List<CasePartyInvestigationDTO>();
            var partyInvestigations = await _context.CasePartyInvestigation
                    .Include(u => u.Investigator).AsNoTracking()
                    .Where(u => u.CasePartyId == casePartyId)
                    .AsNoTracking().ToListAsync();

            if (partyInvestigations.Count > 0)
                result = _mapper.Map<List<CasePartyInvestigationDTO>>(partyInvestigations);
            return result;
        }
    }
}
