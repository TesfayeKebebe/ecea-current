using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CasePartyRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CasePartyRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> Create(Guid caUnitId, CasePartyEditDTO postedParty)
        {
            var caUnit = await _context.CAUnit.Where(ca => ca.CAUnitId == caUnitId && ca.IsDeleted == false).FirstOrDefaultAsync();
            if (caUnit == null)
                return false;
            if (postedParty.PartyCategory != CasePartyCategory.Witness)
            { postedParty.SuspectWitness = false; }

            if (postedParty.PartyType == CasePartyType.Sole)
                postedParty.ExchangeActorId = null;
            if (postedParty.PartyType == CasePartyType.Organization)
                postedParty.ExchangeActorId = null;
            if (postedParty.ExchangeActor != null)
            {
                postedParty.ExchangeActor = null;
            }

            var casePartyId = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;

            CaseParty caseParty = _mapper.Map<CaseParty>(postedParty);
            caseParty.CasePartyId = casePartyId;
            caseParty.CAUnitId = caUnitId;
            caseParty.CreatedDateTime = dateTime;
            caseParty.UpdatedDateTime = dateTime;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    _context.CaseParty.Add(caseParty);

                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }
        }
        //Guid caUnitId,
        public async Task<PartyDTO> SavePlaintiffCaseParty(PartyDTO postedParty)
        {
            bool isNew = postedParty.CasePartyId is null; // New or Update?

            DateTime dateTime = DateTime.Now;
            // map DTO to manager entity 
            
            // We should always use transaction for operations involving two or more entities
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    // first save case party entity
                    CaseParty caseParty = CaseApplicationHelper.GetParty(postedParty);
                    var caUnit = await _context.CAUnit.Where(cau => cau.CAUnitId == postedParty.CAUnitId && cau.IsDeleted == false).FirstOrDefaultAsync();
                    if (isNew)
                    {
                        if (postedParty.PartyType == 2)
                            postedParty.ExchangeActorId = null;
                        if (postedParty.PartyType == 3)
                            postedParty.ExchangeActorId = null;

                        if (postedParty.ExchangeActor != null)
                        {
                            postedParty.ExchangeActorId = postedParty.ExchangeActor.ExchangeActorId;
                            postedParty.ExchangeActor = null;
                        }

                        if (postedParty.CaseApplicationId != null)
                        {
                            var casePartyId = Guid.NewGuid();
                            caseParty.CasePartyId = casePartyId;
                          
                            caseParty.CAUnitId = new Guid(postedParty.CaseApplicationId);
                            var capp = await _context.CaseApplication
                               .FirstOrDefaultAsync(cap => cap.CaseApplicationId.ToString() == postedParty.CaseApplicationId);
                            if (postedParty.PartyCategory == 2)
                            {
                                capp.PlaintiffId = caseParty.CasePartyId;
                                _context.Entry(capp).State = EntityState.Modified;
                            }
                        }
                        else
                        {
                            var casePartyId = Guid.NewGuid();
                            caseParty.CasePartyId = casePartyId;
                            caseParty.CAUnitId = postedParty.CAUnitId;
                            var capp = await _context.CaseApplication
                          .FirstOrDefaultAsync(cap => cap.CaseApplicationId == postedParty.CAUnitId);
                            if (capp != null) {
                                if (postedParty.PartyCategory == 2)
                                {
                                    capp.PlaintiffId = casePartyId;
                                    _context.Entry(capp).State = EntityState.Modified;
                                }
                            }
                        }
                        _context.CaseParty.Add(caseParty);
                        _context.SaveChanges();
                    }
                    else
                    {
                        if (postedParty.PartyType == 2)
                            postedParty.ExchangeActorId = null;
                        if (postedParty.PartyType == 3)
                            postedParty.ExchangeActorId = null;

                        if (postedParty.ExchangeActor != null)
                        {
                            postedParty.ExchangeActorId = postedParty.ExchangeActor.ExchangeActorId;
                            postedParty.ExchangeActor = null;
                        }
                        //CaseApplication capp = null;
                         
                        var oldCparty = await _context.CaseParty.Include(cp=>cp.CAUnit)
                         .FirstOrDefaultAsync(cap => cap.CasePartyId.ToString() == postedParty.CasePartyId);
                        var oldCapp = await _context.CaseApplication
                         .FirstOrDefaultAsync(cap => cap.CaseApplicationId == oldCparty.CAUnitId);
                        if (oldCapp != null)
                        {
                            if (postedParty.PartyCategory == 2)
                            { 
                                oldCapp.PlaintiffId = new Guid(postedParty.CasePartyId);
                                _context.Entry(oldCapp).State = EntityState.Modified;
                                //_context.SaveChanges();
                            }
                        }
                        var oldParty = await _context.CaseParty.Include(cp => cp.CAUnit).FirstOrDefaultAsync(cp => cp.CasePartyId == new Guid(postedParty.CasePartyId)); 
                        postedParty.IsDeleted = false;
                        postedParty.IsActive = true;
                        postedParty.CAUnitId = oldParty.CAUnitId;
                        oldParty = _mapper.Map(postedParty, oldParty);
                        _context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }

                transaction.Commit();
                return postedParty;
            }
        }

        public async Task<bool> Update(CasePartyEditDTO updatedParty)
        {
            var oldPartyUnit = await _context.CaseParty.Where(ca => ca.CasePartyId == updatedParty.CasePartyId && ca.IsDeleted == false).FirstOrDefaultAsync();
            if (oldPartyUnit == null)
                return false;
            if (updatedParty.PartyCategory != CasePartyCategory.Witness)
            { updatedParty.SuspectWitness = false; }

            if (updatedParty.PartyType == CasePartyType.Sole)
                updatedParty.ExchangeActorId = null;
            if (updatedParty.ExchangeActor != null)
            {
                updatedParty.ExchangeActor = null;
            }

            var casePartyId = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;
            updatedParty.PartyCategory = (CasePartyCategory)oldPartyUnit.PartyCategory;
            updatedParty.PartyType = (CasePartyType)oldPartyUnit.PartyType;

            CaseParty modifiedCaseParty = _mapper.Map(updatedParty, oldPartyUnit);
            modifiedCaseParty.UpdatedDateTime = dateTime;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(modifiedCaseParty).State = EntityState.Modified;
                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }
        }
        //public async Task<PartyDTO> getCaseParty(string lang, Guid partyId)
        //{ 
        //    CaseParty cprty = null;

        //    cprty = await _context.CaseParty.Include(u => u.ExchangeActor).ThenInclude(o => o.OwnerManager)
        //        .FirstOrDefaultAsync(a => a.CasePartyId == partyId);

        //    return CaseApplicationHelper.GetPartyDTO(cprty);

        //}
        public async Task<CasePartyEditDTO> GetCasePartyById(Guid casePartyId)
        {
            CasePartyEditDTO result = null;
            //&& u.IsDeleted == false
            var caseParty = await _context.CaseParty.Where(u => u.CasePartyId == casePartyId )
                .Include(u => u.ExchangeActor).ThenInclude(u => u.OwnerManager)
                .Include(u => u.Region)
                .Include(u => u.Zone)
                .Include(u => u.Woreda)
                .Include(u => u.Kebele)
                .Include(u => u.Nationality)
                .AsNoTracking().FirstOrDefaultAsync();
            if (caseParty != null)
            {
                result = _mapper.Map<CasePartyEditDTO>(caseParty);
            }
            return result;
        }
        public async Task<PartyDTO> getCaseParty(string lang, Guid caseId)
        {
            CaseParty cprty = null;

            cprty = await _context.CaseParty.Include(cau=>cau.CAUnit).Include(u => u.ExchangeActor).ThenInclude(o => o.OwnerManager)
                .FirstOrDefaultAsync(a => a.CasePartyId == caseId);

            return CaseApplicationHelper.GetPartyDTO(cprty);

        }
        public async Task<IEnumerable<CasePartyDTO>> GetAllByCategory(Guid caUnitId, CasePartyCategory partyCategory, CaseAdministrationUnitType caUnitType)
        {
            try
            {
                var result = new List<CasePartyDTO>();
                List<Guid> parentId = new List<Guid>();
                parentId = await GetParentCAUnit(caUnitId, caUnitType);
                if (parentId == null)
                {
                    return result;
                }
                bool isSuspect = (partyCategory == CasePartyCategory.Collaborator || partyCategory == CasePartyCategory.Suspect);
                var caseParties = await _context.CaseParty
                            .Where(u => (isSuspect && new List<int> { 1, 3 }.Contains(u.PartyCategory) || (!isSuspect && u.PartyCategory == (int)partyCategory)) && parentId.Contains(u.CAUnitId))
                        .OrderByDescending(u => u.CreatedDateTime)
                        .OrderBy(m => m.PartyCategory)
                        .Include(u => u.ExchangeActor).ThenInclude(u => u.OwnerManager)
                        .Include(u => u.Region)
                        .Include(u => u.Zone)
                        .Include(u => u.Woreda)
                        .Include(u => u.Kebele)
                        .Include(u => u.Nationality)
                        .AsNoTracking().ToListAsync();
                result = _mapper.Map<List<CasePartyDTO>>(caseParties);
                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<List<Guid>> GetParentCAUnit(Guid caUnitId, CaseAdministrationUnitType? caUnitType)
        {
            List<Guid> parentId = new List<Guid>();
            switch (caUnitType)
            {
                case CaseAdministrationUnitType.Application:
                    parentId = await _context.CaseApplication.Where(inv => inv.CaseApplicationId == caUnitId)
                        .Select(lc => new List<Guid> { lc.CaseApplicationId })
                        .AsNoTracking().FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.Investigation:
                    parentId = await _context.CaseInvestigation.Where(inv => inv.CaseInvestigationId == caUnitId)
                        .Select(lc => new List<Guid> { lc.CaseApplicationId, lc.CaseInvestigationId })
                        .AsNoTracking().FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.Case:
                    parentId = await _context.Case.Where(inv => inv.CaseId == caUnitId)
                        .Select(lc => new List<Guid> { lc.CaseInvestigation.CaseApplicationId, lc.CaseInvestigationId, lc.CaseId })
                        .AsNoTracking().FirstOrDefaultAsync();
                    break;
                case CaseAdministrationUnitType.CaseTracking:
                    parentId = await _context.CaseDecisionTracking.Where(inv => inv.CaseDecisionTrackingId == caUnitId)
                        .Select(lc => new List<Guid> { lc.CaseDecisionTrackingId, lc.CaseId, lc.Case.CaseInvestigationId, lc.Case.CaseInvestigation.CaseApplicationId })
                        .AsNoTracking().FirstOrDefaultAsync();
                    break;
                default:
                    break;
            }

            return parentId;
        }

        public async Task<IEnumerable<CasePartyDTO>> GetAllAtCourtSession(Guid caUnitId, CasePartyCategory partyCategory)
        {
            try
            {
                bool isSuspect = (partyCategory == CasePartyCategory.Collaborator || partyCategory == CasePartyCategory.Suspect);
        var p = _context.Woreda.ToList();

                var caseParties = await (from t in _context.CasePartyTracking
                                         join u in _context.CaseParty.OrderBy(m => m.PartyCategory)
                                        .Include(u => u.ExchangeActor).ThenInclude(u => u.OwnerManager)
                                        .Include(u => u.Region)
                                        .Include(u => u.Zone)
                                        .Include(u => u.Woreda)
                                        .Include(u => u.Kebele)
                                        .Include(u => u.Nationality)
                                        on t.CasePartyId equals u.CasePartyId
                                         where t.CaseDecisionTrackingId == caUnitId && (isSuspect && new List<int> { 1, 3 }.Contains(u.PartyCategory) || (!isSuspect && u.PartyCategory == (int)partyCategory))
                                         select _mapper.Map<CasePartyDTO>(u)
                                         )
                        .AsNoTracking().ToListAsync();
                return caseParties;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateAtCourtSession(CasePartyTrackingDTO postedParty)
        {
            try
            {
                var caUnit = await _context.CasePartyTracking.Where(ca => ca.CasePartyId == postedParty.CasePartyId && ca.CaseDecisionTrackingId == postedParty.CaseDecisionTrackingId && ca.IsDeleted == false).FirstOrDefaultAsync();
                if (caUnit != null)
                {
                    caUnit.CasePartyId = postedParty.CasePartyId;
                    caUnit.AmhRemark = postedParty.AmhRemark;
                    caUnit.Remark = postedParty.Remark;
                    _context.SaveChanges();
                    return true;
                }


                else
                {
                    CasePartyTracking casePartyTracking = _mapper.Map<CasePartyTracking>(postedParty);

                    var casePartyId = Guid.NewGuid();
                    DateTime dateTime = DateTime.Now;
                    casePartyTracking.CasePartyTrackingId = Guid.NewGuid();
                    casePartyTracking.CreatedDateTime = dateTime;
                    casePartyTracking.UpdatedDateTime = dateTime;
                    using (var transaction = await _context.Database.BeginTransactionAsync())
                    {
                        try
                        {

                            _context.CasePartyTracking.Add(casePartyTracking);
                            _context.SaveChanges();
                            transaction.Commit();

                            return true;

                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new Exception(ex.InnerException.ToString());
                        }

                    }

                }






            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> UpdateAtCourtSession(CasePartyTrackingDTO postedParty)
        {
            var oldCaseTracking = await _context.CasePartyTracking.Where(ca => ca.CasePartyTrackingId == postedParty.CasePartyTrackingId && ca.IsDeleted == false).FirstOrDefaultAsync();
            if (oldCaseTracking == null)
                return false;

            DateTime dateTime = DateTime.Now;
            CasePartyTracking updatedPartyTracking = _mapper.Map(postedParty, oldCaseTracking);
            updatedPartyTracking.CreatedDateTime = oldCaseTracking.CreatedDateTime;
            updatedPartyTracking.IsActive = oldCaseTracking.IsActive;
            updatedPartyTracking.IsDeleted = oldCaseTracking.IsDeleted;
            updatedPartyTracking.CasePartyId = oldCaseTracking.CasePartyId;
            updatedPartyTracking.CaseDecisionTrackingId = oldCaseTracking.CaseDecisionTrackingId;
            updatedPartyTracking.UpdatedDateTime = dateTime;
            updatedPartyTracking.CreatedUserId = oldCaseTracking.CreatedUserId;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    _context.CasePartyTracking.Update(updatedPartyTracking);
                    _context.SaveChanges();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }

        }
        public async Task<CasePartyTrackingDTO> GetPartyAtCourtSession(Guid courtSessionId, Guid casePartyId)
        {
            CasePartyTrackingDTO result = null;
            var caseParty = await _context.CasePartyTracking.Where(u => u.CaseDecisionTrackingId == courtSessionId && u.CasePartyId == casePartyId && u.IsDeleted == false)

                .AsNoTracking().FirstOrDefaultAsync();
            if (caseParty != null)
            {
                result = _mapper.Map<CasePartyTrackingDTO>(caseParty);
            }
            return result;
        }

    }
}
