﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseStatusRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public CaseStatusRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<CaseLookUpDTO>> GetAll(string lang)
        {
            try
            {
                List<CaseLookUpDTO> lookups = null;

                lookups = await _context.CaseStatusType
                    .Select(lk => new CaseLookUpDTO
                    {
                        Id = lk.CaseStatusTypeId,
                        Description = (lang == "et") ? lk.DescriptionAmh : lk.DescriptionEng,
                        Code = lk.TypeCode,
                        GroupCode = lk.Category.TypeCode,
                        GroupDescription = (lang == "et") ? lk.Category.DescriptionAmh : lk.Category.DescriptionEng,
                        SubGroupCode = lk.StateCategory.TypeCode,
                        SubGroupDescription = (lang == "et") ? lk.StateCategory.DescriptionAmh : lk.StateCategory.DescriptionEng,
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return lookups;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<CaseLookUpDTO>> GetAllByMainCategory(string lang, string groupCode)
        {
            try
            {
                List<CaseLookUpDTO> lookups = null;

                lookups = await _context.CaseStatusType
                    .Where(lookUp => lookUp.StateCategory.TypeCode == groupCode)
                    .Select(lk => new CaseLookUpDTO
                    {
                        Id = lk.CaseStatusTypeId,
                        Description = (lang == "et") ? lk.DescriptionAmh : lk.DescriptionEng,
                        Code = lk.TypeCode,
                        GroupCode = lk.Category.TypeCode,
                        GroupDescription = (lang == "et") ? lk.Category.DescriptionAmh : lk.Category.DescriptionEng,
                        SubGroupCode = lk.StateCategory.TypeCode,
                        SubGroupDescription = (lang == "et") ? lk.StateCategory.DescriptionAmh : lk.StateCategory.DescriptionEng,
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return lookups;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<CaseLookUpDTO>> GetAllBySubCategory(string lang, string groupCode)
        {
            try
            {
                List<CaseLookUpDTO> lookups = null;

                lookups = await _context.CaseStatusType
                    .Where(lookUp => lookUp.Category.TypeCode == groupCode)
                    .Select(lk => new CaseLookUpDTO
                    {
                        Id = lk.CaseStatusTypeId,
                        Description = (lang == "et") ? lk.DescriptionAmh : lk.DescriptionEng,
                        Code = lk.TypeCode,
                        GroupCode = lk.Category.TypeCode,
                        GroupDescription = (lang == "et") ? lk.Category.DescriptionAmh : lk.Category.DescriptionEng,
                        SubGroupCode = lk.StateCategory.TypeCode,
                        SubGroupDescription = (lang == "et") ? lk.StateCategory.DescriptionAmh : lk.StateCategory.DescriptionEng,
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return lookups;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<CaseLookUpDTO>> GetAllForCaseApplication(string lang, Guid caseApplicationId)
        {
            try
            {
                var caseApplication = await _context.CaseApplication.Where(x => x.CaseApplicationId == caseApplicationId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                List<CaseLookUpDTO> lookups = null;
                if (caseApplication != null)
                {
                    IEnumerable<CaseStatusType> result = await _context.CaseStatusTransition.Where(x => x.ParentStatusTypeId == caseApplication.CaseStatusTypeId)
                       .Select(s => s.ChildStatusType)
                       .Include(s => s.Category)
                       .Include(s => s.StateCategory)
                   .AsNoTracking()
                   .ToListAsync();
                    lookups = result.Select(
                    lk => new CaseLookUpDTO
                    {
                        Code = lk.TypeCode,
                        Id = lk.CaseStatusTypeId,
                        Description = (lang == "et") ? lk.OptionDescriptionAmh : lk.OptionDescriptionEng,
                        GroupCode = lk.Category.TypeCode,
                        GroupDescription = (lang == "et") ? lk.Category.DescriptionAmh : lk.Category.DescriptionEng,
                        SubGroupCode = lk.StateCategory.TypeCode,
                        SubGroupDescription = (lang == "et") ? lk.StateCategory.DescriptionAmh : lk.StateCategory.DescriptionEng,
                    }
                    ).ToList();
                }
                return lookups;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<CaseStatusType> GetSingleByCode(string typeCode)
        {
            try
            {
                var lookup = await _context.CaseStatusType.FirstOrDefaultAsync(c => c.TypeCode == typeCode);
                return lookup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<CaseStatusType> GetSingleById(int typeId)
        {
            try
            {
                var lookup = await _context.CaseStatusType.FirstOrDefaultAsync(c => c.CaseStatusTypeId == typeId);
                return lookup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
