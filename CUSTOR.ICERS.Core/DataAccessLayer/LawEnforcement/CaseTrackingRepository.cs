using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
//using DevExpress.Compatibility.System.Web;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
  public class CaseTrackingRepository
  {
    private readonly ECEADbContext _context;

    private readonly IMapper _mapper;

    public CaseTrackingRepository(ECEADbContext context, IMapper mapper)
    {
      _context = context;
      _mapper = mapper;

    }

    public async Task<CaseDecisionTrackingDTO> NewCourtSessionDecision(CaseDecisionTrackingDTO postedCaseTracking)
   {
      try
      {
        if (postedCaseTracking.IsClosed && postedCaseTracking.IsAppointment)
          return null;
        if (postedCaseTracking.IsAppointment && postedCaseTracking.Appointment == null)
          return null;
        if (postedCaseTracking.IsClosed)
        {
          if (postedCaseTracking.DecisionRemark == null || (postedCaseTracking.DecisionTypeId == 0 || postedCaseTracking.DecisionTypeId == null) ||
              postedCaseTracking.ClosingReason == null
              )
          {
            return null;
          }

        }

        var caseApplication = await _context.CaseApplication
        .Where(ca => ca.CaseInvestigation.Case.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false
        && (ca.CaseStatusTypeId == 31)
        )
        .AsNoTracking().FirstOrDefaultAsync();
        if (caseApplication == null)
          return null;



        DateTime dateTime = DateTime.Now;
        var caseTracking = _mapper.Map<CaseDecisionTracking>(postedCaseTracking);
        caseTracking.Appointment = null;
        CaseAppointment nextAppointment = null;
        var oldCaseTracking = _context.CaseDecisionTracking.FirstOrDefault(x => x.CaseDecisionTrackingId == caseTracking.CaseDecisionTrackingId);
        if (postedCaseTracking.IsAppointment)
        {
          var oldAppointments = await _context.CaseAppointment
                .Where(ca => ca.CaseId == postedCaseTracking.CaseId && ca.AppointmentDate > postedCaseTracking.Appointment.AppointmentDate && ca.IsDeleted == false
                )
                .AsNoTracking().ToListAsync();
          if (oldAppointments.Count > 0)
            return null;

          nextAppointment = _mapper.Map<CaseAppointment>(postedCaseTracking.Appointment);
          nextAppointment.CreatedUserId = postedCaseTracking.CreatedUserId;
          nextAppointment.CaseAppointmentId = Guid.NewGuid();
          nextAppointment.CaseId = postedCaseTracking.CaseId;
          nextAppointment.CreatedDateTime = dateTime;
          nextAppointment.IsActive = true;
          nextAppointment.IsDeleted = false;
          nextAppointment.UpdatedDateTime = dateTime;
          nextAppointment.AppointmentDate = nextAppointment.AppointmentDate.AddDays(1);
          nextAppointment.CreatedUserId = postedCaseTracking.CreatedUserId;
          var caseAppointment = _context.Case.Where(x => x.CaseApplicationId == caseApplication.CaseApplicationId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
          caseAppointment.LastAppointmentId = nextAppointment.CaseAppointmentId;
        }
        else
        {
          caseTracking.AppointmentId = null;
          caseTracking.Appointment = null;

        }
        Case legalCase = null;
        if (postedCaseTracking.IsClosed)
        {
          legalCase = await _context.Case
      .Where(ca => ca.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false)
      .AsNoTracking().FirstOrDefaultAsync();
          if (legalCase == null)
            return null;
          legalCase.ClosingDate = dateTime;
          caseApplication.CaseStatusTypeId = 30;
          legalCase.ClosingReason = postedCaseTracking.ClosingReason;
          legalCase.DecisionRemark = postedCaseTracking.DecisionRemark;
          legalCase.DecisionTypeId = postedCaseTracking.DecisionTypeId;
          legalCase.DecisionCourtSessionId = postedCaseTracking.CaseDecisionTrackingId;
          legalCase.CreatedUserId = postedCaseTracking.CreatedUserId;
          legalCase.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
          legalCase.EngOtherRemark = postedCaseTracking.EngOtherRemark;
          legalCase.MaxInjuctionDate = postedCaseTracking.MaxInjuctionDate;
          legalCase.MinInjuctionDate = postedCaseTracking.MinInjuctionDate;

        }
        caseTracking.CreatedDateTime = dateTime;
        caseTracking.IsActive = true;
        caseTracking.IsDeleted = false;
        caseTracking.UpdatedDateTime = dateTime;


        CAUnit cAUnit = new CAUnit
        {
          CAUnitId = postedCaseTracking.CaseDecisionTrackingId,
          CreatedDateTime = dateTime,
          UpdatedDateTime = dateTime  ,
          CreatedUserId = postedCaseTracking.CreatedUserId
        };

        caseTracking.CaseDecisionTrackingId = cAUnit.CAUnitId;
        using (var transaction = await _context.Database.BeginTransactionAsync())
        {
          try
          {
            if (nextAppointment != null)
              _context.Entry(nextAppointment).State = EntityState.Added;
            //if (oldCaseTracking != null)
            //{
            //  oldCaseTracking.AppointmentId = nextAppointment.CaseAppointmentId;
            //  _context.SaveChanges();
            //}
            //else
            //{
            caseTracking.AppointmentId = nextAppointment.CaseAppointmentId;

            //}
            //await DeActivateOldAppointments(postedCaseTracking.CaseId);

            //await DeActivateOldCourtSessionDecisions(postedCaseTracking.CaseId);
            if (!postedCaseTracking.IsClosed)
            {
              //var CaUnit = _context.CAUnit.FirstOrDefault(x => x.CAUnitId == cAUnit.CAUnitId);
              //if (CaUnit == null)
              _context.Entry(cAUnit).State = EntityState.Added;
              //var caseTD = _context.CaseDecisionTracking.FirstOrDefault(x => x.CaseDecisionTrackingId == caseTracking.CaseDecisionTrackingId);
              //if (caseTD == null)
              //if (caseTD == null)
              _context.Entry(caseTracking).State = EntityState.Added; //_context.Entry(caseTracking).State = EntityState.Modified;
                                                                      // else



              _context.Entry(caseApplication).State = EntityState.Modified;

              if (legalCase != null)
                _context.Entry(legalCase).State = EntityState.Modified;
            }


            _context.SaveChanges();
            transaction.Commit();
            postedCaseTracking.CaseDecisionTrackingId = caseTracking.CaseDecisionTrackingId;
            return postedCaseTracking;

          }
          catch (Exception ex)
          {
            transaction.Rollback();
            throw new Exception(ex.InnerException.ToString());
          }
        }
      }
      catch (Exception ex)
      {

        throw new Exception(ex.InnerException.ToString());
      }

    }
    public async Task<CaseDecisionTrackingDTO> UpdateCourtSessionDecision(CaseDecisionTrackingDTO postedCaseTracking)
    {
      if (postedCaseTracking.IsClosed && postedCaseTracking.IsAppointment)
        return null;
      if (postedCaseTracking.IsAppointment && postedCaseTracking.Appointment == null)
        return null;

      var caseApplication = await _context.CaseApplication
      .Where(ca => ca.CaseInvestigation.Case.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false
      && (ca.CaseStatusTypeId == 31)
      )
      .AsNoTracking().FirstOrDefaultAsync();
      if (caseApplication == null)
        return null;


      var caseTracking = _mapper.Map<CaseDecisionTracking>(postedCaseTracking);

      DateTime dateTime = DateTime.Now;
      var oldCaseTracking = await _context.CaseDecisionTracking.Where(c => c.CaseDecisionTrackingId == postedCaseTracking.CaseDecisionTrackingId && c.IsActive == true).AsNoTracking().FirstOrDefaultAsync();
      if (oldCaseTracking == null)
      {
        CAUnit cAUnit = new CAUnit
        {
          CAUnitId = postedCaseTracking.CaseDecisionTrackingId,
          CreatedDateTime = dateTime,
          UpdatedDateTime = dateTime,
        };
        _context.CAUnit.Add(cAUnit);
        _context.CaseDecisionTracking.Add(caseTracking);
       
      }
        

      caseTracking.Appointment = null;
      CaseAppointment newAppointment = null;
      CaseAppointment updatedAppointment = null;
      if (postedCaseTracking.IsAppointment)
      {
        var oldAppointments = await _context.CaseAppointment
              .Where(ca => ca.CaseId == postedCaseTracking.CaseId && ca.AppointmentDate > postedCaseTracking.Appointment.AppointmentDate && ca.CaseAppointmentId != postedCaseTracking.CaseDecisionTrackingId && ca.IsDeleted == false
              )
              .AsNoTracking().ToListAsync();
        if (oldAppointments.Count > 0)
          return null;
        if (oldCaseTracking.AppointmentId == null)
        {
          newAppointment = _mapper.Map<CaseAppointment>(postedCaseTracking.Appointment);
          newAppointment.CaseAppointmentId = Guid.NewGuid();
          newAppointment.CaseId = postedCaseTracking.CaseId;
          newAppointment.CreatedDateTime = dateTime;
          newAppointment.IsActive = true;
          newAppointment.IsDeleted = false;
          newAppointment.UpdatedDateTime = dateTime;
          newAppointment.CreatedUserId = postedCaseTracking.Appointment.CreatedUserId;
          newAppointment.EngOtherRemark = postedCaseTracking.EngOtherRemark;
          newAppointment.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
          caseTracking.AppointmentId = newAppointment.CaseAppointmentId;
       
          var caseAppointment = _context.Case.Where(x => x.CaseApplicationId == caseApplication.CaseApplicationId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
          caseAppointment.LastAppointmentId = newAppointment.CaseAppointmentId;
        }
        else
        {
          var oldAppointment = await _context.CaseAppointment.Where(c => c.CaseAppointmentId == postedCaseTracking.CaseDecisionTrackingId).AsNoTracking().FirstOrDefaultAsync();

          if (oldAppointment == null)
            return null;
          updatedAppointment = _mapper.Map(postedCaseTracking.Appointment, oldAppointment);
        }

      }
      else
      {
        caseTracking.AppointmentId = null;
        caseTracking.Appointment = null;

      }
      Case legalCase = null;
      if (postedCaseTracking.IsClosed)
      {

        legalCase = await _context.Case
    .Where(ca => ca.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false)
    .AsNoTracking().FirstOrDefaultAsync();
        if (legalCase == null)
          return null;
        legalCase.ClosingDate = dateTime;
        caseApplication.CaseStatusTypeId = 30;
        legalCase.ClosingReason = postedCaseTracking.ClosingReason;
        legalCase.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
        legalCase.EngOtherRemark = postedCaseTracking.EngOtherRemark;
        legalCase.MaxInjuctionDate = postedCaseTracking.MaxInjuctionDate;
        legalCase.MinInjuctionDate = postedCaseTracking.MinInjuctionDate;

      }
      legalCase.DecisionTypeId = postedCaseTracking.DecisionTypeId;
      caseTracking.CreatedDateTime = dateTime;
      caseTracking.IsActive = true;
      caseTracking.IsDeleted = false;
      caseTracking.UpdatedDateTime = dateTime;
      using (var transaction = await _context.Database.BeginTransactionAsync())
      {
        try
        {
          if (newAppointment != null && updatedAppointment == null)
            _context.Entry(newAppointment).State = EntityState.Added;
          if (newAppointment == null && updatedAppointment != null)
            _context.Entry(updatedAppointment).State = EntityState.Modified;
          await DeActivateOldAppointments(postedCaseTracking.CaseId);
          await DeActivateOldCourtSessionDecisions(postedCaseTracking.CaseId);

          //_context.Entry(oldCaseTracking).State = EntityState.Modified;

          _context.Entry(caseApplication).State = EntityState.Modified;

          if (legalCase != null)
            _context.Entry(legalCase).State = EntityState.Modified;

          _context.SaveChanges();
          transaction.Commit();
          postedCaseTracking.CaseDecisionTrackingId = caseTracking.CaseDecisionTrackingId;
          return postedCaseTracking;

        }
        catch (Exception ex)
        {
          transaction.Rollback();
          throw new Exception(ex.InnerException.ToString());
        }
      }
    }

    public async Task<CaseDecisionTrackingDTO> UpdateCourtSessionDecisionDetails(CaseDecisionTrackingDTO postedCaseTracking)
    {
      if (postedCaseTracking.IsClosed && postedCaseTracking.IsAppointment)
        return null;
      if (postedCaseTracking.IsAppointment && postedCaseTracking.Appointment == null)
        return null;

      var caseApplication = await _context.CaseApplication
      .Where(ca => ca.CaseInvestigation.Case.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false
      && (ca.CaseStatusTypeId == 31)
      )
      .AsNoTracking().FirstOrDefaultAsync();
      if (caseApplication == null)
        return null;



      DateTime dateTime = DateTime.Now;
      var oldCaseTracking = await _context.CaseDecisionTracking.Where(c => c.CaseDecisionTrackingId == postedCaseTracking.CaseDecisionTrackingId).AsNoTracking().FirstOrDefaultAsync();
      var caseTracking = _mapper.Map<CaseDecisionTracking>(postedCaseTracking);
      caseTracking.Appointment = null;
      CaseAppointment newAppointment = null;
      CaseAppointment updatedAppointment = null;
      if (postedCaseTracking.IsAppointment)
      {
        var oldAppointments = await _context.CaseAppointment
              .Where(ca => ca.CaseAppointmentId == postedCaseTracking.CaseDecisionTrackingId && ca.IsDeleted == false
              )
              .AsNoTracking().FirstOrDefaultAsync();
        //if (oldAppointments.Count > 0)
        //  return null;
        if (oldCaseTracking != null)
        {
          if (oldCaseTracking.AppointmentId != null)
          {
            //newAppointment = _mapper.Map<CaseAppointment>(postedCaseTracking.Appointment);
            oldAppointments.CaseAppointmentId = postedCaseTracking.CaseDecisionTrackingId;
            oldAppointments.CaseId = postedCaseTracking.CaseId;
            //newAppointment.CreatedDateTime = dateTime;
            oldAppointments.IsActive = true;
            oldAppointments.IsDeleted = false;
            oldAppointments.UpdatedDateTime = dateTime;
            oldAppointments.AppointmentDate = postedCaseTracking.Appointment.AppointmentDate.AddDays(1);
            oldAppointments.AppointmentTime = postedCaseTracking.Appointment.AppointmentTime;
            oldAppointments.Location = postedCaseTracking.Appointment.Location;
            oldAppointments.AmhAppointmentReason = postedCaseTracking.Appointment.AmhAppointmentReason;
            oldAppointments.AppointmentReason = postedCaseTracking.Appointment.AppointmentReason;
            oldAppointments.UpdatedUserId = postedCaseTracking.UpdatedUserId;
            oldAppointments.EngOtherRemark = postedCaseTracking.EngOtherRemark;
            oldAppointments.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
            oldAppointments.UpdatedUserId = postedCaseTracking.UpdatedUserId;
            caseTracking.AppointmentId = oldAppointments.CaseAppointmentId;

            //var cases = _context.Case.FirstOrDefault(x => x.CaseId == postedCaseTracking.CaseId);
            //cases.LastAppointmentId = oldAppointments.CaseAppointmentId;
            Case legalCase = null;
            oldCaseTracking.CreatedDateTime = dateTime;
            oldCaseTracking.IsActive = true;
            oldCaseTracking.IsDeleted = false;
            oldCaseTracking.UpdatedDateTime = dateTime;
            oldCaseTracking.FirstJudgeId = postedCaseTracking.FirstJudgeId;
            oldCaseTracking.SecondJudgeId = postedCaseTracking.SecondJudgeId;
            oldCaseTracking.ThirdJudgeId = postedCaseTracking.ThirdJudgeId;
            oldCaseTracking.Remark = postedCaseTracking.Remark;
            oldCaseTracking.AmhRemark = postedCaseTracking.AmhRemark;
           
            oldCaseTracking.UpdatedUserId = postedCaseTracking.UpdatedUserId;
            //transaction.Commit();
            postedCaseTracking.CaseDecisionTrackingId = caseTracking.CaseDecisionTrackingId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
              try
              {

                if (oldAppointments != null)
                  _context.Entry(oldAppointments).State = EntityState.Modified;

                if (oldCaseTracking != null)
                  _context.Entry(oldCaseTracking).State = EntityState.Modified;

                _context.Entry(caseApplication).State = EntityState.Modified;

                if (legalCase != null)
                  _context.Entry(legalCase).State = EntityState.Modified;

                _context.SaveChanges();
                transaction.Commit();
                postedCaseTracking.CaseDecisionTrackingId = caseTracking.CaseDecisionTrackingId;
                return postedCaseTracking;

              }
              catch (Exception ex)
              {
                transaction.Rollback();
                throw new Exception(ex.InnerException.ToString());
              }
            }
            //using (var transaction = await _context.Database.BeginTransactionAsync())
            //{
            //  try
            //  {
            //    //if (newAppointment != null && updatedAppointment == null)
            //    //  _context.Entry(newAppointment).State = EntityState.Added;
            //    //if (newAppointment == null && updatedAppointment != null)

            //    //await DeActivateOldAppointments(postedCaseTracking.CaseId);

            //    //await DeActivateOldCourtSessionDecisions(postedCaseTracking.CaseId);

            //    //_context.Entry(oldCaseTracking).State = EntityState.Modified;
            //    _context.Entry(oldAppointments).State = EntityState.Modified;

            //    _context.Entry(caseApplication).State = EntityState.Modified;

            //    if (oldCaseTracking != null)
            //      _context.Entry(oldCaseTracking).State = EntityState.Modified;



            //  }
            //  catch (Exception ex)
            //  {
            //    transaction.Rollback();
            //    throw new Exception(ex.InnerException.ToString());
            //  }
            //}
          }

        }
        else
        {
          try
          {
            if (postedCaseTracking.IsClosed && postedCaseTracking.IsAppointment)
              return null;
            if (postedCaseTracking.IsAppointment && postedCaseTracking.Appointment == null)
              return null;
            if (postedCaseTracking.IsClosed)
            {
              if (postedCaseTracking.DecisionRemark == null || (postedCaseTracking.DecisionTypeId == 0 || postedCaseTracking.DecisionTypeId == null) ||
                  postedCaseTracking.ClosingReason == null
                  )
              {
                return null;
              }

            }
            caseTracking.Appointment = null;
            CaseAppointment nextAppointment = null;

            if (postedCaseTracking.IsAppointment)
            {
              nextAppointment = _mapper.Map<CaseAppointment>(postedCaseTracking.Appointment);
              nextAppointment.CaseAppointmentId = Guid.NewGuid();
              nextAppointment.CaseId = postedCaseTracking.CaseId;
              nextAppointment.CreatedDateTime = dateTime;
              nextAppointment.IsActive = true;
              nextAppointment.IsDeleted = false;
              nextAppointment.UpdatedDateTime = dateTime;
              nextAppointment.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
              nextAppointment.EngOtherRemark = postedCaseTracking.EngOtherRemark;
              caseTracking.AppointmentId = nextAppointment.CaseAppointmentId;
              var caseAppointment = _context.Case.FirstOrDefault(x => x.CaseId == postedCaseTracking.CaseId);
              caseAppointment.LastAppointmentId = nextAppointment.CaseAppointmentId;
            }
            else
            {
              caseTracking.AppointmentId = null;
              caseTracking.Appointment = null;

            }
            Case legalCase = null;
            if (postedCaseTracking.IsClosed)
            {
              legalCase = await _context.Case
          .Where(ca => ca.CaseId == postedCaseTracking.CaseId && ca.IsDeleted == false)
          .AsNoTracking().FirstOrDefaultAsync();
              if (legalCase == null)
                return null;
              legalCase.ClosingDate = dateTime;
              caseApplication.CaseStatusTypeId = 30;
              legalCase.ClosingReason = postedCaseTracking.ClosingReason;
              legalCase.DecisionRemark = postedCaseTracking.DecisionRemark;
              legalCase.DecisionTypeId = postedCaseTracking.DecisionTypeId;
              legalCase.DecisionCourtSessionId = postedCaseTracking.CaseDecisionTrackingId;
              legalCase.CreatedUserId = postedCaseTracking.UpdatedUserId;
              legalCase.AmhOtherRemark = postedCaseTracking.AmhOtherRemark;
              legalCase.EngOtherRemark = postedCaseTracking.EngOtherRemark;
              legalCase.MaxInjuctionDate = postedCaseTracking.MaxInjuctionDate;
              legalCase.MinInjuctionDate = postedCaseTracking.MinInjuctionDate;
            }
            caseTracking.CreatedDateTime = dateTime;
            caseTracking.IsActive = true;
            caseTracking.IsDeleted = false;
            caseTracking.UpdatedDateTime = dateTime;
            caseTracking.CreatedUserId = postedCaseTracking.UpdatedUserId;

            CAUnit cAUnit = new CAUnit
            {
              CAUnitId = postedCaseTracking.CaseDecisionTrackingId,
              CreatedDateTime = dateTime,
              UpdatedDateTime = dateTime ,
              CreatedUserId = postedCaseTracking.UpdatedUserId,
            };

            caseTracking.CaseDecisionTrackingId = cAUnit.CAUnitId;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
              try
              {
                if (nextAppointment != null)
                  _context.Entry(nextAppointment).State = EntityState.Added;

                //await DeActivateOldAppointments(postedCaseTracking.CaseId);

                //await DeActivateOldCourtSessionDecisions(postedCaseTracking.CaseId);
                if (!postedCaseTracking.IsClosed)
                {
                  var CaUnit = _context.CAUnit.FirstOrDefault(x => x.CAUnitId == cAUnit.CAUnitId);
                  if (CaUnit == null)
                    _context.Entry(cAUnit).State = EntityState.Added;
                  var caseTD = _context.CaseDecisionTracking.FirstOrDefault(x => x.CaseDecisionTrackingId == caseTracking.CaseDecisionTrackingId);
                  if (caseTD == null)
                    _context.Entry(caseTracking).State = EntityState.Added; //_context.Entry(caseTracking).State = EntityState.Modified;
                                                                            // else



                  _context.Entry(caseApplication).State = EntityState.Modified;

                  if (legalCase != null)
                    _context.Entry(legalCase).State = EntityState.Modified;
                }


                _context.SaveChanges();
                transaction.Commit();
                postedCaseTracking.CaseDecisionTrackingId = caseTracking.CaseDecisionTrackingId;
                return postedCaseTracking;

              }
              catch (Exception ex)
              {
                transaction.Rollback();
                throw new Exception(ex.InnerException.ToString());
              }
            }
          }
          catch (Exception ex)
          {

            throw new Exception(ex.InnerException.ToString());
          }
        }


      }


      return postedCaseTracking;

    }

    public async Task<CaseTrackingDTO> GetDecisionByAppointmentId(Guid appointmentId)
    {
      try
      {
        var appointments = _context.CaseAppointment.Where(x => x.CaseAppointmentId == appointmentId).OrderByDescending(x => x.CreatedDateTime).Include(at => at.AppointmentType).FirstOrDefault();
        CaseTrackingDTO result = null;
        result = await (from ctr in _context.CaseDecisionTracking
                        join lc in _context.Case.Include(d => d.DecisionType)
                        on ctr.CaseId equals lc.CaseId
                        join fj in _context.Users.ToList()
                        on ctr.FirstJudgeId equals fj.Id
                        join sj in _context.Users
                        on ctr.SecondJudgeId equals sj.Id
                        join tj in _context.Users
                        on ctr.ThirdJudgeId equals tj.Id
                        //join appt in _context.CaseAppointment.Include(at => at.AppointmentType)
                        //on ctr.CaseDecisionTrackingId equals appt.CaseAppointmentId

                        //join aptype in _context.Lookup
                        //on appt.AppointmentTypeId equals aptype.LookupId
                        join caseApp in _context.CaseApplication on
                         lc.CaseApplicationId equals caseApp.CaseApplicationId
                        join party in _context.CaseParty on caseApp.SuspectId equals party.CasePartyId
                        where (ctr.CaseDecisionTrackingId == appointmentId)
                        select new CaseTrackingDTO
                        {
                          CaseId = ctr.CaseId,
                          DecisionDate = ctr.DecisionDate.Equals(DateTime.MinValue) ? DateTime.Now : ctr.DecisionDate,
                          CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                          IsAppointment = ctr.IsAppointment,
                          IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                          ThirdJudgeId = ctr.ThirdJudgeId,
                          SecondJudgeId = ctr.SecondJudgeId,
                          Remark = ctr.Remark,
                          AmhRemark = ctr.AmhRemark,
                          FirstJudgeId = ctr.FirstJudgeId,
                          FirstJudge = fj.FullName,
                          SecondJudge = sj.FullName,
                          ThirdJudge = tj.FullName,
                          AppointmentDate = appointments.AppointmentDate,
                          AppointmentReason = appointments.AppointmentReason,
                          AppointmentTime = appointments.AppointmentTime,
                          AppointmentType = _mapper.Map<LookUpDisplayView>(appointments.AppointmentType),
                          AppointmentTypeId = appointments.AppointmentTypeId,
                          CaseInvestigationId = lc.CaseInvestigationId,
                          CaseName = lc.CaseName,
                          FileNo = lc.FileNo,
                          OpeningDate = lc.OpeningDate,
                          OpeningReason = lc.OpeningReason,
                          CaseTrackingId = ctr.CaseDecisionTrackingId.Equals(Guid.Empty) ? "" : ctr.CaseDecisionTrackingId.ToString(),
                          AppointmentId = appointments.CaseAppointmentId,
                          IsActive = ctr.IsActive,
                          DecisionCourtSessionId = lc.DecisionCourtSessionId,
                          DecisionTypeId = lc.DecisionTypeId,
                          DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                          ClosingDate = lc.ClosingDate,
                          ClosingReason = lc.ClosingReason,
                          DecisionRemark = lc.DecisionRemark,
                          Url = ctr.Url,
                          Location = appointments.Location,
                          AmhAppointmentReason = appointments.AmhAppointmentReason,
                          ParentCaseId = lc.ParentCaseId,
                          ExchangeActorId =party.ExchangeActorId,
                   
                          EngAppointmentOtherRemark = appointments.EngOtherRemark,
                          AmhAppointmentOtherRemark = appointments.AmhOtherRemark,

                        })
                                .AsNoTracking()
                                        .FirstOrDefaultAsync();
        if (result == null)
          result = await GetAppointmentById(appointmentId);
        return result;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }

    public async Task<CaseTrackingDTO> GetDecisionByHistoryAppointmentId(Guid appointmentId)
    {
      try
      {
        var appointments = _context.CaseAppointment.Where(x => x.CaseAppointmentId == appointmentId).OrderByDescending(x => x.CreatedDateTime).Include(at => at.AppointmentType).FirstOrDefault();
        CaseTrackingDTO result = null;
        result = await (from ctr in _context.CaseDecisionTracking
                        join lc in _context.Case.Include(d => d.DecisionType)
                        on ctr.CaseId equals lc.ParentCaseId
                        join fj in _context.Users.ToList()
                        on ctr.FirstJudgeId equals fj.Id
                        join sj in _context.Users
                        on ctr.SecondJudgeId equals sj.Id
                        join tj in _context.Users
                        on ctr.ThirdJudgeId equals tj.Id
                        //join appt in _context.CaseAppointment.Include(at => at.AppointmentType)
                        //on ctr.CaseDecisionTrackingId equals appt.CaseAppointmentId

                        //join aptype in _context.Lookup
                        //on appt.AppointmentTypeId equals aptype.LookupId
                        join caseApp in _context.CaseApplication on
                         lc.CaseApplicationId equals caseApp.CaseApplicationId
                        //where (ctr.CaseDecisionTrackingId == appointmentId)
                        select new CaseTrackingDTO
                        {
                          CaseId = ctr.CaseId,
                          DecisionDate = ctr.DecisionDate.Equals(DateTime.MinValue) ? DateTime.Now : ctr.DecisionDate,
                          CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                          IsAppointment = ctr.IsAppointment,
                          IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                          ThirdJudgeId = ctr.ThirdJudgeId,
                          SecondJudgeId = ctr.SecondJudgeId,
                          Remark = ctr.Remark,
                          AmhRemark = ctr.AmhRemark,
                          FirstJudgeId = ctr.FirstJudgeId,
                          FirstJudge = fj.FullName,
                          SecondJudge = sj.FullName,
                          ThirdJudge = tj.FullName,
                          AppointmentDate = appointments.AppointmentDate,
                          AppointmentReason = appointments.AppointmentReason,
                          AppointmentTime = appointments.AppointmentTime,
                          AppointmentType = _mapper.Map<LookUpDisplayView>(appointments.AppointmentType),
                          AppointmentTypeId = appointments.AppointmentTypeId,
                          CaseInvestigationId = lc.CaseInvestigationId,
                          CaseName = lc.CaseName,
                          FileNo = lc.FileNo,
                          OpeningDate = lc.OpeningDate,
                          OpeningReason = lc.OpeningReason,
                          CaseTrackingId = ctr.CaseDecisionTrackingId.Equals(Guid.Empty) ? "" : ctr.CaseDecisionTrackingId.ToString(),
                          AppointmentId = appointments.CaseAppointmentId,
                          IsActive = ctr.IsActive,
                          DecisionCourtSessionId = lc.DecisionCourtSessionId,
                          DecisionTypeId = lc.DecisionTypeId,
                          DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                          ClosingDate = lc.ClosingDate,
                          ClosingReason = lc.ClosingReason,
                          DecisionRemark = lc.DecisionRemark,
                          Url = ctr.Url,
                          Location = appointments.Location,
                          AmhAppointmentReason = appointments.AmhAppointmentReason,
                          ParentCaseId = lc.ParentCaseId,
                          EngOtherRemark = lc.EngOtherRemark,
                          AmhOtherRemark = lc.AmhOtherRemark

                        })
                                .AsNoTracking()
                                        .FirstOrDefaultAsync();
        if (result == null)
          result = await GetAppointmentById(appointmentId);
        return result;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
    public async Task<CaseTrackingDTO> GetDecisionHistoryByCaseParantId(Guid ParentCaseId)
    {
      try
      {
        var appointments = _context.CaseAppointment.Where(x => x.CaseId == ParentCaseId).OrderByDescending(x => x.CreatedDateTime).Include(at => at.AppointmentType).FirstOrDefault();
        CaseTrackingDTO result = null;
        result = await (from ctr in _context.CaseDecisionTracking
                        join lc in _context.Case.Include(d => d.DecisionType)
                        on ctr.CaseId equals lc.ParentCaseId
                        join fj in _context.Users.ToList()
                        on ctr.FirstJudgeId equals fj.Id
                        join sj in _context.Users
                        on ctr.SecondJudgeId equals sj.Id
                        join tj in _context.Users
                        on ctr.ThirdJudgeId equals tj.Id
                        //join appt in _context.CaseAppointment.Include(at => at.AppointmentType)
                        //on ctr.CaseDecisionTrackingId equals appt.CaseAppointmentId

                        //join aptype in _context.Lookup
                        //on appt.AppointmentTypeId equals aptype.LookupId
                        join caseApp in _context.CaseApplication on
                         lc.CaseApplicationId equals caseApp.CaseApplicationId
                        where (lc.ParentCaseId == ParentCaseId)
                        select new CaseTrackingDTO
                        {
                          CaseId = ctr.CaseId,
                          DecisionDate = ctr.DecisionDate.Equals(DateTime.MinValue) ? DateTime.Now : ctr.DecisionDate,
                          CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                          IsAppointment = ctr.IsAppointment,
                          IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                          ThirdJudgeId = ctr.ThirdJudgeId,
                          SecondJudgeId = ctr.SecondJudgeId,
                          Remark = ctr.Remark,
                          AmhRemark = ctr.AmhRemark,
                          FirstJudgeId = ctr.FirstJudgeId,
                          FirstJudge = fj.FullName,
                          SecondJudge = sj.FullName,
                          ThirdJudge = tj.FullName,
                          AppointmentDate = appointments.AppointmentDate,
                          AppointmentReason = appointments.AppointmentReason,
                          AppointmentTime = appointments.AppointmentTime,
                          AppointmentType = _mapper.Map<LookUpDisplayView>(appointments.AppointmentType),
                          AppointmentTypeId = appointments.AppointmentTypeId,
                          CaseInvestigationId = lc.CaseInvestigationId,
                          CaseName = lc.CaseName,
                          FileNo = lc.FileNo,
                          OpeningDate = lc.OpeningDate,
                          OpeningReason = lc.OpeningReason,
                          CaseTrackingId = ctr.CaseDecisionTrackingId.Equals(Guid.Empty) ? "" : ctr.CaseDecisionTrackingId.ToString(),
                          AppointmentId = appointments.CaseAppointmentId,
                          IsActive = ctr.IsActive,
                          DecisionCourtSessionId = lc.DecisionCourtSessionId,
                          DecisionTypeId = lc.DecisionTypeId,
                          DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                          ClosingDate = lc.ClosingDate,
                          ClosingReason = lc.ClosingReason,
                          DecisionRemark = lc.DecisionRemark,
                          Url = ctr.Url,
                          Location = appointments.Location,
                          AmhAppointmentReason = appointments.AmhAppointmentReason,
                          ParentCaseId = lc.ParentCaseId,
                           EngOtherRemark = lc.EngOtherRemark,
                           AmhOtherRemark = lc.AmhOtherRemark
                         
                        })
                                .AsNoTracking()
                                        .FirstOrDefaultAsync();
        //if (result == null)
        //  result = await GetAppointmentById(appointmentId);
        return result;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }

    public async Task<CaseTrackingDTO> GetAppointmentById(Guid appointmentId)
    {
      CaseTrackingDTO result = null;
      result = await (from appt in _context.CaseAppointment
                      join lc in _context.Case.Include(d => d.DecisionType)
                      on appt.CaseId equals lc.CaseId
                      join aptype in _context.Lookup
                      on appt.AppointmentTypeId equals aptype.LookupId
                      join caseApp in _context.CaseApplication on
                      lc.CaseApplicationId equals caseApp.CaseApplicationId
                      join party in _context.CaseParty on caseApp.SuspectId equals party.CasePartyId
                      where (appt.CaseAppointmentId == appointmentId)
                      select new CaseTrackingDTO
                      {
                        CaseId = appt.CaseId,
                        IsAppointment = (caseApp.CaseStatusTypeId != 30) ? true : false,
                        IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,

                        DecisionDate = appt.AppointmentDate,
                        AppointmentId = appt.CaseAppointmentId,
                        AppointmentDate = appt.AppointmentDate,
                        AppointmentReason = appt.AppointmentReason,
                        AppointmentTime = appt.AppointmentTime,
                        AppointmentType = _mapper.Map<LookUpDisplayView>(aptype),
                        AppointmentTypeId = appt.AppointmentTypeId,
                        CaseInvestigationId = lc.CaseInvestigationId,
                        CaseName = lc.CaseName,
                        FileNo = lc.FileNo,
                        OpeningDate = lc.OpeningDate,
                        OpeningReason = lc.OpeningReason,
                        CaseDecisionTrackingId = appt.CaseAppointmentId,
                        CaseTrackingId = appt.CaseAppointmentId.ToString(),
                        IsActive = appt.IsActive,
                        DecisionCourtSessionId = lc.DecisionCourtSessionId,
                        DecisionTypeId = lc.DecisionTypeId,
                        DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                        ClosingDate = lc.ClosingDate,
                        ClosingReason = lc.ClosingReason,
                        DecisionRemark = lc.DecisionRemark,
                        Url = "",
                        Location = appt.Location,
                        AmhAppointmentReason = appt.AmhAppointmentReason,
                        ParentCaseId = lc.ParentCaseId,
                        ExchangeActorId = party.ExchangeActorId ,
     
                        EngAppointmentOtherRemark = appt.EngOtherRemark,
                        AmhAppointmentOtherRemark = appt.AmhOtherRemark,

                      })
                      .AsNoTracking()
                              .FirstOrDefaultAsync();
      return result;
    }
    public async Task<bool> UpdateUrl(CaseTrackingVideoDTO updatedData)
    {
      var oldData = await _context.CaseDecisionTracking.Where(ca => ca.CaseDecisionTrackingId == updatedData.CaseDecisionTrackingId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
      if (oldData == null)
        return false;
      oldData.Url = updatedData.Url;
      using (var transaction = await _context.Database.BeginTransactionAsync())
      {
        try
        {
          _context.Entry(oldData).State = EntityState.Modified;

          _context.SaveChanges();
          transaction.Commit();

          return true;

        }
        catch (Exception ex)
        {
          transaction.Rollback();
          throw new Exception(ex.InnerException.ToString());
        }
      }
    }
    public async Task<CaseDecisionTrackingDTO> GetCaseTrackingById(Guid caseTrackingId)
    {
      CaseDecisionTrackingDTO result = await (from ctr in _context.CaseDecisionTracking
                              .Include(at => at.FirstJudge)
                              .Include(at => at.SecondJudge)
                              .Include(at => at.ThirdJudge)
                                              join appt in _context.CaseAppointment.Include(at => at.AppointmentType)
                                              on ctr.AppointmentId equals appt.CaseAppointmentId
                                              join aptype in _context.Lookup
                                                on appt.AppointmentTypeId equals aptype.LookupId
                                              join lc in _context.Case on ctr.CaseId equals lc.CaseId
                                              join caseApp in _context.CaseApplication on
                                               lc.CaseApplicationId equals caseApp.CaseApplicationId
                                              where (ctr.CaseDecisionTrackingId == caseTrackingId)
                                              select new CaseDecisionTrackingDTO
                                              {
                                                CaseId = ctr.CaseId,
                                                DecisionDate = ctr.DecisionDate,
                                                CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                                                IsAppointment = ctr.IsAppointment,
                                                IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                                                ThirdJudgeId = ctr.FirstJudgeId,
                                                SecondJudgeId = ctr.SecondJudgeId,
                                                Remark = ctr.Remark,
                                                AmhRemark = ctr.AmhRemark,
                                                FirstJudgeId = ctr.FirstJudgeId,
                                                ThirdJudge = _context.Users.FirstOrDefault(x => x.Id == ctr.FirstJudgeId).FullName,
                                                SecondJudge = _context.Users.FirstOrDefault(x => x.Id == ctr.SecondJudgeId).FullName,
                                                FirstJudge = _context.Users.FirstOrDefault(x => x.Id == ctr.ThirdJudgeId).FullName,
                                                Appointment = _mapper.Map<AppointmentDTO>(ctr.Appointment),
                                                IsActive = appt.IsActive,
                                                Url = ctr.Url,
                                                ParentCaseId = lc.ParentCaseId,
                                                AmhOtherRemark = lc.AmhOtherRemark,
                                                EngOtherRemark =lc.EngOtherRemark
                                              })
                              .AsNoTracking()
                              .FirstOrDefaultAsync();
      return result;
    }

    public async Task<PagedResult<CaseTrackingDTO>> SearchCaseTracking(CaseTrackingSearchCriteriaModel queryParameters)
    {
      try
      {
        var dataResult = await (from ctr in _context.CaseAppointment.Include(at => at.AppointmentType)
                                join lc in _context.Case.Include(d => d.DecisionType)
                                on ctr.CaseId equals lc.CaseId
                                join inv in _context.CaseInvestigation
                                on lc.CaseInvestigationId equals inv.CaseInvestigationId
                                join ca in _context.CaseApplication
                                on inv.CaseApplicationId equals ca.CaseApplicationId
                                join cst in _context.CaseStatusType
                                on ca.CaseStatusTypeId equals cst.CaseStatusTypeId
                                join ct in _context.Lookup
                                  on ca.CaseTypeId equals ct.LookupId
                                where (
                                  (queryParameters.FileNo == null || (lc.FileNo.Contains(queryParameters.FileNo))) &&
                                  (queryParameters.CaseName == null || (lc.CaseName.Contains(queryParameters.CaseName))) &&
                                  (queryParameters.From == null || ctr.AppointmentDate.Date >= DateTime.Parse(queryParameters.From)) &&
                                  (queryParameters.To == null || ctr.AppointmentDate.Date <= DateTime.Parse(queryParameters.To)) &&
                                  lc.IsDeleted == false
                                  && cst.CaseStatusTypeId == 31
                                  && (lc.DecisionTypeId == null)
                                  )
                                orderby (ctr.AppointmentDate) descending

                                select new CaseTrackingDTO
                                {
                                  CaseId = lc.CaseId,
                                  CaseInvestigationId = inv.CaseInvestigationId,
                                  CaseTypeId = ca.CaseTypeId,
                                  InvestigationFileNo = inv.FileNo,
                                  CaseName = lc.CaseName,
                                  CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                  OpeningDate = lc.OpeningDate,
                                  FileNo = lc.FileNo,
                                  AppointmentDate = ctr.AppointmentDate,
                                  AppointmentTime = ctr.AppointmentTime,
                                  AppointmentType = _mapper.Map<LookUpDisplayView>(ctr.AppointmentType),
                                  AppointmentTypeId = ctr.AppointmentTypeId,
                                  CaseDecisionTrackingId = ctr.CaseAppointmentId,
                                  IsAppointment = ca.CaseStatusTypeId == 31,
                                  IsClosed = ca.CaseStatusTypeId == 30,
                                  AppointmentId = ctr.CaseAppointmentId,
                                  CaseTrackingId = ctr.CaseAppointmentId.ToString(),
                                  IsActive = ctr.IsActive,
                                  DecisionCourtSessionId = lc.DecisionCourtSessionId,
                                  DecisionTypeId = lc.DecisionTypeId,
                                  DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                                  ClosingDate = lc.ClosingDate,
                                  ClosingReason = lc.ClosingReason,
                                  DecisionRemark = lc.DecisionRemark,
                                  CaseApplicationId = inv.CaseApplicationId,
                                  ParentCaseId = lc.ParentCaseId ,
                                  AmhOtherRemark =  ctr.AmhOtherRemark,
                                  EngOtherRemark     = ctr.EngOtherRemark
                                       
                                  
                                })
                            .AsNoTracking()
                            .ToListAsync();

        var dr = dataResult.Distinct(new CaseTrackingComparer()).ToList();
        dr.AsQueryable().Paging(queryParameters.PageSize, queryParameters.PageIndex);
        //var dr=dataResult.AsQueryable().Paging(queryParameters.PageSize, queryParameters.PageIndex);

        return new PagedResult<CaseTrackingDTO>()
        {
          Items = _mapper.Map<List<CaseTrackingDTO>>(dr),
          ItemsCount = dr.Count()
        };

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }

    public async Task<PagedResult<CaseTrackingDTO>> GetAllCourtSessions(Guid caseId, int pageIndex, int pageSize)
    {
      try
      {
        var dataResult = await (from ctr in _context.CaseDecisionTracking
                                join lc in _context.Case.Include(d => d.DecisionType)
                                on ctr.CaseId equals lc.CaseId
                                join fj in _context.Users
                                on ctr.FirstJudgeId equals fj.Id
                                join sj in _context.Users
                                on ctr.SecondJudgeId equals sj.Id
                                join tj in _context.Users
                                on ctr.ThirdJudgeId equals tj.Id
                                join appt in _context.CaseAppointment
                                on ctr.AppointmentId equals appt.CaseAppointmentId
                                join aptype in _context.Lookup
                                on appt.AppointmentTypeId equals aptype.LookupId
                                join caseApp in _context.CaseApplication on
                                  lc.CaseApplicationId equals caseApp.CaseApplicationId
                                where (ctr.CaseId == caseId)
                                orderby ctr.CreatedDateTime descending
                                select new CaseTrackingDTO
                                {
                                  CaseId = ctr.CaseId,
                                  DecisionDate = ctr.DecisionDate.Equals(DateTime.MinValue) ? DateTime.Now : ctr.DecisionDate,
                                  CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                                  IsAppointment = ctr.IsAppointment,
                                  IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                                  ThirdJudgeId = ctr.ThirdJudgeId,
                                  SecondJudgeId = ctr.SecondJudgeId,
                                  Remark = ctr.Remark,
                                  AmhRemark = ctr.AmhRemark,
                                  FirstJudgeId = ctr.FirstJudgeId,
                                  FirstJudge = fj.FullName,
                                  SecondJudge = sj.FullName,
                                  ThirdJudge = tj.FullName,
                                  AppointmentDate = appt.AppointmentDate,
                                  AppointmentReason = appt.AppointmentReason,
                                  AppointmentTime = appt.AppointmentTime,
                                  AppointmentType = _mapper.Map<LookUpDisplayView>(aptype),
                                  AppointmentTypeId = appt.AppointmentTypeId,
                                  CaseInvestigationId = lc.CaseInvestigationId,
                                  CaseName = lc.CaseName,
                                  FileNo = lc.FileNo,
                                  OpeningDate = lc.OpeningDate,
                                  OpeningReason = lc.OpeningReason,
                                  CaseTrackingId = ctr.CaseDecisionTrackingId.Equals(Guid.Empty) ? "" : ctr.CaseDecisionTrackingId.ToString(),
                                  AppointmentId = appt.CaseAppointmentId,
                                  IsActive = ctr.IsActive,
                                  DecisionCourtSessionId = lc.DecisionCourtSessionId,
                                  DecisionTypeId = lc.DecisionTypeId,
                                  DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                                  ClosingDate = lc.ClosingDate,
                                  ClosingReason = lc.ClosingReason,
                                  DecisionRemark = lc.DecisionRemark,
                                  ParentCaseId = lc.ParentCaseId ,
                                  AmhOtherRemark = lc.AmhOtherRemark,
                                  EngOtherRemark = lc.EngOtherRemark

                                })
                    .AsNoTracking()
                    .Paging(pageSize, pageIndex)
                    .ToListAsync();
        return new PagedResult<CaseTrackingDTO>()
        {
          Items = _mapper.Map<List<CaseTrackingDTO>>(dataResult),
          ItemsCount = dataResult.Count()
        };
      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }
    public async Task<PagedResult<CaseTrackingDTO>> GetAllCourtSessionsHistory(Guid caseId, int pageIndex, int pageSize)
    {
      try
      {
        var dataResult = await (from ctr in _context.CaseDecisionTracking
                                join lc in _context.Case.Include(d => d.DecisionType)
                                on ctr.CaseId equals lc.ParentCaseId
                                join fj in _context.Users
                                on ctr.FirstJudgeId equals fj.Id
                                join sj in _context.Users
                                on ctr.SecondJudgeId equals sj.Id
                                join tj in _context.Users
                                on ctr.ThirdJudgeId equals tj.Id
                                join appt in _context.CaseAppointment
                                on ctr.AppointmentId equals appt.CaseAppointmentId
                                join aptype in _context.Lookup
                                on appt.AppointmentTypeId equals aptype.LookupId
                                join caseApp in _context.CaseApplication on
                                  lc.CaseApplicationId equals caseApp.CaseApplicationId
                                where (ctr.CaseId == caseId && ctr.IsActive == false)
                                orderby ctr.CreatedDateTime descending
                                select new CaseTrackingDTO
                                {
                                  CaseId = ctr.CaseId,
                                  DecisionDate = ctr.DecisionDate.Equals(DateTime.MinValue) ? DateTime.Now : ctr.DecisionDate,
                                  CaseDecisionTrackingId = ctr.CaseDecisionTrackingId,
                                  IsAppointment = ctr.IsAppointment,
                                  IsClosed = (caseApp.CaseStatusTypeId == 30) ? true : false,
                                  ThirdJudgeId = ctr.ThirdJudgeId,
                                  SecondJudgeId = ctr.SecondJudgeId,
                                  Remark = ctr.Remark,
                                  AmhRemark = ctr.AmhRemark,
                                  FirstJudgeId = ctr.FirstJudgeId,
                                  FirstJudge = fj.FullName,
                                  SecondJudge = sj.FullName,
                                  ThirdJudge = tj.FullName,
                                  AppointmentDate = appt.AppointmentDate,
                                  AppointmentReason = appt.AppointmentReason,
                                  AppointmentTime = appt.AppointmentTime,
                                  AppointmentType = _mapper.Map<LookUpDisplayView>(aptype),
                                  AppointmentTypeId = appt.AppointmentTypeId,
                                  CaseInvestigationId = lc.CaseInvestigationId,
                                  CaseName = lc.CaseName,
                                  FileNo = lc.FileNo,
                                  OpeningDate = lc.OpeningDate,
                                  OpeningReason = lc.OpeningReason,
                                  CaseTrackingId = ctr.CaseDecisionTrackingId.Equals(Guid.Empty) ? "" : ctr.CaseDecisionTrackingId.ToString(),
                                  AppointmentId = appt.CaseAppointmentId,
                                  IsActive = ctr.IsActive,
                                  DecisionCourtSessionId = lc.DecisionCourtSessionId,
                                  DecisionTypeId = lc.DecisionTypeId,
                                  DecisionType = _mapper.Map<LookUpDisplayView>(lc.DecisionType),
                                  ClosingDate = lc.ClosingDate,
                                  ClosingReason = lc.ClosingReason,
                                  DecisionRemark = lc.DecisionRemark ,
                                  AmhOtherRemark = lc.AmhOtherRemark,
                                  EngOtherRemark = lc.EngOtherRemark

                                })
            .AsNoTracking()
            .Paging(pageSize, pageIndex)
            .ToListAsync();
        return new PagedResult<CaseTrackingDTO>()
        {
          Items = _mapper.Map<List<CaseTrackingDTO>>(dataResult),
          ItemsCount = dataResult.Count()
        };
      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }

    private async Task DeActivateOldAppointments(Guid caseId)
    {
      try
      {
        var oldAppointments = await _context.CaseAppointment.Where(a => a.CaseId == caseId && a.IsActive == true).AsNoTracking().ToListAsync();
        foreach (var apt in oldAppointments)
        {
          apt.IsActive = false;
          _context.Entry(apt).State = EntityState.Modified;
        }

      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }

    private async Task DeActivateOldCourtSessionDecisions(Guid caseId)
    {
      try
      {
        var oldCourtSessionDecisions = await _context.CaseDecisionTracking.Where(a => a.CaseId == caseId && a.IsActive == true).AsNoTracking().ToListAsync();
        foreach (var dec in oldCourtSessionDecisions)
        {
          dec.IsActive = false;
          _context.Entry(dec).State = EntityState.Modified;
        }
      }
      catch (Exception ex)
      {
        throw new Exception(ex.InnerException.ToString());
      }
    }

  }

  public class CaseTrackingComparer : IEqualityComparer<CaseTrackingDTO>
  {
    public bool Equals(CaseTrackingDTO x, CaseTrackingDTO y)
    {
      return x.FileNo == y.FileNo;
    }

    public int GetHashCode(CaseTrackingDTO obj)
    {
      return obj.FileNo.GetHashCode();
    }
  }
}
