using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class CaseTribunalFeedBackRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public CaseTribunalFeedBackRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<CaseTribunalFeedBackDto> Create(CaseTribunalFeedBackDto postedFeedback)
        {
          if(postedFeedback.CaseLookUpId!=null)
      {   if(postedFeedback.CaseLookUpId==37)
        {
          var caseInve = _context.CaseInvestigation.FirstOrDefault(x => x.CaseInvestigationId == postedFeedback.CaseInvestigationId);
          var caseAppln = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == caseInve.CaseApplicationId);
          caseAppln.CaseStatusTypeId = 35;
          _context.SaveChanges();

        }

      }
            DateTime dateTime = DateTime.Now;
            var feedBack = _mapper.Map<CaseTribunalFeedBack>(postedFeedback);
            feedBack.CreatedDateTime = dateTime;
            feedBack.UpdatedDateTime = dateTime;
            _context.CaseTribunalFeedBack.Add(feedBack);
            _context.SaveChanges();
            return postedFeedback;
        }
        public async Task<CaseTribunalFeedBackDto> Update(CaseTribunalFeedBackDto postedFeedback)
        {
      if (postedFeedback.CaseLookUpId != null)
      {
        if (postedFeedback.CaseLookUpId == 37)
        {
          var caseInve = _context.CaseInvestigation.FirstOrDefault(x => x.CaseInvestigationId == postedFeedback.CaseInvestigationId);
          var caseAppln = _context.CaseApplication.FirstOrDefault(x => x.CaseApplicationId == caseInve.CaseApplicationId);
          caseAppln.CaseStatusTypeId = 35;
          _context.SaveChanges();

        }

      }
      DateTime dateTime = DateTime.Now;
            var feedBack = _mapper.Map<CaseTribunalFeedBack>(postedFeedback);
            var all = _context.CaseTribunalFeedBack;
            var oldFeedBack = _context.CaseTribunalFeedBack.FirstOrDefault(x => x.Id == postedFeedback.Id);
      oldFeedBack.CreatedDateTime = dateTime;
      oldFeedBack.UpdatedDateTime = dateTime;
            oldFeedBack.CaseInvestigationId = feedBack.CaseInvestigationId;
            oldFeedBack.CaseLookUpId = feedBack.CaseLookUpId;
            oldFeedBack.FeedBack = feedBack.FeedBack;
            oldFeedBack.UpdatedUserId = feedBack.UpdatedUserId;
            _context.SaveChanges();
            return postedFeedback;
        }
        public async Task<List<CaseLookUpDropDown>> GetCaseLookUp(string lang)
        {
            List<CaseLookUpDropDown> caseLookUpDropDowns = new List<CaseLookUpDropDown>();
            caseLookUpDropDowns = await _context.CaseLookUp.Where(x => x.TypeCode == "JOB_TYPE").Select(calD => new CaseLookUpDropDown { CaseLookUpId = calD.CaseLookUpId, FeedBackType = (lang == "et") ? calD.DescriptionAmh : calD.DescriptionEng }).ToListAsync();
            return caseLookUpDropDowns;
        }
        public async Task<List<CaseTribunalFeedBackDetail>> GetCaseTribunal(Guid CaseInvestigationId, string lang)
        {
            List<CaseTribunalFeedBackDetail> caseTribunalFeedBackDetails = new List<CaseTribunalFeedBackDetail>();
            try
            {
                caseTribunalFeedBackDetails = await (from ca in _context.CaseTribunalFeedBack
                                                     join lk in _context.CaseLookUp on ca.CaseLookUpId equals lk.CaseLookUpId
                                                     where (ca.CaseInvestigationId == CaseInvestigationId)
                                                     select new CaseTribunalFeedBackDetail
                                                     {
                                                         CaseInvestigationId = ca.CaseInvestigationId,
                                                         CaseLookUpId = ca.CaseLookUpId,
                                                         FeedBack = ca.FeedBack,
                                                         Id = ca.Id,
                                                         FeedBackType = (lang == "et") ? lk.DescriptionAmh : lk.DescriptionEng,
                                                         CreatedDate = EthiopicDateTime.GetEthiopicDate(ca.CreatedDateTime.Day, ca.CreatedDateTime.Month, ca.CreatedDateTime.Year),
                                                         CreatedDateTime = ca.CreatedDateTime,
                                                         //IsActive = ca.IsActive,
                                                         FeedBackTypeEng = lk.DescriptionEng,
                                                         //CreatedUserId = ca.CreatedUserId,
                                                         //IsDeleted = ca.IsDeleted,
                                                         UpdatedDateTime = ca.UpdatedDateTime,
                                                         //UpdatedUserId = ca.UpdatedUserId
                                                     }).OrderByDescending(x => x.Id).ToListAsync();
                return caseTribunalFeedBackDetails;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    public async Task<List<CaseTribunalFeedBackDetail>> GetCaseTribunalJudge(Guid CaseInvestigationId, string lang)
    {
      List<CaseTribunalFeedBackDetail> caseTribunalFeedBackDetails = new List<CaseTribunalFeedBackDetail>();
      try
      {
        caseTribunalFeedBackDetails = await (from ca in _context.CaseTribunalFeedBack
                                             join lk in _context.Users on ca.CreatedUserId.ToString() equals lk.Id

                                             where (ca.CaseInvestigationId == CaseInvestigationId && ca.CaseLookUpId==null)
                                             select new CaseTribunalFeedBackDetail
                                             {
                                               CaseInvestigationId = ca.CaseInvestigationId,
                                               FeedBack = ca.FeedBack,
                                               Id = ca.Id,
                                               CreatedDate = EthiopicDateTime.GetEthiopicDate(ca.CreatedDateTime.Day, ca.CreatedDateTime.Month, ca.CreatedDateTime.Year),
                                               CreatedDateTime = ca.CreatedDateTime,
                                               //IsActive = ca.IsActive,
                                               //CreatedUserId = ca.CreatedUserId,
                                               //IsDeleted = ca.IsDeleted,
                                               UpdatedDateTime = ca.UpdatedDateTime,
                                               FullName = lk.FullName
                                             }).OrderByDescending(x => x.Id).ToListAsync();
        return caseTribunalFeedBackDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }

  }
}
