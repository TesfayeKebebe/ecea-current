using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class LegalCaseRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public LegalCaseRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<LegalCaseDTO> Create(LegalCaseDTO postedLegalCase)
        {
            var caseApplication = await _context.CaseApplication.Where(ca => ca.CaseApplicationId == postedLegalCase.CaseApplicationId && ca.CaseInvestigation.CaseInvestigationId == postedLegalCase.CaseInvestigationId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            if (caseApplication == null)
                return null;
            DateTime dateTime = DateTime.Now;
            var legalCase = _mapper.Map<Case>(postedLegalCase);
            legalCase.CreatedDateTime = dateTime;
            legalCase.UpdatedDateTime = dateTime;
            legalCase.OpeningDate = dateTime;

            CAUnit cAUnit = new CAUnit
            {
                CAUnitId = Guid.NewGuid(),
                CreatedDateTime = dateTime,
                UpdatedDateTime = dateTime
            };

            legalCase.CaseId = cAUnit.CAUnitId;

            caseApplication.CaseStatusTypeId = 29;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(caseApplication).State = EntityState.Modified;
                    _context.CAUnit.Add(cAUnit);
                    _context.Case.Add(legalCase);

                    _context.SaveChanges();
                    transaction.Commit();
                    postedLegalCase.CaseId = legalCase.CaseId;
                    return postedLegalCase;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<bool> Update(LegalCaseDTO updatedLegalCase)
        {
            var oldLegalCase = await _context.Case.Where(ca => ca.CaseId == updatedLegalCase.CaseId && ca.CaseInvestigationId == updatedLegalCase.CaseInvestigationId && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            if (oldLegalCase == null)
                return false;

            DateTime dateTime = DateTime.Now;
            var modifiedLegalCase = _mapper.Map<Case>(updatedLegalCase);
            modifiedLegalCase.OpeningDate = oldLegalCase.OpeningDate;
            modifiedLegalCase.CreatedDateTime = oldLegalCase.CreatedDateTime;
            modifiedLegalCase.IsActive = oldLegalCase.IsActive;
            modifiedLegalCase.IsDeleted = oldLegalCase.IsDeleted;
            modifiedLegalCase.UpdatedDateTime = dateTime;
            modifiedLegalCase.CreatedUserId = oldLegalCase.CreatedUserId;
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(modifiedLegalCase).State = EntityState.Modified;

                    _context.SaveChanges();
                    transaction.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
        public async Task<LegalCaseDTO> GetLegalCaseById(Guid caseLegalCaseId)
        {
      try
      {
        LegalCaseDTO result = null;
        var caseLegalCase = await _context.Case.Where(u => u.CaseId == caseLegalCaseId && u.IsDeleted == false).Include(lc => lc.DecisionType)
            .AsNoTracking().FirstOrDefaultAsync();
        var caseappealDocument = await _context.CaseAppealDocument.Where(x => x.CaseId == caseLegalCaseId).OrderByDescending(c => c.CreatedDateTime).AsNoTracking().FirstOrDefaultAsync();
        if (caseLegalCase != null)
        {

          result = _mapper.Map<LegalCaseDTO>(caseLegalCase);
          result.OpeningDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(result.OpeningDate).ToString());
          result.ClosingDateET = result.ClosingDate != null ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(result.ClosingDate.Value).ToString()) : "";
          result.EthiopMinInjuctionDate = result.MinInjuctionDate != null ? (EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(result.MinInjuctionDate.Value).ToString())) : "";
          result.EthiopMaxInjuctionDate = result.MaxInjuctionDate != null ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(result.MaxInjuctionDate.Value).ToString()) : "";
          result.AttachmentContent = caseappealDocument?.AttachmentContent;
          result.FileName = caseappealDocument?.FileName;
        }
        return result;

      }
      catch (Exception ex)
      {

        throw  new Exception(ex.Message);
      }


        }
    public async Task<PagedResult<LegalCaseListModel>> SearchLegalCase(LegalCaseSearchCriteriaModel queryParameters)
        {
            try
            {
                var dataResult = await (from lc in _context.Case
                                        join inv in _context.CaseInvestigation
                                        on lc.CaseInvestigationId equals inv.CaseInvestigationId
                                        join ca in _context.CaseApplication
                                        on inv.CaseApplicationId equals ca.CaseApplicationId
                                        join cst in _context.CaseStatusType
                                        on ca.CaseStatusTypeId equals cst.CaseStatusTypeId
                                        join ct in _context.Lookup
                                          on ca.CaseTypeId equals ct.LookupId
                                        join dt in _context.Lookup
                                        on ca.DepartmentId equals dt.LookupId
               
                                        where (
                                          (queryParameters.CaseType == null || (ca.CaseTypeId == queryParameters.CaseType)) &&
                                          (queryParameters.StatusType == null || (ca.CaseStatusTypeId == queryParameters.StatusType)) &&
                                          (queryParameters.FileNo == null || (lc.FileNo.Contains(queryParameters.FileNo) )) &&
                                          (queryParameters.CaseName == null || (lc.CaseName.Contains(queryParameters.CaseName))) &&
                                          (queryParameters.From == null || lc.CreatedDateTime.Date >= DateTime.Parse(queryParameters.From)) &&
                                          (queryParameters.To == null || lc.CreatedDateTime.Date <= DateTime.Parse(queryParameters.To)) &&
                                          lc.IsDeleted == false && cst.StateCategoryId == 19
                                          )
                                        orderby (lc.CreatedDateTime) descending

                                        select new LegalCaseListModel
                                        {
                                            CaseId = lc.CaseId,
                                            CaseInvestigationId = inv.CaseInvestigationId,
                                            CaseStatusTypeId = ca.CaseStatusTypeId,
                                            CaseTypeId = ca.CaseTypeId,
                                            InvestigationFileNo = inv.FileNo,
                                            CaseName = lc.CaseName,
                                            ApplicationDate = ca.ApplicationDate,
                                            EventDate = ca.EventDate,
                                            ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.ApplicationDate).ToString()),
                                            EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.EventDate).ToString()),
                                            CaseApplicationId = ca.CaseApplicationId,
                                            CaseStatusCode = ca.CaseStatusType.TypeCode,
                                            CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                            CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                            Department = _mapper.Map<LookUpDisplayView>(dt),
                                            Description = ca.Description,
                                            PlaintIffPartyId = ca.PlaintiffId ,          
                                            SuspectPartyId = ca.SuspectId,
                                            CreatedDate = lc.CreatedDateTime,
                                            CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(lc.CreatedDateTime).ToString()),
                                            OpeningDate = lc.OpeningDate,
                                            OpeningDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(lc.OpeningDate).ToString()),
                                            FileNo = lc.FileNo,
                                            UpdatedDate = lc.UpdatedDateTime,
                                            UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(lc.UpdatedDateTime).ToString()),
                                        })
                                    .AsNoTracking()
                                    .ToListAsync();
        foreach (var data in dataResult)
        {
          var decision = _context.CaseTribunalFeedBack.Where(x => x.CaseInvestigationId == data.CaseInvestigationId && x.CaseLookUpId != null).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
          if(decision!=null)
          { var look = _context.CaseLookUp.FirstOrDefault(x => decision.CaseLookUpId == x.CaseLookUpId);
            data.DecisionAmh = look?.DescriptionAmh;
            data.DecisionEng = look?.DescriptionEng;
          }
          if (data.SuspectPartyId != null)
          {
            var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectPartyId);
            if (sus != null)
            {
              if (sus?.ExchangeActorId != null)
              {
                var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
                var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
                  data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
                  data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
                  data.SuspectOrganizationName = sueA?.OrganizationNameEng;
                  data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
              }
              else
              {
                data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
                data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
                data.SuspectOrganizationName = sus?.OrganizationNameEng;
                data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
              }

            }

          }
          else if (data.PlaintIffPartyId != null)
          {
            var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintIffPartyId);
            
            if (plaintIff.ExchangeActorId != null)
            {
              var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
              var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
              data.PlaintiffFullNameAmh =(sueA?.OrganizationNameAmh !=null)? sueA?.OrganizationNameAmh: seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
              data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
            }
            else
            {
              data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh!=null)? plaintIff?.OrganizationNameAmh: plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
              data.PlaintiffFullName = (plaintIff?.OrganizationNameEng!=null)? plaintIff?.OrganizationNameEng:plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
            }

          }
        }
               var dr = dataResult.Distinct(new ContentComparer()).ToList();
                var pendingInvestigations = await GetPendingInvestigations();
                pendingInvestigations = pendingInvestigations.Where(inv => (queryParameters.StatusType == null || (inv.CaseStatusTypeId == queryParameters.StatusType))).ToList();
                dr.AddRange(pendingInvestigations);

                dr.AsQueryable().Paging(queryParameters.PageSize, queryParameters.PageIndex);

                return new PagedResult<LegalCaseListModel>()
                {
                    Items = _mapper.Map<List<LegalCaseListModel>>(dr),
                    ItemsCount = dr.Count()
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<LegalCaseListModel>> GetPendingInvestigations()
        {
            var pendingInvestigations = await (from inv in _context.CaseInvestigation
                                               join ca in _context.CaseApplication
                                               on inv.CaseApplicationId equals ca.CaseApplicationId
                                               join cst in _context.CaseStatusType
                                              on ca.CaseStatusTypeId equals cst.CaseStatusTypeId
                                               join ct in _context.Lookup
                                                 on ca.CaseTypeId equals ct.LookupId
                                               join dt in _context.Lookup
                                               on ca.DepartmentId equals dt.LookupId
                                               where (ca.CaseStatusTypeId == 27)
                                               orderby (ca.CreatedDateTime) descending
                                               select new LegalCaseListModel
                                               {
                                                   CaseInvestigationId = inv.CaseInvestigationId,
                                                   CaseStatusTypeId = ca.CaseStatusTypeId,
                                                   CaseTypeId = ca.CaseTypeId,
                                                   InvestigationFileNo = inv.FileNo,
                                                   ApplicationDate = ca.ApplicationDate,
                                                   EventDate = ca.EventDate,
                                                   ApplicationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.ApplicationDate).ToString()),
                                                   EventDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.EventDate).ToString()),
                                                   CaseApplicationId = ca.CaseApplicationId,
                                                   CaseStatusCode = ca.CaseStatusType.TypeCode,
                                                   CaseStatusType = _mapper.Map<LookUpDisplayView>(cst),
                                                   CaseType = _mapper.Map<LookUpDisplayView>(ct),
                                                   CreatedDate = ca.CreatedDateTime,
                                                   CreatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.CreatedDateTime).ToString()),
                                                   Department = _mapper.Map<LookUpDisplayView>(dt),
                                                   Description = ca.Description,
                                                 PlaintIffPartyId= ca.PlaintiffId ,
                                                  
                                                  
                                                   //SuspectFullNameAmh = seALb.FirstNameAmh + " " + seALb.FatherNameAmh + " " + seALb.GrandFatherNameAmh,
                                                 //SuspectFullName = seALb.FirstNameEng + " " + seALb.FatherNameEng + " " + seALb.GrandFatherNameEng,
                                                 //SuspectOrganizationName = sueA.OrganizationNameEng,
                                                 //SuspectOrganizationNameAmh = sueA.OrganizationNameAmh,
                                                 SuspectPartyId = ca.SuspectId,
                                                 UpdatedDate = ca.UpdatedDateTime,
                                                   UpdatedDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ca.UpdatedDateTime).ToString()),
                                                   FileNo = "",
                                                   OpeningDate = DateTime.MinValue,
                                                   OpeningDateET = "" ,
                                                 
                                                  
                                               }).AsNoTracking().ToListAsync();
      foreach (var data in pendingInvestigations)
      {
        var decision = _context.CaseTribunalFeedBack.Where(x => x.CaseInvestigationId == data.CaseInvestigationId && x.CaseLookUpId != null).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault();
        if (decision != null)
        {
          var look = _context.CaseLookUp.FirstOrDefault(x => decision.CaseLookUpId == x.CaseLookUpId);
          data.DecisionAmh = look?.DescriptionAmh;
          data.DecisionEng = look?.DescriptionEng;
        }

        if (data.SuspectPartyId != null)
        {
          var sus = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.SuspectPartyId);
          if (sus != null)
          {
            if (sus?.ExchangeActorId != null)
            {
              var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == sus.ExchangeActorId).FirstOrDefaultAsync();
              var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == sus.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
              data.SuspectFullNameAmh = seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
              data.SuspectFullName = seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
              data.SuspectOrganizationName = sueA?.OrganizationNameEng;
              data.SuspectOrganizationNameAmh = sueA?.OrganizationNameAmh;
            }
            else
            {
              data.SuspectFullNameAmh = sus?.FirstNameAmh + " " + sus?.FatherNameAmh + " " + sus?.GrandFatherNameAmh;
              data.SuspectFullName = sus?.FirstNameEng + " " + sus?.FatherNameEng + " " + sus?.GrandFatherNameEng;
              data.SuspectOrganizationName = sus?.OrganizationNameEng;
              data.SuspectOrganizationNameAmh = sus?.OrganizationNameAmh;
            }

          }

        }
        else if (data.PlaintIffPartyId != null)
        {
          var plaintIff = _context.CaseParty.FirstOrDefault(x => x.CasePartyId == data.PlaintIffPartyId);

          if (plaintIff.ExchangeActorId != null)
          {
            var sueA = await _context.ExchangeActor.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).FirstOrDefaultAsync();
            var seALb = await _context.OwnerManager.Where(x => x.ExchangeActorId == plaintIff.ExchangeActorId).OrderByDescending(c => c.CreatedDateTime).FirstOrDefaultAsync();
            data.PlaintiffFullNameAmh = (sueA?.OrganizationNameAmh != null) ? sueA?.OrganizationNameAmh : seALb?.FirstNameAmh + " " + seALb?.FatherNameAmh + " " + seALb?.GrandFatherNameAmh;
            data.PlaintiffFullName = (sueA?.OrganizationNameEng != null) ? sueA?.OrganizationNameEng : seALb?.FirstNameEng + " " + seALb?.FatherNameEng + " " + seALb?.GrandFatherNameEng;
          }
          else
          {
            data.PlaintiffFullNameAmh = (plaintIff?.OrganizationNameAmh != null) ? plaintIff?.OrganizationNameAmh : plaintIff?.FirstNameAmh + " " + plaintIff?.FatherNameAmh + " " + plaintIff?.GrandFatherNameAmh;
            data.PlaintiffFullName = (plaintIff?.OrganizationNameEng != null) ? plaintIff?.OrganizationNameEng : plaintIff?.FirstNameEng + " " + plaintIff?.FatherNameEng + " " + plaintIff?.GrandFatherNameEng;
          }
        }
      }
      return pendingInvestigations;
        }

        public async Task<LegalCaseDTO> CreateAppeal(LegalCaseDTO postedLegalCase, Guid parentCaseId)
        {
            var caseApplication = await _context.CaseApplication.Where(ca => ca.CaseInvestigation.CaseInvestigationId == postedLegalCase.CaseInvestigationId && ca.CaseInvestigation.CaseInvestigationId == postedLegalCase.CaseInvestigationId && ca.CaseStatusTypeId == 30 && ca.IsDeleted == false).AsNoTracking().FirstOrDefaultAsync();
            if (caseApplication == null)
                return null;
            DateTime dateTime = DateTime.Now;
            var legalCase = _mapper.Map<Case>(postedLegalCase);
            legalCase.CreatedDateTime = dateTime;
            legalCase.UpdatedDateTime = dateTime;
            legalCase.OpeningDate = dateTime;

            legalCase.ParentCaseId = parentCaseId;

            CAUnit cAUnit = new CAUnit
            {
                CAUnitId = Guid.NewGuid(),
                CreatedDateTime = dateTime,
                UpdatedDateTime = dateTime
            };

            legalCase.CaseId = cAUnit.CAUnitId;

            caseApplication.CaseStatusTypeId = 34;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.Entry(caseApplication).State = EntityState.Modified;
                    _context.CAUnit.Add(cAUnit);
                    _context.Case.Add(legalCase);

                    _context.SaveChanges();
                    transaction.Commit();
                    postedLegalCase.CaseId = legalCase.CaseId;
                    return postedLegalCase;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
    }

    public class ContentComparer : IEqualityComparer<LegalCaseListModel>
    {
        public bool Equals(LegalCaseListModel x, LegalCaseListModel y)
        {
            return x.FileNo == y.FileNo;
        }

        public int GetHashCode(LegalCaseListModel obj)
        {
            return obj.FileNo.GetHashCode();
        }
    }
}
