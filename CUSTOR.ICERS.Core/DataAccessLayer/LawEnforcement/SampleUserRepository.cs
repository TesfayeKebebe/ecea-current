using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.LawEnforcement
{
    public class SampleUserRepository
    {
        private readonly ECEADbContext _context;
        public SampleUserRepository(ECEADbContext context)
        {
            _context = context;
        }
        //public async Task<IEnumerable<SampleUserDTO>> GetAllUsers(string lang="et")
        //{
        //    var sampleUsers = await _context.SampleUser
        //        .Include(u=>u.Department)
        //        .Include(u=>u.SampleUserRole).ThenInclude(x=>x.SampleRole)
        //        .AsNoTracking().ToListAsync();
        //    var users = getMappedUsers(sampleUsers, lang);
        //    return users;
        //}
        public async Task<IEnumerable<SampleUserDTO>> GetUsersByRole(string roleName, string lang = "et")
        {
            try
            {
                List<SampleUserDTO> users = new List<SampleUserDTO>();
                var roles = _context.Roles.FirstOrDefault(x => x.Name == roleName);
                if (roles != null)
                {
                    var usersByRole = _context.UserRoles.Where(x => x.RoleId == roles.Id);
                    foreach (var userRol in usersByRole)
                    {
                        var usersData = _context.Users.FirstOrDefault(x => x.Id == userRol.UserId);
                        var user = new SampleUserDTO
                        {
                            //DepartmentId =1 ,
                            //  DepartmentName = (lang == "et") ? u.Department.DescriptionAmh : u.Department.DescriptionEng,
                            DisplayName = usersData?.FullName,
                            UserId = usersData?.Id
                        };


                        var role = new SampleRoleDTO
                        {
                            RoleId = roles.Id,
                            RoleName = roles.Name,
                            Description = "",
                        };
                        user.Roles.Add(role);


                        users.Add(user);

                    }
                }


                //var sampleUsers = await _context.SampleUser
                //    .Where(u => u.SampleUserRole.Where(ur => ur.SampleRole.RoleName ==roleName).FirstOrDefault().SampleUserId==u.SampleUserId)
                //    .Include(u => u.Department)
                //    .Include(u => u.SampleUserRole).ThenInclude(x => x.SampleRole)
                //    .AsNoTracking().ToListAsync();
                //var users = getMappedUsers(sampleUsers, lang);
                return users;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        //public List<SampleUserDTO> getMappedUsers(List<SampleUser> sampleUsers,string lang = "et")
        //{
        //    List<SampleUserDTO> users = new List<SampleUserDTO>();
        //    //foreach (var u in sampleUsers)
        //    //{
        //    //    var user = new SampleUserDTO
        //    //    {
        //    //        DepartmentId = u.DepartmentId,
        //    //        DepartmentName = (lang == "et") ? u.Department.DescriptionAmh : u.Department.DescriptionEng,
        //    //        DisplayName = (lang == "et") ? u.FirstNameAmh+" "+u.FatherNameAmh+" "+u.GrandFatherNameAmh: u.FirstNameEng + " " + u.FatherNameEng + " " + u.GrandFatherNameEng,
        //    //        UserId = u.SampleUserId
        //    //    };
        //    //    var roles = "";
        //    //    foreach (var r in u.SampleUserRole)
        //    //    {
        //    //        var role = new SampleRoleDTO
        //    //        {
        //    //            RoleId = r.SampleRole.SampleRoleId,
        //    //            RoleName = r.SampleRole.RoleName,
        //    //            Description = (lang == "et") ? r.SampleRole.DescriptionAmh : r.SampleRole.DescriptionEng,
        //    //        };
        //    //        user.Roles.Add(role);
        //    //    }
        //    //    var roleNames = user.Roles.Select(ur => ur.Description);
        //    //    roles = string.Join('|', roleNames);
        //    //    user.DisplayName = user.DisplayName + " (" + user.DepartmentName + ") " + "[" + roles + "]";
        //    //    users.Add(user);

        //    //}
        //    return users;
        //}
    }
}
