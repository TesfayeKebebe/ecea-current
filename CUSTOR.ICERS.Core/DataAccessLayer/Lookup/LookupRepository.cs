using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Lookup
{
    public class LookupRepository
    {
        private readonly ECEADbContext _context;
        private readonly IDistributedCache _distributedCache;
        private readonly Settings _settings;
        private readonly IMapper _mapper;

        public LookupRepository(ECEADbContext context, IDistributedCache distributedCache, IConfiguration configuration, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _settings = new Settings(configuration);
            _distributedCache = distributedCache;
        }


        // Get lookup data from the cache if it exist otherwise it get
        // from the lookup type table and store in the cache for feature use
        public async Task<IEnumerable<LookupDTO>> GetLookups(string lang, int lookupTypeId)
        {

            IEnumerable<LookupDTO> lookups = null;
            string cacheKey = "Lookups:" + lookupTypeId + lang;

            try
            {
                var cachedLookups = await _distributedCache.GetStringAsync(cacheKey);
                if (cachedLookups != null)
                {
                    lookups = JsonConvert.DeserializeObject<IEnumerable<LookupDTO>>(cachedLookups);
                }
                else
                {
                    lookups = await _context.Lookup
                        .Where(l => l.LookupTypeId == lookupTypeId && l.IsActive == true && l.IsDeleted == false).OrderBy(x=>x.LookupId)
                        .Select(l => new LookupDTO
                        {
                            Id = l.LookupId,
                            Description = (lang == "et") ? l.DescriptionAmh : l.DescriptionEng
                        }).AsNoTracking().ToListAsync();

                    DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));
                    await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(lookups),
                        cacheOptions);
                }

                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<bool> SaveLookup(LookupDTO4 updatedSetting)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    var oldSetting = await _context.Lookup.FirstOrDefaultAsync(clb => clb.LookupTypeId == updatedSetting.LookupTypeId && clb.LookupId == updatedSetting.LookupId);

                    if (oldSetting == null)
                    {

                        var newCommodity = _mapper.Map<EntityLayer.Lookup.Lookup>(updatedSetting);

                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.IsDeleted = oldSetting.IsDeleted;
                        updatedSetting.IsActive = oldSetting.IsActive;
                        updatedSetting.CreatedUserId = oldSetting.CreatedUserId;
                        updatedSetting.UpdatedDateTime = DateTime.Now;
                        updatedSetting.CreatedDateTime = oldSetting.CreatedDateTime;
                        var updateReportPeriod = _mapper.Map(updatedSetting, oldSetting);
                        _context.Entry(updateReportPeriod).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }
        public async Task<bool> SaveLookupType(LookupTypeDTO updatedSetting)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    var oldLookup = await _context.LookupType.FirstOrDefaultAsync(clb => clb.LookupTypeId == updatedSetting.LookupTypeId);
                    if (oldLookup == null)
                    {
                        var newCommodity = _mapper.Map<LookupType>(updatedSetting);
                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.IsDeleted = oldLookup.IsDeleted;
                        updatedSetting.IsActive = oldLookup.IsActive;
                        updatedSetting.CreatedUserId = oldLookup.CreatedUserId;
                        updatedSetting.UpdatedDateTime = DateTime.Now;
                        updatedSetting.CreatedDateTime = oldLookup.CreatedDateTime;

                        oldLookup = _mapper.Map(updatedSetting, oldLookup);
                        _context.Entry(oldLookup).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        public async Task<List<LookupDTO4>> GetLookupsById(int lookupTypeId)
        {


            try
            {

                var lookups = await _context.Lookup
                        .Where(l => l.LookupTypeId == lookupTypeId && l.IsDeleted == false).OrderBy(x => x.DescriptionEng)
                        .Select(l => new LookupDTO4
                        {
                            LookupId = l.LookupId,
                            DescriptionEng = l.DescriptionEng,
                            DescriptionAmh = l.DescriptionAmh
                        }).AsNoTracking().ToListAsync();

                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        // get lookup data using guid
        //public async Task<IEnumerable<LookupDTO>> GetLookupData(string lang, Guid lookupTypeId)
        //{

        //    IEnumerable<LookupDTO> lookups = null;
        //    string cacheKey = "Lookups:" + lookupTypeId + lang;

        //    try
        //    {
        //        var cachedLookups = await _distributedCache.GetStringAsync(cacheKey);
        //        if (cachedLookups != null)
        //        {
        //            lookups = JsonConvert.DeserializeObject<IEnumerable<LookupDTO>>(cachedLookups);
        //        }
        //        else
        //        {
        //            lookups = await _context.Lookup
        //                .Where(l => l.LookupTypeId == lookupTypeId && l.IsActive == true && l.IsDeleted == false)
        //                .Select(l => new LookupDTO
        //                {
        //                    Id = l.LookupId,
        //                    Description = (lang == "et") ? l.DescriptionAmh : l.DescriptionEng
        //                }).AsNoTracking().ToListAsync();

        //            DistributedCacheEntryOptions cacheOptions = new DistributedCacheEntryOptions()
        //                .SetAbsoluteExpiration(TimeSpan.FromMinutes(_settings.ExpirationPeriod));
        //            await _distributedCache.SetStringAsync(cacheKey, JsonConvert.SerializeObject(lookups),
        //                cacheOptions);
        //        }

        //        return lookups;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //}

        public async Task<List<LookupTypeDTO>> GetAllLookupTypeData()
        {


            try
            {

                var lookups = await _context.LookupType.Where(l => l.IsDeleted == false).OrderBy(x => x.DescriptionEng)
                  .Select(n => new LookupTypeDTO
                  {
                      LookupTypeId = n.LookupTypeId,
                      DescriptionAmh = n.DescriptionAmh,
                      DescriptionEng = n.DescriptionEng,
                  })
                  .AsNoTracking()
                  .ToListAsync();




                return lookups;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> DeleteLookUpValue(int lookupid)
        {
            try
            {
                var oldInjunction = await _context.Lookup.FirstOrDefaultAsync(lo =>
                    lo.LookupId == lookupid);
                if (oldInjunction != null)
                {
                    oldInjunction.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteLookUpType(int lookuptypeid)
        {
            try
            {
                var oldInjunction = await _context.LookupType.FirstOrDefaultAsync(lo =>
                    lo.LookupTypeId == lookuptypeid);
                if (oldInjunction != null)
                {
                    oldInjunction.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
