﻿using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Lookup
{
    public class NationalitiesRepository
    {
        private readonly ECEADbContext _context;

        public NationalitiesRepository(ECEADbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<NationalityDTO>> GetNationality(string lang)
        {
            IEnumerable<NationalityDTO> nations = null;

            try
            {
                nations = await _context.Nationality
                    .Select(n => new NationalityDTO
                    {
                        Id = n.CountryId,
                        Description = lang == "et" ? n.CountryNameAmh : n.CountryNameEng
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return nations;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
