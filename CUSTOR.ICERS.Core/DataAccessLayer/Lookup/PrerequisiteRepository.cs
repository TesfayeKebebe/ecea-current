﻿using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Lookup
{
    public class PrerequisiteRepository
    {
        private readonly ECEADbContext _context;

        public PrerequisiteRepository(ECEADbContext context)
        {
            _context = context;
        }

        public async Task<Prerequisite> SavePrerequisite(Prerequisite postedPrerequisite)
        {
            try
            {
                _context.Prerequisite.Add(postedPrerequisite);
                await _context.SaveChangesAsync();
                return postedPrerequisite;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public PagedResult<Prerequisite> GetAllPrerequisites(QueryParameters queryParameters)
        {
            try
            {
                var prerequisites = _context.Prerequisite.Paging(queryParameters.PageCount, queryParameters.PageNumber).ToList();

                return new PagedResult<Prerequisite>()
                {
                    Items = prerequisites,
                    ItemsCount = _context.Prerequisite.Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public PagedResult<ServicePrerequisteDTO> GetAllPrerequisiteByService(ServicePrerequisiteQueryParameters queryParameters)
        {
            try
            {
                var servicePrerequiste = (from servicePrerequisite in _context.ServicePrerequisite
                                          join prerequisite in _context.Prerequisite on servicePrerequisite.PrerequisiteId equals prerequisite.PrerequisiteId
                                          where servicePrerequisite.ServiceId == queryParameters.ServiceGuid
                                                && (servicePrerequisite.CustomerTypeId == queryParameters.ExchangeActorType)
                                          select new ServicePrerequisteDTO
                                          {
                                              PrerequisiteId = servicePrerequisite.PrerequisiteId,
                                              ServicePrerequisiteId = servicePrerequisite.ServicePrerequisiteId,
                                              DescriptionAmh = prerequisite.DescriptionAmh,
                                              DescriptionEng = prerequisite.DescriptionEng,
                                              IsRequired = servicePrerequisite.IsRequired,
                                              IsDocument = servicePrerequisite.IsDocument,
                                              IsActive = servicePrerequisite.IsActive.GetValueOrDefault()
                                          }).Paging(queryParameters.PageCount, queryParameters.PageNumber).ToList();

                return new PagedResult<ServicePrerequisteDTO>()
                {
                    Items = servicePrerequiste,
                    ItemsCount = (from servicePrerequisite in _context.ServicePrerequisite
                                  join prerequisite in _context.Prerequisite on servicePrerequisite.PrerequisiteId equals prerequisite.PrerequisiteId
                                  where servicePrerequisite.ServiceId == queryParameters.ServiceGuid
                                        && (servicePrerequisite.CustomerTypeId == queryParameters.ExchangeActorType)
                                  select new ServicePrerequisteDTO
                                  {
                                      ServicePrerequisiteId = servicePrerequisite.ServicePrerequisiteId
                                  }).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public PagedResult<Prerequisite> GetAllPrerequisitesNotAssighendToService(ServicePrerequisiteQueryParameters queryParameters)
        {
            try
            {
                var prerequisites = (from prerequisite in _context.Prerequisite
                                     where !_context.ServicePrerequisite.Any(sp => sp.PrerequisiteId == prerequisite.PrerequisiteId
                                                                               && sp.CustomerTypeId == queryParameters.ExchangeActorType
                                                                               && sp.ServiceId == queryParameters.ServiceGuid)
                                     select new Prerequisite
                                     {
                                         PrerequisiteId = prerequisite.PrerequisiteId,
                                         DescriptionAmh = prerequisite.DescriptionAmh,
                                         DescriptionEng = prerequisite.DescriptionEng,
                                         //IsRequired = prerequisite.IsActive,
                                         //IsDocument = prerequisite.IsDocument
                                     }).Paging(queryParameters.PageCount, queryParameters.PageNumber).ToList();
                return new PagedResult<Prerequisite>()
                {
                    Items = prerequisites,
                    ItemsCount = (from prerequisite in _context.Prerequisite
                                  where !_context.ServicePrerequisite.Any(sp => sp.PrerequisiteId == prerequisite.PrerequisiteId
                                                                            && sp.CustomerTypeId == queryParameters.ExchangeActorType
                                                                            && sp.ServiceId == queryParameters.ServiceGuid)
                                  select new Prerequisite
                                  {
                                      PrerequisiteId = prerequisite.PrerequisiteId,
                                      DescriptionAmh = prerequisite.DescriptionAmh,
                                      DescriptionEng = prerequisite.DescriptionEng,
                                  }).Count()
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<Prerequisite> GetPrerequisite(int prerequisiteId)
        {
            Prerequisite prerequisite = null;
            try
            {
                prerequisite = await _context.Prerequisite
                   .FirstOrDefaultAsync(p => p.PrerequisiteId == prerequisiteId);

                return prerequisite;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<Prerequisite> UpdatePrerequisite(Prerequisite postedPrerequisite)
        {
            var preRequisteToUpdate = _context.Prerequisite.FirstOrDefault(p => p.PrerequisiteId == postedPrerequisite.PrerequisiteId);
            try
            {
                preRequisteToUpdate.DescriptionAmh = postedPrerequisite.DescriptionAmh;
                preRequisteToUpdate.DescriptionEng = postedPrerequisite.DescriptionEng;
                preRequisteToUpdate.UpdatedDateTime = DateTime.Now;

                _context.Entry(preRequisteToUpdate).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return preRequisteToUpdate;
        }

        public async Task<bool> DeletePrerequisite(int PrerequisiteId)
        {
            var prerequisite = await _context.Prerequisite.FindAsync(PrerequisiteId);
            if (prerequisite == null)
            {
                return false;
            }
            // delete it's related table data
            _context.Prerequisite.Remove(prerequisite);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
