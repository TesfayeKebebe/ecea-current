﻿using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Registration
{
    public class MemberProductRepository
    {
        private readonly ECEADbContext _context;

        public MemberProductRepository(ECEADbContext context)
        {
            _context = context;
        }

        public async Task<bool> DeleteMemberProduct(Guid ExchangeActorId)
        {
            try
            {
                List<MemberProduct> memberProductList = await _context.MemberProduct
                .Where(mp => mp.ExchangeActorId == ExchangeActorId)
                .AsNoTracking()
                .ToListAsync();

                foreach (MemberProduct memberProduct in memberProductList)
                {
                    var deletedMemberPorudct = new MemberProduct { MemberProductId = memberProduct.MemberProductId };
                    _context.Remove(deletedMemberPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<bool> DeleteCustomerBussinessField(Guid ExchangeActorId)
        {
            try
            {
                List<CustomerBussinessFiled> customerBussinessesFiledList = await _context.CustomerBussinessFiled
                .Where(mp => mp.ExchangeActorId == ExchangeActorId)
                .AsNoTracking()
                .ToListAsync();

                foreach (CustomerBussinessFiled bussinessFiled in customerBussinessesFiledList)
                {
                    var deletedMemberPorudct = new CustomerBussinessFiled { CustomerBusinessFildeId = bussinessFiled.CustomerBusinessFildeId };
                    _context.Remove(deletedMemberPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<bool> DeleteClientProduct(int memberClientInformationId)
        {
            try
            {
                List<ClientProduct> ClientProductList = await _context.ClientProduct
                .Where(mp => mp.MemberClientInformationId == memberClientInformationId)
                .AsNoTracking()
                .ToListAsync();

                foreach (ClientProduct clientProduct in ClientProductList)
                {
                    var deletedClientPorudct = new ClientProduct { ClientProductId = clientProduct.ClientProductId };
                    _context.Remove(deletedClientPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

    }
}
