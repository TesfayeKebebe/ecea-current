﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
//using DevExpress.XtraRichEdit.Commands.Internal;
//using DevExpress.XtraRichEdit.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Notification
{
    public class NotificationRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public NotificationRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<PagedResult<NotificationViewModel>> GetNotifiation(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;


            if (queryParameters.WorkFlowUserRoleId == 0 || queryParameters.WorkFlowUserRoleId == 3)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                else if (queryParameters.WorkFlowUserRoleId == 3)
                {
                    approvalSatus = 2;
                }

                return await OneDirectionFlow(queryParameters, approvalSatus);

            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionFlow(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }
        private async Task<PagedResult<NotificationViewModel>> OneDirectionFlow(NotificationQueryParameters queryParameters, int approvalStatus)
        {


            var userNotification = await (from sa in _context.ServiceApplication
                     join om in _context.OwnerManager on new { sa.ExchangeActorId, IsManager = true } equals new { om.ExchangeActorId, om.IsManager } into ownMan
                     from ownerManager in ownMan.DefaultIfEmpty()
                     where sa.ApprovalStatus == approvalStatus && sa.ApprovalFinished == false
                     orderby sa.CreatedDateTime descending
                     select new NotificationViewModel
                     {
                         PreparedDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime).ToString()) : sa.UpdatedDateTime.ToString("MMMM dd, yyyy"),
                         ServiceTypeId = sa.ServiceId,
                         ServiceApplicationId = sa.ServiceApplicationId,
                         ApprovalStatus = sa.ApprovalStatus.GetValueOrDefault(),
                         StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(sa.CreatedDateTime).ToString()) : sa.CreatedDateTime.ToString("MMMM dd, yyyy"),
                         CustomerTypeId = sa.CustomerTypeId,
                         MangagerName = queryParameters.Lang == "et" ? (ownerManager.FirstNameAmh + " " + ownerManager.FatherNameAmh + " " + ownerManager.GrandFatherNameAmh) :
                        (ownerManager.FirstNameEng + " " + ownerManager.FatherNameEng + " " + ownerManager.GrandFatherNameEng),
                         OrganizationName = sa.ExchangeActor.OrganizationNameAmh
                     }).Paging(queryParameters.PageCount, queryParameters.PageNumber)
                       .AsNoTracking()
                       .ToListAsync();


                    return new PagedResult<NotificationViewModel>()
                    {
                        Items = _mapper.Map<List<NotificationViewModel>>(userNotification),
                        ItemsCount = _context.ServiceApplication.Where(sa => sa.ApprovalStatus == approvalStatus && sa.ApprovalFinished == false).Count()
                    };

            //try
            //{
            //    var userNotification = await _context.ServiceApplication
            //        .Where(sa => sa.ApprovalStatus == approvalStatus && sa.ApprovalFinished == false && (sa.ExchangeActor.OwnerManager.IsManager == true || sa.ExchangeActor.OwnerManager.IsEceaContact == true))
            //        .OrderByDescending(sa => sa.CreatedDateTime)
            //        .Select(data => new NotificationViewModel
            //        {
            //            PreparedDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString()) : data.UpdatedDateTime.ToString("MMMM dd, yyyy"),
            //            ServiceTypeId = data.ServiceId,
            //            ServiceApplicationId = data.ServiceApplicationId,
            //            ApprovalStatus = data.ApprovalStatus.GetValueOrDefault(),
            //            StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString()) : data.CreatedDateTime.ToString("MMMM dd, yyyy"),
            //            CustomerTypeId = data.CustomerTypeId,
            //            MangagerName = queryParameters.Lang == "et" ?  (data.ExchangeActor.OwnerManager.FirstNameAmh + " " + data.ExchangeActor.OwnerManager.FatherNameAmh + " " + data.ExchangeActor.OwnerManager.GrandFatherNameAmh) :
            //            (data.ExchangeActor.OwnerManager.FirstNameEng + " " + data.ExchangeActor.OwnerManager.FatherNameEng + " " + data.ExchangeActor.OwnerManager.GrandFatherNameEng),
            //            OrganizationName = data.ExchangeActor.OrganizationNameAmh
            //        })
            //        .Paging(queryParameters.PageCount, queryParameters.PageNumber).AsNoTracking().ToListAsync();

            //    return new PagedResult<NotificationViewModel>()
            //    {
            //        Items = _mapper.Map<List<NotificationViewModel>>(userNotification),
            //        ItemsCount = _context.ServiceApplication.Where(sa => sa.ApprovalStatus == approvalStatus && sa.ApprovalFinished == false).Count()
            //    };
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.InnerException.ToString());
            //}
        }

        public async Task<List<PendingInjunctionViewDTO>> GetInjunctionNotifications(string lang, int userRoleId)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;
            if (userRoleId == 0)
            {
                if (userRoleId == 0)
                {
                    approvalSatus = 4;
                }
                return await OneDirectionInjunctionFlow(lang, approvalSatus);

            }
            else
            {
                if (userRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (userRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }
                else if (userRoleId == 3)
                {
                    approvalSatus = 2;
                }

                return await TwoDirectionInjunctionFlow(lang, approvalSatus, approvalStatusTwo);
            }

        }

        public async Task<PagedResult<PendingInjunctionViewDTO>> GetInjunctionsTobeLifted(InjunctionQueryParameters parameters)
        {
            try
            {
                int totalSearchResult = 0;
                List<PendingInjunctionViewDTO> InjunctionList = null;
                InjunctionList = await(from ij in _context.Injunction
                                       join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                       join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                       join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                       join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                       where ij.IsDeleted == false && ij.InjunctionStatus == 0 
                                       && (ij.ApprovalDecision == 0 || ij.ApprovalDecision == 5 || ij.ApprovalDecision == 6)
                                       &&((ij.InjunctionEndDate - ij.InjunctionStartDate).TotalDays<15
)
                                       orderby ij.InjunctionId descending
                                       select new PendingInjunctionViewDTO
                                       {
                                           InjunctionId = ij.InjunctionId,
                                           InjunctionBodyId = (parameters.Lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                           InjunctionLetterNo = ij.InjunctionLetterNo,
                                           ExchangeActorId = ij.ExchangeActorId,
                                           InjunctionStatus = ij.InjunctionStatus,
                                           Reason = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                           SpecialReason = ij.SpecialReason,
                                           ExchangeActor = ij.ExchangeActor,
                                           InjunctionEndDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                           InjunctionStartDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                       })
                                 .ToListAsync();

                totalSearchResult = InjunctionList.Count();
                var pagedResult = InjunctionList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);
                return new PagedResult<PendingInjunctionViewDTO>()
                {
                    Items = _mapper.Map<List<PendingInjunctionViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<List<PendingInjunctionViewDTO>> TwoDirectionInjunctionFlow(string lang, int approvalSatus, int approvalStatusTwo)
        {
            try
            {

                var data = await (from injunction in _context.Injunction
                                      //  join _workflow in _context.WorkFlow on injunction.InjunctionId equals _workflow.InjunctionId
                                  join _lookup in _context.Lookup on injunction.InjunctionBodyId equals _lookup.LookupId
                                  join _lukup in _context.Lookup on injunction.Reason equals _lukup.LookupId
                                  where injunction.ApprovalDecision == approvalSatus || injunction.ApprovalDecision == approvalStatusTwo
                                  select new PendingInjunctionViewDTO
                                  {
                                      ExchangeActor = injunction.ExchangeActor,
                                      InjunctionId = injunction.InjunctionId,
                                      SpecialReason = injunction.SpecialReason,
                                      InjunctionLetterNo = injunction.InjunctionLetterNo,
                                      InjunctionStatus = injunction.InjunctionStatus,
                                      InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(injunction.InjunctionEndDate).ToString()) : injunction.InjunctionEndDate.ToString(),
                                      InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(injunction.InjunctionStartDate).ToString()) : injunction.InjunctionStartDate.ToString(),
                                      Reason = _lukup.DescriptionAmh,
                                      CurrentStep = injunction.ApprovalDecision,
                                      InjunctionBodyId = _lukup.DescriptionAmh,
                                      ExchangeActorId = injunction.ExchangeActor.ExchangeActorId,
                                  })
                                  .AsNoTracking().ToListAsync();
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<List<PendingInjunctionViewDTO>> OneDirectionInjunctionFlow(string lang, int approvalSatus)
        {
            try
            {
                var InjunctionData =
                   await (from injunction in _context.Injunction
                          join lukup in _context.Lookup on injunction.InjunctionBodyId equals lukup.LookupId
                          join _lookup in _context.Lookup on injunction.Reason equals _lookup.LookupId
                          // join flow in _context.WorkFlow on injunction.InjunctionId equals flow.InjunctionId
                          where injunction.ApprovalDecision == approvalSatus
                          select new PendingInjunctionViewDTO
                          {
                              InjunctionBodyId = lukup.DescriptionAmh,
                              ExchangeActor = injunction.ExchangeActor,
                              InjunctionId = injunction.InjunctionId,
                              Reason = _lookup.DescriptionAmh,
                              SpecialReason = injunction.SpecialReason,
                              InjunctionLetterNo = injunction.InjunctionLetterNo,
                              InjunctionStatus = injunction.InjunctionStatus,
                              CurrentStep = injunction.ApprovalDecision,
                              InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(injunction.InjunctionEndDate).ToString()) : injunction.InjunctionEndDate.ToString(),
                              InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(injunction.InjunctionStartDate).ToString()) : injunction.InjunctionStartDate.ToString(),

                          }).Distinct().AsNoTracking().ToListAsync();

                return InjunctionData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<PagedResult<NotificationViewModel>> TwoDirectionFlow(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {

            var userNotification = await (from sa in _context.ServiceApplication
                     join om in _context.OwnerManager on new {sa.ExchangeActorId, IsManager = true } equals new { om.ExchangeActorId, om.IsManager} into ownMan
                     from ownerManager in ownMan.DefaultIfEmpty()
                     where (sa.ApprovalStatus == approvalStatusForward || sa.ApprovalStatus == approvalStatusBackward) && sa.ApprovalFinished == false
                     orderby sa.CreatedDateTime descending
                     select new NotificationViewModel
                     {

                         PreparedDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime).ToString()) : sa.UpdatedDateTime.ToString("MMMM dd, yyyy"),
                         ServiceTypeId = sa.ServiceId,
                         ServiceApplicationId = sa.ServiceApplicationId,
                         ApprovalStatus = sa.ApprovalStatus.GetValueOrDefault(),
                         StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(sa.CreatedDateTime).ToString()) : sa.CreatedDateTime.ToString("MMMM dd, yyyy"),
                         CustomerTypeId = sa.CustomerTypeId,
                         MangagerName = queryParameters.Lang == "et" ? (ownerManager.FirstNameAmh + " " + ownerManager.FatherNameAmh + " " + ownerManager.GrandFatherNameAmh) :
                         (ownerManager.FirstNameEng + " " + ownerManager.FatherNameEng + " " + ownerManager.GrandFatherNameEng),
                         OrganizationName = sa.ExchangeActor.OrganizationNameAmh

                     }).Paging(queryParameters.PageCount, queryParameters.PageNumber)
                       .AsNoTracking()
                       .ToListAsync();


                    return new PagedResult<NotificationViewModel>()
                    {
                        Items = _mapper.Map<List<NotificationViewModel>>(userNotification),
                        ItemsCount = _context.ServiceApplication.Where(sa => (sa.ApprovalStatus == approvalStatusForward || sa.ApprovalStatus == approvalStatusBackward) && sa.ApprovalFinished == false).Count()
                    };

            //try
            //{
            //    var userNotification = await _context.ServiceApplication
            //        .Where(sa => (sa.ApprovalStatus == approvalStatusForward || sa.ApprovalStatus == approvalStatusBackward) && sa.ApprovalFinished == false && (sa.ExchangeActor.OwnerManager.IsManager == true || sa.ExchangeActor.OwnerManager.IsEceaContact == true))
            //        .OrderByDescending(sa => sa.CreatedDateTime)
            //        .Select(data => new NotificationViewModel
            //        {
            //            PreparedDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.UpdatedDateTime).ToString()) : data.UpdatedDateTime.ToString("MMMM dd, yyyy"),
            //            ServiceTypeId = data.ServiceId,
            //            ServiceApplicationId = data.ServiceApplicationId,
            //            ApprovalStatus = data.ApprovalStatus.GetValueOrDefault(),
            //            StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString()) : data.CreatedDateTime.ToString("MMMM dd, yyyy"),
            //            CustomerTypeId = data.CustomerTypeId,
            //            MangagerName = queryParameters.Lang == "et" ? (data.ExchangeActor.OwnerManager.FirstNameAmh + " " + data.ExchangeActor.OwnerManager.FatherNameAmh + " " + data.ExchangeActor.OwnerManager.GrandFatherNameAmh) :
            //            (data.ExchangeActor.OwnerManager.FirstNameEng + " " + data.ExchangeActor.OwnerManager.FatherNameEng + " " + data.ExchangeActor.OwnerManager.GrandFatherNameEng),
            //            OrganizationName = data.ExchangeActor.OrganizationNameAmh
            //        })
            //        .Paging(queryParameters.PageCount, queryParameters.PageNumber).AsNoTracking().ToListAsync();

            //    return new PagedResult<NotificationViewModel>()
            //    {
            //        Items = _mapper.Map<List<NotificationViewModel>>(userNotification),
            //        ItemsCount = _context.ServiceApplication.Where(sa => (sa.ApprovalStatus == approvalStatusForward || sa.ApprovalStatus == approvalStatusBackward) && sa.ApprovalFinished == false).Count()
            //    };

            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.InnerException.ToString());
            //}
        }

        public async Task<PagedResult<MemberTradeViolationView>> GetNotifiationTradeExecution(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;
             if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                return await lastDirectionFlowTradeExecution(queryParameters, approvalSatus);
            }

            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionFlowTradeExecution(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }

        public async Task<PagedResult<MemberTradeViolationView>> lastDirectionFlowTradeExecution(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var violatedExchangeActor = await _context.TradeExcutionViolationRecord
                                .Where(x => x.MemberTradeViolation.ViolationStatus == 0 &&
                                            x.Status == approvalStatus)
                                 .Select(n => new MemberTradeViolationView
                                 {
                                     MemberTradeViolationId = n.MemberTradeViolationId,
                                     ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,

                                     CustomerType = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionEng,
                                     OrganizationName = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                                                     n.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                                     ApprovalStatus = n.Status,
                                     StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) : n.CreatedDateTime.Date.ToString()
                                 })
                                .AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageCount)
                                                     .AsNoTracking()
                                                     .ToListAsync();
                totalResultList = violatedExchangeActor.Count();

                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(violatedExchangeActor),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberTradeViolationView>> OneDirectionFlowTradeExecution(NotificationQueryParameters queryParameters, int approvalStatus, int directorStatus)
        {
            int totalResultList = 0;
            try
            {
                var violatedExchangeActor = await _context.TradeExcutionViolationRecord
                .Where(x => x.MemberTradeViolation.ViolationStatus == 0 &&
                                               (x.MemberTradeViolation.Status == approvalStatus ||
                                                x.MemberTradeViolation.Status == directorStatus))
                 .Select(n => new MemberTradeViolationView
                 {
                     MemberTradeViolationId = n.MemberTradeViolationId,
                     ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,

                     CustomerType = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionEng,
                     OrganizationName = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                                     n.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                     ApprovalStatus = n.MemberTradeViolation.Status.GetValueOrDefault(),
                     StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) : n.CreatedDateTime.Date.ToString()
                 })
                //.AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageCount)
                                     .AsNoTracking()
                                     .ToListAsync();
                totalResultList = violatedExchangeActor.Count();

                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(violatedExchangeActor),
                    ItemsCount = totalResultList
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberTradeViolationView>> TwoDirectionFlowTradeExecution(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {
            int totalResultList = 0;
            try
            {
                
                var violatedExchangeActor = await _context.TradeExcutionViolationRecord
                 .Where(x => (x.Status == approvalStatusForward ||
                             x.Status == approvalStatusBackward) &&
                             x.MemberTradeViolation.ViolationStatus == 0
                             )
                  .Select(n => new MemberTradeViolationView
                  {
                      MemberTradeViolationId = n.MemberTradeViolationId,
                      ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,

                      CustomerType = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                     n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionEng,
                      OrganizationName = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          n.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                     ApprovalStatus = n.Status,
                     StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) : n.CreatedDateTime.Date.ToString()
                  })
                 //.AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageCount)
                                      .AsNoTracking()
                                      .ToListAsync();
                totalResultList = violatedExchangeActor.Count();

                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(violatedExchangeActor),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<PagedResult<MemberTradeViolationView>> GetNotifiationViolationList(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 2;
                }
                return await ViolationInJuctionOrSendToLaw(queryParameters, approvalSatus);

            }  else
            {
                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = null,
                    ItemsCount = 0
                };
            }         

        }

        public async Task<PagedResult<MemberTradeViolationView>> ViolationInJuctionOrSendToLaw(NotificationQueryParameters queryParameters, int approvalStatusForward)
        {
            int totalResultList = 0;
            try
            {
                var violatedExchangeActor = await _context.TradeExcutionViolationRecord
                 .Where(x => x.MemberTradeViolation.ViolationStatus == 0 &&
                             ((x.ViolationRecordId == 122) ||(x.ViolationRecordId == 123) ||
                             (x.ViolationRecordId == 125) || (x.ViolationRecordId == 126)) &&
                             x.MemberTradeViolation.Status == approvalStatusForward )
                  .Select(n => new MemberTradeViolationView
                  {
                      MemberTradeViolationId = n.MemberTradeViolationId,
                      ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,

                      CustomerType = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                 n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionEng,
                      OrganizationName = (queryParameters.Lang == "et") ? n.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                                      n.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                      ApprovalStatus = n.MemberTradeViolation.Status.GetValueOrDefault(),
                      StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) : n.CreatedDateTime.Date.ToString(),
                      DecisionType = n.ViolationRecordId
                  })
                 .AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageNumber)
                                      .AsNoTracking()
                                      .ToListAsync();
                totalResultList = violatedExchangeActor.Count();

                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(violatedExchangeActor),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberClientTradeViewModel>> GetNotifiationTradeExecutionReport(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;
            if (queryParameters.WorkFlowUserRoleId == 7)
            {
                if (queryParameters.WorkFlowUserRoleId == 7)
                {
                    approvalSatus = 4;
                }
                return await OneDirectionTradeExcutionFlow(queryParameters, approvalSatus);

            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 7;
                    approvalStatusTwo = 4;
                }
                else if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }
                else if (queryParameters.WorkFlowUserRoleId == 3)
                {
                    approvalSatus = 2;
                }

                return await TwoDirectionTradeExcutionFlow(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }

        public async Task<PagedResult<MemberClientTradeViewModel>> OneDirectionTradeExcutionFlow(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var tradeExcution = await _context.MemberClientTrade
                    .Where(sa => sa.WorkFlowStatus == approvalStatus && sa.TradeExcutionStatusTypeId == 2)
                    .Select(data => new MemberClientTradeViewModel
                    {
                        MemberClientTradeId = data.MemberClientTradeId,
                        ExchangeActorId = data.ExchangeActorId,

                        OrganizationName = (queryParameters.Lang == "et") ? data.ExchangeActor.OrganizationNameAmh :
                                                        data.ExchangeActor.OrganizationNameEng,
                        ReportType = queryParameters.Lang == "et" ? data.ReportType.DescriptionAmh :
                                                    data.ReportType.DescriptionEng,
                        ReportPeriod = queryParameters.Lang == "et" ? data.ReportPeriod.DescriptionAmh : data.ReportPeriod.DescriptionEng,
                        ApprovalStatus = data.WorkFlowStatus.GetValueOrDefault(),
                        ReportDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ReportDate.Date).ToString()) : data.ReportDate.Date.ToString()
                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                totalResultList = tradeExcution.Count();
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = _mapper.Map<List<MemberClientTradeViewModel>>(tradeExcution),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberClientTradeViewModel>> TwoDirectionTradeExcutionFlow(NotificationQueryParameters notificationQuery, int approvalStatusForward, int approvalStatusBackward)
        {
            int totalResultList = 0;
            try
            {
                var tradeExcution = await _context.MemberClientTrade
                    .Where(sa => sa.TradeExcutionStatusTypeId == 2 &&
                           sa.WorkFlowStatus == approvalStatusForward ||
                           sa.WorkFlowStatus == approvalStatusBackward)
                    .Select(data => new MemberClientTradeViewModel
                    {
                        MemberClientTradeId = data.MemberClientTradeId,
                        ExchangeActorId = data.ExchangeActorId,
                        OrganizationName = (notificationQuery.Lang == "et") ? data.ExchangeActor.OrganizationNameAmh :
                                                        data.ExchangeActor.OrganizationNameEng,
                        ReportType = notificationQuery.Lang == "et" ? data.ReportType.DescriptionAmh :
                                                    data.ReportType.DescriptionEng,
                        ReportPeriod = notificationQuery.Lang == "et" ? data.ReportPeriod.DescriptionAmh :
                                                      data.ReportPeriod.DescriptionEng,
                        ApprovalStatus = data.WorkFlowStatus.GetValueOrDefault(),
                        ReportDate = notificationQuery.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ReportDate.Date).ToString()) : data.ReportDate.Date.ToString()
                    })
                    .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                totalResultList = tradeExcution.Count();
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = _mapper.Map<List<MemberClientTradeViewModel>>(tradeExcution),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberTradeViolationView>> OneDirectionBankTransactionFlow(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var bankTransactionlist = await _context.BankOverSightViolationDetail.Where(x =>x.BankOversightViolation.ViolationStatus == 0 &&
                                                                                               x.Status == approvalStatus )
                                                  .Distinct()
                                                  .Select(n => new MemberTradeViolationView
                                                  {
                                                      BankOversightViolationId = n.BankOversightViolationId,
                                                      ExchangeActorId = n.BankOversightViolation.ExchangeActorId,

                                                      CustomerType = (queryParameters.Lang == "et") ? n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                 n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionEng,
                                                      OrganizationName = (queryParameters.Lang == "et") ? n.BankOversightViolation.ExchangeActor.OrganizationNameAmh :
                                                                                      n.BankOversightViolation.ExchangeActor.OrganizationNameEng,
                                                      ApprovalStatus = n.Status,
                                                      StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime).ToString()) : n.CreatedDateTime.Date.ToString(),
                                                     // DecisionType = n.DecisionTypeId
                                                  })
                                                  .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                                                  .AsNoTracking()
                                                  .ToListAsync();
                totalResultList = bankTransactionlist.Count();
                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(bankTransactionlist),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<MemberTradeViolationView>> TwoDirectionFlowBankViolation(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {
            int totalResultList = 0;
            try
            {

                var violatedExchangeActor = await _context.BankOverSightViolationDetail.Where(x => x.BankOversightViolation.ViolationStatus == 0 &&
                                                 (x.Status == approvalStatusForward || x.Status == approvalStatusBackward ))
                  .Select(n => new MemberTradeViolationView
                  {
                      BankOversightViolationId = n.BankOversightViolationId,
                      ExchangeActorId = n.BankOversightViolation.ExchangeActorId,

                      CustomerType = (queryParameters.Lang == "et") ? n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                 n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionEng,
                      OrganizationName = (queryParameters.Lang == "et") ? n.BankOversightViolation.ExchangeActor.OrganizationNameAmh :
                                                                          n.BankOversightViolation.ExchangeActor.OrganizationNameEng,
                      ApprovalStatus = n.Status,
                      StartDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) : n.CreatedDateTime.Date.ToString()
                  })
                                     // .Paging(queryParameters.PageCount, queryParameters.PageCount)
                                      .AsNoTracking()
                                      .ToListAsync();
                totalResultList = violatedExchangeActor.Count();

                return new PagedResult<MemberTradeViolationView>()
                {
                    Items = _mapper.Map<List<MemberTradeViolationView>>(violatedExchangeActor),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberTradeViolationView>> GetNotifiationBankTransaction(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                return await OneDirectionBankTransactionFlow(queryParameters, approvalSatus);
            }

            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionFlowBankViolation(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }


        public async Task<List<RecognitionExpirNotificationViewModel>> GetRecognitionExpirationNotification(string lang)
        {
            try
            {
                var expireNotificaiton = await (from exa in _context.ExchangeActor
                                                join om in _context.OwnerManager on exa.ExchangeActorId equals om.ExchangeActorId
                                                where (exa.CustomerTypeId != (int)Enum.CustomerType.SeetlementBank && exa.CustomerTypeId != (int)Enum.CustomerType.Auditor && exa.CustomerTypeId != (int)Enum.CustomerType.Representative)
                                                && exa.Status == (int)Enum.ExchangeActorStatus.Active && DateTime.Now.CompareTo(exa.ExpireDateTime.GetValueOrDefault()) > 0
                                                select new RecognitionExpirNotificationViewModel
                                                {
                                                    ExchangeActorId = exa.ExchangeActorId.ToString(),
                                                    EcxCode = exa.Ecxcode,
                                                    OrganizationName = lang == "et" ? exa.OrganizationNameAmh : exa.OrganizationNameEng,
                                                    FullName = lang == "et" ? om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh : om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng,
                                                    CustomerType = lang == "et" ? exa.CustomerType.DescriptionAmh : exa.CustomerType.DescriptionEng,
                                                    IssueDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(exa.IssueDateTime.GetValueOrDefault()).ToString()) : exa.IssueDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                                    ExpireDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(exa.ExpireDateTime.GetValueOrDefault()).ToString()) : exa.ExpireDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                                    RepresentativeList = (from exchaneActor in _context.ExchangeActor
                                                                          join repom in _context.OwnerManager on exchaneActor.ExchangeActorId equals repom.ExchangeActorId
                                                                          where exchaneActor.DelegatorId == exa.ExchangeActorId
                                                                          select new RecognitionExpirNotificationViewModel
                                                                          {
                                                                              ExchangeActorId = exchaneActor.DelegatorId.ToString(),
                                                                              EcxCode = exchaneActor.Ecxcode,
                                                                              FullName = lang == "et" ? repom.FirstNameAmh + " " + repom.FatherNameAmh + " " + repom.GrandFatherNameAmh : repom.FirstNameEng + " " + repom.FatherNameEng + " " + repom.GrandFatherNameEng,
                                                                              IssueDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(exchaneActor.IssueDateTime.GetValueOrDefault()).ToString()) : exchaneActor.IssueDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                                                                              ExpireDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(exchaneActor.ExpireDateTime.GetValueOrDefault()).ToString()) : exchaneActor.ExpireDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy")
                                                                          }).AsNoTracking().ToList()

                                                }).AsNoTracking().ToListAsync();


                return expireNotificaiton;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }


        public async Task<PagedResult<MemberClientTradeViewModel>> GetTradeExecutionNotifiationReport(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 7;
                }
                return await OneDirectionTradeExecutionReport(queryParameters, approvalSatus);

            }
            else
            {
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = null,
                    ItemsCount = 0
                };
            }

        }

        public async Task<PagedResult<MemberClientTradeViewModel>> OneDirectionTradeExecutionReport(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var tradeExcution = await _context.MemberClientTrade
                    .Where(sa => sa.WorkFlowStatus == approvalStatus &&
                    sa.TradeExcutionStatusTypeId == 2 &&
                    sa.ReportTypeId == 1)
                    .Select(data => new MemberClientTradeViewModel
                    {
                        MemberClientTradeId = data.MemberClientTradeId,
                        ExchangeActorId = data.ExchangeActorId,

                        OrganizationName = (queryParameters.Lang == "et") ? data.ExchangeActor.OrganizationNameAmh :
                                                        data.ExchangeActor.OrganizationNameEng,
                        ReportType = queryParameters.Lang == "et" ? data.ReportType.DescriptionAmh :
                                                    data.ReportType.DescriptionEng,
                        ReportPeriod = queryParameters.Lang == "et" ? data.ReportPeriod.DescriptionAmh : data.ReportPeriod.DescriptionEng,
                        ApprovalStatus = data.WorkFlowStatus.GetValueOrDefault(),
                        ReportDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ReportDate.Date).ToString()) : data.ReportDate.Date.ToString("MMMM DD, YYYY")
                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                totalResultList = tradeExcution.Count();
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = _mapper.Map<List<MemberClientTradeViewModel>>(tradeExcution),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberClientTradeViewModel>> GetFinanceNotifiationReport(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 7;
                }
                return await OneDirectionFinanceReport(queryParameters, approvalSatus);

            }
            else
            {
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = null,
                    ItemsCount = 0
                };
            }

        }

        public async Task<PagedResult<MemberClientTradeViewModel>> OneDirectionFinanceReport(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var tradeExcution = await _context.MemberClientTrade
                    .Where(sa => sa.WorkFlowStatus == approvalStatus &&
                    sa.TradeExcutionStatusTypeId == 2 &&
                    sa.ReportTypeId == 2)
                    .Select(data => new MemberClientTradeViewModel
                    {
                        MemberClientTradeId = data.MemberClientTradeId,
                        ExchangeActorId = data.ExchangeActorId,

                        OrganizationName = (queryParameters.Lang == "et") ? data.ExchangeActor.OrganizationNameAmh :
                                                        data.ExchangeActor.OrganizationNameEng,
                        ReportType = queryParameters.Lang == "et" ? data.ReportType.DescriptionAmh :
                                                    data.ReportType.DescriptionEng,
                        ReportPeriod = queryParameters.Lang == "et" ? data.ReportPeriod.DescriptionAmh : data.ReportPeriod.DescriptionEng,
                        ApprovalStatus = data.WorkFlowStatus.GetValueOrDefault(),
                        ReportDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ReportDate.Date).ToString()) : data.ReportDate.Date.ToString("MMMM DD, YYYY")
                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                totalResultList = tradeExcution.Count();
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = _mapper.Map<List<MemberClientTradeViewModel>>(tradeExcution),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberClientTradeViewModel>> GetClientInformationNotifiationReport(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 7;
                }
                return await OneDirectionClientInformationReport(queryParameters, approvalSatus);

            }
            else
            {
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = null,
                    ItemsCount = 0
                };
            }

        }

        public async Task<PagedResult<MemberClientTradeViewModel>> OneDirectionClientInformationReport(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            int totalResultList = 0;
            try
            {
                var tradeExcution = await _context.MemberClientTrade
                    .Where(sa => sa.WorkFlowStatus == approvalStatus &&
                    sa.TradeExcutionStatusTypeId == 2 &&
                    sa.ReportTypeId == 3)
                    .Select(data => new MemberClientTradeViewModel
                    {
                        MemberClientTradeId = data.MemberClientTradeId,
                        ExchangeActorId = data.ExchangeActorId,

                        OrganizationName = (queryParameters.Lang == "et") ? data.ExchangeActor.OrganizationNameAmh :
                                                        data.ExchangeActor.OrganizationNameEng,
                        ReportType = queryParameters.Lang == "et" ? data.ReportType.DescriptionAmh :
                                                    data.ReportType.DescriptionEng,
                        ReportPeriod = queryParameters.Lang == "et" ? data.ReportPeriod.DescriptionAmh : data.ReportPeriod.DescriptionEng,
                        ApprovalStatus = data.WorkFlowStatus.GetValueOrDefault(),
                        ReportDate = queryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ReportDate.Date).ToString()) : data.ReportDate.Date.ToString()
                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                totalResultList = tradeExcution.Count();
                return new PagedResult<MemberClientTradeViewModel>()
                {
                    Items = _mapper.Map<List<MemberClientTradeViewModel>>(tradeExcution),
                    ItemsCount = totalResultList
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<PagedResult<MemberFinancialAuditorNotificationView>> GetAnnualAuditNotifiationReport(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 7;
                }
                return await GetExchangeActorAnnulAuditor(queryParameters, approvalSatus);

            }
            else
            {
                return new PagedResult<MemberFinancialAuditorNotificationView>()
                {
                    Items = null,
                    ItemsCount = 0
                };
            }

        }
        public async Task<PagedResult<MemberFinancialAuditorNotificationView>> GetExchangeActorAnnulAuditor(NotificationQueryParameters notificationQuery, int approverStatus)
        {
            try
            {
                var exachangeActorList = await _context.MemberFinancialAuditor.Where(x => x.Status == approverStatus && x.TradeExcutionStatusTypeId == 2)
                         .Select(n => new MemberFinancialAuditorNotificationView
                         {
                             MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                             ExchangeActorId = n.ExchangeActorId,
                             AuditorId = n.Ecxcode,
                             OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                n.ExchangeActor.OrganizationNameEng,
                             DateReport = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) :
                                                     n.CreatedDateTime.Date.ToString(),
                             AuditorName = n.Ecxcode,
                             Remark = n.Remark,
                             AnnualAuditStatus = (notificationQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                    n.TradeExcutionStatusType.DescriptionEng,
                             TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                             ApprovalStatus = n.Status
                         })
                         .AsNoTracking()
                         .ToListAsync();
                return new PagedResult<MemberFinancialAuditorNotificationView>()
                {
                    Items = _mapper.Map<List<MemberFinancialAuditorNotificationView>>(exachangeActorList),
                    ItemsCount = _context.MemberFinancialAuditor.Where(x => x.Status == approverStatus &&
                                                                       x.TradeExcutionStatusTypeId == 2).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> OneDirectionOffSiteNotification(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => sa.Status == approvalStatus && sa.ReportTypeId == 4)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatus && x.ReportTypeId == 4).Count()
                };
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> TwoDirectionOffSiteNotification(NotificationQueryParameters queryParameters, int approvalStatusForward,int approvalStatusBackward)
        {
            try
            {
                
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => (sa.Status == approvalStatusForward ||
                                                                                             sa.Status == approvalStatusBackward) && 
                                                                                             sa.ReportTypeId == 4)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => (x.Status == approvalStatusForward || 
                                                                              x.Status == approvalStatusBackward) && x.ReportTypeId==4).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteNotifiation(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;


            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                } 
                    return await OneDirectionOffSiteNotification(queryParameters, approvalSatus);
            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionOffSiteNotification(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }


        public async Task<PagedResult<OffSiteMonitoringReportNotification>> OneDirectionOffSiteFinancialNotification(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => sa.Status == approvalStatus &&
                                                                                             (sa.ReportTypeId == 2))
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatus &&
                                                                            (x.ReportTypeId == 2)).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> TwoDirectionOffSiteFinancialNotification(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => (sa.Status == approvalStatusForward ||
                                                                                             sa.Status == approvalStatusBackward) &&
                                                                                             (sa.ReportTypeId == 2))
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => (x.Status == approvalStatusForward ||
                                                                              x.Status == approvalStatusBackward) &&
                                                                              (x.ReportTypeId == 2)).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteFinancialNotifiation(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;


            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 2;
                }
                return await OneDirectionOffSiteFinancialNotification(queryParameters, approvalSatus);
            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionOffSiteFinancialNotification(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }

        public async Task<PagedResult<OffSiteMonitoringReportNotification>> OneDirectionOffSiteClientNotification(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => sa.Status == approvalStatus &&
                                                                                             sa.ReportTypeId == 3)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatus).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> TwoDirectionOffSiteClientNotification(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => (sa.Status == approvalStatusForward ||
                                                                                             sa.Status == approvalStatusBackward) &&
                                                                                             sa.ReportTypeId == 3)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatusForward ||
                                                                              x.Status == approvalStatusBackward).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteClientNotifiation(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;


            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                //if (queryParameters.WorkFlowUserRoleId == 0)
                //{
                //    approvalSatus = 2;
                //}
                return await OneDirectionOffSiteClientNotification(queryParameters, approvalSatus);
            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionOffSiteClientNotification(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }

        public async Task<PagedResult<OffSiteMonitoringReportNotification>> OneDirectionOffSiteTradeNotification(NotificationQueryParameters queryParameters, int approvalStatus)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => sa.Status == approvalStatus &&
                                                                                             sa.ReportTypeId == 1)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatus).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> TwoDirectionOffSiteTradeNotification(NotificationQueryParameters queryParameters, int approvalStatusForward, int approvalStatusBackward)
        {
            try
            {
                var offSiteNotification = await _context.OffSiteMonitoringReport.Where(sa => (sa.Status == approvalStatusForward ||
                                                                                             sa.Status == approvalStatusBackward) &&
                                                                                             sa.ReportTypeId == 1)
                                                     .Select(n => new OffSiteMonitoringReportNotification
                                                     {
                                                         OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                         ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                                                        n.ReportPeriod.DescriptionEng,
                                                         ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                                                                      n.ReportType.DescriptionEng,
                                                         ApprovalStatus = n.Status
                                                     })
                                                     .AsNoTracking()
                                                     .ToListAsync();
                return new PagedResult<OffSiteMonitoringReportNotification>()
                {
                    Items = _mapper.Map<List<OffSiteMonitoringReportNotification>>(offSiteNotification),
                    ItemsCount = _context.OffSiteMonitoringReport.Where(x => x.Status == approvalStatusForward ||
                                                                              x.Status == approvalStatusBackward).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<OffSiteMonitoringReportNotification>> GetOffSiteTradeNotifiation(NotificationQueryParameters queryParameters)
        {
            int approvalSatus = 0;
            int approvalStatusTwo = 0;


            if (queryParameters.WorkFlowUserRoleId == 0)
            {
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 4;
                }
                if (queryParameters.WorkFlowUserRoleId == 0)
                {
                    approvalSatus = 2;
                }
                return await OneDirectionOffSiteTradeNotification(queryParameters, approvalSatus);
            }
            else
            {
                if (queryParameters.WorkFlowUserRoleId == 1)
                {
                    approvalSatus = 0;
                    approvalStatusTwo = 5;
                }
                else if (queryParameters.WorkFlowUserRoleId == 2)
                {
                    approvalSatus = 1;
                    approvalStatusTwo = 6;
                }

                return await TwoDirectionOffSiteTradeNotification(queryParameters, approvalSatus, approvalStatusTwo);
            }

        }



    }
}
