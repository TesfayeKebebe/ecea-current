﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.Enum;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.OversightReport
{
    public class ComparisionRepository
    {

        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public ComparisionRepository(ECEADbContext context, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<List<ComparisonData>> GetMonthlyComparisionData(int firstYear, int firstMonth, int secondYear, int secondMonth,int type, string lang)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                string   FirstFilterType = String.Concat(firstYear, "/", "", firstMonth, "");

                string SecondFilterType = String.Concat(secondYear, "/", "", secondMonth, "");


                summaryByFirstCase = await (from s in _context.OversightFollowUpReport
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat(firstYear, "/", "", firstMonth, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Month == firstMonth
                                                              && r.DateOfInspection.Year == firstYear
                                                              && r.FollowUpTypeId==type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Month == firstMonth
                                                               && r.DateOfInspection.Year == firstYear
                                                               && r.FollowUpTypeId == type
                                                               &&  r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                                          Where(r => r.DateOfInspection.Month == firstMonth
                                                                          && r.DateOfInspection.Year == firstYear
                                                                          && r.FollowUpTypeId == type
                                                                          && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.
                                                                              Where(r => r.DateOfInspection.Month == firstMonth
                                                                              && r.DateOfInspection.Year == firstYear 
                                                                                 && r.FollowUpTypeId == type 
                                                                                 && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null &&
                                                                                    r.DateOfInspection.Month >= firstMonth
                                                                                && r.DateOfInspection.Year <= firstYear).Count(),

                                            }).FirstOrDefaultAsync();



                summaryBySecondCase = await (from s in _context.OversightFollowUpReport
                                             select new ComparisonData
                                             {
                                                 FilterType = String.Concat(secondYear, "/", secondMonth, ""),
                                                 GivenwrittenWarning = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Month == secondMonth
                                                                      && r.DateOfInspection.Year == secondYear
                                                                      && r.FollowUpTypeId == type 
                                                                      &&  r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                 orallWarned = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Month == secondMonth
                                                                  && r.DateOfInspection.Year == secondYear
                                                                    && r.FollowUpTypeId == type
                                                                  && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                 RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Month == secondMonth
                                                                   && r.DateOfInspection.Year <= secondYear
                                                                      && r.FollowUpTypeId == type 
                                                                      && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                 SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Month == secondMonth
                                                                    && r.DateOfInspection.Year == secondYear
                                                                       && r.FollowUpTypeId == type
                                                                    && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                 TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null
                                                                        && r.DateOfInspection.Month == secondMonth
                                                                      && r.FollowUpTypeId == type
                                                                    && r.DateOfInspection.Year == secondYear).Count(),
                                             }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<OversightReportMOnthlySummaryModel>> GetCheckListComparisionData(int firstYear,int secondYear, int type, string language)
        {
            try
            {
                DateTime FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", firstYear));
                DateTime FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", firstYear));

                DateTime SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", secondYear));
                DateTime SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", secondYear));

                List<OversightReportMOnthlySummaryModel> FirstYearData = new List<OversightReportMOnthlySummaryModel>();
                FirstYearData = await _context.OversightReportMOnthlySummaryModel.FromSqlRaw("exec dbo.pMonthlySummury {0},{1},{2},{3}",
                    FirstYearStartDate, FirstYearEndDate, language, type).ToListAsync();
                return  FirstYearData;
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
        }

        public async Task<List<ComparisonData>> GetAnnualComparisionData(int firstYear, int secondYear, int type, string lang)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();




                summaryByFirstCase = await(from s in _context.OversightFollowUpReport
                                           select new ComparisonData
                                           {
                                               FilterType = String.Concat(firstYear, ""),
                                               GivenwrittenWarning = _context.OversightFollowUpReport.
                                                             Where(r =>  r.DateOfInspection.Year == firstYear
                                                             && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                               orallWarned = _context.OversightFollowUpReport.
                                                             Where(r =>  r.DateOfInspection.Year == firstYear
                                                                && r.FollowUpTypeId == type
                                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                               RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                                         Where(r => r.DateOfInspection.Year == firstYear
                                                                         && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                               SentForLawEnforcement = _context.OversightFollowUpReport.
                                                                             Where(r => r.DateOfInspection.Year == firstYear
                                                                             && r.FollowUpTypeId == type &&
                                                                                 r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                               TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null &&
                                                                                   r.DateOfInspection.Year <= firstYear).Count(),

                                           }).FirstOrDefaultAsync();



                summaryBySecondCase = await(from s in _context.OversightFollowUpReport
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat(secondYear, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Year == secondYear
                                                  && r.FollowUpTypeId == type
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.Where(r =>  r.DateOfInspection.Year == secondYear 
                                                && r.FollowUpTypeId == type
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r =>r.DateOfInspection.Year <= secondYear
                                                  && r.FollowUpTypeId == type 
                                                  && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Year == secondYear && r.FollowUpTypeId == type
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null && 
                                                r.DateOfInspection.Year == secondYear && r.FollowUpTypeId == type).Count(),
                                            }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<ComparisonData>> GetNineMonthComparisionData(int firstYear, int order1, int secondYear, int order2, int type)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                //Dates for First year

                DateTime FirstYearStartDate= DateTime.UtcNow; 
                DateTime FirstYearEndDate= DateTime.UtcNow;


                //Dates for second Year

                DateTime SecondYearStartDate= DateTime.UtcNow; 
                DateTime SecondYearEndDate=DateTime.UtcNow; 

          
                if (order1 == 1)
                {

                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", firstYear));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "03", "/", firstYear+1));



                }
                else if(order1==2)
                {
                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "04", "/", firstYear));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", firstYear + 1));
                }

                if (order2 == 1)
                {

                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", secondYear));
                    SecondYearEndDate= Convert.ToDateTime(String.Concat("30/", "03", "/", secondYear + 1));

                }
                else if(order2==2)
                {
                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "04", "/", secondYear));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", secondYear + 1));
                }




                summaryByFirstCase = await(from s in _context.OversightFollowUpReport
                                           select new ComparisonData
                                           {
                                               FilterType = String.Concat(firstYear, ""),
                                               GivenwrittenWarning = _context.OversightFollowUpReport.
                                                             Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                             && r.DateOfInspection<= FirstYearEndDate.Date
                                                             && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                               orallWarned = _context.OversightFollowUpReport.
                                                             Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                             && r.DateOfInspection <= FirstYearEndDate.Date
                                                              && r.FollowUpTypeId == type
                                                             && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                               RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                                         Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                             && r.DateOfInspection <= FirstYearEndDate.Date
                                                             && r.FollowUpTypeId == type
                                                             && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                               SentForLawEnforcement = _context.OversightFollowUpReport.
                                                                             Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                             && r.DateOfInspection <= FirstYearEndDate.Date
                                                             && r.FollowUpTypeId == type
                                                             && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                               TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                             && r.DateOfInspection <= FirstYearEndDate.Date
                                                             && r.FollowUpTypeId == type
                                                             && r.DepartmentDecison != null).Count(),

                                           }).FirstOrDefaultAsync();



                summaryBySecondCase = await(from s in _context.OversightFollowUpReport
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat(secondYear, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r =>
                                                                        r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                        && r.DateOfInspection.Date<= SecondYearEndDate.Date
                                                                         && r.FollowUpTypeId == type
                                                                        && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.Where(r =>
                                                                       r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                        && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                                         && r.FollowUpTypeId == type
                                                                        && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => 
                                                                           r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                            && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                                             && r.FollowUpTypeId == type
                                                                            && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                         && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                                          && r.FollowUpTypeId == type
                                                                         && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                        && r.DateOfInspection.Date <= SecondYearEndDate.Date && r.FollowUpTypeId == type
                                                                        && r.DepartmentDecison != null).Count(),
                                            }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ComparisonData>> GetQuarterlyComparisionData(int firstYear,int quarter1, int secondYear,int quarter2,int type, string language)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                //Dates for First year

                DateTime FirstYearStartDate = DateTime.UtcNow;
                DateTime FirstYearEndDate = DateTime.UtcNow;


                //Dates for second Year

                DateTime SecondYearStartDate = DateTime.UtcNow;
                DateTime SecondYearEndDate = DateTime.UtcNow;

                // Dates for First Year
                if (quarter1 == 1)
                {

                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", firstYear));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "09", "/", firstYear));
                 }
                else if (quarter1 == 2)
                {
                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "10", "/", firstYear));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", firstYear));
                }
                else if (quarter1 == 3)
                {
                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", firstYear+1));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "03", "/", firstYear + 1));
                }
                else if (quarter1 ==4)
                {
                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "04", "/", firstYear+1));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", firstYear + 1));
                }
                // Dates for second Quarter
                if (quarter2 == 1)
                {

                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", secondYear));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "09", "/", secondYear));
                }
                else if (quarter2 == 2)
                {
                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "10", "/", secondYear));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", secondYear));
                }
                else if (quarter2 == 3)
                {
                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", secondYear + 1));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "03", "/", secondYear + 1));
                }
                else if (quarter2 == 4)
                {
                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "04", "/", secondYear + 1));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", secondYear + 1));
                }




                summaryByFirstCase = await (from s in _context.OversightFollowUpReport orderby s.CreatedDateTime
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat(firstYear, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                              && r.FollowUpTypeId==type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                                          Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.
                                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison != null).Count(),

                                            }).FirstOrDefaultAsync();



                summaryBySecondCase = await (from s in _context.OversightFollowUpReport
                                             orderby s.CreatedDateTime
                                             select new ComparisonData
                                             {
                                                 FilterType = String.Concat(secondYear, ""),
                                                 GivenwrittenWarning = _context.OversightFollowUpReport.Where(r =>
                                                         r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                         && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                          && r.FollowUpTypeId == type
                                                         && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),


                                                 orallWarned = _context.OversightFollowUpReport.Where(r =>
                                                     r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                      && r.DateOfInspection.Date <= SecondYearEndDate.Date 
                                                      && r.FollowUpTypeId == type && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),

                                                 RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r =>
                                                            r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                             && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                             && r.FollowUpTypeId == type &&
                                                             r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                 SentForLawEnforcement = _context.OversightFollowUpReport
                                                                  .Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                          && r.DateOfInspection.Date <= SecondYearEndDate.Date 
                                                                          && r.FollowUpTypeId == type && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                 TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                 && r.DateOfInspection.Date <= SecondYearEndDate.Date && r.FollowUpTypeId == type && r.DepartmentDecison != null).Count(),
                                             }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ComparisonData>> GetSemisterComparisionData(int firstYear, int semister1, int secondYear, int semister2,int type)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                //Dates for First year

                DateTime FirstYearStartDate = DateTime.UtcNow;
                DateTime FirstYearEndDate = DateTime.UtcNow;


                //Dates for second Year

                DateTime SecondYearStartDate = DateTime.UtcNow;
                DateTime SecondYearEndDate = DateTime.UtcNow;


                if (semister1 == 1)
                {

                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", firstYear));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", firstYear));



                }
                else if (semister1 == 2)
                {
                    FirstYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", firstYear+1));
                    FirstYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", firstYear + 1));
                }

                if (semister2 == 1)
                {

                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "07", "/", secondYear));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "12", "/", secondYear));

                }
                else if (semister2 == 2)
                {
                    SecondYearStartDate = Convert.ToDateTime(String.Concat("01/", "01", "/", secondYear+1));
                    SecondYearEndDate = Convert.ToDateTime(String.Concat("30/", "06", "/", secondYear + 1));
                }




                summaryByFirstCase = await (from s in _context.OversightFollowUpReport
                                            orderby s.CreatedDateTime
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat(firstYear, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.
                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                                          Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.
                                                                              Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= FirstYearStartDate.Date
                                                              && r.DateOfInspection <= FirstYearEndDate.Date
                                                               && r.FollowUpTypeId == type
                                                              && r.DepartmentDecison != null).Count(),

                                            }).FirstOrDefaultAsync();



                summaryBySecondCase = await (from s in _context.OversightFollowUpReport
                                             orderby s.CreatedDateTime
                                             select new ComparisonData
                                             {
                                                 FilterType = String.Concat(secondYear, ""),
                                                 GivenwrittenWarning = _context.OversightFollowUpReport.Where(r =>
                                                                         r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                         && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                                          && r.FollowUpTypeId == type
                                                                         && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                 orallWarned = _context.OversightFollowUpReport.Where(r =>
                                                                         r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                          && r.DateOfInspection.Date <= SecondYearEndDate.Date
                                                                           && r.FollowUpTypeId == type
                                                                          && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                 RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r =>
                                                                    r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                     && r.DateOfInspection.Date <= SecondYearEndDate.Date && r.FollowUpTypeId == type
                                                                     && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                 SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                          && r.DateOfInspection.Date <= SecondYearEndDate.Date && r.FollowUpTypeId == type
                                                                          && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                 TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondYearStartDate.Date
                                                                 && r.DateOfInspection.Date <= SecondYearEndDate.Date && r.FollowUpTypeId == type
                                                                 && r.DepartmentDecison != null).Count(),
                                             }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public async  Task<List<OversightReportMOnthlySummaryModel>> GetSummaryByCheckList(DateTime from, DateTime to, string lang, int type)
        //{
        //    try
        //    {
        //        List<OversightReportMOnthlySummaryModel> data = new List<OversightReportMOnthlySummaryModel>();
        //        data = await _context.OversightReportMOnthlySummaryModel.FromSql("exec dbo.pMonthlySummury {0},{1},{2},{3}", from, to, lang, type).ToListAsync();
        //        return data;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception(e.InnerException.Message);
        //    }

        //}
    }
}
