﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.Enum;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.OversightReport
{
   public class OversightMonthlySummaryrepo
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public OversightMonthlySummaryrepo(ECEADbContext context, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<int> PostMonthlyReport(MonthlyOnsiteFollowupSummary postedData)
        {
            try
            {

                MonthlyOnsiteFollowupSummary existingdata = _context.MonthlyOnsiteFollowupSummary.Where(re => re.RptCriteriaId == postedData.RptCriteriaId
                 && re.RptMonth == postedData.RptMonth && re.RptYear == postedData.RptYear).FirstOrDefault();

                if (existingdata==null)
                {
                    _context.MonthlyOnsiteFollowupSummary.Add(postedData);
                    await _context.SaveChangesAsync();
                    return postedData.ReportId;

                }
                else
                {
                    return 0;
                }

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

     

        public async Task<bool> SendForAppproval(SendForApprovalModel updatedData)
        {
            try
            {
                MonthlyOnsiteFollowupSummary FilledData = await _context.MonthlyOnsiteFollowupSummary.FirstOrDefaultAsync(ms => ms.RptMonth == updatedData.RptMonth
                                        && ms.RptYear == updatedData.RptYear);
       
                if (FilledData != null)
                {
                    FilledData.RptStatus =(int) MonthlyOverSightSummaryStatus.SubmitedToLeader;
                    FilledData.UpdatedDateTime = DateTime.UtcNow;
                   _context.SaveChanges();
                    return  true;
                }else
                {
                    return false;
                }
               
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> ApproveMOnthlySummary(SendForApprovalModel updatedData)
        {
            try
            {
                MonthlyOnsiteFollowupSummary FilledData = await _context.MonthlyOnsiteFollowupSummary.FirstOrDefaultAsync(ms => ms.RptMonth == updatedData.RptMonth
                                        && ms.RptYear == updatedData.RptYear);

                if (FilledData != null)
                {
                    FilledData.RptStatus = (int)MonthlyOverSightSummaryStatus.ApprovedByDepartment;
                    FilledData.UpdatedDateTime = DateTime.UtcNow;
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ComparisonData>> CompareExecution(int FirstPhase, int SecondPhase, string unit)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();


                if (unit != null)
                {
                    switch (unit)
                    {
                        case "month":
                            summary = await GetComparisionByMonth(FirstPhase, SecondPhase);
                            break;
                        case "quarterly":
                            summary = await GetComparisionByQuarter(FirstPhase, SecondPhase);
                            break;
                        case "annual":
                            summary = await GetComparisionByYear(FirstPhase, SecondPhase);
                            break;
                        case "semister":
                            summary = await GetComparisionBySemister(FirstPhase, SecondPhase);
                            break;
                        case "threeFouth":
                            summary = await GetComparisionByNineMonth(FirstPhase, SecondPhase);
                            break;

                    }
                }
                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private async Task<List<ComparisonData>> GetComparisionBySemister(int firstPhase, int secondPhase)
        {

            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                summaryByFirstCase = await (from s in _context.MonthlyOnsiteFollowupSummary
                                            select new ComparisonData
                                            {
                                                FilterType = firstPhase.ToString(),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.
                                                Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.
                                                Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.
                                                Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.
                                                Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null).Count(),
                                            }).FirstOrDefaultAsync();


                summaryBySecondCase = await (from s in _context.MonthlyOnsiteFollowupSummary
                                             select new ComparisonData
                                             {
                                                 FilterType = secondPhase.ToString(),
                                                 GivenwrittenWarning = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                 orallWarned = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                 RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                 SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                 TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null).Count(),
                                             }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<List<ComparisonData>> GetComparisionByNineMonth(int firstPhase, int secondPhase)
        {

            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                string startdate = "01-07-" + firstPhase;
                string enddate = "01-02-" + (firstPhase + 1);



                DateTime fsdate = Convert.ToDateTime(startdate).Date;
                DateTime fsdateend = Convert.ToDateTime(enddate).Date;




                summaryByFirstCase = await(from s in _context.OversightFollowUpReport select
                     new ComparisonData
                                           {
                                  FilterType = firstPhase.ToString(),
                                  GivenwrittenWarning = _context.OversightFollowUpReport.
                                               Where(r =>r.DateOfInspection.Date>=fsdate.Date && r.DateOfInspection<=fsdateend.Date
                                               && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                 orallWarned = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date > fsdate && r.DateOfInspection.Date <= fsdateend.Date
                                               && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                 RecordedAsGoAhead = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date > fsdate.Date && r.DateOfInspection.Date <= fsdateend.Date &&
                                               r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                 SentForLawEnforcement = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date > fsdate && r.DateOfInspection.Date <= fsdateend.Date &&
                                               r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date > fsdate.Date
                                               && r.DateOfInspection <= fsdateend.Date && r.DepartmentDecison != null).Count(),
                                           }).FirstOrDefaultAsync();


                string second_startdate = "01-07-" + secondPhase;
                string second_enddate = "01-02-" + (secondPhase + 1);

                DateTime second_fsdate = Convert.ToDateTime(second_startdate).Date;
                DateTime second_fsdateend = Convert.ToDateTime(second_enddate).Date;

                summaryBySecondCase = await(from s in _context.OversightFollowUpReport
                                            select new ComparisonData
                                            {
                                                FilterType = secondPhase.ToString(),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r =>
                                                r.DateOfInspection.Date > second_fsdate.Date &&
                                                r.DateOfInspection <= second_fsdateend.Date && 
                                                r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),

                                                orallWarned = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date > second_fsdate.Date &&
                                                r.DateOfInspection <= second_fsdateend.Date && 
                                                r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date > second_fsdate.Date &&
                                                r.DateOfInspection <= second_fsdateend.Date && 
                                                r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date > second_fsdate.Date &&
                                                r.DateOfInspection <= second_fsdateend.Date && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date > second_fsdate.Date &&
                                                r.DateOfInspection <= second_fsdateend.Date && r.DepartmentDecison != null).Count(),
                                            }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<List<ComparisonData>> GetComparisionByYear(int firstPhase, int secondPhase)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                summaryByFirstCase = await(from s in _context.OversightFollowUpReport
                                           select new ComparisonData
                                           {
                                               FilterType = firstPhase.ToString(),
                                               GivenwrittenWarning = _context.OversightFollowUpReport.
                                               Where(r =>r.DateOfInspection.Year==firstPhase&&
                                               r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                               orallWarned = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Year == firstPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                               RecordedAsGoAhead = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Year == firstPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                               SentForLawEnforcement = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Year == firstPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                               TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null && r.DateOfInspection.Year == firstPhase ).Count(),

                                           }).FirstOrDefaultAsync();


                summaryBySecondCase = await(from s in _context.MonthlyOnsiteFollowupSummary
                                            select new ComparisonData
                                            {
                                                FilterType = secondPhase.ToString(),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r =>
                                                r.DateOfInspection.Year == secondPhase &&
                                                r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                                orallWarned = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Year == secondPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                                RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Year == secondPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Year == secondPhase && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null && r.DateOfInspection.Year == secondPhase).Count(),
                                            }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<List<ComparisonData>> GetComparisionByQuarter(int firstPhase, int secondPhase)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();


                int year = DateTime.UtcNow.Year;
                string month = firstPhase.ToString().Length == 1 ? String.Concat("0", firstPhase) : firstPhase.ToString();
                string secondMonth = secondPhase.ToString().Length == 1 ? String.Concat("0", secondPhase) : secondPhase.ToString();

                string _firstMonth = firstPhase.ToString().Length == 1 ? '0' + firstPhase.ToString() : firstPhase.ToString();
                int finalDay = firstPhase == 2 ? 28 : 30;
                int secondfinalDay = secondPhase == 2 ? 28 : 30;

                DateTime FirstQuarterStartDate;
                DateTime FirstQuarterEndDate;
                DateTime SecondQuarterStartDate;
                DateTime SecondQuarterEndDate;


                var firstQuarter = _context.ReportPeriod.Where(pr => pr.ReportPeriodId == firstPhase).FirstOrDefault();
                var secondQuarter = _context.ReportPeriod.Where(p => p.ReportPeriodId == secondPhase).FirstOrDefault();

               
               FirstQuarterStartDate = firstQuarter.StartDate;
               FirstQuarterEndDate = firstQuarter.DeadLine;
                

              SecondQuarterStartDate = secondQuarter.StartDate;
              SecondQuarterEndDate = secondQuarter.DeadLine;
                


                summaryByFirstCase = await(from s in _context.OversightFollowUpReport
                                        .Where(r => r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                           && r.DateOfInspection.Date <= FirstQuarterEndDate.Date)

                                           select new ComparisonData
                                           {
                                               FilterType = firstPhase.ToString(),
                                               GivenwrittenWarning = _context.OversightFollowUpReport.
                                               Where(r =>
                                                r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= FirstQuarterEndDate.Date
                                               && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),

                                               orallWarned = _context.OversightFollowUpReport.
                                               Where(r =>
                                                r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= FirstQuarterEndDate.Date
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),

                                               RecordedAsGoAhead = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= FirstQuarterEndDate.Date 
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),

                                               SentForLawEnforcement = _context.OversightFollowUpReport.
                                               Where(r =>
                                                r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= FirstQuarterEndDate.Date
                                                &&  r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),

                                               TotalEvaluationCount = _context.OversightFollowUpReport
                                               .Where(r => r.DepartmentDecison != null && r.DateOfInspection.Date >= FirstQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= FirstQuarterEndDate.Date).Count(),

                                           }).FirstOrDefaultAsync();


                                summaryBySecondCase = await(from s in _context.OversightFollowUpReport
                                                            .Where(r => r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                           && r.DateOfInspection.Date <= SecondQuarterEndDate.Date)
                                                            select new ComparisonData
                                            {
                                                FilterType = secondPhase.ToString(),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r => 
                                                r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                                && r.DateOfInspection.Date <= SecondQuarterEndDate.Date
                                                && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),

                                                orallWarned = _context.OversightFollowUpReport.Where(r =>
                                                     r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                                   && r.DateOfInspection.Date <= SecondQuarterEndDate.Date
                                                   && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),

                                                RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r =>
                                                 r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                                   && r.DateOfInspection.Date <= SecondQuarterEndDate.Date
                                                   && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                                SentForLawEnforcement = _context.OversightFollowUpReport.Where(r =>
                                                 r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                                   && r.DateOfInspection.Date <= SecondQuarterEndDate.Date
                                                   && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                                TotalEvaluationCount = _context.OversightFollowUpReport.
                                                Where(r=> r.DepartmentDecison!=null && r.DateOfInspection.Date >= SecondQuarterStartDate.Date
                                                   && r.DateOfInspection.Date <= SecondQuarterEndDate.Date).Count(),
                                            }).FirstOrDefaultAsync();
                            summary.Add(summaryByFirstCase);
                            summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
         }

        private async Task<List<ComparisonData>> GetComparisionByMonth(int firstPhase, int secondPhase)
        {
            try
            {
                List<ComparisonData> summary = new List<ComparisonData>();
                ComparisonData summaryByFirstCase = new ComparisonData();
                ComparisonData summaryBySecondCase = new ComparisonData();

                int year = DateTime.UtcNow.Year;
                string month = firstPhase.ToString().Length == 1 ?  String.Concat("0", firstPhase): firstPhase.ToString();
                string secondMonth = secondPhase.ToString().Length == 1 ? String.Concat("0", secondPhase) : secondPhase.ToString();

                string _firstMonth = firstPhase.ToString().Length == 1 ? '0' + firstPhase.ToString() : firstPhase.ToString();
                int finalDay = firstPhase == 2 ? 28 : 30;
                int secondfinalDay = secondPhase == 2 ? 28 : 30;


                DateTime StartDate =Convert.ToDateTime ( String.Concat( "01/", month, "/",year));
                DateTime EndDate = Convert.ToDateTime(String.Concat(finalDay, "/", month, "/" , year));

        
                summaryByFirstCase = await(from s in _context.OversightFollowUpReport.Where(r=>r.DateOfInspection.Date >=StartDate.Date
                                           && r.DateOfInspection.Date<=EndDate.Date )
                                         select new ComparisonData
                                           {
                                               FilterType = String.Concat("", firstPhase,""),
                                               GivenwrittenWarning = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date >= StartDate.Date
                                                           && r.DateOfInspection.Date <= EndDate.Date
                                                           && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                                               orallWarned = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date >= StartDate.Date
                                           && r.DateOfInspection.Date <= EndDate.Date &&
                                           r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                                               RecordedAsGoAhead = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date >= StartDate.Date
                                           && r.DateOfInspection.Date <= EndDate.Date&& r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                                               SentForLawEnforcement = _context.OversightFollowUpReport.
                                               Where(r => r.DateOfInspection.Date >= StartDate.Date
                                           && r.DateOfInspection.Date <= EndDate.Date &&
                                           r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                                               TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null &&
                                               r.DateOfInspection.Date >= StartDate.Date
                                           && r.DateOfInspection.Date <= EndDate.Date).Count(),

                                           }).FirstOrDefaultAsync();


                DateTime SecondStartDate = Convert.ToDateTime(String.Concat("01/", secondMonth, "/", year));
                DateTime SecondEndDate = Convert.ToDateTime(String.Concat(secondfinalDay, "/", secondMonth, "/", year));

                summaryBySecondCase = await(from s in _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date)
                                            select new ComparisonData
                                            {
                                                FilterType = String.Concat("", secondPhase, ""),
                                                GivenwrittenWarning = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date && r.DepartmentDecison == (int)OversightFollowUpDecisions.LETTERWARNING).Count(),
                         orallWarned = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date && r.DepartmentDecison == (int)OversightFollowUpDecisions.ORALWARNING).Count(),
                        RecordedAsGoAhead = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date && r.DepartmentDecison == (int)OversightFollowUpDecisions.GOAHEAD).Count(),
                       SentForLawEnforcement = _context.OversightFollowUpReport.Where(r => r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date && r.DepartmentDecison == (int)OversightFollowUpDecisions.SENDTOLAWENFORCEMENT).Count(),
                       TotalEvaluationCount = _context.OversightFollowUpReport.Where(r => r.DepartmentDecison != null && r.DateOfInspection.Date >= SecondStartDate.Date
                                          && r.DateOfInspection.Date <= SecondEndDate.Date).Count(),
                                            }).FirstOrDefaultAsync();
                summary.Add(summaryByFirstCase);
                summary.Add(summaryBySecondCase);


                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> MakeDecisionOnSingleFollowUp(ApprovalModel decision)
        {
            var data = _context.OversightFollowUpReport.Where(d => d.FollowUpId == decision.FollowUpId).FirstOrDefault();
            if (data != null)
            {
                if (decision.Role == 2)
                {

                    data.TeamLeaderDecision = decision.Decision;
                }
                else if (decision.Role == 3)
                {
                    data.DepartmentDecison = decision.Decision;
                   

                }
                await _context.SaveChangesAsync();
                return  true;
            }
            else
            {
                return  false;
            }
        }

        public async Task<List<MonthlySummaryModel>> GetlastMonthReportstatus(int year, int month, int type)
        {
            try
            {
               List<MonthlySummaryModel> summary = new List<MonthlySummaryModel>();
                summary =await( from sm in _context.MonthlyOnsiteFollowupSummary.Where(st => st.RptMonth == month && st.RptYear == year && st.RptFollowUPType==type)
                                join l in _context.Lookup on sm.RptFollowUPType equals l.LookupId
                                join cr in _context.Oversight_report_criteria on sm.RptCriteriaId equals cr.CriteriaId
                                join us in _context.Users on sm.RptSubmitedBy.ToString() equals us.Id
                                select new MonthlySummaryModel
                                {
                                    ReportId= sm.ReportId,
                                    RptGeneralDescription= sm.RptGeneralDescription,
                                    RptStatus= sm.RptStatus,
                                    RptMonth= sm.RptMonth,
                                    RptSubmitedDate= sm.RptSubmitedDate,
                                    RptCriteriaId= cr.CriteriaNameAmh,
                                    RptDepartmentDecision= sm.RptDepartmentDecision,
                                    RptYear= sm.RptYear,
                                    RptProblemsFaced= sm.RptProblemsFaced,
                                    RptFinalDecision= sm.RptFinalDecision,
                                    RptFollowUPType= l.DescriptionAmh+'('+l.DescriptionEng+')',
                                    RptLeaderDecision= sm.RptLeaderDecision,
                                    RptMeasurementsTaken= sm.RptMeasurementsTaken,
                                    RptSubmitedBy= us.FullName
                                }).ToListAsync();
                return summary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<MonthlyOnsiteFollowupSummary> CheckMonthlyReportByCriteria(int year, int month, int criteriaId)
        {
            try
            {
              MonthlyOnsiteFollowupSummary Data = new MonthlyOnsiteFollowupSummary();
                Data = await _context.MonthlyOnsiteFollowupSummary.Where(o => o.RptCriteriaId == criteriaId
                && o.RptMonth == month && o.RptYear == year).FirstOrDefaultAsync();
            return Data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
          

        }


        public async Task<PagedResult<FollowupSummaryViewModel>> GetMonthlyReport(FilterCriteria _filterCriteria)
        {
            try
            {
                List<FollowupSummaryViewModel> Data = new List<FollowupSummaryViewModel>();
                int totalSearchResult = 0;
                Data = await (from osr in _context.OversightFollowUpReport
                                .Where(report => report.IsDeleted == false &&
                                report.DateOfInspection <= _filterCriteria.EndDate && report.DateOfInspection >= _filterCriteria.StartDate &&
                                report.TeamDecision != (int)OversightFollowUpDecisionStatus.incomplete&& report.FollowUpTypeId==_filterCriteria.followupType)
                                .OrderByDescending(or => or.DateOfInspection)
                              join sa in _context.Lookup
                            on osr.TeamDecision equals sa.LookupId
                              join ex in _context.ExchangeActor
                            on osr.ExchangeActorId equals ex.ExchangeActorId 
                              join tl in _context.Lookup on osr.TeamLeaderDecision equals tl.LookupId into teamleader from tl in teamleader.DefaultIfEmpty()
                              join dep in _context.Lookup on osr.DepartmentDecison equals dep.LookupId into departmet from l in departmet.DefaultIfEmpty()
                          //    join v in _context.MemberTradeViolation on osr.ExchangeActorId equals v.ExchangeActorId into vo from d in vo.DefaultIfEmpty()
                           //   join violation in _context.TradeExcutionViolationRecord on d.MemberTradeViolationId equals violation.MemberTradeViolationId into _vo
                             // from v2 in _vo.DefaultIfEmpty()

                              select new FollowupSummaryViewModel
                              {
                                 //LastRecord =v2.ApproverDecision.ToString() ,
                                  LastRecord = (_filterCriteria.Language == "et") ? l.DescriptionAmh : l.DescriptionEng,
                                  DepartmentDecision = (_filterCriteria.Language == "et") ? l.DescriptionAmh : l.DescriptionEng,
                                  TeamDecision = (_filterCriteria.Language == "et") ? sa.DescriptionAmh : sa.DescriptionEng,
                                  DateOfInspection = (_filterCriteria.Language == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.Date.ToString(),
                                  FollowUpId = osr.FollowUpId,
                                  TeamLeaderDecision = (_filterCriteria.Language == "et") ? tl.DescriptionAmh : tl.DescriptionEng,
                                  ExchangeActor = (_filterCriteria.Language == "et") ? ex.OrganizationNameAmh : ex.OrganizationNameEng
                              }).ToListAsync();


                totalSearchResult = Data.Count();

                var pagedResult = Data.AsQueryable().Paging(_filterCriteria.PageCount, _filterCriteria.PageNumber);
                return new PagedResult<FollowupSummaryViewModel>()
                {
                    Items = _mapper.Map<List<FollowupSummaryViewModel>>(pagedResult),
                    ItemsCount = Data.Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

      

     
    }
}
