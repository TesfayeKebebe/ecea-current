﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Enum;
using CustomerType = CUSTOR.ICERS.Core.EntityLayer.Common.CustomerType;

namespace CUSTOR.ICERS.Core.DataAccessLayer.OversightReportRepo
{
    public class OversightReportRepo
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public OversightReportRepo(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<Oversight_report_criteria_viewModel>> GetCriteria(int exchangeActorType, int type, string lang)
        {
            try
            {
                var data = await (from osr in _context.Oversight_report_criteria
                                  join lo in _context.Lookup on exchangeActorType equals lo.LookupId
                                  where osr.ExchangeActorTypeId == exchangeActorType && osr.CriteriaTypeId == type 
                                  && osr.IsActive==true && osr.IsDeleted==false
                                  select new Oversight_report_criteria_viewModel
                                  {
                                      CriteriaId = osr.CriteriaId,
                                      Description = (lang == "et") ? osr.DescriptionAmh : osr.DescriptionEng,
                                      CriteriaName = (lang == "et") ? osr.CriteriaNameAmh : osr.CriteriaNameEng,
                                      Weight = osr.Weight,
                                      ExchangeActorTypeId = (exchangeActorType == 0) ? (lang == "et" ? "አይታወቅም" : "undefined") : (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng
                                  })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Oversight_report_criteria_viewModel>> GetCriteriaByType(int type, string lang)
        {
            try
            {
               // Financial = 144,
                  //workArea = 145,
                  //bank = 193
                int exchangeActorType = 0;
                if (type == 145)
                {
                    exchangeActorType = 69;
                }
               else if (type == 193)
                {
                    exchangeActorType = 69;
                }
                else if (type == 144)
                {
                    exchangeActorType = 6;
                }
                var data = await(from osr in _context.Oversight_report_criteria
                                 join lo in _context.Lookup on type equals lo.LookupId
                                 where osr.CriteriaTypeId == type && osr.ExchangeActorTypeId==exchangeActorType
                                 && osr.IsActive == true && osr.IsDeleted == false
                                 select new Oversight_report_criteria_viewModel
                                 {
                                     CriteriaId = osr.CriteriaId,
                                     Description = (lang == "et") ? osr.DescriptionAmh : osr.DescriptionEng,
                                     CriteriaName = (lang == "et") ? osr.CriteriaNameAmh : osr.CriteriaNameEng,
                                    // Weight = osr.Weight,
                                   //  ExchangeActorTypeId = (exchangeActorType == 0) ? (lang == "et" ? "አይታወቅም" : "undefined") : (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng
                                 })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<OversightFollowUpReport> GetIcompleteReportDetails( int id)
        {
            try
            {
                var data = (from osr in _context.OversightFollowUpReport
                                 .Where(r=>r.FollowUpId==id)
                                 select new OversightFollowUpReport
                                 {
                                    FollowUpId= id,
                                    CreatedUserId= osr.CreatedUserId,
                                    DateOfInspection= osr.DateOfInspection,
                                    FollowUpTypeId= osr.FollowUpTypeId,
                                    ExchangeActorId= osr.ExchangeActorId,
                                    Currentstatus= osr.Currentstatus,
                                    CreatedDateTime=osr.CreatedDateTime,
                                    OfficeClosed= osr.OfficeClosed,
                                    Synopsis= osr.Synopsis,
                                    Remark= osr.Remark,
                                    ReportedBy= osr.ReportedBy,
                                    IsDeleted= osr.IsDeleted,
                                    NoOfEvaluators= osr.NoOfEvaluators,
                                    DepartmentDecison= osr.DepartmentDecison,
                                    EvaluationTeamMembers= osr.EvaluationTeamMembers,
                                    ExchangeActor= osr.ExchangeActor,
                                    InfoProvidersName= osr.InfoProvidersName,
                                    InfoProvidersCount= osr.InfoProvidersCount,
                                    UpdatedUserId= osr.UpdatedUserId,
                                    IsActive= osr.IsActive,
                                    TeamDecision= osr.TeamDecision,
                                    TeamLeaderDecision= osr.TeamLeaderDecision,
                                    UpdatedDateTime=osr.UpdatedDateTime
          })
                                  .FirstOrDefault();
                return Task.FromResult(data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<OversightReportMOnthlySummaryModel>> GetMonthlySummary(string lang, DateTime from, 
            DateTime to, int type, SearchScheduleModel filter)
        {
            try
            {
                List< OversightReportMOnthlySummaryModel> data = new List<OversightReportMOnthlySummaryModel>();
                data = await _context.OversightReportMOnthlySummaryModel.FromSqlRaw("exec dbo.pMonthlySummury {0},{1},{2},{3}", from, to, lang,type).ToListAsync();

                var pagedResult = data.AsQueryable().Paging(filter.PageCount, filter.PageNumber);
                return new PagedResult<OversightReportMOnthlySummaryModel>()
                {
                    Items = _mapper.Map<List<OversightReportMOnthlySummaryModel>>(pagedResult),
                    ItemsCount = data.Count()
                };
                //return data;
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
          
        }

        public Task<bool> EditFollowupBycheckList(OversightReportDetail data)
        {
            try
            {
                OversightReportDetail oldData = _context.OversightReportDetail
                    .Where(de=>de.OversightFollowUpDetaiId==data.OversightFollowUpDetaiId).FirstOrDefault();
                if (oldData != null)
                {
                    oldData.WeightPerCriteria = data.WeightPerCriteria;
                    oldData.GivenSuggesiton = data.GivenSuggesiton;
                    oldData.Investigation = data.Investigation;
                    oldData.UpdatedDateTime = DateTime.UtcNow;
                    oldData.UpdatedUserId = data.UpdatedUserId;
                    _context.SaveChanges();
                    return Task.FromResult(true);
                }
                else
                {

                    return Task.FromResult(false);
                }

            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OversiteFollowUpFinalDecisionModel> GetFinalDecisionOfOversiteFollowup(int followupId)
        {
            try
            {
                OversiteFollowUpFinalDecisionModel data = new OversiteFollowUpFinalDecisionModel();

                OversightFollowUpReport da = await _context.OversightFollowUpReport
                    .Where(d => d.FollowUpId == followupId).FirstOrDefaultAsync();
                if (da != null)
                {
                    data.FinalDecision = da.TeamDecision;
                    data.InfoProvidersCount = da.InfoProvidersCount;
                    data.InfoProvidersName = da.InfoProvidersName;
                    data.NoOfEvaluators = da.NoOfEvaluators;
                        data.Remark = da.Remark;
                    data.Synopsis = da.Synopsis;
                }


                return data;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<bool> EditFollowup(OversightFollowUpReport data)
        {
            try
            {
                OversightFollowUpReport oldData = _context.OversightFollowUpReport
                    .Where(de => de.FollowUpId == data.FollowUpId).FirstOrDefault();
                if (oldData != null)
                {
                    //oldData.TeamDecision = data.TeamDecision;
                   // oldData.Remark = data.Remark;
                    oldData.Synopsis = data.Synopsis;
                    //oldData.InfoProvidersCount = data.InfoProvidersCount;
                    oldData.InfoProvidersName = data.InfoProvidersName;
                    oldData.UpdatedDateTime = DateTime.UtcNow;
                    oldData.UpdatedUserId = data.UpdatedUserId;
                    //_context.Entry(data).State = EntityState.Modified;
                    _context.SaveChanges();
                    return Task.FromResult(true);
                }
                else
                {

                    return Task.FromResult(false);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Oversight_report_criteriaDTO>> GetallCriteria()
        {
            try
            {
                var data = await (from osr in _context.Oversight_report_criteria
                                 .Where(report => report.IsDeleted == false)
                                  join sa in _context.Lookup
                                 on osr.ExchangeActorTypeId equals sa.LookupId
                                  orderby osr.ExchangeActorTypeId ascending
                                  select new Oversight_report_criteriaDTO
                                  {
                                      CriteriaId = osr.CriteriaId,
                                      DescriptionEng = osr.DescriptionEng,
                                      DescriptionAmh = osr.DescriptionAmh,
                                      CriteriaNameAmh = osr.CriteriaNameAmh,
                                      CriteriaNameEng = osr.CriteriaNameEng,
                                      Weight = osr.Weight,
                                      CriteriaType = osr.CriteriaTypeId,
                                 
                                      ExchangeActorTypeId = osr.ExchangeActorTypeId,
                                      ExchangeActorDescriptionAmh = sa.DescriptionAmh,
                                      ExchangeActorDescriptionEng = sa.DescriptionEng
                                  })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<OversightReportViewModel>> GetIncompleteEvaluations(string lang, int type, SearchScheduleModel filter)
        {
            try
            {
                //List<OversightReportViewModel> incomplete = new List<OversightReportViewModel>();
                var incomplete = await (from inc in _context.OversightFollowUpReport 
                                        .Where(res=>res.TeamDecision== (int)OversightFollowUpDecisionStatus.incomplete)
                                        .Where(ty=>ty.FollowUpTypeId==type)
                                        join lo in _context.Lookup on inc.TeamDecision  equals lo.LookupId 
                                        select new OversightReportViewModel
                    {
                    TeamDecision= (lang == "et") ? lo.DescriptionAmh: lo.DescriptionEng,
                    Currentstatus= inc.Currentstatus,
                    FollowUpId=inc.FollowUpId,
                    ExchangeActorId=inc.ExchangeActorId.ToString(),
                    ReportedBy= inc.ReportedBy,
                    Remark=inc.Remark,
                    ExchangeActor= inc.ExchangeActor,
                    Synopsis= inc.Synopsis,
                 
                    DateOfInspection = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(inc.DateOfInspection).ToString()) : inc.DateOfInspection.ToString(),
                }).ToListAsync();

                var pagedResult = incomplete.AsQueryable().Paging(filter.PageCount, filter.PageNumber);
                return new PagedResult<OversightReportViewModel>()
                {
                    Items = _mapper.Map<List<OversightReportViewModel>>(pagedResult),
                    ItemsCount = incomplete.Count()
                };      
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<OversightReportDetailViewModel>>  GetEvaluationDetails(int id,string language)
        {
            try
            {
                List<OversightReportDetailViewModel> reportDetail = new List<OversightReportDetailViewModel>();
                reportDetail = await (from de in _context.OversightReportDetail
                                    .Where(osfr => osfr.FollowUpId == id)
                                      join cr in _context.Oversight_report_criteria
                                      on de.CriteriaId equals cr.CriteriaId
                                      select new OversightReportDetailViewModel
                                      {
                                          CriteriaId = (language == "et") ? cr.CriteriaNameAmh : cr.CriteriaNameEng,
                                          FollowUpId = de.FollowUpId,
                                          GivenSuggesiton = de.GivenSuggesiton,
                                          Investigation = de.Investigation,
                                          score = de.WeightPerCriteria.ToString() + "/" + cr.Weight.ToString(),
                                          OversightFollowUpDetaiId=de.OversightFollowUpDetaiId,
                                          Remark=de.Remark

                                      }).ToListAsync();
                              
                              

                return  reportDetail;

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OversightReportDashboardDataViewModel> GetDashboardData()
        {
            try
            {
                var data = await (from osrd in _context.OversightFollowUpReport
                                  join osrd_details in _context.OversightReportDetail 
                                  on osrd.FollowUpId equals osrd_details.FollowUpId into Details
                                  from m in Details.DefaultIfEmpty()
                                  join osrd_criteria in _context.Oversight_report_criteria 
                                  on m.CriteriaId equals osrd_criteria.CriteriaId into criteria
                                  from cr in criteria.DefaultIfEmpty()
                                  select new OversightReportDashboardDataViewModel
                                  {
                                      CompletedEvaluations = _context.OversightFollowUpReport
                                             .Select(f => f.FollowUpId).Count(),

                                      DaysOnMission = _context.OversightFollowUpReport
                                              .Select(fd => fd.DateOfInspection).Distinct().Count(),
                                               ExchangeActors = _context.OversightFollowUpReport
                                               .Select(fx => fx.ExchangeActorId).Distinct().Count(),
                                      Totalcriteria = _context.OversightReportDetail
                                                    .Select(fc => fc.CriteriaId).Distinct().Count(),
                                      WarningsGiven = _context.OversightFollowUpReport
                                                     .Where(xg => xg.TeamDecision != 0)
                                                     .Select(f => f.TeamDecision).Distinct().Count(),
                                      ExchangeActorsEvaluated = _context.OversightFollowUpReport
                                                    .Select(f => f.ExchangeActorId).Distinct().Count(),
                                  }).FirstOrDefaultAsync();

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<EvaluationsByDate>> GetEvaluationsCountByDate(PaginateModel model)
        {
            try
            {
                List<EvaluationsByDate> data = new List<EvaluationsByDate>();
                data = await (from osr in _context.OversightFollowUpReport
                               .Where(da => da.Currentstatus == true)
                               .OrderByDescending(d => d.FollowUpId)
                               .GroupBy(d => d.DateOfInspection)
                              select new EvaluationsByDate
                              {
                                  Date = (model.Language == "et") ? EthiopicDateTime
                                                .TranslateEthiopicDateMonth(
                                      EthiopicDateTime.GetEthiopicDate(osr.Key.Date).ToString())
                                                : osr.Key.Date.ToShortDateString(),
                                  FollowUpCount = osr.Select(p => p.FollowUpId).Distinct().Count()
                              })
                              .ToListAsync();
                var pagedResult = data.AsQueryable().Paging(model.PageCount, model.PageNumber);
                return new PagedResult<EvaluationsByDate>()
                {
                    Items = _mapper.Map<List<EvaluationsByDate>>(pagedResult),
                    ItemsCount = data.Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OversightReportDecisionRecord> GetLastEvaluationDecision(Guid exchangeActorId, string lang)
        {
            try
            {
                var data = await (from osr in _context.OversightFollowUpReport
                .Where(osrd => osrd.ExchangeActorId == exchangeActorId && osrd.TeamDecision != (int)OversightFollowUpDecisionStatus.incomplete)
                                  join _lookup in _context.Lookup on osr.TeamDecision equals _lookup.LookupId

                                  orderby osr.DateOfInspection descending
                                  select new OversightReportDecisionRecord
                                  {
                                      DecisionId = osr.TeamDecision,
                                      DecisionDescription = (lang == "et") ? _lookup.DescriptionAmh : _lookup.DescriptionEng,
                                      LastDateOfInspection = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),
                                  }).FirstOrDefaultAsync();
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;

                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<OversightReportViewModel> SearchReports(OversightReportSearchCriteria Criteria)
        {
            try
            {
                var _report = new OversightReportViewModel();
                var curdata = Convert.ToDateTime(Criteria.DateOfInspection.AddDays(1)).Date;
                _report = await (from osr in _context.OversightFollowUpReport
                                  .Where(report => report.DateOfInspection.Date == Convert.ToDateTime(Criteria.DateOfInspection.AddDays(1)).Date &&
                                  //report.ExchangeActorId == Criteria.ExchangeActorId && report.TeamDecision!= (int)OnsightFollowUpStatus.Started)
                                  report.FollowUpTypeId==Criteria.FollowUpTypeId &&
                                  report.ExchangeActorId == Criteria.ExchangeActorId)
                                 join sa in _context.Lookup
                                 on osr.TeamDecision equals sa.LookupId
                                 select new OversightReportViewModel
                                 {
                                     FollowUpId = osr.FollowUpId,
                                     ExchangeActorId = (Criteria.Lang == "et") ? osr.ExchangeActor.OrganizationNameAmh : osr.ExchangeActor.OrganizationNameEng,
                                     ReportedBy = osr.ReportedBy,
                                     Remark = osr.Remark,
                                     Currentstatus = osr.Currentstatus,
                                     TeamDecision = (Criteria.Lang == "et") ? sa.DescriptionAmh : sa.DescriptionEng,
                                     Synopsis = osr.Synopsis,
                                     DateOfInspection = (Criteria.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),

                                 })
                  .FirstOrDefaultAsync();
                return _report;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<OversightReportMasterData>> GeneralSearchReports(OvsersightReportGeneralSearchCriteria criteria)
        {
            try
            {

                int totalSearchResult = 0;
                var empty = Guid.Empty;
                var ActualDate = criteria.ToDate.AddDays(1);

                List<OversightReportMasterData> ReportList;
                ReportList = await (from osr in _context.OversightFollowUpReport
                                  .Where(osrd => (criteria.ExchangeActorId == Guid.Empty 
                                  || osrd.ExchangeActorId == criteria.ExchangeActorId)
                                  &&( osrd.FollowUpTypeId== criteria.FollowUpType)                     
                                  && (criteria.FinalDecision == 0 || osrd.TeamDecision == criteria.FinalDecision)
                                  && (criteria.FromDate == null || osrd.DateOfInspection >= criteria.FromDate.AddDays(1))
                                 && (criteria.ToDate == null || osrd.DateOfInspection <= ActualDate.AddDays(1))
                                 && (osrd.TeamDecision != (int)OversightFollowUpDecisionStatus.incomplete)
                                    )
                                   .Include(c => c.ExchangeActor)
                                   .OrderByDescending(date => date.DateOfInspection)
                                    join sa in _context.Lookup
                                    on osr.TeamDecision equals sa.LookupId
                                    join lo in _context.Lookup on osr.TeamLeaderDecision equals lo.LookupId into tedec
                                    from lo in tedec.DefaultIfEmpty()
                                    join _lo in _context.Lookup on osr.DepartmentDecison equals _lo.LookupId into ded from _lo in ded.DefaultIfEmpty()


                                    select new OversightReportMasterData
                                    {
                                        FollowUpId = osr.FollowUpId,
                                        ReportedBy = osr.ReportedBy,
                                        Remark = osr.Remark,
                                        Currentstatus = osr.Currentstatus ?? false,
                                        TeamDecision = (criteria.Language == "et") ? sa.DescriptionAmh : sa.DescriptionEng,
                                        TeamLeaderDecision = (criteria.Language == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                        DepartmentDecison = (criteria.Language == "et") ? _lo.DescriptionAmh : _lo.DescriptionEng,

                                        Synopsis = osr.Synopsis,
                                        DateOfInspection = (criteria.Language == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),
                                        ExchangeActorId = osr.ExchangeActorId,
                                        ExchangeActor = osr.ExchangeActor
                                    })
                      .ToListAsync();
                        totalSearchResult = ReportList.Count();

                        var pagedResult = ReportList.AsQueryable().Paging(criteria.PageCount, criteria.PageNumber);
                        return new PagedResult<OversightReportMasterData>()
                        {
                            Items = _mapper.Map<List<OversightReportMasterData>>(pagedResult),
                            ItemsCount = ReportList.Count()
                        };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public async Task<bool?> GetCurrentStatus(int followupId)
        {
            try
            {
                OversightFollowUpReport currentData = await _context.OversightFollowUpReport.FirstOrDefaultAsync(osrda => osrda.FollowUpId == followupId);
                if (currentData != null)
                {
                    return currentData.Currentstatus;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> MarkReportComplete(CompletedoversitReportData data)
        {
            try
            {
                OversightFollowUpReport oldreport = await _context.OversightFollowUpReport.FirstOrDefaultAsync(osrda => osrda.FollowUpId == data.FollowUpId);
                if (oldreport != null)
                {
                    oldreport.Currentstatus = true;
                    oldreport.NoOfEvaluators = data.NoOfEvaluators;
                    oldreport.TeamDecision = data.FinalDecision;
                    oldreport.Synopsis = data.Synopsis;
                    oldreport.Remark = data.Remark;
                    oldreport.InfoProvidersCount = data.InfoProvidersCount;
                    oldreport.EvaluationTeamMembers = data.EvaluationTeamMembers;
                    oldreport.InfoProvidersName = data.InfoProvidersName;
                    oldreport.ReportedBy = data.ReportedBy;
                    oldreport.CreatedUserId = data.ReportedBy;
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> ReportAsOfficeIsClosed(OfficeClosedData data)
        {
            try
            {


                OversightFollowUpReport officeclosed = new OversightFollowUpReport
                {
                    Currentstatus = true,
                    ExchangeActorId= data.ExchangeActorId,
                    NoOfEvaluators = data.NoOfEvaluators,
                    TeamDecision = data.FinalDecision,
                    Synopsis = data.Synopsis,
                    Remark = data.Remark,
                    InfoProvidersCount = data.InfoProvidersCount,
                    EvaluationTeamMembers = data.EvaluationTeamMembers,
                    InfoProvidersName = data.InfoProvidersName,
                    ReportedBy = data.ReportedBy,
                    CreatedUserId = data.ReportedBy,
                    FollowUpTypeId = data.FollowUpTypeId,
                    OfficeClosed = true,
                    DateOfInspection = data.DateOfInspection.AddDays(1)
                };
                _context.OversightFollowUpReport.Add(officeclosed);
                await _context.SaveChangesAsync();
                return officeclosed.FollowUpId;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            }




                public async Task<bool> UpdateCriteriaAsync(Oversight_report_criteria postedData)
                  {
            
                try
                {
                

                var oldCritiera = _context.Oversight_report_criteria
                    .Where(os => os.CriteriaId == postedData.CriteriaId)
                    .FirstOrDefault();

                    if (oldCritiera != null)
                    {

                 
                    oldCritiera.CriteriaId = postedData.CriteriaId;
                    oldCritiera.CriteriaNameAmh = postedData.CriteriaNameAmh;
                    oldCritiera.CriteriaNameEng = postedData.CriteriaNameEng;

                    oldCritiera.CriteriaTypeId = postedData.CriteriaTypeId;

                    oldCritiera.DescriptionEng = postedData.DescriptionEng;
                    oldCritiera.DescriptionAmh = postedData.DescriptionAmh;

                    oldCritiera.UpdatedDateTime = DateTime.UtcNow;
                    oldCritiera.UpdatedUserId = postedData.UpdatedUserId;
                    oldCritiera.IsActive = true;
                    oldCritiera.Weight = postedData.Weight;
                    oldCritiera.IsDeleted = false;
                    oldCritiera.ExchangeActorTypeId = postedData.ExchangeActorTypeId;
                
                        //_context.Entry(newcritia).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        return true;
                }
                

                    return false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            
        }
        public async Task<bool> Deletecriteria(int criteriaId)
        {
            try
            {
                var oldCriteria = await _context.Oversight_report_criteria.FirstOrDefaultAsync(Inj =>
                    Inj.CriteriaId == criteriaId);
                if (oldCriteria != null)
                {
                    oldCriteria.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostOversightReportCriteria(Oversight_report_criteria postedData)
        {
            try
            {

                var newData = _mapper.Map<Oversight_report_criteria>(postedData);
                _context.Oversight_report_criteria.Add(newData);
                await _context.SaveChangesAsync();
                return newData.CriteriaId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<OversightReportDetail>> CheckDetailReport(OversightDetailReportSearchCriteria criteria)
        {
            try
            {

                var data = await (from osrd in _context.OversightReportDetail
                                 .Where(eacd => eacd.FollowUpId == criteria.FollowupId && eacd.CriteriaId == criteria.CriteriaId)
                                  select new OversightReportDetail
                                  {
                                      WeightPerCriteria = osrd.WeightPerCriteria,
                                      FollowUpId = osrd.FollowUpId,
                                      CriteriaId = osrd.CriteriaId,
                                      GivenSuggesiton = osrd.GivenSuggesiton,
                                      Investigation = osrd.Investigation,
                                      Remark = osrd.Remark,
                                      OversightFollowUpDetaiId = osrd.OversightFollowUpDetaiId

                                  })
                  .ToListAsync();

                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<OversightReportDetail> GetDetailsByFollowUpId(int followupId)
        {
            try
            {
                var data = new OversightReportDetail();
                data = await (from detail in _context.OversightReportDetail
                              .Where(osred => osred.FollowUpId == followupId && osred.IsDeleted == false)
                              select new OversightReportDetail
                              {
                                  OversightFollowUpDetaiId = detail.OversightFollowUpDetaiId,
                                  FollowUpId = detail.FollowUpId,
                                  CriteriaId = detail.CriteriaId,
                                  GivenSuggesiton = detail.GivenSuggesiton,
                                  Investigation = detail.Investigation,
                                  Remark = detail.Remark,
                                  WeightPerCriteria = detail.WeightPerCriteria
                              }
                              ).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<float> GetTotalweight(int followupId)
        {
            try
            {
                var data = await _context.OversightReportDetail
                                 .Where(osred => osred.FollowUpId == followupId)
                                 .Select(os => (float?)os.WeightPerCriteria)
                                 .DefaultIfEmpty(0)
                                 .SumAsync();

                return (float)data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<ReportSearchResult>> ReportLsit(string lang)
        {
            try
            {
                var data = await (from osr in _context.OversightFollowUpReport
                                  select new ReportSearchResult
                                  {
                                      FollowUpId = osr.FollowUpId,
                                      ReportedBy = osr.ReportedBy,
                                      Remark = osr.Remark,
                                      DateOfInspection = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),
                                  })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<OversightReportMasterDataViewModel> GetReportDetails(string lang, int FollowUpId)
        {
            try
            {
                float summedValue=  _context.OversightReportDetail.Where(er => er.FollowUpId == FollowUpId).Sum(s => s.WeightPerCriteria);
                var CurrentOversightData = await (from osr in _context.OversightFollowUpReport
                                 .Where(osfr => osfr.FollowUpId == FollowUpId)
                                     .Include(ex => ex.ExchangeActor)
                                                  join sa in _context.Lookup
                                                  on osr.TeamDecision equals sa.LookupId  
                                                  join _users in _context.Users on osr.CreatedUserId.ToString() equals _users.Id
                                                  select new OversightReportMasterDataViewModel
                                                  {
                                                      FollowUpId = osr.FollowUpId,
                                                      //ReportedBy = osr.EvaluationTeamMembers,
                                                      Remark = osr.Remark,
                                                      DateOfInspection = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),
                                                      Currentstatus = osr.Currentstatus,
                                                      ExchangeActorId = (lang == "et") ? osr.ExchangeActor.OrganizationNameAmh : osr.ExchangeActor.OrganizationNameEng,
                                                      Synopsis = osr.Synopsis,
                                                      TeamDecision = (lang == "et") ? sa.DescriptionAmh : sa.DescriptionEng,
                                                      ExchangeActor = osr.ExchangeActor,
                                                      Weight= summedValue,
                                                  })
                                  .FirstOrDefaultAsync();

              

                var data = await (from osr in _context.OversightFollowUpReport
                                  .Where(osfr => osfr.FollowUpId == FollowUpId)
                                      .Include(ex => ex.ExchangeActor)
                                  join sa in _context.Lookup
                                  on osr.TeamDecision equals sa.LookupId
                                  join rd in _context.OversightReportDetail
                                  on osr.FollowUpId equals rd.FollowUpId
                                  join _users in _context.Users on osr.CreatedUserId.ToString() equals _users.Id

                                  select new OversightReportMasterDataViewModel
                                  {
                                      FollowUpId = osr.FollowUpId,
                                      //ReportedBy = osr.EvaluationTeamMembers,
                                      Remark = osr.Remark,
                                      DateOfInspection = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(osr.DateOfInspection).ToString()) : osr.DateOfInspection.ToString(),
                                      Currentstatus = osr.Currentstatus,
                                      ExchangeActorId = (lang == "et") ? osr.ExchangeActor.OrganizationNameAmh : osr.ExchangeActor.OrganizationNameEng,
                                      Synopsis = osr.Synopsis,
                                      TeamDecision = (lang == "et") ? sa.DescriptionAmh : sa.DescriptionEng,
                                      ExchangeActor = osr.ExchangeActor,
                                      Weight = summedValue, 
                                  })
                                  .FirstOrDefaultAsync();
                var dtaa = data ?? CurrentOversightData;
                return dtaa;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ReportDetaitPerCriteriaDTO>> GetReportDetailsPerCritera(int id, string lang)
        {
            try
            {


                List<ReportDetaitPerCriteriaDTO> data = null;
                data = await (from osr in _context.OversightReportDetail
                                  .Where(osfr => osfr.FollowUpId == id)
                              join sa in _context.Oversight_report_criteria
                              on osr.CriteriaId equals sa.CriteriaId

                              select new ReportDetaitPerCriteriaDTO
                              {
                                  CriteriaId = osr.CriteriaId,
                                  GivenSuggestion = osr.GivenSuggesiton,
                                  OvRDetailId= osr.OversightFollowUpDetaiId,
                                  Investigation = osr.Investigation,
                                  CriteriaName = (lang == "et") ? sa.CriteriaNameAmh : sa.CriteriaNameEng,
                                  WeightPerCriteria = osr.WeightPerCriteria 
                              }).ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostOversightReport(OversightReportDTO postedData)
        {
            try
            {
                var newData = _mapper.Map<OversightFollowUpReport>(postedData);
                _context.OversightFollowUpReport.Add(newData);
                await _context.SaveChangesAsync();
                return newData.FollowUpId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetExecutedEvaluationsCount(int followupId)
        {
            try
            {
                var data = await (from osrD in _context.OversightReportDetail
                                  .Where(details => details.FollowUpId == followupId)
                                  select new OversightReportDetail
                                  {
                                      FollowUpId = osrD.FollowUpId,
                                      Remark = osrD.Remark,
                                      CriteriaId = osrD.CriteriaId,
                                      GivenSuggesiton = osrD.GivenSuggesiton,
                                      OversightFollowUpDetaiId = osrD.OversightFollowUpDetaiId,
                                      Investigation = osrD.Investigation,

                                  })
                                  .ToListAsync();
                return data.Count();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<OversightDashbordDataByDecision>> GetFollowupReportByFinalDecision()
        {
            try
            {
                List<OversightDashbordDataByDecision> data = new List<OversightDashbordDataByDecision>();

                data = await (from p in _context.OversightFollowUpReport
                              where p.TeamDecision!=(int)(OversightFollowUpDecisionStatus.incomplete)
                              group p by p.DepartmentDecison into pg
                              join bp in _context.Lookup on pg.FirstOrDefault().DepartmentDecison equals bp.LookupId
                              select new OversightDashbordDataByDecision
                              {
                                  Decision = bp.DescriptionAmh,
                                  DecisionCount = pg.Select(c => c.FollowUpId).Distinct().Count()
                              }).ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ReportStatusResult> SaveOversightReport(OversightReportDTO fullData)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    int followupId = 0;
                    var operationResult = new ReportStatusResult();
                    DateTime _date = new DateTime(fullData.DateOfInspection.Year, fullData.DateOfInspection.Month, fullData.DateOfInspection.Day, 0, 0, 0);


                    OversightFollowUpReport masterData = new OversightFollowUpReport
                    {
                        FollowUpId = fullData.FollowUpId,
                        DateOfInspection = _date.AddDays(1),
                        ExchangeActorId = fullData.ExchangeActorId,
                        Remark = fullData.Remark,
                        TeamDecision = fullData.TeamDecision,
                        ReportedBy = fullData.ReportedBy,
                        Synopsis = fullData.Synopsis,
                        Currentstatus = fullData.Currentstatus,
                        FollowUpTypeId = fullData.FollowUpTypeId
                    };

                    OversightReportDetail detailsData = new OversightReportDetail
                    {
                        WeightPerCriteria = fullData.DetailsWeightPerCritiera,
                        Remark = fullData.DetailsRemark,
                        GivenSuggesiton = fullData.DetailsGivenSuggestion,
                        CriteriaId = fullData.DetailsCriteriaId,
                        Investigation = fullData.DetailInvestigation,
                        CreatedUserId= fullData.ReportedBy
                    };


                    // Check if oversight followupid matches  an existing report
                    var existingReport = await _context.OversightFollowUpReport
                                           .FirstOrDefaultAsync(osr => osr.ExchangeActorId == masterData.ExchangeActorId 
                                           && osr.FollowUpTypeId== masterData.FollowUpTypeId
                                           && osr.DateOfInspection == masterData.DateOfInspection);

                    if (existingReport == null)
                    {
                        masterData.Currentstatus = false;
                        _context.OversightFollowUpReport.Add(masterData);
                        await _context.SaveChangesAsync();
                        followupId = masterData.FollowUpId;
                        detailsData.FollowUpId = followupId;
                        operationResult.FollowUpId = followupId;
                    }
                    else
                    {
                        followupId = existingReport.FollowUpId;
                        operationResult.Currentstatus = false;
                        operationResult.FollowUpId = followupId;

                    }
                    //check Detail Report if followupid already exists
                    var existingReportdetail = await _context.OversightReportDetail
                                               .FirstOrDefaultAsync(osrd => osrd.CriteriaId == detailsData.CriteriaId
                                               && osrd.FollowUpId == followupId);

                    if (existingReportdetail == null)
                    {
                        if (followupId != 0)
                        {
                            detailsData.FollowUpId = followupId;
                            _context.OversightReportDetail.Add(detailsData);
                            await _context.SaveChangesAsync();
                            operationResult.OvRDetailId = detailsData.OversightFollowUpDetaiId;
                        }

                    }
                    else
                    {
                        operationResult.OvRDetailId = existingReportdetail.OversightFollowUpDetaiId;

                    }
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    return operationResult;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        
    }
}
