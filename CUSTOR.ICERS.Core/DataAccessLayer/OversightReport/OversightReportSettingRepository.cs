﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.OversightReport
{
    public class OversightReportSettingRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public OversightReportSettingRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> AddDecisonType(OversightReportSetting data)
        {
            try
            {
                // data.DecisionId = new Guid();
                var newData = _mapper.Map<OversightReportSetting>(data);
                _context.OversightReportSetting.Add(newData);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<OversightReportSettingDTO>> GetDecisonTypes()
        {
            try
            {
                var data = await (from decsion in _context.OversightReportSetting
                                  select new OversightReportSettingDTO
                                  {
                                      DecisionId = decsion.DecisionId,
                                      DecisionNameAmh = decsion.DecisionNameAmh,
                                      DecisionDescriptionAmh = decsion.DecisionDescriptionAmh,
                                      DecisionNameEng = decsion.DecisionNameEng,
                                      DecisionDescriptionEng = decsion.DecisionDescriptionEng
                                  })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
