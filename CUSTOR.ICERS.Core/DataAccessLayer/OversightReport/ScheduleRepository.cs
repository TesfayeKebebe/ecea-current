﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using Microsoft.EntityFrameworkCore;

namespace CUSTOR.ICERS.Core.DataAccessLayer.OversightReport
{

    public class ScheduleRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ScheduleRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> UploadSchedule(OversightSchedule[] schedule)
        {
            try
            {
                var data = schedule;
                var newData = new OversightSchedule();
            
                foreach (var item in schedule)
                {
                    newData = _mapper.Map<OversightSchedule>(item);
                    var exchangeActor = await _context.ExchangeActor.FirstOrDefaultAsync(
                        ex => ex.Ecxcode.Equals(newData.Ecxcode.Trim(), StringComparison.OrdinalIgnoreCase));
                    if (exchangeActor != null)
                    {
                        newData.ExchangeActorId = exchangeActor.ExchangeActorId;
                    }
                    newData.CreatedUserId = Guid.NewGuid();
                    newData.CreatedDateTime = DateTime.Now;
                    newData.IsActive = true;
                    newData.IsDeleted = false;
                    _context.OversightSchedule.Add(newData);
                    await _context.SaveChangesAsync();
                }
                return newData.ScheduleId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> UpdateSingleSchedule(OversightSchedule data)
        {
            try
            {
                OversightSchedule selectedSchedule = await _context.OversightSchedule.FirstOrDefaultAsync(sc => sc.ScheduleId == data.ScheduleId);
                if (selectedSchedule != null)
                {
                    selectedSchedule.ScheduleMonth = data.ScheduleMonth;
                    selectedSchedule.ScheduleYear = data.ScheduleYear;
                    selectedSchedule.ScheduleDate = data.ScheduleDate;
                    selectedSchedule.Address = data.Address;
                    selectedSchedule.ExchangeActor = data.ExchangeActor;
                    selectedSchedule.IsActive = true;
                    selectedSchedule.IsDeleted = false;
                    selectedSchedule.UpdatedUserId = data.UpdatedUserId;
                    selectedSchedule.UpdatedDateTime = DateTime.UtcNow;
                    _context.SaveChanges();
                    return true;
                }
                else
                {

                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> DeleteSchedule(int id)
        {
            var Schedule =await (from sc in _context.OversightSchedule
                            where sc.ScheduleId == id
                            select sc
                                  ).FirstOrDefaultAsync();
            if (Schedule != null)
            {
                _context.OversightSchedule.Remove(Schedule);
               await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<PagedResult<OversightSchedule>> GetCurrentMonthSchedule(SearchScheduleModel filter)
        {
            try
            {

                List<OversightSchedule> Schedulelist = new List<OversightSchedule>();
                Schedulelist = await (from sc in _context.OversightSchedule
                                      where sc.ScheduleMonth == filter.ScheduleMonth && sc.ScheduleYear == filter.ScheduleYear
                                      select sc
                                      ).ToListAsync();


                var pagedResult = Schedulelist.AsQueryable().Paging(filter.PageCount, filter.PageNumber);
                return new PagedResult<OversightSchedule>()
                {
                    Items = _mapper.Map<List<OversightSchedule>>(pagedResult),
                    ItemsCount = Schedulelist.Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateSchedule(OversightSchedule[] schedule)
        {
            try
            {
                var data = schedule;
                var newData = new OversightSchedule();

                _context.OversightSchedule.RemoveRange(_context.OversightSchedule.Where(x => x.ScheduleYear == schedule[0].ScheduleYear && x.ScheduleMonth==schedule[0].ScheduleMonth));
                _context.SaveChanges();

                foreach (var item in schedule)
                {
     
                    newData = _mapper.Map<OversightSchedule>(item);
                    newData.CreatedUserId = Guid.NewGuid();
                    newData.CreatedDateTime = DateTime.Now;
                    newData.IsActive = true;
                    newData.IsDeleted = false;
                    _context.OversightSchedule.Add(newData);
                   await _context.SaveChangesAsync();
                }
                return newData.ScheduleId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<OversightSchedule>> GetScheduleByMonth(SearchScheduleModel _scheduleModel)
        {
            try
            {
                List<OversightSchedule> currentData = null;
                currentData = await (from schedule in _context.OversightSchedule
                                     .Where(da => (_scheduleModel.ScheduleMonth == 0 || da.ScheduleMonth == _scheduleModel.ScheduleMonth)
                                     && (_scheduleModel.ScheduleYear == 0 || da.ScheduleYear == _scheduleModel.ScheduleYear))
                                     select new OversightSchedule
                                     {
                                         ExchangeActor = schedule.ExchangeActor,
                                         ScheduleYear = schedule.ScheduleYear,
                                         Address = schedule.Address,
                                         ScheduleId = schedule.ScheduleId,
                                         CreatedUserId = schedule.CreatedUserId,
                                         ScheduleDate = schedule.ScheduleDate,
                                         ScheduleMonth = schedule.ScheduleMonth,
                                         Ecxcode=schedule.Ecxcode
                                     }).ToListAsync();

                var pagedResult = currentData.AsQueryable().Paging(_scheduleModel.PageCount, _scheduleModel.PageNumber);
                return new PagedResult<OversightSchedule>()
                {
                    Items = _mapper.Map<List<OversightSchedule>>(pagedResult),
                    ItemsCount = currentData.Count()
                };
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<SearchScheduleByMonthsModel>> GetScheduleByMonthly(SearchScheduleModel _scheduleModel)
        {
            List<SearchScheduleByMonthsModel> scheduletCount;

            scheduletCount = await _context.OversightSchedule.GroupBy(r => new { r.ScheduleMonth, r.ScheduleYear })
                                   .Select(r => new SearchScheduleByMonthsModel
                                   {
                                       ScheduleMonth = r.Key.ScheduleMonth,
                                       ScheduleYear = r.Key.ScheduleYear,
                                       scheduleCount = r.Select(c => c.ScheduleId).Distinct().Count(),
                                   }).ToListAsync();

            var pagedResult = scheduletCount.AsQueryable().Paging(_scheduleModel.PageCount, _scheduleModel.PageNumber);
            return new PagedResult<SearchScheduleByMonthsModel>()
            {
                Items = _mapper.Map<List<SearchScheduleByMonthsModel>>(pagedResult),
                ItemsCount = scheduletCount.Count()
            };
            //return scheduletCount;
        }

        public async Task<List<SearchScheduleByMonthsModel>> GetScheduleCountByMonth(SearchScheduleModel scheduleModel)
        {
            List<SearchScheduleByMonthsModel> scheduletCount;

            scheduletCount =await _context.OversightSchedule.GroupBy(r => new { r.ScheduleMonth, r.ScheduleYear })
                                   .Select(r => new SearchScheduleByMonthsModel
                                   {
                                       ScheduleMonth = r.Key.ScheduleMonth,
                                       ScheduleYear = r.Key.ScheduleYear,
                                       scheduleCount = r.Select(c => c.ScheduleId).Distinct().Count(), 
                                   }).ToListAsync();


            return scheduletCount;
        }

        public async Task<PagedResult<OversightSchedule>> GetSchedule(PaginateModel model)
        {
            try
            {
                int totalSearchResult = 0;

                List<OversightSchedule> scheduletList;
                scheduletList = await (from scheduleData in _context.OversightSchedule
                                       select new OversightSchedule
                                       {
                                           ScheduleId = scheduleData.ScheduleId,
                                           Address = scheduleData.Address,
                                           ExchangeActor = scheduleData.ExchangeActor,
                                           ScheduleYear = scheduleData.ScheduleYear,
                                           ScheduleDate = scheduleData.ScheduleDate,
                                           ScheduleMonth = scheduleData.ScheduleMonth,
                                           Ecxcode = scheduleData.Ecxcode

                                       }).ToListAsync();

                totalSearchResult = scheduletList.Count();

                var pagedResult = scheduletList.AsQueryable().Paging(model.PageCount, model.PageNumber);
                return new PagedResult<OversightSchedule>()
                {
                    Items = _mapper.Map<List<OversightSchedule>>(pagedResult),
                    ItemsCount = scheduletList.Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}