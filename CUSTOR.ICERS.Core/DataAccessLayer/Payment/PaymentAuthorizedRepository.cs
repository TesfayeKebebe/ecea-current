﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Payment
{
    public class PaymentAuthorizedRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public PaymentAuthorizedRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<IEnumerable<PaymentAuthorizationStatusDTO>> GetPaymentAuthorizationStatus(Guid serviceApplicaitonId)
        {
            try
            {
                var paymetAuthorizationStatus2 = await _context.PaymentAuthorized
                    .Where(pa => pa.ServiceApplicationId == serviceApplicaitonId)
                    .Include(pa => pa.ServiceApplication)
                    .Select(data => new PaymentAuthorizationStatusDTO
                    {
                        DisableNextButton = data.IsPaid ? false : true,
                    })
                    .AsNoTracking()
                    .ToListAsync();


                return paymetAuthorizationStatus2;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<PagedResult<AuthorizedPaymentOrderSearchDTO>> SearchAuthorizedPaymentOrder(PaymentQueryParameters customerQueryParameters)
        {
            try
            {
                int totalSearchResult = 0;

                List<AuthorizedPaymentOrderSearchDTO> athorizedPaymentOrderLis = null;

                if (customerQueryParameters.Lang == "et")
                {
                    athorizedPaymentOrderLis = await SearchAuthorizedPaymentOrderInAmharic(customerQueryParameters);
                }
                else
                {
                    athorizedPaymentOrderLis = await SearchAuthorizedPaymentOrderInEnglish(customerQueryParameters);
                }

                totalSearchResult = athorizedPaymentOrderLis.Count();

                var pagedResult = athorizedPaymentOrderLis.AsQueryable().Paging(customerQueryParameters.PageCount, customerQueryParameters.PageNumber);

                return new PagedResult<AuthorizedPaymentOrderSearchDTO>()
                {
                    Items = _mapper.Map<List<AuthorizedPaymentOrderSearchDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<List<AuthorizedPaymentOrderSearchDTO>> SearchAuthorizedPaymentOrderInAmharic(PaymentQueryParameters customerQueryParameters)
        {
            try
            {
                CVGeez objGeez = new CVGeez();

                List<AuthorizedPaymentOrderSearchDTO> athorizedPaymentOrderList =

                    await (from sa in _context.ServiceApplication
                           join om in _context.OwnerManager on
                           
                           new { sa.ExchangeActorId, IsManager = true } equals new { om.ExchangeActorId, om.IsManager } into ownManager
                           from ownerManager in ownManager.DefaultIfEmpty()
                           where sa.IsPaymentAuthorized == true && sa.IsActive == true && sa.IsDeleted == false && 
                           (string.IsNullOrEmpty(customerQueryParameters.From) || sa.AuthorizedDateTime >= DateTime.Parse(customerQueryParameters.From)) &&
                           (string.IsNullOrEmpty(customerQueryParameters.To) || sa.AuthorizedDateTime <= DateTime.Parse(customerQueryParameters.To)) &&
                           (customerQueryParameters.PaymentStatus == null || (sa.IsPaid == (customerQueryParameters.PaymentStatus == 1 ? true : false))) &&
                           (customerQueryParameters.ServiceType == null || sa.Service.ServiceId == customerQueryParameters.ServiceType) &&
                           (string.IsNullOrEmpty(customerQueryParameters.Ecxcode) || sa.ExchangeActor.Ecxcode.Contains(customerQueryParameters.Ecxcode.Trim())) && 
                           (sa.ExchangeActor.OrganizationNameAmhSort.Contains(objGeez.GetSortValueU(customerQueryParameters.OrganizationName == null ? string.Empty : customerQueryParameters.OrganizationName.Trim())))
                           orderby sa.AuthorizedDateTime descending
                           select new AuthorizedPaymentOrderSearchDTO
                           {
                               ServiceId = sa.ServiceId,
                               OrganizationName = (sa.ExchangeActor.CustomerTypeId == (int)Enum.CustomerType.Representative || sa.ExchangeActor.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative) ?
                               ownerManager.FirstNameAmh + " " + ownerManager.FatherNameAmh + " " + ownerManager.GrandFatherNameAmh : sa.ExchangeActor.OrganizationNameAmh,
                               ServiceType = sa.Service.DescriptionAmh,
                               ServiceApplicationId = sa.ServiceApplicationId,
                               IsPaid = sa.IsPaid.GetValueOrDefault(false),
                               AuthorizedDate = (sa.AuthorizedDateTime !=null)?EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(sa.AuthorizedDateTime.Value).ToString()):"",
                               ExchangeActorId = sa.ExchangeActorId,
                               ExchangeActorType = sa.ExchangeActor.CustomerType.DescriptionAmh,
                               CustomerTypeId = sa.CustomerTypeId,
                               
                           })
                            .AsNoTracking()
                            .ToListAsync();

                return athorizedPaymentOrderList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<List<AuthorizedPaymentOrderSearchDTO>> SearchAuthorizedPaymentOrderInEnglish(PaymentQueryParameters customerQueryParameters)
        {
            try
            {
                List<AuthorizedPaymentOrderSearchDTO> athorizedPaymentOrderLis =
                    await (from sa in _context.ServiceApplication
                           join om in _context.OwnerManager on
                           new { sa.ExchangeActorId, IsManager = true } equals new { om.ExchangeActorId, om.IsManager } into ownManager
                           from ownerManager in ownManager.DefaultIfEmpty()
                           where sa.IsPaymentAuthorized == true && sa.IsActive == true && sa.IsDeleted == false && 
                           (string.IsNullOrEmpty(customerQueryParameters.From) || sa.AuthorizedDateTime >= DateTime.Parse(customerQueryParameters.From)) &&
                           (string.IsNullOrEmpty(customerQueryParameters.To) || sa.AuthorizedDateTime <= DateTime.Parse(customerQueryParameters.To)) &&
                           (customerQueryParameters.PaymentStatus == null || (sa.IsPaid == (customerQueryParameters.PaymentStatus == 1 ? true : false))) &&
                           (customerQueryParameters.ServiceType == null || sa.Service.ServiceId == customerQueryParameters.ServiceType) &&
                           (string.IsNullOrEmpty(customerQueryParameters.Ecxcode) || sa.ExchangeActor.Ecxcode.Contains(customerQueryParameters.Ecxcode.Trim())) &&
                           ((string.IsNullOrEmpty(customerQueryParameters.OrganizationName)) || sa.ExchangeActor.OrganizationNameEng.Contains(customerQueryParameters.OrganizationName))
                           orderby sa.AuthorizedDateTime descending
                           select new AuthorizedPaymentOrderSearchDTO
                           {
                               ServiceId = sa.ServiceId,
                               OrganizationName = (sa.ExchangeActor.CustomerTypeId == (int)Enum.CustomerType.Representative || sa.ExchangeActor.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative) ?
                               ownerManager.FirstNameEng + " " + ownerManager.FatherNameEng + " " + ownerManager.GrandFatherNameEng : sa.ExchangeActor.OrganizationNameEng,
                               ServiceType = sa.Service.DescriptionEng,
                               ServiceApplicationId = sa.ServiceApplicationId,
                               IsPaid = sa.IsPaid.GetValueOrDefault(false),
                               AuthorizedDate = sa.AuthorizedDateTime.GetValueOrDefault().ToString("MMMM dd, yyyy"),
                               ExchangeActorId = sa.ExchangeActorId,
                               ExchangeActorType = sa.ExchangeActor.CustomerType.DescriptionEng,
                               CustomerTypeId = sa.CustomerTypeId

                           })
                        .AsNoTracking()
                        .ToListAsync();

                return athorizedPaymentOrderLis;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> PostPaymentAuthorize(ServiceApplicationDTO postedPaymentAuthorization)
        {
            try
            {
                ServiceApplication toUpdateServiceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == postedPaymentAuthorization.ServiceApplicationId
                && sa.IsActive == true && sa.IsDeleted == false);

                if (toUpdateServiceApplication != null)
                {
                    toUpdateServiceApplication.IsPaid = false;
                    toUpdateServiceApplication.IsPaymentAuthorized = true;
                    toUpdateServiceApplication.AuthorizedDateTime = DateTime.Now;
                    toUpdateServiceApplication.Status = (int)ServiceApplicaitonStatus.SendForPayment; // send for payment

                    await _context.SaveChangesAsync();

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
    }
}
