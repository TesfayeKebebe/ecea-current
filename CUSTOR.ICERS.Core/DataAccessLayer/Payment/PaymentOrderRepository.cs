﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Payment
{
    public class PaymentOrderRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;


        public PaymentOrderRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Guid> PostPaymentOrder(PaymentOrderDTO postedPaymentOrder)
        {
            ServiceApplication serviceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa =>
            sa.IsActive == true && sa.IsDeleted == false && sa.ServiceApplicationId == postedPaymentOrder.ServiceApplicationId);

            if (serviceApplication != null)
            {
                serviceApplication.IsPaid = true;
                serviceApplication.UpdatedDateTime = DateTime.Now;
                serviceApplication.Status = (int)ServiceApplicaitonStatus.Paid;
                serviceApplication.CreatedUserId = postedPaymentOrder.CreatedUserId;
            }

            PaymentOrder paymentOrder = new PaymentOrder
            {
                PaymentOrderId = Guid.NewGuid(),
                ServiceApplicationId = postedPaymentOrder.ServiceApplicationId,
                ExchangeActorId = postedPaymentOrder.ExchangeActorId,
                ExchangeActorTypeId = postedPaymentOrder.ExchangeActorTypeId,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                ServiceId = serviceApplication.ServiceId,
                CreatedUserId = postedPaymentOrder.CreatedUserId
                
            };

            PaymentOrderDetail paymentOrderDetail = new PaymentOrderDetail
            {
                PaymentOrderDetailId = Guid.NewGuid(),
                PaymentOrderId = paymentOrder.PaymentOrderId,
                PaymentType = postedPaymentOrder.PaymentType,
                CheckNo = postedPaymentOrder.CheckNo,
                ReceiptNo = postedPaymentOrder.ReceiptNo,
                TotalAmount = postedPaymentOrder.TotalCost,
                CreatedUserId = postedPaymentOrder.CreatedUserId
            };

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.PaymentOrder.Add(paymentOrder);
                    _context.PaymentOrderDetail.Add(paymentOrderDetail);
                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return paymentOrder.PaymentOrderId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
    }
}
