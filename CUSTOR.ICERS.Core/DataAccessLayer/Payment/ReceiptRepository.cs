﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using System;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Payment
{
    public class ReceiptRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ReceiptRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostReceiptData(ReceiptDTO postedReceipt)
        {
            try
            {
                var receiptMapped = _mapper.Map<Receipt>(postedReceipt);

                _context.Add(receiptMapped);

                await _context.SaveChangesAsync();

                return receiptMapped.ReceiptId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
    }
}
