﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class CancellationRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private ExchangeActorRepository _exchangeActorRepository;

        public CancellationRepository(ECEADbContext context, IMapper mapper, ExchangeActorRepository exchangeActorRepository)
        {
            _context = context;
            _mapper = mapper;
            _exchangeActorRepository = exchangeActorRepository;
        }
        public async Task<ServiceStatusDTO> GetExchangeActorCancellationStatus(Guid exchangeActorId)
        {

            try
            {

                ServiceStatusDTO serviceStatus = await (from cancellation in _context.Cancellation
                                                        where cancellation.ExchangeActorId == exchangeActorId
                                                        select new ServiceStatusDTO
                                                        {
                                                            ExchangeActorStatus = cancellation.ExchangeActor.Status,
                                                            ServiceApplicationStatus = cancellation.ServiceApplication.Status
                                                        })
                                                        .AsNoTracking()
                                                        .FirstOrDefaultAsync();

                if (serviceStatus == null)
                {
                    return null;
                }

                return serviceStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<IActionResult> PostCancellationGeneralInfo(CancellationDTO postedCancellation)
        {


            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(postedCancellation.ExchangeActorId);

            if (customerStatusResponse.IsCancelled)
            {
                return new ObjectResult(customerStatusResponse);

            }


            Cancellation cancellation = null;

            cancellation = _mapper.Map<Cancellation>(postedCancellation);

            int generalInformation = (int)CancellationServiceApplicationSteps.General_Information;
            int prerequisite = (int)CancellationServiceApplicationSteps.Prerequisite;

            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                ExchangeActorId = postedCancellation.ExchangeActorId,
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = generalInformation,
                NextStep = prerequisite,
                ServiceId = (int)ServiceType.Cancellation,
                IsFinished = false,
                CreatedUserId = postedCancellation.CreatedUserId,
                UpdatedUserId = postedCancellation.CreatedUserId
            };

            cancellation.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            cancellation.CreatedUserId = postedCancellation.CreatedUserId;
            cancellation.UpdatedUserId = postedCancellation.CreatedUserId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.Cancellation.Add(cancellation);
                    _context.SaveChanges();
                    transaction.Commit();


                    return new ObjectResult(_mapper.Map<ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        public async Task<CancellationDTO> GetCancellationDataWithServiceApp(string lang, Guid serviceApplicationId)
        {
            try
            {
                CancellationDTO cancellationData = await (from cancellation in _context.Cancellation
                                                          where cancellation.IsActive == true && cancellation.IsDeleted == false && cancellation.ServiceApplicationId == serviceApplicationId
                                                          select new CancellationDTO
                                                          {
                                                              CancellationType = cancellation.CancellationType,
                                                              ExchangeActorName = cancellation.ExchangeActor.CustomerTypeId == 63 ? lang == "et" ?  // customer type is Reprsentative type
                                                              cancellation.ExchangeActor.OwnerManager.FirstNameAmh + " " + cancellation.ExchangeActor.OwnerManager.FatherNameAmh + " " + cancellation.ExchangeActor.OwnerManager.GrandFatherNameAmh :
                                                              cancellation.ExchangeActor.OwnerManager.FirstNameEng + " " + cancellation.ExchangeActor.OwnerManager.FatherNameEng + " " + cancellation.ExchangeActor.OwnerManager.GrandFatherNameEng :
                                                              lang == "et" ? cancellation.ExchangeActor.OrganizationNameAmh : cancellation.ExchangeActor.OrganizationNameEng,
                                                              Description = cancellation.Description,
                                                              ExchangeActorType = lang == "et" ? cancellation.ExchangeActor.CustomerType.DescriptionAmh : cancellation.ExchangeActor.CustomerType.DescriptionEng,
                                                              ExchangeActorId = cancellation.ExchangeActorId,
                                                              ServiceApplicationId = cancellation.ServiceApplicationId


                                                          }).AsNoTracking()
                                                            .FirstOrDefaultAsync();

                return cancellationData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<IActionResult> UpdateCancellation(CancellationDTO updatedCancellationData)
        {

            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(updatedCancellationData.ExchangeActorId);

            if (customerStatusResponse.IsCancelled)
            {
                return new ObjectResult(customerStatusResponse);

            }


            Cancellation oldCancellationData = await _context.Cancellation.FirstOrDefaultAsync(re => re.ServiceApplicationId == updatedCancellationData.ServiceApplicationId);

            ServiceApplication oldServiceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == updatedCancellationData.ServiceApplicationId);

            if (oldCancellationData == null || oldServiceApplication == null)
            {
                return new ObjectResult(null);
            }


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    oldCancellationData = _mapper.Map<CancellationDTO, Cancellation>(updatedCancellationData, oldCancellationData);

                    oldCancellationData.UpdatedDateTime = DateTime.Now;

                    oldCancellationData.IsActive = updatedCancellationData.IsActive;
                    oldCancellationData.IsDeleted = updatedCancellationData.IsDeleted;
                    oldCancellationData.CreatedUserId = updatedCancellationData.CreatedUserId;

                    oldServiceApplication.ExchangeActorId = updatedCancellationData.ExchangeActorId;

                    await _context.SaveChangesAsync();
                    transaction.Commit();


                    return new OkObjectResult(_mapper.Map<CancellationDTO>(oldCancellationData));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }

        public async Task<bool> PostFinalCancellation(CancellationDTO finalCancellationDTO)
        {
            ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == finalCancellationDTO.ExchangeActorId)
                .FirstOrDefaultAsync();

            ServiceApplication serviceApplication = await _context.ServiceApplication.Where(sa => sa.ServiceApplicationId == finalCancellationDTO.ServiceApplicationId)
                .FirstOrDefaultAsync();

            // if cancellation process is final get the exchange actor login info from AspNetUsers and change IsEnabled = false

            if (exchangeActor == null || serviceApplication == null)
            {
                return false;
            }

            exchangeActor.Status = (int)ExchangeActorStatus.Cancelled;
            exchangeActor.UpdatedUserId = finalCancellationDTO.UpdatedUserId;

            int serviceApplicationNextStep = (int)CancellationServiceApplicationSteps.Prerequisite;

            serviceApplication.UpdatedDateTime = DateTime.Now;
            serviceApplication.Status = (int)ServiceApplicaitonStatus.Completed;
            serviceApplication.ServiceEndDateTime = DateTime.Now;
            serviceApplication.IsFinished = true;
            serviceApplication.NextStep = serviceApplicationNextStep;
            serviceApplication.CurrentStep = serviceApplicationNextStep;


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }
    }
}
