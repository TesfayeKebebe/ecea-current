﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Recognition
{
    public class InjunctionBody : BaseEntity
    {
        public InjunctionBody()
        {

        }
        [Key]
        public Guid InjunctionBodyId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string Remark { get; set; }
    }
    public partial class InjunctionBodyDTO
    {
        public Guid InjunctionBodyId { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }



    }
}
