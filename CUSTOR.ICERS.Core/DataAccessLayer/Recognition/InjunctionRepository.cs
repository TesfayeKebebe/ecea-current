﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class InjunctionRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public InjunctionRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<int> PostInjunction(InjunctionInfoDTO postedInjunction)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    int injunctionId = 0;
                    // extract injunction iformation
                    PostInjunctionDTO newInjunctionInfo = new PostInjunctionDTO
                    {
                        ExchangeActorId = postedInjunction.ExchangeActorId,
                        InjunctionBodyId = postedInjunction.InjunctionBodyId,
                        InjunctionStatus = 0,
                        Reason = postedInjunction.Reason,
                        InjunctionStartDate = postedInjunction.InjunctionStartDate,
                        InjunctionEndDate = postedInjunction.InjunctionEndDate,
                        InjunctionLetterNo = postedInjunction.InjunctionLetterNo,
                        SpecialReason = postedInjunction.SpecialReason,
                        CreatedUserId = postedInjunction.CreatedUserId,
                        ApprovalDecision = postedInjunction.ApprovalDecision,
                        ApprovalFinished = postedInjunction.ApprovalFinished 
                    };

                    // map to injunction
                    Injunction _InjunctionInfo = _mapper.Map<Injunction>(newInjunctionInfo);


                    _context.Injunction.Add(_InjunctionInfo);
                    await _context.SaveChangesAsync();
                    // update InjunctionId with the newly generated id it is used as reference to workflow
                    injunctionId = _InjunctionInfo.InjunctionId;


                    ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == postedInjunction.ExchangeActorId)
                    .FirstOrDefaultAsync();

                    // workflow Data
                    WorkFlowDTO flowDTO = new WorkFlowDTO
                    {
                        ProcessedByName = postedInjunction.ProcessedByName ?? "Unkown user",
                        InjunctionId = injunctionId,
                        ServiceType = (int)ServiceType.Injunction,
                        Comment = postedInjunction.Comment,
                        ApprovalFinished = postedInjunction.ApprovalFinished,
                        CreatedUserId = (Guid)postedInjunction.CreatedUserId,
                        FinalDec = 3
                    };
                    var workFlowData = _mapper.Map<WorkFlow>(flowDTO);
                    _context.WorkFlow.Add(workFlowData);
                    exchangeActor.Status = (int)ExchangeActorStatus.Active;
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    return injunctionId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> PostInjunctionfordataEntery(InjunctionInfoDTOForDataEntry postedInjunction)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    PostInjunctionDTOForDataEntry newInjunctionInfo = new PostInjunctionDTOForDataEntry();

                    int injunctionId = 0;
                    if (postedInjunction.InjunctionLiftedDate != null)
                    {
                        DateTime newvalue = (DateTime)postedInjunction.InjunctionLiftedDate;

                        newInjunctionInfo.ExchangeActorId = postedInjunction.ExchangeActorId;
                        newInjunctionInfo.InjunctionBodyId = postedInjunction.InjunctionBodyId;
                        newInjunctionInfo.InjunctionStatus = 0;
                        newInjunctionInfo.Reason = postedInjunction.Reason;
                        newInjunctionInfo.InjunctionStartDate = EthiopicDateTime.GetGregorianDate(
                        postedInjunction.InjunctionStartDate.Day,
                        postedInjunction.InjunctionStartDate.Month, postedInjunction.InjunctionStartDate.Year);
                        newInjunctionInfo.InjunctionEndDate = EthiopicDateTime.GetGregorianDate(
                        postedInjunction.InjunctionEndDate.Day,
                        postedInjunction.InjunctionEndDate.Month, postedInjunction.InjunctionEndDate.Year);
                        newInjunctionInfo.InjunctionLetterNo = postedInjunction.InjunctionLetterNo;
                        newInjunctionInfo.InjunctionLiftedLetterNo = postedInjunction.InjunctionLiftedLetterNo;
                        newInjunctionInfo.InjunctionLiftedBy = postedInjunction.InjunctionLiftedBy;
                        newInjunctionInfo.InjunctionLiftReason = postedInjunction.InjunctionLiftReason;

                        newInjunctionInfo.SpecialReason = postedInjunction.SpecialReason;
                        newInjunctionInfo.InjunctionLiftedDate = EthiopicDateTime.GetGregorianDate(
                         newvalue.Day, newvalue.Month, newvalue.Year);

                        newInjunctionInfo.CreatedUserId = postedInjunction.CreatedUserId;
                        newInjunctionInfo.ApprovalDecision = 4;
                        newInjunctionInfo.ApprovalFinished = true;
                         
                        newInjunctionInfo.IsActive = (postedInjunction.DataType == 2) ? true : false;
                        newInjunctionInfo.IsDeleted = false;
                    }
                    else
                    {
                        newInjunctionInfo.ExchangeActorId = postedInjunction.ExchangeActorId;
                        newInjunctionInfo.InjunctionBodyId = postedInjunction.InjunctionBodyId;
                        newInjunctionInfo.InjunctionStatus = 0;
                        newInjunctionInfo.Reason = postedInjunction.Reason;
                        newInjunctionInfo.InjunctionStartDate = EthiopicDateTime.GetGregorianDate(
                        postedInjunction.InjunctionStartDate.Day,
                        postedInjunction.InjunctionStartDate.Month, postedInjunction.InjunctionStartDate.Year);
                        newInjunctionInfo.InjunctionEndDate = EthiopicDateTime.GetGregorianDate(
                            postedInjunction.InjunctionEndDate.Day,
                            postedInjunction.InjunctionEndDate.Month, postedInjunction.InjunctionEndDate.Year);
                        newInjunctionInfo.InjunctionLetterNo = postedInjunction.InjunctionLetterNo;
                        newInjunctionInfo.InjunctionLiftedLetterNo = null;
                        newInjunctionInfo.InjunctionLiftedBy = null;
                        newInjunctionInfo.InjunctionLiftReason = null;

                        newInjunctionInfo.SpecialReason = postedInjunction.SpecialReason;


                        newInjunctionInfo.CreatedUserId = postedInjunction.CreatedUserId;
                        newInjunctionInfo.ApprovalDecision = 4;
                        newInjunctionInfo.ApprovalFinished = true;
                        newInjunctionInfo.IsActive = (postedInjunction.DataType == 2) ? true : false;
                        newInjunctionInfo.IsDeleted = false;
                    }

                    // extract injunction iformation

                    // map to injunction
                    Injunction _InjunctionInfo = _mapper.Map<Injunction>(newInjunctionInfo);
                    _context.Injunction.Add(_InjunctionInfo);
                    await _context.SaveChangesAsync();
                    // update InjunctionId with the newly generated id it is used as reference to workflow
                    injunctionId = _InjunctionInfo.InjunctionId;


                    ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == postedInjunction.ExchangeActorId)
                    .FirstOrDefaultAsync();

                    // workflow Data
                    WorkFlowDTO flowDTO = new WorkFlowDTO
                    {
                        ProcessedByName = postedInjunction.ProcessedByName ?? "Unkown user",
                        InjunctionId = injunctionId,
                        ServiceType = (int)ServiceType.Injunction,
                        Comment = postedInjunction.Comment,
                        ApprovalFinished = true,
                        CreatedUserId = postedInjunction.CreatedUserId,
                        FinalDec = 3
                    };
                    var workFlowData = _mapper.Map<WorkFlow>(flowDTO);
                    _context.WorkFlow.Add(workFlowData);
                    if (postedInjunction.DataType == 2)
                    {
                        exchangeActor.Status = (int)ExchangeActorStatus.Under_Injunction;
                    }

                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    return injunctionId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }


        public async Task<List<ExchangeActor>> GetExpiredRenewals(string lang)
        {
            List<ExchangeActor> expiredRenewals = await _context.ExchangeActor
                                        .Where(ex => ex.ExpireDateTime.GetValueOrDefault().Date < DateTime.UtcNow.AddMonths((int)GracePeriod.RenewalGracePeriod)).ToListAsync();
            return expiredRenewals;
        }

        public async Task<bool> UpdateInjunctionInfo(InjunctionInfoDTO updatedInjunction)
        {
            try
            {
                var oldInjunction = await _context.Injunction.FirstOrDefaultAsync(Inj =>
                    Inj.InjunctionId == updatedInjunction.InjunctionId);

                if (oldInjunction != null)
                {
                    oldInjunction = _mapper.Map(updatedInjunction, oldInjunction);
                    await _context.SaveChangesAsync();

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> LiftInjunction(InjunctionLiftDTO injunctionData)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var oldInjunction = await _context.Injunction.FirstOrDefaultAsync(Inj =>
                        Inj.InjunctionId == injunctionData.InjunctionId);
                    ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == injunctionData.ExchangeActorId)
                   .FirstOrDefaultAsync();
                    if (oldInjunction != null)
                    {
                        oldInjunction.ApprovalDecision = 1;
                        oldInjunction.InjunctionStatus = 0;
                        oldInjunction.IsDeleted = false;
                        oldInjunction.IsActive = false;
                        oldInjunction.InjunctionLiftedLetterNo = injunctionData.InjunctionLiftedLetterNo;
                        oldInjunction.InjunctionLiftReason = injunctionData.InjunctionLiftReason;
                        oldInjunction.InjunctionLiftedBy = injunctionData.InjunctionLiftedBy;
                        oldInjunction.InjunctionLiftedDate = injunctionData.InjunctionLiftedDate;
                        _context.Injunction.Update(oldInjunction);
                        exchangeActor.Status = (int)ExchangeActorStatus.Active;
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<bool> DeleteInjunction(int injunctionId)
        {
            try
            {
                var oldInjunction = await _context.Injunction.FirstOrDefaultAsync(Inj =>
                    Inj.InjunctionId == injunctionId);
                if (oldInjunction != null)
                {
                    oldInjunction.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<InjunctionViewDTO>> GetInjunctions(InjunctionQueryParameters parameters)
        {
            try
            {

                int totalSearchResult = 0;
                List<InjunctionViewDTO> InjunctionList = null;
                // status 1= injunction means active
                InjunctionList = await (from ij in _context.Injunction
                                        join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                        join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                        join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                        join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                        where ij.IsDeleted == false && ij.IsActive == true
                                        && ij.InjunctionStatus == 1 && ij.InjunctionStatus != 0
                                        && ij.ApprovalFinished == true
                                        && (ij.ApprovalDecision == 1 || ij.ApprovalDecision == 2 || ij.ApprovalDecision == 3)
                                        orderby ij.InjunctionId descending
                                        select new InjunctionViewDTO
                                        {
                                            InjunctionId = ij.InjunctionId,
                                            InjunctionBodyId = (parameters.Lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                            InjunctionLetterNo = ij.InjunctionLetterNo,
                                            ExchangeActorId = ij.ExchangeActorId,
                                            InjunctionStatus = ij.InjunctionStatus,
                                            Reason = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                            ReasonId = ij.Reason,
                                            ExchangeActor = ij.ExchangeActor,
                                            InjunctionEndDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                            InjunctionStartDate = (parameters.Lang == "et") ? EthiopicDateTime
                                            .TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                        })
                                 .ToListAsync();

                totalSearchResult = InjunctionList.Count();
                var pagedResult = InjunctionList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);

                return new PagedResult<InjunctionViewDTO>()
                {
                    Items = _mapper.Map<List<InjunctionViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<CancellationViewDTO>> GetCancellations(InjunctionQueryParameters parameters)
        {
            try
            {

                int totalSearchResult = 0;
                List<CancellationViewDTO> InjunctionList = null;
                // status 1= injunction means active
                InjunctionList = await (from cancl in _context.Cancellation
                                        join lo in _context.Lookup on cancl.CancellationType equals lo.LookupId
                                        join ex in _context.ExchangeActor on cancl.ExchangeActorId equals ex.ExchangeActorId
                                        where cancl.IsDeleted == false && cancl.IsActive == true
                                        orderby cancl.CancellationId descending
                                        select new CancellationViewDTO
                                        {
                                            CancellationId = cancl.CancellationId,
                                            CancellationTypeName = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                            ExchangeActorId = cancl.ExchangeActorId,
                                            Description = cancl.Description,
                                            ExchangeActor = cancl.ExchangeActor,
                                            CancellationDate = (parameters.Lang == "et") ? EthiopicDateTime
                                            .TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(cancl.CreatedDateTime).ToString()) : cancl.CreatedDateTime.ToString(),
                                        })
                                 .ToListAsync();

                totalSearchResult = InjunctionList.Count();
                var pagedResult = InjunctionList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);

                return new PagedResult<CancellationViewDTO>()
                {
                    Items = _mapper.Map<List<CancellationViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<PendingInjunctionViewDTO>> GetPendingInjunctions(InjunctionQueryParameters parameters)
        {
            try
            {
                int totalSearchResult = 0;
                List<PendingInjunctionViewDTO> InjunctionList = null;
                InjunctionList = await (from ij in _context.Injunction
                                        join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                        join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                        join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                        join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                        where ij.IsDeleted == false && ij.InjunctionStatus == 0 &&
                                        (ij.ApprovalDecision == 0
                                        || ij.ApprovalDecision == 4
                                        || ij.ApprovalDecision == 5
                                        || ij.ApprovalDecision == 6)
                                        orderby ij.InjunctionId descending
                                        select new PendingInjunctionViewDTO
                                        {
                                            InjunctionId = ij.InjunctionId,
                                            InjunctionBodyId = (parameters.Lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                            InjunctionLetterNo = ij.InjunctionLetterNo,
                                            ExchangeActorId = ij.ExchangeActorId,
                                            InjunctionStatus = ij.InjunctionStatus,
                                            CurrentStep = ij.ApprovalDecision,
                                            Reason = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                            SpecialReason = ij.SpecialReason,
                                            ExchangeActor = ij.ExchangeActor,
                                            InjunctionEndDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                            InjunctionStartDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                        })
                                 .ToListAsync();

                totalSearchResult = InjunctionList.Count();
                var pagedResult = InjunctionList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);
                return new PagedResult<PendingInjunctionViewDTO>()
                {
                    Items = _mapper.Map<List<PendingInjunctionViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<InjunctionViewDTO>> GetLiftedInjunctions(InjunctionQueryParameters parameters)
        {
            try
            {
                int totalSearchResult = 0;
                List<InjunctionViewDTO> InjunctionList = null;
                //status 1= injunction is active
                InjunctionList = await (from ij in _context.Injunction
                                        join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                        join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                        join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                        join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                        where ij.IsDeleted == false && ij.InjunctionStatus == 0 && ij.ApprovalDecision == 1

                                        orderby ij.InjunctionId descending
                                        select new InjunctionViewDTO
                                        {
                                            InjunctionId = ij.InjunctionId,
                                            InjunctionBodyId = (parameters.Lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                            InjunctionLetterNo = ij.InjunctionLetterNo,
                                            ExchangeActorId = ij.ExchangeActorId,
                                            InjunctionStatus = ij.InjunctionStatus,
                                            Reason = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                            ReasonId = ij.Reason,
                                            ExchangeActor = ij.ExchangeActor,
                                            InjunctionEndDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                            InjunctionStartDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                            InjunctionLiftedDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionLiftedDate).ToString()) : ij.InjunctionLiftedDate.ToString(),
                                        })
                                 .ToListAsync();
                totalSearchResult = InjunctionList.Count();
                var pagedResult = InjunctionList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);

                return new PagedResult<InjunctionViewDTO>()
                {
                    Items = _mapper.Map<List<InjunctionViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<InjunctionViewDTO> GetInjunctionById(string lang, int id)
        {
            try
            {
                Guid exchangeActorId = _context.Injunction.Where(d => d.InjunctionId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();



                var data = await (from ij in _context.Injunction
                                  join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                  join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                  join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                  join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                  join lukUP in _context.Lookup on ij.Reason equals lukUP.LookupId
                                  where ij.InjunctionId == id
                                  select new InjunctionViewDTO
                                  {
                                      InjunctionId = ij.InjunctionId,
                                      InjunctionBodyId = (lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                      InjunctionLetterNo = ij.InjunctionLetterNo,
                                      ExchangeActorId = ij.ExchangeActorId,
                                      OwnerManager = customerLegalBody,


                                      InjunctionStatus = ij.InjunctionStatus,
                                      Reason = (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                      ExchangeActor = ij.ExchangeActor,
                                      InjunctionLiftedBy = (ij.InjunctionStatus == 0) ? ((lang == "et") ? lukUP.DescriptionAmh :
                                      lukUP.DescriptionEng) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                      InjunctionLiftedLetterNo = ij.InjunctionLiftedLetterNo,
                                      InjunctionLiftReason = ij.InjunctionLiftReason,
                                      ApprovalDecision = ij.ApprovalDecision,
                                      ReasonId = ij.Reason,// usefull to decide if injunction can be lifted
                                      InjunctionLiftedDate = (ij.InjunctionStatus == 0) ? (
                                      (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate
                                      (ij.InjunctionLiftedDate).ToString()) : ij.InjunctionLiftedDate.ToString()) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                      InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                      InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                  })
                             .FirstOrDefaultAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<LetterContent> GetInjunctionPrintById(string lang, int id)
        {
            try
            {
                var EtCurrentDate = "";
                Guid exchangeActorId = _context.Injunction.Where(d => d.InjunctionId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();

                var injunctionData = await (from ij in _context.Injunction
                                            join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                            join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                            join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                            join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                            join lukUP in _context.Lookup on ij.Reason equals lukUP.LookupId
                                            where ij.InjunctionId == id
                                            select new InjunctionViewDTO
                                            {
                                                InjunctionId = ij.InjunctionId,
                                                InjunctionBodyId = (lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                                InjunctionLetterNo = ij.InjunctionLetterNo,
                                                ExchangeActorId = ij.ExchangeActorId,
                                                OwnerManager = customerLegalBody,


                                                InjunctionStatus = ij.InjunctionStatus,
                                                Reason = (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                                ExchangeActor = ij.ExchangeActor,
                                                InjunctionLiftedBy = (ij.InjunctionStatus == 0) ? ((lang == "et") ? lukUP.DescriptionAmh :
                                                lukUP.DescriptionEng) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                                InjunctionLiftedLetterNo = ij.InjunctionLiftedLetterNo,
                                                InjunctionLiftReason = ij.InjunctionLiftReason,
                                                ApprovalDecision = ij.ApprovalDecision,
                                                ReasonId = ij.Reason,// usefull to decide if injunction can be lifted
                                                InjunctionLiftedDate = (ij.InjunctionStatus == 0) ? (
                                                (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate
                                                (ij.InjunctionLiftedDate).ToString()) : ij.InjunctionLiftedDate.ToString()) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                                InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                                InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                            })
                             .FirstOrDefaultAsync();
                var injunctions = _context.Injunction.FirstOrDefault(x => x.InjunctionId == injunctionData.InjunctionId);
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "InjunctionLetter").FirstOrDefaultAsync();
                var LetterExchangeActorTypeRep = letterContent.BodyHeader.Replace(@"አባል1___", "አባል " + "<strong>" + injunctionData.ExchangeActor?.CustomerType?.DescriptionAmh + "</strong>");
                var injuctionDate = "";
                //var caseType = "";

                if (injunctions != null)
                {
                    injuctionDate = EthiopicDateTime.GetEthiopicDate(injunctions.InjunctionStartDate.Day,
                                     injunctions.InjunctionStartDate.Month, injunctions.InjunctionStartDate.Year);
                }

                var exchangeActorNameRep = LetterExchangeActorTypeRep.Replace(@"አግሮጲያ  ትሬዲንግ ኃ/የተ/የግ/ማ1___", "" + "<strong>" + injunctionData.ExchangeActor?.OrganizationNameAmh + "</strong>");
                var reasonRep = exchangeActorNameRep.Replace(@"የተጣራ ሀብት መጉደሉን በ2011 በጀት ዓመት ዓመታዊ የኦዲት ሪፖርቱ የገለጸ በመሆኑ1__",
                " " + "<strong>" + injunctionData.Reason + " </strong> ");
                var injuctionStartDateRep = reasonRep.Replace(@"ህዳር 10 ቀን 2012___", "" + injunctionData.InjunctionStartDate);
                var ccNameRep = injuctionStartDateRep.Replace(@"ሴቲት ትሬዲንግ ኃ/የተ/የግ/ማ2___", " " + injunctionData.ExchangeActor?.OrganizationNameAmh);

                LetterContent letter = new LetterContent();
                letter.BodyHeader = ccNameRep;
                return letter;
                //end of letter contente
                //return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<LetterContent> GetLiftingInjunctionPrintById(string lang, int id)
        {
            try
            {
                var EtCurrentDate = "";
                Guid exchangeActorId = _context.Injunction.Where(d => d.InjunctionId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();

                var injunctionData = await (from ij in _context.Injunction
                                            join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                            join _look in _context.Lookup on ij.InjunctionBodyId equals _look.LookupId
                                            join ex in _context.ExchangeActor on ij.ExchangeActorId equals ex.ExchangeActorId
                                            join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                            join lukUP in _context.Lookup on ij.Reason equals lukUP.LookupId
                                            where ij.InjunctionId == id
                                            select new InjunctionViewDTO
                                            {
                                                InjunctionId = ij.InjunctionId,
                                                InjunctionBodyId = (lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                                InjunctionLetterNo = ij.InjunctionLetterNo,
                                                ExchangeActorId = ij.ExchangeActorId,
                                                OwnerManager = customerLegalBody,


                                                InjunctionStatus = ij.InjunctionStatus,
                                                Reason = (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                                ExchangeActor = ij.ExchangeActor,
                                                InjunctionLiftedBy = (ij.InjunctionStatus == 0) ? ((lang == "et") ? lukUP.DescriptionAmh :
                                                lukUP.DescriptionEng) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                                InjunctionLiftedLetterNo = ij.InjunctionLiftedLetterNo,
                                                InjunctionLiftReason = ij.InjunctionLiftReason,
                                                ApprovalDecision = ij.ApprovalDecision,
                                                ReasonId = ij.Reason,// usefull to decide if injunction can be lifted
                                                InjunctionLiftedDate = (ij.InjunctionStatus == 0) ? (
                                                (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate
                                                (ij.InjunctionLiftedDate).ToString()) : ij.InjunctionLiftedDate.ToString()) : ((lang == "et") ? "አልተነሳም" : "Not lifted yet"),
                                                InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                                InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                            })
                             .FirstOrDefaultAsync();
                var injunctions = _context.Injunction.FirstOrDefault(x => x.InjunctionId == injunctionData.InjunctionId);
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "LiftInjunctionLetter").FirstOrDefaultAsync();
                var LetterExchangeActorTypeRep = letterContent.BodyHeader.Replace(@"አባል1___", "አባል " + "<strong>" + injunctionData.ExchangeActor?.CustomerType?.DescriptionAmh + "</strong>");
                var injuctionDate = "";
                //var caseType = "";

                if (injunctions != null)
                {
                    injuctionDate = EthiopicDateTime.GetEthiopicDate(injunctions.InjunctionStartDate.Day,
                                     injunctions.InjunctionStartDate.Month, injunctions.InjunctionStartDate.Year);
                }

                var exchangeActorNameRep = LetterExchangeActorTypeRep.Replace(@"ሞጣ የገበሬዎች ህብረት ስራ ማህበራት ዩኒየን1___", "" + "<strong>" + injunctionData.ExchangeActor?.OrganizationNameAmh + "</strong>");
                var reasonRep = exchangeActorNameRep.Replace(@"የተጣራሃብትመጠን መውረድ1_",
                " " + "<strong>" + injunctionData.Reason + " </strong> ");
                var injuctionStartDateRep = reasonRep.Replace(@"ከጥቅምት 17 , 20131_", "" + injunctionData.InjunctionStartDate);
                var ccNameRep = injuctionStartDateRep.Replace(@"ሞጣ የገበሬዎች ህብረት ስራ ማህበራት ዩኒየን1__", " " + injunctionData.ExchangeActor?.OrganizationNameAmh);

                LetterContent letter = new LetterContent();
                letter.BodyHeader = ccNameRep;
                return letter;
                //end of letter contente
                //return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<LetterContent> GetCancelledPrintById(string lang, int id)
        {
            try
            {
                var EtCurrentDate = "";
                Guid exchangeActorId = _context.Cancellation.Where(d => d.CancellationId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();

                var injunctionData = await (from cancl in _context.Cancellation
                                            join lo in _context.Lookup on cancl.CancellationType equals lo.LookupId
                                            join ex in _context.ExchangeActor on cancl.ExchangeActorId equals ex.ExchangeActorId
                                            where cancl.IsDeleted == false && cancl.IsActive == true
                                            orderby cancl.CancellationId descending
                                            where cancl.CancellationId == id
                                            select new CancellationViewDTO
                                            {
                                                CancellationId = cancl.CancellationId,
                                                CancellationTypeName = (lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                                ExchangeActorId = cancl.ExchangeActorId,
                                                Description = cancl.Description,
                                                ExchangeActor = cancl.ExchangeActor,
                                                CancellationDate = (lang == "et") ? EthiopicDateTime
                                                .TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(cancl.CreatedDateTime).ToString()) : cancl.CreatedDateTime.ToString(),
                                            })
                             .FirstOrDefaultAsync();
                var injunctions = _context.Cancellation.FirstOrDefault(x => x.CancellationId == injunctionData.CancellationId);
                //return data;
                EtCurrentDate = EthiopicDateTime.GetEthiopicDate(DateTime.Now).ToString();
                var letterContent = await _context.LetterContent.FromSqlRaw(@"select BodyHeader, Category from LetterContent where Category={0} ", "LiftInjunctionLetter").FirstOrDefaultAsync();
                var LetterExchangeActorTypeRep = letterContent.BodyHeader.Replace(@"አባል1___", "አባል " + "<strong>" + injunctionData.ExchangeActor?.CustomerType?.DescriptionAmh + "</strong>");
                var injuctionDate = "";
                //var caseType = "";

                if (injunctions != null)
                {
                    injuctionDate = EthiopicDateTime.GetEthiopicDate(injunctions.CreatedDateTime.Day,
                                     injunctions.CreatedDateTime.Month, injunctions.CreatedDateTime.Year);
                }

                var exchangeActorNameRep = LetterExchangeActorTypeRep.Replace(@"ሞጣ የገበሬዎች ህብረት ስራ ማህበራት ዩኒየን1___", "" + "<strong>" + injunctionData.ExchangeActor?.OrganizationNameAmh + "</strong>");
                var reasonRep = exchangeActorNameRep.Replace(@"የተጣራሃብትመጠን መውረድ1_",
                " " + "<strong>" + injunctionData.Description + " </strong> ");
                var injuctionStartDateRep = reasonRep.Replace(@"ከጥቅምት 17 , 20131_", "" + injunctionData.CancellationDate);
                var ccNameRep = injuctionStartDateRep.Replace(@"ሞጣ የገበሬዎች ህብረት ስራ ማህበራት ዩኒየን1__", " " + injunctionData.ExchangeActor?.OrganizationNameAmh);

                LetterContent letter = new LetterContent();
                letter.BodyHeader = ccNameRep;
                return letter;
                //end of letter contente
                //return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<List<InjunctionViewDTO>> GetInjunctionHistory(Guid selected_customer_id, string lang)
        {
            try
            {

                var data = await (from ij in _context.Injunction
                                  .Include(c => c.ExchangeActor)
                                  where ij.ExchangeActorId == selected_customer_id
                                  join luk in _context.Lookup on ij.Reason equals luk.LookupId
                                  join injbody in _context.Lookup on ij.InjunctionBodyId equals injbody.LookupId


                                  select new InjunctionViewDTO
                                  {
                                      InjunctionId = ij.InjunctionId,
                                      InjunctionBodyId = (lang == "et") ? injbody.DescriptionAmh : injbody.DescriptionEng,
                                      InjunctionLetterNo = ij.InjunctionLetterNo,
                                      ExchangeActorId = ij.ExchangeActorId,
                                      InjunctionStatus = ij.InjunctionStatus,
                                      Reason = (lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                      ExchangeActor = ij.ExchangeActor,
                                      InjunctionLiftedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionLiftedDate).ToString()) : ij.InjunctionLiftedDate.ToString(),
                                      InjunctionEndDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                      InjunctionStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                  })
                                  .ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> GetCurrentStatus(Guid customer_id)
        {
            try
            {
                ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == customer_id)
                      .FirstOrDefaultAsync();
                return exchangeActor.Status;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<InjunctionViewDTO>> GetExpiringInjunctions(InjunctionQueryParameters parameters)
        {
            try
            {
                List<InjunctionViewDTO> expiringInjunctions = null;
                int totalSearchResult = 0;
                DateTime currentDate = DateTime.UtcNow;
                expiringInjunctions = await (from ij in _context.Injunction
                                 .Include(c => c.ExchangeActor)
                                 .Where(inj => (inj.InjunctionEndDate - currentDate).Days <= 15 && (inj.ApprovalFinished == true) && (inj.InjunctionStatus == 1)) //todo change static number to const
                                             join luk in _context.Lookup on ij.InjunctionBodyId equals luk.LookupId
                                             join lo in _context.Lookup on ij.Reason equals lo.LookupId
                                             select new InjunctionViewDTO
                                             {
                                                 InjunctionId = ij.InjunctionId,
                                                 InjunctionBodyId = (parameters.Lang == "et") ? luk.DescriptionAmh : luk.DescriptionEng,
                                                 InjunctionLetterNo = ij.InjunctionLetterNo,
                                                 ExchangeActorId = ij.ExchangeActorId,
                                                 InjunctionStatus = ij.InjunctionStatus,
                                                 Reason = (parameters.Lang == "et") ? lo.DescriptionAmh : lo.DescriptionEng,
                                                 ReasonId = ij.Reason,
                                                 ExchangeActor = ij.ExchangeActor,
                                                 InjunctionEndDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionEndDate).ToString()) : ij.InjunctionEndDate.ToString(),
                                                 InjunctionStartDate = (parameters.Lang == "et") ? EthiopicDateTime
                                                 .TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(ij.InjunctionStartDate).ToString()) : ij.InjunctionStartDate.ToString(),
                                             })
                                 .ToListAsync();
                totalSearchResult = expiringInjunctions.Count();
                var pagedResult = expiringInjunctions.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);
                return new PagedResult<InjunctionViewDTO>()
                {
                    Items = _mapper.Map<List<InjunctionViewDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> ApproveOrDeclineInjunction(InjunctionApprovalDTO approvalData)
        {


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    var oldInjunction = await _context.Injunction.FirstOrDefaultAsync(Inj =>
                    Inj.InjunctionId == approvalData.InjunctionId);
                    var status = approvalData.ApprovalDecision;
                    if (oldInjunction != null)
                    {
                        oldInjunction.ApprovalDecision = approvalData.ApprovalDecision;
                        oldInjunction.DecisionMadeBy = approvalData.DecisionMadeBy;
                        oldInjunction.DecisionSummary = approvalData.DecisionSummary;
                        oldInjunction.DecissionDate = approvalData.DecissionDate;
                        oldInjunction.ApprovalFinished = approvalData.ApprovalFinished;
                        // oldInjunction.InjunctionStatus = Status;
                    }

                    ExchangeActor exchangeActor = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == approvalData.ExchangeActorId)
                    .FirstOrDefaultAsync();
                    //_context.Injunction.Add(oldInjunction);
                    if (exchangeActor != null)
                    {
                        exchangeActor.Status = approvalData.customerStatus;
                    }
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

    }
}
