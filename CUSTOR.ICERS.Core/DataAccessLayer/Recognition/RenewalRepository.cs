﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Recognition
{
    public class RenewalRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private ExchangeActorRepository _exchangeActorRepository;


        public RenewalRepository(ECEADbContext context, IMapper mapper, ExchangeActorRepository exchangeActorRepository)
        {
            _context = context;
            _mapper = mapper;
            _exchangeActorRepository = exchangeActorRepository;
        }


        public async Task<IActionResult> PostRenewalGeneralInfo(RenewalDTO postedRenewal)
        {

            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(postedRenewal.ExchangeActorId);

            if (customerStatusResponse.IsCancelled || customerStatusResponse.IsUnderInjunction)
            {
                return new ObjectResult(customerStatusResponse);

            }

            // if the renewal period is not reached customre can not able to renew the recognition
            customerStatusResponse = await _exchangeActorRepository.GetCustomerRecognitionExpirStatus(postedRenewal.ExchangeActorId);

            if (customerStatusResponse.IsLicenceNotExpired || customerStatusResponse.IsDelgatorNotRenewed)
            {
                return new ObjectResult(customerStatusResponse);
            }

            Renewal renewal = null;

            renewal = _mapper.Map<Renewal>(postedRenewal);

            int generalInformation = (int)RenewalServiceApplicaionSteps.General_Information;
            int prerequisite = (int)RenewalServiceApplicaionSteps.Prerequisite;

            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                ExchangeActorId = postedRenewal.ExchangeActorId,
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = generalInformation,
                NextStep = prerequisite,
                ServiceId = postedRenewal.RenewalServieType,
                IsFinished = false,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                CustomerTypeId = postedRenewal.CustomerTypeId,
                CreatedUserId = postedRenewal.CreatedUserId,
                UpdatedUserId = postedRenewal.CreatedUserId
            };

            renewal.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            renewal.CreatedUserId = postedRenewal.CreatedUserId;
            renewal.UpdatedUserId = postedRenewal.CreatedUserId;


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.Renewal.Add(renewal);
                    await _context.SaveChangesAsync();
                    transaction.Commit();

                    return new OkObjectResult(_mapper.Map<ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        // TODO update Renewal Information
        public async Task<IActionResult> UpdateRenewal(RenewalDTO updatedRenewalData)
        {
            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(updatedRenewalData.ExchangeActorId);

            if (customerStatusResponse.IsCancelled || customerStatusResponse.IsUnderInjunction)
            {
                return new ObjectResult(customerStatusResponse);

            }

            customerStatusResponse = await _exchangeActorRepository.GetCustomerRecognitionExpirStatus(updatedRenewalData.ExchangeActorId);

            if (customerStatusResponse.IsLicenceNotExpired || customerStatusResponse.IsDelgatorNotRenewed)
            {
                return new ObjectResult(customerStatusResponse);
            }


            Renewal oldRenewalData = await _context.Renewal.FirstOrDefaultAsync(re => re.ServiceApplicationId == updatedRenewalData.ServiceApplicationId);

            oldRenewalData.CreatedUserId = updatedRenewalData.CreatedUserId;
            oldRenewalData.UpdatedUserId = updatedRenewalData.UpdatedUserId;

            ServiceApplication oldServiceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == updatedRenewalData.ServiceApplicationId);

            if (oldRenewalData == null || oldServiceApplication == null)
            {
                return new ObjectResult(null);
            }


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    oldRenewalData = _mapper.Map<RenewalDTO, Renewal>(updatedRenewalData, oldRenewalData);
                    oldRenewalData.UpdatedDateTime = DateTime.Now;


                    // update the new exchange actor

                    oldServiceApplication.ExchangeActorId = updatedRenewalData.ExchangeActorId;

                    await _context.SaveChangesAsync();
                    transaction.Commit();


                    return new OkObjectResult(_mapper.Map<RenewalDTO>(oldRenewalData));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }

        public async Task<RenewalDTO> GetRenewalDataByServiceAppId(string lang, Guid serviceApplicationId)
        {
            try
            {

                RenewalDTO exchangeActorRenewalData = await (from renewal in _context.Renewal
                                                             join ea in _context.ExchangeActor on renewal.ExchangeActorId equals ea.ExchangeActorId
                                                             join om in _context.OwnerManager on renewal.ExchangeActorId equals om.ExchangeActorId
                                                             where renewal.IsActive == true && renewal.IsDeleted == false && renewal.ServiceApplicationId == serviceApplicationId && om.IsManager == true
                                                             select new RenewalDTO
                                                             {
                                                                 ExchangeActorName = (ea.CustomerTypeId == (int)CustomerType.Representative || ea.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative) 
                                                                 ? lang == "et" ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh) : (om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng)
                                                                 : lang == "et" ? ea.OrganizationNameAmh : ea.OrganizationNameEng,
                                                                 ExchangeActorType = lang == "et" ? renewal.ExchangeActor.CustomerType.DescriptionAmh : renewal.ExchangeActor.CustomerType.DescriptionEng,
                                                                 ServiceApplicationId = renewal.ServiceApplicationId,
                                                                 ExchangeActorId = renewal.ExchangeActorId

                                                             }).AsNoTracking()
                                                               .FirstOrDefaultAsync();

                return exchangeActorRenewalData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

    }
}
