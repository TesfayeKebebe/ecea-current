﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Recognition
{
    public class ReplacementRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private ExchangeActorRepository _exchangeActorRepository;

        public ReplacementRepository(ECEADbContext context, IMapper mapper, ExchangeActorRepository exchangeActorRepository)
        {
            _context = context;
            _mapper = mapper;
            _exchangeActorRepository = exchangeActorRepository;

        }

        public async Task<IActionResult> PostReplacementGeneralInfo(ReplacementDTO postedReplacement)
        {

            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(postedReplacement.ExchangeActorId);

            if (customerStatusResponse.IsCancelled || customerStatusResponse.IsUnderInjunction)
            {
                return new ObjectResult(customerStatusResponse);
            }


            Replacement replacement = null;

            replacement = _mapper.Map<Replacement>(postedReplacement);

            int generalInformation = (int)RenewalServiceApplicaionSteps.General_Information;
            int prerequisite = (int)RenewalServiceApplicaionSteps.Prerequisite;

            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                ExchangeActorId = postedReplacement.ExchangeActorId,
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = generalInformation,
                NextStep = prerequisite,
                ServiceId = (int)ServiceType.Replacement,
                IsFinished = false,
                CustomerTypeId = postedReplacement.CustomerTypeId,
                CreatedUserId = postedReplacement.CreatedUserId,
                UpdatedUserId = postedReplacement.CreatedUserId
            };

            replacement.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            replacement.CreatedUserId = postedReplacement.CreatedUserId;
            replacement.UpdatedUserId = postedReplacement.CreatedUserId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.Replacement.Add(replacement);
                    _context.SaveChanges();
                    transaction.Commit();


                    return new ObjectResult(_mapper.Map<ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        public async Task<IActionResult> UpdateReplacment(ReplacementDTO updatedReplacmentData)
        {

            ResultResponse customerStatusResponse = await _exchangeActorRepository.GetCustomerSatus(updatedReplacmentData.ExchangeActorId);

            if (customerStatusResponse.IsCancelled)
            {
                return new ObjectResult(customerStatusResponse);
            }

            Replacement oldReplacmentData = await _context.Replacement.FirstOrDefaultAsync(re => re.ServiceApplicationId == updatedReplacmentData.ServiceApplicationId);
            
            oldReplacmentData.UpdatedDateTime = DateTime.Now;
            oldReplacmentData.UpdatedUserId = updatedReplacmentData.UpdatedUserId;
            oldReplacmentData.CreatedUserId = oldReplacmentData.CreatedUserId;
            oldReplacmentData.IsActive = oldReplacmentData.IsActive;
            oldReplacmentData.IsDeleted = oldReplacmentData.IsDeleted;

            ServiceApplication oldServiceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == updatedReplacmentData.ServiceApplicationId);

            if (oldReplacmentData == null || oldServiceApplication == null)
            {
                return new ObjectResult(null);
            }


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    oldReplacmentData = _mapper.Map<ReplacementDTO, Replacement>(updatedReplacmentData, oldReplacmentData);
                    
                    oldServiceApplication.ExchangeActorId = updatedReplacmentData.ExchangeActorId;

                    await _context.SaveChangesAsync();
                    transaction.Commit();


                    return new OkObjectResult(_mapper.Map<ReplacementDTO>(oldReplacmentData));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }

        public async Task<ReplacementDTO> GetReplacementDataByServiceAppId(string lang, Guid serviceApplicationId)
        {
            try
            {

                ReplacementDTO exchangeActorRenewalData = await (from replacement in _context.Replacement
                        join om in _context.OwnerManager on replacement.ExchangeActorId equals om.ExchangeActorId
                        where replacement.IsActive == true && replacement.IsDeleted == false && replacement.ServiceApplicationId == serviceApplicationId
                        select new ReplacementDTO
                        {
                            ExchangeActorName = (replacement.ExchangeActor.CustomerTypeId == (int)CustomerType.Representative || replacement.ExchangeActor.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative)? 
                            lang == "et" ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameEng) : (om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng)
                            :lang == "et" ? replacement.ExchangeActor.OrganizationNameAmh : replacement.ExchangeActor.OrganizationNameEng,
                            ExchangeActorType = lang == "et" ? replacement.ExchangeActor.CustomerType.DescriptionAmh : replacement.ExchangeActor.CustomerType.DescriptionEng,
                            ServiceApplicationId = replacement.ServiceApplicationId,
                            ExchangeActorId = replacement.ExchangeActorId

                        }).AsNoTracking()
                    .FirstOrDefaultAsync();

                return exchangeActorRenewalData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
    }
}
