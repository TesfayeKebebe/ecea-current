using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerType = CUSTOR.ICERS.Core.Enum.CustomerType;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class ExchangeActorRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ExchangeActorRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IActionResult> PostExchnageActor(ExchangeActorGeneralInfoDTO postedCustomer)
        {

            if (postedCustomer.CustomerTypeId == (int)Enum.CustomerType.Representative || postedCustomer.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
            {
                return await PostReprsentative(postedCustomer);
            }


            //Is Customer Existed
            ResultResponse statusResponse = await IsCustomerAlreadySaved(postedCustomer.Ecxcode);

            if (statusResponse.IsSaved)
            {
                return new ObjectResult(statusResponse);
            }

            //check the capital type
            ResultResponse netWorthCapitalResponse =  new ResultResponse();

            if (postedCustomer.NetWorthCapital != 0)
            {
                netWorthCapitalResponse = await CustomerHasVaidCapital(postedCustomer.CustomerTypeId, postedCustomer.MemberCategoryId.GetValueOrDefault(), postedCustomer.NetWorthCapital);
            }

            if(netWorthCapitalResponse.IsMemberIntermidaryCapitalBelow == true || netWorthCapitalResponse.IsMemberTraderCapitalBelow == true
                || netWorthCapitalResponse.IsNonMemberDirectTraderCapitalBelow == true)
            {
                return new ObjectResult(netWorthCapitalResponse);
            }

            CVGeez objGeez = new CVGeez();

            ExchangeActor exchangeActor = null;

            exchangeActor = _mapper.Map<ExchangeActor>(postedCustomer);

            int generalInformation = (int)RecognitionServiceApplicaitonSteps.General_Information;

            int nextStep = 0;

            if (postedCustomer.CustomerTypeId == (int)Enum.CustomerType.SeetlementBank ||
                postedCustomer.CustomerTypeId == (int)Enum.CustomerType.Auditor || postedCustomer.CustomerTypeId == (int)Enum.CustomerType.NationalTradeAssociation)
            {
                nextStep = (int)RecognitionServiceApplicaitonSteps.Owner;
            }
            else
            {
                nextStep = (int)RecognitionServiceApplicaitonSteps.SisterCompany;
            }


            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = generalInformation,
                NextStep = nextStep,
                ServiceId = (int)ServiceType.Recognition,
                IsFinished = false,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                OrganizationTypeId = postedCustomer.OrganizationTypeId.GetValueOrDefault(),
                CustomerTypeId = postedCustomer.CustomerTypeId,
                Ecxcode = postedCustomer.Ecxcode,
                CreatedUserId = postedCustomer.CreatedUserId,
                UpdatedUserId = postedCustomer.CreatedUserId
            };

            exchangeActor.ExchangeActorId = Guid.NewGuid();
            exchangeActor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            exchangeActor.OrganizationNameAmhSort = objGeez.GetSortValueU(exchangeActor.OrganizationNameAmh != null ? exchangeActor.OrganizationNameAmh : string.Empty);
            exchangeActor.OrganizationNameEngSort = objGeez.GetSortValueU(exchangeActor.OrganizationNameEng != null ? exchangeActor.OrganizationNameEng : string.Empty);
            exchangeActor.CreatedUserId = postedCustomer.CreatedUserId;
            exchangeActor.UpdatedUserId = postedCustomer.CreatedUserId;


            newServiceApplication.ExchangeActorId = exchangeActor.ExchangeActorId;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.ExchangeActor.Add(exchangeActor);
                    _context.SaveChanges();
                    transaction.Commit();

                    return new ObjectResult(_mapper.Map<ServiceApplication, ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }

        }

        private async Task<IActionResult> PostReprsentative(ExchangeActorGeneralInfoDTO postedCustomer)
        {

            //Is Customer Existed
            ResultResponse statusResponse = await IsCustomerAlreadySaved(postedCustomer.Ecxcode);

            if (statusResponse.IsSaved)
            {
                return new ObjectResult(statusResponse);
            }



            statusResponse = await GetCustomerSatus(postedCustomer.DelegatorId.GetValueOrDefault());

            if (statusResponse.IsCancelled || statusResponse.IsUnderInjunction)
            {
                return new ObjectResult(statusResponse);

            }

            // ResultResponse statusResponse = await GetCustomerRecognitionExpirStatus(postedCustomer.DelegatorId.GetValueOrDefault());


            statusResponse = await GetExchangeActorReprsentativeCountStatus(postedCustomer.DelegatorId.GetValueOrDefault());

            if (statusResponse.HaveMax)
            {
                return new ObjectResult(statusResponse);
            }



            ExchangeActor exchangeActor = null;

            exchangeActor = _mapper.Map<ExchangeActor>(postedCustomer);

            int generalInformation = (int)RecognitionServiceApplicaitonSteps.General_Information;
            int nextStep = (int)RecognitionServiceApplicaitonSteps.Owner;


            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                Status = (int)ServiceApplicaitonStatus.Drafted, // drafted state
                CurrentStep = generalInformation,
                NextStep = nextStep,
                ServiceId = (int)ServiceType.Recognition, // put recognition id here
                IsFinished = false,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                CustomerTypeId = postedCustomer.CustomerTypeId,
                CreatedUserId = postedCustomer.CreatedUserId,
                UpdatedUserId = postedCustomer.CreatedUserId
                
            };

            exchangeActor.ExchangeActorId = Guid.NewGuid();
            exchangeActor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            exchangeActor.CreatedUserId = postedCustomer.CreatedUserId;
            exchangeActor.UpdatedUserId = postedCustomer.CreatedUserId;


            newServiceApplication.ExchangeActorId = exchangeActor.ExchangeActorId;

            foreach (MemberRepresentativeType data in exchangeActor.MemberRepresentativeType)
            {
                data.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            }

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.ExchangeActor.Add(exchangeActor);
                    _context.SaveChanges();
                    transaction.Commit();

                    return new ObjectResult(_mapper.Map<ServiceApplication, ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }
        }

        private async Task<ResultResponse> CustomerHasVaidCapital(int customerTypeId, int memberCategoryId, decimal netWorthCapital)
        {
            ResultResponse netWorthCapitalStatusResponse = new ResultResponse();

            var actorCapital = await _context.RecoginationTotalCapital.FirstOrDefaultAsync(capital => 
            capital.ExchangeActorTypeId == customerTypeId && capital.MemberCategoryId == memberCategoryId);

            if(customerTypeId == (int)CustomerType.Member && memberCategoryId == (int)MemberCategory.Intermideary && netWorthCapital < actorCapital.Amount)
            {
                netWorthCapitalStatusResponse.IsMemberIntermidaryCapitalBelow = true;

            } else if(customerTypeId == (int)CustomerType.Member && memberCategoryId == (int)MemberCategory.Trader && netWorthCapital < actorCapital.Amount)
            {
                netWorthCapitalStatusResponse.IsMemberTraderCapitalBelow = true;
            }
            else if(customerTypeId == (int)CustomerType.NonMemberTrading && memberCategoryId == 0 && netWorthCapital < actorCapital.Amount)
            {
                netWorthCapitalStatusResponse.IsNonMemberDirectTraderCapitalBelow = true;
            }

            return netWorthCapitalStatusResponse;
        }

        private async Task<ResultResponse> IsCustomerAlreadySaved(string ecxCode)
        {
            ResultResponse statusResponse = new ResultResponse();

            try
            {
                var exchangeActor = await _context.ExchangeActor.FirstOrDefaultAsync(ex => ex.Ecxcode.Equals(ecxCode.Trim(), StringComparison.OrdinalIgnoreCase));

                if (exchangeActor == null)
                {
                    statusResponse.IsSaved = false;
                }
                else
                {
                    statusResponse.IsSaved = true;
                }

                return statusResponse;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<ResultResponse> GetCustomerSatus(Guid exchangeActorId)
        {

            ResultResponse statusResponse = new ResultResponse();

            try
            {
                var exchangeActor = await _context.ExchangeActor.SingleOrDefaultAsync(ea => ea.ExchangeActorId == exchangeActorId
               && ea.IsDeleted == false && ea.IsActive == true);

                if (exchangeActor.Status == (int)ExchangeActorStatus.Active)
                {
                    statusResponse.IsActive = true;
                }
                else if (exchangeActor.Status == (int)ExchangeActorStatus.Cancelled)
                {
                    statusResponse.IsCancelled = true;

                }
                else if (exchangeActor.Status == (int)ExchangeActorStatus.Under_Injunction)
                {
                    statusResponse.IsUnderInjunction = true;
                }


                return statusResponse;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<ResultResponse> GetCustomerRecognitionExpirStatus(Guid exchangeActorId)
        {
            ResultResponse expirationResponse = new ResultResponse();

            try
            {
                var exchangeActor = await _context.ExchangeActor
                    .Include(ea => ea.Delegator)
                    .SingleOrDefaultAsync(ea => ea.ExchangeActorId == exchangeActorId
                    && ea.IsDeleted == false && ea.IsActive == true);

                var result = DateTime.Now.Date.CompareTo(exchangeActor.ExpireDateTime.GetValueOrDefault().Date);

                if (exchangeActor.CustomerTypeId == (int)Enum.CustomerType.Representative || exchangeActor.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
                {
                    // compare the delegator and rep expiration date
                    var deleatorExpiratonDate = exchangeActor.ExpireDateTime.GetValueOrDefault().CompareTo(exchangeActor.Delegator.ExpireDateTime.GetValueOrDefault());

                    if (result <= 0)
                    {
                        expirationResponse.IsLicenceNotExpired = true; // licence not expired

                    }else if (deleatorExpiratonDate == 0)
                    {
                        expirationResponse.IsDelgatorNotRenewed = true;
                    }
                }
                else
                {
                    if (result < 0)
                    {
                        expirationResponse.IsLicenceNotExpired = true; // licence not expired
                    }
                    else if (result > 0)
                    {
                        expirationResponse.IsExpired = true; // final day for licence expiration

                    }
                }

                return expirationResponse;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<ResultResponse> GetExchangeActorReprsentativeCountStatus(Guid exchangeActorId)
        {
            ResultResponse repCountStatusResponse = new ResultResponse();

            try
            {

                var repCountSatus = await (from rep in _context.ExchangeActor
                                           where rep.DelegatorId == exchangeActorId && rep.Status == (int)Enum.ExchangeActorStatus.Active
                                           group rep by rep.Delegator.CustomerTypeId into x
                                           select new
                                           {
                                               CustomerType = x.Key,
                                               Count = x.Count()
                                           }).FirstOrDefaultAsync();

                if (repCountSatus == null)
                {
                    repCountStatusResponse.HaveMax = false;

                    return repCountStatusResponse;
                }



                if (repCountSatus.Count >= (int)ExchangeActorMaxReprsentative.Member && repCountSatus.CustomerType == (int)Enum.CustomerType.Member)
                {
                    repCountStatusResponse.HaveMax = true;

                }
                else if (repCountSatus.Count >= (int)ExchangeActorMaxReprsentative.NonMemberDirectTradder && repCountSatus.CustomerType == (int)Enum.CustomerType.NonMemberTrading)
                {
                    repCountStatusResponse.HaveMax = true;

                }

                return repCountStatusResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<ExchangeActorGeneralInfoDTO> GetCustomerById(string lang, Guid serviceApplicationId)
        {
            try
            {

                ExchangeActorGeneralInfoDTO data = await (from exchangeActor in _context.ExchangeActor
                                                          where exchangeActor.ServiceApplicationId == serviceApplicationId
                                                          select new ExchangeActorGeneralInfoDTO
                                                          {
                                                              MemberProduct = (from memberProduct in _context.MemberProduct
                                                                               where memberProduct.ExchangeActorId == exchangeActor.ExchangeActorId
                                                                               select new MemberProductDTO
                                                                               {
                                                                                   CommodityId = memberProduct.CommodityId
                                                                               }).ToList(),
                                                              CustomerBussinessFiled = (from buissinessfields in _context.CustomerBussinessFiled
                                                                               where buissinessfields.ExchangeActorId == exchangeActor.ExchangeActorId
                                                                               select new CustomerBusinessFiledDto
                                                                               {
                                                                                   BusinessFiledId = buissinessfields.BusinessFiledId
                                                                               }).ToList(),
                                                              MemberRepresentativeType = (from memberRepresentative in _context.MemberRepresentativeType
                                                                                          where memberRepresentative.ExchangeActorId == exchangeActor.ExchangeActorId
                                                                                          select new MemberRepresentativeTypeDTO
                                                                                          {
                                                                                              RepresentativeTypeId = memberRepresentative.RepresentativeTypeId
                                                                                          }).ToList(),
                                                              DelgatorOrganizationName = lang == "et" ? exchangeActor.Delegator.OrganizationNameAmh : exchangeActor.Delegator.OrganizationNameEng,

                                                              ExchangeActorId = exchangeActor.ExchangeActorId,
                                                              Tin = exchangeActor.Tin,
                                                              Ecxcode = exchangeActor.Ecxcode,
                                                              CustomerTypeId = exchangeActor.CustomerTypeId,
                                                              MemberCategoryId = exchangeActor.MemberCategoryId,
                                                              DelegatorId = exchangeActor.DelegatorId,
                                                              OrganizationNameEng = exchangeActor.OrganizationNameEng,
                                                              OrganizationNameAmh = exchangeActor.OrganizationNameAmh,
                                                              OrganizationTypeId = exchangeActor.OrganizationTypeId,
                                                              // BuisnessFiledId = exchangeActor.BuisnessFiledId,
                                                              //CustomerBusinessFiled = (from customerBusinessFiled in _context.CustomerBussinessFiled
                                                              //                         where customerBusinessFiled.ServiceApplicationId == exchangeActor.ServiceApplicationId
                                                              //                         select new CustomerBusinessFiledDto
                                                              //                         {
                                                              //                             BusinessFiledId = customerBusinessFiled.BusinessFiledId
                                                              //                         }).ToList(),
                                                              Email = exchangeActor.Email,
                                                              RegionId = exchangeActor.RegionId,
                                                              ZoneId = exchangeActor.ZoneId,
                                                              WoredaId = exchangeActor.WoredaId,
                                                              KebeleId = exchangeActor.KebeleId,
                                                              MobileNo = exchangeActor.MobileNo,
                                                              Tel = exchangeActor.Tel,
                                                              FaxNo = exchangeActor.FaxNo,
                                                              OrganizationAreaNameAmh = exchangeActor.OrganizationAreaNameAmh,
                                                              OrganizationAreaNameEng = exchangeActor.OrganizationAreaNameEng,
                                                              Pobox = exchangeActor.Pobox,
                                                              HouseNo = exchangeActor.HouseNo,
                                                              NetWorthCapital = exchangeActor.NetWorthCapital.GetValueOrDefault(0),
                                                              OrganizationEstablishedDate = exchangeActor.OrganizationEstablishedDate,
                                                              HeadOffice = exchangeActor.HeadOffice,
                                                              HeadOfficeAmh = exchangeActor.HeadOfficeAmh,
                                                              DelegatorTypeId = exchangeActor.Delegator.CustomerTypeId

                                                          })
                                   .FirstOrDefaultAsync();



                return data;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<IActionResult> UpdateExchangeActorAmendment(ExchangeActorGeneralInfoDTO updatedCustomer)
        {

            if (updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.Representative || updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
            {
                return await UpdateExchangeActorReprsentativeForAmendement(updatedCustomer);
            }

            //check the capital type
            ResultResponse netWorthCapitalResponse = new ResultResponse();

            if (updatedCustomer.NetWorthCapital != 0)
            {
                netWorthCapitalResponse = await CustomerHasVaidCapital(updatedCustomer.CustomerTypeId, updatedCustomer.MemberCategoryId.GetValueOrDefault(), updatedCustomer.NetWorthCapital);
            }


            if (netWorthCapitalResponse.IsMemberIntermidaryCapitalBelow == true || netWorthCapitalResponse.IsMemberTraderCapitalBelow == true
                 || netWorthCapitalResponse.IsNonMemberDirectTraderCapitalBelow == true)
            {
                return new ObjectResult(netWorthCapitalResponse);
            }

            try
            {
                var oldCustomer = await _context.ExchangeActor.FirstOrDefaultAsync(c =>
                    c.ExchangeActorId == updatedCustomer.ExchangeActorId);
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCustomer.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCustomer.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ServiceApplicationId == oldCustomer.ServiceApplicationId).FirstOrDefault();

                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == updatedCustomer.ExchangeActorId).FirstOrDefault();

                if (updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.Member || updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
                {
                    await DeleteMemberProduct(updatedCustomer.ExchangeActorId);
                    await DeleteCustomerBussinessField(updatedCustomer.ExchangeActorId);
                }
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {                     
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = updatedCustomer.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = updatedCustomer.ServiceApplicationId;
                    }
                }
                else
                {
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = (int)RecognitionServiceApplicaitonSteps.PaymentOrder,
                        NextStep = (int)RecognitionServiceApplicaitonSteps.PaymentOrder,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = updatedCustomer.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = updatedCustomer.CustomerTypeId,
                        Ecxcode = updatedCustomer.Ecxcode,
                        CreatedUserId = updatedCustomer.CreatedUserId,
                        UpdatedUserId = updatedCustomer.CreatedUserId,
                        ExchangeActorId = updatedCustomer.ExchangeActorId,
                         
                    };
                    _context.Add(newServiceApplication);
                    updatedCustomer.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }

                updatedCustomer.IsActive = oldCustomer.IsActive;

                updatedCustomer.IsDeleted = oldCustomer.IsDeleted;
                updatedCustomer.CreatedUserId = oldCustomer.CreatedUserId;
                if (oldCustomer != null)
                {
                    CVGeez objGeez = new CVGeez();

                    oldCustomer = _mapper.Map<ExchangeActorGeneralInfoDTO, ExchangeActor>(updatedCustomer, oldCustomer);

                    oldCustomer.OrganizationNameAmhSort = objGeez.GetSortValueU(updatedCustomer.OrganizationNameAmh != null ? updatedCustomer.OrganizationNameAmh : string.Empty);
                    oldCustomer.OrganizationNameEngSort = objGeez.GetSortValueU(updatedCustomer.OrganizationNameEng != null ? updatedCustomer.OrganizationNameEng : string.Empty);


                    await _context.SaveChangesAsync();

                    return new ObjectResult(true);
                }

                return new ObjectResult(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ExchangeActorServicesDto> GetServiceByExchangeActor(Guid ExchagneActorId, string lang)
        {
            try
            {
                int renewalService = (int)ServiceType.Renewal;
                int cancllationService = (int)ServiceType.Cancellation;
                int RenewalByPenality = (int)ServiceType.RenewalWithPenality;
                int reprsentative = (int)CustomerType.Representative;
                int nonrepresntative = (int)CustomerType.NonMemberDirectTraderTradeReprsentative;
                int Associlation = (int)CustomerType.NationalTradeAssociation;

                ExchangeActorServicesDto exchangeActorServices = new ExchangeActorServicesDto();

                var exchangeActor = await _context.ExchangeActor.FirstOrDefaultAsync(x => x.ExchangeActorId == ExchagneActorId);
                List<RepresentativeViewModelForProfile>  represntativechange = new List<RepresentativeViewModelForProfile>();
                if (exchangeActor.CustomerTypeId ==reprsentative || exchangeActor.CustomerTypeId == nonrepresntative)
                {
                     represntativechange = await (from cer in _context.RepresentativeChange
                                          join old in _context.ExchangeActor on
                                           cer.OldDelegatorId equals old.ExchangeActorId
                                          join newrep in _context.ExchangeActor on
                                          cer.NewDelegatorId equals newrep.ExchangeActorId
                                          join sa in _context.ServiceApplication on
                                         cer.ServiceApplicationId equals sa.ServiceApplicationId
                                          where cer.RepresentativeExchangeActorId == ExchagneActorId && sa.IsFinished ==true
                                          orderby cer.CreatedDateTime
                                          select new RepresentativeViewModelForProfile
                                          {
                                              CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime) : sa.UpdatedDateTime.Date.ToString(),
                                              NewDelegatorName = (lang == "et") ? newrep.OrganizationNameAmh : newrep.OrganizationNameEng,
                                              OldDelegatorName = (lang == "et") ? old.OrganizationNameAmh : old.OrganizationNameEng,
                                              

                                          })
                               .ToListAsync();
                }
                var renewals = await  (from cer in _context.Certificate
                                        join sa in _context.ServiceApplication on
                                         cer.ServiceApplicationId equals sa.ServiceApplicationId
                                         where cer.ExchangeActorId == ExchagneActorId && (cer.ServiceId == renewalService||cer.ServiceId== RenewalByPenality) && sa.IsFinished ==true
                                         orderby cer.IssuedDateTime
                                       select new CertificateForProfile
                                       {
                                       ExchangeActorId = cer.ExchangeActorId,
                                       IssuedDateTime = cer.IssuedDateTime,
                                       ExpireDateTime = cer.ExpireDateTime,
                                       ServiceApplicationId =cer.ServiceApplicationId,
                                       RenewalType = cer.ServiceId
                                       })
                                  .ToListAsync();

                var cancellations = await (from cer in _context.Cancellation
                                      join sa in _context.ServiceApplication on
                                       cer.ServiceApplicationId equals sa.ServiceApplicationId
                                      join lu in _context.Lookup on
                                      cer.CancellationType equals lu.LookupId
                                      where cer.ExchangeActorId == ExchagneActorId && sa.ServiceId == cancllationService && sa.IsFinished == true
                                      orderby cer.CreatedDateTime
                                      select new CancellationVM
                                      {
                                          CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(cer.CreatedDateTime) : cer.CreatedDateTime.Date.ToString(),
                                          CancellationType = (lang == "et") ? lu.DescriptionAmh : lu.DescriptionEng,
                                          Description = cer.Description

                                      })
                                  .ToListAsync();
                var AddressChange = await (from dc in _context.DataChange
                                           join sa in _context.ServiceApplication on
                                            dc.ServiceApplicationId equals sa.ServiceApplicationId
                                           join re in _context.Region on
                                            dc.OldRegionId equals re.RegionId
                                           join zo in _context.Zone on
                                            dc.OldZoneId equals zo.ZoneId
                                           join ke in _context.Kebele on
                                           dc.OldKebeleId equals ke.KebeleId
                                           join wo in _context.Woreda on
                                            dc.OldWoredaId  equals wo.WoredaId
                                           where dc.ExchangeActorId == ExchagneActorId && dc.DataChangeTypeId == 37 && sa.IsFinished == true
                                           orderby dc.CreatedDateTime
                                           select new AddressChangeDTOForProfile
                                           {
                                               RegionId = (lang == "et") ? re.DescriptionAmh :re.DescriptionEng ,
                                               ZoneId = (lang == "et") ? zo.DescriptionAmh : zo.DescriptionEng,
                                               WoredaId = (lang == "et") ? wo.DescriptionAmh :wo.DescriptionEng ,
                                               KebeleId = (lang == "et") ? ke.DescriptionAmh :ke.DescriptionEng ,
                                               HouseNo = dc.OldHouseNo,
                                               Email =  dc.OldEmail,
                                               Tel =  dc.OldTel,
                                               MobileNo =  dc.OldMobileNo,
                                               FaxNo =  dc.OldFaxNo,
                                               Pobox =  dc.OldPobox,
                                               OrganizationAreaNameEng=  dc.OldOrganizationAreaNameEng,
                                               OrganizationAreaNameAmh = dc.OldOrganizationAreaNameAmh,
                                               CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime) : sa.UpdatedDateTime.Date.ToString(),

                                           })
                                      .ToListAsync();
                var OwnerManagerAddressChange = await (from dc in _context.DataChange
                                                       join sa in _context.ServiceApplication on
                                                        dc.ServiceApplicationId equals sa.ServiceApplicationId
                                                       join re in _context.Region on
                                                        dc.OldRegionId equals re.RegionId
                                                       join zo in _context.Zone on
                                                        dc.OldZoneId equals zo.ZoneId
                                                       join ke in _context.Kebele on
                                                       dc.OldKebeleId equals ke.KebeleId
                                                       join wo in _context.Woreda on
                                                        dc.OldWoredaId equals wo.WoredaId
                                                       where dc.ExchangeActorId == ExchagneActorId && dc.DataChangeTypeId == 78 && sa.IsFinished == true
                                                       orderby dc.CreatedDateTime
                                                       select new AddressChangeDTOForProfile
                                                       {
                                                           RegionId = (lang == "et") ? re.DescriptionAmh : re.DescriptionEng,
                                                           ZoneId = (lang == "et") ? zo.DescriptionAmh : zo.DescriptionEng,
                                                           WoredaId = (lang == "et") ? wo.DescriptionAmh : wo.DescriptionEng,
                                                           KebeleId = (lang == "et") ? ke.DescriptionAmh : ke.DescriptionEng,
                                                           HouseNo = dc.OldHouseNo,
                                                           Email = dc.OldEmail,
                                                           Tel = dc.OldTel,
                                                           MobileNo = dc.OldMobileNo,
                                                           FaxNo = dc.OldFaxNo,
                                                           Pobox = dc.OldPobox,
                                                           OrganizationAreaNameEng = dc.OldOrganizationAreaNameEng,
                                                           OrganizationAreaNameAmh = dc.OldOrganizationAreaNameAmh,
                                                           CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime) : sa.UpdatedDateTime.Date.ToString(),

                                                       })
                                      .ToListAsync();
                var FinacilaInsititutionAddressChange = await (from dc in _context.DataChange
                                                       join sa in _context.ServiceApplication on
                                                        dc.ServiceApplicationId equals sa.ServiceApplicationId
                                                       where dc.ExchangeActorId == ExchagneActorId && dc.DataChangeTypeId == 182 && sa.IsFinished == true
                                                       orderby dc.CreatedDateTime
                                                       select new FinancilaInstutiutionDataChangeDTOForProfile
                                                       {
                                                      
                                                           Tel = dc.OldTel,
                                                           FaxNo = dc.OldFaxNo,
                                                           Pobox = dc.OldPobox,
                                                           HeadOffice = dc.OldHeadOffice,
                                                           HeadOfficeAmh= dc.OldHeadOfficeAmh,
                                                           CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime) : sa.UpdatedDateTime.Date.ToString(),

                                                       })
                                             .ToListAsync();
                var OrganizationNamechange = await (from dc in _context.DataChange
                                           join sa in _context.ServiceApplication on
                                            dc.ServiceApplicationId equals sa.ServiceApplicationId
                                           where dc.ExchangeActorId == ExchagneActorId && dc.DataChangeTypeId == 0 && sa.IsFinished == true
                                           orderby dc.CreatedDateTime
                                           select new NameChangeDTOForProfile
                                           {
                                               CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(sa.UpdatedDateTime) : sa.UpdatedDateTime.Date.ToString(),
                                               OldOrganizationNameAmh =dc.OldOrganizationNameAmh,
                                               OldOrganizationNameEng =dc.OldOrganizationNameEng,
                                               OrganizationNameAmh = (dc.OrganizationNameAmh ==null)?dc.OldOrganizationNameAmh: dc.OrganizationNameAmh,
                                               OrganizationNameEng = (dc.OrganizationNameEng == null) ? dc.OldOrganizationNameEng : dc.OrganizationNameEng,

                                           })
                                      .ToListAsync();

                exchangeActorServices.AddressChange = AddressChange;
                exchangeActorServices.OrganizationNamechange = OrganizationNamechange;
                exchangeActorServices.Cancel = cancellations;
                exchangeActorServices.OwnerManagerAddressChange = OwnerManagerAddressChange;
                exchangeActorServices.FinacilaInsititutionAddressChange = FinacilaInsititutionAddressChange;


                var replacment = await _context.Replacement.Where(x => x.ExchangeActorId == ExchagneActorId).ToListAsync();
                var bans = await _context.Injunction.Where(x => x.ExchangeActorId == ExchagneActorId && x.ApprovalFinished ==true).ToListAsync();
                var looks = _context.Lookup;

                if (renewals.Count() != 0)
                {
                    List<RenewalVm> renewalVms = new List<RenewalVm>();

                    foreach (var item in renewals)
                    {

                        RenewalVm vm = new RenewalVm()
                        {
                            IssueDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(exchangeActor.IssueDateTime.GetValueOrDefault()) : exchangeActor.IssueDateTime.GetValueOrDefault().Date.ToString(),
                            ExpireDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.ExpireDateTime) : exchangeActor.ExpireDateTime.GetValueOrDefault().Date.ToString(),
                            WithPenalityRenewal = (item.RenewalType==RenewalByPenality)? "Yes":"No",
                            
                        };
                        renewalVms.Add(vm);
                    }
                    exchangeActorServices.Renewals = renewalVms;

                }

             

                if (represntativechange.Count() != 0)
                {
                    List<RepresentativeViewModelForProfile> repchanges = new List<RepresentativeViewModelForProfile>();
                    foreach (var repchange in represntativechange)
                    {
                        RepresentativeViewModelForProfile repdata = new RepresentativeViewModelForProfile()
                        {
                            NewDelegatorName = repchange.NewDelegatorName,
                            CreatedDateTime = repchange.CreatedDateTime,
                            OldDelegatorName = repchange.OldDelegatorName,

                        };
                        repchanges.Add(repdata);
                    }
                    exchangeActorServices.RepresentativeChange = repchanges;

                }

                if (replacment.Count() != 0)
                {
                    List<ReplacementVM> replacmentdata = new List<ReplacementVM>();
                    foreach (var rep in replacment)
                    {
                        ReplacementVM cancellationDTO = new ReplacementVM()
                        {
                            CreatedDateTime = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(rep.CreatedDateTime) : rep.CreatedDateTime.ToString(),

                        };
                        replacmentdata.Add(cancellationDTO);
                    }
                    exchangeActorServices.Replacment = replacmentdata;

                }
                if (bans.Count() != 0)
                {
                    List<InjunctionForProfile> injunctionViews = new List<InjunctionForProfile>();
                    foreach (var item in bans)
                    {
                        var reason = looks.FirstOrDefault(x => x.LookupId == item.Reason);
                        var injuct = looks.FirstOrDefault(x => x.LookupId == item.InjunctionBodyId);
                        var lift = looks.FirstOrDefault(x => x.LookupId == item.InjunctionLiftedBy);

                        if (item.InjunctionLiftedDate.Year>1000)
                        {
                            InjunctionForProfile vm = new InjunctionForProfile()

                            {
                                Reason = (lang == "et") ? reason.DescriptionAmh : reason.DescriptionEng,
                                InjunctionLetterNo = item.InjunctionLetterNo,
                                InjunctionBodyId = (lang == "et") ? injuct.DescriptionAmh : injuct.DescriptionEng,
                                InjunctionStartDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.InjunctionStartDate) : item.InjunctionStartDate.ToString(),
                                InjunctionEndDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.InjunctionEndDate) : item.InjunctionEndDate.ToString(),
                                InjunctionLiftedBy = (lang == "et") ? lift.DescriptionAmh : lift.DescriptionEng,
                                InjunctionLiftedLetterNo = item.InjunctionLiftedLetterNo,
                                InjunctionLiftedDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.InjunctionLiftedDate) : item.InjunctionLiftedDate.ToString(),
                                InjunctionLiftReason = item.InjunctionLiftReason

                            };
                            injunctionViews.Add(vm);

                        }
                        else
                        {
                            InjunctionForProfile vm = new InjunctionForProfile()
                            {
                                Reason = (lang == "et") ? reason.DescriptionAmh : reason.DescriptionEng,
                                InjunctionLetterNo = item.InjunctionLetterNo,
                                InjunctionBodyId = (lang == "et") ? injuct.DescriptionAmh : injuct.DescriptionEng,
                                InjunctionStartDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.InjunctionStartDate) : item.InjunctionStartDate.ToString(),
                                InjunctionEndDate = (lang == "et") ? EthiopicDateTime.GetEthiopicDate(item.InjunctionEndDate) : item.InjunctionEndDate.ToString(),

                            };
                            injunctionViews.Add(vm);

                        }
                    }
                    exchangeActorServices.Ban = injunctionViews;

                }

                return exchangeActorServices;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<ExchangeActorGeneralInfoDTO> GetCustomerAmendomentById(string lang, Guid exchangeActorId)
        {
            try
            {

                ExchangeActorGeneralInfoDTO data = await (from exchangeActor in _context.ExchangeActor
                                                          join sa in _context.ServiceApplication on
                                                          exchangeActor.ServiceApplicationId equals sa.ServiceApplicationId
                                                          where exchangeActor.ExchangeActorId == exchangeActorId &&
                                                          exchangeActor.Status == 20 && 
                                                          (sa.ServiceId == 1 || sa.ServiceId==13)
                                                          select new ExchangeActorGeneralInfoDTO
                                                          {
                                                              MemberProduct = (from memberProduct in _context.MemberProduct
                                                                               where memberProduct.ExchangeActorId == exchangeActor.ExchangeActorId
                                                                               select new MemberProductDTO
                                                                               {
                                                                                   CommodityId = memberProduct.CommodityId
                                                                               }).ToList(),
                                                              CustomerBussinessFiled = (from bussinessfiled in _context.CustomerBussinessFiled
                                                                               where bussinessfiled.ExchangeActorId == exchangeActor.ExchangeActorId
                                                                               select new CustomerBusinessFiledDto
                                                                               {
                                                                                   BusinessFiledId = bussinessfiled.BusinessFiledId
                                                                               }).ToList(),
                                                              MemberRepresentativeType = (from memberRepresentative in _context.MemberRepresentativeType
                                                                                          where memberRepresentative.ServiceApplicationId == exchangeActor.ServiceApplicationId
                                                                                          select new MemberRepresentativeTypeDTO
                                                                                          {
                                                                                              RepresentativeTypeId = memberRepresentative.RepresentativeTypeId
                                                                                          }).ToList(),
                                                              DelgatorOrganizationName = lang == "et" ? exchangeActor.Delegator.OrganizationNameAmh : exchangeActor.Delegator.OrganizationNameEng,

                                                              ExchangeActorId = exchangeActor.ExchangeActorId,
                                                              Tin = exchangeActor.Tin,
                                                              Ecxcode = exchangeActor.Ecxcode,
                                                              CustomerTypeId = exchangeActor.CustomerTypeId,
                                                              MemberCategoryId = exchangeActor.MemberCategoryId,
                                                              DelegatorId = exchangeActor.DelegatorId,
                                                              OrganizationNameEng = exchangeActor.OrganizationNameEng,
                                                              OrganizationNameAmh = exchangeActor.OrganizationNameAmh,
                                                              OrganizationTypeId = exchangeActor.OrganizationTypeId,
                                                              // BuisnessFiledId = exchangeActor.BuisnessFiledId,
                                                              //CustomerBusinessFiled = (from customerBusinessFiled in _context.CustomerBussinessFiled
                                                              //                         where customerBusinessFiled.ServiceApplicationId == exchangeActor.ServiceApplicationId
                                                              //                         select new CustomerBusinessFiledDto
                                                              //                         {
                                                              //                             BusinessFiledId = customerBusinessFiled.BusinessFiledId
                                                              //                         }).ToList(),
                                                              Email = exchangeActor.Email,
                                                              RegionId = exchangeActor.RegionId,
                                                              ZoneId = exchangeActor.ZoneId,
                                                              WoredaId = exchangeActor.WoredaId,
                                                              KebeleId = exchangeActor.KebeleId,
                                                              MobileNo = exchangeActor.MobileNo,
                                                              Tel = exchangeActor.Tel,
                                                              FaxNo = exchangeActor.FaxNo,
                                                              OrganizationAreaNameAmh = exchangeActor.OrganizationAreaNameAmh,
                                                              OrganizationAreaNameEng = exchangeActor.OrganizationAreaNameEng,
                                                              Pobox = exchangeActor.Pobox,
                                                              HouseNo = exchangeActor.HouseNo,
                                                              NetWorthCapital = exchangeActor.NetWorthCapital.GetValueOrDefault(0),
                                                              OrganizationEstablishedDate = exchangeActor.OrganizationEstablishedDate,
                                                              HeadOffice = exchangeActor.HeadOffice,
                                                              HeadOfficeAmh = exchangeActor.HeadOfficeAmh,
                                                              DelegatorTypeId = exchangeActor.Delegator.CustomerTypeId,
                                                              ServiceApplication =sa

                                                          })
                                   .FirstOrDefaultAsync();



                return data;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<IActionResult> UpdateCustomer(ExchangeActorGeneralInfoDTO updatedCustomer)
        {

            if (updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.Representative || updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
            {
                return await UpdateExchangeActorReprsentative(updatedCustomer);
            }

            //check the capital type
            ResultResponse netWorthCapitalResponse = new ResultResponse();

            if (updatedCustomer.NetWorthCapital != 0)
            {
                netWorthCapitalResponse = await CustomerHasVaidCapital(updatedCustomer.CustomerTypeId, updatedCustomer.MemberCategoryId.GetValueOrDefault(), updatedCustomer.NetWorthCapital);
            }

            if (netWorthCapitalResponse.IsMemberIntermidaryCapitalBelow == true || netWorthCapitalResponse.IsMemberTraderCapitalBelow == true
                 || netWorthCapitalResponse.IsNonMemberDirectTraderCapitalBelow == true)
            {
                return new ObjectResult(netWorthCapitalResponse);
            }

            try
            {
               
                var oldCustomer = await _context.ExchangeActor.FirstOrDefaultAsync(c =>
                    c.ExchangeActorId == updatedCustomer.ExchangeActorId);

                if (updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.Member || updatedCustomer.CustomerTypeId == (int)Enum.CustomerType.NonMemberDirectTraderTradeReprsentative)
                {
                    await DeleteMemberProduct(updatedCustomer.ExchangeActorId);
                    await DeleteCustomerBussinessField(updatedCustomer.ExchangeActorId);
                }
                updatedCustomer.IsActive = oldCustomer.IsActive;
                updatedCustomer.IsDeleted = oldCustomer.IsDeleted;
                updatedCustomer.CreatedUserId = oldCustomer.CreatedUserId;
                updatedCustomer.CreatedDateTime = oldCustomer.CreatedDateTime;
                updatedCustomer.UpdatedDateTime = DateTime.Now;

                if (oldCustomer != null)
                {
                    CVGeez objGeez = new CVGeez();

                    oldCustomer = _mapper.Map<ExchangeActorGeneralInfoDTO, ExchangeActor>(updatedCustomer, oldCustomer);

                    oldCustomer.OrganizationNameAmhSort = objGeez.GetSortValueU(updatedCustomer.OrganizationNameAmh != null ? updatedCustomer.OrganizationNameAmh : string.Empty);
                    oldCustomer.OrganizationNameEngSort = objGeez.GetSortValueU(updatedCustomer.OrganizationNameEng != null ? updatedCustomer.OrganizationNameEng : string.Empty);


                    await _context.SaveChangesAsync();

                    return new ObjectResult(true);
                }

                return new ObjectResult(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<IActionResult> UpdateExchangeActorReprsentative(ExchangeActorGeneralInfoDTO updatedCustomer)
        {

            ResultResponse statusResponse = await GetCustomerSatus(updatedCustomer.DelegatorId.GetValueOrDefault());

            if (statusResponse.IsCancelled || statusResponse.IsUnderInjunction)
            {
                return new ObjectResult(statusResponse);

            }

            //statusResponse = await GetExchangeActorReprsentativeCountStatus(updatedCustomer.DelegatorId.GetValueOrDefault());


            //if (statusResponse.HaveMax)
            //{
            //    return new ObjectResult(statusResponse);
            //}


            try
            {

                var oldCustomer = await _context.ExchangeActor.FirstOrDefaultAsync(c =>
                    c.ExchangeActorId == updatedCustomer.ExchangeActorId);

                await DeleteMemberProduct(updatedCustomer.ExchangeActorId);

                updatedCustomer.IsActive = oldCustomer.IsActive;
                updatedCustomer.IsDeleted = oldCustomer.IsDeleted;
                updatedCustomer.CreatedUserId = oldCustomer.CreatedUserId;
                updatedCustomer.CreatedDateTime = oldCustomer.CreatedDateTime;
                updatedCustomer.UpdatedDateTime = DateTime.Now;

                var serviceApplicationId = oldCustomer.ServiceApplicationId;

                if (oldCustomer != null)
                {

                    oldCustomer = _mapper.Map<ExchangeActorGeneralInfoDTO, ExchangeActor>(updatedCustomer, oldCustomer);


                    foreach (MemberRepresentativeType data in oldCustomer.MemberRepresentativeType)
                    {
                        data.ServiceApplicationId = serviceApplicationId;
                    }


                    await _context.SaveChangesAsync();

                    return new ObjectResult(true);
                }

                return new ObjectResult(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        private async Task<IActionResult> UpdateExchangeActorReprsentativeForAmendement(ExchangeActorGeneralInfoDTO updatedCustomer)
        {

            ResultResponse statusResponse = await GetCustomerSatus(updatedCustomer.DelegatorId.GetValueOrDefault());

            if (statusResponse.IsCancelled || statusResponse.IsUnderInjunction)
            {
                return new ObjectResult(statusResponse);

            }

            //no need to maz because of update
            //statusResponse = await GetExchangeActorReprsentativeCountStatus(updatedCustomer.DelegatorId.GetValueOrDefault());


            //if (statusResponse.HaveMax)
            //{
            //    return new ObjectResult(statusResponse);
            //}



            try
            {

                var oldCustomer = await _context.ExchangeActor.FirstOrDefaultAsync(c =>
                    c.ExchangeActorId == updatedCustomer.ExchangeActorId);
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCustomer.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCustomer.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ServiceApplicationId == oldCustomer.ServiceApplicationId).FirstOrDefault();

                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == updatedCustomer.ExchangeActorId).FirstOrDefault();

                await DeleteMemeberRepresentativeType(updatedCustomer.ExchangeActorId);

                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {
                       
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = updatedCustomer.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = updatedCustomer.ServiceApplicationId;
                    }
                }
                else
                {
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = (int)RecognitionServiceApplicaitonSteps.PaymentOrder,
                        NextStep = (int)RecognitionServiceApplicaitonSteps.PaymentOrder,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = updatedCustomer.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = updatedCustomer.CustomerTypeId,
                        Ecxcode = updatedCustomer.Ecxcode,
                        CreatedUserId = updatedCustomer.CreatedUserId,
                        UpdatedUserId = updatedCustomer.CreatedUserId,
                        ExchangeActorId = updatedCustomer.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    updatedCustomer.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }

             

                updatedCustomer.IsActive = oldCustomer.IsActive;
                updatedCustomer.IsDeleted = oldCustomer.IsDeleted;
                updatedCustomer.CreatedUserId = oldCustomer.CreatedUserId;
                updatedCustomer.CreatedDateTime = oldCustomer.CreatedDateTime;
                updatedCustomer.UpdatedDateTime =DateTime.Now;



                if (oldCustomer != null)
                {

                    oldCustomer = _mapper.Map<ExchangeActorGeneralInfoDTO, ExchangeActor>(updatedCustomer, oldCustomer);


                    foreach (MemberRepresentativeType data in oldCustomer.MemberRepresentativeType)
                    {
                        data.ServiceApplicationId = oldCustomer.ServiceApplicationId;
                    }


                    await _context.SaveChangesAsync();

                    return new ObjectResult(true);
                }

                return new ObjectResult(false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public async Task<PagedResult<CusteromSearchDTO>> SearchCustomer(CustomerQueryParameters customerQueryParameters)
        {
            int totalSearchResult = 0;

            try
            {
                List<CusteromSearchDTO> customerList = null;

                if (customerQueryParameters.Lang == "et")
                {
                    customerList = await SearchCustomerInAmharic(customerQueryParameters);
                }
                else
                {
                    customerList = await SearchCustomerInEng(customerQueryParameters);
                }



                totalSearchResult = customerList.Count();

                var pagedResult = customerList.AsQueryable().Paging(customerQueryParameters.PageCount, customerQueryParameters.PageNumber);

                return new PagedResult<CusteromSearchDTO>()
                {
                    Items = _mapper.Map<List<CusteromSearchDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<List<CusteromSearchDTO>> SearchCustomerInAmharic(CustomerQueryParameters customerQueryParameters)
        {

            try
            {
                CVGeez objGeez = new CVGeez();

                List<CusteromSearchDTO> customerData = await (from c in _context.ExchangeActor
                                                              join s in _context.ServiceApplication
                                                              on c.ExchangeActorId equals s.ExchangeActorId
                                                              join l in _context.Lookup
                                                              on c.CustomerTypeId equals l.LookupId
                                                              join cert in _context.Certificate
                                                              on c.ServiceApplicationId equals cert.ServiceApplicationId
                                                              where ((string.IsNullOrEmpty(customerQueryParameters.Ecxcode) || c.Ecxcode.Contains(customerQueryParameters.Ecxcode.Trim())) &&
                                                              ((customerQueryParameters.Tin == null) || c.Tin.Contains(customerQueryParameters.Tin.Trim())) &&
                                                              (c.OrganizationNameAmhSort.Contains(objGeez.GetSortValueU(customerQueryParameters.OrganizationName == null ? string.Empty : customerQueryParameters.OrganizationName.Trim()))) &&
                                                              (customerQueryParameters.CustomerType == null || (c.CustomerTypeId == customerQueryParameters.CustomerType)) &&
                                                              (customerQueryParameters.MemberCategory == null || (c.MemberCategoryId == customerQueryParameters.MemberCategory)) &&
                                                              c.IsDeleted == false && s.IsFinished == true && s.ServiceId == (int)ServiceType.Recognition) && (c.OwnerManager.IsManager == true || c.OwnerManager.IsEceaContact == true || c.CustomerTypeId == (int)CustomerType.SeetlementBank)
                                                              orderby (s.CreatedDateTime) descending

                                                              select new CusteromSearchDTO
                                                              {
                                                                  Ecxcode = c.Ecxcode,
                                                                  ExchangeActorId = c.ExchangeActorId,
                                                                  OrganizationName = c.OrganizationNameAmh,
                                                                  CustomerType = l.DescriptionAmh,
                                                                  LegalBodyName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.FirstNameAmh : " ",
                                                                  LegalBodyFatherName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.FatherNameAmh : " ",
                                                                  LegalBodyGrandFatherName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.GrandFatherNameAmh : " ",
                                                                  CustomerTypeId = l.LookupId,
                                                                  FullName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? (c.OwnerManager.FirstNameAmh + " " + c.OwnerManager.FatherNameAmh + " " + c.OwnerManager.GrandFatherNameAmh) : " ",
                                                                  ServiceApplicationId = s.ServiceApplicationId,
                                                                  Status = c.Status,
                                                                  ActiveReprsentativeCount = (from representative in _context.ExchangeActor
                                                                                              where representative.DelegatorId == c.ExchangeActorId
                                                                                              select representative).Count(),
                                                                  IsLicenceExpired = DateTime.Now.Date.CompareTo(cert.ExpireDateTime.AddMonths(4)) > 0 ? true : false,
                                                                  Tin = c.Tin,
                                                                  DelegatorId = c.DelegatorId.GetValueOrDefault(),
                                                                  MemberCategory = c.MemberCategory.DescriptionAmh,
                                                                  MemberCategoryId = c.MemberCategoryId.GetValueOrDefault(),
                                                                  MemberRepresentativeType = (from memberRepresentative in _context.MemberRepresentativeType
                                                                                              where memberRepresentative.ExchangeActorId == c.ExchangeActorId
                                                                                              select new MemberRepresentativeTypeDTO
                                                                                              {
                                                                                                  RepresentativeTypeId = memberRepresentative.RepresentativeTypeId
                                                                                              }).ToList(),

                                                              }).AsNoTracking()
                                                                .ToListAsync();

                return customerData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<List<CusteromSearchDTO>> SearchCustomerInEng(CustomerQueryParameters customerQueryParameters)
        {
            try
            {
                List<CusteromSearchDTO> customerData = await (from c in _context.ExchangeActor
                                                              join s in _context.ServiceApplication
                                                              on c.ServiceApplicationId equals s.ServiceApplicationId
                                                              join l in _context.Lookup
                                                              on c.CustomerTypeId equals l.LookupId
                                                              join cert in _context.Certificate
                                                              on c.ServiceApplicationId equals cert.ServiceApplicationId
                                                              where ((string.IsNullOrEmpty(customerQueryParameters.Ecxcode) || c.Ecxcode.Contains(customerQueryParameters.Ecxcode.Trim())) &&
                                                              ((customerQueryParameters.Tin == null) || c.Tin.Contains(customerQueryParameters.Tin.Trim())) &&
                                                              ((string.IsNullOrEmpty(customerQueryParameters.OrganizationName)) || c.OrganizationNameEng.Contains(customerQueryParameters.OrganizationName)) &&
                                                              (customerQueryParameters.CustomerType == null || (c.CustomerTypeId == customerQueryParameters.CustomerType)) &&
                                                              (customerQueryParameters.MemberCategory == null || (c.MemberCategoryId == customerQueryParameters.MemberCategory)) &&
                                                              c.IsDeleted == false &&
                                                              s.IsFinished == true &&
                                                              s.ServiceId == (int)ServiceType.Recognition) && (c.OwnerManager.IsManager == true || c.OwnerManager.IsEceaContact == true || c.CustomerTypeId == (int)CustomerType.SeetlementBank)
                                                              orderby (s.CreatedDateTime) descending

                                                              select new CusteromSearchDTO
                                                              {
                                                                  Ecxcode = c.Ecxcode,
                                                                  ExchangeActorId = c.ExchangeActorId,
                                                                  OrganizationName = c.OrganizationNameEng,
                                                                  CustomerType = l.DescriptionEng,
                                                                  LegalBodyName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.FirstNameEng : " ",
                                                                  LegalBodyFatherName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.FatherNameEng : " ",
                                                                  LegalBodyGrandFatherName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? c.OwnerManager.GrandFatherNameEng : " ",
                                                                  CustomerTypeId = l.LookupId,
                                                                  FullName = (c.CustomerTypeId != (int)CustomerType.SeetlementBank) ? (c.OwnerManager.FirstNameEng + " " + c.OwnerManager.FatherNameEng + " " + c.OwnerManager.GrandFatherNameEng) : " ",
                                                                  ServiceApplicationId = s.ServiceApplicationId,
                                                                  Status = c.Status,
                                                                  ActiveReprsentativeCount = (from representative in _context.ExchangeActor
                                                                                              where representative.DelegatorId == c.ExchangeActorId
                                                                                              select representative).Count(),
                                                                  IsLicenceExpired = cert.ExpireDateTime.AddMonths(4).Date.CompareTo(DateTime.Now.Date) < 0 ? true : false, // add grace period for renewal in the setting
                                                                  Tin = c.Tin,
                                                                  MemberCategory = c.MemberCategory.DescriptionEng,
                                                                  DelegatorId = c.DelegatorId.GetValueOrDefault(),
                                                                  MemberCategoryId = c.MemberCategoryId.GetValueOrDefault()
                                                              }).AsNoTracking()
                                                                .ToListAsync();

                return customerData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<CustomerRecognitionListDTO> GetCustomerRecogntioinInfo(string lang, Guid ExchangeActorId)
        {
            try
            {

                var data = await (from customer in _context.ExchangeActor
                                  join om in _context.OwnerManager on customer.ExchangeActorId equals om.ExchangeActorId
                                  join customerType in _context.Lookup on customer.CustomerTypeId equals customerType.LookupId
                                  join certifcate in _context.Certificate on customer.ServiceApplicationId equals certifcate.ServiceApplicationId
                                  where customer.ExchangeActorId == ExchangeActorId && om.IsManager == true
                                  select new CustomerRecognitionListDTO
                                  {
                                      ExchangeActorType = (lang == "et") ? customerType.DescriptionAmh : customerType.DescriptionEng,
                                      CertificateIssueDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(certifcate.IssuedDateTime).ToString()) : certifcate.IssuedDateTime.ToString("MMMM dd, yyyy"),
                                      CertificateExpireDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(certifcate.ExpireDateTime).ToString()) : certifcate.ExpireDateTime.ToString("MMMM dd, yyyy"),
                                      OrganizationName = (lang == "et") ? (customer.CustomerTypeId == (int)CustomerType.Representative || customer.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative) ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh) : (customer.OrganizationNameAmh)
                                      : (customer.CustomerTypeId == (int)CustomerType.Representative || customer.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative) ? (om.FirstNameEng + " " + om.FatherNameEng + om.GrandFatherNameEng) : (customer.OrganizationNameEng)
                                  })
                                  .AsNoTracking()
                                  .FirstOrDefaultAsync();

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        // use for user editing 
        public async Task<ExchangeActorGeneralInfoForPRofile> GetExchangeActorGeneralInfo(Guid ExchagneActorId)
        {
            try
            {
                ExchangeActorGeneralInfoForPRofile exchangeActorAddress = await (from exchangeActor in _context.ExchangeActor
                                                         where exchangeActor.ExchangeActorId == ExchagneActorId
                                                         select new ExchangeActorGeneralInfoForPRofile
                                                         {
                                                            OrganizationNameEng=exchangeActor.OrganizationNameEng,
                                                            CustomerTypeId = exchangeActor.CustomerTypeId
                                                            
                                                         })

                                                        .AsNoTracking()
                                                        .FirstOrDefaultAsync();

                return exchangeActorAddress;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<AddressChangeDTO> GetExchangeActorAddress(Guid ExchagneActorId)
        {
            try
            {
                AddressChangeDTO exchangeActorAddress = await (from exchangeActor in _context.ExchangeActor
                                                         where exchangeActor.ExchangeActorId == ExchagneActorId
                                                         select new AddressChangeDTO
                                                         {
                                                             Email = exchangeActor.Email,
                                                             OldEmail = exchangeActor.Email,
                                                             RegionId = exchangeActor.RegionId,
                                                             OldRegionId = exchangeActor.RegionId,
                                                             ZoneId = exchangeActor.ZoneId,
                                                             OldZoneId = exchangeActor.ZoneId,
                                                             WoredaId = exchangeActor.WoredaId,
                                                             OldWoredaId = exchangeActor.WoredaId,
                                                             KebeleId = exchangeActor.KebeleId,
                                                             OldKebeleId = exchangeActor.KebeleId,
                                                             MobileNo = exchangeActor.MobileNo,
                                                             OldMobileNo = exchangeActor.MobileNo,
                                                             Tel = exchangeActor.Tel,
                                                             OldTel = exchangeActor.Tel,
                                                             FaxNo = exchangeActor.FaxNo,
                                                             OldFaxNo = exchangeActor.FaxNo,
                                                             Pobox = exchangeActor.Pobox,
                                                             OldPobox = exchangeActor.Pobox,
                                                             HouseNo = exchangeActor.HouseNo,
                                                             OldHouseNo = exchangeActor.HouseNo,
                                                             OrganizationAreaNameAmh = exchangeActor.OrganizationAreaNameAmh,
                                                             OldOrganizationAreaNameAmh = exchangeActor.OrganizationAreaNameAmh,
                                                             OrganizationAreaNameEng = exchangeActor.OrganizationAreaNameEng,
                                                             OldOrganizationAreaNameEng = exchangeActor.OrganizationAreaNameEng,
                                                             HeadOffice = exchangeActor.HeadOffice,
                                                             OldHeadOffice = exchangeActor.HeadOffice,
                                                             HeadOfficeAmh = exchangeActor.HeadOfficeAmh,
                                                             OldHeadOfficeAmh = exchangeActor.HeadOfficeAmh,
                                                             OldRegion = exchangeActor.Region.DescriptionAmh,
                                                             OldZone = exchangeActor.Zone.DescriptionAmh,
                                                             OldWoreda = exchangeActor.Woreda.DescriptionAmh,
                                                             OldKebele = exchangeActor.Kebele.DescriptionAmh
                                                         })

                                                        .AsNoTracking()
                                                        .FirstOrDefaultAsync();

                return exchangeActorAddress;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<IEnumerable<StaticData6>> GetMemebrReprsentative(Guid serviceApplicationId)
        {
            try
            {
                IEnumerable<StaticData6> memberReprsentative = await _context.MemberRepresentativeType
                        .Where(mr => mr.ServiceApplicationId == serviceApplicationId && mr.IsActive == true && mr.IsDeleted == false)
                        .Select(data => new StaticData6
                        {
                            Id = data.RepresentativeTypeId
                        })
                        .AsNoTracking()
                        .ToListAsync();

                return memberReprsentative;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            List<AuditAmendmentDMV> ListOfAudit = new List<AuditAmendmentDMV>();
            List<AuditAmendmentDMV> Listexch = new List<AuditAmendmentDMV>();


            ListOfAudit = await _context.AuditAmendmentDMV.FromSqlRaw(@"exec dbo.getAmendment {0},{1},{2}", table, exchangeActorId, actionId).OrderByDescending(x => x.ActionDateTime).ToListAsync();

            foreach (var item in ListOfAudit)
            {
                AuditAmendmentDMV g = new AuditAmendmentDMV();

                if (item.ColumnName != null)
                {

                    switch (item.ColumnName)
                    {
                        case "OrganizationNameAmh":
                        case "OrganizationNameEng":
                        case "OrganizationAreaNameAmh":
                        case "OrganizationAreaNameEng":
                        case "MobileNo":
                        case "Tel":
                        case "NetWorthCapital":
                        case "Tin":
                        case "RegionId":
                        case "ZoneId":
                            if (Listexch.Count > 0)
                            {

                                if (Listexch.Count(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime) > 0)
                                {
                                    foreach (var i in Listexch.Where(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime).ToList())
                                    {
                                        i.ColumnName = item.ColumnName;
                                        i.ColumnName = item.ColumnName;
                                        i.NewValue = item.NewValue;
                                        i.ActionDateTime = item.ActionDateTime;
                                        i.OldValue = item.OldValue;
                                        i.UserFullName = item.UserFullName;
                                    }
                                }
                                else
                                {
                                    g.ColumnName = item.ColumnName;
                                    g.NewValue = item.NewValue;
                                    g.ActionDateTime = item.ActionDateTime;
                                    g.OldValue = item.OldValue;
                                    g.UserFullName = item.UserFullName;
                                    Listexch.Add(g);
                                }
                            }
                            else
                            {
                                g.ColumnName = item.ColumnName;
                                g.NewValue = item.NewValue;
                                g.ActionDateTime = item.ActionDateTime;
                                g.OldValue = item.OldValue;
                                g.UserFullName = item.UserFullName;
                                Listexch.Add(g);
                            }
                            break;
                    }
                }
            }
            return Listexch;
        }


        public async Task<bool> DeleteMemberProduct(Guid ExchangeActorId)
        {
            try
            {
                List<MemberProduct> memberProductList = await _context.MemberProduct
                .Where(mp => mp.ExchangeActorId == ExchangeActorId)
                .AsNoTracking()
                .ToListAsync();

                foreach (MemberProduct memberProduct in memberProductList)
                {
                    var deletedMemberPorudct = new MemberProduct { MemberProductId = memberProduct.MemberProductId };
                    _context.Remove(deletedMemberPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<bool> DeleteCustomerBussinessField(Guid ExchangeActorId)
        {
            try
            {
                List<CustomerBussinessFiled> customerBussinessesFiledList = await _context.CustomerBussinessFiled
                .Where(mp => mp.ExchangeActorId == ExchangeActorId)
                .AsNoTracking()
                .ToListAsync();

                foreach (CustomerBussinessFiled bussinessFiled in customerBussinessesFiledList)
                {
                    var deletedMemberPorudct = new CustomerBussinessFiled { CustomerBusinessFildeId = bussinessFiled.CustomerBusinessFildeId };
                    _context.Remove(deletedMemberPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<bool> DeleteClientProduct(int memberClientInformationId)
        {
            try
            {
                List<ClientProduct> ClientProductList = await _context.ClientProduct
                .Where(mp => mp.MemberClientInformationId == memberClientInformationId)
                .AsNoTracking()
                .ToListAsync();

                foreach (ClientProduct clientProduct in ClientProductList)
                {
                    var deletedClientPorudct = new ClientProduct { ClientProductId = clientProduct.ClientProductId };
                    _context.Remove(deletedClientPorudct);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<bool> DeleteMemeberRepresentativeType(Guid ExchangeActorId)
        {

            try
            {
                List<MemberRepresentativeType> memberRepresentativeTypeList = await _context.MemberRepresentativeType
                 .Where(r => r.ExchangeActorId == ExchangeActorId)
                 .AsNoTracking()
                 .ToListAsync();

                // You can use a stub to represent the entity to be deleted and 
                // thereby stop the entity being retrieved from the database:
                foreach (MemberRepresentativeType reType in memberRepresentativeTypeList)
                {
                    var deletedRepresentativType = new MemberRepresentativeType { MemberRepresentativeTypeId = reType.MemberRepresentativeTypeId };
                    _context.Remove(deletedRepresentativType);
                    _context.SaveChanges();

                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

    }
}
