using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class OwnerManagerRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public OwnerManagerRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostCustomerLegalBody(OwnerManagerDTO postedCustomerLegalBody)
        {
            try
            {
                if (postedCustomerLegalBody.ManagerTypeId == null || postedCustomerLegalBody.ManagerTypeId != (int)ManagerType.Owner)
                {
                    var managerId = await this.SaveManager(postedCustomerLegalBody);

                    return managerId;
                }
                else
                {
                    var manager = await _context.OwnerManager.Where(owner => owner.OwnerManagerId == postedCustomerLegalBody.OwnerManagerId).SingleOrDefaultAsync();
                    manager.NationalityId = postedCustomerLegalBody.NationalityId;
                    manager.EducationalLevelId = postedCustomerLegalBody.EducationalLevelId;
                    manager.IdentityCardType = postedCustomerLegalBody.IdentityCardType;
                    manager.IdentityCard = postedCustomerLegalBody.IdentityCard;
                    manager.ManagerTypeId = postedCustomerLegalBody.ManagerTypeId;
                    manager.CreatedUserId = postedCustomerLegalBody.CreatedUserId;
                    manager.Photo = postedCustomerLegalBody.Photo;
                    manager.IsManager = true;

                    await _context.SaveChangesAsync();

                    return manager.OwnerManagerId;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        private async Task<int> SaveManager(OwnerManagerDTO postedCustomerLegalBody)
        {
            try
            {
                CVGeez objGeez = new CVGeez();
                OwnerManager newCustomerLegalBody = _mapper.Map<OwnerManager>(postedCustomerLegalBody);

                newCustomerLegalBody.FirstNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.FirstNameAmh);
                newCustomerLegalBody.FatherNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.FatherNameAmh);
                newCustomerLegalBody.GrandFatherNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.GrandFatherNameAmh);
                newCustomerLegalBody.CreatedUserId = postedCustomerLegalBody.CreatedUserId;
                newCustomerLegalBody.OwnerManagerId = 0;

                if (postedCustomerLegalBody.ManagerTypeId == null)
                {
                    newCustomerLegalBody.IsOwner = true;
                    newCustomerLegalBody.IsManager = false;
                }
                else
                {
                    newCustomerLegalBody.IsOwner = false;
                    newCustomerLegalBody.IsManager = true;
                }

                _context.OwnerManager.Add(newCustomerLegalBody);

                await _context.SaveChangesAsync();

                return newCustomerLegalBody.OwnerManagerId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostAmendCustomerLegalBody(OwnerManagerDTO postedCustomerLegalBody)
        {
            try
            {
                if (postedCustomerLegalBody.ManagerTypeId == null || postedCustomerLegalBody.ManagerTypeId != (int)ManagerType.Owner)
                {
                    var managerId = await this.SaveAmendmentManager(postedCustomerLegalBody);

                    return managerId;
                }
                else
                {
                    var manager = await _context.OwnerManager.Where(owner => owner.OwnerManagerId == postedCustomerLegalBody.OwnerManagerId).SingleOrDefaultAsync();
                    manager.NationalityId = postedCustomerLegalBody.NationalityId;
                    manager.EducationalLevelId = postedCustomerLegalBody.EducationalLevelId;
                    manager.IdentityCardType = postedCustomerLegalBody.IdentityCardType;
                    manager.IdentityCard = postedCustomerLegalBody.IdentityCard;
                    manager.ManagerTypeId = postedCustomerLegalBody.ManagerTypeId;
                    manager.CreatedUserId = postedCustomerLegalBody.CreatedUserId;
                    manager.Photo = postedCustomerLegalBody.Photo;
                    manager.IsManager = true;

                    await _context.SaveChangesAsync();

                    return manager.OwnerManagerId;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        private async Task<int> SaveAmendmentManager(OwnerManagerDTO postedCustomerLegalBody)
        {
            try
            {
                CVGeez objGeez = new CVGeez();
                OwnerManager newCustomerLegalBody = _mapper.Map<OwnerManager>(postedCustomerLegalBody);

                newCustomerLegalBody.FirstNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.FirstNameAmh);
                newCustomerLegalBody.FatherNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.FatherNameAmh);
                newCustomerLegalBody.GrandFatherNameAmhSort = objGeez.GetSortValueU(postedCustomerLegalBody.GrandFatherNameAmh);
                newCustomerLegalBody.CreatedUserId = postedCustomerLegalBody.CreatedUserId;
                newCustomerLegalBody.OwnerManagerId = 0;

                if (postedCustomerLegalBody.ManagerTypeId == null)
                {
                    newCustomerLegalBody.IsOwner = true;
                    newCustomerLegalBody.IsManager = false;
                }
                else
                {
                    newCustomerLegalBody.IsOwner = false;
                    newCustomerLegalBody.IsManager = true;
                }

                _context.OwnerManager.Add(newCustomerLegalBody);
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == newCustomerLegalBody.ExchangeActorId &&
                                                                           x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == newCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == newCustomerLegalBody.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == newCustomerLegalBody.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {

                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                await _context.SaveChangesAsync();

                return newCustomerLegalBody.OwnerManagerId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<OwnerManagerDTO> GetCustomerLegalBodyById(Guid ExchangeActorId)
        {
            try
            {
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == ExchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();

                return _mapper.Map<OwnerManager, OwnerManagerDTO>(customerLegalBody);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateCustomerLegalBody(OwnerManagerDTO updatedCusetomerLegalBody)
        {
            var oldManager = await _context.OwnerManager.Where(manager => manager.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
            manager.IsManager == true).AsNoTracking().SingleOrDefaultAsync();

            if (updatedCusetomerLegalBody.ManagerTypeId == null || (oldManager.ManagerTypeId != (int)ManagerType.Owner &&
                updatedCusetomerLegalBody.ManagerTypeId != (int)ManagerType.Owner))
            {
                var isUpdated = await this.UpdateManager(updatedCusetomerLegalBody);

                return isUpdated;
            }
            else
            {
                if (oldManager.ManagerTypeId == (int)ManagerType.Owner && updatedCusetomerLegalBody.ManagerTypeId == (int)ManagerType.Owner)
                {
                    var newManager = await _context.OwnerManager.Where(manager => manager.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId &&
                    manager.IsOwner == true).AsNoTracking().SingleOrDefaultAsync();

                    // check if old manaer id is equal to new onwer mangaer id

                    if (oldManager.OwnerManagerId == newManager.OwnerManagerId)
                    {
                        newManager.Photo = updatedCusetomerLegalBody.Photo;
                        newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                        newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                        newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                        newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                        newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                        newManager.IsManager = true;

                        _context.Attach(newManager).State = EntityState.Modified;

                    }
                    else
                    {
                        oldManager.Photo = null;
                        oldManager.EducationalLevelId = 0;
                        oldManager.IdentityCardType = 0;
                        oldManager.IdentityCard = null;
                        oldManager.ManagerTypeId = null;
                        oldManager.NationalityId = null;
                        oldManager.IsManager = false;

                        _context.Attach(oldManager).State = EntityState.Modified;


                        newManager.Photo = updatedCusetomerLegalBody.Photo;
                        newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                        newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                        newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                        newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                        newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                        newManager.IsManager = true;

                        _context.Attach(newManager).State = EntityState.Modified;
                    }


                    await _context.SaveChangesAsync();

                    return true;

                }
                else if (updatedCusetomerLegalBody.ManagerTypeId == (int)ManagerType.Owner && oldManager.ManagerTypeId != (int)ManagerType.Owner)
                {
                    _context.Entry(oldManager).State = EntityState.Deleted;


                    var newManager = await _context.OwnerManager.Where(manager => manager.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId && manager.IsOwner == true).SingleOrDefaultAsync();

                    newManager.Photo = updatedCusetomerLegalBody.Photo;
                    newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                    newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                    newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                    newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                    newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                    newManager.UpdatedUserId = updatedCusetomerLegalBody.UpdatedUserId;
                    newManager.IsManager = true;

                    await _context.SaveChangesAsync();

                    return true;
                }
                else
                {

                    oldManager.Photo = null;
                    oldManager.EducationalLevelId = 0;
                    oldManager.IdentityCardType = 0;
                    oldManager.IdentityCard = null;
                    oldManager.ManagerTypeId = null;
                    oldManager.NationalityId = null;
                    oldManager.IsManager = false;

                    await this.SaveManager(updatedCusetomerLegalBody);

                    _context.Attach(oldManager).State = EntityState.Modified;

                    await _context.SaveChangesAsync();

                    return true;

                }
            }

        }
        public async Task<bool> AmendmentCustomerLegalBody(OwnerManagerDTO updatedCusetomerLegalBody)
        {
            var oldManager = await _context.OwnerManager.Where(manager => manager.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
            manager.IsManager == true).AsNoTracking().SingleOrDefaultAsync();

            if (updatedCusetomerLegalBody.ManagerTypeId == null || (oldManager.ManagerTypeId != (int)ManagerType.Owner &&
                updatedCusetomerLegalBody.ManagerTypeId != (int)ManagerType.Owner))
            {
                var isUpdated = await this.AmendmentManager(updatedCusetomerLegalBody);


                return isUpdated;
            }
            else
            {
                if (oldManager.ManagerTypeId == (int)ManagerType.Owner && updatedCusetomerLegalBody.ManagerTypeId == (int)ManagerType.Owner)
                {
                    var newManager = await _context.OwnerManager.Where(manager => manager.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId &&
                    manager.IsOwner == true).AsNoTracking().SingleOrDefaultAsync();

                    // check if old manaer id is equal to new onwer mangaer id

                    if (oldManager.OwnerManagerId == newManager.OwnerManagerId)
                    {
                        newManager.Photo = updatedCusetomerLegalBody.Photo;
                        newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                        newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                        newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                        newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                        newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                        newManager.IsManager = true;

                        _context.Attach(newManager).State = EntityState.Modified;

                    }
                    else
                    {
                        oldManager.Photo = null;
                        oldManager.EducationalLevelId = 0;
                        oldManager.IdentityCardType = 0;
                        oldManager.IdentityCard = null;
                        oldManager.ManagerTypeId = null;
                        oldManager.NationalityId = null;
                        oldManager.IsManager = false;

                        _context.Attach(oldManager).State = EntityState.Modified;


                        newManager.Photo = updatedCusetomerLegalBody.Photo;
                        newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                        newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                        newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                        newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                        newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                        newManager.IsManager = true;

                        _context.Attach(newManager).State = EntityState.Modified;
                    }

                    var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == newManager.ExchangeActorId &&
                                                                         x.ServiceId == 1).FirstOrDefault();
                    var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                             x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                    var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                           ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                    var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                                  ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                    if (serviceAmendments != null)
                    {
                        if (serviceAmendments.ApprovalFinished)
                        {
                            ServiceApplication newServiceApplication = new ServiceApplication
                            {
                                ServiceApplicationId = Guid.NewGuid(),
                                Status = (int)ServiceApplicaitonStatus.Drafted,
                                CurrentStep = oldServiceApp.CurrentStep,
                                NextStep = oldServiceApp.NextStep,
                                ServiceId = (int)ServiceType.Amendment,
                                IsFinished = false,
                                CreatedDateTime = DateTime.Now,
                                UpdatedDateTime = DateTime.Now,
                                OrganizationTypeId = newManager.Customer.OrganizationTypeId.GetValueOrDefault(),
                                CustomerTypeId = newManager.Customer.CustomerTypeId,
                                Ecxcode = newManager.Customer.Ecxcode,
                                CreatedUserId = newManager.Customer.CreatedUserId,
                                UpdatedUserId = newManager.Customer.CreatedUserId
                            };
                            _context.Add(newServiceApplication);
                            ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                            oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                            foreach (var owen in listOfOldOwner)
                            {
                                owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                            }
                            listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                    }
                    else
                    {
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = newManager.Customer.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = newManager.Customer.CustomerTypeId,
                            Ecxcode = newManager.Customer.Ecxcode,
                            CreatedUserId = newManager.Customer.CreatedUserId,
                            UpdatedUserId = newManager.Customer.CreatedUserId
                        };
                        _context.Add(newServiceApplication);
                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    await _context.SaveChangesAsync();

                    return true;

                }
                else if (updatedCusetomerLegalBody.ManagerTypeId == (int)ManagerType.Owner && oldManager.ManagerTypeId != (int)ManagerType.Owner)
                {
                    _context.Entry(oldManager).State = EntityState.Deleted;


                    var newManager = await _context.OwnerManager.Where(manager => manager.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId && manager.IsOwner == true).SingleOrDefaultAsync();

                    newManager.Photo = updatedCusetomerLegalBody.Photo;
                    newManager.EducationalLevelId = updatedCusetomerLegalBody.EducationalLevelId;
                    newManager.IdentityCardType = updatedCusetomerLegalBody.IdentityCardType;
                    newManager.IdentityCard = updatedCusetomerLegalBody.IdentityCard;
                    newManager.ManagerTypeId = updatedCusetomerLegalBody.ManagerTypeId;
                    newManager.NationalityId = updatedCusetomerLegalBody.NationalityId;
                    newManager.UpdatedUserId = updatedCusetomerLegalBody.UpdatedUserId;
                    newManager.IsManager = true;
                    var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == newManager.ExchangeActorId &&
                                                                         x.ServiceId == 1).FirstOrDefault();
                    var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                             x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                    var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                           ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                    var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                                  ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                    if (serviceAmendments != null)
                    {
                        if (serviceAmendments.ApprovalFinished)
                        {
                            ServiceApplication newServiceApplication = new ServiceApplication
                            {
                                ServiceApplicationId = Guid.NewGuid(),
                                Status = (int)ServiceApplicaitonStatus.Drafted,
                                CurrentStep = oldServiceApp.CurrentStep,
                                NextStep = oldServiceApp.NextStep,
                                ServiceId = (int)ServiceType.Amendment,
                                IsFinished = false,
                                CreatedDateTime = DateTime.Now,
                                UpdatedDateTime = DateTime.Now,
                                OrganizationTypeId = newManager.Customer.OrganizationTypeId.GetValueOrDefault(),
                                CustomerTypeId = newManager.Customer.CustomerTypeId,
                                Ecxcode = newManager.Customer.Ecxcode,
                                CreatedUserId = newManager.Customer.CreatedUserId,
                                UpdatedUserId = newManager.Customer.CreatedUserId
                            };
                            _context.Add(newServiceApplication);
                            ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                            oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                            foreach (var owen in listOfOldOwner)
                            {
                                owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                            }
                            listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                    }
                    else
                    {
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = newManager.Customer.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = newManager.Customer.CustomerTypeId,
                            Ecxcode = newManager.Customer.Ecxcode,
                            CreatedUserId = newManager.Customer.CreatedUserId,
                            UpdatedUserId = newManager.Customer.CreatedUserId
                        };
                        _context.Add(newServiceApplication);
                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == newManager.ExchangeActorId).FirstOrDefault();
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }

                    await _context.SaveChangesAsync();

                    return true;
                }
                else
                {

                    oldManager.Photo = null;
                    oldManager.EducationalLevelId = 0;
                    oldManager.IdentityCardType = 0;
                    oldManager.IdentityCard = null;
                    oldManager.ManagerTypeId = null;
                    oldManager.NationalityId = null;
                    oldManager.IsManager = false;

                    await this.SaveManager(updatedCusetomerLegalBody);

                    _context.Attach(oldManager).State = EntityState.Modified;

                    await _context.SaveChangesAsync();

                    return true;

                }
            }

        }
        private async Task<bool> UpdateManager(OwnerManagerDTO updatedCusetomerLegalBody)
        {
            var oldCustomerLegalBody = await _context.OwnerManager.FirstOrDefaultAsync(clb => clb.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId);

            if (oldCustomerLegalBody != null)
            {

                _mapper.Map<OwnerManagerDTO, OwnerManager>(updatedCusetomerLegalBody, oldCustomerLegalBody);

                oldCustomerLegalBody.UpdatedUserId = updatedCusetomerLegalBody.UpdatedUserId;
                oldCustomerLegalBody.IsManager = true;
                oldCustomerLegalBody.IsOwner = false;

                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
        private async Task<bool> AmendmentManager(OwnerManagerDTO updatedCusetomerLegalBody)
        {
            var oldCustomerLegalBody = await _context.OwnerManager.FirstOrDefaultAsync(clb => clb.OwnerManagerId == updatedCusetomerLegalBody.OwnerManagerId);

            if (oldCustomerLegalBody != null)
            {
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == oldCustomerLegalBody.ExchangeActorId &&
                                                                         x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == oldCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == updatedCusetomerLegalBody.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {
                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        oldCustomerLegalBody.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldCustomerLegalBody.ExchangeActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    oldCustomerLegalBody.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                _mapper.Map<OwnerManagerDTO, OwnerManager>(updatedCusetomerLegalBody, oldCustomerLegalBody);
                oldCustomerLegalBody.UpdatedUserId = updatedCusetomerLegalBody.UpdatedUserId;
                oldCustomerLegalBody.IsManager = true;
                oldCustomerLegalBody.IsOwner = false;
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }
        // yemane
        public async Task<IEnumerable<StaticData8>> GetCustomerLegalBodyByCustomerId(Guid customerId, string lang)
        {

            var legalBodyList = await _context.ExchangeActor.Where(x => x.DelegatorId == customerId)
                                              .Select(l => new StaticData8
                                              {
                                                  Id = l.OwnerManager.OwnerManagerId,
                                                  ParentId = l.DelegatorId,
                                                  Description = (lang == "et") ? l.OwnerManager.FirstNameAmh + " " + l.OwnerManager.FatherNameAmh + " " + l.OwnerManager.GrandFatherNameAmh :
                                                                               l.OwnerManager.FirstNameEng + " " + l.OwnerManager.FatherNameEng + " " + l.OwnerManager.GrandFatherNameEng
                                              })
                                              .AsNoTracking()
                                              .ToListAsync();
            if (legalBodyList.Count() == 0)
            {
                return null;
            }

            return legalBodyList;
        }

        public async Task<AddressDTO> GetOwnerManagerAddress(Guid OwnerManagerId, string lang)
        {
            try
            {
                AddressDTO ownerManagerAddress = await (from ownerManger in _context.OwnerManager
                                                        where ownerManger.ExchangeActorId == OwnerManagerId && ownerManger.IsManager == true
                                                        select new AddressDTO
                                                        {
                                                            Email = ownerManger.Email,
                                                            RegionId = ownerManger.RegionId,
                                                            Region = lang == "et" ? ownerManger.Region.DescriptionAmh : ownerManger.Region.DescriptionEng,
                                                            ZoneId = ownerManger.ZoneId,
                                                            Zone = lang == "et" ? ownerManger.Zone.DescriptionAmh : ownerManger.Zone.DescriptionEng,
                                                            WoredaId = ownerManger.WoredaId,
                                                            Woreda = lang == "et" ? ownerManger.Woreda.DescriptionAmh : ownerManger.Woreda.DescriptionEng,
                                                            KebeleId = ownerManger.KebeleId,
                                                            Kebele = lang == "et" ? ownerManger.Kebele.DescriptionAmh : ownerManger.Kebele.DescriptionEng,
                                                            MobileNo = ownerManger.MobileNo,
                                                            Tel = ownerManger.Tel,
                                                            FaxNo = ownerManger.Fax,
                                                            Pobox = ownerManger.Pobox,
                                                            HouseNo = ownerManger.HouseNo
                                                        })
                                                             .AsNoTracking()
                                                             .FirstOrDefaultAsync();
                return ownerManagerAddress;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<IEnumerable<OwnerViewModel>> GetOwnersByOrganizationId(Guid organizationId, string lang)
        {
            List<OwnerViewModel> ownerList = await _context.OwnerManager.Where(s => s.IsActive == true && s.IsDeleted == false && s.ExchangeActorId == organizationId && s.IsOwner == true)
                                                .Select(ow => new OwnerViewModel
                                                {
                                                    OwnerId = ow.OwnerManagerId,
                                                    OwnerFullName = lang == "et" ? (ow.FirstNameAmh + " " + ow.FatherNameAmh + " " + ow.GrandFatherNameAmh) : (ow.FirstNameEng + " " + ow.FatherNameEng + " " + ow.GrandFatherNameEng),
                                                    MobileNo = ow.MobileNo,
                                                    JobPosition = ow.Jobposition,
                                                    IsEceaContact = ow.IsEceaContact,
                                                    IsEcxContact = ow.IsEcxContact,
                                                    IsSettlementTeam = ow.IsSettlementTeam,
                                                    IsTrained = ow.IsTrained

                                                }).AsNoTracking().ToListAsync();
            return ownerList;
        }

        public async Task<OwnerDTO> GetOwnerById(int ownerId)
        {
            OwnerDTO owner = await _context.OwnerManager
                .Where(own => own.IsActive == true && own.IsDeleted == false && own.OwnerManagerId == ownerId)
                .Select(ow => new OwnerDTO
                {
                    OwnerId = ow.OwnerManagerId,
                    FirstNameAmh = ow.FirstNameAmh,
                    FatherNameAmh = ow.FatherNameAmh,
                    GrandFatherNameAmh = ow.GrandFatherNameAmh,
                    FirstNameEng = ow.FirstNameEng,
                    FatherNameEng = ow.FatherNameEng,
                    GrandFatherNameEng = ow.GrandFatherNameEng,
                    Gender = ow.GenderId,
                    RegionId = ow.RegionId,
                    ZoneId = ow.ZoneId,
                    WoredaId = ow.WoredaId,
                    KebeleId = ow.KebeleId,
                    Email = ow.Email,
                    Tel = ow.Tel,
                    Fax = ow.Fax,
                    MobileNo = ow.MobileNo,
                    JobPosition = ow.Jobposition,
                    IsEceaContact = ow.IsEceaContact,
                    IsEcxContact = ow.IsEcxContact,
                    IsSettlementTeam = ow.IsSettlementTeam,
                    IsTrained = ow.IsTrained
                }).AsNoTracking().FirstOrDefaultAsync();

            return owner;
        }

        public async Task<bool> DeleteOwner(int ownerId)
        {
            var owner = await _context.OwnerManager.Where(ow => ow.OwnerManagerId == ownerId).FirstOrDefaultAsync();

            if (owner == null)
            {
                return false;
            }

            _context.Entry(owner).State = EntityState.Deleted;

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateOwner(OwnerManagerDTO owner)
        {
            var oldOwner = await _context.OwnerManager.FirstOrDefaultAsync(ow => ow.OwnerManagerId == owner.OwnerManagerId);

            if (owner.ManagerTypeId == null && oldOwner.IsManager == true)
            {
                owner.Photo = oldOwner.Photo;
                owner.EducationalLevelId = oldOwner.EducationalLevelId;
                owner.IdentityCardType = (int)oldOwner.IdentityCardType;
                owner.IdentityCard = oldOwner.IdentityCard;
                owner.ManagerTypeId = oldOwner.ManagerTypeId;
                owner.NationalityId = (int)oldOwner.NationalityId;
                owner.UpdatedUserId = oldOwner.UpdatedUserId;
                owner.IsManager = oldOwner.IsManager;
            }
            if (oldOwner != null)
            {
                CVGeez objGeez = new CVGeez();
                owner.FirstNameAmhSort = objGeez.GetSortValueU(owner.FirstNameAmh);
                owner.FatherNameAmhSort = objGeez.GetSortValueU(owner.FatherNameAmh);
                owner.GrandFatherNameAmhSort = objGeez.GetSortValueU(owner.GrandFatherNameAmh);
                owner.IsActive = true;
                owner.IsDeleted = false;
                owner.IsOwner = oldOwner.IsOwner;
                owner.CreatedUserId = oldOwner.CreatedUserId;
                owner.CreatedDateTime = oldOwner.CreatedDateTime;
                owner.UpdatedDateTime = DateTime.Now;
                _mapper.Map<OwnerManagerDTO, OwnerManager>(owner, oldOwner);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
        public async Task<bool> AmendmentOwner(OwnerManagerDTO owner)
        {
            var oldOwner = await _context.OwnerManager.FirstOrDefaultAsync(ow => ow.OwnerManagerId == owner.OwnerManagerId);
            var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == oldOwner.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
            var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == oldOwner.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
            if (oldOwner != null)
            {
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == oldOwner.ExchangeActorId &&
                                                                        x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == oldOwner.ExchangeActorId).FirstOrDefault();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {
                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldOwner.ExchangeActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }

                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldOwner.ExchangeActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }

                if (owner.ManagerTypeId == null && oldOwner.IsManager == true)
                {
                    owner.Photo = oldOwner.Photo;
                    owner.EducationalLevelId = oldOwner.EducationalLevelId;
                    owner.IdentityCardType = (int)oldOwner.IdentityCardType;
                    owner.IdentityCard = oldOwner.IdentityCard;
                    owner.ManagerTypeId = oldOwner.ManagerTypeId;
                    owner.NationalityId = (int)oldOwner.NationalityId;
                    owner.UpdatedUserId = oldOwner.UpdatedUserId;
                    owner.IsManager = oldOwner.IsManager;
                }
                CVGeez objGeez = new CVGeez();
                owner.FirstNameAmhSort = objGeez.GetSortValueU(owner.FirstNameAmh);
                owner.FatherNameAmhSort = objGeez.GetSortValueU(owner.FatherNameAmh);
                owner.GrandFatherNameAmhSort = objGeez.GetSortValueU(owner.GrandFatherNameAmh);
                owner.IsActive = true;
                owner.IsDeleted = false;
                owner.IsOwner = oldOwner.IsOwner;
                owner.CreatedUserId = oldOwner.CreatedUserId;
                owner.CreatedDateTime = oldOwner.CreatedDateTime;
                owner.UpdatedDateTime = DateTime.Now;
                _mapper.Map<OwnerManagerDTO, OwnerManager>(owner, oldOwner);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
        // get owner informatin inclugin photo for data chae

        public async Task<OwnerDataChangeVM> GetManagerInforForDataChange(Guid exchangeActorId, string lang)
        {

            var data = await (from om in _context.OwnerManager
                              join gender in _context.Lookup on om.GenderId equals gender.LookupId
                              join managerType in _context.Lookup on om.ManagerTypeId equals managerType.LookupId
                              join nation in _context.Nationality on om.NationalityId equals nation.CountryId
                              join edcaitonLevel in _context.Lookup on om.EducationalLevelId equals edcaitonLevel.LookupId
                              join identityType in _context.Lookup on om.IdentityCardType equals identityType.LookupId
                              where (om.ExchangeActorId == exchangeActorId && om.IsManager == true)
                              select new OwnerDataChangeVM
                              {
                                  FirstNameAmh = om.FirstNameAmh,
                                  FatherNameAmh = om.FatherNameAmh,
                                  GrandFatherNameAmh = om.GrandFatherNameAmh,
                                  FirstNameEng = om.FirstNameEng,
                                  FatherNameEng = om.FatherNameEng,
                                  GrandFatherNameEng = om.GrandFatherNameEng,
                                  Gender = lang == "et" ? gender.DescriptionAmh : gender.DescriptionEng,
                                  GenderId = om.GenderId.GetValueOrDefault(),
                                  JobPosition = om.Jobposition,
                                  ManagerTypeId = om.ManagerTypeId.GetValueOrDefault(),
                                  ManagerType = lang == "et" ? managerType.DescriptionAmh : managerType.DescriptionEng,
                                  Nationality = lang == "et" ? nation.CountryNameAmh : nation.CountryNameEng,
                                  NationalityId = om.NationalityId.GetValueOrDefault(),
                                  EducationalLevel = lang == "et" ? edcaitonLevel.DescriptionAmh : edcaitonLevel.DescriptionEng,
                                  EducationalLevelId = om.EducationalLevelId,
                                  IdentityCardTypeId = om.IdentityCardType.GetValueOrDefault(),
                                  IdentityNumber = om.IdentityCard,
                                  Photo = om.Photo,
                                  IdentityCardType = lang == "et" ? identityType.DescriptionAmh : identityType.DescriptionEng,
                                  ManagerId = om.OwnerManagerId

                              }).AsNoTracking()
                                .FirstOrDefaultAsync();


            return data;
        }

        public async Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            List<AuditAmendmentDMV> ListOfAudit = new List<AuditAmendmentDMV>();
            List<AuditAmendmentDMV> Listexch = new List<AuditAmendmentDMV>();
            var owners = await _context.OwnerManager.Where(x => x.ExchangeActorId == Guid.Parse(exchangeActorId)).ToListAsync();
            foreach (var own in owners)
            {
                ListOfAudit = await _context.AuditAmendmentDMV.FromSqlRaw(@"exec dbo.getAmendment {0},{1},{2}", table, own.OwnerManagerId, actionId).OrderByDescending(x => x.ActionDateTime).ToListAsync();
                // var dist = ListOfAudit.Select(x => new { x.ActionDateTime }).ToList();

                foreach (var item in ListOfAudit)
                {
                    AuditAmendmentDMV g = new AuditAmendmentDMV();

                    if (item.ColumnName != null)
                    {

                        switch (item.ColumnName)
                        {
                            case "OrganizationNameAmh":
                            case "OrganizationNameEng":
                            case "MobileNo":
                            case "Tel":
                                if (Listexch.Count > 0)
                                {

                                    if (Listexch.Count(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime) > 0)
                                    {
                                        foreach (var i in Listexch.Where(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime).ToList())
                                        {
                                            i.ColumnName = item.ColumnName;
                                            i.ColumnName = item.ColumnName;
                                            i.NewValue = item.NewValue;
                                            i.ActionDateTime = item.ActionDateTime;
                                            i.OldValue = item.OldValue;
                                            i.UserFullName = item.UserFullName;
                                        }
                                    }
                                    else
                                    {
                                        g.ColumnName = item.ColumnName;
                                        g.NewValue = item.NewValue;
                                        g.ActionDateTime = item.ActionDateTime;
                                        g.OldValue = item.OldValue;
                                        g.UserFullName = item.UserFullName;
                                        Listexch.Add(g);
                                    }
                                }
                                else
                                {
                                    g.ColumnName = item.ColumnName;
                                    g.NewValue = item.NewValue;
                                    g.ActionDateTime = item.ActionDateTime;
                                    g.OldValue = item.OldValue;
                                    g.UserFullName = item.UserFullName;
                                    Listexch.Add(g);
                                }
                                break;
                        }

                    }



                }
            }




            return Listexch;
        }

        public async Task<bool> UploadOwners(IFormFile excelFile, Guid exchangeActorId)
        {
            var folderName = Path.Combine("wwwroot", "Upload");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileToSave = Path.Combine(pathToSave, excelFile.FileName).ToString();
            int serNo = 0;
            try
            {
                List<OwnerManagerDTO> owners = new List<OwnerManagerDTO>();
                using (var fileStream = new FileStream(fileToSave, FileMode.Create))
                {
                    await excelFile.CopyToAsync(fileStream);
                    fileStream.Flush();
                    //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    using (var package = new ExcelPackage(fileStream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowCount = worksheet.Dimension.Rows;
                        for (int row = 2; row <= rowCount; row++)
                        {
                            serNo++;
                            CVGeez objGeez = new CVGeez();
                            owners.Add(new OwnerManagerDTO
                            {
                                ExchangeActorId = exchangeActorId,
                                FirstNameAmh = worksheet.Cells[row, 1].Value.ToString().Trim(),
                                FatherNameAmh = worksheet.Cells[row, 2].Value.ToString().Trim(),
                                GrandFatherNameAmh = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                FirstNameEng = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                FatherNameEng = worksheet.Cells[row, 5].Value.ToString().Trim(),
                                GrandFatherNameEng = worksheet.Cells[row, 6].Value.ToString().Trim(),
                                GenderId = int.Parse(worksheet.Cells[row, 7].Value.ToString().Trim()),
                                Jobposition = worksheet.Cells[row, 8].Value.ToString().Trim(),

                                Tel = worksheet.Cells[row, 9].Value.ToString().Trim(),
                                MobileNo = worksheet.Cells[row, 10].Value.ToString().Trim(),
                                RegionId = int.Parse(worksheet.Cells[row, 11].Value.ToString().Trim()),

                                FirstNameAmhSort = objGeez.GetSortValueU(worksheet.Cells[row, 1].Value.ToString().Trim()),
                                FatherNameAmhSort = objGeez.GetSortValueU(worksheet.Cells[row, 2].Value.ToString().Trim()),
                                GrandFatherNameAmhSort = objGeez.GetSortValueU(worksheet.Cells[row, 3].Value.ToString().Trim()),
                                //CreatedUserId = postedCustomerLegalBody.CreatedUserId,
                                IsActive = true,
                                IsManager = false,
                                IsOwner = true,
                                CreatedDateTime = DateTime.Now,
                                CreatedUserId = System.Guid.NewGuid(),
                                UpdatedDateTime = DateTime.Now,
                                IsDeleted = false,
                                UpdatedUserId = System.Guid.NewGuid()
                            });
                            await _context.SaveChangesAsync();
                        }
                    }

                }

                return true;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw new Exception($"Error in record number {serNo} " + e.Message);
            }

        }



    }
}
