﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Registration
{
    public class SettlementBankRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public SettlementBankRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostSettlementBanContact(SettlementBankContactDTO settlementBankContact)
        {
            SettlementBankContact newSettlementBankContact = _mapper.Map<SettlementBankContact>(settlementBankContact);

            _context.SettlementBankContact.Add(newSettlementBankContact);

            await _context.SaveChangesAsync();

            return newSettlementBankContact.Id;
        }

        public async Task<IEnumerable<OwnerViewModel>> GetSettlementBankContact(string lang, Guid exchangeActorId)
        {
            List<OwnerViewModel> settlmentBankContactList = await _context.SettlementBankContact.Where(s => s.IsActive == true && s.IsDeleted == false && s.ExchangeActorId == exchangeActorId)
                                                .Select(settlment => new OwnerViewModel
                                                {
                                                    OwnerId = settlment.Id,
                                                    OwnerFullName = lang == "et" ? (settlment.FirstNameAmh + " " + settlment.FatherNameAmh + " " + settlment.GrandFatherNameAmh) :
                                                    (settlment.FirstNameEng + " " + settlment.FatherNameEng + " " + settlment.GrandFatherNameEng),
                                                    MobileNo = settlment.MobileNo,
                                                    JobPosition = settlment.Jobposition,
                                                    IsEceaContact = settlment.IsEceaContact,
                                                    IsEcxContact = settlment.IsEcxContact,
                                                    IsSettlementTeam = settlment.IsSettlementTeam,
                                                    IsTrained = settlment.IsTrained
                                                }).AsNoTracking()
                                                  .ToListAsync();

                                                return settlmentBankContactList;
        }

        public async Task<OwnerDTO> GetSettlementBankContactById(int contactId)
        {
            OwnerDTO owner = await _context.SettlementBankContact
                .Where(contact => contact.IsActive == true && contact.IsDeleted == false && contact.Id == contactId)
                .Select(settlementContact => new OwnerDTO
                {
                    OwnerId = settlementContact.Id,
                    FirstNameAmh = settlementContact.FirstNameAmh,
                    FatherNameAmh = settlementContact.FatherNameAmh,
                    GrandFatherNameAmh = settlementContact.GrandFatherNameAmh,
                    FirstNameEng = settlementContact.FirstNameEng,
                    FatherNameEng = settlementContact.FatherNameEng,
                    GrandFatherNameEng = settlementContact.GrandFatherNameEng,
                    Email = settlementContact.Email,
                    Fax = settlementContact.Fax,
                    MobileNo = settlementContact.MobileNo,
                    JobPosition = settlementContact.Jobposition,
                    IsEceaContact = settlementContact.IsEceaContact,
                    IsEcxContact = settlementContact.IsEcxContact,
                    IsSettlementTeam = settlementContact.IsSettlementTeam,
                    IsTrained = settlementContact.IsTrained
                }).AsNoTracking().FirstOrDefaultAsync();

            return owner;
        }

        public async Task<bool> UpdateSettlementBankContact(SettlementBankContactDTO settlementContact)
        {
            var oldOwner = await _context.SettlementBankContact.FirstOrDefaultAsync(contact => contact.Id == settlementContact.OwnerManagerId);

            if (oldOwner != null)
            {
                // CVGeez objGeez = new CVGeez();
                settlementContact.IsActive = true;
                settlementContact.IsDeleted = false;
                settlementContact.CreatedUserId = oldOwner.CreatedUserId;
                settlementContact.CreatedDateTime = oldOwner.CreatedDateTime;
                settlementContact.UpdatedDateTime = DateTime.Now;
                _mapper.Map<SettlementBankContactDTO, SettlementBankContact>(settlementContact, oldOwner);


                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<bool> DeleteSettlementBankContact(int contactId)
        {
            var settlementContact = await _context.SettlementBankContact.Where(ow => ow.Id == contactId).FirstOrDefaultAsync();

            if (settlementContact == null)
            {
                return false;
            }

            _context.Entry(settlementContact).State = EntityState.Deleted;

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
