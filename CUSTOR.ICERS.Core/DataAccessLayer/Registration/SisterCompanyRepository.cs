﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Registration
{
    public class SisterCompanyRepository
    {

        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public SisterCompanyRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostSisterCompany(SisterCompanyPostDTO postedSitserCompany)
        {

            try
            {
                var sisterCompany = _mapper.Map<SisterCompany>(postedSitserCompany);

                _context.SisterCompany.Add(sisterCompany);

                await _context.SaveChangesAsync();

                return sisterCompany.SisterCompanyId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostAmendmentSisterCompany(SisterCompanyPostDTO postedSitserCompany)
        {

            try
            {
                var sisterCompany = _mapper.Map<SisterCompany>(postedSitserCompany);
                _context.SisterCompany.Add(sisterCompany);
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == sisterCompany.ExchangeActorId &&
                                                                           x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == sisterCompany.ExchangeActorId).FirstOrDefault();
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == sisterCompany.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == sisterCompany.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {

                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == sisterCompany.ExchangeActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == sisterCompany.ExchangeActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                await _context.SaveChangesAsync();

                return sisterCompany.SisterCompanyId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IActionResult> SaveEcxMemberSisterCompnay(EcxMemberSistetCompanyDTO postedSitserCompany)
        {

            ResultResponse response = await ValidateRequest(postedSitserCompany);

            if (response.IsExisted == true)
            {
                return new ObjectResult(response);
            }

            try
            {
                var ecxMemberSisterCompany = _mapper.Map<EcxMemberSisterCompany>(postedSitserCompany);

                _context.EcxMemberSisterCompany.Add(ecxMemberSisterCompany);

                await _context.SaveChangesAsync();

                return new OkObjectResult(ecxMemberSisterCompany.ECXMemberSisterCompanyId);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IActionResult> PostAmendmentEcxMemberSisisterCompany(EcxMemberSistetCompanyDTO postedSitserCompany)
        {

            ResultResponse response = await ValidateRequest(postedSitserCompany);

            if (response.IsExisted == true)
            {
                return new ObjectResult(response);
            }

            try
            {
                var ecxMemberSisterCompany = _mapper.Map<EcxMemberSisterCompany>(postedSitserCompany);

                _context.EcxMemberSisterCompany.Add(ecxMemberSisterCompany);

                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId &&
                                                                          x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId).FirstOrDefault();
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {

                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == ecxMemberSisterCompany.ExchagneActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                await _context.SaveChangesAsync();

                return new OkObjectResult(ecxMemberSisterCompany.ECXMemberSisterCompanyId);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<SisterCompanyGetDTO>> GetAllSisterCompany(Guid exchanegActorId, string lang)
        {
            try
            {
                List<SisterCompanyGetDTO> nonEcxMembersisterCompanyList = new List<SisterCompanyGetDTO>();
                List<SisterCompanyGetDTO> EcxMembersisterCompanyList = new List<SisterCompanyGetDTO>();


                nonEcxMembersisterCompanyList = await _context.SisterCompany
                                                    .Where(s => s.IsActive == true && s.IsDeleted == false && s.ExchangeActorId == exchanegActorId)
                                                    .Select(sc => new SisterCompanyGetDTO
                                                    {
                                                        IsEcxMember = sc.IsEcxMember,
                                                        MobileNo = sc.MobileNo,
                                                        OrganizationName = lang == "et" ? sc.OrganizationNameAmh : sc.OrganizationNameEng,
                                                        SisterCompanyId = sc.SisterCompanyId
                                                    }).AsNoTracking()
                                                      .ToListAsync();


                EcxMembersisterCompanyList = await (from membeSisCompany in _context.EcxMemberSisterCompany
                                                    join exchangeActor in _context.ExchangeActor
                                                    on membeSisCompany.MemberExchangeActorId equals exchangeActor.ExchangeActorId
                                                    where (membeSisCompany.IsActive == true && membeSisCompany.IsDeleted == false && membeSisCompany.ExchagneActorId == exchanegActorId)
                                                    select new SisterCompanyGetDTO
                                                    {
                                                        IsEcxMember = true,
                                                        MobileNo = exchangeActor.MobileNo,
                                                        OrganizationName = lang == "et" ? exchangeActor.OrganizationNameAmh : exchangeActor.OrganizationNameEng,
                                                        SisterCompanyId = membeSisCompany.ECXMemberSisterCompanyId
                                                    }).AsNoTracking()
                                                      .ToListAsync();

                List<SisterCompanyGetDTO> sisterCompanyList = nonEcxMembersisterCompanyList.Union(EcxMembersisterCompanyList).Distinct().ToList();

                return sisterCompanyList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<SisterCompanyPostDTO> GetSisterCompanyById(int compnayId)
        {
            try
            {
                SisterCompany sisterCompany = await _context.SisterCompany
                    .Where(sc => sc.SisterCompanyId == compnayId).FirstOrDefaultAsync();

                return _mapper.Map<SisterCompany, SisterCompanyPostDTO>(sisterCompany);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> DeletSisterCompany(int Id, bool isEcxMember)
        {

            try
            {

                if (isEcxMember)
                {
                    EcxMemberSisterCompany memberSisterCompany = await _context.EcxMemberSisterCompany
                        .Where(sc => sc.ECXMemberSisterCompanyId == Id).FirstOrDefaultAsync();

                    if (memberSisterCompany == null)
                    {
                        return false;
                    }

                    memberSisterCompany.IsDeleted = true;

                    _context.Entry(memberSisterCompany).State = EntityState.Deleted;
                    await _context.SaveChangesAsync();

                }
                else
                {
                    SisterCompany nonEcxMemberSisterCompany = await _context.SisterCompany
                        .Where(sc => sc.SisterCompanyId == Id).FirstOrDefaultAsync();

                    if (nonEcxMemberSisterCompany == null)
                    {
                        return false;
                    }

                    _context.Entry(nonEcxMemberSisterCompany).State = EntityState.Deleted;
                    await _context.SaveChangesAsync();

                }

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> UpdateSisterCompany(SisterCompanyPostDTO updatedSisterCompany)
        {
            try
            {
                SisterCompany oldSisterCompany = await _context.SisterCompany.FirstOrDefaultAsync(sc => sc.SisterCompanyId == updatedSisterCompany.SisterCompanyId);

                if (oldSisterCompany == null)
                {
                    return false;
                }

                _mapper.Map<SisterCompanyPostDTO, SisterCompany>(updatedSisterCompany, oldSisterCompany);

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<bool> AmendmentSisterCompany(SisterCompanyPostDTO updatedSisterCompany)
        {
            try
            {
                SisterCompany oldSisterCompany = await _context.SisterCompany.FirstOrDefaultAsync(sc => sc.SisterCompanyId == updatedSisterCompany.SisterCompanyId);
                updatedSisterCompany.IsActive = oldSisterCompany.IsActive;
                updatedSisterCompany.IsDeleted = oldSisterCompany.IsDeleted;
                updatedSisterCompany.CreatedDateTime = oldSisterCompany.CreatedDateTime;
                updatedSisterCompany.UpdatedDateTime = DateTime.Now;
                if (oldSisterCompany == null)
                {
                    return false;
                }
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == oldSisterCompany.ExchangeActorId &&
                                                                           x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == oldSisterCompany.ExchangeActorId).FirstOrDefault();
                var listOfOldOwner = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == oldSisterCompany.ExchangeActorId &&
                                                                          ow.IsActive == true && ow.IsOwner == true).ToListAsync();
                var listOfOwnerManager = await _context.OwnerManager.Where(ow => ow.ExchangeActorId == oldSisterCompany.ExchangeActorId &&
                                                                              ow.IsActive == true && ow.IsManager == true).FirstOrDefaultAsync();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {

                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldSisterCompany.ExchangeActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldExchangeactor.CustomerTypeId,
                            Ecxcode = oldExchangeactor.Ecxcode,
                            CreatedUserId = oldExchangeactor.CreatedUserId,
                            UpdatedUserId = oldExchangeactor.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId
                        };
                        _context.Add(newServiceApplication);                         
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        foreach (var owen in listOfOldOwner)
                        {
                            owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                        }
                        listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldSisterCompany.ExchangeActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId
                    };
                    _context.Add(newServiceApplication);                    
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    foreach (var owen in listOfOldOwner)
                    {
                        owen.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                    listOfOwnerManager.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                _mapper.Map<SisterCompanyPostDTO, SisterCompany>(updatedSisterCompany, oldSisterCompany);

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<IActionResult> AmendmentEcxMemberSisterCompany(EcxMemberSistetCompanyDTO updatedEcxMemberSisterCompany)
        {

            ResultResponse validationReponse = await ValidateRequest(updatedEcxMemberSisterCompany);

            if (validationReponse.IsExisted == true)
            {
                return new ObjectResult(validationReponse);
            }

            try
            {

                EcxMemberSisterCompany oldSisterCompany = await _context.EcxMemberSisterCompany.FirstOrDefaultAsync(sc => sc.ECXMemberSisterCompanyId == updatedEcxMemberSisterCompany.ECXMemberSisterCompanyId);

                if (oldSisterCompany == null)
                {
                    validationReponse.IsExisted = false;

                    return new ObjectResult(validationReponse);
                }
                var oldServiceApp = _context.ServiceApplication.Where(x => x.ExchangeActorId == oldSisterCompany.ExchagneActorId &&
                                                                           x.ServiceId == 1).FirstOrDefault();
                var serviceAmendments = _context.ServiceApplication.Where(x => x.ServiceId == 13 &&
                                                                         x.ExchangeActorId == oldSisterCompany.ExchagneActorId).FirstOrDefault();
                if (serviceAmendments != null)
                {
                    if (serviceAmendments.ApprovalFinished)
                    {
                        ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldSisterCompany.ExchagneActorId).FirstOrDefault();
                        ServiceApplication newServiceApplication = new ServiceApplication
                        {
                            ServiceApplicationId = Guid.NewGuid(),
                            Status = (int)ServiceApplicaitonStatus.Drafted,
                            CurrentStep = oldServiceApp.CurrentStep,
                            NextStep = oldServiceApp.NextStep,
                            ServiceId = (int)ServiceType.Amendment,
                            IsFinished = false,
                            CreatedDateTime = DateTime.Now,
                            UpdatedDateTime = DateTime.Now,
                            OrganizationTypeId = oldSisterCompany.Customer.OrganizationTypeId.GetValueOrDefault(),
                            CustomerTypeId = oldSisterCompany.Customer.CustomerTypeId,
                            Ecxcode = oldSisterCompany.Customer.Ecxcode,
                            CreatedUserId = oldSisterCompany.Customer.CreatedUserId,
                            UpdatedUserId = oldSisterCompany.Customer.CreatedUserId,
                            ExchangeActorId = oldExchangeactor.ExchangeActorId,
                        };
                        _context.Add(newServiceApplication);
                        
                        oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                    }
                }
                else
                {
                    ExchangeActor oldExchangeactor = _context.ExchangeActor.Where(x => x.ExchangeActorId == oldSisterCompany.ExchagneActorId).FirstOrDefault();
                    ServiceApplication newServiceApplication = new ServiceApplication
                    {
                        ServiceApplicationId = Guid.NewGuid(),
                        Status = (int)ServiceApplicaitonStatus.Drafted,
                        CurrentStep = oldServiceApp.CurrentStep,
                        NextStep = oldServiceApp.NextStep,
                        ServiceId = (int)ServiceType.Amendment,
                        IsFinished = false,
                        CreatedDateTime = DateTime.Now,
                        UpdatedDateTime = DateTime.Now,
                        OrganizationTypeId = oldExchangeactor.OrganizationTypeId.GetValueOrDefault(),
                        CustomerTypeId = oldExchangeactor.CustomerTypeId,
                        Ecxcode = oldExchangeactor.Ecxcode,
                        CreatedUserId = oldExchangeactor.CreatedUserId,
                        UpdatedUserId = oldExchangeactor.CreatedUserId,
                        ExchangeActorId = oldExchangeactor.ExchangeActorId,
                    };
                    _context.Add(newServiceApplication);                   
                    oldExchangeactor.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
                }
                _mapper.Map<EcxMemberSistetCompanyDTO, EcxMemberSisterCompany>(updatedEcxMemberSisterCompany, oldSisterCompany);

                await _context.SaveChangesAsync();

                validationReponse.IsExisted = true;

                return new ObjectResult(validationReponse);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<IActionResult> UpdateEcxMemberSisterCompany(EcxMemberSistetCompanyDTO updatedEcxMemberSisterCompany)
        {

            ResultResponse validationReponse = await ValidateRequest(updatedEcxMemberSisterCompany);

            if (validationReponse.IsExisted == true)
            {
                return new ObjectResult(validationReponse);
            }

            try
            {

                EcxMemberSisterCompany oldSisterCompany = await _context.EcxMemberSisterCompany.FirstOrDefaultAsync(sc => sc.ECXMemberSisterCompanyId == updatedEcxMemberSisterCompany.ECXMemberSisterCompanyId);

                if (oldSisterCompany == null)
                {
                    validationReponse.IsExisted = false;

                    return new ObjectResult(validationReponse);
                }

                _mapper.Map<EcxMemberSistetCompanyDTO, EcxMemberSisterCompany>(updatedEcxMemberSisterCompany, oldSisterCompany);

                await _context.SaveChangesAsync();

                validationReponse.IsExisted = true;

                return new ObjectResult(validationReponse);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        private async Task<ResultResponse> ValidateRequest(EcxMemberSistetCompanyDTO ecxMemberSisterCompanyInfo)
        {
            ResultResponse validationResponse = new ResultResponse();

            try
            {
                var ecxMemberSisterCompany = await _context.EcxMemberSisterCompany.SingleOrDefaultAsync(sc => sc.ExchagneActorId == ecxMemberSisterCompanyInfo.ExchagneActorId
                && sc.MemberExchangeActorId == ecxMemberSisterCompanyInfo.MemberExchangeActorId);

                if (ecxMemberSisterCompany == null)
                {
                    validationResponse.IsExisted = false; // customoer not registred
                }
                else
                {
                    validationResponse.IsExisted = true; // customer registred 
                }

                return validationResponse;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<AuditAmendmentDMV>> GetAuditByCriteria(string table, string exchangeActorId, int actionId)
        {
            List<AuditAmendmentDMV> ListOfAudit = new List<AuditAmendmentDMV>();
            List<AuditAmendmentDMV> Listexch = new List<AuditAmendmentDMV>();
            var sisterCom = await _context.SisterCompany.Where(x => x.ExchangeActorId == Guid.Parse(exchangeActorId)).ToListAsync();
            foreach (var own in sisterCom)
            {
                ListOfAudit = await _context.AuditAmendmentDMV.FromSqlRaw(@"exec dbo.getAmendment {0},{1},{2}", table, own.SisterCompanyId, actionId).OrderByDescending(x => x.ActionDateTime).ToListAsync();
                // var dist = ListOfAudit.Select(x => new { x.ActionDateTime }).ToList();

                foreach (var item in ListOfAudit)
                {
                    AuditAmendmentDMV g = new AuditAmendmentDMV();

                    if (item.ColumnName != null)
                    {

                        switch (item.ColumnName)
                        {
                            case "OrganizationNameAmh":
                            case "OrganizationNameEng":
                            case "MobileNo":
                            case "Tel":
                                if (Listexch.Count > 0)
                                {

                                    if (Listexch.Count(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime) > 0)
                                    {
                                        foreach (var i in Listexch.Where(x => x.ColumnName == item.ColumnName && item.ActionDateTime == x.ActionDateTime).ToList())
                                        {
                                            i.ColumnName = item.ColumnName;
                                            i.ColumnName = item.ColumnName;
                                            i.NewValue = item.NewValue;
                                            i.ActionDateTime = item.ActionDateTime;
                                            i.OldValue = item.OldValue;
                                            i.UserFullName = item.UserFullName;
                                        }
                                    }
                                    else
                                    {
                                        g.ColumnName = item.ColumnName;
                                        g.NewValue = item.NewValue;
                                        g.ActionDateTime = item.ActionDateTime;
                                        g.OldValue = item.OldValue;
                                        g.UserFullName = item.UserFullName;
                                        Listexch.Add(g);
                                    }
                                }
                                else
                                {
                                    g.ColumnName = item.ColumnName;
                                    g.NewValue = item.NewValue;
                                    g.ActionDateTime = item.ActionDateTime;
                                    g.OldValue = item.OldValue;
                                    g.UserFullName = item.UserFullName;
                                    Listexch.Add(g);
                                }
                                break;
                        }

                    }



                }
            }


       

            return Listexch;
        }
    }
}

