﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class RepresentativeChangeRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly ExchangeActorRepository _exchangeActorRepository;
        public RepresentativeChangeRepository(ECEADbContext context, IMapper mapper, ExchangeActorRepository exchangeActorRepository)
        {
            _context = context;
            _mapper = mapper;
            _exchangeActorRepository = exchangeActorRepository;

        }

        public async Task<IActionResult> postRepresentativeChangeData(RepresentativeChangeDTO representativeChangeData)
        {


            ResultResponse statusResponse = await _exchangeActorRepository.GetCustomerSatus(representativeChangeData.NewDelegatorId);

            if (statusResponse.IsCancelled || statusResponse.IsUnderInjunction)
            {
                return new ObjectResult(statusResponse);

            }

            statusResponse = await _exchangeActorRepository.GetExchangeActorReprsentativeCountStatus(representativeChangeData.NewDelegatorId);

            if (statusResponse.HaveMax)
            {
                return new ObjectResult(statusResponse);
            }


            RepresentativeChange repChange = null;

            repChange = _mapper.Map<RepresentativeChange>(representativeChangeData);

            int generalInformation = (int)RepresentativeChangeSteps.General_Information;
            int prerequisite = (int)RepresentativeChangeSteps.Prerequisite;

            ServiceApplication newServiceApplication = new ServiceApplication
            {
                ServiceApplicationId = Guid.NewGuid(),
                ExchangeActorId = representativeChangeData.RepresentativeExchangeActorId,
                Status = (int)ServiceApplicaitonStatus.Drafted,
                CurrentStep = generalInformation,
                NextStep = prerequisite,
                ServiceId = (int)ServiceType.RepresentativeChange,
                IsFinished = false,
                CreatedDateTime = DateTime.Now,
                UpdatedDateTime = DateTime.Now,
                CreatedUserId = representativeChangeData.CreatedUserId,
                UpdatedUserId = representativeChangeData.CreatedUserId,
                CustomerTypeId = representativeChangeData.CustomerTypeId

            };

            repChange.ServiceApplicationId = newServiceApplication.ServiceApplicationId;
            repChange.UpdatedUserId = representativeChangeData.CreatedUserId;


            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    _context.ServiceApplication.Add(newServiceApplication);
                    _context.RepresentativeChange.Add(repChange);

                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return new ObjectResult(_mapper.Map<ServiceApplication, ServiceApplicationDTO>(newServiceApplication));

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        public async Task<IActionResult> updateRepresentativeChange(RepresentativeChangeDTO representativeChangeData)
        {

            ResultResponse statusResponse = await _exchangeActorRepository.GetCustomerSatus(representativeChangeData.NewDelegatorId);

            if (statusResponse.IsCancelled || statusResponse.IsUnderInjunction)
            {
                return new ObjectResult(statusResponse);

            }

            statusResponse = await _exchangeActorRepository.GetExchangeActorReprsentativeCountStatus(representativeChangeData.NewDelegatorId);

            if (statusResponse.HaveMax)
            {
                return new ObjectResult(statusResponse);
            }

            var oldRepresentativeChangeData = await _context.RepresentativeChange.FirstOrDefaultAsync(rep =>
                    rep.ServiceApplicationId == representativeChangeData.ServiceApplicationId);

            var serviceApplication = await _context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == representativeChangeData.ServiceApplicationId);
            

            if (oldRepresentativeChangeData == null || serviceApplication ==  null)
            {
                return new ObjectResult(false);
            }

            using (var transactoin = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    oldRepresentativeChangeData.NewDelegatorId = representativeChangeData.NewDelegatorId;
                    oldRepresentativeChangeData.OldDelegatorId = representativeChangeData.OldDelegatorId;
                    oldRepresentativeChangeData.RepresentativeExchangeActorId = representativeChangeData.RepresentativeExchangeActorId;
                    oldRepresentativeChangeData.UpdatedUserId = representativeChangeData.UpdatedUserId;

                    serviceApplication.UpdatedUserId = representativeChangeData.UpdatedUserId;
                    serviceApplication.CustomerTypeId = representativeChangeData.CustomerTypeId;

                    await _context.SaveChangesAsync();
                    transactoin.Commit();

                    return new ObjectResult(true);

                }
                catch (Exception ex)
                {
                    transactoin.Rollback();
                    throw new Exception(ex.InnerException.ToString());
                }

            }
        }

        public async Task<RepresentativeViewModel> getRepresentativeChangeData(string lang, Guid serviceApplicationId)
        {
            try
            {
                var data = await (from reprsentativeChange in _context.RepresentativeChange
                                  join serviceApplication in _context.ServiceApplication on reprsentativeChange.ServiceApplicationId equals serviceApplication.ServiceApplicationId
                                  join exchangeActor in _context.ExchangeActor on reprsentativeChange.NewDelegatorId equals exchangeActor.ExchangeActorId
                                  join om in _context.OwnerManager on reprsentativeChange.RepresentativeExchangeActorId equals om.ExchangeActorId
                                  where reprsentativeChange.ServiceApplicationId == serviceApplicationId && om.IsManager == true
                                  select new RepresentativeViewModel
                                  {
                                      OldDelegatorId = reprsentativeChange.OldDelegatorId,
                                      NewDelegatorId = reprsentativeChange.NewDelegatorId,
                                      ServiceApplicationId = reprsentativeChange.ServiceApplicationId,
                                      RepresentativeExchangeActorId = reprsentativeChange.RepresentativeExchangeActorId,
                                      RepresentativeName = lang == "et" ? (om.FirstNameAmh + " " + om.FatherNameAmh + " " + om.GrandFatherNameAmh)  : (om.FirstNameEng + " " + om.FatherNameEng + " " + om.GrandFatherNameEng),
                                      NewDelegatorName = lang == "et" ? exchangeActor.OrganizationNameAmh : exchangeActor.OrganizationNameEng,
                                  })
                                  .AsNoTracking()
                                  .FirstOrDefaultAsync();

                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> updateDelgatorCompnay(ServiceApplicationDTO representativeInfo)
        {
            var representativeData = await _context.ExchangeActor.Where(ea => ea.ExchangeActorId == representativeInfo.ExchangeActorId).FirstOrDefaultAsync();
            var serviceApplication = await _context.ServiceApplication.Where(sa => sa.ServiceApplicationId == representativeInfo.ServiceApplicationId).FirstOrDefaultAsync();
            var representativeChangeData = await _context.RepresentativeChange.Where(sa => sa.ServiceApplicationId == representativeInfo.ServiceApplicationId).AsNoTracking().FirstOrDefaultAsync();

            if (representativeData == null || representativeChangeData == null || serviceApplication == null)
            {
                return false;
            }

            // assign the new delegator id
            representativeData.DelegatorId = representativeChangeData.NewDelegatorId;
            representativeData.UpdatedUserId = representativeInfo.UpdatedUserId;


            int serviceApplicationStep = (int)RepresentativeChangeSteps.Certificate;
            // update service applicaiton to final state
            serviceApplication.UpdatedDateTime = DateTime.Now;
            serviceApplication.IsFinished = true;
            serviceApplication.Status = (int)ServiceApplicaitonStatus.Completed;
            serviceApplication.NextStep = serviceApplicationStep;
            serviceApplication.CurrentStep = serviceApplicationStep;
            serviceApplication.ServiceEndDateTime = DateTime.Now;

            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    return true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }
    }
}
