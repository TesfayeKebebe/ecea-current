﻿using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class ReprsentativeTypeRepository
    {
        private readonly ECEADbContext _context;

        public ReprsentativeTypeRepository(ECEADbContext context)
        {
            _context = context;
        }

        public async Task<bool> DeleteMemeberRepresentativeType(Guid ExchangeActorId)
        {

            try
            {
                List<MemberRepresentativeType> memberRepresentativeTypeList = await _context.MemberRepresentativeType
                 .Where(r => r.ExchangeActorId == ExchangeActorId)
                 .AsNoTracking()
                 .ToListAsync();

                // You can use a stub to represent the entity to be deleted and 
                // thereby stop the entity being retrieved from the database:
                foreach (MemberRepresentativeType reType in memberRepresentativeTypeList)
                {
                    var deletedRepresentativType = new MemberRepresentativeType { MemberRepresentativeTypeId = reType.MemberRepresentativeTypeId };
                    _context.Remove(deletedRepresentativType);
                    _context.SaveChanges();

                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }
    }
}
