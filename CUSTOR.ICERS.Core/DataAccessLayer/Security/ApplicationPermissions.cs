using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CUSTOR.Security
{
    public static class ApplicationPermissions
    {
        public static ReadOnlyCollection<ApplicationPermission> AllPermissions;

        public const string UsersPermissionGroupName = "User Permissions";
        public static ApplicationPermission ViewUsers = new ApplicationPermission("View Users", "users.view", UsersPermissionGroupName, "Permission to view other users account details");
        public static ApplicationPermission ManageUsers = new ApplicationPermission("Manage Users", "users.manage", UsersPermissionGroupName, "Permission to create, delete and modify other users account details");

        public const string RolesPermissionGroupName = "Role Permissions";
        public static ApplicationPermission ViewRoles = new ApplicationPermission("View Roles", "roles.view", RolesPermissionGroupName, "Permission to view available roles");
        public static ApplicationPermission ManageRoles = new ApplicationPermission("Manage Roles", "roles.manage", RolesPermissionGroupName, "Permission to create, delete and modify roles");
        public static ApplicationPermission AssignRoles = new ApplicationPermission("Assign Roles", "roles.assign", RolesPermissionGroupName, "Permission to assign roles to users");

        public const string SuperAdministrationPermissionGroupName = "Super Administration Permissions";
        public static ApplicationPermission ManageSiteAdministrators = new ApplicationPermission("Manage Site Administrators", "superadmin.manageAdmins", SuperAdministrationPermissionGroupName, "Permission to manage site administrators");
        public static ApplicationPermission ManageLookups = new ApplicationPermission("Manage Static Data", "superadmin.manageLookups", SuperAdministrationPermissionGroupName, "Permission to manage sites, lookups");
        public static ApplicationPermission ManageSettings = new ApplicationPermission("Manage System Settings", "superadmin.manageSettings", SuperAdministrationPermissionGroupName, "Permission to manage configuration settings");

        // Recognition permission
        public const string RecognitionPermissionGroupName = "Recoginition Administration Permissions";
        public static ApplicationPermission ManageRecognitions = new ApplicationPermission("Manage Recognition", "recognition.manage", RecognitionPermissionGroupName, "Permission to manage recognitions");
        public static ApplicationPermission ManageCancellations = new ApplicationPermission("Cancel Recognition", "Recognition.cancel", RecognitionPermissionGroupName, "Permission to cancel recognition");

        public static ApplicationPermission AddRecognitions = new ApplicationPermission("Add Recognition", "recognition.add", RecognitionPermissionGroupName, "Permission to add recognitions");
        public static ApplicationPermission ViewRecognition = new ApplicationPermission("View Recognition", "recognition.view", RecognitionPermissionGroupName, "Permission to view recognition");
        public static ApplicationPermission UpateRecognition = new ApplicationPermission("Edit Recognition", "recognition.edit", RecognitionPermissionGroupName, "Permission to edit recognition");

        public static ApplicationPermission ManageInjunctions = new ApplicationPermission("Manage Injunctions", "injunnction.manage", RecognitionPermissionGroupName, "Permission to manage injunctions");
        public static ApplicationPermission AddInjunctions = new ApplicationPermission("Add Injunctions", "injunction.add", RecognitionPermissionGroupName, "Permission to add new injunction");
        public static ApplicationPermission LiftInjunctions = new ApplicationPermission("Lift Injunctions", "injunction.lift", RecognitionPermissionGroupName, "Permission to lift injunctions");
        public static ApplicationPermission ApproveInjunction = new ApplicationPermission("Approve Injunction", "injunction.approve", RecognitionPermissionGroupName, "Permission to approve/cancel recognition");
        public static ApplicationPermission ViewInjunction = new ApplicationPermission("View Injunction", "injunction.view", RecognitionPermissionGroupName, "Permission to view injunction List");


        public const string TradeExecutionPermissionGroupName = "Trade Execution  Permissions";
        public static ApplicationPermission ManageFinancialReports = new ApplicationPermission("Manage financial reports", "financial.reports", TradeExecutionPermissionGroupName, "Permission to manage financial reports");
        public static ApplicationPermission ViewTradeExecution = new ApplicationPermission("View Trade Execution", "trade.view", TradeExecutionPermissionGroupName, "Permission to View Trade Execution");
        public static ApplicationPermission ViewMemberClientInformation = new ApplicationPermission("View Member Client Information", "clientinformation.view", TradeExecutionPermissionGroupName, "Permission to View Member Client Information");
        public static ApplicationPermission CreateTradeExecution = new ApplicationPermission("Create Trade Execution", "trade.create", TradeExecutionPermissionGroupName, "Permission to Create Trade Execution");
        public static ApplicationPermission CreateFinancialReport = new ApplicationPermission("Create Financial Report", "financial.create", TradeExecutionPermissionGroupName, "Permission to Create Financial Report");
        public static ApplicationPermission CreateClientInformation = new ApplicationPermission("Create Client Information", "clientinformation.create", TradeExecutionPermissionGroupName, "Permission to Create Member Client Information");
        public static ApplicationPermission CreateMemberViolation = new ApplicationPermission("Create Member Violation", "violation.create", TradeExecutionPermissionGroupName, "Permission to Create Member Violation");
        public static ApplicationPermission ViewMemberViolation = new ApplicationPermission("View Member Violation", "violation.view", TradeExecutionPermissionGroupName, "Permission to View Member Violation");


        //Aminstrative tribunal
        public const string AdministrativeTribunalPermissionGroupName = "Tribunal Director";
        public static ApplicationPermission ManageCases = new ApplicationPermission("Manage Tribunal Cases", "case.manage", AdministrativeTribunalPermissionGroupName, "Permission to manage cases");
        public static ApplicationPermission CasesView = new ApplicationPermission("View Cases", "Case.View", AdministrativeTribunalPermissionGroupName, "Permission to manage cases");
        public static ApplicationPermission FeedBack = new ApplicationPermission("Manage FeedBack", "Case.FeedBack", AdministrativeTribunalPermissionGroupName, "Permission to manage feedback");
       public static ApplicationPermission CaseApproval = new ApplicationPermission("Case Approval", "Case.Approval", AdministrativeTribunalPermissionGroupName, "Permission to approve case");


    public const string LawEnforcementComplaintRegister = "Law Enforcement Complaint Register";
        public static ApplicationPermission AddCaseApplication = new ApplicationPermission("Add Case Application", "CaseApplication.Add", LawEnforcementComplaintRegister, "Permission for register Case application");
        public static ApplicationPermission ApplicationCaseFinding = new ApplicationPermission("Application Case Finding", "CaseApplication.Finding", LawEnforcementComplaintRegister, "Permission for Case Finding");
        public static ApplicationPermission EditCaseApplication = new ApplicationPermission("Edit Case Application", "CaseApplication.Edit", LawEnforcementComplaintRegister, "Permission for edit Case application");
        public static ApplicationPermission AddCollaborators = new ApplicationPermission("Add Collaborators", "Collaborators.Add", LawEnforcementComplaintRegister, "Permission for register Collaborators");
        public static ApplicationPermission EditCollaborators = new ApplicationPermission("Edit Collaborators", "Collaborators.Edit", LawEnforcementComplaintRegister, "Permission for Edit Collaborators");
        public static ApplicationPermission AddWitnesses = new ApplicationPermission("Add Witnesses", "Witnesses.Add", LawEnforcementComplaintRegister, "Permission for register Witnesses");
        public static ApplicationPermission EditWitnesses = new ApplicationPermission("Edit Witnesses", "Witnesses.Edit", LawEnforcementComplaintRegister, "Permission for Edit Witnesses");
        public static ApplicationPermission AddCaseDocument = new ApplicationPermission("Add Case Document", "CaseDocument.Add", LawEnforcementComplaintRegister, "Permission for register Case Document");
        public static ApplicationPermission EditCaseDocument = new ApplicationPermission("Edit Case Document", "CaseDocument.Edit", LawEnforcementComplaintRegister, "Permission for Edit Case Document");
        public static ApplicationPermission AddReporterDecision = new ApplicationPermission("Add Reporter Decision", "ReporterDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Reporter Decision");
        public static ApplicationPermission AddInvestigatorDecision = new ApplicationPermission("Add Investigator Decision", "InvestigatorDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Investigator Decision");
        public static ApplicationPermission AddInvestigationTeamHeadDecision = new ApplicationPermission("Add Investigation Team Head Decision", "InvestigationTeamHeadDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Investigator Decision");
        public static ApplicationPermission AddLawEnforcementDirectorDecision = new ApplicationPermission("Add Law Enforcement Director Decision", "LawEnforcementDirectorDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Director Decision");
        public static ApplicationPermission AddLawEnforcementMainDirectorDecision = new ApplicationPermission("Add Law Enforcement Main Director Decision", "LawEnforcementMainDirectorDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Main Director Decision");
        public static ApplicationPermission AddLawEnforcementCaseExpertDecision = new ApplicationPermission("Add Law Enforcement Case Expert Decision", "LawEnforcementCaseExpertDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Case Expert Decision");
        public static ApplicationPermission AddLawEnforcementCaseExpertTeamHeadDecision = new ApplicationPermission("Add Law Enforcement Case Expert Team Head Decision", "LawEnforcementCaseExpertTeamHeadDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Case Expert Team HeadDecision");
        //public static ApplicationPermission AddLawEnforcementInvestigation = new ApplicationPermission("Add Law Enforcement Case Expert Team Head Decision", "LawEnforcementCaseExpertTeamHeadDecision.Add", LawEnforcementComplaintRegister, "Permission for Law Enforcement Case Expert Team HeadDecision");
        
        public const string LawEnforcementInvestigationOfficer = "Law Enforcement Investigation Officer";
        public static ApplicationPermission AddInvestigatorOfficer = new ApplicationPermission("Add Investigator Officer", "InvestigatorOfficer.Add", LawEnforcementInvestigationOfficer, "Permission for Investigator Officer");
        
        public const string LawEnforcementInvestigationRegister = "Law Enforcement Investigation Register";
        public static ApplicationPermission AddInvestigationFile = new ApplicationPermission("Add Investigation File", "InvestigationFile.Add", LawEnforcementInvestigationRegister, "Permission for register Investigation File");
        public static ApplicationPermission AddInvestigatorReporter = new ApplicationPermission("Add Investigator Reporter", "InvestigatorReporter.Add", LawEnforcementInvestigationRegister, "Permission for Law Enforcement Investigator Reporter");
        public static ApplicationPermission AddInvestigatorRegisterTeamHead = new ApplicationPermission("Add Investigator Register Team Head", "InvestigatorRegisterTeamHead.Add", LawEnforcementInvestigationRegister, "Permission for Law Enforcement Investigator Register Team Head");
        public static ApplicationPermission AddInvestigator
      = new ApplicationPermission("Add Investigator Director", "InvestigatorDirector.Add", LawEnforcementInvestigationRegister, "Permission for Law Enforcement Investigator Director");

        public static ApplicationPermission AddInvestigatorMainDirector = new ApplicationPermission("Add Investigator Main Director", "InvestigatorMainDirector.Add", LawEnforcementInvestigationRegister, "Permission for Investigator Main Director");
        public static ApplicationPermission AddInvestigatorCaseExpert = new ApplicationPermission("Add Investigator Case Expert Decision", "InvestigatorCaseExpert.Add", LawEnforcementInvestigationRegister, "Permission for Investigator Case Expert");
        public static ApplicationPermission AddInvestigatorCaseExpertTeamHead = new ApplicationPermission("Add Investigator Case Expert Team Head Decision", "InvestigatorCaseExpertTeamHead.Add", LawEnforcementInvestigationRegister, "Permission for Investigator Case Expert Team Head");
        public static ApplicationPermission ViewCaseByCaseExpert = new ApplicationPermission("View Case By Prosecution Expert", "Case.View", LawEnforcementInvestigationRegister, "Permission for View Case");
        public static ApplicationPermission PrintCaseByInvestigator = new ApplicationPermission("Print Case By Investigator", "Case.InvestigatorPrint", LawEnforcementInvestigationRegister, "Permission for Print Investigator");

        public const string AdministrativeTribunalOfficer = "Tribunal Registrar";
        public static ApplicationPermission ViewCase = new ApplicationPermission("View Case For Registrar", "Case.View", AdministrativeTribunalOfficer, "Permission for View Case");
        public static ApplicationPermission AddFile = new ApplicationPermission("Add File For Registrar", "File.Add", AdministrativeTribunalOfficer, "Permission for add file");
        public static ApplicationPermission EditFile = new ApplicationPermission("Edit File For Registrar", "File.Edit", AdministrativeTribunalOfficer, "Permission for edit file");
        public static ApplicationPermission AddAppointement = new ApplicationPermission("Add Appointement For Registrar", "Appointement.Add", AdministrativeTribunalOfficer, "Permission for add appointement");
        public static ApplicationPermission EditAppointement = new ApplicationPermission("Edit Appointement For Registrar", "Appointement.Edit", AdministrativeTribunalOfficer, "Permission for edit appointement");
        public static ApplicationPermission AddCourtDecisionByOfficer = new ApplicationPermission("Add Court Decision For Registrar", "CourtDecision.Add", AdministrativeTribunalOfficer, "Permission for add court decision");
        public static ApplicationPermission EditCourtDecisionByOfficer = new ApplicationPermission("Edit Court Decision For Registrar", "CourtDecision.Edit", AdministrativeTribunalOfficer, "Permission for edit court decision");
        public static ApplicationPermission ViewCourtDecisionByOfficer = new ApplicationPermission("View Court Decision For Registrar", "CourtDecision.View", AdministrativeTribunalOfficer, "Permission for view court decision");
       public static ApplicationPermission AddProsecutionAnswer = new ApplicationPermission("Add Prosecution Answer For Registrar", "Prosecution.Add", AdministrativeTribunalOfficer, "Permission for adding prosecution answer");

    public const string Judgmentofficer = "Administrative Judge";
        public static ApplicationPermission AddCourtDecision = new ApplicationPermission("Add Court Decision For Judge", "CourtDecision.Add", Judgmentofficer, "Permission for add court decision");
        public static ApplicationPermission EditCourtDecision = new ApplicationPermission("Edit Court Decision For Judge", "CourtDecision.Edit", Judgmentofficer, "Permission for edit court decision");
        public static ApplicationPermission ViewCourtDecision = new ApplicationPermission("View Court Decision For Judge", "CourtDecision.View", Judgmentofficer, "Permission for view court decision");
        public static ApplicationPermission ViewInvestigation = new ApplicationPermission("View Investigation For Judge", "Investigation.View", Judgmentofficer, "Permission for view investigation");
      public static ApplicationPermission CasesViewForJudge = new ApplicationPermission("View Cases For Judge", "Case.View", Judgmentofficer, "Permission to manage cases");
    public static ApplicationPermission FeedBackForJudge = new ApplicationPermission("Manage FeedBack For Judge", "Case.FeedBack", Judgmentofficer, "Permission to manage feedback");

    //Law enforcement
    public const string LawEnforcementPermissionGroupName = "Law Enformcement Permission";
        public static ApplicationPermission ManageLawEnforcementCase = new ApplicationPermission("Manage Law Enforcement Case", "case.manage", LawEnforcementPermissionGroupName, "Perminssion to manage case application");
        public static ApplicationPermission AddCourtDecisionLawEnforcement = new ApplicationPermission("Add Court Decision", "CourtDecision.Add", LawEnforcementPermissionGroupName, "Permission for add court decision");
        public static ApplicationPermission EditCourtDecisionLawEnforcement = new ApplicationPermission("Edit Court Decision", "CourtDecision.Edit", LawEnforcementPermissionGroupName, "Permission for edit court decision");
        public static ApplicationPermission AddCaseApplicationLawEnforcement = new ApplicationPermission("Add Case Application", "CaseApplication.Add", LawEnforcementPermissionGroupName, "Permission for register Case application");


        // For Finance
        public const string FinancePermissionGroupName = "Finance Permission";
        public static ApplicationPermission ViewAuthorizedPayment = new ApplicationPermission("View authorized payment order", "finance.view", FinancePermissionGroupName, "Permission to view authorized payment order");
        public static ApplicationPermission RecivePayment = new ApplicationPermission("Recive payment", "finance.recive", FinancePermissionGroupName, "Permission to recive payment");
        public static ApplicationPermission CancelAuthorizedPaymentOrder = new ApplicationPermission("Cancel authorized payment order", "finance.cancel", FinancePermissionGroupName, "Permission to cancel authorized payment order");

        //for oversight followup report
        public const string OversightFollowUpPermissionGroupName = "Oversight Report permissions";
        public static ApplicationPermission CreateOversightReport = new ApplicationPermission("Add oversight followup report", "oversightreport.add", OversightFollowUpPermissionGroupName, "Permission to add new oversight followup report");
        public static ApplicationPermission viewOversightReport = new ApplicationPermission("view oversight followup report", "oversightreport.view", OversightFollowUpPermissionGroupName, "Permission to view oversight followup report");
        public static ApplicationPermission approveOversightReport = new ApplicationPermission("Approve oversight followup report", "oversightreport.approve", OversightFollowUpPermissionGroupName, "Permission to approve oversight followup report");

        //for Bank oversight followup report
        public const string BankOversightFollowUpPermissionGroupName = "Bank Oversight Report permissions";
        public static ApplicationPermission CreateBankOversightReport = new ApplicationPermission("Add bank oversight followup report", "bankoversightreport.add", BankOversightFollowUpPermissionGroupName, "Permission to add new oversight followup report");
        public static ApplicationPermission viewOBankversightReport = new ApplicationPermission("view bank oversight followup report", "bankoversightreport.view", BankOversightFollowUpPermissionGroupName, "Permission to view oversight followup report");
        public static ApplicationPermission approveBankOversightReport = new ApplicationPermission("Approve Bank oversight followup report", "bankoversightreport.approve", BankOversightFollowUpPermissionGroupName, "Permission to approve oversight followup report");


        //for Bank oversight followup report
        public const string FinancialOversightFollowUpPermissionGroupName = "Financial Oversight Report permissions";
        public static ApplicationPermission CreateFinancialOversightReport = new ApplicationPermission("Add Financial oversight followup report", "financialoversightreport.add", FinancialOversightFollowUpPermissionGroupName, "Permission to add new oversight followup report");
        public static ApplicationPermission viewFinancialOversightReport = new ApplicationPermission("view Financial oversight followup report", "financialoversightreport.view", FinancialOversightFollowUpPermissionGroupName, "Permission to view oversight followup report");
        public static ApplicationPermission approveFinancialOversightReport = new ApplicationPermission("Approve Financial oversight followup report", "financialoversightreport.approve", FinancialOversightFollowUpPermissionGroupName, "Permission to approve oversight followup report");





        // For approval
        public const string ApprovalPermissionGroup = "Approval Permission";
        public const string WorkFlowPermissionGroupName = "Work Flow Permissions";
        public static ApplicationPermission Prepare = new ApplicationPermission("Prepare", "0", WorkFlowPermissionGroupName, "Permission to prepare");
        public static ApplicationPermission Checker = new ApplicationPermission("Checker", "1", WorkFlowPermissionGroupName, "Permission to checker");
        public static ApplicationPermission ApproverOne = new ApplicationPermission("Approver One", "2", WorkFlowPermissionGroupName, "Permission to Approver one");
        public static ApplicationPermission ApproverTwo = new ApplicationPermission("Approver Two", "3", WorkFlowPermissionGroupName, "Permission to Approver Two");
        public static ApplicationPermission Reporter = new ApplicationPermission("Reporter", "7", WorkFlowPermissionGroupName, "Permission to Reporter");

        //Setting Permission
        public const string SettingPermissionGroupName = "Setting Administration Permissions";
        public static ApplicationPermission ViewSetting = new ApplicationPermission("View Setting", "setting.view", SettingPermissionGroupName, "Permission to view settings");
        public static ApplicationPermission ManageSetting = new ApplicationPermission("Manage Setting", "setting.manage", SettingPermissionGroupName, "Permission to edit setting");
        //Off-Site Monitoring
        public const string OffSiteMonitoring = "Off-Site Monitoring";
        public static ApplicationPermission MemberClientInformation = new ApplicationPermission("Manage Member Client Information", "clientinformation.view", OffSiteMonitoring, "Permission to manage member client information");
        public static ApplicationPermission MemberFinance = new ApplicationPermission("Manage Member Finance", "finance.view", OffSiteMonitoring, "Permission to manage Members Finance");
        public static ApplicationPermission MemberTradeExecution = new ApplicationPermission("Manage Member Trade Execution", "tradeexecution.view", OffSiteMonitoring, "Permission to manage Members Trade execution");
        public static ApplicationPermission OffSiteMoniteringTeamLeader = new ApplicationPermission("Manage Off-Site Monitoring Team Leader", "offsiteteamleader.view", OffSiteMonitoring, "Permission to manage off-site monitoring");
        public static ApplicationPermission OffSiteMoniteringDeptDirector = new ApplicationPermission("Manage Off-Site Monitoring Department Director", "offsitedeptdirector.view", OffSiteMonitoring, "Permission to manage off-site monitoring");
        public static ApplicationPermission OffSiteMoniteringView = new ApplicationPermission("Manage Off-Site Monitoring View", "offsiteview.view", OffSiteMonitoring, "Permission to manage off-site monitoring view");
        //start settlement Bank
        public const string SettlementBank = "Settlement Bank";
        public static ApplicationPermission SettlementBankOfficer = new ApplicationPermission("Settlement Bank Officer", "bankofficer.view", SettlementBank, "Permission to manage settlement bank officer");
        public static ApplicationPermission SettlementBankTeamLeader = new ApplicationPermission("Settlement Bank Team Leader", "bankteamleader.view", SettlementBank, "Permission to manage settlement bank team leader");
        public static ApplicationPermission SettlementBankDepartmentHead = new ApplicationPermission("Settlement Bank Department Head", "bankdepthead.view", SettlementBank, "Permission to manage settlement bank department head");
        //end settlement bank
        //start surveillance
        public const string Surveillance = "Surveillance";
        public static ApplicationPermission ManageSurveillance = new ApplicationPermission("Manage Surveillance", "Surveillance.Manage", Surveillance, "Permission to manage surveillance");
        public static ApplicationPermission ViewSurveillanceViolation = new ApplicationPermission("View Surveillance Violation", "surveillance.view", Surveillance, "Permission to view surveillance violation");

        //end surveillance
        //start ECX Report
        public const string ECXReport = "ECX Report";
        public static ApplicationPermission SettlementGuarantee = new ApplicationPermission("Manage Settlement Guarantee Fund", "GuaranteeFund.Manage", ECXReport, "Permission to manage Guarantee Fund");
        public static ApplicationPermission ServiceLevelAgreement = new ApplicationPermission("Manage Service Level Agreement", "SLA.Manage", ECXReport, "Permission to Service Level Agreement");
        //end surveillance

        static ApplicationPermissions()
        {

            List<ApplicationPermission> allPermissions = new List<ApplicationPermission>()

            {
           
                
                ViewUsers,
                ManageUsers,
                ViewRoles,
                ManageRoles,
                AssignRoles,
                ManageSiteAdministrators,
                ManageLookups,
                ManageSettings,
                ManageRecognitions,
                ManageInjunctions,
                ManageCancellations,
                CreateOversightReport,
                viewOversightReport,
                ViewSetting,
                ManageSetting,
                approveOversightReport,
                CreateBankOversightReport,
                CreateFinancialOversightReport,
                approveBankOversightReport,
                approveFinancialOversightReport,
                //ManageTradeExecutions,
                //ManageClients,
                //ManageReports,
                ManageCases,
                //ManagePetition,
                //ManageCourt,
                AddRecognitions,
                AddInjunctions,
                ViewRecognition,
                UpateRecognition,
                LiftInjunctions,
                ApproveInjunction,
                ViewInjunction,
                ManageFinancialReports,
                ViewTradeExecution,
                ViewMemberClientInformation,
                ViewMemberViolation,
                CreateFinancialReport,
                CreateMemberViolation,
                CreateTradeExecution,
                Prepare,
                Checker,
                ApproverOne,
                ApproverTwo,
                //ManageFinancialReports,
                ViewTradeExecution,
                //For finance
                ViewAuthorizedPayment,
                RecivePayment,
                CancelAuthorizedPaymentOrder,
                ManageCases,
                ManageLawEnforcementCase,
                ViewCase,
                CasesView,
                AddFile,
                EditFile,
                AddAppointement,
                EditAppointement,
                AddCourtDecision,
                EditCourtDecision,
                AddCourtDecisionByOfficer,
                EditCourtDecisionByOfficer,
                ViewCourtDecision,
                ViewCourtDecisionByOfficer,
                AddCourtDecisionLawEnforcement,
                EditCourtDecisionLawEnforcement,
                AddCaseApplication,
                ApplicationCaseFinding,
                EditCaseApplication,
                AddCollaborators,
                AddWitnesses,
                EditCollaborators,
                EditWitnesses,
                AddCaseDocument,
                EditCaseDocument,
                AddInvestigatorOfficer,
                AddReporterDecision,
                AddInvestigatorDecision,
                AddInvestigationTeamHeadDecision,
                AddLawEnforcementDirectorDecision,
                AddInvestigationFile,
                AddInvestigatorReporter,
                AddInvestigatorRegisterTeamHead,
                PrintCaseByInvestigator,
                AddInvestigatorMainDirector,
                AddInvestigatorCaseExpert,
                AddInvestigatorCaseExpertTeamHead,
                AddCaseApplicationLawEnforcement,
                ViewInvestigation,
                FeedBack,
                Reporter,
                MemberClientInformation,
                MemberFinance,
                MemberTradeExecution,
                OffSiteMoniteringTeamLeader,
                OffSiteMoniteringDeptDirector,
                OffSiteMoniteringView,
                SettlementBankOfficer,
                SettlementBankTeamLeader,
                SettlementBankDepartmentHead ,
                ManageSurveillance,
                ViewSurveillanceViolation,
                CaseApproval,
               CasesViewForJudge,
               FeedBackForJudge ,
               ViewCaseByCaseExpert,
               AddProsecutionAnswer,
               SettlementGuarantee,
               ServiceLevelAgreement
  };

            AllPermissions = allPermissions.AsReadOnly();
        }

        public static ApplicationPermission GetPermissionByName(string permissionName)
        {
            return AllPermissions.Where(p => p.Name == permissionName).FirstOrDefault();
        }

        public static ApplicationPermission GetPermissionByValue(string permissionValue)
        {
            return AllPermissions.Where(p => p.Value == permissionValue).FirstOrDefault();
        }

        public static string[] GetAllPermissionValues()
        {
            return AllPermissions.Select(p => p.Value).ToArray();
        }

        public static string[] GetAdministrativePermissionValues()
        {
            return new string[] { ManageUsers, ManageRoles, AssignRoles };
        }
        //public static string[] GetRecognitionUserValues()
        //{
        //    return new string[] { ManageRecognitions, AddRecognitions,  AddInjunctions,LiftInjunctions,ManageCancellations };
        //}
        //public static string[] GetAdministrativeTribunalUserValues()
        //{
        //    return new string[] { ManageCases, ManageCourt, ManagePetition};
        //}
        //public static string[] GetTraderUserValues()
        //{
        //    return new string[] { ManageClients, ManageTradeExecutions };
        //}
    }

    public class ApplicationPermission
    {
        public ApplicationPermission()
        { }

        public ApplicationPermission(string name, string value, string groupName, string description = null)
        {
            Name = name;
            Value = value;
            GroupName = groupName;
            Description = description;
        }

        public string Name { get; set; }
        public string Value { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Value;
        }

        public static implicit operator string(ApplicationPermission permission)
        {
            return permission.Value;
        }
    }
}
