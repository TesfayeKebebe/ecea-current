using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Service
{

    public class ServiceApplicationRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public ServiceApplicationRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }

        #region save service application status
        public async Task<ServiceApplicationDTO> PostServiceApplicationStatus(ServiceApplicationDTO postedServiceApplicationStatus)
        {
            ServiceApplication serviceApplication;

            try
            {
                serviceApplication = mapper.Map<ServiceApplication>(postedServiceApplicationStatus);

                context.ServiceApplication.Add(serviceApplication);

                await context.SaveChangesAsync();

                return mapper.Map<ServiceApplicationDTO>(serviceApplication);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public async Task<List<ServiceApplicationDTO>> GetServiceApplicationById(int ExchangeActorId, Guid serviceApplicationId)
        {
            try
            {
                List<ServiceApplicationDTO> serviceApp = null;


                serviceApp = await (from serviceApplication in context.ServiceApplication
                                    join customer in context.ExchangeActor on serviceApplication.ExchangeActorId equals customer.ExchangeActorId
                                    where serviceApplication.ServiceApplicationId == serviceApplicationId
                                    select new ServiceApplicationDTO
                                    {
                                        ServiceApplicationId = serviceApplication.ServiceApplicationId,
                                        ExchangeActorId = serviceApplication.ExchangeActorId,
                                        Status = serviceApplication.Status,
                                        CurrentStep = serviceApplication.CurrentStep,
                                        NextStep = serviceApplication.NextStep,
                                        ServiceId = serviceApplication.ServiceId,
                                        CustomerTypeId = customer.CustomerTypeId,
                                        IsPaid = serviceApplication.IsPaid,
                                        IsPaymentAuthorized = serviceApplication.IsPaymentAuthorized

                                    })
                       .ToListAsync();

                return serviceApp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ServiceApplicationDTO> UpdateServiceApplicaitonState(ServiceApplicationDTO updatedServiceApplication)
        {
            try
            {
                var oldServiceApplicationState = await context.ServiceApplication.FirstOrDefaultAsync(s =>
                    s.IsDeleted == false && s.ServiceApplicationId == updatedServiceApplication.ServiceApplicationId);

                if (oldServiceApplicationState == null)
                {
                    return null;
                }


                if (updatedServiceApplication.IsFinished == true)
                {
                    oldServiceApplicationState.ServiceEndDateTime = DateTime.Now;
                }

                oldServiceApplicationState.NextStep = updatedServiceApplication.NextStep;
                oldServiceApplicationState.CurrentStep = updatedServiceApplication.CurrentStep;
                oldServiceApplicationState.Status = updatedServiceApplication.Status;
                oldServiceApplicationState.UpdatedDateTime = DateTime.Now;
                oldServiceApplicationState.UpdatedUserId = updatedServiceApplication.UpdatedUserId;



                await context.SaveChangesAsync();

                return mapper.Map<ServiceApplicationDTO>(oldServiceApplicationState);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<ServiceApplicationDTO>> GetDraftedApplication()
        {

            List<ServiceApplication> draftedApplication = await context.ServiceApplication
                .Where(app => app.Status == (int)ServiceApplicaitonStatus.Drafted || app.Status == (int)ServiceApplicaitonStatus.WaitingForApproval).AsNoTracking().ToListAsync();

            return mapper.Map<IEnumerable<ServiceApplication>, IEnumerable<ServiceApplicationDTO>>(draftedApplication);
        }

        public async Task<PagedResult<ServiceApplicationSearchDTO>> SearchServiceApplication(ServiceApplicationQueryParameters queryParameters)
        {
            try
            {
                int totalSearchResult = 0;

                List<ServiceApplicationSearchDTO> serviceApplicationList = null;

                if (queryParameters.Lang == "et")
                {
                    serviceApplicationList = await SearchServiceApplicationInAmharic(queryParameters);
                }
                else
                {
                    serviceApplicationList = await SearchServiceApplicationInEnglish(queryParameters);
                }


                totalSearchResult = serviceApplicationList.Count();

                var pagedResult = serviceApplicationList.AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageNumber);

                return new PagedResult<ServiceApplicationSearchDTO>()
                {
                    Items = mapper.Map<List<ServiceApplicationSearchDTO>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<ServiceApplicationDTO> GetServiceApplication(Guid serviceApplicationId)
        {
            try
            {
                ServiceApplicationDTO serviceApp = null;

                serviceApp = await (from serviceApplication in context.ServiceApplication
                                    join customer in context.ExchangeActor on serviceApplication.ExchangeActorId equals customer.ExchangeActorId
                                    where serviceApplication.ServiceApplicationId == serviceApplicationId
                                    select new ServiceApplicationDTO
                                    {
                                        ServiceApplicationId = serviceApplication.ServiceApplicationId,
                                        ExchangeActorId = serviceApplication.ExchangeActorId,
                                        Status = serviceApplication.Status,
                                        CurrentStep = serviceApplication.CurrentStep,
                                        NextStep = serviceApplication.NextStep,
                                        ServiceId = serviceApplication.ServiceId,
                                        CustomerTypeId = customer.CustomerTypeId,
                                        IsPaid = serviceApplication.IsPaid,
                                        IsPaymentAuthorized = serviceApplication.IsPaymentAuthorized,
                                        MemberCategoryId = customer.MemberCategoryId.GetValueOrDefault(),
                                        ApprovalStatus = serviceApplication.ApprovalStatus,
                                        ApprovalFinished = serviceApplication.ApprovalFinished,
                                        OrganizationTypeId = serviceApplication.OrganizationTypeId
                                    })
                       .FirstOrDefaultAsync();

                return serviceApp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdaterServiceApplication(Guid serviceApplicationId)
        {
            try
            {
                ServiceApplication toUpdateServiceApplication = await context.ServiceApplication.FirstOrDefaultAsync(sa => sa.ServiceApplicationId == serviceApplicationId
                && sa.IsActive == true && sa.IsDeleted == false);

                if (toUpdateServiceApplication != null)
                {
                    toUpdateServiceApplication.IsPaid = false;
                    toUpdateServiceApplication.IsPaymentAuthorized = true;
                    toUpdateServiceApplication.AuthorizedDateTime = DateTime.Now;

                    await context.SaveChangesAsync();

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<List<ServiceApplicationSearchDTO>> SearchServiceApplicationInAmharic(ServiceApplicationQueryParameters queryParameters)
        {
            try
            {
                CVGeez objGeez = new CVGeez();
                List<ServiceApplicationSearchDTO> serviceApplicationList = await context.ServiceApplication.Where(sa => sa.IsDeleted == false)
                    .Include(sa => sa.ExchangeActor)
                    .Include(sa => sa.Service)
                    .Where(sa =>
                    (string.IsNullOrEmpty(queryParameters.From) || sa.CreatedDateTime >= DateTime.Parse(queryParameters.From)) &&
                    (string.IsNullOrEmpty(queryParameters.To) || sa.CreatedDateTime <= DateTime.Parse(queryParameters.To).AddDays(1)) &&
                    (queryParameters.ServiceStatus == null || sa.Status == queryParameters.ServiceStatus) &&
                    (sa.ExchangeActor.OrganizationNameAmhSort.Contains(objGeez.GetSortValueU(queryParameters.OrganizationName == null ? string.Empty : queryParameters.OrganizationName.Trim()))) &&
                    (string.IsNullOrEmpty(queryParameters.Ecxcode) || sa.ExchangeActor.Ecxcode.Contains(queryParameters.Ecxcode.Trim().ToLower())) &&
                    (string.IsNullOrEmpty(queryParameters.Tin) || sa.ExchangeActor.Tin.Contains(queryParameters.Tin.Trim())) &&
                    (queryParameters.ServiceType == null || sa.ServiceId == queryParameters.ServiceType) && 
                    (queryParameters.CustomerType == null || sa.ExchangeActor.CustomerTypeId == queryParameters.CustomerType))
                    .OrderBy(sa => sa.Status)
                    .Select(data => new ServiceApplicationSearchDTO
                    {
                        FullName = (data.ExchangeActor.OrganizationNameAmh),
                        ServiceType = data.Service.DescriptionAmh,
                        CurrentStep = data.CurrentStep,
                        NextStep = data.NextStep,
                        ExchangeActorId = data.ExchangeActorId,
                        ServiceApplicationId = data.ServiceApplicationId,
                        StartDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.CreatedDateTime).ToString()),
                        Status = data.Status,
                        ServiceId = data.ServiceId,
                        IsPaymentAuthorized = data.IsPaymentAuthorized.GetValueOrDefault(false) ? data.IsPaymentAuthorized.Value : false,
                        IsPaid = data.IsPaid.GetValueOrDefault(false) ? data.IsPaid.Value : false,
                        // ManagerName = data.ExchangeActor.OwnerManager.IsManager == true ? (data.ExchangeActor.OwnerManager.FirstNameAmh + " " + data.ExchangeActor.OwnerManager.FatherNameAmh + " " + data.ExchangeActor.OwnerManager.GrandFatherNameAmh): "",
                        ManagerName = (from ownerManager in context.OwnerManager
                                       where ownerManager.IsManager == true && ownerManager.ExchangeActorId == data.ExchangeActorId
                                       select new ManaerName
                                       {
                                           FullName = ownerManager.FirstNameAmh + ownerManager.FatherNameAmh + ownerManager.GrandFatherNameAmh
                                       }).SingleOrDefault(),
                        EndDate = (data.ServiceEndDateTime.HasValue ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(data.ServiceEndDateTime.Value).ToString()) : ""),
                        Ecxcode = data.ExchangeActor.Ecxcode
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return serviceApplicationList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        private async Task<List<ServiceApplicationSearchDTO>> SearchServiceApplicationInEnglish(ServiceApplicationQueryParameters queryParameters)
        {
            CVGeez objGeez = new CVGeez();


            try
            {
                List<ServiceApplicationSearchDTO> serviceApplicationList = new List<ServiceApplicationSearchDTO>();

                serviceApplicationList = await context.ServiceApplication.Where(sa => sa.IsDeleted == false)
                    .Include(sa => sa.ExchangeActor)
                        .ThenInclude(c => c.OwnerManager)
                    .Include(sa => sa.Service)
                    .Where(sa =>
                    (string.IsNullOrEmpty(queryParameters.From) || sa.CreatedDateTime >= DateTime.Parse(queryParameters.From)) &&
                    (string.IsNullOrEmpty(queryParameters.To) || sa.CreatedDateTime <= DateTime.Parse(queryParameters.To).AddDays(1)) &&
                    (queryParameters.ServiceStatus == null || sa.Status == queryParameters.ServiceStatus) &&
                    (string.IsNullOrEmpty(queryParameters.OrganizationName) || sa.ExchangeActor.OrganizationNameEng.Contains(queryParameters.OrganizationName)) &&
                    (string.IsNullOrEmpty(queryParameters.Ecxcode) || sa.ExchangeActor.Ecxcode.Contains(queryParameters.Ecxcode.Trim().ToLower())) &&
                    (string.IsNullOrEmpty(queryParameters.Tin) || sa.ExchangeActor.Tin.Contains(queryParameters.Tin.Trim())) &&
                    (queryParameters.ServiceType == null || sa.ServiceId == queryParameters.ServiceType) &&
                    (queryParameters.CustomerType == null || sa.ExchangeActor.CustomerTypeId == queryParameters.CustomerType))
                    .OrderBy(sa => sa.Status).ThenByDescending(sa => sa.CreatedDateTime)
                    .Select(data => new ServiceApplicationSearchDTO
                    {
                        FullName = queryParameters.Lang == "et" ? (data.ExchangeActor.OrganizationNameAmh) : (data.ExchangeActor.OrganizationNameEng),
                        ServiceType = queryParameters.Lang == "et" ? data.Service.DescriptionAmh : data.Service.DescriptionEng,
                        CurrentStep = data.CurrentStep,
                        NextStep = data.NextStep,
                        ExchangeActorId = data.ExchangeActorId,
                        ServiceApplicationId = data.ServiceApplicationId,
                        StartDate = data.CreatedDateTime.ToString("MMMM dd, yyyy"),
                        Status = data.Status,
                        ServiceId = data.ServiceId,
                        IsPaymentAuthorized = data.IsPaymentAuthorized.GetValueOrDefault(false) ? data.IsPaymentAuthorized.Value : false,
                        IsPaid = data.IsPaid.GetValueOrDefault(false) ? data.IsPaid.Value : false,
                        // ManagerName = (data.ExchangeActor.OwnerManager.FirstNameEng + " " + data.ExchangeActor.OwnerManager.FatherNameEng + " " + data.ExchangeActor.OwnerManager.GrandFatherNameEng),
                        EndDate = data.ServiceEndDateTime.HasValue ? data.ServiceEndDateTime.Value.ToString("MMMM dd, yyyy") : "",
                        Ecxcode = data.ExchangeActor.Ecxcode

                    })
                    .AsNoTracking()
                    .ToListAsync();

                return serviceApplicationList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }
    }
}
