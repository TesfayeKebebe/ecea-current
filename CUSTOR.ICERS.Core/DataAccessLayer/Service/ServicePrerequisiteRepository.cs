﻿using CUSTOR.ICERS.Core.EntityLayer.Service;
using CUSTOR.ICERS.Core.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Service
{
    public class ServicePrerequisiteRepository
    {
        private readonly ECEADbContext _context;

        public ServicePrerequisiteRepository(ECEADbContext context)
        {
            _context = context;
        }


        public async Task<List<ServicePrerequisiteWithDocumentDTO>> GetServicePrerequisite(string lang, int sid, int customerType)
        {

            try
            {
                List<ServicePrerequisiteWithDocumentDTO> result = null;


                result = await (from prerequisite in _context.Prerequisite
                                join servicePrerequisite in _context.ServicePrerequisite on prerequisite.PrerequisiteId equals servicePrerequisite.PrerequisiteId
                                where servicePrerequisite.ServiceId == sid && servicePrerequisite.CustomerTypeId == customerType
                                select new ServicePrerequisiteWithDocumentDTO
                                {
                                    ServicePrerequisiteId = servicePrerequisite.ServicePrerequisiteId,
                                    ServiceId = servicePrerequisite.ServiceId,
                                    IsRequired = servicePrerequisite.IsRequired,
                                    IsDocument = servicePrerequisite.IsDocument,
                                    Description = (lang == "et") ? prerequisite.DescriptionAmh : prerequisite.DescriptionEng,
                                    PrerequisiteId = prerequisite.PrerequisiteId
                                })
                        .ToListAsync();


                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }

        }

        public async Task<ServicePrerequisite> AddPrerequisiteForService(ServicePrerequisite servicePrerequisite)
        {
            try
            {
                // servicePrerequisite.EventDatetime = DateTime.Now;
                // servicePrerequisite.UserName = user.UserName;
                _context.ServicePrerequisite.Add(servicePrerequisite);
                await _context.SaveChangesAsync();
                return servicePrerequisite;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> Delete(int ServicePrereuisiteId)
        {
            try
            {
                var servicePrerequisite = await _context.ServicePrerequisite.SingleOrDefaultAsync(sp => sp.ServicePrerequisiteId == ServicePrereuisiteId);
                if (servicePrerequisite == null)
                {
                    return false;
                }
                _context.ServicePrerequisite.Remove(servicePrerequisite);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> EditPrerequistiteRequirment(int ServicePrereuisiteId, bool IsRequired, bool IsActive, bool IsDocument)
        {
            try
            {
                var servicePrerequisite = await _context.ServicePrerequisite.SingleOrDefaultAsync(sp => sp.ServicePrerequisiteId == ServicePrereuisiteId);
                if (servicePrerequisite == null)
                {
                    return false;
                }
                servicePrerequisite.IsRequired = IsRequired;
                servicePrerequisite.IsActive = IsActive;
                servicePrerequisite.IsDocument = IsDocument;
                _context.Entry(servicePrerequisite).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<UploadedDocumentVM>> GetAttachedDocumentOfService(Guid exchangeActorId, int serviceId, bool isDocument)
        {

            var attachedDocument = await (from sa in _context.ServiceApplication
             join cp in _context.CustomerPrerequisite on sa.ServiceApplicationId equals cp.ServiceApplicationId
             join sp in _context.ServicePrerequisite on cp.ServicePrerequisiteId equals sp.ServicePrerequisiteId
             join pr in _context.Prerequisite on sp.ServicePrerequisiteId equals pr.PrerequisiteId
             where sa.ServiceId == serviceId && sa.ExchangeActorId == exchangeActorId && sp.IsDocument == true 
             && sa.Status ==  (int)ServiceApplicaitonStatus.Completed
             select new UploadedDocumentVM
             {
                 FileName = cp.FileName,
                 Description = pr.DescriptionAmh
             }).AsNoTracking()
               .ToListAsync();

            return attachedDocument;
        }
    }
}
