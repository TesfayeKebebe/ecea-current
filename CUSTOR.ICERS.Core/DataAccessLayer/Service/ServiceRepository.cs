﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Service
{
    public class ServiceRepository
    {
        private readonly ECEADbContext _context;

        private readonly IMapper _mapper;

        public ServiceRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<LookupDTO3>> GetAllServiceType(string lang)
        {
            try
            {
                List<LookupDTO3> servieTypes = null;

                servieTypes = await _context.Service
                    .Where(service => service.IsDeleted == false && service.IsActive == true)
                    .Select(service => new LookupDTO3
                    {
                        Id = service.ServiceId,
                        Description = (lang == "et") ? service.DescriptionAmh : service.DescriptionEng
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return servieTypes;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

    }
}
