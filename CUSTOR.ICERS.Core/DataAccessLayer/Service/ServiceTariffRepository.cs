﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Service
{

    public class ServiceTariffRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public ServiceTariffRepository(ECEADbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;

        }

        //public async Task<decimal> GetServieTariffTotalCost(int serviceId)
        //{
        //    try
        //    {
        //        return await _context.ServiceTariff.Where(st => st.ServiceId == serviceId)
        //             .Include(st => st.Tariff)
        //             .SumAsync(st => st.Tariff.TariffAmount);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.InnerException.ToString());
        //    }
        //}

        public async Task<bool> SaveserviceTariff(ServicetariffDTO postedData)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            { 

                try
                {
                    var oldTariff = await _context.ServiceTariff.Where(Inj =>
                         Inj.ServiceTariffId == postedData.ServiceTariffId).FirstOrDefaultAsync();

                    if (oldTariff != null)
                    {
                        postedData.IsDeleted = oldTariff.IsDeleted;
                        postedData.IsActive = oldTariff.IsActive;
                        postedData.CreatedUserId = oldTariff.CreatedUserId;
                        postedData.UpdatedDateTime = DateTime.Now;
                        postedData.CreatedDateTime = oldTariff.CreatedDateTime;
                        ServiceTariff newCommodity = _mapper.Map(postedData, oldTariff);
                      
                        //   oldTariff = _mapper.Map<ServicetariffDTO, Tariff>(postedData, oldTariff);
                        _context.Entry(newCommodity).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }
                    else
                    {
                        ServiceTariff newData = _mapper.Map<ServiceTariff>(postedData);
                        _context.Add(newData);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        }
        public async Task<List<ServiceTariffDTO>> GetServiceTariffByServiecID(string lang, int serviceId, int exchangeActorTypeId)
        {

            var serviceTariff = await (from st in _context.ServiceTariff
                                       where st.ServiceId == serviceId && st.ExchangeActorTypeId == exchangeActorTypeId
                                       select new ServiceTariffDTO
                                       {
                                           TariffAmount = st.TariffAmount,
                                           Description = (lang == "et") ? st.DescriptionAmh : st.DescriptionEng
                                       })
                                       .AsNoTracking()
                                       .ToListAsync();

            return serviceTariff;
        }
        public async Task<bool> DeleteTariff(int tariffId)
        {
            try
            {
                var oldTariff = await _context.ServiceTariff.FirstOrDefaultAsync(Inj =>
                     Inj.ServiceTariffId == tariffId);
                if (oldTariff != null)
                {
                    oldTariff.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<AllServiceTariffDTO>> GetAllServiceTariff(string lang)
        {

           
            try
            {
                var serviceTariff = await (from st in _context.ServiceTariff
                                               //join ta in _context.Tariff on st.TariffId equals ta.TariffId
                                           where st.IsDeleted == false
                                           join ex in _context.Lookup 
                                           on st.ExchangeActorTypeId equals ex.LookupId
                                           orderby st.ExchangeActorTypeId
                                           join sty in _context.Service on st.ServiceId equals sty.ServiceId
                                           orderby sty.ServiceId
                                           select new AllServiceTariffDTO
                                           {
                                               TariffAmount = st.TariffAmount,
                                               DescriptionEng = st.DescriptionEng,
                                               DescriptionAmh = st.DescriptionAmh,
                                               DescriptionExchange = (lang == "et") ? ex.DescriptionAmh : ex.DescriptionEng,
                                               DescriptionService = (lang == "et") ? sty.DescriptionAmh : sty.DescriptionEng,
                                               ExchangeActorTypeId = st.ExchangeActorTypeId,
                                               ServiceId = st.ServiceId,
                                               ServiceTariffId = st.ServiceTariffId
                                           })
                                   .AsNoTracking()
                                   .ToListAsync();

                return serviceTariff;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
          

        }
    }

}





