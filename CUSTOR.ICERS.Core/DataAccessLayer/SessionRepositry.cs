﻿using CUSTOR.ICERS.Core.EntityLayer.ECX;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class SessionRepositry
    {
        ECEADbContext context;
        public SessionRepositry(ECEADbContext oTRLSDbContext)
        {
            context = oTRLSDbContext;
        }
        public async Task<List<SessionDTo>> GetSession()
        {
            List<SessionDTo> addedSession = new List<SessionDTo>();
            addedSession = await context.Session.Select(s=>new SessionDTo() {
                Description = s.Description,
                Id = s.Id
            }).ToListAsync();         
            return addedSession;
        }
    }
}
