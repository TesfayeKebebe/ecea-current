﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Setting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Setting
{
    public class GeneralSettingRepository
    {
        private readonly IMapper _mapper;
        private readonly ECEADbContext _context;
        public GeneralSettingRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<GeneralSetting> GetSetting()
        {
            try
            {
                var reportPrd = await _context.GeneralSetting
                    .Where(x => x.ID == 1)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PeriodicDurationSetting> GetRenewalSetting()
        {
            try
            {
                var reportPrd = await _context.PeriodicDurationSetting
                      .Where(x => x.ID == 1)
                      .AsNoTracking()
                      .FirstOrDefaultAsync();

                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateSetting(GeneralSettingDTO updatedSetting)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    var oldCustomerLegalBody = await _context.GeneralSetting.FirstOrDefaultAsync(clb => clb.ID == 1);

                    if (oldCustomerLegalBody == null)
                    {
                        GeneralSetting newCommodity = _mapper.Map<GeneralSetting>(updatedSetting);
                        newCommodity.ID = 1;
                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.ID = 1;
                        var updateReportPeriod = _mapper.Map(updatedSetting, oldCustomerLegalBody);
                        _context.Entry(updateReportPeriod).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }


        public async Task<bool> UpdateSettingPeriodicDuration(PeriodicDurationSettingDTO updatedSetting, int id)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())

                try
                {
                    var oldRenewalSetting = await _context.PeriodicDurationSetting.FirstOrDefaultAsync(clb => clb.ID == 1);

                    if (oldRenewalSetting == null)
                    {
                        PeriodicDurationSetting newCommodity = _mapper.Map<PeriodicDurationSetting>(updatedSetting);
                        newCommodity.ID = 1;
                        _context.Add(newCommodity);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        updatedSetting.ID = 1;
                        updatedSetting.IsDeleted = oldRenewalSetting.IsDeleted;
                        updatedSetting.IsActive = oldRenewalSetting.IsActive;
                        updatedSetting.CreatedUserId = oldRenewalSetting.CreatedUserId;
                        updatedSetting.UpdatedDateTime = DateTime.Now;
                        updatedSetting.CreatedDateTime = oldRenewalSetting.CreatedDateTime;

                        if (id == 1)
                        {
                            updatedSetting.LawEnforcmentGracePeriodDuration = oldRenewalSetting.LawEnforcmentGracePeriodDuration;
                            updatedSetting.LawEnforcmentGracePeriodUnit = oldRenewalSetting.LawEnforcmentGracePeriodUnit;
                        }
                        if (id == 2)
                        {
                            updatedSetting.InjectionDuration = oldRenewalSetting.InjectionDuration;
                            updatedSetting.InjectionDurationUnit = oldRenewalSetting.InjectionDurationUnit;
                            updatedSetting.GracePeriodDuration = oldRenewalSetting.LawEnforcmentGracePeriodUnit;
                            updatedSetting.GracePeriodUnit = oldRenewalSetting.LawEnforcmentGracePeriodUnit;
                            updatedSetting.RenewalDuration = oldRenewalSetting.RenewalDuration;
                            updatedSetting.RenewalDurationUnit= oldRenewalSetting.RenewalDurationUnit;
                            updatedSetting.TemporaryRenewalDuration = oldRenewalSetting.TemporaryRenewalDuration;
                            updatedSetting.TemporaryRenewalDurationUnit = oldRenewalSetting.TemporaryRenewalDurationUnit;

                        }
                        var updateReportPeriod = _mapper.Map(updatedSetting, oldRenewalSetting);
                        _context.Entry(updateReportPeriod).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        public PeriodicDurationSettingDTO GetGeneralSetting()
        {
            try
            {

                var data = (from setting in _context.PeriodicDurationSetting
                            select new PeriodicDurationSettingDTO
                            {
                                RenewalDuration = setting.RenewalDuration.GetValueOrDefault(),
                                RenewalDurationUnit = setting.RenewalDurationUnit.GetValueOrDefault()
                            })
                                  .AsNoTracking()
                                  .FirstOrDefault();

                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

        public async Task<bool> Savetotalcapital(RecoginationTotalCapitalDTO postedData)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    var oldTariff = await _context.RecoginationTotalCapital.Where(Inj =>
                         Inj.Id == postedData.Id).FirstOrDefaultAsync();

                    if (oldTariff != null)
                    {
                        postedData.IsDeleted = oldTariff.IsDeleted;
                        postedData.IsActive = oldTariff.IsActive;
                        postedData.CreatedUserId = oldTariff.CreatedUserId;
                        postedData.UpdatedDateTime = DateTime.Now;
                        postedData.CreatedDateTime = oldTariff.CreatedDateTime;
                        RecoginationTotalCapital newCommodity = _mapper.Map(postedData, oldTariff);

                        //   oldTariff = _mapper.Map<ServicetariffDTO, Tariff>(postedData, oldTariff);
                        _context.Entry(newCommodity).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        transaction.Commit();

                        return true;
                    }
                    else
                    {
                        RecoginationTotalCapital newData = _mapper.Map<RecoginationTotalCapital>(postedData);
                        _context.Add(newData);
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }
        public async Task<List<AllRecoginationTotalCapitalDTO>> getallTotalCapital(string lang)
        {


            try
            {
                var capitals = await (from st in _context.RecoginationTotalCapital
                                           //join ta in _context.Tariff on st.TariffId equals ta.TariffId
                                           where st.IsDeleted == false
                                           join ex in _context.Lookup
                                           on st.ExchangeActorTypeId equals ex.LookupId
                                           join ic in _context.Lookup on st.MemberCategoryId equals ic.LookupId into g
                                           from e in g.DefaultIfEmpty()
                                           orderby st.ExchangeActorTypeId
                                           select new AllRecoginationTotalCapitalDTO
                                           {
                                               Amount = st.Amount,
                                               DescriptionExchange = (lang == "et") ? ex.DescriptionAmh : ex.DescriptionEng,
                                               ExchangeActorTypeId = st.ExchangeActorTypeId,
                                               MemberCategoryId=st.MemberCategoryId,
                                               DescriptionMemberCatagory = (lang == "et") ? e.DescriptionAmh : e.DescriptionEng,
                                               Id = st.Id
                                           })
                                   .AsNoTracking()
                                   .ToListAsync();

                return capitals;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
        //public async Task<List<RecoginationTotalCapitalDTO>> GetServiceTariffByServiecID(string lang, int exchangeActorTypeId)
        //{

        //    var serviceTariff = await (from st in _context.RecoginationTotalCapital
        //                               where st.ExchangeActorTypeId == exchangeActorTypeId
        //                               select new RecoginationTotalCapitalDTO
        //                               {
        //                                   Amount = st.Amount,
        //                                   Description = (lang == "et") ? st.DescriptionAmh : st.DescriptionEng
        //                               })
        //                               .AsNoTracking()
        //                               .ToListAsync();

        //    return serviceTariff;
        //}
        public async Task<bool> DeleteCapital(int Id)
        {
            try
            {
                var oldCapital = await _context.RecoginationTotalCapital.FirstOrDefaultAsync(Inj => Inj.Id == Id);
                if (oldCapital != null)
                {
                    oldCapital.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
