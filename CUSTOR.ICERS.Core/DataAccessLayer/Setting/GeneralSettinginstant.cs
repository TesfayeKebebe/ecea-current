﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Setting
{
   public class GeneralSettinginstant
    {

        private  static IMapper _mapper;
        private  static ECEADbContext _context;
        public GeneralSettinginstant(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        private static  GeneralSettinginstant _mySingletonServiceInstance = new GeneralSettinginstant(_context,_mapper);

        public async Task<GeneralSetting> GetSetting()
        {
            try
            {
                var reportPrd = await _context.GeneralSetting
                    .Where(x => x.ID == 1)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static GeneralSettinginstant GetInstance() => _mySingletonServiceInstance;

    }
}
