﻿namespace CUSTOR.Oversight.DAL
{
    public class BankTransSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
