﻿namespace CUSTOR.Oversight.DAL
{
    public class BankUploadSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
