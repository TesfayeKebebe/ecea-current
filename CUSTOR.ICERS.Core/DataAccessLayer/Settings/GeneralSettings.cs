﻿namespace CUSTOR.Oversight.DAL
{
    public class GeneralSettings
    {
        public string OrganizationName { get; set; }
        public string ReturnURL { get; set; }
        public string AutoSaveAuditFields { get; set; }

    }
}
