﻿namespace CUSTOR.Oversight.DAL
{
    public class SiteSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
