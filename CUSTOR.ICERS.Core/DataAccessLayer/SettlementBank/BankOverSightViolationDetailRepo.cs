﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank
{
   public class BankOverSightViolationDetailRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public BankOverSightViolationDetailRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<BankOverSightViolationDetail> GetBankViolationRecordById(int id)
        {
            try
            {
                var tradeViolation = await context.BankOverSightViolationDetail
                          .Where(x => x.BankOverSightViolationDetailId == id)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                return tradeViolation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<BankOverSightViolationViewModels>> GetListOfViolationById(Guid exchangeActorId, string lang)
        {
            try
            {
                var bankVioaltionList = await context.BankOverSightViolationDetail
                    .Where(x => x.BankOversightViolation.ExchangeActorId == exchangeActorId &&
                            x.BankOversightViolation.ViolationStatus == 0)
                    .Select(n => new BankOverSightViolationViewModels
                    {
                        BankOversightViolationId = n.BankOversightViolationId,
                        FullName = (lang == "et")? n.BankOversightViolation.ExchangeActor.OrganizationNameAmh:
                                                   n.BankOversightViolation.ExchangeActor.OrganizationNameEng,
                        ECXCode = n.BankOversightViolation.ExchangeActor.Ecxcode,
                        CustomerType = (lang == "et") ? n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionAmh:
                                        n.BankOversightViolation.ExchangeActor.CustomerType.DescriptionAmh,
                        BankOverSightViolationDetailId = n.BankOverSightViolationDetailId,
                        DecisionTypeId = n.DecisionTypeId,
                        FaultTypeId = n.FaultTypeId,
                        ExchangeActorId = (n.ExchangeActorId != null) ? n.ExchangeActorId : Guid.Empty,
                        OrganizationName = (n.ExchangeActorId != Guid.Empty) ? ((lang == "et") ? n.ExchangeActor.OrganizationNameAmh:
                                              n.ExchangeActor.OrganizationNameEng): " ",
                        AccountNo = (n.AccountNo != null) ? n.AccountNo : " ",
                        Amount = n.Amount,
                        ErrorOcuredDueto = n.ErrorOcuredDueto,
                        DateOfError = n.DateOfError,
                        ErrorOfDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.DateOfError).ToString()) : n.DateOfError.ToShortDateString(),
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return bankVioaltionList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<BankOverSightViolationViewModels>> GetBankViolationListById(int exchangeActorId, string lang)
        {
            try
            {
                var bankVioaltionList = await context.BankOverSightViolationDetail
                    .Where(x => x.BankOversightViolationId == exchangeActorId &&
                            x.BankOversightViolation.ViolationStatus == 0)
                    .Select(n => new BankOverSightViolationViewModels
                    {
                        BankOversightViolationId = n.BankOversightViolationId,
                        BankOverSightViolationDetailId = n.BankOverSightViolationDetailId,
                        DecisionTypeId = n.DecisionTypeId,
                        FaultTypeId = n.FaultTypeId,
                        ExchangeActorId = (n.ExchangeActorId != null) ? n.ExchangeActorId : Guid.Empty,
                        OrganizationName = (n.ExchangeActorId != Guid.Empty) ? ((lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                              n.ExchangeActor.OrganizationNameEng) : " ",
                        AccountNo = (n.AccountNo != null) ? n.AccountNo : " ",
                        Amount = n.Amount,
                        ErrorOcuredDueto = n.ErrorOcuredDueto,
                        DateOfError = n.DateOfError,
                        ErrorOfDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.DateOfError).ToString()) : n.DateOfError.ToShortDateString(),
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return bankVioaltionList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<BankOverSightViolationViewModels>> GetBankViolationSearchByCriteria(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                var memberViolation = await context.BankOverSightViolationDetail.Where(x=>x.BankOversightViolation.Status == 2 &&
                                      (string.IsNullOrEmpty(clientTrade.ECXCode) || x.BankOversightViolation.ExchangeActor.Ecxcode == clientTrade.ECXCode) &&
                                      (string.IsNullOrEmpty(clientTrade.From) || x.DateOfError >= DateTime.Parse(clientTrade.From)) &&
                                      (string.IsNullOrEmpty(clientTrade.To) || x.DateOfError <= DateTime.Parse(clientTrade.To)) &&
                                      (clientTrade.ViolationTypeId == 0 || x.FaultTypeId == clientTrade.ViolationTypeId) &&
                                      (clientTrade.DecisionTypeId == 0 || x.DecisionTypeId == clientTrade.DecisionTypeId))
                                      .Select(n => new BankOverSightViolationViewModels
                                      {
                                          BankOversightViolationId = n.BankOversightViolationId,
                                          BankOverSightViolationDetailId = n.BankOverSightViolationDetailId,
                                          DecisionTypeId = n.DecisionTypeId,
                                          FaultTypeId = n.FaultTypeId,
                                          ExchangeActorId = n.ExchangeActorId,
                                          OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh : n.ExchangeActor.OrganizationNameEng,
                                          AccountNo = n.AccountNo,
                                          Amount = n.Amount,
                                          ErrorOcuredDueto = n.ErrorOcuredDueto,
                                          DateOfError = n.DateOfError,
                                          ErrorOfDate = (clientTrade.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.DateOfError).ToString()) : n.DateOfError.ToShortDateString()                                      })
                                      .AsNoTracking()
                                      .ToListAsync();
                return memberViolation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<int> UpdateBankViolation(BankOverSightViolationDetailDTO bankViolation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var bankViolationInDb = await context.BankOverSightViolationDetail
                                                      .Include(y=>y.BankOversightViolation)
                                                      .Where(x => x.BankOverSightViolationDetailId == bankViolation.BankOverSightViolationDetailId)
                                                      .AsNoTracking()
                                                      .FirstOrDefaultAsync();
                    if (bankViolationInDb != null)
                    {
                        var updateClientInfo = mapper.Map(bankViolation, bankViolationInDb);
                        context.Entry(updateClientInfo).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return bankViolationInDb.BankOversightViolationId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<int> DeleteBankViolation(BankOverSightViolationDetail bankViolation)
        {
            try
            {
                var bankViolationDetail = await context.BankOverSightViolationDetail
                    .Where(x => x.BankOverSightViolationDetailId == bankViolation.BankOverSightViolationDetailId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (bankViolationDetail != null)
                {
                    var deleteTradeExcution = mapper.Map(bankViolation, bankViolationDetail);
                    context.Entry(deleteTradeExcution).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return bankViolationDetail.BankOverSightViolationDetailId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<Guid> UpdateViolationRecordStatus(Guid exchangeActorId)
        {
            try
            {
                var violatedExchangeActor = await context.BankOversightViolation
                .Where(x => x.ViolationStatus == 0 &&
                x.Status == 2 &&
                x.ExchangeActorId == exchangeActorId)
                .AsNoTracking()
                .ToListAsync();
                foreach (var rec in violatedExchangeActor)
                {
                    rec.ViolationStatus = 1;
                    rec.Status = 0;
                    context.Entry(rec).State = EntityState.Modified;
                }
                await context.SaveChangesAsync();
                return exchangeActorId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
