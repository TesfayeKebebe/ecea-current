﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank
{
    public class BankOverSightViolationRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public BankOverSightViolationRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> CreateViolationRecord(BankOversightViolationDTO memberTradeViolation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tradeViolation = await context.BankOversightViolation.Where(
                                                                   x => x.ExchangeActorId == memberTradeViolation.ExchangeActorId &&
                                                                   x.ViolationStatus == 0)
                                                                   .AsNoTracking()
                                                                   .FirstOrDefaultAsync();
                    if (tradeViolation == null)
                    {
                        BankOversightViolation newViolationRecord = mapper.Map<BankOversightViolation>(memberTradeViolation);
                        context.Entry(newViolationRecord).State = EntityState.Added;
                        foreach (var clientInfo in newViolationRecord.BankOverSightViolationDetail)
                        {
                            clientInfo.CreatedUserId = memberTradeViolation.CreatedUserId;
                            context.Entry(clientInfo).State = EntityState.Added;
                        }
                        foreach (var clientInfo in newViolationRecord.WorkFlow)
                        {
                            clientInfo.CreatedUserId = memberTradeViolation.CreatedUserId;
                            context.Entry(clientInfo).State = EntityState.Added;
                        }
                        context.SaveChanges();
                        transaction.Commit();
                        return newViolationRecord.BankOversightViolationId;
                    }
                    else
                    {
                        foreach (var clientInfo in memberTradeViolation.BankOverSightViolationDetail)
                        {
                            clientInfo.BankOversightViolationId = tradeViolation.BankOversightViolationId;
                            BankOverSightViolationDetail mappedDetail = mapper.Map<BankOverSightViolationDetail>(clientInfo);
                            context.BankOverSightViolationDetail.Add(mappedDetail);
                        }
                        context.SaveChanges();
                        transaction.Commit();
                        return tradeViolation.BankOversightViolationId;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }


            }

        }
    }
}

