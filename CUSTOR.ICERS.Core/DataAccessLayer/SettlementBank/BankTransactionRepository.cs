using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank
{
    public class BankTransactionRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly BankTransSettings _settings;
        IHttpContextAccessor _httpAccessor;
        public BankTransactionRepository(ECEADbContext context, IMapper mapper, IOptions<BankTransSettings> settings,
                    IHttpContextAccessor httpAccessor)
        {
            _context = context;
            _mapper = mapper;
            _settings = settings.Value;
            _httpAccessor = httpAccessor;
            _context.CurrentUserId = _httpAccessor.HttpContext?.User.FindFirst(ClaimConstants.Subject)?.Value?.Trim();
            _context.AutoSaveAuditFields = true;

        }

        public async Task<BankTransaction> AddBankTransaction(BankTransaction bankTrans)
        {

            bankTrans.CreatedDateTime = DateTime.Now;
            _context.BankTransaction.Add(bankTrans);
            await _context.SaveChangesAsync();
            return bankTrans;
        }
        public async Task<bool> UploadTransaction(IFormFile xmlFile, string siteCode)
        {

            var folderName = Path.Combine("Uploads", "XML");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileToSave = Path.Combine(pathToSave, xmlFile.FileName).ToString();
            try
            {
                using (var fileStream = new FileStream(fileToSave, FileMode.Create))
                {
                    await xmlFile.CopyToAsync(fileStream);
                    fileStream.Dispose();
                    XDocument xDoc = XDocument.Load(fileToSave);
                    //to-do change to utc
                    string dateFormat = "m/d/yyyy";
                    int i = xDoc.Descendants("BankTransaction").ToList().Count;
                    List<BankTransaction> transList = xDoc.Descendants("transaction").Select(xNode =>
                    new BankTransaction
                    {
                        AccountHolderName = xNode.Element("AccountHolderName").Value,
                        AccountNo = xNode.Element("AccountNumber").Value,
                        AccountType = xNode.Element("AccountType").Value,
                        MemberType = xNode.Element("MemberType").Value,
                        BranchName = xNode.Element("BranchName").Value,
                        Credit = Convert.ToDecimal(xNode.Element("Credit").Value),
                        Debit = Convert.ToDecimal(xNode.Element("Debit").Value),
                        Balance = Convert.ToDecimal(xNode.Element("Balance").Value),
                        BBF = Convert.ToDecimal(xNode.Element("BBF").Value),
                        TransactionDate = DateTime.ParseExact(xNode.Element("TransactionDate").Value, dateFormat, CultureInfo.InvariantCulture),
                        Reference = xNode.Element("Reference").Value,
                        UnAuthorized = true,
                        IsThroughUpload = true,
                        SiteCode = siteCode,
                        CreatedUserId = "Abebe", //to be changed
                        CreatedDateTime = DateTime.UtcNow //to be changed

                    }).ToList();

                    int n = transList.Count;
                    return await AddBankTransactions(transList, siteCode, xmlFile.FileName, n);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw new Exception(e.Message);
            }
        }
        public async Task<bool> AddBankTransactions(List<BankTransaction> transList, string siteCode, string fileName, int noRecords)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    _context.BankTransaction.AddRange(transList);
                    await _context.SaveChangesAsync();
                    BankUpload upload = new BankUpload();
                    upload.SiteCode = siteCode;
                    string fileNameToSave = fileName + "_" + siteCode + "_" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
                    upload.FileName = fileNameToSave;
                    upload.NoOfRecords = transList.Count;
                    upload.NoOfRecords = noRecords;
                    _context.BankUpload.Add(upload);
                    await _context.SaveChangesAsync();

                }
                catch (Exception ex)
                {
                    string s = ex.Message;
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                transaction.Commit();
            }
            return true;
        }
        public async Task<PagedResult<BankTransactionDTO>> GetAllBankTransactionsAsync(QueryParameters qp)
        {
            try

            {
                qp.PageCount = qp.PageCount == 0 ? _settings.DefaultPageSize : qp.PageCount;
                DateTime From = qp.DateFrom;
                DateTime To = qp.DateTo;
                // this is expensive - to be improved later
                int itemsCount = _context.BankTransaction.Where(x => x.SiteCode.ToLower().TrimEnd() == qp.SiteCode.ToLower().TrimEnd() &&
                   (x.TransactionDate >= From && x.TransactionDate <= To)).Count();

                IEnumerable<BankTransaction> query = await _context.BankTransaction
                    .Where(x => x.SiteCode.ToLower().TrimEnd() == qp.SiteCode.ToLower().TrimEnd() && (x.TransactionDate >= From && x.TransactionDate <= To))
                    .Paging(qp.PageCount, qp.PageNumber)
                    .ToListAsync();

                var result = _mapper.Map<IEnumerable<BankTransaction>, IEnumerable<BankTransactionDTO>>(query);
                return new PagedResult<BankTransactionDTO>()
                {
                    Items = result,
                    ItemsCount = itemsCount
                };

            }
            catch (Exception ex)
            {
                string str = ex.Message;
                throw new Exception(ex.Message);
            }

        }
        public async Task<PagedResult<BankTransactionDTO>> GetAllBankTransactionsAsync(QueryParameters qParam, string siteCode)
        {
            qParam.PageCount = qParam.PageCount == 0 ? _settings.DefaultPageSize : qParam.PageCount;
            IEnumerable<BankTransaction> query = await _context.BankTransaction
                .Paging(qParam.PageCount, qParam.PageNumber)
                .ToListAsync();

            if (!string.IsNullOrEmpty(siteCode))
                query = query.Where(x => x.SiteCode == siteCode);

            var result = _mapper.Map<IEnumerable<BankTransaction>, IEnumerable<BankTransactionDTO>>(query);
            //int i = _context.BankTransaction.Count();
            return new PagedResult<BankTransactionDTO>()
            {
                Items = result,
                ItemsCount = _context.BankTransaction.Count()
            };

        }
        public async Task<List<BankTransaction>> GetAllBankTransactionList(string memberCode, string memberType, string accountType, string SiteCode, string start, string end)

        {
            try
            {
                List<BankTransaction> bankTransaction = null;
                if (memberCode == null || memberType == null || accountType == null || start == null || end == null || SiteCode == null)

                {
                    bankTransaction = await _context.BankTransaction
                                                                   .Where(x => x.UnAuthorized == true || x.UnAuthorized == false)
                                                                   .AsNoTracking()
                                                                   .ToListAsync();
                }
                else if (memberCode == null || memberType == null || accountType == null || SiteCode != null && start == null && end == null)
                {
                    bankTransaction = await _context.BankTransaction
                                                                   .Where(x =>
                                                                   x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                   .AsNoTracking()
                                                                   .ToListAsync();
                }
                else if (memberCode != null || memberType != null || accountType != null || SiteCode != null && start != null && end != null)

                {
                    bankTransaction = await _context.BankTransaction.Where(x =>  x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start))>=0 &&
                                                                                   x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                       x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd() &&
                                                                                       x.MemberType == memberType &&
                                                                                       x.AccountType == accountType &&
                                                                                       x.MemberCode == memberCode)
                                                                                        .AsNoTracking()
                                                                                       .ToListAsync();
                }
                else if (memberCode == null || memberType != null || accountType != null || SiteCode != null && start != null && end != null)

                {
                    bankTransaction = await _context.BankTransaction.Where(x =>   x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                   x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                   x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd() &&
                                                                                   x.MemberType == memberType &&
                                                                                   x.AccountType == accountType )
                                                                                        .AsNoTracking()
                                                                                       .ToListAsync();
                }
                else if (memberType == null || accountType == null || SiteCode == null && start != null && end != null)
                {

                    bankTransaction = await _context.BankTransaction.Where(x => 
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start))>= 0 &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                .AsNoTracking()
                                                                                .ToListAsync();
                }
                else if (memberCode != null || memberType != null || accountType != null || SiteCode == null && start == null && end == null)
                {
                    bankTransaction = await _context.BankTransaction.Where(x => 
                                                                            x.MemberCode == memberCode &&
                                                                           x.MemberType == memberType &&
                                                                           x.AccountType == accountType)
                                                                            .AsNoTracking()
                                                                            .ToListAsync();
                }

                return bankTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<BankTransactionViewModel>> GetDailyBankTransactionList(SearchBankTransactionCriteria searchBankTransaction)
        {
            try
            {
              var  bankTransaction = await _context.BankTransaction.Where(x => ( x.UnAuthorized == true || x.UnAuthorized == false) &&
                                                                               (string.IsNullOrEmpty(searchBankTransaction.SiteCode) || x.SiteCode == searchBankTransaction.SiteCode) &&
                                                                               (string.IsNullOrEmpty(searchBankTransaction.MemberType) || x.MemberType == searchBankTransaction.MemberType) &&
                                                                               (string.IsNullOrEmpty(searchBankTransaction.AccountType) || x.AccountType == searchBankTransaction.AccountType) &&
                                                                               (string.IsNullOrEmpty(searchBankTransaction.Start) || x.TransactionDate >= DateTime.Parse(searchBankTransaction.Start))&&
                                                                               (string.IsNullOrEmpty(searchBankTransaction.End) || x.TransactionDate <= DateTime.Parse(searchBankTransaction.End))

                                                                               )
                                                          .Select(n => new BankTransactionViewModel
                                                          {
                                                              AccountHolderName = n.AccountHolderName,
                                                              AccountNo = n.AccountNo,
                                                              AccountType = n.AccountType,
                                                              MemberType = n.MemberType,
                                                              BBF = n.BBF,
                                                              Debit = n.Debit,
                                                              Credit = n.Credit,
                                                              Balance = n.Balance
                                                          })
                                                          .AsNoTracking()
                                                          .ToListAsync();
                return bankTransaction;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<BankUploadDTO>> GetUploadSummaryAsync(string siteCode)
        {
            try
            {

                var query = await _context.BankUploadDTO
                            .FromSqlRaw<BankUploadDTO>(@"SELECT BankUploadId, BankUpload.SiteCode, BankUpload.CreatedDateTime, NoOfRecords, BankUpload.FileName,  
                                                                        s.NameEnglish as BankName FROM BankUpload, Site s  Where  BankUpload.SiteCode = s.SiteCode and 
                                                                        BankUpload.SiteCode = {siteCode}")
                            .AsNoTracking()
                            .OrderByDescending(d => d.CreatedDateTime)
                            .ToListAsync();

                return query;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                throw new Exception(ex.Message);
            }
        }
        public async Task<BankTransaction> GetBankTransactionAsync(int BankTransactionId)
        {
            return await _context.BankTransaction.AsNoTracking().Where(b => b.BankTransactionId == BankTransactionId).FirstOrDefaultAsync();
        }
        public async Task<BankTransactionDTO> DeleteBankTransactionAsync(int BankTransactionId)
        {
            BankTransaction BankTransaction = await GetBankTransactionAsync(BankTransactionId);

            if (BankTransaction != null)
            {
                _context.BankTransaction.Remove(BankTransaction);

                await _context.SaveChangesAsync();
            }
            return _mapper.Map<BankTransaction, BankTransactionDTO>(BankTransaction);
        }
        public async Task<List<ClearingSettlementBank>> GetListClearingSettlementBank(string SiteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if (SiteCode != null || start != null || end != null)
                {                   
                    DateTime transactionReportMonnth = DateTime.Parse(start).AddMonths(0);
                    var listOfPayins = await _context.BankTransaction.Where(x =>
                                                                              x.AccountType == "PayIn" &&
                                                                              x.MemberType == "Member" &&
                                                                              x.TransactionDate.Month == transactionReportMonnth.Month &&                                                                             
                                                                              x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                              .OrderBy(x=>x.TransactionDate)
                                                                              .ToListAsync();
                    var membersBBFPayIn = listOfPayins.Select(x => new { x.AccountHolderName,x.SiteCode}).Distinct();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    var memberPayInBBf = 0M;
                    foreach (var name in membersBBFPayIn)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listOfPayins.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if (xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                        else if (xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        memberPayInBBf += bbf; 
                    }
                    var listOfPayin = await _context.BankTransaction.Where(x => 
                                                                              x.AccountType == "PayIn" &&
                                                                              x.MemberType == "Member" &&
                                                                              (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                              x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                              x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                               .ToListAsync();


                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = memberPayInBBf;
                    settlementBank.TotalDebet = listOfPayin.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfPayin.Sum(x => x.Credit);
                    settlementBank.Balance = (settlementBank.TotalCredit + memberPayInBBf) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Member Pay-In";
                    addedClear.Add(settlementBank);

                    var listClientOfPayins = await _context.BankTransaction.Where(x =>
                                                                              x.AccountType == "PayIn" &&
                                                                              x.MemberType == "Client" &&
                                                                              x.TransactionDate.Month == transactionReportMonnth.Month &&
                                                                              x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                              .OrderBy(x => x.TransactionDate)
                                                                              .ToListAsync();
                    var ClientsBBFPayIn = listClientOfPayins.Select(x => new { x.AccountHolderName, x.SiteCode }).Distinct();
                    var ClientPayInBBf = 0M;
                    foreach (var name in ClientsBBFPayIn)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listClientOfPayins.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if (xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                        else if (xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        ClientPayInBBf += bbf;
                    }

                    var listOfCleintPayIn = await _context.BankTransaction.Where(x => 
                                                                                  x.AccountType == "PayIn" &&
                                                                                  x.MemberType == "Client" &&
                                                                                  (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                  x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                  x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                  .ToListAsync();

                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = ClientPayInBBf;
                    settlementBank.TotalDebet = listOfCleintPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = (ClientPayInBBf + settlementBank.TotalCredit) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Client Pay-In";
                    addedClear.Add(settlementBank);


                    var listNonMemberOfPayins = await _context.BankTransaction.Where(x =>
                                                                            x.AccountType == "PayIn" &&
                                                                            x.MemberType == "Non-Member" &&
                                                                            x.TransactionDate.Month == transactionReportMonnth.Month &&
                                                                            x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                            .OrderBy(x => x.TransactionDate)
                                                                            .ToListAsync();
                    var nonMembersBBFPayIn = listNonMemberOfPayins.Select(x => new { x.AccountHolderName, x.SiteCode }).Distinct();
                    var nonMemberPayInBBf = 0M;
                    foreach (var name in nonMembersBBFPayIn)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listNonMemberOfPayins.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if(xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                       else if (xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        nonMemberPayInBBf += bbf;
                    }
                    var listOfNonMebersPayIn = await _context.BankTransaction.Where(x => 
                                                                             x.AccountType == "PayIn" &&
                                                                             x.MemberType == "Non-Member" &&
                                                                             (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                             x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                             x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                             .ToListAsync();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = nonMemberPayInBBf;
                    settlementBank.TotalDebet = listOfNonMebersPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = (nonMemberPayInBBf + settlementBank.TotalCredit) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-In";
                    addedClear.Add(settlementBank);

                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = addedClear.Sum(x => x.BBF);
                    settlementBank.TotalDebet = addedClear.Sum(x => x.TotalDebet);
                    settlementBank.TotalCredit = addedClear.Sum(x => x.TotalCredit);
                    settlementBank.Balance = addedClear.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY-IN";
                    addedClear.Add(settlementBank);


                    var listMemberOfPayOut = await _context.BankTransaction.Where(x =>
                                                                           x.AccountType == "PayOut" &&
                                                                           x.MemberType == "Member" &&
                                                                           x.TransactionDate.Month == transactionReportMonnth.Month &&
                                                                           x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                           .OrderBy(x => x.TransactionDate)
                                                                           .ToListAsync();
                    var membersBBFPayOut = listMemberOfPayOut.Select(x => new { x.AccountHolderName, x.SiteCode }).Distinct();
                    var membersPayOutBBf = 0M;
                    foreach (var name in membersBBFPayOut)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listMemberOfPayOut.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if(xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                       else if (xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        membersPayOutBBf += bbf;
                    }
                    var listOfMebersPayOut = await _context.BankTransaction.Where(x => 
                                                                                       x.AccountType == "PayOut" &&
                                                                                       x.MemberType == "Member" &&
                                                                                       (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                       x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0 )&&
                                                                                       x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                       .ToListAsync();

                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = membersPayOutBBf;
                    settlementBank.TotalDebet = listOfMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = (membersPayOutBBf + settlementBank.TotalCredit) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Members Pay-Out";
                    addedClear.Add(settlementBank);

                    var listClientOfPayOut = await _context.BankTransaction.Where(x =>
                                                                              x.AccountType == "PayOut" &&
                                                                              x.MemberType == "Client" &&
                                                                              x.TransactionDate.Month == transactionReportMonnth.Month &&
                                                                              x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                              .OrderBy(x => x.TransactionDate)
                                                                              .ToListAsync();
                    var ClientsBBFPayOut = listClientOfPayOut.Select(x => new { x.AccountHolderName, x.SiteCode }).Distinct();
                    var ClientPayOutBBf = 0M;
                    foreach (var name in ClientsBBFPayOut)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listClientOfPayOut.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if (xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                        else if(xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        ClientPayOutBBf += bbf;
                    }

                    var listOfCleintPayOut = await _context.BankTransaction.Where(x => 
                                                                                  x.AccountType == "PayOut" &&
                                                                                  x.MemberType == "Client" &&
                                                                                  (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                  x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                  x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                 .ToListAsync();

                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = ClientPayOutBBf;
                    settlementBank.TotalDebet = listOfCleintPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = (settlementBank.TotalCredit + ClientPayOutBBf) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Client Pay-Out";
                    addedClear.Add(settlementBank);

                    var listNonMembersOfPayOut = await _context.BankTransaction.Where(x =>
                                                                              x.AccountType == "PayOut" &&
                                                                              x.MemberType == "Non-Member" &&
                                                                              x.TransactionDate.Month == transactionReportMonnth.Month &&
                                                                              x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                              .OrderBy(x => x.TransactionDate)
                                                                              .ToListAsync();
                    var noNmembersBBFPayOut = listNonMembersOfPayOut.Select(x => new { x.AccountHolderName, x.SiteCode }).Distinct();
                    var nonMemberPayOutBBF = 0M;
                    foreach (var name in noNmembersBBFPayOut)
                    {
                        settlementBank = new ClearingSettlementBank();
                        List<BankTransaction> xx = listNonMembersOfPayOut.Where(x => x.AccountHolderName == name.AccountHolderName && x.SiteCode == name.SiteCode).OrderBy(x => x.TransactionDate).ToList();
                        var bbf = 0M;
                        if (xx.Count() == 0)
                        {
                            bbf = 0;
                        }
                        else if (xx.Count() == 1)
                        {
                            bbf = xx[0].BBF;
                        }
                        else
                        {
                            bbf = xx[xx.Count() - 1].BBF;
                        }
                        nonMemberPayOutBBF += bbf;
                    }


                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => 
                                                                                      x.AccountType == "PayOut" &&
                                                                                      x.MemberType == "Non-Member" &&
                                                                                      (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                      x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                      x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                      .ToListAsync();

                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = nonMemberPayOutBBF;
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = (nonMemberPayOutBBF + settlementBank.TotalCredit) - settlementBank.TotalDebet;
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);
                    var listOfTotalPayOut = await _context.BankTransaction.Where(x => 
                                                                                          x.AccountType == "PayOut" &&
                                                                                          (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                          .ToListAsync();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfTotalPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfTotalPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfTotalPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfTotalPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY_OUT";
                    addedClear.Add(settlementBank);
                    var listOfDiffPayIn = await _context.BankTransaction.Where(x => 
                                                                                          x.AccountType == "PayIn" &&
                                                                                          (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                          .ToListAsync();
                    var listOfDiffPayOut = await _context.BankTransaction.Where(x => 
                                                                                          x.AccountType == "PayOut" &&
                                                                                          (x.TransactionDate.Date.CompareTo(DateTime.Parse(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(DateTime.Parse(end)) <= 0) &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == SiteCode.ToLower().TrimEnd())
                                                                                          .ToListAsync();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = (listOfDiffPayIn.Sum(x => x.BBF)) - (listOfDiffPayOut.Sum(x => x.BBF));
                    settlementBank.TotalDebet = (listOfDiffPayIn.Sum(x => x.Debit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.TotalCredit = (listOfDiffPayIn.Sum(x => x.Credit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.Balance = (listOfDiffPayIn.Sum(x => x.Balance)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.AccountType = "DIFFERENCE B/N PAY-IN and PAY-OUT";
                    addedClear.Add(settlementBank);
                }
                else
                {
                    return null;
                }
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<ClearingSettlementBank>> GetTotalMemberPayInBankList(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfPayin = await _context.BankTransaction.Where(x => 
                                                                                              x.AccountType == "PayIn" &&
                                                                                              x.MemberType == "Member")
                                                                                               .ToListAsync();

                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfPayin.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfPayin.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfPayin.Sum(x => x.Credit);
                    settlementBank.Balance = listOfPayin.Sum(x => x.Balance);
                    settlementBank.AccountType = "Member Pay-In";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfPayin = await _context.BankTransaction.Where(x =>
                                                                                x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                x.AccountType == "PayIn" &&
                                                                                x.MemberType == "Member")
                                                                                .ToListAsync();

                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfPayin.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfPayin.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfPayin.Sum(x => x.Credit);
                    settlementBank.Balance = listOfPayin.Sum(x => x.Balance);
                    settlementBank.AccountType = "Member Pay-In";
                    addedClear.Add(settlementBank);

                }else if(siteCode == null && start != null && end != null)
                {
                    var listOfPayin = await _context.BankTransaction.Where(x =>
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                x.AccountType == "PayIn" &&
                                                                                x.MemberType == "Member")
                                                                                .ToListAsync();

                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfPayin.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfPayin.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfPayin.Sum(x => x.Credit);
                    settlementBank.Balance = listOfPayin.Sum(x => x.Balance);
                    settlementBank.AccountType = "Member Pay-In";
                    addedClear.Add(settlementBank);
                }else if(siteCode != null && start != null && end != null)
                {
                    var listOfPayin = await _context.BankTransaction.Where(x =>
                                                                                x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                x.AccountType == "PayIn" &&
                                                                                x.MemberType == "Member")
                                                                                .ToListAsync();

                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfPayin.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfPayin.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfPayin.Sum(x => x.Credit);
                    settlementBank.Balance = listOfPayin.Sum(x => x.Balance);
                    settlementBank.AccountType = "Member Pay-In";
                    addedClear.Add(settlementBank);
                }
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<ClearingSettlementBank>> GetTotalClientPayInList(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfCleintPayIn = await _context.BankTransaction.Where(x => 
                                                                                                  x.AccountType == "PayIn" &&
                                                                                                  x.MemberType == "Client")
                                                                                                  .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayIn.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayIn.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-In";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfCleintPayIn = await _context.BankTransaction.Where(x =>
                                                                                       x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                                  x.AccountType == "PayIn" &&
                                                                                                  x.MemberType == "Client")
                                                                                                  .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayIn.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayIn.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-In";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfCleintPayIn = await _context.BankTransaction.Where(x =>
                                                                               x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                              x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                              x.AccountType == "PayIn" &&
                                                                              x.MemberType == "Client")
                                                                              .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayIn.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayIn.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-In";
                    addedClear.Add(settlementBank);


                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfCleintPayIn = await _context.BankTransaction.Where(x =>
                                                                                x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                                  x.AccountType == "PayIn" &&
                                                                                                  x.MemberType == "Client")
                                                                                                  .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayIn.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayIn.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayIn.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayIn.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-In";
                    addedClear.Add(settlementBank);

                }
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetNonMembersPayInList( string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => 
                                                                                           x.AccountType == "PayIn" &&
                                                                                           x.MemberType == "Non-Member")
                                                                                            .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.AccountType == "PayIn" &&
                                                                                          x.MemberType == "Non-Member")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);


                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                          x.AccountType == "PayIn" &&
                                                                                          x.MemberType == "Non-Members")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                          x.AccountType == "PayIn" &&
                                                                                          x.MemberType == "Non-Members")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);
                }                
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetTotalPayInList(string siteCode,string start,string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => 
                                                                                   x.AccountType == "PayIn")
                                                                                   .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY-IN";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.AccountType == "PayIn")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY-IN";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                               x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                              x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                x.AccountType == "PayIn")
                                                                                .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY-IN";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                 x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                 x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                 x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                 x.AccountType == "PayIn")
                                                                                .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL PAY-IN";
                    addedClear.Add(settlementBank);
                }                
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetMembersTotalPayOut(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfMebersPayOut = await _context.BankTransaction.Where(x => 
                                                                                                      x.AccountType == "PayOut" &&
                                                                                                      x.MemberType == "Member")
                                                                                                      .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Members Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                       x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                                      x.AccountType == "PayOut" &&
                                                                                                      x.MemberType == "Member")
                                                                                                      .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Members Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                                      x.AccountType == "PayOut" &&
                                                                                                      x.MemberType == "Member")
                                                                                                      .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Members Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfMebersPayOut = await _context.BankTransaction.Where(x =>
                                                                                x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                                      x.AccountType == "PayOut" &&
                                                                                                      x.MemberType == "Member")
                                                                                                      .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Members Pay-Out";
                    addedClear.Add(settlementBank);
                }               
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetClientTotalPayOut(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfCleintPayOut = await _context.BankTransaction.Where(x => 
                                                                                                  x.AccountType == "PayOut" &&
                                                                                                  x.MemberType == "Client")
                                                                                                 .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfCleintPayOut = await _context.BankTransaction.Where(x =>
                                                                                       x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                       x.AccountType == "PayOut" &&
                                                                                       x.MemberType == "Client")
                                                                                       .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfCleintPayOut = await _context.BankTransaction.Where(x =>
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                       x.AccountType == "PayOut" &&
                                                                                       x.MemberType == "Client")
                                                                                       .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfCleintPayOut = await _context.BankTransaction.Where(x =>
                                                                                       x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                       x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                       x.AccountType == "PayOut" &&
                                                                                       x.MemberType == "Client")
                                                                                       .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfCleintPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfCleintPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfCleintPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfCleintPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Client Pay-Out";
                    addedClear.Add(settlementBank);

                }
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<ClearingSettlementBank>> GetNonMembersTotalPayOut(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                                            // x.SiteCode == siteCode &&
                                                                                                             x.MemberType == "Non-Members")
                                                                                                             .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);

                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.MemberType == "Non-Members")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                          x.MemberType == "Non-Members")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 &&
                                                                                          x.MemberType == "Non-Members")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "Non-Members Direct Trading Pay-Out";
                    addedClear.Add(settlementBank);
                }                
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetTotalPayOut(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "Pay-Out")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd())
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0 )
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL Pay-Out";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfNonMebersPayOut = await _context.BankTransaction.Where(x => x.AccountType == "PayOut" &&
                                                                                          x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                          x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = listOfNonMebersPayOut.Sum(x => x.BBF);
                    settlementBank.TotalDebet = listOfNonMebersPayOut.Sum(x => x.Debit);
                    settlementBank.TotalCredit = listOfNonMebersPayOut.Sum(x => x.Credit);
                    settlementBank.Balance = listOfNonMebersPayOut.Sum(x => x.Balance);
                    settlementBank.AccountType = "TOTAL Pay-Out";
                    addedClear.Add(settlementBank);

                }
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClearingSettlementBank>> GetDifferencePayInPayOut(string siteCode, string start, string end)
        {
            List<ClearingSettlementBank> addedClear = new List<ClearingSettlementBank>();
            try
            {
                if(siteCode == null && start == null && end == null)
                {
                    var listOfDiffPayIn = await _context.BankTransaction.Where(x =>  x.AccountType == "PayIn")
                                                                                                          .ToListAsync();
                    var listOfDiffPayOut = await _context.BankTransaction.Where(x =>  x.AccountType == "PayOut")
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = (listOfDiffPayIn.Sum(x => x.BBF)) - (listOfDiffPayOut.Sum(x => x.BBF));
                    settlementBank.TotalDebet = (listOfDiffPayIn.Sum(x => x.Debit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.TotalCredit = (listOfDiffPayIn.Sum(x => x.Credit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.Balance = (listOfDiffPayIn.Sum(x => x.Balance)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.AccountType = "DIFFERENCE B/N PAY-IN and PAY-OUT";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start == null && end == null)
                {
                    var listOfDiffPayIn = await _context.BankTransaction.Where(x =>  
                                                                                    x.AccountType == "PayIn" &&
                                                                                    x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd())
                                                                                    .ToListAsync();
                    var listOfDiffPayOut = await _context.BankTransaction.Where(x =>  
                                                                                     x.AccountType == "PayOut" &&
                                                                                     x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd())
                                                                                     .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = (listOfDiffPayIn.Sum(x => x.BBF)) - (listOfDiffPayOut.Sum(x => x.BBF));
                    settlementBank.TotalDebet = (listOfDiffPayIn.Sum(x => x.Debit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.TotalCredit = (listOfDiffPayIn.Sum(x => x.Credit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.Balance = (listOfDiffPayIn.Sum(x => x.Balance)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.AccountType = "DIFFERENCE B/N PAY-IN and PAY-OUT";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode == null && start != null && end != null)
                {
                    var listOfDiffPayIn = await _context.BankTransaction.Where(x =>
                                                                                   x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                   x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                    .ToListAsync();
                    var listOfDiffPayOut = await _context.BankTransaction.Where(x =>  
                                                                                     x.AccountType == "PayOut" &&
                                                                                     x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                     x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = (listOfDiffPayIn.Sum(x => x.BBF)) - (listOfDiffPayOut.Sum(x => x.BBF));
                    settlementBank.TotalDebet = (listOfDiffPayIn.Sum(x => x.Debit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.TotalCredit = (listOfDiffPayIn.Sum(x => x.Credit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.Balance = (listOfDiffPayIn.Sum(x => x.Balance)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.AccountType = "DIFFERENCE B/N PAY-IN and PAY-OUT";
                    addedClear.Add(settlementBank);
                }
                else if(siteCode != null && start != null && end != null)
                {
                    var listOfDiffPayIn = await _context.BankTransaction.Where(x => 
                                                                                    x.AccountType == "PayIn" &&
                                                                                    x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                    x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                    x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                     .ToListAsync();
                    var listOfDiffPayOut = await _context.BankTransaction.Where(x => 
                                                                                     x.AccountType == "PayOut" &&
                                                                                     x.SiteCode.ToLower().TrimEnd() == siteCode.ToLower().TrimEnd() &&
                                                                                     x.TransactionDate.Date.CompareTo(Convert.ToDateTime(start)) >= 0 &&
                                                                                     x.TransactionDate.Date.CompareTo(Convert.ToDateTime(end)) <= 0)
                                                                                          .ToListAsync();
                    ClearingSettlementBank settlementBank = new ClearingSettlementBank();
                    settlementBank.BBF = (listOfDiffPayIn.Sum(x => x.BBF)) - (listOfDiffPayOut.Sum(x => x.BBF));
                    settlementBank.TotalDebet = (listOfDiffPayIn.Sum(x => x.Debit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.TotalCredit = (listOfDiffPayIn.Sum(x => x.Credit)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.Balance = (listOfDiffPayIn.Sum(x => x.Balance)) - (listOfDiffPayOut.Sum(x => x.Debit));
                    settlementBank.AccountType = "DIFFERENCE B/N PAY-IN and PAY-OUT";
                    addedClear.Add(settlementBank);
                }                
                return addedClear;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<AllBankTransactionReportView> GetAllBankTransactions(SearchBankTransactionCriteria searchBankTransaction)
        {
            List<AllBankTransactionReportView> addBankTransaction = new List<AllBankTransactionReportView>();
            try
            {
                var allBankTransaction = _context.BankTransaction.Include(x=>x.Site).Where(x => x.AccountType == searchBankTransaction.AccountType &&
                                                               (string.IsNullOrEmpty(searchBankTransaction.MemberType) || x.MemberType == searchBankTransaction.MemberType)&&
                                                               x.AccountType == searchBankTransaction.AccountType &&
                                                               (string.IsNullOrEmpty(searchBankTransaction.Start) ||
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.Start)) >=0) &&
                                                               (string.IsNullOrEmpty(searchBankTransaction.End) ||
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.End))<=0))
                                                               .AsNoTracking()
                                                               .ToList();
                if (allBankTransaction.Count() == 0)
                {
                    return null;
                }
                decimal totalBalance = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Sum(x => x.Balance)));
                if (totalBalance == 0)
                {
                    totalBalance = 1;
                }
                decimal totalDebit = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Sum(x => x.Debit)));
                if (totalDebit == 0)
                {
                    totalDebit = 1;
                }
                decimal totalCredit = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Sum(x => x.Credit)));
                if (totalCredit == 0)
                {
                    totalCredit = 1;
                }
                AllBankTransactionReportView settlementBank = new AllBankTransactionReportView();
                var allBankTransactionList = allBankTransaction.Select(x => new { x.SiteCode,x.Site }).Distinct();
                foreach (var name in allBankTransactionList)
                {
                    settlementBank = new AllBankTransactionReportView();
                    settlementBank.BankName = (searchBankTransaction.Lang == "et") ? name.Site.Name : name.Site.NameEnglish;
                    settlementBank.BBF = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.BBF)));
                    settlementBank.TotalDebet = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Debit)));
                    settlementBank.TotalCredit = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Credit)));
                    settlementBank.Balance = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Balance)));
                    settlementBank.TotalDebitPercentage = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Debit) * 100 / (totalDebit)));
                    settlementBank.TotalCreditPercentage = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Credit) * 100 / (totalCredit)));
                    settlementBank.BalancePercentage = Convert.ToDecimal(String.Format("{0:00.00}", allBankTransaction.Where(x => x.SiteCode == name.SiteCode).Sum(x => x.Balance) * 100 / (totalBalance)));
                    addBankTransaction.Add(settlementBank);

                }

                settlementBank = new AllBankTransactionReportView();
                settlementBank.BBF = addBankTransaction.Sum(x => x.BBF);
                settlementBank.TotalDebet = addBankTransaction.Sum(x => x.TotalDebet);
                settlementBank.TotalCredit = addBankTransaction.Sum(x => x.TotalCredit);
                settlementBank.Balance = addBankTransaction.Sum(x => x.Balance);
                settlementBank.TotalDebitPercentage = addBankTransaction.Sum(x => x.TotalDebitPercentage);
                settlementBank.TotalCreditPercentage = addBankTransaction.Sum(x => x.TotalCreditPercentage);
                settlementBank.BalancePercentage = addBankTransaction.Sum(x => x.BalancePercentage);
                settlementBank.BankName = "TOTAL";
                addBankTransaction.Add(settlementBank);
                return addBankTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<UnAuthorizedBankTransaction> GetAllUnauthorizedBankTransactions(SearchBankTransactionCriteria searchBankTransaction)
        {
            try
            {
                var allBankTransaction =  _context.BankTransaction.Where(x=>x.UnAuthorized == false && x.AccountType == "PayOut" &&
                                                                         (string.IsNullOrEmpty(searchBankTransaction.Start) || x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.Start)) >= 0) &&
                                                                         (string.IsNullOrEmpty(searchBankTransaction.End) || x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.End)) <= 0) &&
                                                                         ((string.IsNullOrEmpty(searchBankTransaction.SiteCode)) || x.SiteCode == searchBankTransaction.SiteCode) &&
                                                                         ((string.IsNullOrEmpty(searchBankTransaction.MemberType)) || x.MemberType == searchBankTransaction.MemberType))

                                               .Select(n => new UnAuthorizedBankTransaction
                                               {
                                                   MemberCode = n.MemberCode,
                                                   AccountHolderName = n.AccountHolderName,
                                                   TransactionDate = n.TransactionDate,
                                                   BBF = n.BBF,
                                                   Credit = n.Credit,
                                                   Debit = n.Debit,
                                                   Balance = n.Balance
                                               })
                                                .AsNoTracking()
                                                .ToList();
                return allBankTransaction;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
         }
        public List<BankTransactionTopTen> GetBankTransactionTopTen(SearchBankTransactionCriteria searchBankTransaction)
        {
            
            try
            {
                List<BankTransactionTopTen> dataTen;
                switch (searchBankTransaction.AmountType)
                {
                    case 1:
                        dataTen = _context.BankTransactionTopTen.FromSqlRaw<BankTransactionTopTen>
                                            (@"Select AccountHolderName, sum(debit) as Amount
                                              from dbo.BankTransaction
                                              where 1=1 and debit > 0.00 and
                                              SiteCode = {0} and MemberType ={1} 
                                              and AccountType = {2} and
                                              TransactionDate >= {3} and TransactionDate <= {4}
                                               group by AccountHolderName
                                               order by Amount desc
                                               OFFSET 0 ROWS FETCH NEXT {5} ROWS ONLY",                                             
                                             searchBankTransaction.SiteCode,
                                             searchBankTransaction.MemberType,
                                             searchBankTransaction.AccountType,
                                             searchBankTransaction.DateFrom,
                                             searchBankTransaction.DateTo,
                                             searchBankTransaction.Top)
                                            .AsNoTracking()
                                            .ToList();
                        break;
                    case 2:
                        dataTen = _context.BankTransactionTopTen.FromSqlRaw<BankTransactionTopTen>
                                            (@"Select AccountHolderName, sum(credit) as Amount
                                              from dbo.BankTransaction
                                              where 1=1 and credit > 0.00 and
                                              SiteCode = {0} and MemberType ={1} 
                                              and AccountType = {2} and
                                              TransactionDate >= {3} and TransactionDate <= {4}
                                               group by AccountHolderName
                                               order by Amount desc
                                               OFFSET 0 ROWS FETCH NEXT {5} ROWS ONLY",
                                             searchBankTransaction.SiteCode,
                                             searchBankTransaction.MemberType,
                                             searchBankTransaction.AccountType,
                                             searchBankTransaction.DateFrom,
                                             searchBankTransaction.DateTo,
                                             searchBankTransaction.Top)
                                            .AsNoTracking()
                                            .ToList();                        
                        break;
                    default:                        
                        dataTen = null;
                        break;
                }


               
                return dataTen;

            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<BankTransactionMonthlyReport> GetBankTransactionMonthlyReportList(SearchBankTransactionCriteria searchBankTransaction)
        {
            List<BankTransactionMonthlyReport> addBankTransaction = new List<BankTransactionMonthlyReport>();
            try
            {
                var totalMonthTransaction = _context.BankTransaction.Include(x=>x.Site).Where(x => (x.UnAuthorized == true || x.UnAuthorized == false) &&
                                                                              (string.IsNullOrEmpty(searchBankTransaction.Start) ||
                                                                              x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.Start))>= 0) &&
                                                                              (string.IsNullOrEmpty(searchBankTransaction.End) ||
                                                                              x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.End)) <= 0))
                                                                                 .AsNoTracking()
                                                                                 .ToList();
                if (totalMonthTransaction.Count() == 0)
                {
                    return null;
                }
                var listOfPayIn = totalMonthTransaction.Where(x => x.AccountType == "PayIn").ToList();
                var totalPayIn = listOfPayIn.Select(x => new { x.AccountNo }).Distinct().Count();
                if (totalPayIn == 0)
                {
                    totalPayIn = 1;
                }
                var listOfPayOut = totalMonthTransaction.Where(x => x.AccountType == "PayOut").ToList();
                decimal totalPayOut = listOfPayOut.Select(x => new { x.AccountNo }).Distinct().Count();
                if (totalPayOut == 0)
                {
                    totalPayOut = 1;
                }
                BankTransactionMonthlyReport settlementBank = new BankTransactionMonthlyReport();
                var allBankTransactionList = totalMonthTransaction.Select(x => new { x.SiteCode,x.Site}).Distinct();
                
                foreach (var name in allBankTransactionList)
                {
                    settlementBank = new BankTransactionMonthlyReport();
                    settlementBank.BankName =  (searchBankTransaction.Lang == "et")?name.Site.Name:name.Site.NameEnglish;
                    var accontMemberPayIn = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Member" && n.AccountType == "PayIn").ToList();
                    settlementBank.MemberPayIn = accontMemberPayIn.Select(x => new { x.AccountNo }).Distinct().Count(); ;
                    var accontClinetPayIn = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Client" && n.AccountType == "PayIn").ToList();
                    settlementBank.ClientPayIn = accontClinetPayIn.Select(x => new { x.AccountNo }).Distinct().Count();
                    var accontNonMemberPayIn = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Non-Member" && n.AccountType == "PayIn").ToList();
                    settlementBank.NonMemberPayIn = accontNonMemberPayIn.Select(x => new { x.AccountNo }).Distinct().Count();
                    var accountMemberPayOut = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Member" && n.AccountType == "PayOut").ToList();
                    settlementBank.MemberPayOut = accountMemberPayOut.Select(x => new { x.AccountNo }).Distinct().Count();
                    var accountClientPayOut = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Client" && n.AccountType == "PayOut").ToList();
                    settlementBank.ClientPayOut = accountClientPayOut.Select(x => new { x.AccountNo}).Distinct().Count();
                    var accountNonMemberPayOut = totalMonthTransaction.Where(n => n.SiteCode == name.SiteCode && n.MemberType == "Non-Member" && n.AccountType == "PayOut").ToList();
                    settlementBank.NonMemberPayOut = accountNonMemberPayOut.Select(x=> new { x.AccountNo}).Distinct().Count();
                    settlementBank.TotalPayIn = settlementBank.MemberPayIn + settlementBank.ClientPayIn + settlementBank.NonMemberPayIn;
                    settlementBank.PayInPercentage = Convert.ToDecimal(String.Format("{0:00.00}", (settlementBank.TotalPayIn * 100)/totalPayIn));
                    settlementBank.TotalPayOut = settlementBank.MemberPayOut + settlementBank.ClientPayOut + settlementBank.NonMemberPayOut;
                    settlementBank.PayOutPercentage = Convert.ToDecimal(String.Format("{0:00.00}", (settlementBank.TotalPayOut * 100) / totalPayOut));
                    addBankTransaction.Add(settlementBank);
                }
                settlementBank = new BankTransactionMonthlyReport();
                settlementBank.MemberPayIn = addBankTransaction.Sum(x => x.MemberPayIn);
                settlementBank.ClientPayIn = addBankTransaction.Sum(x => x.ClientPayIn);
                settlementBank.NonMemberPayIn = addBankTransaction.Sum(x => x.NonMemberPayIn);
                settlementBank.MemberPayOut = addBankTransaction.Sum(x => x.MemberPayOut);
                settlementBank.ClientPayOut = addBankTransaction.Sum(x => x.ClientPayOut);
                settlementBank.NonMemberPayOut = addBankTransaction.Sum(x => x.NonMemberPayOut);
                settlementBank.TotalPayIn = addBankTransaction.Sum(x => x.TotalPayIn);
                settlementBank.PayInPercentage = addBankTransaction.Sum(x => x.PayInPercentage);
                settlementBank.TotalPayOut = addBankTransaction.Sum(x => x.TotalPayOut);
                settlementBank.PayOutPercentage = addBankTransaction.Sum(x => x.PayOutPercentage);
                settlementBank.BankName = searchBankTransaction.Lang == "et" ? "ድምር" : "Total";
                addBankTransaction.Add(settlementBank);
                return addBankTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
      public List<ViolationBankTransaction>  GetViolationTransaction(SearchBankTransactionCriteria searchBankTransaction)
        {
            try
            {
                List<ViolationBankTransaction> data;
                switch (searchBankTransaction.ViolationTypeId)
                {
                    case 1:
                        data = _context.ViolationBankTransaction.FromSqlRaw<ViolationBankTransaction>(@"
                              Select SiteCode,AccountHolderName,MemberCode,MemberType,AccountType,Credit,Debit from BankTransaction
                              where TransactionDate between {0} and {1} and
					           MemberCode in(Select ecx.Ecxcode from ExchangeActor as ecx
							                     join Cancellation as can on
												 ecx.ExchangeActorId = can.ExchangeActorId
							                     where ecx.Status = 64 and can.UpdatedDateTime between {2} and between {3})",
                                             DateTime.Parse(searchBankTransaction.Start),
                                             DateTime.Parse(searchBankTransaction.End),
                                             DateTime.Parse(searchBankTransaction.Start),
                                             DateTime.Parse(searchBankTransaction.End))

                                            .AsNoTracking()
                                            .ToList();

                        break;
                    case 2:
                        data = _context.ViolationBankTransaction.FromSqlRaw<ViolationBankTransaction>(@"
                              Select SiteCode,AccountHolderName,MemberCode,MemberType,AccountType,Credit,Debit from BankTransaction
                               where TransactionDate between {0} and {1} and
                                MemberCode in(Select ecx.Ecxcode from ExchangeActor as ecx
                                               join Injunction as inj on
			                                   ecx.ExchangeActorId = inj.ExchangeActorId 
			                                   Where inj.InjunctionStartDate between {2} and {3} and
					                            inj.InjunctionStatus = 1)",
                                              DateTime.Parse(searchBankTransaction.Start),
                                              DateTime.Parse(searchBankTransaction.End),
                                              DateTime.Parse(searchBankTransaction.Start),
                                              DateTime.Parse(searchBankTransaction.End)
                                              )
                                             .AsNoTracking()
                                             .ToList();
                        break;
                    case 3:
                        data = _context.ViolationBankTransaction.FromSqlRaw<ViolationBankTransaction>(@"
                              Select SiteCode,AccountHolderName,MemberCode,MemberType,AccountType,Credit,Debit from BankTransaction
                              where TransactionDate between {0} and  {1} and
					           MemberCode in(Select Ecxcode as ExpireDate from ExchangeActor
                                             Where(DATEDIFF(day,ExpireDateTime,{2}) < 485 ))",
                                             DateTime.Parse(searchBankTransaction.Start),
                                             DateTime.Parse(searchBankTransaction.End),
                                             DateTime.Parse(searchBankTransaction.Start))
                                            .AsNoTracking()
                                            .ToList();
                        break;

                    default:
                        data = null;
                        break;
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<DailySettlementTransactionReport> GetDailySettlementTransactionReport(SearchBankTransactionCriteria searchBankTransaction)
        {
            List<DailySettlementTransactionReport> addBankTransaction = new List<DailySettlementTransactionReport>();
            try
            {
                var allBankTransaction = _context.BankTransaction.Include(x=>x.Site).Where(x =>(String.IsNullOrEmpty(searchBankTransaction.SiteCode) || x.SiteCode.ToLower().TrimEnd() == searchBankTransaction.SiteCode.ToLower().TrimEnd()) &&
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.Start))>=0 &&
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.End))<=0)
                                                               .AsNoTracking()
                                                               .ToList();
                if(allBankTransaction.Count != 0)
                {
                    DailySettlementTransactionReport settlementBank = new DailySettlementTransactionReport();
                    var allBankTransactionList = allBankTransaction.Select(x => new { x.Site,x.SiteCode, x.TransactionDate, x.AccountHolderName, x.AccountNo }).Distinct();
                    foreach (var name in allBankTransactionList)
                    {
                        settlementBank = new DailySettlementTransactionReport();
                        settlementBank.BankName =(searchBankTransaction.Lang == "et")? name.Site.Name:name.Site.NameEnglish;
                        settlementBank.TransactionDate = name.TransactionDate.Date.ToString();
                        settlementBank.AccountHolderName = name.AccountHolderName;
                        settlementBank.AccountNo = name.AccountNo;
                        settlementBank.MemberDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                       x.AccountNo == name.AccountNo &&
                                                                                       x.AccountHolderName == name.AccountHolderName &&
                                                                                       x.TransactionDate == name.TransactionDate &&
                                                                                       x.MemberType == "Member" &&
                                                                                       x.AccountType == "PayIn").Sum(x => x.Debit);
                        settlementBank.ClientDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                        x.AccountNo == name.AccountNo &&
                                                                                        x.AccountHolderName == name.AccountHolderName &&
                                                                                        x.TransactionDate == name.TransactionDate &&
                                                                                        x.MemberType == "Client" &&
                                                                                        x.AccountType == "PayIn").Sum(x => x.Debit);
                        settlementBank.NonMembersDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                            x.AccountNo == name.AccountNo &&
                                                                                            x.AccountHolderName == name.AccountHolderName &&
                                                                                            x.TransactionDate == name.TransactionDate &&
                                                                                            x.MemberType == "Non-Member" &&
                                                                                            x.AccountType == "PayIn").Sum(x => x.Debit);
                        settlementBank.MemberCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                           x.AccountNo == name.AccountNo &&
                                                                                           x.AccountHolderName == name.AccountHolderName &&
                                                                                           x.TransactionDate == name.TransactionDate &&
                                                                                          x.MemberType == "Member" &&
                                                                                          x.AccountType == "PayOut").Sum(x => x.Credit);
                        settlementBank.ClientCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                          x.AccountNo == name.AccountNo &&
                                                                                          x.AccountHolderName == name.AccountHolderName &&
                                                                                          x.TransactionDate == name.TransactionDate &&
                                                                                          x.MemberType == "Client" &&
                                                                                          x.AccountType == "PayOut").Sum(x => x.Credit);
                        settlementBank.NonMembersCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode &&
                                                                                              x.AccountNo == name.AccountNo &&
                                                                                              x.AccountHolderName == name.AccountHolderName &&
                                                                                              x.TransactionDate == name.TransactionDate &&
                                                                                              x.MemberType == "Non-Member" &&
                                                                                              x.AccountType == "PayOut").Sum(x => x.Credit);
                        addBankTransaction.Add(settlementBank);

                    }
                    settlementBank = new DailySettlementTransactionReport();
                    settlementBank.BankName = searchBankTransaction.Lang == "et" ? "ጠቅላላ ድምር" : "Grand Total";
                    settlementBank.MemberDebitPayIn = addBankTransaction.Sum(x => x.MemberDebitPayIn);
                    settlementBank.ClientDebitPayIn = addBankTransaction.Sum(x => x.ClientDebitPayIn);
                    settlementBank.NonMembersDebitPayIn = addBankTransaction.Sum(x => x.NonMembersDebitPayIn);

                    settlementBank.MemberCreditPayOut = addBankTransaction.Sum(x => x.MemberCreditPayOut);
                    settlementBank.ClientCreditPayOut = addBankTransaction.Sum(x => x.ClientCreditPayOut);
                    settlementBank.NonMembersCreditPayOut = addBankTransaction.Sum(x => x.NonMembersCreditPayOut);
                    addBankTransaction.Add(settlementBank);

                    return addBankTransaction;
                }
                else
                {
                    return null;
                }
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<SettlementTransactionReportSummery> GetSettlementTransactionReportSummery(SearchBankTransactionCriteria searchBankTransaction)
        {
            List<SettlementTransactionReportSummery> addBankTransaction = new List<SettlementTransactionReportSummery>();
            try
            {
                var allBankTransaction = _context.BankTransaction.Include(x=>x.Site).
                                                                Where(x => (string.IsNullOrEmpty(searchBankTransaction.Start) ||
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.Start))>=0) &&
                                                               (string.IsNullOrEmpty(searchBankTransaction.End) ||
                                                               x.TransactionDate.Date.CompareTo(DateTime.Parse(searchBankTransaction.End))<=0))
                                                               .AsNoTracking()
                                                               .ToList();
                SettlementTransactionReportSummery settlementBank = new SettlementTransactionReportSummery();
                var allBankTransactionList = allBankTransaction.Select(x => new { x.SiteCode,x.Site }).Distinct();
                foreach (var name in allBankTransactionList)
                {
                    settlementBank = new SettlementTransactionReportSummery();
                    settlementBank.BankName = (searchBankTransaction.Lang == "et") ? name.Site.Name : name.Site.NameEnglish;
                    settlementBank.MemberDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Member" && x.AccountType == "PayIn").Sum(x => x.Debit);
                    settlementBank.ClientDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Client" && x.AccountType == "PayIn").Sum(x => x.Debit);
                    settlementBank.NonMembersDebitPayIn = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Non-Member" && x.AccountType == "PayIn").Sum(x => x.Debit);
                    settlementBank.MemberCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Member" && x.AccountType == "PayOut").Sum(x => x.Credit);
                    settlementBank.ClientCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Client" && x.AccountType == "PayOut").Sum(x => x.Credit);
                    settlementBank.NonMembersCreditPayOut = allBankTransaction.Where(x => x.SiteCode == name.SiteCode && x.MemberType == "Non-Member" && x.AccountType == "PayOut").Sum(x => x.Credit);
                    addBankTransaction.Add(settlementBank);

                }
                settlementBank = new SettlementTransactionReportSummery();
                settlementBank.BankName = searchBankTransaction.Lang == "et" ? "ድምር" : "Total";
                settlementBank.MemberDebitPayIn = addBankTransaction.Sum(x => x.MemberDebitPayIn);
                settlementBank.ClientDebitPayIn = addBankTransaction.Sum(x => x.ClientDebitPayIn);
                settlementBank.NonMembersDebitPayIn = addBankTransaction.Sum(x => x.NonMembersDebitPayIn);
                settlementBank.MemberCreditPayOut = addBankTransaction.Sum(x => x.MemberCreditPayOut);
                settlementBank.ClientCreditPayOut = addBankTransaction.Sum(x => x.ClientCreditPayOut);
                settlementBank.NonMembersCreditPayOut = addBankTransaction.Sum(x => x.NonMembersCreditPayOut);
                addBankTransaction.Add(settlementBank);
                return addBankTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
