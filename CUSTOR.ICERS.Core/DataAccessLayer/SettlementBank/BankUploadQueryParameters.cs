﻿using CUSTOR.ICERS.Core.EntityLayer.Common;

namespace CUSTOR.Oversight.DAL
{
    public class BankUploadQueryParameters : QueryParameters
    {

        public int BankUploadId { get; set; }

    }
}
