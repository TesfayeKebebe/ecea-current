﻿using AutoMapper;
using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.DAL.Repository.Ecx
{
    public class DelayRegisterPerBankRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public DelayRegisterPerBankRepository(ECEADbContext context, IMapper _mapper)
        {
            this.context = context;
            mapper = _mapper;
        }
        public async Task<List<DelayRegisterPerBankDTO>> GetDelayRegisterPerBankList(SearchEcxParameters qp)
        {
            try
            {

                var query = await context.DelayRegisterPerBank.Where(x => (string.IsNullOrEmpty(qp.DateFrom) || x.ReportDate.Date >= DateTime.Parse(qp.DateFrom).Date) &&
                                                                          (string.IsNullOrEmpty(qp.DateFrom) || x.ReportDate.Date <= DateTime.Parse(qp.DateTo).Date))
                    .Select(n => new DelayRegisterPerBankDTO
                    {
                        ReportDate = n.ReportDate,
                        BankName = n.BankName,
                        PayIn = n.PayIn,
                        PayOut = n.PayOut,
                        BalanceOne = n.BalanceOne,
                        BalanceTwo = n.BalanceTwo,
                        DelayRegisterPerBankId = n.DelayRegisterPerBankId,
                        Total = (n.PayIn + n.PayOut + n.BalanceOne + n.BalanceTwo)
                    })
                    .Paging(qp.PageCount, qp.PageNumber)
                    .ToListAsync();
                return query;

            }
            catch (Exception ex)
            {
                string str = ex.Message;
                throw new Exception(ex.Message);
            }

        }
    }
}
