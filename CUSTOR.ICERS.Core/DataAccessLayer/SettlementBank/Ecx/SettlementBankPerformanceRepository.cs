﻿using AutoMapper;
using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.DAL.Repository.Ecx
{
    public class SettlementBankPerformanceRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public SettlementBankPerformanceRepository(ECEADbContext context, IMapper _mapper)
        {
            this.context = context;
            mapper = _mapper;
        }

        public async Task<List<SettlementBankPerformanceDTO>> GetAllSettleBankPerformanceList(SearchEcxParameters qp)
        {
            try
            {
                var query = await context.SettlementBankPerformance
                                                     .Where(x => (string.IsNullOrEmpty(qp.DateFrom) || x.ReportDate.Date >= DateTime.Parse(qp.DateFrom).Date) &&
                                                     (string.IsNullOrEmpty(qp.DateTo) || x.ReportDate.Date <= DateTime.Parse(qp.DateTo).Date))
                                                     .Select(n => new SettlementBankPerformanceDTO
                                                     {
                                                         BankName = n.BankName,
                                                         PayInSLATimeInHours = n.PayInSLATimeInHours,
                                                         PayOutSLATimeInHours = n.PayOutSLATimeInHours,
                                                         BalanceOneSLAInHours = n.BalanceOneSLAInHours,
                                                         BalanceTwoSLAInHours = n.BalanceTwoSLAInHours,
                                                         BalanceOneAvg = n.BalanceOneAvg,
                                                         BalanceOneMax = n.BalanceOneMax,
                                                         BalanceOneMin = n.BalanceOneMin,
                                                         BalanceTwoAvg = n.BalanceTwoAvg,
                                                         BalanceTwoMax = n.BalanceTwoMax,
                                                         BalanceTwoMin = n.BalanceTwoMin,
                                                         PayInAvg = n.PayInAvg,
                                                         PayInMax = n.PayInMax,
                                                         PayInMin = n.PayInMin,
                                                         PayOutAvg = n.PayOutAvg,
                                                         PayOutMax = n.PayOutMax,
                                                         PayOutMin = n.PayOutMin                                                     
                                                     })
                    //.Paging(qp.PageCount, qp.PageNumber)
                    .ToListAsync();
                return query;

            }
            catch (Exception ex)
            {
                string str = ex.Message;
                throw new Exception(ex.Message);
            }

        }
    }
}

