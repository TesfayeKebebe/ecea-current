﻿using AutoMapper;
using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.DAL.DTO.ECX;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.DAL.Repository.Ecx
{
    public class SettlementGuarnatteFundRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper mapper;

        public SettlementGuarnatteFundRepository(ECEADbContext context, IMapper _mapper)
        {
            _context = context;
            mapper = _mapper;
        }

        public async Task<List<SettlementGuarnatteFundDTO>> GetAllSettlementGuaranteeFundList(SearchEcxParameters qp)
        {
            try
            {

                var query = await _context.SettlementGuarnatteFund
                    .Where(x => (string.IsNullOrEmpty(qp.SiteCode) || x.BankCode == qp.SiteCode) &&
                                (string.IsNullOrEmpty(qp.DateFrom) || x.ReportDate.Date >= DateTime.Parse(qp.DateFrom).Date) &&
                                (string.IsNullOrEmpty(qp.DateTo) || x.ReportDate.Date <= DateTime.Parse(qp.DateTo).Date))
                    .Select(n => new SettlementGuarnatteFundDTO
                    {
                        BankName = n.BankName,
                        Description = n.Description,
                        Sub_Description = n.Sub_Description,
                        ReportDate = n.ReportDate,
                        MemberName = n.MemberName,
                        Amount = n.Amount,
                        Remark = n.Remark,
                        BankCode = n.BankCode,
                        SettlementGuarnatteFundId = n.SettlementGuarnatteFundId
                    })

                    .ToListAsync();
                return query;

            }
            catch (Exception ex)
            {
                string str = ex.Message;
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<GuaranteeFundViewModel>> GetBankGuaranteeFundList(SearchEcxParameters qp)
        {
            try
            {
                List<GuaranteeFundViewModel> addGuaranteeFund = new List<GuaranteeFundViewModel>();
                var banklist = await _context.SettlementGuarnatteFund.Where(x => x.BankCode != "N/A" &&
                                                     (string.IsNullOrEmpty(qp.SiteCode) || x.BankCode == qp.SiteCode) &&
                                                     (string.IsNullOrEmpty(qp.DateFrom) || x.ReportDate >= DateTime.Parse(qp.DateFrom)) &&
                                                     (string.IsNullOrEmpty(qp.DateTo) || x.ReportDate <= DateTime.Parse(qp.DateTo)))
                                        .ToListAsync();
                var allBankGuarnteeFundList = banklist.Select(x => new { x.BankCode, x.BankName }).Distinct();
                GuaranteeFundViewModel settlementBank = new GuaranteeFundViewModel();
                foreach (var item in allBankGuarnteeFundList)
                {
                    settlementBank = new GuaranteeFundViewModel();
                    settlementBank.BankName = item.BankName;
                    settlementBank.Total = banklist.Where(x => x.BankCode == item.BankCode).Sum(x => x.Amount);
                    addGuaranteeFund.Add(settlementBank);
                }
                return addGuaranteeFund;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
