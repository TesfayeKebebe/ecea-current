﻿using CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank.Ecx
{
   public class WithdrawalTransactionReportRepository
    {
        private readonly ECEADbContext _context;

        public WithdrawalTransactionReportRepository(ECEADbContext context)
        {
            _context = context;
        }
        public List<SettlementTransactionMonthlyView> GetWithdrawalTransactionMontlyReport(SearchSqlQueryParms searchParms)
        {
            try
            {
                var reconcilationList = _context.SettlementTransactionMonthly.FromSqlRaw("EXECUTE dbo.pGetWithDrawalTransactionMonthly {0},{1},{2}",
                                                                                      searchParms.SiteCode, searchParms.From.Date, searchParms.To.Date).ToList();
                List<SettlementTransactionMonthlyView> listOfSettlementTransaction = new List<SettlementTransactionMonthlyView>();
                SettlementTransactionMonthlyView settlementTransaction = new SettlementTransactionMonthlyView();
                var allSettlementTransaction = reconcilationList.Select(x => new
                {
                    x.BankName,
                    x.AccountNumber,
                    x.MemberId,
                    x.OrganizationName,
                    x.TradeDate
                }).Distinct();
                foreach (var item in allSettlementTransaction)
                {
                    settlementTransaction = new SettlementTransactionMonthlyView();
                    settlementTransaction.BankName = item.BankName;
                    settlementTransaction.TradeDate = item.TradeDate;
                    settlementTransaction.OrganizationName = item.OrganizationName;
                    settlementTransaction.MemberId = item.MemberId;
                    settlementTransaction.AccountNumber = item.AccountNumber;

                    settlementTransaction.MemberPayIn = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountNumber == item.AccountNumber &&
                                                                                  x.AccountType == "Member Pay-In" && x.MemberId == item.MemberId &&
                                                                                  x.OrganizationName == item.OrganizationName &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.ClientPayIn = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountNumber == item.AccountNumber &&
                                                                                  x.AccountType == "Client Pay-In" && x.MemberId == item.MemberId &&
                                                                                  x.OrganizationName == item.OrganizationName &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.MemberPayOut = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                 x.AccountNumber == item.AccountNumber &&
                                                                                 x.AccountType == "Member Pay-Out" && x.MemberId == item.MemberId &&
                                                                                 x.OrganizationName == item.OrganizationName &&
                                                                                 x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.ClientPayOut = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountNumber == item.AccountNumber &&
                                                                                  x.AccountType == "Client Pay-Out" && x.MemberId == item.MemberId &&
                                                                                  x.OrganizationName == item.OrganizationName &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    listOfSettlementTransaction.Add(settlementTransaction);
                }
                return listOfSettlementTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<SettlementTransactionDailyView> GetWithdrawalTransactionDailyReport(SearchSqlQueryParms searchParms)
        {
            try
            {
                var reconcilationList = _context.SettlementTransactionDaily.FromSqlRaw("EXECUTE dbo.pGetWithdrawalTransactionDaily {0},{1},{2}",
                                                                                      searchParms.SiteCode, searchParms.From.Date, searchParms.To.Date).ToList();
                List<SettlementTransactionDailyView> listOfSettlementTransaction = new List<SettlementTransactionDailyView>();
                SettlementTransactionDailyView settlementTransaction = new SettlementTransactionDailyView();
                var allSettlementTransaction = reconcilationList.Select(x => new
                {
                    x.BankName,
                    x.TradeDate
                }).Distinct();
                foreach (var item in allSettlementTransaction)
                {
                    settlementTransaction = new SettlementTransactionDailyView();
                    settlementTransaction.BankName = item.BankName;
                    settlementTransaction.TradeDate = item.TradeDate;

                    settlementTransaction.MemberPayIn = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountType == "Member Pay-In" &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.ClientPayIn = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountType == "Client Pay-In" &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.MemberPayOut = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                 x.AccountType == "Member Pay-Out" &&
                                                                                 x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    settlementTransaction.ClientPayOut = reconcilationList.Where(x => x.BankName == item.BankName &&
                                                                                  x.AccountType == "Client Pay-Out" &&
                                                                                  x.TradeDate == item.TradeDate).Sum(x => x.Amount);
                    listOfSettlementTransaction.Add(settlementTransaction);
                }
                return listOfSettlementTransaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
