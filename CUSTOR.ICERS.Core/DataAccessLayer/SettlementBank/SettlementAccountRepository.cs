﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
  public class SettlementAccountRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public SettlementAccountRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public List<SettlementAccountDTO> GetBankSettlementAccountList(SearchBankTransactionCriteria searchBankTransaction)
        {
            try
            {
                var svgSettlement = context.SettlementAccount.Where(x => (String.IsNullOrEmpty(searchBankTransaction.SiteCode) || x.SiteCode.ToLower().TrimEnd() == searchBankTransaction.SiteCode.ToLower().TrimEnd()) &&
                                                                         (string.IsNullOrEmpty(searchBankTransaction.Start) || x.TransactionDate.Date >= DateTime.Parse(searchBankTransaction.Start).Date) &&
                                                                         (string.IsNullOrEmpty(searchBankTransaction.End) || x.TransactionDate.Date <= DateTime.Parse(searchBankTransaction.End).Date)
                                                                        )

                                    .Select(n => new SettlementAccountDTO
                                    {
                                        NameOfAccount = n.NameOfAccount,
                                        AccountNo = n.AccountNo,
                                        BBF = n.BBF,
                                        Debit = n.Debit,
                                        Credit = n.Credit,
                                        Balance = n.Balance
                                    })
                                   .AsNoTracking()
                                   .ToList();
                return svgSettlement;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
