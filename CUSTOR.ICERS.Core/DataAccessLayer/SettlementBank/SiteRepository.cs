﻿using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CUSTOR.Oversight.DAL
{
    public class SiteRepository
    {
        private readonly ECEADbContext _context;
        private readonly SiteSettings _settings;
        IHttpContextAccessor _httpAccessor;

        public SiteRepository(ECEADbContext context, IOptions<SiteSettings> settings,
                              IHttpContextAccessor httpAccessor)
        {
            _context = context;
            _settings = settings.Value;
            _httpAccessor = httpAccessor;
            _context.CurrentUserId = _httpAccessor.HttpContext?.User.FindFirst(ClaimConstants.Subject)?.Value?.Trim();

        }


        public async Task<bool> UpdateSite(Site site)
        {

            try
            {

                _context.Update(site);
                _context.Entry(site).Property(x => x.SiteId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }
        public async Task<bool> AddSite(Site site)
        {

            _context.Site.Add(site);
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<IEnumerable<Site>> GetAllSitesAsync(QueryParameters qp)
        {
            qp.PageCount = qp.PageCount == 0 ? _settings.DefaultPageSize : qp.PageCount;
            IEnumerable<Site> query = await _context.Site
                .Paging(qp.PageCount, qp.PageNumber)
                .ToListAsync();
            return query;

        }

        public async Task<IEnumerable<Site>> GetAllSitesList()
        {
            var sites = await _context.Site
            .Where(s => s.IsActive == true)
            .ToListAsync();
            return sites;
        }
        public async Task<IEnumerable<StaticData7>> GetSitesList(string lang, bool isActive)
        {
            // To-do - Add Caching
            IEnumerable<StaticData7> sites = await _context.Site
            .Where(s => s.IsActive == isActive)
            .Select(s => new StaticData7
            {
                Id = s.SiteCode,
                Description = (lang == "et") ? s.Name : s.NameEnglish
            })
            .ToListAsync();

            return sites;
        }

        public async Task<Site> GetSiteAsync(string siteCode)
        {
            var site = await _context.Site.Where(s => s.SiteCode == siteCode).FirstOrDefaultAsync();
            return site;
        }

        public async Task<bool> DeleteSiteAsync(string siteId)
        {
            Site site = await GetSiteAsync(siteId);

            if (site != null)
            {
                _context.Site.Remove(site);

                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}