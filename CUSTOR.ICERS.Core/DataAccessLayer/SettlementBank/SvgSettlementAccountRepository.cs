﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CUSTOR.ICERS.Core.DataAccessLayer.SettlementBank
{
    public class SvgSettlementAccountRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly IOptions<BankTransSettings> _settings;
        private readonly IHttpContextAccessor _httpAccessor;

        public SvgSettlementAccountRepository(ECEADbContext context, IMapper mapper, 
                                              IOptions<BankTransSettings> settings,
                                              IHttpContextAccessor httpAccessor)
        {
            _context = context;
            _mapper = mapper;
            _settings = settings;
            _httpAccessor = httpAccessor;
            _context.CurrentUserId = _httpAccessor.HttpContext?.User.FindFirst(ClaimConstants.Subject)?.Value?.Trim();
            _context.AutoSaveAuditFields = true;
        }

        public List<SvgSettlementAccountViewModel> GetBankSVGandSettlementAccountList(SearchBankTransactionCriteria searchBankTransaction)
        {
            try
            {
                var svgSettlement = _context.SvgSettlementAccount.Where(x=>(String.IsNullOrEmpty(searchBankTransaction.SiteCode) || x.SiteCode.ToLower().TrimEnd() == searchBankTransaction.SiteCode.ToLower().TrimEnd()) &&
                                                                          (string.IsNullOrEmpty(searchBankTransaction.Start) || x.CreatedDateTime.Date >= DateTime.Parse(searchBankTransaction.Start).Date) &&
                                                                          (string.IsNullOrEmpty(searchBankTransaction.End) || x.CreatedDateTime.Date <= DateTime.Parse(searchBankTransaction.End).Date)
                                                                        )

                                    .Select(n => new SvgSettlementAccountViewModel
                                    {
                                      NameOfAccount = n.NameOfAccount,
                                      AccountNo = n.AccountNo,
                                      BBF = n.BBF,
                                      Debit = n.Debit,
                                      Credit = n.Credit,
                                      Balance = n.Balance

                                    })
                                   .AsNoTracking()
                                   .ToList();
                return svgSettlement;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       
    }
}
