﻿using CUSTOR.ICERS.Core.EntityLayer.Trade;
using CUSTOR.ICERS.Core.EntityLayer.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Trade
{
    public class TradeRepository
    {
        private readonly ECEADbContext _context;

        public TradeRepository(ECEADbContext context)
        {
            _context = context;
        }

        public IEnumerable<TradeViewModel> GetTradeInformations(TradeSearchCritira tradeSearchCriteria)
        {
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@from",
                            SqlDbType =  System.Data.SqlDbType.Date,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = tradeSearchCriteria.From
                        },
                        new SqlParameter() {
                            ParameterName = "@to",
                            SqlDbType =  System.Data.SqlDbType.Date,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = tradeSearchCriteria.To
                        }};

            var trade = _context.TradeViewModels.FromSqlRaw("exec uspGetTradeInformation @from, @to", param).ToList();

            return trade;
        }
    }
}
