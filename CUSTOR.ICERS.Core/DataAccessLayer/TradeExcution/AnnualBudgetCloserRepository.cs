﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class AnnualBudgetCloserRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public AnnualBudgetCloserRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> Create(AnnualBudgetCloserNewDTO annualBudgetCloser)
        {
          

            try
            {   
                AnnualBudgetCloser newAnnualBug = mapper.Map<AnnualBudgetCloser>(getconverted(annualBudgetCloser));
                context.Add(newAnnualBug);
                await context.SaveChangesAsync();
                return newAnnualBug.AnnualBudgetCloserId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<AnnualBudgetCloserView>> GetAnnualBudgetCloserList(string lang)
        {

            try
            {
                var listOfReportPeriod = await context.AnnualBudgetCloser.Where(cd => cd.IsDeleted ==false)
                    .Select(n => new AnnualBudgetCloserView
                    {
                        AnnualBudgetCloserId = n.AnnualBudgetCloserId,
                        Description = lang == "et" ? n.DescriptionAmh : n.DescriptionEng,
                        EndDate = n.EndDate,
                        StartDate = n.StartDate,
                        DescriptionAmh = n.DescriptionAmh,
                        DescriptionEng = n.DescriptionEng,
                        BeginningDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.StartDate.Date).ToString()) : n.StartDate.Date.ToString(),
                        DeadLine = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.EndDate.Date).ToString()) : n.EndDate.Date.ToString(),
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return listOfReportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<AnnualBudgetCloserViewModel>> GetAnnualBudgetCloser(string lang)
        {
            try
            {
                List<AnnualBudgetCloserViewModel> reportPeriod = null;
                reportPeriod = await context.AnnualBudgetCloser
                    .Select(n => new AnnualBudgetCloserViewModel
                    {
                        Id = n.AnnualBudgetCloserId,
                        Description = lang == "et" ? n.DescriptionAmh : n.DescriptionEng

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return reportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Update(AnnualBudgetCloserNewDTO annualBudgetCloser)
        {
            try
            {
                var annualBudget = await context.AnnualBudgetCloser
                    .Where(x => x.AnnualBudgetCloserId == annualBudgetCloser.AnnualBudgetCloserId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (annualBudget != null)
                {
                    annualBudgetCloser.IsDeleted = annualBudget.IsDeleted;
                    annualBudgetCloser.IsActive = annualBudget.IsActive;
                    annualBudgetCloser.CreatedUserId = annualBudget.CreatedUserId;
                    annualBudgetCloser.UpdatedDateTime = DateTime.Now;
                    annualBudgetCloser.CreatedDateTime = annualBudget.CreatedDateTime;
                    var updateReportPeriod = mapper.Map(getconverted(annualBudgetCloser), annualBudget);
                    context.Entry(updateReportPeriod).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return annualBudget.AnnualBudgetCloserId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<AnnualBudgetCloser> GetAnnualBudgetCloserId(int annualBudgetCloserId)
        {
            try
            {
                var annualBudgetCloser = await context.AnnualBudgetCloser
                    .Where(x => x.AnnualBudgetCloserId == annualBudgetCloserId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return annualBudgetCloser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeleteAnnualBuget(int annualBudgetCloserId)
        {
            try
            {
                var oldannualBudgetCloserId = await context.AnnualBudgetCloser.FirstOrDefaultAsync(Inj =>
                    Inj.AnnualBudgetCloserId == annualBudgetCloserId);
                if (oldannualBudgetCloserId != null)
                {
                    oldannualBudgetCloserId.IsDeleted = true;
                    await context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public AnnualBudgetCloserDTO getconverted(AnnualBudgetCloserNewDTO annualBudgetCloserId)
        {
            AnnualBudgetCloserDTO masterData = new AnnualBudgetCloserDTO
            {
                EndDate = DateTime.Parse(annualBudgetCloserId.EndDate),
                StartDate= DateTime.Parse(annualBudgetCloserId.StartDate),
                DescriptionAmh= annualBudgetCloserId.DescriptionAmh,
                DescriptionEng=annualBudgetCloserId.DescriptionEng,
                AnnualBudgetCloserId= annualBudgetCloserId.AnnualBudgetCloserId,
                CreatedUserId=annualBudgetCloserId.CreatedUserId,
                IsDeleted=false,
                IsActive=true,
                UpdatedUserId=annualBudgetCloserId.UpdatedUserId,


            };

            return masterData;
        }
    }
}
