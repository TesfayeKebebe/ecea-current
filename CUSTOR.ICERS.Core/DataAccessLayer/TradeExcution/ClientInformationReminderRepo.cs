﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class ClientInformationReminderRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public ClientInformationReminderRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> Create(ClientInformationReminderDTO reportReminderDTO)
        {
            try
            {
                ClientInformationReminder financialReport = mapper.Map<ClientInformationReminder>(reportReminderDTO);
                context.Add(financialReport);
                await context.SaveChangesAsync();
                return financialReport.ClientInformationReminderId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Update(ClientInformationReminderDTO financialReport)
        {
            try
            {
                var financialReminder = await context.ClientInformationReminder
                          .Where(x => x.ClientInformationReminderId == financialReport.ClientInformationReminderId)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                if (financialReminder != null)
                {
                    financialReport.IsDeleted = financialReminder.IsDeleted;
                    financialReport.IsActive = financialReminder.IsActive;
                    financialReport.CreatedUserId = financialReminder.CreatedUserId;
                    financialReport.UpdatedDateTime = DateTime.Now;
                    financialReport.CreatedDateTime = financialReminder.CreatedDateTime;
                    var updateFinancialReminder = mapper.Map(financialReport, financialReminder);
                    context.Entry(updateFinancialReminder).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return financialReminder.ClientInformationReminderId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Delete(ClientInformationReminderDTO financialReport)
        {
            try
            {
                var financialReminder = await context.ClientInformationReminder
                          .Where(x => x.ClientInformationReminderId == financialReport.ClientInformationReminderId)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                if (financialReminder != null)
                {
                    var updateFinancialReminder = mapper.Map(financialReport, financialReminder);
                    context.Entry(updateFinancialReminder).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return financialReminder.ClientInformationReminderId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ClientInformationReminder> GetClientInformationReminderId(int financialId)
        {
            try
            {
                var financialAmount = await context.ClientInformationReminder
                                                    .Where(x => x.ClientInformationReminderId == financialId)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ClientInformationReminderDTO>> GetClientInformationReminderList(string lang)
        {
            try
            {
                var financialAmount = await context.ClientInformationReminder
                                                   .Select(n => new ClientInformationReminderDTO
                                                   {
                                                       ClientInformationReminderId = n.ClientInformationReminderId,
                                                       DescriptionAmh = (lang == "et") ? n.DescriptionAmh : n.DescriptionEng
                                                   })
                                                   .AsNoTracking()
                                                   .ToListAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}