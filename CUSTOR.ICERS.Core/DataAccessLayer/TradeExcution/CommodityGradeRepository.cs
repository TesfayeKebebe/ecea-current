﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public partial class CommodityGradeRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public CommodityGradeRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }

        //public async Task<IEnumerable<CommodityGradeDTO>> GetCommudityGradeList(string lang)
        //{
        //    IEnumerable<CommodityGradeDTO> commodityList = null;
        //    try
        //    {
        //        commodityList = await context.CommodityGrade
        //                                    .Select(n => new CommodityGradeDTO
        //                                    {
        //                                        Id = n.CommodityGradeId,
        //                                        Description = lang == "et" ? n.DescriptionAmh : n.DescriptoinEng
        //                                    })
        //                                    .AsNoTracking()
        //                                    .ToListAsync();
        //        return commodityList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        public async Task<IEnumerable<StaticData2>> GetCommodityGradeById(int commodityTypeId, string lang)
        {
            IEnumerable<StaticData2> commodityGradeList = null;
            try
            {
                commodityGradeList = await context.CommodityGrade.Where(x => x.CommodityTypeId == commodityTypeId)
                                                                  .Select(n => new StaticData2
                                                                  {
                                                                      Id = n.CommodityGradeId,
                                                                      ParentId = n.CommodityTypeId,
                                                                      Description = lang == "et" ? n.DescriptionAmh : n.DescriptoinEng

                                                                  })
                                                                  .AsNoTracking()
                                                                  .ToListAsync();
                return commodityGradeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CommodityGradeByIdDTO> GetCommodityGradeByIda(int commoditygradeId, string lang)
        {
            CommodityGradeByIdDTO commodityGradeList = null;
            try
            {
                commodityGradeList = await (from osr in context.CommodityGrade
                                            where (osr.CommodityGradeId == commoditygradeId)
                                            join lo in context.CommodityType
                                            on osr.CommodityTypeId equals lo.CommodityTypeId
                                            join ct in context.Commodity
                                            on lo.CommodityId equals ct.CommodityId
                                            select new CommodityGradeByIdDTO
                                            {
                                                CommodityGradeId = osr.CommodityGradeId,
                                                CommodityTypeId = osr.CommodityTypeId,
                                                CommodityId = ct.CommodityId,
                                                DescriptionAmh = osr.DescriptionAmh,
                                                DescriptoinEng = osr.DescriptoinEng,
                                                CommodityName = lang == "et" ? ct.DescriptionAmh : ct.DesciptionEng,
                                                Description = lang == "et" ? lo.DescriptionAmh : lo.DescriptionEng

                                            }).FirstOrDefaultAsync();

                return commodityGradeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Create(CommodityGradeDTO commodity)
        {
            try
            {
                CommodityGrade newCommodity = mapper.Map<CommodityGrade>(commodity);
                context.Add(newCommodity);
                await context.SaveChangesAsync();
                return newCommodity.CommodityGradeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<int> Update(CommodityGradeDTO commodityType)
        {
            try
            {
                var commodity = await context.CommodityGrade
                    .Where(x => x.CommodityGradeId == commodityType.CommodityGradeId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (commodity != null)
                {

                    commodityType.IsDeleted = commodity.IsDeleted;
                    commodityType.IsActive = commodity.IsActive;
                    commodityType.CreatedUserId = commodity.CreatedUserId;
                    commodityType.UpdatedDateTime = DateTime.Now;
                    commodityType.CreatedDateTime = commodity.CreatedDateTime;
                    var updateReportPeriod = mapper.Map(commodityType, commodity);
                    context.Entry(updateReportPeriod).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return commodity.CommodityGradeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Delete(CommodityGradeDTO commodity)
        {
            try
            {
                var commodityDelet = await context.CommodityGrade
                                    .Where(x => x.CommodityGradeId == commodity.CommodityGradeId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (commodityDelet != null)
                {
                    var deleteReportPeriod = mapper.Map(commodity, commodityDelet);
                    context.Entry(deleteReportPeriod).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return commodityDelet.CommodityGradeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<CommodityGradeDTO>> GetAllCommodityList(string lang)
        {
            try
            {
                var listOfReportPeriod = await context.CommodityGrade
                    .Select(n => new CommodityGradeDTO
                    {
                        CommodityGradeId = n.CommodityGradeId,
                        DescriptionAmh = lang == "et" ? n.DescriptionAmh : n.DescriptoinEng,
                        CommodityName = lang == "et" ? n.CommodityType.Commodity.DescriptionAmh : n.CommodityType.Commodity.DesciptionEng,
                        CommodityTypeName = lang == "et" ? n.CommodityType.DescriptionAmh : n.CommodityType.DescriptionEng
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return listOfReportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CommodityGrade> GetCommodityById(int commodityId)
        {
            try
            {

                var reportPrd = await context.CommodityGrade
                    .Where(x => x.CommodityGradeId == commodityId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
