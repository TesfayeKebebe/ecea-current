﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public partial class CommodityTypeRepositoory
    {
        private readonly ECEADbContext _context;
        private readonly IMapper mapper;

        public CommodityTypeRepositoory(ECEADbContext context, IMapper _mapper)
        {
            _context = context;
            mapper = _mapper;
        }

        public async Task<IEnumerable<StaticData2>> GetCommodityTypeById(int commodityId, string lang)
        {
            IEnumerable<StaticData2> commodityTypeList = null;
            try
            {
                commodityTypeList = await _context.CommodityType.Where(c => c.CommodityId == commodityId)
                                                                .Select(x => new StaticData2
                                                                {

                                                                    Id = x.CommodityTypeId,
                                                                    ParentId = x.CommodityId,
                                                                    Description = lang == "et" ? x.DescriptionAmh : x.DescriptionEng
                                                                })
                                                                .AsNoTracking()
                                                                .ToListAsync();

                return commodityTypeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Create(CommodityTypeDTO commodity)
        {
            try
            {
                CommodityType newCommodity = mapper.Map<CommodityType>(commodity);
                _context.Add(newCommodity);
                await _context.SaveChangesAsync();
                return newCommodity.CommodityTypeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<int> Update(CommodityTypeDTO commodityType)
        {
            try
            {
                var commodity = await _context.CommodityType
                    .Where(x => x.CommodityTypeId == commodityType.CommodityTypeId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (commodity != null)
                {
                    commodityType.IsDeleted = commodity.IsDeleted;
                    commodityType.IsActive = commodity.IsActive;
                    commodityType.CreatedUserId = commodity.CreatedUserId;
                    commodityType.UpdatedDateTime = DateTime.Now;
                    commodityType.CreatedDateTime = commodity.CreatedDateTime;
                    var updateReportPeriod = mapper.Map(commodityType, commodity);
                    _context.Entry(updateReportPeriod).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                return commodity.CommodityId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Delete(CommodityTypeDTO commodity)
        {
            try
            {
                var commodityDelet = await _context.CommodityType
                                    .Where(x => x.CommodityTypeId == commodity.CommodityTypeId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (commodityDelet != null)
                {

                    var deleteReportPeriod = mapper.Map(commodity, commodityDelet);
                    _context.Entry(deleteReportPeriod).State = EntityState.Deleted;
                    await _context.SaveChangesAsync();
                }
                return commodityDelet.CommodityId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<CommodityTypeDTO>> GetAllCommodityList(string lang)
        {
            try
            {
                var listOfReportPeriod = await _context.CommodityType
                    .Select(n => new CommodityTypeDTO
                    {
                        CommodityTypeId = n.CommodityTypeId,
                        CommodityName = lang == "et" ? n.Commodity.DescriptionAmh : n.Commodity.DesciptionEng,
                        DescriptionAmh = lang == "et" ? n.DescriptionAmh : n.DescriptionEng,


                    })
                    .AsNoTracking()
                    .ToListAsync();
                return listOfReportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<CommodityType> GetCommodityById(int commodityId)
        {
            try
            {
                var reportPrd = await _context.CommodityType
                    .Where(x => x.CommodityTypeId == commodityId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
