﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class ExchangeActorFinanicialRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public ExchangeActorFinanicialRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> Create(ExchangeActorFinanicialDTO exchangeActorFinanicial)
        {
            try
            {
                var financialPerformance = await context.ExchangeActorFinanicial
                    .Where(x => x.CustomerTypeId == exchangeActorFinanicial.CustomerTypeId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (financialPerformance == null)
                {
                    ExchangeActorFinanicial newActorFinanicial = mapper.Map<ExchangeActorFinanicial>(exchangeActorFinanicial);
                    context.Add(newActorFinanicial);
                    await context.SaveChangesAsync();
                    return newActorFinanicial.ExchangeActorFinanicialId;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Update(ExchangeActorFinanicialDTO exchangeActorFinanicial)
        {
            try
            {
                var financialAmount = await context.ExchangeActorFinanicial
                    .Where(x => x.ExchangeActorFinanicialId == exchangeActorFinanicial.ExchangeActorFinanicialId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (financialAmount != null)
                {
                    var updateFinancialAmount = mapper.Map(exchangeActorFinanicial, financialAmount);
                    context.Entry(updateFinancialAmount).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return financialAmount.ExchangeActorFinanicialId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Delete(ExchangeActorFinanicialDTO exchangeActorFinanicial)
        {
            try
            {
                var financialAmount = await context.ExchangeActorFinanicial
                                    .Where(x => x.ExchangeActorFinanicialId == exchangeActorFinanicial.ExchangeActorFinanicialId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (financialAmount != null)
                {
                    var updateFinancialAmount = mapper.Map(exchangeActorFinanicial, financialAmount);
                    updateFinancialAmount.IsDeleted = true;

                    context.Entry(updateFinancialAmount).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return financialAmount.ExchangeActorFinanicialId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<ExchangeActorFinanicial> GetFinancialAmountById(int financialId)
        {
            try
            {
                var financialAmount = await context.ExchangeActorFinanicial
                                                    .Where(x => x.ExchangeActorFinanicialId == financialId)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ExchangeActorFinanicialDTO>> GetAllFinancialList()
        {
            try
            {
                var financialAmount = await context.ExchangeActorFinanicial
                                                   .Select(n => new ExchangeActorFinanicialDTO
                                                   {
                                                       ExchangeActorFinanicialId = n.ExchangeActorFinanicialId,
                                                       CustomerTypeId = n.CustomerTypeId,
                                                       Amount = n.Amount
                                                   })
                                                   .AsNoTracking()
                                                   .ToListAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<FinancialPerformanceViewModel>> GetListOfCustomerType(string lang)
        {
            try
            {
                List<FinancialPerformanceViewModel> performanceData = await (from c in context.ExchangeActorFinanicial
                                                                             join l in context.Lookup
                                                                        on c.CustomerTypeId equals l.LookupId
                                                                             select new FinancialPerformanceViewModel
                                                                             {
                                                                                 Id = c.ExchangeActorFinanicialId,
                                                                                 Description = (lang == "et") ? l.DescriptionAmh : l.DescriptionEng

                                                                             }).AsNoTracking().ToListAsync();
                return performanceData;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<StaticData5> GetFinancialPerformanceById(int finanicalPerformanceId)
        {

            try
            {
                var commodityGradeList = await context.ExchangeActorFinanicial
                                                       .Where(x => x.ExchangeActorFinanicialId == finanicalPerformanceId)
                                                                    .Select(n => new StaticData5
                                                                    {
                                                                        Id = n.ExchangeActorFinanicialId,
                                                                        ParentId = n.CustomerTypeId,
                                                                        Amount = n.Amount,
                                                                        CustomerTypeId = n.CustomerTypeId

                                                                    })
                                                                    .AsNoTracking()
                                                                    .FirstOrDefaultAsync();
                return commodityGradeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
