﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
   public class FinancialAuditoredFileUploadRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public FinancialAuditoredFileUploadRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<List<FinancialAuditoredFileUploadView>> ListUploadFileOfAuditored(string memberFinancialAuditoredId)
        {
            try
            {
                var uploadList = await context.FinancialAuditoredFileUpload.Where(
                                 x => x.MemberFinancialAuditorId == memberFinancialAuditoredId)
                    .Select(n => new FinancialAuditoredFileUploadView
                    {
                       // MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                        Url = n.Url
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return uploadList;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<FinancialAuditoredFileUploadView>> OneUploadFileOfAuditored(int memberFinancialAuditoredId)
        {
            try
            {
                var uploadList = await context.FinancialAuditoredFileUpload.Where(
                                 x => x.FinancialAuditoredFileUploadId == memberFinancialAuditoredId)
                    .Select(n => new FinancialAuditoredFileUploadView
                    {
                        // MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                        Url = n.Url
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return uploadList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<PagedResult<AuditedAnnualDocument>> GetAllAnnualAuditedDocument(ClientTradeQueryParameters queryParameters)
        {
            try
            {
                var auditFileUpload = await context.FinancialAuditoredFileUpload.Where(
                                              x=>x.MemberFinancialAuditor.TradeExcutionStatusTypeId== 1 &&
                                              x.MemberFinancialAuditor.ExchangeActorId == queryParameters.ExchangeActorId)
                        .Select(n => new AuditedAnnualDocument
                        {
                            FinancialAuditoredFileUploadId = n.FinancialAuditoredFileUploadId,
                            MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                            FileName = n.FileName,
                            CreatedDate = (queryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) :
                                                      n.CreatedDateTime.Date.ToString(),
                        })
                        .AsQueryable().Paging(queryParameters.PageCount, queryParameters.PageNumber)
                     .AsNoTracking()
                     .ToListAsync();
               int totalResultList = auditFileUpload.Count();
                return new PagedResult<AuditedAnnualDocument>()
                {
                    Items = mapper.Map<List<AuditedAnnualDocument>>(auditFileUpload),
                    ItemsCount = context.FinancialAuditoredFileUpload.Where(
                                              x => x.MemberFinancialAuditor.TradeExcutionStatusTypeId == 1 &&
                                              x.MemberFinancialAuditor.ExchangeActorId == queryParameters.ExchangeActorId).Count()
                };
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<AuditedAnnualDocument>> GetMemberAnnualFileUploadReport(ClientTradeQueryParameters queryParameters)
        {
            try
            {
                if (queryParameters.CustomerTypeId == 72 || queryParameters.CustomerTypeId == 88)
                {
                    var auditFileUpload = await context.FinancialAuditoredFileUpload.Where(
                                                                                  f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                                  ((string.IsNullOrEmpty(queryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(queryParameters.ECXCode)) &&
                                                                                  ((queryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == queryParameters.AnnualBudgetCloserId)) &&
                                                                                  ((queryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == queryParameters.FieldBusinessId)) &&
                                                                                  ((queryParameters.CustomerTypeId == 0) || (f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == queryParameters.CustomerTypeId)) &&
                                                                                   ((queryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == queryParameters.Year))
                                                                                  )
                        .Select(n => new AuditedAnnualDocument
                        {
                            FinancialAuditoredFileUploadId = n.FinancialAuditoredFileUploadId,
                            MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                            FileName = n.FileName,
                            CreatedDate = (queryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) :
                                                      n.CreatedDateTime.Date.ToString(),
                        })
                    .AsNoTracking()
                     .ToListAsync();
                    return auditFileUpload;
                }
                else if(queryParameters.CustomerTypeId == 6 || queryParameters.CustomerTypeId == 90 || queryParameters.CustomerTypeId == 5)
                {
                    var auditFileUpload = await context.FinancialAuditoredFileUpload.Where(
                                                                        f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                       ((string.IsNullOrEmpty(queryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(queryParameters.ECXCode)) &&
                                                                       ((queryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == queryParameters.AnnualBudgetCloserId)) &&
                                                                       ((queryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == queryParameters.FieldBusinessId)) &&
                                                                       ((queryParameters.CustomerTypeId == 0) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == queryParameters.CustomerTypeId)) &&
                                                                       ((queryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == queryParameters.Year))
                                                                        )
                                            .Select(n => new AuditedAnnualDocument
                                            {
                                                FinancialAuditoredFileUploadId = n.FinancialAuditoredFileUploadId,
                                                MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                                                FileName = n.FileName,
                                                CreatedDate = (queryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) :
                                                                          n.CreatedDateTime.Date.ToString(),
                                            })
                                         .AsNoTracking()
                                         .ToListAsync();
                    return auditFileUpload;
                }
                else
                {
                    var auditFileUpload = await context.FinancialAuditoredFileUpload.Where(
                                                                                        f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                                        ((f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == 6) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == 5) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == 90)) &&
                                                                                       ((string.IsNullOrEmpty(queryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(queryParameters.ECXCode)) &&
                                                                                       ((queryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == queryParameters.AnnualBudgetCloserId)) &&
                                                                                       ((queryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == queryParameters.FieldBusinessId)) &&                                                                                                      
                                                                                       ((queryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == queryParameters.Year))
                                                                                                      )
                                            .Select(n => new AuditedAnnualDocument
                                            {
                                                FinancialAuditoredFileUploadId = n.FinancialAuditoredFileUploadId,
                                                MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                                                FileName = n.FileName,
                                                CreatedDate = (queryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime.Date).ToString()) :
                                                                          n.CreatedDateTime.Date.ToString(),
                                            })
                                         .AsNoTracking()
                                         .ToListAsync();
                    return auditFileUpload;
                }

                    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> DeleteAuditorFileUpload(FinancialAuditoredFileUploadDTO fileUpload)
        {
            try
            {
                var tradeDetail = await context.FinancialAuditoredFileUpload
                    .Where(x => x.FinancialAuditoredFileUploadId == fileUpload.FinancialAuditoredFileUploadId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (tradeDetail != null)
                {
                    var deleteTradeExcution = mapper.Map(fileUpload, tradeDetail);
                    context.Entry(deleteTradeExcution).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return tradeDetail.FinancialAuditoredFileUploadId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
