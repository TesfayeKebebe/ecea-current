﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class FinancialReportReminderRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public FinancialReportReminderRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> Create(FinancialReportReminderDTO reportReminderDTO)
        {
            try
            {
                FinancialReportReminder financialReport = mapper.Map<FinancialReportReminder>(reportReminderDTO);
                context.Add(financialReport);
                await context.SaveChangesAsync();
                return financialReport.FinancialReportReminderId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Update(FinancialReportReminderDTO financialReport)
        {
            try
            {
                var financialReminder = await context.FinancialReportReminder
                          .Where(x => x.FinancialReportReminderId == financialReport.FinancialReportReminderId)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                if (financialReminder != null)
                {
                    financialReport.IsDeleted = financialReminder.IsDeleted;
                    financialReport.IsActive = financialReminder.IsActive;
                    financialReport.CreatedUserId = financialReminder.CreatedUserId;
                    financialReport.UpdatedDateTime = DateTime.Now;
                    financialReport.CreatedDateTime = financialReminder.CreatedDateTime;
                    var updateFinancialReminder = mapper.Map(financialReport, financialReminder);
                    context.Entry(updateFinancialReminder).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return financialReminder.FinancialReportReminderId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> Delete(FinancialReportReminderDTO financialReport)
        {
            try
            {
                var financialReminder = await context.FinancialReportReminder
                          .Where(x => x.FinancialReportReminderId == financialReport.FinancialReportReminderId)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                if (financialReminder != null)
                {

                    var updateFinancialReminder = mapper.Map(financialReport, financialReminder);
                    context.Entry(updateFinancialReminder).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return financialReminder.FinancialReportReminderId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<FinancialReportReminder> GetFinancialReportReminderId(int financialId)
        {
            try
            {
                var financialAmount = await context.FinancialReportReminder
                                                    .Where(x => x.FinancialReportReminderId == financialId)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<FinancialReportReminderDTO>> GetFinancialReportReminderList(string lang)
        {
            try
            {
                var financialAmount = await context.FinancialReportReminder
                                                   .Select(n => new FinancialReportReminderDTO
                                                   {
                                                       FinancialReportReminderId = n.FinancialReportReminderId,
                                                       DescriptionAmh = (lang == "et") ? n.DescriptionAmh : n.DescriptionEng
                                                   })
                                                   .AsNoTracking()
                                                   .ToListAsync();
                return financialAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
