﻿using AutoMapper;
using CUSTOR.ICERS.API.Controllers.TradeExcution;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
   public class MemberAuditorRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public MemberAuditorRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<MemberAuditorAgreementView>> GetAllMemberAuditorAgrement(OffSiteMonitoringQueryParameters notificationQuery)
        {
            
            try
            {                
                
             var  dataList = await _context.MemberAuditorAgrement
                    .Select(c => new MemberAuditorAgreementView
                    {
                        id = c.MemberAuditorAgrementId,
                        EcxCode = c.EcxCode,
                        OrganiztionName = notificationQuery.Lang == "et" ? c.ExchangeActor.OrganizationNameAmh:c.ExchangeActor.OrganizationNameEng,
                        AuditorName = (from audit in _context.ExchangeActor where audit.Ecxcode == c.EcxCode
                                                    select new AuditorName
                                                    {
                                                        AuditorNames = notificationQuery.Lang == "et" ? audit.OrganizationNameAmh: audit.OrganizationNameEng
                                                    }).FirstOrDefault(),
                        FromDate = notificationQuery.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.FromDate.Date).ToString()) :
                                                       c.FromDate.Date.ToString(),
                        ToDate = notificationQuery.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.ToDate.Date).ToString()) :
                                                      c.ToDate.Date.ToString(),
                        ExchangeActorId = c.ExchangeActorId
                    })
                    .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                int totalSearchResult = _context.MemberAuditorAgrement.Count();
                return new PagedResult<MemberAuditorAgreementView>()
                {
                    Items = _mapper.Map<List<MemberAuditorAgreementView>>(dataList),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
    }
}
