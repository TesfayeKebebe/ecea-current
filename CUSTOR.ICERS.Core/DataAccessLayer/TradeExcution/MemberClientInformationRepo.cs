﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public partial class MemberClientInformationRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public MemberClientInformationRepo(ECEADbContext _context,
                                           IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }


        public async Task<IEnumerable<StaticData3>> GetClientInformationByCustomerId(Guid customerId, string lang)
        {
            IEnumerable<StaticData3> clientInfos = null;
            try
            {
                clientInfos = await context.MemberClientInformation.Where(
                                                                    x => x.MemberClientTrade.ExchangeActorId == customerId &&
                                                                    x.Status == 1 &&
                                                                    x.MemberClientTrade.TradeExcutionStatusTypeId == 2)
                     .Select(x => new StaticData3
                     {
                         Id = x.MemberClientInformationId,
                         ParentId = x.ExchangeActorId,
                         Description = x.ClientTypeId == (int)Enum.ClientType.Individual ? ((lang == "et") ? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh :
                                                   x.FirstNameEng + " " + x.FatherNameEng + " " + x.GrandFatherNameEng) : ((lang == "et") ? x.OrganizationNameAmh : x.OrganizationNameEng),
                     })
                    .AsNoTracking()
                    .ToListAsync();
                return clientInfos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }     



        public async Task<PagedResult<ClientInformationViewModel>> GetClientInfoListById(ClientTradeQueryParameters queryParameters)
        {
            try
            {
                var excutionTrade = await context.MemberClientInformation
                    .Where(w => w.MemberClientTradeId == queryParameters.MemberClientTradeId)
                    .Select(x => new ClientInformationViewModel
                    {
                        MemberClientTradeId = x.MemberClientTradeId,
                        MemberClientInformationId = x.MemberClientInformationId,
                        ExchangeActorId = x.ExchangeActorId,
                        CustomerFullName = (queryParameters.Lang == "et") ? x.MemberClientTrade.ExchangeActor.OwnerManager.FirstNameAmh + " " +
                                                           x.MemberClientTrade.ExchangeActor.OwnerManager.FatherNameAmh + " " +
                                                           x.MemberClientTrade.ExchangeActor.OwnerManager.GrandFatherNameAmh :
                                                           x.MemberClientTrade.ExchangeActor.OwnerManager.FirstNameEng + " " +
                                                           x.MemberClientTrade.ExchangeActor.OwnerManager.FatherNameEng + " " +
                                                           x.MemberClientTrade.ExchangeActor.OwnerManager.GrandFatherNameEng,
                        FullName = x.ClientTypeId == (int)Enum.ClientType.Individual ? ((queryParameters.Lang == "et") ? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh:
                                                   x.FirstNameEng + " " + x.FatherNameEng + " " + x.GrandFatherNameEng):((queryParameters.Lang == "et") ? x.OrganizationNameAmh:x.OrganizationNameEng),
                        //CommidityTypeId = x.CommidityTypeId,
                        Gender = x.Gender,
                        HouseNo = x.HouseNo,
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        Status = x.Status,
                        WoredaId = x.WoredaId,
                        BusinessFiledId = x.BusinessFiledId,
                        ZoneId = x.ZoneId,
                        BusinessSector = x.BusinessFiledId,
                        ClientProduct = (from client in context.ClientProduct
                                         where (client.MemberClientInformationId == x.MemberClientInformationId)
                                         select new ClientProductDTO
                                         {
                                             CommodityId = client.CommodityId
                                         }).ToList(),
                        CommodityName =  context.ClientProduct
                                      .Where(y=>y.MemberClientInformationId == x.MemberClientInformationId)
                                      .Select(d=> new ClientProductViewModel
                                      {
                                                CommodityId= d.CommodityId,
                                                CommodityName = (queryParameters.Lang == "et") ? d.Commodity.DescriptionAmh:
                                                                                 d.Commodity.DesciptionEng
                                      }).ToList(),
                        AgreementDate = x.AgreementDate,
                        DateAgreement = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.AgreementDate.Date).ToString()),
                        MobilePhone = x.MobilePhone,
                        RegularPhone = x.RegularPhone,
                        TradeTypeId = x.TradeTypeId

                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
              int  totalResultList = excutionTrade.Count();
                return new PagedResult<ClientInformationViewModel>()
                {
                    Items = mapper.Map<List<ClientInformationViewModel>>(excutionTrade),
                    ItemsCount = context.MemberClientInformation
                    .Where(w => w.MemberClientTradeId == queryParameters.MemberClientTradeId).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClientInformationViewModel>> GetAllClientInformationList(ClientTradeQueryParameters queryParameters)
        {
            try
            {
                var excutionTrade = await context.MemberClientInformation.Where(x=> (String.IsNullOrEmpty(queryParameters.ECXCode) ||
                                                                                    x.MemberClientTrade.ExchangeActor.Ecxcode == queryParameters.ECXCode))
                    .Select(x => new ClientInformationViewModel
                    {
                        OrganizationName = (queryParameters.Lang=="et")?x.MemberClientTrade.ExchangeActor.OrganizationNameAmh:
                                                                        x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                        MemberClientTradeId = x.MemberClientTradeId,
                        MemberClientInformationId = x.MemberClientInformationId,
                        ExchangeActorId = x.ExchangeActorId,
                        FullName = x.ClientTypeId == (int)Enum.ClientType.Individual ? ((queryParameters.Lang == "et") ? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh :
                                                   x.FirstNameEng + " " + x.FatherNameEng + " " + x.GrandFatherNameEng) : ((queryParameters.Lang == "et") ? x.OrganizationNameAmh : x.OrganizationNameEng),
                        Gender = x.Gender,
                        HouseNo = x.HouseNo,
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        Status = x.Status,
                        WoredaId = x.WoredaId,
                        BusinessFiledId = x.BusinessFiledId,
                        ZoneId = x.ZoneId,
                        BusinessSector = x.BusinessFiledId,
                        ClientProduct = (from client in context.ClientProduct
                                         where (client.MemberClientInformationId == x.MemberClientInformationId)
                                         select new ClientProductDTO
                                         {
                                             CommodityId = client.CommodityId
                                         }).ToList(),
                        CommodityName = context.ClientProduct
                                      .Where(y => y.MemberClientInformationId == x.MemberClientInformationId)
                                      .Select(d => new ClientProductViewModel
                                      {
                                          CommodityId = d.CommodityId,
                                          CommodityName = (queryParameters.Lang == "et") ? d.Commodity.DescriptionAmh :
                                                                                 d.Commodity.DesciptionEng
                                      }).ToList(),
                        AgreementDate = x.AgreementDate,
                        DateAgreement = (queryParameters.Lang == "et")?EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.AgreementDate.Date).ToString()):
                                                                       x.AgreementDate.Date.ToString(),
                        MobilePhone = x.MobilePhone,
                        RegularPhone = x.RegularPhone,
                        TradeTypeId = x.TradeTypeId,
                        RegionName =(queryParameters.Lang == "et")? x.Region.DescriptionAmh: x.Region.DescriptionEng,
                        ZoneName = (queryParameters.Lang == "et") ? x.Zone.DescriptionAmh : x.Zone.DescriptionEng,
                        WoredaName = (queryParameters.Lang == "et") ? x.Woreda.DescriptionAmh : x.Woreda.DescriptionEng,
                        KebeleName = (queryParameters.Lang == "et") ? x.Kebele.DescriptionAmh : x.Kebele.DescriptionEng
                    })                    
                    .AsNoTracking()
                    .ToListAsync();
                return excutionTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<ClientInformationViewModel>> GetClientInformationList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                List<ClientInformationViewModel> informationViewModels = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    informationViewModels = await GetClientInformationListAmh(clientTradeQueryParameters);
                }
                else
                {
                    informationViewModels = await GetClientInformationListEng(clientTradeQueryParameters);
                }
                int totalSearchResult = informationViewModels.Count();
               // var pagedResult = informationViewModels.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return new PagedResult<ClientInformationViewModel>()
                {
                    Items = mapper.Map<List<ClientInformationViewModel>>(informationViewModels),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public PagedResult<ClientInformationViewModel> GetMembersClientInformationList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientInfo =  context.ExchangeActor
                    .Where(x => x.MemberCategoryId == 72 &&
                                x.Status == 20)                    
                    .Select(x => new ClientInformationViewModel
                    {
                        
                        OrganizationName =(clientTradeQueryParameters.Lang == "et")? x.OrganizationNameAmh:
                                                                                     x.OrganizationAreaNameEng,
                        CustomerType =(clientTradeQueryParameters.Lang == "et")? x.CustomerType.DescriptionAmh:
                                                                                 x.CustomerType.DescriptionEng,

                        
                        MobilePhone = x.MobileNo,
                        RegularPhone = x.Tel,
                        ExchangeActorId = x.ExchangeActorId
                        
                    })
                    .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                    .AsNoTracking()
                    .ToList();
                int totalSearchResult = context.ExchangeActor.Where(x => x.MemberCategoryId == 72 && x.Status == 20).Count();
                return new PagedResult<ClientInformationViewModel>()
                {
                    Items = mapper.Map<List<ClientInformationViewModel>>(clientInfo),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public PagedResult<ClientInformationViewModel> GetListOfBasicClientInformation(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientInfo =  context.MemberClientInformation
                    .Where(x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                    x.MemberClientTrade.ReportTypeId == 3 &&
                    (x.MemberClientTrade.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId) &&
                    ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                    ((clientTradeQueryParameters.Status == 0) || (x.Status == clientTradeQueryParameters.Status)) &&
                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.BusinessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                    ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                    )
                    .OrderByDescending(c => c.AgreementDate)
                    .Select(x => new ClientInformationViewModel
                    {
                        MemberClientInformationId = x.MemberClientInformationId,
                        OrganizationName = clientTradeQueryParameters.Lang == "et"? x.MemberClientTrade.ExchangeActor.OrganizationNameAmh:
                                                                                    x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                        CustomerType = clientTradeQueryParameters.Lang == "et" ?x.MemberClientTrade.ExchangeActor.MemberCategory.DescriptionAmh:
                                                                                x.MemberClientTrade.ExchangeActor.MemberCategory.DescriptionEng,
                        ECXCode = x.MemberClientTrade.ExchangeActor.Ecxcode,
                        FullName = x.ClientTypeId == (int)Enum.ClientType.Individual ? ((clientTradeQueryParameters.Lang == "et")? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh:
                                                                                         x.FirstNameEng +" " +x.FatherNameEng +" "+ x.GrandFatherNameEng)                        
                                                                                         : (clientTradeQueryParameters.Lang == "et") ? x.OrganizationNameAmh:x.OrganizationNameEng,
                        BusinessFiledId = x.BusinessFiledId,
                        AgreementDate = x.AgreementDate,
                        DateAgreement = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.AgreementDate.Date).ToString()):
                                                                                    x.AgreementDate.Date.ToString(),
                        MobilePhone = x.MobilePhone,
                        RegularPhone = x.RegularPhone,
                        CommodityName = (from client in context.ClientProduct
                                         where (client.MemberClientInformationId == x.MemberClientInformationId)
                                         select new ClientProductViewModel
                                         {
                                             CommodityId = client.CommodityId,
                                             CommodityName = (clientTradeQueryParameters.Lang == "et") ? client.Commodity.DescriptionAmh:
                                                                                                         client.Commodity.DesciptionEng
                                         })
                                         .AsNoTracking()
                                         .ToList(),
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        ZoneId = x.ZoneId,
                        WoredaId = x.WoredaId,
                        Gender = x.Gender,
                        TradeTypeId = x.TradeTypeId,
                        Status = x.Status,
                        MemberClientTradeId = x.MemberClientTradeId
                    })
                    .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                    .AsNoTracking()
                    .ToList();
                int totalSearchResult = context.MemberClientInformation
                    .Where(x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                    x.MemberClientTrade.ReportTypeId == 3 &&
                    (x.MemberClientTrade.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId) &&
                    ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                    ((clientTradeQueryParameters.Status == 0) || (x.Status == clientTradeQueryParameters.Status)) &&
                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.BusinessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                    ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year))).Count();
                return new PagedResult<ClientInformationViewModel>()
                {
                    Items = mapper.Map<List<ClientInformationViewModel>>(clientInfo),
                    ItemsCount = totalSearchResult
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClientInformationViewModel>> GetClientInformationListAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientInfo = await context.MemberClientInformation
                    .Where(
                    x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                    x.MemberClientTrade.ReportTypeId == 3 &&
                    ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                    x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                    ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                    ((clientTradeQueryParameters.Status == 0) || (x.Status == clientTradeQueryParameters.Status)) &&
                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                    )
                    .OrderByDescending(c => c.AgreementDate)
                    .Select(x => new ClientInformationViewModel
                    {
                        MemberClientInformationId = x.MemberClientInformationId,
                        OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                        FullName = x.ClientTypeId == (int)Enum.ClientType.Individual ? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh : x.OrganizationNameAmh,
                        BusinessFiledId = x.BusinessFiledId,
                        AgreementDate = x.AgreementDate,
                        DateAgreement = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.AgreementDate.Date).ToString()),
                        MobilePhone = x.MobilePhone,
                        RegularPhone = x.RegularPhone,
                        CommodityName = (from client in context.ClientProduct
                                           where (client.MemberClientInformationId == x.MemberClientInformationId)
                                         select new ClientProductViewModel
                                         {
                                             CommodityId = client.CommodityId,
                                             CommodityName= client.Commodity.DescriptionAmh
                                         })
                                         .AsNoTracking()
                                         .ToList(),
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        ZoneId = x.ZoneId,
                        WoredaId = x.WoredaId,
                        Gender = x.Gender,
                        MemberClientTradeId = x.MemberClientTradeId,
                        PreparedByFullName = x.MemberClientTrade.PreparedByFirstNameAmh +" "+
                                             x.MemberClientTrade.PreparedByFatherNameAmh +" "+
                                             x.MemberClientTrade.PreparedByGrandFatherNameAmh,
                        PreparedDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.MemberClientTrade.PreparedDate.Date).ToString()),
                        Position = x.MemberClientTrade.PositionAmh
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return clientInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ClientInformationViewModel>> GetClientInformationListEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientInfo = await context.MemberClientInformation
                    .Where(
                    x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                    x.MemberClientTrade.ReportTypeId == 3 &&
                    ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                    x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                    ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                    ((clientTradeQueryParameters.Status == 0) || (x.Status == clientTradeQueryParameters.Status)) &&
                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                    )
                    .OrderByDescending(c => c.AgreementDate)
                    .Select(x => new ClientInformationViewModel
                    {
                        MemberClientInformationId = x.MemberClientInformationId,
                        OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                        FullName = x.ClientTypeId == (int)Enum.ClientType.Individual ?  x.FirstNameEng + " " + x.FatherNameEng + " " +x.GrandFatherNameAmh  : x.OrganizationNameEng,                       
                        BusinessFiledId = x.BusinessFiledId,
                        AgreementDate = x.AgreementDate,
                        DateAgreement = x.AgreementDate.Date.ToString(),
                        MobilePhone = x.MobilePhone,
                        RegularPhone = x.RegularPhone,
                        CommodityName = (from client in context.ClientProduct
                                         where (client.MemberClientInformationId == x.MemberClientInformationId)
                                         select new ClientProductViewModel
                                         {
                                             CommodityId = client.CommodityId,
                                             CommodityName= client.Commodity.DesciptionEng
                                         })
                                         .AsNoTracking()
                                         .ToList(),
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        ZoneId = x.ZoneId,
                        WoredaId = x.WoredaId,
                        Gender = x.Gender,
                        MemberClientTradeId = x.MemberClientTradeId,
                        PreparedByFullName = x.MemberClientTrade.PreparedByFirstName +" " +
                                             x.MemberClientTrade.PreparedByFatherName +" " +
                                             x.MemberClientTrade.PreparedByGrandFatherName,
                        PreparedDate =(x.MemberClientTrade.PreparedDate.Date).ToString(),
                        Position = x.MemberClientTrade.Position

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return clientInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<ClientInformationViewModel> GetClintInformationById(int memberClientId)
        {
            try
            {
                var clientInfo = await context.MemberClientInformation
                    .Where(x => x.MemberClientInformationId == memberClientId)
                    .Select(x => new ClientInformationViewModel
                    {

                        MemberClientInformationId = x.MemberClientInformationId,
                        ExchangeActorId = x.ExchangeActorId,
                        CustomerFullName = x.MemberClientTrade.ExchangeActor.OwnerManager.FirstNameAmh + " " +
                                           x.MemberClientTrade.ExchangeActor.OwnerManager.FatherNameAmh + " " +
                                           x.MemberClientTrade.ExchangeActor.OwnerManager.GrandFatherNameAmh,
                        FullName = x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh,
                        FatherNameAmh = x.FatherNameAmh,
                        FatherNameEng = x.FatherNameEng,
                        FirstNameAmh = x.FirstNameAmh,
                        FirstNameEng = x.FirstNameEng,
                        Gender = x.Gender,
                        GrandFatherNameAmh = x.GrandFatherNameAmh,
                        GrandFatherNameEng = x.GrandFatherNameEng,
                        HouseNo = x.HouseNo,
                        KebeleId = x.KebeleId,
                        RegionId = x.RegionId,
                        Status = x.Status,
                        WoredaId = x.WoredaId,
                        BusinessFiledId = x.BusinessFiledId,
                        ZoneId = x.ZoneId,
                        //CommodityId = x.CommodityId,
                        CustomerType = x.MemberClientTrade.ExchangeActor.CustomerType.DescriptionAmh,
                        OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                        ClientProduct = (from client in context.ClientProduct
                                         where(client.MemberClientInformationId == memberClientId)
                                         select new ClientProductDTO { 
                                            CommodityId = client.CommodityId                                           
                                         })
                                         .AsNoTracking()
                                         .ToList(),
                        AgreementDate = x.AgreementDate,
                        TradeTypeId = x.TradeTypeId,
                        RegularPhone = x.RegularPhone,
                        MobilePhone = x.MobilePhone,
                        ClientTypeId = x.ClientTypeId,
                        OrganizationNameAmh = x.OrganizationNameAmh,
                        OrganizationNameEng = x.OrganizationNameEng,
                        MemberClientTradeId = x.MemberClientTradeId

                    })
                    .FirstOrDefaultAsync();
                return clientInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateMemberClientInformation(MemberClientInformationDTO memberClientInformation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var clientInformation = await context.MemberClientInformation
                              .Where(x => x.MemberClientInformationId == memberClientInformation.MemberClientInformationId)
                              .AsNoTracking()
                              .FirstOrDefaultAsync();

                    if (clientInformation != null)
                    {
                        var updateClientInfo = mapper.Map(memberClientInformation, clientInformation);
                        context.Entry(updateClientInfo).State = EntityState.Modified;
                        foreach(var commodity in updateClientInfo.ClientProduct)
                        {
                            context.Entry(commodity).State = EntityState.Added;
                        }
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return clientInformation.MemberClientTradeId;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<Guid> DeleteMemberClientInformation(MemberClientInformationDTO clientInformationDTO)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var clientInformation = await context.MemberClientInformation
                                                         .Where(x => x.MemberClientInformationId == clientInformationDTO.MemberClientInformationId)
                                                         .AsNoTracking()
                                                         .FirstOrDefaultAsync();
                    if (clientInformation != null)
                    {
                        var deleteClientInformation = mapper.Map(clientInformationDTO, clientInformation);
                        context.Entry(deleteClientInformation).State = EntityState.Deleted;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return clientInformation.ExchangeActorId;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        // save Client Basic Information
        public async Task<Guid> SaveClientInformation(MemberClientInformationDTO memberClientInformation)
        {
            try
            {
                var clientInfo = mapper.Map<MemberClientInformation>(memberClientInformation);
                context.MemberClientInformation.Add(clientInfo);
                await context.SaveChangesAsync();
                return clientInfo.ExchangeActorId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ClientInformationViewModel>> GetClientFullName(ClientTradeQueryParameters clientTradeQueryParameters, Guid exachangeActor)
        {
            try
            {
                var clientInfo = await context.MemberClientInformation
                    .Where(
                    x => x.Status == 1 &&
                   // x.MemberClientTrade.WorkFlowStatus == 2 &&
                    x.MemberClientTrade.ExchangeActor.ExchangeActorId == exachangeActor &&
                    ((string.IsNullOrEmpty(clientTradeQueryParameters.FullName)) ||
                    x.FirstNameAmh.Contains(clientTradeQueryParameters.FullName)) &&
                    (x.BusinessFiledId == clientTradeQueryParameters.FieldBusinessId)
                    )
                    .OrderByDescending(c => c.AgreementDate)
                    .Select(x => new ClientInformationViewModel
                    {
                        MemberClientInformationId = x.MemberClientInformationId,
                        FullName =(clientTradeQueryParameters.Lang == "et") ? x.FirstNameAmh + " " + x.FatherNameAmh + " " + x.GrandFatherNameAmh:
                                                                              x.FirstNameEng + " " + x.FatherNameEng + " " + x.GrandFatherNameEng,
                        BusinessFiledId = x.BusinessFiledId,

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return clientInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<MembersTotalClient>> GetMembersTotalNoOfClient(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                if(clientTrade.QuantityFrom > 0 || clientTrade.QuantityTo > 0)
                {
                    var totalActiveClient = await context.MemberClientInformation.Where(x => x.Status == 1 &&
                                                                                                       x.MemberClientTrade.ReportTypeId == 3 &&
                                                                                                        x.MemberClientTrade.TradeExcutionStatusTypeId == 2)
                                                .GroupBy(r => r.MemberClientTrade.ExchangeActorId)
                                                     .Select(b => new
                                                     {
                                                         Id = b.Key,
                                                         Count = b.Count()
                                                     })
                                                   .AsNoTracking()
                                                   .ToListAsync();
                    var totalPassiveClient = await context.MemberClientInformation.Where(x => x.Status == 2 &&
                                                                                         x.MemberClientTrade.ReportTypeId == 3 &&
                                                                                         x.MemberClientTrade.TradeExcutionStatusTypeId == 2)
                                 .GroupBy(r => r.MemberClientTrade.ExchangeActorId)
                                      .Select(b => new
                                      {
                                          Id = b.Key,
                                          Count = b.Count()
                                      })
                                    .AsNoTracking()
                                    .ToListAsync();
                    var clientTradeRepo = await (from exa in context.ExchangeActor
                                                 join x in totalActiveClient
                                            on exa.ExchangeActorId equals x.Id
                                                 where x.Count >= clientTrade.QuantityFrom &&
                                                       x.Count <= clientTrade.QuantityTo
                                                 //     join  y in totalPassiveClient
                                                 //on exa.ExchangeActorId equals y.Id

                                                 select new MembersTotalClient
                                                 {
                                                     OrganizationName = (clientTrade.Lang == "et") ? exa.OrganizationNameAmh :
                                                                                              exa.OrganizationNameEng,
                                                     CustomerType = (clientTrade.Lang == "et") ? exa.CustomerType.DescriptionAmh :
                                                                                           exa.CustomerType.DescriptionEng,
                                                     ClientActive = x.Count,
                                                     // ClientPassive = totalActiveClient y.Count

                                                 })
                                     .Paging(clientTrade.PageCount, clientTrade.PageNumber)
                                     .OrderByDescending(x => x.ClientActive)
                                     .AsNoTracking()
                                     .ToListAsync();
                    var totalResultLists = (from exa in context.ExchangeActor
                                            join x in totalActiveClient
                                       on exa.ExchangeActorId equals x.Id
                                       where x.Count >= clientTrade.QuantityFrom &&
                                             x.Count <= clientTrade.QuantityTo
                                            select new MembersTotalClient
                                            {
                                                OrganizationName = (clientTrade.Lang == "et") ? exa.OrganizationNameAmh :
                                                                     exa.OrganizationNameEng,
                                                CustomerType = (clientTrade.Lang == "et") ? exa.CustomerType.DescriptionAmh :
                                                                                          exa.CustomerType.DescriptionEng,
                                                ClientActive = x.Count,
                                                // ClientPassive = totalActiveClient y.Count

                                            })
                                              .ToList();
                    return new PagedResult<MembersTotalClient>()
                    {
                        Items = mapper.Map<List<MembersTotalClient>>(clientTradeRepo),
                        ItemsCount = totalResultLists.Count()
                    };

                }
                else
                {
                    var totalActiveClient = await context.MemberClientInformation.Where(x => x.Status == 1 &&
                                                                                                       x.MemberClientTrade.ReportTypeId == 3 &&
                                                                                                        x.MemberClientTrade.TradeExcutionStatusTypeId == 2)
                                                .GroupBy(r => r.MemberClientTrade.ExchangeActorId)
                                                     .Select(b => new
                                                     {
                                                         Id = b.Key,
                                                         Count = b.Count()
                                                     })
                                                   .AsNoTracking()
                                                   .ToListAsync();
                    var totalPassiveClient = await context.MemberClientInformation.Where(x => x.Status == 2 &&
                                                                                         x.MemberClientTrade.ReportTypeId == 3 &&
                                                                                         x.MemberClientTrade.TradeExcutionStatusTypeId == 2)
                                 .GroupBy(r => r.MemberClientTrade.ExchangeActorId)
                                      .Select(b => new
                                      {
                                          Id = b.Key,
                                          Count = b.Count()
                                      })
                                    .AsNoTracking()
                                    .ToListAsync();
                    var clientTradeRepo = await (from exa in context.ExchangeActor
                                                 join x in totalActiveClient
                                            on exa.ExchangeActorId equals x.Id

                                                 select new MembersTotalClient
                                                 {
                                                     OrganizationName = (clientTrade.Lang == "et") ? exa.OrganizationNameAmh :
                                                                                              exa.OrganizationNameEng,
                                                     CustomerType = (clientTrade.Lang == "et") ? exa.CustomerType.DescriptionAmh :
                                                                                           exa.CustomerType.DescriptionEng,
                                                     ClientActive = x.Count,
                                                     // ClientPassive = totalActiveClient y.Count

                                                 })
                                     .Paging(clientTrade.PageCount, clientTrade.PageNumber)
                                     .OrderByDescending(x => x.ClientActive)
                                     .AsNoTracking()
                                     .ToListAsync();
                    var totalResultLists = (from exa in context.ExchangeActor
                                            join x in totalActiveClient
                                       on exa.ExchangeActorId equals x.Id
                                            select new MembersTotalClient
                                            {
                                                OrganizationName = (clientTrade.Lang == "et") ? exa.OrganizationNameAmh :
                                                                     exa.OrganizationNameEng,
                                                CustomerType = (clientTrade.Lang == "et") ? exa.CustomerType.DescriptionAmh :
                                                                                          exa.CustomerType.DescriptionEng,
                                                ClientActive = x.Count,
                                                // ClientPassive = totalActiveClient y.Count

                                            })
                                              .ToList();
                    return new PagedResult<MembersTotalClient>()
                    {
                        Items = mapper.Map<List<MembersTotalClient>>(clientTradeRepo),
                        ItemsCount = totalResultLists.Count()
                    };

                }

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TradeExecutionAmountViewModel> GetTotalNoListOfClientInformation(TradeExecutionOrderSearchQuery tradeExecution)
        {
            try
            {
                List<TradeExecutionAmountViewModel> data;
                if(tradeExecution.CommodityId != 0)
                {

                }
                else
                {

                }
                if (tradeExecution.Lang == "et")
                {
                    if (tradeExecution.OrderTypeId == "DESC")
                    {
                        data = context.TradeExecutionAmountViewModel.FromSqlRaw
                                                      (@"Select mct.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                                         count(mci.MemberClientTradeId) Amount from MemberClientInformation mci
                                                         Join MemberClientTrade mct on
                                                         mci.MemberClientTradeId = mct.MemberClientTradeId
                                                         join ExchangeActor ecx on
                                                         mct.ExchangeActorId = ecx.ExchangeActorId
                                                         WHERE mci.Status = {0}
                                                        GROUP BY ecx.OrganizationNameAmh,mct.ExchangeActorId
                                                        ORDER BY Amount DESC
                                                         OFFSET 0 ROWS FETCH NEXT {1} ROWS ONLY",                    
                                                          tradeExecution.Status,
                                                          tradeExecution.TopLevel)
                                                          .AsNoTracking()
                                                          .ToList();
                    }
                    else
                    {
                        data = context.TradeExecutionAmountViewModel.FromSqlRaw
                                                     (@"Select mct.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                                         count(mci.MemberClientTradeId) Amount from MemberClientInformation mci
                                                         Join MemberClientTrade mct on
                                                         mci.MemberClientTradeId = mct.MemberClientTradeId
                                                         join ExchangeActor ecx on
                                                         mct.ExchangeActorId = ecx.ExchangeActorId
                                                         WHERE mci.Status = {0}
                                                        GROUP BY ecx.OrganizationNameAmh,mct.ExchangeActorId
                                                        ORDER BY Amount ASC
                                                         OFFSET 0 ROWS FETCH NEXT {1} ROWS ONLY",
                                                         tradeExecution.Status,
                                                         tradeExecution.TopLevel)
                                                         .AsNoTracking()
                                                         .ToList();
                    }

                }
                else
                {
                    if (tradeExecution.OrderTypeId == "DESC")
                    {
                        data = context.TradeExecutionAmountViewModel.FromSqlRaw
                                                      (@"Select mct.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                                         count(mci.MemberClientTradeId) Amount from MemberClientInformation mci
                                                         Join MemberClientTrade mct on
                                                         mci.MemberClientTradeId = mct.MemberClientTradeId
                                                         join ExchangeActor ecx on
                                                         mct.ExchangeActorId = ecx.ExchangeActorId
                                                         WHERE mci.Status = {0}
                                                        GROUP BY ecx.OrganizationNameAmh,mct.ExchangeActorId
                                                        ORDER BY Amount DESC
                                                         OFFSET 0 ROWS FETCH NEXT {1} ROWS ONLY",
                                                          tradeExecution.Status,
                                                          tradeExecution.TopLevel)
                                                          .AsNoTracking()
                                                          .ToList();
                    }
                    else
                    {
                        data = context.TradeExecutionAmountViewModel.FromSqlRaw
                                                       (@"Select mct.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                                         count(mci.MemberClientTradeId) Amount from MemberClientInformation mci
                                                         Join MemberClientTrade mct on
                                                         mci.MemberClientTradeId = mct.MemberClientTradeId
                                                         join ExchangeActor ecx on
                                                         mct.ExchangeActorId = ecx.ExchangeActorId
                                                         WHERE mci.Status = {0}
                                                        GROUP BY ecx.OrganizationNameAmh,mct.ExchangeActorId
                                                        ORDER BY Amount ASC
                                                         OFFSET 0 ROWS FETCH NEXT {1} ROWS ONLY",
                                                           tradeExecution.Status,
                                                           tradeExecution.TopLevel)
                                                           .AsNoTracking()
                                                           .ToList();
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
