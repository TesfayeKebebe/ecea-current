﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class MemberClientTradeDetailRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public MemberClientTradeDetailRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<PagedResult<BoughtSelfTradeExecutionDTO>> GetAllBoughtSelfTrade(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            int totalResultList = 0;
            try
            {
                List<BoughtSelfTradeExecutionDTO> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = await GetAllBoughtSelfTradeAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = await GetAllBoughtSelfTradeEng(clientTradeQueryParameters);
                }
                totalResultList = tradeList.Count();
                var pagedResult = tradeList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return new PagedResult<BoughtSelfTradeExecutionDTO>()
                {
                    Items = mapper.Map<List<BoughtSelfTradeExecutionDTO>>(pagedResult),
                    ItemsCount = totalResultList
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public PagedResult<MembersTradeExecutionReportPeriod> GetAllTradeExecutionList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId==72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = context.MemberClientTrade.Where(x => x.ReportTypeId == 1 && x.TradeExcutionStatusTypeId == 2 &&
                                                           ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                           ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                                          ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                                         ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                         ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)))
                                                            .Select(n => new MembersTradeExecutionReportPeriod
                                                            {

                                                                ExchangeActorId = n.ExchangeActorId,
                                                                MemberClientTradeId = n.MemberClientTradeId,
                                                                OrganizationName = clientTradeQueryParameters.Lang == "et" ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                             n.ExchangeActor.OrganizationNameEng,
                                                                CustomerType = clientTradeQueryParameters.Lang == "et" ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                          n.ExchangeActor.MemberCategory.DescriptionEng,
                                                                DateReport = clientTradeQueryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                         n.ReportDate.Date.ToString(),
                                                                MobilePhone = n.ExchangeActor.MobileNo,
                                                                RegularPhone = n.ExchangeActor.Tel,
                                                                Year = n.Year,
                                                                ReportPeriodId = n.ReportPeriodId

                                                            })
                                        .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                        .AsNoTracking()
                                        .ToList();
                    int totalSearchResult = context.MemberClientTrade.Where(x => x.ReportTypeId == 1 && x.TradeExcutionStatusTypeId == 2 &&
                                           ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                           ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                           ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                           ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                           ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))).Count();
                    return new PagedResult<MembersTradeExecutionReportPeriod>()
                    {
                        Items = mapper.Map<List<MembersTradeExecutionReportPeriod>>(clientTrade),
                        ItemsCount = totalSearchResult
                    };
                }
                else
                {
                    var clientTrade = context.MemberClientTrade.Where(x => x.ReportTypeId == 1 && x.TradeExcutionStatusTypeId == 2 &&
                                                           ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                           ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                                          ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                                         ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                         ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)))
                                                            .Select(n => new MembersTradeExecutionReportPeriod
                                                            {

                                                                ExchangeActorId = n.ExchangeActorId,
                                                                MemberClientTradeId = n.MemberClientTradeId,
                                                                OrganizationName = clientTradeQueryParameters.Lang == "et" ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                             n.ExchangeActor.OrganizationNameEng,
                                                                CustomerType = clientTradeQueryParameters.Lang == "et" ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                          n.ExchangeActor.CustomerType.DescriptionEng,
                                                                DateReport = clientTradeQueryParameters.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                         n.ReportDate.Date.ToString(),
                                                                MobilePhone = n.ExchangeActor.MobileNo,
                                                                RegularPhone = n.ExchangeActor.Tel,
                                                                Year = n.Year,
                                                                ReportPeriodId = n.ReportPeriodId

                                                            })
                                                            .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                        .AsNoTracking()
                                        .ToList();
                    int totalSearchResult = context.MemberClientTrade.Where(x => x.ReportTypeId == 1 && x.TradeExcutionStatusTypeId == 2 &&
                                           ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                           ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                           ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                           ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                           ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))).Count();
                    return new PagedResult<MembersTradeExecutionReportPeriod>()
                    {
                        Items = mapper.Map<List<MembersTradeExecutionReportPeriod>>(clientTrade),
                        ItemsCount = totalSearchResult
                    };
                }
                
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            }

        public async Task<PagedResult<BoughtSelfTradeExecutionDTO>> GetAllMembersTradeExecutionList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {            
                    var clientTrade = await context.MemberClientTradeDetail.Where(x => x.MemberClientTrade.ReportTypeId == 1 && x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((x.MemberClientTrade.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId)) &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                        ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&                                      
                                        ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                                        ((clientTradeQueryParameters.CommodityId == 0) || (x.CommodityId == clientTradeQueryParameters.CommodityId)) &&
                                        ((clientTradeQueryParameters.UnitMeasurment == 0) || (x.UnitMeasurementId == clientTradeQueryParameters.UnitMeasurment)) &&
                                        ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport)))
                                           
                                            .Select(n => new BoughtSelfTradeExecutionDTO
                                             {
                                                MemberClientTradeId = n.MemberClientTradeId,
                                               OrganizationName = clientTradeQueryParameters.Lang == "et"? n.MemberClientTrade.ExchangeActor.OrganizationNameAmh:
                                                                                                          n.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                               CustomerType =clientTradeQueryParameters.Lang== "et"? n.MemberClientTrade.ExchangeActor.CustomerType.DescriptionAmh:
                                                                                                      n.MemberClientTrade.ExchangeActor.CustomerType.DescriptionEng,
                                               EcxCode = n.MemberClientTrade.ExchangeActor.Ecxcode,
                                               TradeDate = clientTradeQueryParameters.Lang=="et"? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.TradeDate.Date).ToString()):
                                                                                                  n.TradeDate.Date.ToString(),
                                               ClientFullName = n.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? (clientTradeQueryParameters.Lang == "et"? n.MemberClientInformation.FirstNameAmh + " " +
                                                                                                                                            n.MemberClientInformation.FatherNameAmh + " " +
                                                                                                                                            n.MemberClientInformation.GrandFatherNameAmh:
                                                                                                                                            n.MemberClientInformation.FirstNameEng + " " +
                                                                                                                                            n.MemberClientInformation.FatherNameEng + " " +
                                                                                                                                            n.MemberClientInformation.GrandFatherNameEng) :
                                                                                                                                            (clientTradeQueryParameters.Lang=="et"? n.MemberClientInformation.OrganizationNameAmh:
                                                                                                                                             n.MemberClientInformation.OrganizationNameEng),

                                               CommodityName =clientTradeQueryParameters.Lang == "et"? n.CommodityGrade.CommodityType.Commodity.DescriptionAmh:
                                                                                                       n.CommodityGrade.CommodityType.Commodity.DesciptionEng,
                                               MemberClientTradeDetailId = n.MemberClientTradeDetailId,
                                               BayerCommodityGrade = clientTradeQueryParameters.Lang == "et" ? n.CommodityGrade.DescriptionAmh:
                                                                                                               n.CommodityGrade.DescriptoinEng,
                                               BayerCommodityType = clientTradeQueryParameters.Lang == "et" ? n.CommodityGrade.CommodityType.DescriptionAmh:
                                                                                                              n.CommodityGrade.CommodityType.DescriptionEng,
                                               BayerOrderPrice = n.OrderPrice,
                                               BayerUnitPrice = n.UnitPrice,
                                               CommissionInPercent = n.CommissionInPrice,
                                               Reperesentative = clientTradeQueryParameters.Lang == "et" ? n.OwnerManager.FirstNameAmh + " " + n.OwnerManager.FatherNameAmh + " " + n.OwnerManager.GrandFatherNameAmh:
                                                                                                           n.OwnerManager.FirstNameEng + " " + n.OwnerManager.FatherNameEng + " " + n.OwnerManager.GrandFatherNameEng,
                                               Warehouse = n.Warehouse,
                                               LotQuantity = n.Quantity
                                             })
                                           .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                        .AsNoTracking()
                                        .ToListAsync();
                int totalSearchResult = context.MemberClientTradeDetail.Where(x => x.MemberClientTrade.ReportTypeId == 1 && x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                        ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&
                                        ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                                        ((clientTradeQueryParameters.CommodityId == 0) || (x.CommodityId == clientTradeQueryParameters.CommodityId)) &&
                                        ((clientTradeQueryParameters.UnitMeasurment == 0) || (x.UnitMeasurementId == clientTradeQueryParameters.UnitMeasurment)) &&
                                        ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport))).Count();
                return new PagedResult<BoughtSelfTradeExecutionDTO>()
                {
                    Items = mapper.Map<List<BoughtSelfTradeExecutionDTO>>(clientTrade),
                    ItemsCount = totalSearchResult
                };
            
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<BoughtSelfTradeExecutionDTO>> GetAllBoughtSelfTradeAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId== 88)
                {
                    var clientTrade = await context.MemberClientTradeDetail.Where(
                                                                   x => x.MemberClientTrade.ReportTypeId == 1 &&
                                                                   x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                        ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                      ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                                      ((clientTradeQueryParameters.IsTradeExcutionAccomplished == 0) || (x.MemberClientTrade.IsTradeExcutionAccomplished == clientTradeQueryParameters.IsTradeExcutionAccomplished)) &&
                                      ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                      ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                       ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport)))
                                                            .Include(y => y.CommodityGrade)
                                                            .Select(n => new BoughtSelfTradeExecutionDTO
                                                            {
                                                                MemberClientTradeId = n.MemberClientTradeId,
                                                                ExchangeActorId = n.MemberClientTrade.ExchangeActorId,
                                                                OrganizationName = n.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                                                                CustomerType = n.MemberClientTrade.ExchangeActor.MemberCategory.DescriptionAmh,
                                                                DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.MemberClientTrade.ReportDate.Date).ToString()),
                                                                MobilePhone = n.MemberClientTrade.ExchangeActor.MobileNo,
                                                                RegularPhone = n.MemberClientTrade.ExchangeActor.Tel,
                                                                TradeDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.TradeDate.Date).ToString()),
                                                                ClientFullName = n.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? n.MemberClientInformation.FirstNameAmh + " " +
                                                                                                                                                          n.MemberClientInformation.FatherNameAmh + " " +
                                                                                                                                                          n.MemberClientInformation.GrandFatherNameAmh :
                                                                                                                                                          n.MemberClientInformation.OrganizationNameAmh,

                                                                CommodityName = n.CommodityGrade.CommodityType.Commodity.DescriptionAmh,
                                                                MemberClientTradeDetailId = n.MemberClientTradeDetailId,
                                                                BayerCommodityGrade = n.CommodityGrade.DescriptionAmh,
                                                                BayerCommodityType = n.CommodityGrade.CommodityType.DescriptionAmh,
                                                                BayerOrderPrice = n.OrderPrice,
                                                                BayerUnitPrice = n.UnitPrice,
                                                                CommissionInPercent = n.CommissionInPrice,
                                                                Reperesentative = n.OwnerManager.FirstNameAmh + " " + n.OwnerManager.FatherNameAmh + " " + n.OwnerManager.GrandFatherNameAmh,
                                                                Warehouse = n.Warehouse,
                                                                LotQuantity = n.Quantity
                                                            })
                                                            .AsNoTracking()
                                                             .ToListAsync();
                    return clientTrade;
                }
                else
                {
                    var clientTrade = await context.MemberClientTradeDetail.Where(
                                                                   x => x.MemberClientTrade.ReportTypeId == 1 &&
                                                                   x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                        ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                      ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                                      ((clientTradeQueryParameters.IsTradeExcutionAccomplished == 0) || (x.MemberClientTrade.IsTradeExcutionAccomplished == clientTradeQueryParameters.IsTradeExcutionAccomplished)) &&
                                      ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                      ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                       ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport)))
                                                            .Include(y => y.CommodityGrade)
                                                            .Select(n => new BoughtSelfTradeExecutionDTO
                                                            {
                                                                MemberClientTradeId = n.MemberClientTradeId,
                                                                ExchangeActorId = n.MemberClientTrade.ExchangeActorId,
                                                                OrganizationName = n.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                //CustomerType = n.MemberClientTrade.ExchangeActor.CustomerType.DescriptionEng,
                                                                //DateReport = n.MemberClientTrade.ReportDate.ToString(),
                                                                //MobilePhone = n.MemberClientTrade.ExchangeActor.MobileNo,
                                                                //RegularPhone = n.MemberClientTrade.ExchangeActor.Tel,
                                                                TradeDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.TradeDate.Date).ToString()),
                                                                ClientFullName = n.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? n.MemberClientInformation.FirstNameAmh + " " +
                                                                                                                                                          n.MemberClientInformation.FatherNameAmh + " " +
                                                                                                                                                          n.MemberClientInformation.GrandFatherNameAmh :
                                                                                                                                                          n.MemberClientInformation.OrganizationNameAmh,

                                                                CommodityName = n.CommodityGrade.CommodityType.Commodity.DescriptionAmh,
                                                                MemberClientTradeDetailId = n.MemberClientTradeDetailId,
                                                                BayerCommodityGrade = n.CommodityGrade.DescriptionAmh,
                                                                BayerCommodityType = n.CommodityGrade.CommodityType.DescriptionAmh,
                                                                BayerOrderPrice = n.OrderPrice,
                                                                BayerUnitPrice = n.UnitPrice,
                                                                CommissionInPercent = n.CommissionInPrice,
                                                                Reperesentative = n.OwnerManager.FirstNameAmh + " " + n.OwnerManager.FatherNameAmh + " " + n.OwnerManager.GrandFatherNameAmh,
                                                                Warehouse = n.Warehouse,
                                                                LotQuantity = n.Quantity
                                                            })
                                                            .AsNoTracking()
                                                             .ToListAsync();
                    return clientTrade;
                }
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<BoughtSelfTradeExecutionDTO>> GetAllBoughtSelfTradeEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId== 88)
                {
                    var clientTrade = await context.MemberClientTradeDetail.Where(
                                                x => x.MemberClientTrade.ReportTypeId == 1 &&
                                                x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                     ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                     ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&
                    ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                   ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                   ((clientTradeQueryParameters.IsTradeExcutionAccomplished == 0) || (x.MemberClientTrade.IsTradeExcutionAccomplished == clientTradeQueryParameters.IsTradeExcutionAccomplished)) &&
                   ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                   ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                    ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport)))
                                         .Include(y => y.CommodityGrade)
                                         .Select(n => new BoughtSelfTradeExecutionDTO
                                         {
                                             OrganizationName = n.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                             TradeDate = n.TradeDate.ToString(),
                                             ClientFullName = n.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? n.MemberClientInformation.FirstNameEng + " " +
                                                                                                                                       n.MemberClientInformation.FatherNameEng + " " +
                                                                                                                                       n.MemberClientInformation.GrandFatherNameEng :
                                                                                                                                       n.MemberClientInformation.OrganizationNameEng,
                                             MemberClientTradeDetailId = n.MemberClientTradeDetailId,
                                             BayerCommodityGrade = n.CommodityGrade.DescriptoinEng,
                                             BayerCommodityType = n.CommodityGrade.CommodityType.DescriptionEng,
                                             BayerOrderPrice = n.OrderPrice,
                                             BayerUnitPrice = n.UnitPrice,
                                             CommissionInPercent = n.CommissionInPrice,
                                             Reperesentative = n.OwnerManager.FirstNameEng + " " + n.OwnerManager.FatherNameEng + " " + n.OwnerManager.GrandFatherNameEng,
                                             Warehouse = n.Warehouse,
                                             LotQuantity = n.Quantity
                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                    return clientTrade;
                }
                else
                {
                    var clientTrade = await context.MemberClientTradeDetail.Where(
                                                                    x => x.MemberClientTrade.ReportTypeId == 1 &&
                                                                    x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                         ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                         ((clientTradeQueryParameters.Year == 0) || (x.MemberClientTrade.Year == clientTradeQueryParameters.Year)) &&
                                        ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                       ((clientTradeQueryParameters.TradeType == 0) || (x.TradeTypeId == clientTradeQueryParameters.TradeType)) &&
                                       ((clientTradeQueryParameters.IsTradeExcutionAccomplished == 0) || (x.MemberClientTrade.IsTradeExcutionAccomplished == clientTradeQueryParameters.IsTradeExcutionAccomplished)) &&
                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                        ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport)))
                                                             .Include(y => y.CommodityGrade)
                                                             .Select(n => new BoughtSelfTradeExecutionDTO
                                                             {
                                                                 OrganizationName = n.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                 TradeDate = n.TradeDate.ToString(),
                                                                 ClientFullName = n.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? n.MemberClientInformation.FirstNameEng + " " +
                                                                                                                                                           n.MemberClientInformation.FatherNameEng + " " +
                                                                                                                                                           n.MemberClientInformation.GrandFatherNameEng :
                                                                                                                                                           n.MemberClientInformation.OrganizationNameEng,
                                                                 MemberClientTradeDetailId = n.MemberClientTradeDetailId,
                                                                 BayerCommodityGrade = n.CommodityGrade.DescriptoinEng,
                                                                 BayerCommodityType = n.CommodityGrade.CommodityType.DescriptionEng,
                                                                 BayerOrderPrice = n.OrderPrice,
                                                                 BayerUnitPrice = n.UnitPrice,
                                                                 CommissionInPercent = n.CommissionInPrice,
                                                                 Reperesentative = n.OwnerManager.FirstNameEng + " " + n.OwnerManager.FatherNameEng + " " + n.OwnerManager.GrandFatherNameEng,
                                                                 Warehouse = n.Warehouse,
                                                                 LotQuantity = n.Quantity
                                                             })
                                                             .AsNoTracking()
                                                              .ToListAsync();
                    return clientTrade;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<SoldSelfTradeExecutionDTO>> GetAllSoldSelfTrade(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientTrade = await context.MemberClientTradeDetail.Where(
                    x => x.TradeTypeId == 2 &&
                    (string.IsNullOrEmpty(clientTradeQueryParameters.From) ||
                    x.MemberClientTrade.ReportDate >= DateTime.Parse(clientTradeQueryParameters.From)) &&
                    (string.IsNullOrEmpty(clientTradeQueryParameters.To) ||
                    x.MemberClientTrade.ReportDate <= DateTime.Parse(clientTradeQueryParameters.To)) &&
                    ((clientTradeQueryParameters.TradeExcutionReport == 0) || (x.TradeExcutionReport == clientTradeQueryParameters.TradeExcutionReport))
                    )
                                         .Include(y => y.CommodityGrade)
                                         .Select(n => new SoldSelfTradeExecutionDTO
                                         {
                                             TradeDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.TradeDate.Date).ToString()),
                                             SellerCommodityGrade = n.CommodityGrade.DescriptionAmh,
                                             SellerCommodityType = n.CommodityGrade.CommodityType.DescriptionAmh,
                                             SellerOrderPrice = n.OrderPrice,
                                             SellerUnitPrice = n.UnitPrice,
                                             CommissionInPercent = n.CommissionInPrice,
                                             Reperesentative = n.OwnerManager.FirstNameAmh + " " + n.OwnerManager.FatherNameAmh + " " + n.OwnerManager.GrandFatherNameAmh,
                                             Warehouse = n.Warehouse,
                                             LotQuantity = n.Quantity

                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                return clientTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<MemberTradeDetailViewModel>> GetTradeExcutionById(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var excutionTrade = await context.MemberClientTradeDetail
                    .Where(w => w.MemberClientTradeId == clientTradeQueryParameters.MemberClientTradeId)
                    .Select(x => new MemberTradeDetailViewModel
                    {
                        ExchangeActorId = x.MemberClientTrade.ExchangeActorId,
                        MemberClientTradeId = x.MemberClientTrade.MemberClientTradeId,
                        MemberClientTradeDetailId = x.MemberClientTradeDetailId,
                        MemberClientInformationId = x.MemberClientInformationId,
                        ClientFullName = x.MemberClientInformation.ClientTypeId == (int)Enum.ClientType.Individual ? ((clientTradeQueryParameters.Lang == "et") ? x.MemberClientInformation.FirstNameAmh + " " + 
                                                                                                                                       x.MemberClientInformation.FatherNameAmh + " " + 
                                                                                                                                       x.MemberClientInformation.GrandFatherNameAmh :
                                                                                                                                       x.MemberClientInformation.FirstNameEng + " " + 
                                                                                                                                       x.MemberClientInformation.FatherNameEng + " " +
                                                                                                                                       x.MemberClientInformation.GrandFatherNameEng) : 
                                                                                                                                       ((clientTradeQueryParameters.Lang == "et") ? x.MemberClientInformation.OrganizationNameAmh :
                                                                                                                                       x.MemberClientInformation.OrganizationNameEng),

                        OwnerManagerId = x.OwnerManagerId,
                        FloorRepresentativeName = (clientTradeQueryParameters.Lang == "et") ? x.OwnerManager.FirstNameAmh + " " +
                                                                   x.OwnerManager.FatherNameAmh + " " +
                                                                   x.OwnerManager.GrandFatherNameAmh :
                                                                   x.OwnerManager.FirstNameEng + " " +
                                                                   x.OwnerManager.FatherNameEng + " " +
                                                                   x.OwnerManager.GrandFatherNameEng,
                        CommodityName = (clientTradeQueryParameters.Lang == "et") ? x.CommodityGrade.CommodityType.Commodity.DescriptionAmh :
                                                         x.CommodityGrade.CommodityType.Commodity.DesciptionEng,
                        CommodityId = x.CommodityId,
                        CommodityTypeName = (clientTradeQueryParameters.Lang == "et") ? x.CommodityGrade.CommodityType.DescriptionAmh :
                                                             x.CommodityGrade.CommodityType.DescriptionEng,
                        CommudityTypeId = x.CommudityTypeId,
                        CommodityGradeName = (clientTradeQueryParameters.Lang == "et") ? x.CommodityGrade.DescriptionAmh :
                                                             x.CommodityGrade.DescriptoinEng,
                        CommodityGradeId = x.CommodityGradeId,
                        CommissionInPrice = x.CommissionInPrice,
                        ClientTradeRemark = x.ClientTradeRemark,
                        OrderPrice = x.OrderPrice,
                        TradeExcutionReport = x.TradeExcutionReport,
                        UnitMeasurementId = x.UnitMeasurementId,
                        Warehouse = x.Warehouse,
                        TradeTypeId = x.TradeTypeId,
                        Quantity = x.Quantity,
                        TradeDate = x.TradeDate,
                        DateTrade = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.TradeDate.Date).ToString()) :
                                                     x.TradeDate.Date.ToString(),
                        UnitPrice = x.UnitPrice
                    })
                    .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                    .AsNoTracking()
                    .ToListAsync();
                int totalResultList = excutionTrade.Count();
                return new PagedResult<MemberTradeDetailViewModel>()
                {
                    Items = mapper.Map<List<MemberTradeDetailViewModel>>(excutionTrade),
                    ItemsCount = context.MemberClientInformation
                    .Where(w => w.MemberClientTradeId == clientTradeQueryParameters.MemberClientTradeId).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<MemberTradeDetailViewModel> GetTradeExcutionDetailById(int memberClientId)
        {
            try
            {
                var clientInfo = await context.MemberClientTradeDetail
                    .Where(x => x.MemberClientTradeDetailId == memberClientId)
                    .Select(x => new MemberTradeDetailViewModel
                    {
                        ExchangeActorId = x.MemberClientTrade.ExchangeActorId,
                        MemberClientTradeDetailId = x.MemberClientTradeDetailId,
                        MemberClientInformationId = x.MemberClientInformationId,
                        ClientFullName = x.MemberClientInformation.FirstNameAmh + " " + x.MemberClientInformation.FatherNameAmh + " " + x.MemberClientInformation.GrandFatherNameAmh,
                        OwnerManagerId = x.OwnerManagerId,
                        FloorRepresentativeName = x.OwnerManager.FirstNameAmh + " " + x.OwnerManager.FatherNameAmh + " " + x.OwnerManager.GrandFatherNameAmh,
                        CommodityName = x.CommodityGrade.CommodityType.Commodity.DescriptionAmh,
                        CommodityId = x.CommodityId,
                        CommodityTypeName = x.CommodityGrade.CommodityType.DescriptionAmh,
                        CommudityTypeId = x.CommudityTypeId,
                        CommodityGradeName = x.CommodityGrade.DescriptionAmh,
                        CommodityGradeId = x.CommodityGradeId,
                        CommissionInPrice = x.CommissionInPrice,
                        ClientTradeRemark = x.ClientTradeRemark,
                        MemberTradeRemark = x.MemberTradeRemark,
                        OrderPrice = x.OrderPrice,
                        TradeExcutionReport = x.TradeExcutionReport,
                        UnitMeasurementId = x.UnitMeasurementId,
                        Warehouse = x.Warehouse,
                        TradeTypeId = x.TradeTypeId,
                        Quantity = x.Quantity,
                        TradeDate = x.TradeDate,
                        UnitPrice = x.UnitPrice
                    })
                    .FirstOrDefaultAsync();
                return clientInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateTradeExcution(MemberClientTradeDetailDTO memberClientTradeDetail)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tradeDetail = await context.MemberClientTradeDetail
                    .Where(x => x.MemberClientTradeDetailId == memberClientTradeDetail.MemberClientTradeDetailId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                    if (tradeDetail != null)
                    {
                        var updateTradeExcution = mapper.Map(memberClientTradeDetail, tradeDetail);
                        context.Entry(updateTradeExcution).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                        //result = memberClient.MemberClientTradeId;
                    }
                    return tradeDetail.MemberClientTradeId;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<int> DeleteTradeExcution(MemberClientTradeDetailDTO memberClientTradeDetail)
        {
            try
            {
                var tradeDetail = await context.MemberClientTradeDetail
                    .Where(x => x.MemberClientTradeDetailId == memberClientTradeDetail.MemberClientTradeDetailId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (tradeDetail != null)
                {
                    var deleteTradeExcution = mapper.Map(memberClientTradeDetail, tradeDetail);
                    context.Entry(deleteTradeExcution).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return tradeDetail.MemberClientTradeDetailId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }    
        public List<ComparisonOffSiteMonitoring> GetTradeAccomplishedComparatives(ComparativeQueryParameterSearch comparativeQueryParameter)
        {
            try
            {
                List<ComparisonOffSiteMonitoring> comparisonOffSites = new List<ComparisonOffSiteMonitoring>();
                var totalTradeAccomplished = context.MemberClientTradeDetail.Include(x=>x.MemberClientTrade).Where(x => x.MemberClientTrade.IsTradeExcutionAccomplished == 1 &&
                                                                              (comparativeQueryParameter.CustomerTypeId == 0 || (x.MemberClientTrade.ExchangeActor.MemberCategoryId == comparativeQueryParameter.CustomerTypeId ||
                                                                                                                                 x.MemberClientTrade.ExchangeActor.CustomerTypeId == comparativeQueryParameter.CustomerTypeId)) &&
                                                                              (comparativeQueryParameter.ToWhomDoesTradeId == 0 || x.TradeExcutionReport == comparativeQueryParameter.ToWhomDoesTradeId) &&
                                                                              (String.IsNullOrEmpty(comparativeQueryParameter.EcxCode) || x.MemberClientTrade.ExchangeActor.Ecxcode == comparativeQueryParameter.EcxCode) &&
                                                                              (comparativeQueryParameter.BussinessFieldId == 0 || x.MemberClientTrade.ExchangeActor.BuisnessFiledId == comparativeQueryParameter.BussinessFieldId) &&
                                                                              //(comparativeQueryParameter.Quantity == 0 || x.Quantity == comparativeQueryParameter.Quantity) &&
                                                                              //(comparativeQueryParameter.UnitPrice == 0 || x.UnitPrice == comparativeQueryParameter.UnitPrice) &&
                                                                              (comparativeQueryParameter.UnitMeasurementId == 0 || x.UnitMeasurementId == comparativeQueryParameter.UnitMeasurementId) &&
                                                                              (comparativeQueryParameter.CommodityId == 0 || x.CommodityId == comparativeQueryParameter.CommodityId))
                                                                              .AsNoTracking()
                                                                              .ToList();
                if (totalTradeAccomplished.Count() == 0)
                {
                    return null;
                }
                ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                var compOffSiteMonitor = totalTradeAccomplished.Select(x => new { x.MemberClientTrade.Year }).Distinct();
                int totalFirstQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.ReportPeriodId == 1).Count();
                if (totalFirstQuarter == 0)
                {
                    totalFirstQuarter = 1;
                }
                int totalSecondQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.ReportPeriodId == 2).Count();
                if (totalSecondQuarter == 0)
                {
                    totalSecondQuarter = 1;
                }
                int totalThiredQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.ReportPeriodId == 3).Count();
                if (totalThiredQuarter == 0)
                {
                    totalThiredQuarter = 1;
                }
                int totalFourthQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.ReportPeriodId == 4).Count();
                if (totalFourthQuarter == 0)
                {
                    totalFourthQuarter = 1;
                }
                foreach (var name in compOffSiteMonitor)
                {
                    offSiteMonitoring.Year = name.Year;
                    offSiteMonitoring.FirstQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                           x.MemberClientTrade.ReportPeriodId == 1).Count();
                    offSiteMonitoring.SecondQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                              x.MemberClientTrade.ReportPeriodId == 2).Count();
                    offSiteMonitoring.ThiredQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                            x.MemberClientTrade.ReportPeriodId == 3).Count();
                    offSiteMonitoring.FourthQuarter = totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                              x.MemberClientTrade.ReportPeriodId == 4).Count();
                    offSiteMonitoring.PercentageFirstQuarter = (totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                           x.MemberClientTrade.ReportPeriodId == 1).Count()) * 100 / (totalFirstQuarter);
                    offSiteMonitoring.PercentageSecondQuarter = (totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                              x.MemberClientTrade.ReportPeriodId == 2).Count()) * 100 / (totalSecondQuarter);
                    offSiteMonitoring.PercentageThiredQuarter = (totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                            x.MemberClientTrade.ReportPeriodId == 3).Count()) * 100 / (totalThiredQuarter);
                    offSiteMonitoring.PercentageFourthQuarter = (totalTradeAccomplished.Where(x => x.MemberClientTrade.Year == name.Year &&
                                                                              x.MemberClientTrade.ReportPeriodId == 4).Count()) * 100 / (totalFourthQuarter);
                    comparisonOffSites.Add(offSiteMonitoring);
                }

                return comparisonOffSites;
            }
            catch(Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
     public List<TradeExecutionOrderViewModel> GetListOfTradeExecutionOreder(TradeExecutionOrderSearchQuery tradeExecution)
        {
            try
            {
                List<TradeExecutionOrderViewModel> data;
                if(tradeExecution.CommodityId != 0)
                {
                    if (tradeExecution.Lang == "et")
                    {
                        if (tradeExecution.OrderTypeId == "DESC")
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                           and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                tradeExecution.ToWhomDoesTradeId,
                                                                                tradeExecution.TopLevel
                                                                                 )
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                            and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                           and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }
                            //return data;
                        }
                        else
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                           and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel
                                                                                 )
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                           and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }
                        //return data;
                    }
                    else
                    {
                        if (tradeExecution.OrderTypeId == "DESC")
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 // tradeExecution.OrderTypeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 // tradeExecution.OrderTypeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 //  tradeExecution.OrderTypeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }
                        else
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where mctd.CommodityId = {0} and mct.ReportPeriodId = {1} and mct.Year = {2}
                                                                          and mctd.TradeExcutionReport = {3}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {4} ROWS ONLY",
                                                                                 tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }



                    }

                }
                else
                {
                    if (tradeExecution.Lang == "et")
                    {
                        if (tradeExecution.OrderTypeId == "DESC")
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                           and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                tradeExecution.ToWhomDoesTradeId,
                                                                                tradeExecution.TopLevel
                                                                                 )
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                            and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                           and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount desc
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 //tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }
                            //return data;
                        }
                        else
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                           and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel
                                                                                 )
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                          and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameAmh as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                           and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameAmh
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 //tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }
                        //return data;
                    }
                    else
                    {
                        if (tradeExecution.OrderTypeId == "DESC")
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1} and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1} and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 //tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1} and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount DESC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }
                        else
                        {
                            switch (tradeExecution.TradeOrderNameId)
                            {
                                case 1:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.UnitPrice) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                          and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 2:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,SUM(mctd.Quantity) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                          and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                 //tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                case 3:
                                    data = context.TradeExecutionOrderViewModel.FromSqlRaw
                                                                                (@"Select exc.OrganizationNameEng as OrganizationName,COUNT(mct.ExchangeActorId) as Amount from MemberClientTradeDetail mctd
                                                                          join MemberClientTrade mct on mctd.MemberClientTradeId = mct.MemberClientTradeId
                                                                          Join ExchangeActor exc on mct.ExchangeActorId = exc.ExchangeActorId
                                                                          Where  mct.ReportPeriodId = {0} and mct.Year = {1}
                                                                          and mctd.TradeExcutionReport = {2}
                                                                          Group by mct.ExchangeActorId,exc.OrganizationNameEng
                                                                          order by Amount ASC
                                                                          OFFSET 0 ROWS FETCH NEXT {3} ROWS ONLY",
                                                                                // tradeExecution.CommodityId,
                                                                                 tradeExecution.ReportPeriodId,
                                                                                 tradeExecution.Year,
                                                                                 tradeExecution.ToWhomDoesTradeId,
                                                                                 tradeExecution.TopLevel)
                                                                                .AsNoTracking()
                                                                                .ToList();
                                    break;
                                default:
                                    data = null;
                                    break;
                            }

                        }



                    }

                }

                return data;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
