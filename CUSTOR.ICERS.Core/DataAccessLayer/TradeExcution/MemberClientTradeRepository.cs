﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public partial class MemberClientTradeRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper mapper;

        public MemberClientTradeRepository(ECEADbContext context, IMapper _mapper)
        {
            _context = context;
            mapper = _mapper;
        }

        public async Task<int> CreateMemberClinetReg(TradeExcutionReportDTO memberClient)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                //    var response = new ResultResponse();
                try
                {
                    double NrOfDays = 0;
                    DateTime reportDate = DateTime.MinValue;
                    DateTime deadLine = DateTime.MinValue;
                    var deadLineObj = _context.ReportPeriod.Where(
                    x => x.ReportPeriodId == memberClient.ReportPeriodId)
                    .AsNoTracking()
                    .FirstOrDefault();
                    if (deadLineObj != null)
                    {
                        deadLine = deadLineObj.StartDate;
                    }
                    reportDate = memberClient.ReportDate;
                    TimeSpan t = deadLine - reportDate;
                    NrOfDays = t.TotalDays;
                    if (NrOfDays < 0)
                    {
                        var clientTradeId = 0;

                        var sendToApprove = await _context.MemberClientTrade
                                                    .FirstOrDefaultAsync(m => m.ExchangeActorId == memberClient.ExchangeActorId
                                                    && m.ReportTypeId == memberClient.ReportTypeId &&
                                                    m.ReportPeriodId == memberClient.ReportPeriodId &&
                                                    m.Year == memberClient.Year &&
                                                    m.TradeExcutionStatusTypeId == 2);
                        if (sendToApprove == null)
                        {
                            var memberClientInDb = await _context.MemberClientTrade
                                                                            .FirstOrDefaultAsync(m => m.ExchangeActorId == memberClient.ExchangeActorId
                                                                            && m.ReportTypeId == memberClient.ReportTypeId &&
                                                                            m.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                            m.Year == memberClient.Year);
                            if (memberClientInDb == null)
                            {
                                var reportPeriod = await _context.ReportPeriod
                                                    .Where(x => x.ReportPeriodId == memberClient.ReportPeriodId)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync();
                                MemberClientTrade clientTrade = mapper.Map<MemberClientTrade>(memberClient);
                                if (reportPeriod != null)
                                {
                                    clientTrade.DeadLine = reportPeriod.DeadLine;
                                    clientTrade.StartDate = reportPeriod.StartDate;
                                }
                                _context.Entry(clientTrade).State = EntityState.Added;
                                foreach (var item in clientTrade.MemberClientInformation)
                                {
                                    _context.Entry(item).State = EntityState.Added;
                                    foreach (var commodity in item.ClientProduct)
                                    {
                                        _context.Entry(commodity).State = EntityState.Added;
                                    }
                                }

                                _context.SaveChanges();
                                transaction.Commit();
                                clientTradeId = clientTrade.MemberClientTradeId;
                            }
                            else
                            {
                                foreach (var item in memberClient.MemberClientInformation.ToList())
                                {
                                    item.MemberClientTradeId = memberClientInDb.MemberClientTradeId;
                                    MemberClientInformation mappedDetail = mapper.Map<MemberClientInformation>(item);
                                    _context.MemberClientInformation.Add(mappedDetail);
                                    foreach (var product in mappedDetail.ClientProduct)
                                    {
                                        _context.Entry(product).State = EntityState.Added;
                                    }
                                }

                                _context.SaveChanges();
                                transaction.Commit();
                                clientTradeId = memberClientInDb.MemberClientTradeId;
                            }


                            return clientTradeId;

                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }


                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public async Task<int> UpdateMemberTradeDetail(TradeExcutionReportDTO memberClient)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = 0;
                    var memberClientInDb = await _context.MemberClientTrade
                                  .Where(m => m.MemberClientTradeId == memberClient.MemberClientTradeId)
                                  .AsNoTracking()
                                  .FirstOrDefaultAsync();

                    if (memberClientInDb != null)
                    {
                        var updatedMemberClient = mapper.Map(memberClient, memberClientInDb);
                        _context.Entry(updatedMemberClient).State = EntityState.Modified;
                        foreach (var workFlow in updatedMemberClient.WorkFlow)
                        {
                            //workFlow.MemberClientTradeId = memberClientInDb.MemberClientTradeId;
                            _context.Entry(workFlow).State = EntityState.Added;
                        }
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        result = memberClient.MemberClientTradeId;
                    }
                    return result;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> UpdateMemberClientInfo(TradeExcutionReportDTO memberClient)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = 0;
                    var memberClientInDb = await _context.MemberClientTrade
                                  .Where(m => m.MemberClientTradeId == memberClient.MemberClientTradeId)
                                  .AsNoTracking()
                                  .FirstOrDefaultAsync();

                    if (memberClientInDb != null)
                    {
                        var updatedMemberClient = mapper.Map(memberClient, memberClientInDb);
                        _context.Entry(updatedMemberClient).State = EntityState.Modified;
                        foreach (var workFlow in updatedMemberClient.WorkFlow)
                        {
                            //workFlow.MemberClientTradeId = memberClientInDb.MemberClientTradeId;
                            _context.Entry(workFlow).State = EntityState.Added;
                        }
                        await _context.SaveChangesAsync();
                        transaction.Commit();
                        result = memberClient.MemberClientTradeId;
                    }
                    return result;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> CreateMemberTradeDetail(TradeExcutionReportDTO memberClient)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {

                    double NrOfDays = 0;
                    DateTime reportDate = DateTime.MinValue;
                    DateTime deadLine = DateTime.MinValue;
                    var deadLineObj = _context.ReportPeriod.Where(
                    x => x.ReportPeriodId == memberClient.ReportPeriodId)
                    .AsNoTracking()
                    .FirstOrDefault();
                    if (deadLineObj != null)
                    {
                        deadLine = deadLineObj.StartDate;
                    }
                    reportDate = memberClient.ReportDate;
                    TimeSpan t = deadLine - reportDate;
                    NrOfDays = t.TotalDays;
                    if (NrOfDays < 0)
                    {
                        var clientTradeId = 0;
                        if (memberClient.MemberClientTradeDetail != null)
                        {
                            var detilClient = memberClient.MemberClientTradeDetail.FirstOrDefault();
                            var notAccomplished = await _context.MemberClientTrade
                                                        .FirstOrDefaultAsync(y => y.ExchangeActorId == memberClient.ExchangeActorId &&
                                                                              y.IsTradeExcutionAccomplished == 2 &&
                                                                             (y.TradeExcutionNotAccomplish == 3 || y.TradeExcutionNotAccomplish == detilClient.TradeExcutionReport) &&
                                                                              y.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                              y.ReportTypeId == memberClient.ReportTypeId &&
                                                                              y.Year == memberClient.Year);
                            if (notAccomplished == null)
                            {
                                var sendOrApproved = await _context.MemberClientTrade
                                    .FirstOrDefaultAsync(sa => sa.ReportTypeId == memberClient.ReportTypeId &&
                                    (sa.TradeExcutionStatusTypeId == 2 ||
                                    sa.TradeExcutionStatusTypeId == 3) &&
                                    sa.ExchangeActorId == memberClient.ExchangeActorId &&
                                    sa.ReportPeriodId == memberClient.ReportPeriodId &&
                                    sa.ReportTypeId == memberClient.ReportTypeId &&
                                    sa.Year == memberClient.Year);
                                if (sendOrApproved == null)
                                {
                                    var memberClientInDb = await _context.MemberClientTrade
                                                                          .FirstOrDefaultAsync(m => m.ExchangeActorId == memberClient.ExchangeActorId
                                                                          && m.ReportTypeId == memberClient.ReportTypeId &&
                                                                          m.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                          m.Year == memberClient.Year);
                                    if (memberClientInDb == null)
                                    {
                                        var reportPeriod = await _context.ReportPeriod
                                                      .Where(x => x.ReportPeriodId == memberClient.ReportPeriodId)
                                                      .AsNoTracking()
                                                      .FirstOrDefaultAsync();
                                        MemberClientTrade clientTrade = mapper.Map<MemberClientTrade>(memberClient);
                                        if (reportPeriod != null)
                                        {
                                            clientTrade.DeadLine = reportPeriod.DeadLine;
                                            clientTrade.StartDate = reportPeriod.StartDate;
                                        }
                                        _context.Entry(clientTrade).State = EntityState.Added;
                                        foreach (var item in clientTrade.MemberClientTradeDetail)
                                        {
                                            _context.Entry(item).State = EntityState.Added;
                                        }
                                        _context.SaveChanges();
                                        transaction.Commit();
                                        clientTradeId = clientTrade.MemberClientTradeId;
                                    }
                                    else
                                    {
                                        foreach (var item in memberClient.MemberClientTradeDetail.ToList())
                                        {
                                            item.MemberClientTradeId = memberClientInDb.MemberClientTradeId;
                                            MemberClientTradeDetail mappedDetail = mapper.Map<MemberClientTradeDetail>(item);
                                            _context.MemberClientTradeDetail.Add(mappedDetail);
                                        }

                                        _context.SaveChanges();
                                        transaction.Commit();
                                        clientTradeId = memberClientInDb.MemberClientTradeId;

                                    }

                                }
                                else
                                {
                                    return 0;
                                }


                            }
                            else
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            var tradeDetail = await _context.MemberClientTradeDetail.Where(x => x.MemberClientTrade.ExchangeActorId == memberClient.ExchangeActorId &&
                                                                                               x.MemberClientTrade.Year == memberClient.Year &&
                                                                                               x.MemberClientTrade.ReportTypeId == memberClient.ReportTypeId &&
                                                                                               x.MemberClientTrade.ReportPeriodId == memberClient.ReportPeriodId)
                                                                                                .FirstOrDefaultAsync();
                            if (tradeDetail != null)
                            {
                                var tradeIsAccomplish = await _context.MemberClientTrade.Where(x => x.ExchangeActorId == memberClient.ExchangeActorId &&
                                                                                                                             x.Year == memberClient.Year &&
                                                                                                                             x.ReportTypeId == memberClient.ReportTypeId &&
                                                                                                                             x.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                                                                             x.TradeExcutionNotAccomplish == tradeDetail.TradeExcutionReport)
                                                                                                                             .FirstOrDefaultAsync();
                                if (tradeIsAccomplish == null)
                                {
                                    var notAccomplished = await _context.MemberClientTrade
                                                                 .FirstOrDefaultAsync(y => y.ExchangeActorId == memberClient.ExchangeActorId &&
                                                                  y.IsTradeExcutionAccomplished == 2 &&
                                                                  (y.TradeExcutionStatusTypeId == 1 || y.TradeExcutionStatusTypeId == 2) &&
                                                                   y.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                   y.ReportTypeId == memberClient.ReportTypeId &&
                                                                    y.Year == memberClient.Year);
                                    if (notAccomplished == null)
                                    {
                                        var reportPeriod = await _context.ReportPeriod
                                                                                             .Where(x => x.ReportPeriodId == memberClient.ReportPeriodId)
                                                                                             .AsNoTracking()
                                                                                             .FirstOrDefaultAsync();
                                        MemberClientTrade clientTrade = mapper.Map<MemberClientTrade>(memberClient);
                                        if (reportPeriod != null)
                                        {
                                            clientTrade.DeadLine = reportPeriod.DeadLine;
                                            clientTrade.StartDate = reportPeriod.StartDate;
                                        }
                                        _context.Entry(clientTrade).State = EntityState.Added;
                                        _context.SaveChanges();
                                        transaction.Commit();
                                        clientTradeId = clientTrade.MemberClientTradeId;
                                    }
                                    else
                                    {
                                        return 0;
                                    }

                                }
                                else
                                {
                                    return 0;
                                }
                            }
                            else
                            {
                                var notAccomplished = await _context.MemberClientTrade
                                                                     .FirstOrDefaultAsync(y => y.ExchangeActorId == memberClient.ExchangeActorId &&
                                                                      y.IsTradeExcutionAccomplished == 2 &&
                                                                      (y.TradeExcutionStatusTypeId == 1 || y.TradeExcutionStatusTypeId == 2) &&
                                                                       y.ReportPeriodId == memberClient.ReportPeriodId &&
                                                                       y.ReportTypeId == memberClient.ReportTypeId &&
                                                                        y.Year == memberClient.Year);
                                if (notAccomplished == null)
                                {
                                    var reportPeriod = await _context.ReportPeriod
                                                                                         .Where(x => x.ReportPeriodId == memberClient.ReportPeriodId)
                                                                                         .AsNoTracking()
                                                                                         .FirstOrDefaultAsync();
                                    MemberClientTrade clientTrade = mapper.Map<MemberClientTrade>(memberClient);
                                    if (reportPeriod != null)
                                    {
                                        clientTrade.DeadLine = reportPeriod.DeadLine;
                                        clientTrade.StartDate = reportPeriod.StartDate;
                                    }
                                    _context.Entry(clientTrade).State = EntityState.Added;
                                    _context.SaveChanges();
                                    transaction.Commit();
                                    clientTradeId = clientTrade.MemberClientTradeId;
                                }
                                else
                                {
                                    return 0;
                                }

                            }
                        }

                        return clientTradeId;
                    }
                    else
                    {
                        return 0;

                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }


            }
        }


        public async Task<int> CreateMemberFinancial(TradeExcutionReportDTO memberClient)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {

                try
                {
                    double NrOfDays = 0;
                    DateTime reportDate = DateTime.MinValue;
                    DateTime deadLine = DateTime.MinValue;
                    var deadLineObj = _context.ReportPeriod.Where(
                    x => x.ReportPeriodId == memberClient.ReportPeriodId)
                    .AsNoTracking()
                    .FirstOrDefault();
                    if (deadLineObj != null)
                    {
                        deadLine = deadLineObj.StartDate;
                    }
                    reportDate = memberClient.ReportDate;
                    TimeSpan t = deadLine - reportDate;
                    NrOfDays = t.TotalDays;
                    if (NrOfDays < 0)
                    {
                        var clientTradeId = 0;
                        var sendOrApproved = await _context.MemberClientTrade
                                .Where(sa => sa.ReportTypeId == 2 &&
                                (sa.TradeExcutionStatusTypeId == 2 ||
                                sa.TradeExcutionStatusTypeId == 3) &&
                                sa.ExchangeActorId == memberClient.ExchangeActorId &&
                                sa.ReportPeriodId == memberClient.ReportPeriodId &&
                                sa.Year == memberClient.Year)
                                .FirstOrDefaultAsync();
                        if (sendOrApproved == null)
                        {

                            var memberClientInDb = await _context.MemberClientTrade
                                                              .FirstOrDefaultAsync(
                                                               m => m.ExchangeActorId == memberClient.ExchangeActorId
                                                              && m.ReportTypeId == memberClient.ReportTypeId
                                                              && m.ReportPeriodId == memberClient.ReportPeriodId);
                            if (memberClientInDb == null)
                            {
                                var reportPeriod = await _context.ReportPeriod
                                                  .Where(x => x.ReportPeriodId == memberClient.ReportPeriodId)
                                                  .AsNoTracking()
                                                  .FirstOrDefaultAsync();
                                MemberClientTrade clientTrade = mapper.Map<MemberClientTrade>(memberClient);
                                if (reportPeriod != null)
                                {
                                    clientTrade.DeadLine = reportPeriod.DeadLine;
                                    clientTrade.StartDate = reportPeriod.StartDate;
                                }

                                _context.Entry(clientTrade).State = EntityState.Added;
                                foreach (var financial in clientTrade.MemberTradeFinancial)
                                {
                                    _context.Entry(financial).State = EntityState.Added;
                                }
                                _context.SaveChanges();
                                transaction.Commit();
                                clientTradeId = clientTrade.MemberClientTradeId;
                            }
                            else
                            {
                                foreach (var financial in memberClient.MemberTradeFinancial)
                                {
                                    var financialReport = _context.MemberTradeFinancial.Where(
                                       f => f.MemberClientTradeId == memberClientInDb.MemberClientTradeId &&
                                      (f.MemberClientTrade.ExchangeActorId == memberClientInDb.ExchangeActorId) &&
                                      (f.MemberClientTrade.Year == memberClientInDb.Year) &&
                                      (f.MemberClientTrade.ReportPeriodId == memberClientInDb.ReportPeriodId))
                                        .FirstOrDefault();
                                    if (financialReport == null)
                                    {
                                        financial.MemberClientTradeId = memberClientInDb.MemberClientTradeId;
                                        MemberTradeFinancial tradeFinancial = mapper.Map<MemberTradeFinancial>(financial);
                                        _context.MemberTradeFinancial.Add(tradeFinancial);
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                }
                                _context.SaveChanges();
                                transaction.Commit();
                                clientTradeId = memberClientInDb.MemberClientTradeId;
                            }
                            return clientTradeId;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
        }
        public async Task<List<MemberClientTradeDTO>> GetAllMemberClient(int memberClientTradeId)
        {
            try
            {
                var memberClient = await _context.MemberClientTrade.Where(
                    x => x.TradeExcutionStatusTypeId == 2 && x.MemberClientTradeId == memberClientTradeId)
                    .Select(f => new MemberClientTradeDTO
                    {
                        MemberClientTradeId = f.MemberClientTradeId,
                        CustomerFullName = f.ExchangeActor.OwnerManager.FirstNameAmh + " " + f.ExchangeActor.OwnerManager.FatherNameAmh + " " + f.ExchangeActor.OwnerManager.GrandFatherNameAmh,
                        ExchangeActorId = f.ExchangeActorId,
                        OrganizationName = f.ExchangeActor.OrganizationNameAmh,
                        CustomerType = f.ExchangeActor.MemberCategory.DescriptionAmh,
                        MobilePhone = f.ExchangeActor.MobileNo,
                        RegularPhone = f.ExchangeActor.Tel,
                        ReportTypeName = f.ReportType.DescriptionAmh,
                        ReportTypeId = f.ReportTypeId,
                        ReportPeriodId = f.ReportPeriodId,
                        TradeExcutionStatusTypeId = f.TradeExcutionStatusTypeId,
                        IsTradeExcutionAccomplished = f.IsTradeExcutionAccomplished,
                        Year = f.Year,
                        Remark = f.Remark,
                        ReportDate = f.ReportDate,
                        DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(f.ReportDate.Date).ToString()),

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return memberClient;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<StaticData>> GetAllMemberClientList(string lang)
        {
            try
            {
                var memberClient = await _context.MemberClientTrade.Where(
                    x => x.TradeExcutionStatusTypeId == 2)
                    .OrderBy(x => x.ReportDate)
                    .Select(f => new StaticData
                    {
                        Id = f.MemberClientTradeId,
                        Description = lang == "et" ? f.ExchangeActor.OrganizationNameAmh : f.ExchangeActor.OrganizationNameEng

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return memberClient;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PagedResult<MemberClientTradeDTO>> GetTradeExcutionNotAccomplished(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            int totalResultList = 0;
            try
            {
                List<MemberClientTradeDTO> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = await GetTradeExcutionNotAccomplishedAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = await GetTradeExcutionNotAccomplishedEng(clientTradeQueryParameters);
                }
                totalResultList = tradeList.Count();
                var pagedResult = tradeList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return new PagedResult<MemberClientTradeDTO>()
                {
                    Items = mapper.Map<List<MemberClientTradeDTO>>(pagedResult),
                    ItemsCount = totalResultList
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberClientTradeDTO>> GetTradeExcutionNotAccomplishedAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var memberClient = await _context.MemberClientTrade.Where(
                    x => x.IsTradeExcutionAccomplished == 2 &&
                    x.TradeExcutionStatusTypeId == 2 &&
                    x.ReportTypeId == 1 &&
                    ((clientTradeQueryParameters.TradeExcutionNotAccomplish == 0) || (x.TradeExcutionNotAccomplish == clientTradeQueryParameters.TradeExcutionNotAccomplish)) &&
                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                    ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                    ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)))
                    .OrderByDescending(c => c.ReportDate)
                    .Include(c => c.ExchangeActor)
                    .Select(f => new MemberClientTradeDTO
                    {
                        
                        OrganizationName = f.ExchangeActor.OrganizationNameAmh,
                        CustomerType = f.ExchangeActor.MemberCategory.DescriptionAmh,
                        MobilePhone = f.ExchangeActor.MobileNo,
                        RegularPhone = f.ExchangeActor.Tel,
                        ReportDate = f.ReportDate,
                        DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(f.ReportDate.Date).ToString()),
                        Remark = f.Remark

                    })
                    .AsNoTracking()
                    .ToListAsync();
                    return memberClient;

                }
                else
                {
                    var memberClient = await _context.MemberClientTrade.Where(
                                        x => x.IsTradeExcutionAccomplished == 2 &&
                                        x.TradeExcutionStatusTypeId == 2 &&
                                        x.ReportTypeId == 1 &&
                                        ((clientTradeQueryParameters.TradeExcutionNotAccomplish == 0) || (x.TradeExcutionNotAccomplish == clientTradeQueryParameters.TradeExcutionNotAccomplish)) &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                         ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                         ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                         ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                        ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)))
                                        .OrderByDescending(c => c.ReportDate)
                                        .Include(c => c.ExchangeActor)
                                        .Select(f => new MemberClientTradeDTO
                                        {
                                            OrganizationName = f.ExchangeActor.OrganizationNameAmh,
                                            CustomerType = f.ExchangeActor.CustomerType.DescriptionAmh,
                                            MobilePhone = f.ExchangeActor.MobileNo,
                                            RegularPhone = f.ExchangeActor.Tel,
                                            ReportDate = f.ReportDate,
                                            DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(f.ReportDate.Date).ToString()),
                                            Remark = f.Remark

                                        })
                                        .AsNoTracking()
                                        .ToListAsync();
                    return memberClient;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberClientTradeDTO>> GetTradeExcutionNotAccomplishedEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            //int totalResultList = 0;
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var memberClient = await _context.MemberClientTrade.Where(
                                       x => x.IsTradeExcutionAccomplished == 2 &&
                                       x.TradeExcutionStatusTypeId == 2 &&
                                       ((clientTradeQueryParameters.TradeExcutionNotAccomplish == 0) || (x.TradeExcutionNotAccomplish == clientTradeQueryParameters.TradeExcutionNotAccomplish)) &&
                                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                       ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)))
                                       .OrderByDescending(c => c.ReportDate)
                                       .Include(c => c.ExchangeActor)
                                       .Select(f => new MemberClientTradeDTO
                                       {
                                           OrganizationName = f.ExchangeActor.OrganizationNameEng,
                                           CustomerType = f.ExchangeActor.MemberCategory.DescriptionAmh,
                                           MobilePhone = f.ExchangeActor.MobileNo,
                                           RegularPhone = f.ExchangeActor.Tel,
                                           ReportDate = f.ReportDate,
                                           DateReport = f.ReportDate.Date.ToString(),
                                           Remark = f.Remark

                                       })
                                       .AsNoTracking()
                                       .ToListAsync();
                    return memberClient;
                }
                else
                {
                    var memberClient = await _context.MemberClientTrade.Where(
                                                           x => x.IsTradeExcutionAccomplished == 2 &&
                                                           x.TradeExcutionStatusTypeId == 2 &&
                                                           ((clientTradeQueryParameters.TradeExcutionNotAccomplish == 0) || (x.TradeExcutionNotAccomplish == clientTradeQueryParameters.TradeExcutionNotAccomplish)) &&
                                                           ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                           ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)) &&
                                                           ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                           ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                           ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)))
                                                           .OrderByDescending(c => c.ReportDate)
                                                           .Include(c => c.ExchangeActor)
                                                           .Select(f => new MemberClientTradeDTO
                                                           {                                            
                                                               OrganizationName = f.ExchangeActor.OrganizationNameEng,
                                                               CustomerType = f.ExchangeActor.CustomerType.DescriptionAmh,
                                                               MobilePhone = f.ExchangeActor.MobileNo,
                                                               RegularPhone = f.ExchangeActor.Tel,
                                                               ReportDate = f.ReportDate,
                                                               DateReport = f.ReportDate.Date.ToString(),
                                                               Remark = f.Remark

                                                           })
                                                           .AsNoTracking()
                                                           .ToListAsync();
                    return memberClient;
                }
               

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<MemberTradeExcutionDTO> GetMemberClientById(int memberClientId)
        {
            try
            {
                var clientTrade = await _context.MemberClientTrade
                    .Where(x => x.MemberClientTradeId == memberClientId)
                    .Select(n => new MemberTradeExcutionDTO
                    {
                        MemberClientTradeId = n.MemberClientTradeId,
                        ExchangeActorId = n.ExchangeActorId,
                        CustomerFullName = n.ExchangeActor.OwnerManager.FirstNameAmh + " " + n.ExchangeActor.OwnerManager.FatherNameAmh + " " + n.ExchangeActor.OwnerManager.GrandFatherNameAmh,
                        ReportType = n.ReportType.DescriptionAmh,
                        OrganizationName = n.ExchangeActor.OrganizationNameAmh,
                        CustomerType = n.ExchangeActor.MemberCategory.DescriptionAmh,
                        ReportTypeId = n.ReportTypeId,
                        DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()),
                        ReportPeriod = n.ReportPeriod.DescriptionAmh,
                        ReportDate = n.ReportDate,
                        ReportPeriodId = n.ReportPeriodId,
                        Year = n.Year,
                        CreatedDateTime = n.CreatedDateTime,
                        IsTradeExcutionAccomplished = n.IsTradeExcutionAccomplished,
                        Remark = n.Remark,
                        TradeExcutionNotAccomplish = n.TradeExcutionNotAccomplish,
                        TradeStatus = n.TradeExcutionStatusType.DescriptionAmh,
                        TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId
                    })
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return clientTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<PagedResult<MemberTradeExcutionDTO>> GetAllTardeExcutionReport(ClientTradeQueryParameters queryParameters)
        {
            try
            {
                int totalResultList = 0;
                var memberClient = await _context.MemberClientTrade
                    .Where(x => x.TradeExcutionStatusTypeId == 1)
                    .OrderBy(x => x.TradeExcutionStatusTypeId)
                    .Select(n => new MemberTradeExcutionDTO
                    {
                        MemberClientTradeId = n.MemberClientTradeId,
                        ExchangeActorId = n.ExchangeActorId,
                        CustomerFullName = (queryParameters.Lang == "et") ? n.ExchangeActor.OwnerManager.FirstNameAmh + " " +
                                                        n.ExchangeActor.OwnerManager.FatherNameAmh + " " +
                                                        n.ExchangeActor.OwnerManager.GrandFatherNameAmh :
                                                        n.ExchangeActor.OwnerManager.FirstNameEng + " " +
                                                        n.ExchangeActor.OwnerManager.FatherNameEng + " " +
                                                        n.ExchangeActor.OwnerManager.GrandFatherNameEng,
                        ReportType = (queryParameters.Lang == "et") ? n.ReportType.DescriptionAmh :
                                                  n.ReportType.DescriptionEng,
                        OrganizationName = (queryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                            n.ExchangeActor.OrganizationNameEng,
                        CustomerType = (queryParameters.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                        n.ExchangeActor.MemberCategory.DescriptionEng,
                        ReportTypeId = n.ReportTypeId,
                        DateReport = (queryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                      n.ReportDate.Date.ToString(),
                        ReportPeriod = (queryParameters.Lang == "et") ? n.ReportPeriod.DescriptionAmh :
                                                                        n.ReportPeriod.DescriptionEng,
                        ReportDate = n.ReportDate,
                        ReportPeriodId = n.ReportPeriodId,
                        Year = n.Year,
                        CreatedDateTime = n.CreatedDateTime,
                        IsTradeExcutionAccomplished = n.IsTradeExcutionAccomplished,
                        Remark = n.Remark,
                        TradeExcutionNotAccomplish = n.TradeExcutionNotAccomplish,
                        TradeStatus = n.TradeExcutionStatusType.DescriptionAmh,
                        TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                        StartDate = n.StartDate,
                        DeadLine = n.DeadLine


                    })
                    .Paging(queryParameters.PageCount, queryParameters.PageNumber)
                     .AsNoTracking()
                     .ToListAsync();
                totalResultList = memberClient.Count();
                return new PagedResult<MemberTradeExcutionDTO>()
                {
                    Items = mapper.Map<List<MemberTradeExcutionDTO>>(memberClient),
                    ItemsCount = _context.MemberClientTrade
                    .Where(x => x.TradeExcutionStatusTypeId == 1 ||
                    x.TradeExcutionStatusTypeId == 2 ||
                    x.TradeExcutionStatusTypeId == 3).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetMemberTardeNotReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            //int totalResultList = 0;
            try
            {
                List<MemberTradeExecutionViewModel> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = GetMemberTardeNotReportListAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = GetMemberTardeNotReportListEng(clientTradeQueryParameters);
                }
                //totalResultList = tradeList.Count();
                //var pagedResult = tradeList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return tradeList;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<MemberTradeExecutionViewModel> GetMemberTardeNotReportListAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if (clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:                         
                           
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					       Join MemberTradeViolation mtv  on
						   tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						   where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						          tevr.Year = {4} and tevr.ReportTypeId = 1 and
								  tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {1} and
                      ReportTypeId= 1 and 
                      Year = {2})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                    // .OrderBy(x => x.CustomerFullName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                        (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionAmh as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                                         from ExchangeActor as ecx 
                                         join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                         JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                         where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                                         ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {1} and
                                          ReportTypeId= 1 and 
                                          Year = {2})",
                                         clientTradeQueryParameters.CustomerTypeId,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year)
                                       // .OrderBy(x => x.CustomerFullName)
                                        .AsNoTracking()
                                        .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                         (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionAmh as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                          JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                          where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId = 90) and ecx.Status = 20 and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {0} and
                                           ReportTypeId= 1 and 
                                           Year = {1})",
                                          //clientTradeQueryParameters.CustomerTypeId,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year)
                                         //.OrderBy(x => x.CustomerFullName)
                                         .AsNoTracking()
                                         .ToList();
                            break;
                    }

                }
                else
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {2} and
                      ReportTypeId= 1 and 
                      Year = {3})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                        (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionAmh as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                           JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                         where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and  ecx.BuisnessFiledId = {1} and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {2} and
                                          ReportTypeId= 1 and 
                                          Year = {3})",
                                         clientTradeQueryParameters.CustomerTypeId,
                                         clientTradeQueryParameters.FieldBusinessId,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year)
                                        .OrderBy(x => x.OrganizationName)
                                        .AsNoTracking()
                                        .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                         (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionAmh as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                          JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                          where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId = 90) and 
                                          ecx.Status = 20 and ecx.BuisnessFiledId = {0} and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {1} and
                                          ReportTypeId= 1 and 
                                          Year = {2})",
                                          clientTradeQueryParameters.FieldBusinessId,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year
                                          )
                                         .OrderBy(x => x.OrganizationName)
                                         .AsNoTracking()
                                         .ToList();
                            break;
                    }

                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetMemberTardeNotReportListEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if(clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {1} and
                      ReportTypeId= 1 and 
                      Year = {2})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                        (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionEng as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                          JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                         where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {1} and
                                          ReportTypeId= 1 and 
                                          Year = {2})",
                                         clientTradeQueryParameters.CustomerTypeId,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year)
                                        .OrderBy(x => x.CustomerFullName)
                                        .AsNoTracking()
                                        .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                         (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionEng as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                          JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                          where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId = 90) and ecx.Status = 20 and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {0} and
                                          ReportTypeId= 1 and 
                                          Year = {1})",
                                          //clientTradeQueryParameters.CustomerTypeId,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year)
                                         .OrderBy(x => x.CustomerFullName)
                                         .AsNoTracking()
                                         .ToList();
                            break;
                    }

                    }
                else
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                       (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {2} and
                      ReportTypeId= 1 and 
                      Year = {3})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                        (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionEng as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                           JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                         where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and  ecx.BuisnessFiledId = {1} and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {2} and
                                          ReportTypeId= 1 and 
                                          Year = {3})",
                                         clientTradeQueryParameters.CustomerTypeId,
                                         clientTradeQueryParameters.FieldBusinessId,
                                         clientTradeQueryParameters.ReportPeriodId,
                                         clientTradeQueryParameters.Year)
                                        .OrderBy(x => x.CustomerFullName)
                                        .AsNoTracking()
                                        .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                         (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                                         ecx.Ecxcode as CustomerFullName,
                                         l.DescriptionEng as CustomerType,
                                         ecx.MobileNo as MobliePhone,
                                         ecx.Tel as RegularPhone,
                                         (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 1 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                                          from ExchangeActor as ecx 
                                          join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                                          JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                                          where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId = 90) and 
                                          ecx.Status = 20 and ecx.BuisnessFiledId = {0} and
                                           ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					                       ReportPeriodId = {1} and
                                          ReportTypeId= 1 and 
                                          Year = {2})",
                                          clientTradeQueryParameters.FieldBusinessId,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year,
                                          clientTradeQueryParameters.ReportPeriodId,
                                          clientTradeQueryParameters.Year)
                                         .OrderBy(x => x.OrganizationName)
                                         .AsNoTracking()
                                         .ToList();
                            break;
                    }

                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<MemberTradeExecutionViewModel> GetMemberFinancialNotReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            //int totalResultList = 0;
            try
            {
                List<MemberTradeExecutionViewModel> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = GetMemberFinancialNotReportListAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = GetMemberFinancialNotReportListEng(clientTradeQueryParameters);
                }
                //totalResultList = tradeList.Count();
                //var pagedResult = tradeList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return tradeList;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<MemberTradeExecutionViewModel> GetMemberFinancialNotReportListAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if(clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} 
                      and ReportTypeId= 2 and
                      Year = {2})", 
                   clientTradeQueryParameters.CustomerTypeId,
                   clientTradeQueryParameters.ReportPeriodId,
                   //clientTradeQueryParameters.ReportTypeId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} 
                      and ReportTypeId= 2 and
                      Year = {2})", 
                   clientTradeQueryParameters.CustomerTypeId,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {0} 
                      and ReportTypeId= 2 and
                      Year = {1})",
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                    }
                
                }
                else
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and  ecx.BuisnessFiledId ={1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {2} 
                      and ReportTypeId= 2 and
                      Year = {3})",
                   clientTradeQueryParameters.CustomerTypeId,
                   clientTradeQueryParameters.FieldBusinessId,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId ={1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {2} 
                      and ReportTypeId= 2 and
                      Year = {3})",
                   clientTradeQueryParameters.CustomerTypeId,
                   clientTradeQueryParameters.FieldBusinessId,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                   @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90) and ecx.Status = 20 
                      and ecx.BuisnessFiledId ={0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} 
                      and ReportTypeId= 2 and
                      Year = {2})",
                   clientTradeQueryParameters.FieldBusinessId,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year,
                   clientTradeQueryParameters.ReportPeriodId,
                   clientTradeQueryParameters.Year

                   )
               .OrderBy(x => x.OrganizationName)
               .AsNoTracking()
               .ToList();
                            break;
                    }

                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetMemberFinancialNotReportListEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if(clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                                 (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} and
                      ReportTypeId= 2 and 
                      Year = {2})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                      .OrderBy(x => x.OrganizationName)
                      .AsNoTracking()
                      .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                     (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} and
                      ReportTypeId= 2 and 
                      Year = {2})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {0} and
                      ReportTypeId= 2 and 
                      Year = {1})",
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                    }
                
                }
                else
                {
                    switch (clientTradeQueryParameters.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                                                 (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId ={1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {2} and
                      ReportTypeId= 2 and 
                      Year = {3})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year
                      )
                      .OrderBy(x => x.OrganizationName)
                      .AsNoTracking()
                      .ToList();
                            break;
                        case 6:
                        case 90:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId ={1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {2} and
                      ReportTypeId= 2 and 
                      Year = {3})",
                      clientTradeQueryParameters.CustomerTypeId,
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                        default:
                            data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                     (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 2 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90) and ecx.Status = 20 
                      and ecx.BuisnessFiledId ={0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
                      TradeExcutionStatusTypeId = 2 and
					  ReportPeriodId = {1} and
                      ReportTypeId= 2 and 
                      Year = {2})",
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                     .OrderBy(x => x.OrganizationName)
                     .AsNoTracking()
                     .ToList();
                            break;
                    }

                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<List<MemberTradeLateReportView>> GetLateMemberTardeList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            //int totalResultList = 0;
            try
            {
                List<MemberTradeLateReportView> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = await GetLateMemberTardeListAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = await GetLateMemberTardeListEng(clientTradeQueryParameters);
                }
                return tradeList;
                //totalResultList = tradeList.Count();
                //var pagedResult = tradeList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                //return new PagedResult<MemberClientTradeDTO>()
                //{
                //    Items = mapper.Map<List<MemberClientTradeDTO>>(pagedResult),
                //    ItemsCount = totalResultList
                //};
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeLateReportView>> GetLateMemberTardeListAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 1 &&
                                      x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                    ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberTradeLateReportView
                                                         {
                                                             OrganizationName = n.ExchangeActor.OrganizationNameAmh,
                                                             CustomerType = n.ExchangeActor.MemberCategory.DescriptionAmh,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId,
                                                             TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x=> x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                         })
                                                         .AsNoTracking()
                                                          .ToListAsync();
                    return clientTrade;

                }else if(clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 1 &&
                                      x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                    ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberTradeLateReportView
                                                         {
                                                             OrganizationName = n.ExchangeActor.OrganizationNameAmh,
                                                             CustomerType = n.ExchangeActor.CustomerType.DescriptionAmh,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId,
                                                             TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                         })
                                                         .AsNoTracking()
                                                          .ToListAsync();
                    return clientTrade;

                }
                else
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 1 &&
                                      x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberTradeLateReportView
                                                         {
                                                             OrganizationName = n.ExchangeActor.OrganizationNameAmh,
                                                             CustomerType = n.ExchangeActor.CustomerType.DescriptionAmh,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId,
                                                             TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                         })
                                                         .AsNoTracking()
                                                          .ToListAsync();
                    return clientTrade;

                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberTradeLateReportView>> GetLateMemberTardeListEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                        x.ReportTypeId == 1 &&
                                                       x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                                      ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                      ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                      ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                      ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                                           .Select(n => new MemberTradeLateReportView
                                                                           {
                                                                               OrganizationName = n.ExchangeActor.OrganizationNameEng,
                                                                               CustomerType = n.ExchangeActor.MemberCategory.DescriptionEng,
                                                                               MobilePhone = n.ExchangeActor.MobileNo,
                                                                               RegularPhone = n.ExchangeActor.Tel,
                                                                               DateReport = n.ReportDate.Date.ToString(),
                                                                               ExchangeActorId = n.ExchangeActorId,
                                                                               ReportPeriodId = n.ReportPeriodId,
                                                                               ReportTypeId = n.ReportTypeId,
                                                                               TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()

                                                                           })
                                                                           .AsNoTracking()
                                                                            .ToListAsync();
                    return clientTrade;
                }
                else if (clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 5 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                        x.ReportTypeId == 1 &&
                                                       x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                                      ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                      ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                      ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                                           .Select(n => new MemberTradeLateReportView
                                                                           {
                                                                               OrganizationName = n.ExchangeActor.OrganizationNameEng,
                                                                               CustomerType = n.ExchangeActor.CustomerType.DescriptionEng,
                                                                               MobilePhone = n.ExchangeActor.MobileNo,
                                                                               RegularPhone = n.ExchangeActor.Tel,
                                                                               DateReport = n.ReportDate.Date.ToString(),
                                                                               ExchangeActorId = n.ExchangeActorId,
                                                                               ReportPeriodId = n.ReportPeriodId,
                                                                               ReportTypeId = n.ReportTypeId,
                                                                               TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()

                                                                           })
                                                                           .AsNoTracking()
                                                                            .ToListAsync();
                    return clientTrade;
                }
                else
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                        x.ReportTypeId == 1 &&
                                                       x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                                      ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                      //((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                      ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))
                                                                           // ((clientTradeQueryParameters.ReportTypeId == 0) || (x.ReportTypeId == clientTradeQueryParameters.ReportTypeId)))

                                                                           .Select(n => new MemberTradeLateReportView
                                                                           {
                                                                               OrganizationName = n.ExchangeActor.OrganizationNameEng,
                                                                               CustomerType = n.ExchangeActor.CustomerType.DescriptionEng,
                                                                               MobilePhone = n.ExchangeActor.MobileNo,
                                                                               RegularPhone = n.ExchangeActor.Tel,
                                                                               DateReport = n.ReportDate.Date.ToString(),
                                                                               ExchangeActorId = n.ExchangeActorId,
                                                                               ReportPeriodId = n.ReportPeriodId,
                                                                               ReportTypeId = n.ReportTypeId,
                                                                               TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 1 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()

                                                                           })
                                                                           .AsNoTracking()
                                                                            .ToListAsync();
                    return clientTrade;
                }
              


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeLateReportView>> GetLateMemberFinancialList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 2 &&
                                     x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                   ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                   ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                   ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                        .Select(n => new MemberTradeLateReportView
                                                        {
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                           n.ExchangeActor.OrganizationNameEng,
                                                            CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                       n.ExchangeActor.MemberCategory.DescriptionEng,
                                                            MobilePhone = n.ExchangeActor.MobileNo,
                                                            RegularPhone = n.ExchangeActor.Tel,
                                                            DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                     n.ReportDate.Date.ToString(),
                                                            ExchangeActorId = n.ExchangeActorId,
                                                            ReportPeriodId = n.ReportPeriodId,
                                                            ReportTypeId = n.ReportTypeId,
                                                            TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 2 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                        })
                                                        .AsNoTracking()
                                                         .ToListAsync();
                    return clientTrade;

                }
                else if(clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 2 &&
                                     x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                   ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                   ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                   ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                        .Select(n => new MemberTradeLateReportView
                                                        {
                                                            OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                           n.ExchangeActor.OrganizationNameEng,
                                                            CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                       n.ExchangeActor.CustomerType.DescriptionEng,
                                                            MobilePhone = n.ExchangeActor.MobileNo,
                                                            RegularPhone = n.ExchangeActor.Tel,
                                                            DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                     n.ReportDate.Date.ToString(),
                                                            ExchangeActorId = n.ExchangeActorId,
                                                            ReportPeriodId = n.ReportPeriodId,
                                                            ReportTypeId = n.ReportTypeId,
                                                            TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 2 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                        })
                                                        .AsNoTracking()
                                                         .ToListAsync();
                    return clientTrade;

                }
                else
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 2 &&
                                     x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                   //((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                   //((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                   ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                        .Select(n => new MemberTradeLateReportView
                                                        {
                                                            OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                           n.ExchangeActor.OrganizationNameEng,
                                                            CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                       n.ExchangeActor.CustomerType.DescriptionEng,
                                                            MobilePhone = n.ExchangeActor.MobileNo,
                                                            RegularPhone = n.ExchangeActor.Tel,
                                                            DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                     n.ReportDate.Date.ToString(),
                                                            ExchangeActorId = n.ExchangeActorId,
                                                            ReportPeriodId = n.ReportPeriodId,
                                                            ReportTypeId = n.ReportTypeId,
                                                            TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 2 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()
                                                        })
                                                        .AsNoTracking()
                                                         .ToListAsync();
                    return clientTrade;

                }

               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberClientTradeDTO>> GetMemberFinancialOnTimeReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                           x.ReportTypeId == 2 &&
                                                          (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                        ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                        ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                        ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                                             .Select(n => new MemberClientTradeDTO
                                                                             {
                                                                                 OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                                                n.ExchangeActor.OrganizationNameEng,
                                                                                 CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                                            n.ExchangeActor.MemberCategory.DescriptionEng,
                                                                                 MobilePhone = n.ExchangeActor.MobileNo,
                                                                                 RegularPhone = n.ExchangeActor.Tel,
                                                                                 DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                                          n.ReportDate.Date.ToString(),
                                                                                 ExchangeActorId = n.ExchangeActorId,
                                                                                 ReportPeriodId = n.ReportPeriodId,
                                                                                 ReportTypeId = n.ReportTypeId
                                                                             })

                                                                 // .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                                                 .AsNoTracking()
                                                                 .ToListAsync();
                    return clientTrade;
                }
                else if (clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                       x.ReportTypeId == 2 &&
                                      (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberClientTradeDTO
                                                         {
                                                             //CustomerFullName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OwnerManager.FirstNameAmh + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.FatherNameAmh + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.GrandFatherNameAmh :
                                                             //                                                           n.ExchangeActor.OwnerManager.FirstNameEng + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.FatherNameEng + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.GrandFatherNameEng,
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                        n.ExchangeActor.CustomerType.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId
                                                         })

                                             // .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                             .AsNoTracking()
                                             .ToListAsync();
                    return clientTrade;
                }
                else
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                       x.ReportTypeId == 2 &&
                                      (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberClientTradeDTO
                                                         {
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                        n.ExchangeActor.CustomerType.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId
                                                         })
                                             
                                             .AsNoTracking()
                                             .ToListAsync();
                    return clientTrade;
                }
              
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberClientTradeDTO>> GetMemberTradeOnTimeReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                                               x.ReportTypeId == 1 &&
                                                                              (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                                                            ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                                            ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                                            ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                                            ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                           .Select(n => new MemberClientTradeDTO
                                                           {
                                                                                                                       
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                        n.ExchangeActor.MemberCategory.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,

                                                              })
                                                               
                                                            .AsNoTracking()
                                                            .ToListAsync();
                    return clientTrade;
                }
                else if(clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                           x.ReportTypeId == 1 &&
                                                          (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                        ((clientTradeQueryParameters.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                        ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                        ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                                             .Select(n => new MemberClientTradeDTO
                                                                             {
                                                                                                                     
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                        n.ExchangeActor.CustomerType.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,

                                                                             })
                                                                 // .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                                                 .AsNoTracking()
                                                                 .ToListAsync();
                    return clientTrade;
                }
                else
                {
                    var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                           x.ReportTypeId == 1 &&
                                                          (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine) <= 0) &&
                                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                        ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                            .Select(n => new MemberClientTradeDTO
                                                            {
                                                                                                                      
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                        n.ExchangeActor.CustomerType.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,

                                                                             })
                                                            
                                                         .AsNoTracking()
                                                         .ToListAsync();
                    return clientTrade;
                }

               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<ComparisonOffSiteMonitoring> GetListNotAccomplishedTradeComparative(ComparativeQueryParameterSearch comparativeQueryParameter)
        {
            try
            {
                List<ComparisonOffSiteMonitoring> comparisonOffSites = new List<ComparisonOffSiteMonitoring>();
                var totalTradeNotAccomplished = _context.MemberClientTrade.Where(x => x.IsTradeExcutionAccomplished == 2 &&
                                                                              (comparativeQueryParameter.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == comparativeQueryParameter.CustomerTypeId) &&
                                                                              x.TradeExcutionNotAccomplish == comparativeQueryParameter.ToWhomDoesTradeId &&
                                                                              (String.IsNullOrEmpty(comparativeQueryParameter.EcxCode) || x.ExchangeActor.Ecxcode == comparativeQueryParameter.EcxCode))
                                                                              .AsNoTracking()
                                                                              .ToList();
                if (totalTradeNotAccomplished.Count() == 0)
                {
                    return null;
                }
                ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                var compOffSiteMonitor = totalTradeNotAccomplished.Select(x => new { x.Year }).Distinct();
                int totalFirstQuarter = totalTradeNotAccomplished.Where(x => x.ReportPeriodId == 1).Count();
                if (totalFirstQuarter == 0)
                {
                    totalFirstQuarter = 1;
                }
                int totalSecondQuarter = totalTradeNotAccomplished.Where(x => x.ReportPeriodId == 2).Count();
                if (totalSecondQuarter == 0)
                {
                    totalSecondQuarter = 1;
                }
                int totalThiredQuarter = totalTradeNotAccomplished.Where(x => x.ReportPeriodId == 3).Count();
                if (totalThiredQuarter == 0)
                {
                    totalThiredQuarter = 1;
                }
                int totalFourthQuarter = totalTradeNotAccomplished.Where(x => x.ReportPeriodId == 4).Count();
                if (totalFourthQuarter == 0)
                {
                    totalFourthQuarter = 1;
                }
                foreach (var name in compOffSiteMonitor)
                {
                    offSiteMonitoring.Year = name.Year;
                    offSiteMonitoring.FirstQuarter = totalTradeNotAccomplished.Where(x => x.Year == name.Year &&
                                                                           x.ReportPeriodId == 1).Count();
                    offSiteMonitoring.SecondQuarter = totalTradeNotAccomplished.Where(x => x.Year == name.Year &&
                                                                              x.ReportPeriodId == 2).Count();
                    offSiteMonitoring.ThiredQuarter = totalTradeNotAccomplished.Where(x => x.Year == name.Year &&
                                                                            x.ReportPeriodId == 3).Count();
                    offSiteMonitoring.FourthQuarter = totalTradeNotAccomplished.Where(x => x.Year == name.Year &&
                                                                              x.ReportPeriodId == 4).Count();
                    offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (totalFirstQuarter);
                    offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (totalSecondQuarter);
                    offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (totalThiredQuarter);
                    offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (totalFourthQuarter);
                    comparisonOffSites.Add(offSiteMonitoring);
                }

                return comparisonOffSites;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberClientTradeDTO>> GetMemberClientInformationOnTimeReport(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {

                var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                       x.ReportTypeId == 3 &&
                                      (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.DeadLine)<=0) &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberClientTradeDTO
                                                         {
                                                             //CustomerFullName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OwnerManager.FirstNameAmh + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.FatherNameAmh + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.GrandFatherNameAmh :
                                                             //                                                           n.ExchangeActor.OwnerManager.FirstNameEng + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.FatherNameEng + " " +
                                                             //                                                           n.ExchangeActor.OwnerManager.GrandFatherNameEng,
                                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                            n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                        n.ExchangeActor.MemberCategory.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,

                                                         })
                                             // .Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber)
                                             .AsNoTracking()
                                             .ToListAsync();
                return clientTrade;
                //{
                //    Items = mapper.Map<List<MemberClientTradeDTO>>(clientTrade),
                //    ItemsCount = _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 3 &&
                //                          x.ReportTypeId == 2 &&
                //                         (x.ReportDate >= x.StartDate && x.ReportDate <= x.DeadLine) &&
                //                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                //                       ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year))).Count()
                //};


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberTradeLateReportView>> GetLateReportedMemberClientList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var clientTrade = await _context.MemberClientTrade.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                      x.ReportTypeId == 3 &&
                                     x.ReportDate.Date.CompareTo(x.DeadLine) > 0 &&
                                    ((clientTradeQueryParameters.ReportPeriodId == 0) || (x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                    ((clientTradeQueryParameters.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                    ((clientTradeQueryParameters.Year == 0) || (x.Year == clientTradeQueryParameters.Year)))

                                                         .Select(n => new MemberTradeLateReportView
                                                         {
                                                            
                                                             OrganizationName =(clientTradeQueryParameters.Lang == "et")? n.ExchangeActor.OrganizationNameAmh:
                                                                                                                          n.ExchangeActor.OrganizationNameEng,
                                                             CustomerType = (clientTradeQueryParameters.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh:
                                                                                                                        n.ExchangeActor.CustomerType.DescriptionEng,
                                                             MobilePhone = n.ExchangeActor.MobileNo,
                                                             RegularPhone = n.ExchangeActor.Tel,
                                                             DateReport = (clientTradeQueryParameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                                                                                      n.ReportDate.Date.ToString(),
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             ReportPeriodId = n.ReportPeriodId,
                                                             ReportTypeId = n.ReportTypeId,
                                                             TotalNoMembersVioaltion = _context.TradeExcutionViolationRecord.Where(x => x.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      x.ReportTypeId == 3 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()

                                                         })
                                                         .AsNoTracking()
                                                          .ToListAsync();
                return clientTrade;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetMemberClientNotReportListAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if(clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                                        @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 3 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join OwnerManager as own on ecx.ExchangeActorId = own.ExchangeActorId
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = 72) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {0} 
                      and ReportTypeId= 3 and
                      Year = {1})",
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                      .OrderBy(x => x.OrganizationName)
                      .AsNoTracking()
                      .ToList();

                }
                else
                {
                    data = _context.MemberTradeExecutionViewModel.FromSqlRaw(
                    @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 3 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join OwnerManager as own on ecx.ExchangeActorId = own.ExchangeActorId
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = 72) and ecx.Status = 20 and ecx.BuisnessFiledId = { 0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {1} 
                      and ReportTypeId= 3 and
                      Year = {2})",
                    clientTradeQueryParameters.FieldBusinessId,
                    clientTradeQueryParameters.ReportPeriodId,
                    clientTradeQueryParameters.Year,
                    clientTradeQueryParameters.ReportPeriodId,
                    clientTradeQueryParameters.Year)
                .OrderBy(x => x.OrganizationName)
                .AsNoTracking()
                .ToList();    
                }                
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetMemberClientNotReportListEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            List<MemberTradeExecutionViewModel> data;
            try
            {
                if(clientTradeQueryParameters.FieldBusinessId == 0)
                {
                    data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                      (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 3 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join OwnerManager as own on ecx.ExchangeActorId = own.ExchangeActorId
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = 72) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {0} and
                      ReportTypeId= 3 and 
                      Year = {1})",
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                      .OrderBy(x => x.OrganizationName)
                      .AsNoTracking()
                      .ToList();
                }
                else
                {
                    data = _context.MemberTradeExecutionViewModel.FromSqlRaw
                      (@"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobliePhone,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 3 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join OwnerManager as own on ecx.ExchangeActorId = own.ExchangeActorId
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberClientTrade mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = 72) and ecx.Status = 20 and ecx.BuisnessFiledId = {0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberClientTrade where
					  ReportPeriodId = {1} and
                      ReportTypeId= 3 and 
                      Year = {2})",
                      clientTradeQueryParameters.FieldBusinessId,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year,
                      clientTradeQueryParameters.ReportPeriodId,
                      clientTradeQueryParameters.Year)
                      .OrderBy(x => x.OrganizationName)
                      .AsNoTracking()
                      .ToList();
                }
               
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberTradeExecutionViewModel> GetNotReportMemberClientList(ClientTradeQueryParameters clientTradeQueryParameters)
        {            
            try
            {
                List<MemberTradeExecutionViewModel> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList =  GetMemberClientNotReportListAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList =  GetMemberClientNotReportListEng(clientTradeQueryParameters);
                }
                return tradeList;
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberFinancialAuditorViewModel> GetMemberClientActivSuspendedCanceled(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<MemberFinancialAuditorViewModel> exchangeActorSt = null;
                var annualBudgetCloser = _context.AnnualBudgetCloser.Where(x => x.AnnualBudgetCloserId == exchangeActorStatus.AnnualBudgetCloserId).FirstOrDefault();
               
                       
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                                x.MemberCategoryId == 72)
                                                                 .Select(n => new MemberFinancialAuditorViewModel
                                                                 {
                                                                     OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                     CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                                                     MobileNo = n.MobileNo,
                                                                     RegularPhone = n.Tel
                                                                 })
                                                                 .AsNoTracking()
                                                                 .ToList();
                       
                return exchangeActorSt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TotalQuantyOfExchangeactor> GetMemberClientTotalQuantyOfExchangeactor(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<TotalQuantyOfExchangeactor> listOfExchangeactorStatus = new List<TotalQuantyOfExchangeactor>();
                TotalQuantyOfExchangeactor activeExchangeActor = new TotalQuantyOfExchangeactor();
                
                    var exchangeactorActive = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                              x.MemberCategoryId == 72).Count();
                    activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                    activeExchangeActor.Quantity = exchangeactorActive;
                    listOfExchangeactorStatus.Add(activeExchangeActor);
                
                
                TotalQuantyOfExchangeactor injectedExchangeActor = new TotalQuantyOfExchangeactor();
               
                    var exchangeactorInjuction = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                              x.MemberCategoryId == 72).Count();
                    injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                    injectedExchangeActor.Quantity = exchangeactorInjuction;
                    listOfExchangeactorStatus.Add(injectedExchangeActor);
                
               
                TotalQuantyOfExchangeactor cancelegExchangeActor = new TotalQuantyOfExchangeactor();
                
                    var exchangeactorCanceled = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                             x.MemberCategoryId == 72).Count();
                    cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                    cancelegExchangeActor.Quantity = exchangeactorCanceled;
                    listOfExchangeactorStatus.Add(cancelegExchangeActor);
                
               
                return listOfExchangeactorStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberFinancialAuditorViewModel> GetActivSuspendedCanceled(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<MemberFinancialAuditorViewModel> exchangeActorSt = null;
                switch (exchangeActorStatus.ExchageActorStatus)
                {
                    case 1:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                                          x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                                                 .Select(n => new MemberFinancialAuditorViewModel
                                                                 {
                                                                     OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                     CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                                                     MobileNo = n.MobileNo,
                                                                     RegularPhone = n.Tel
                                                                 })
                                                                 .AsNoTracking()
                                                                 .ToList();
                        }
                        else if (exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                              x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                     .Select(n => new MemberFinancialAuditorViewModel
                                     {
                                         OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                         CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                         MobileNo = n.MobileNo,
                                         RegularPhone = n.Tel
                                     })
                                     .AsNoTracking()
                                     .ToList();
                        }
                        else
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                           (x.CustomerTypeId == 6 || x.CustomerTypeId == 90))
                                     .Select(n => new MemberFinancialAuditorViewModel
                                     {
                                         OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                         CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                         MobileNo = n.MobileNo,
                                         RegularPhone = n.Tel
                                     })
                                     .AsNoTracking()
                                     .ToList();
                        }

                        break;

                    case 2:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                               x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                                               .Select(n => new MemberFinancialAuditorViewModel
                                                               {
                                                                   OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                   CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                                                   MobileNo = n.MobileNo,
                                                                   RegularPhone = n.Tel
                                                               })
                                                               .AsNoTracking()
                                                               .ToList();
                        }
                        else if (exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                             x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                                               .Select(n => new MemberFinancialAuditorViewModel
                                                               {
                                                                   OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                   CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                   MobileNo = n.MobileNo,
                                                                   RegularPhone = n.Tel
                                                               })
                                                               .AsNoTracking()
                                                               .ToList();
                        }
                        else
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                             (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ))
                                                                                           .Select(n => new MemberFinancialAuditorViewModel
                                                                                           {
                                                                                               OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                                               CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                                               MobileNo = n.MobileNo,
                                                                                               RegularPhone = n.Tel
                                                                                           })
                                                                                           .AsNoTracking()
                                                                                           .ToList();
                        }

                        break;
                    case 3:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                             x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                    .Select(n => new MemberFinancialAuditorViewModel
                                    {
                                        OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                        CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                        MobileNo = n.MobileNo,
                                        RegularPhone = n.Tel
                                    })
                                    .AsNoTracking()
                                    .ToList();
                        }
                        else if (exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                                         x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                                                .Select(n => new MemberFinancialAuditorViewModel
                                                                {
                                                                    OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                    CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                    MobileNo = n.MobileNo,
                                                                    RegularPhone = n.Tel
                                                                })
                                                                .AsNoTracking()
                                                                .ToList();
                        }
                        else
                        {
                            exchangeActorSt = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                          (x.CustomerTypeId == 6 || x.CustomerTypeId == 90))
                                                                                            .Select(n => new MemberFinancialAuditorViewModel
                                                                                            {
                                                                                                OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                                                CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                                                MobileNo = n.MobileNo,
                                                                                                RegularPhone = n.Tel
                                                                                            })
                                                                                            .AsNoTracking()
                                                                                            .ToList();
                        }
                        break;
                    default:
                        exchangeActorSt = null;
                        break;
                }
                return exchangeActorSt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TotalQuantyOfExchangeactor> GetTotalQuantyOfExchangeactor(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<TotalQuantyOfExchangeactor> listOfExchangeactorStatus = new List<TotalQuantyOfExchangeactor>();
                if(exchangeActorStatus.CustomerTypeId != 0)
                {
                    TotalQuantyOfExchangeactor activeExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorActive = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                  x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);
                    }
                    else
                    {
                        var exchangeactorActive = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                               x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);
                    }
                    TotalQuantyOfExchangeactor injectedExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorInjuction = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                  x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);
                    }
                    else
                    {
                        var exchangeactorInjuction = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                          x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);
                    }
                    TotalQuantyOfExchangeactor cancelegExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorCanceled = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                                 x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorCanceled;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);
                    }
                    else
                    {
                        var exchangeactorActive = _context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                             x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);
                    }

                }
                else
                {
                    TotalQuantyOfExchangeactor activeExchangeActor = new TotalQuantyOfExchangeactor();
                    
                        var exchangeactorActive = _context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                                     (x.CustomerTypeId == 6 || x.CustomerTypeId == 90)).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);

                    TotalQuantyOfExchangeactor injectedExchangeActor = new TotalQuantyOfExchangeactor();
                   
                        var exchangeactorInjuction = _context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                        (x.CustomerTypeId == 6 || x.CustomerTypeId == 90)).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);

                    TotalQuantyOfExchangeactor cancelegExchangeActor = new TotalQuantyOfExchangeactor();
                        var exchangeactorCanceled = _context.ExchangeActor.Where(x => x.Status == 64 && (x.CustomerTypeId == 6 || x.CustomerTypeId == 90)).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorCanceled;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);

                    }
                
                return listOfExchangeactorStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}
