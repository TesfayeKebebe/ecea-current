﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class MemberFinancialAuditorRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;
        private readonly IHostingEnvironment host;
        private readonly IConfiguration Configuration;
        private readonly string[] ACCEPTED_FILE_TYPES = new[] { ".jpg", ".jpeg", ".png", ".pdf" };
        public MemberFinancialAuditorRepository(ECEADbContext _context, IMapper _mapper,
                                                         IHostingEnvironment host,
                                                      IConfiguration configuration)
        {
            context = _context;
            mapper = _mapper;
            this.host = host;
            Configuration = configuration;
        }
        public async Task<Guid> CreateUpload(MemberFinancialAuditorDTO memberFinancial)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {

                    double NrOfDays = 0;
                    DateTime reportDate = DateTime.Now;
                    DateTime auditYearThree = DateTime.Now.AddYears(-3);
                    DateTime auditYearTwo = DateTime.Now.AddYears(-2);
                    DateTime auditYearOne = DateTime.Now.AddYears(-1);
                    DateTime deadLine = DateTime.MinValue;
                    var deadLineObj = context.AnnualBudgetCloser.Where(
                    x => x.AnnualBudgetCloserId == memberFinancial.AnnualBudgetCloserId)
                    .AsNoTracking()
                    .FirstOrDefault();
                    if (deadLineObj != null)
                    {
                        deadLine = deadLineObj.StartDate;
                    }
                    TimeSpan t = deadLine - reportDate;
                    NrOfDays = t.TotalDays;
                    if (NrOfDays <= 0)
                    {
                        int threeYearBack = 0;
                        int twoYearBack = 0;
                        int oneYearBack = 0;
                        int diffBackTwoThree = 0;
                        int diffBackOneTwo = 0;
                        int diffBackZeroOne = 0;
                        int diffBackZeroThree = 0;
                        int currentYear = reportDate.Year;
                        var auditThreeYearBack = await context.MemberFinancialAuditor.Where(x => x.Ecxcode == memberFinancial.Ecxcode &&
                                                                                               x.ReportDate.Year == auditYearThree.Year
                                                                                            ).AsNoTracking().FirstOrDefaultAsync();
                        if (auditThreeYearBack != null)
                        {
                            threeYearBack = auditThreeYearBack.ReportDate.Year;
                        }
                        var auditTwoYearBack = await context.MemberFinancialAuditor.Where(x => x.Ecxcode == memberFinancial.Ecxcode &&
                                                                                               x.ReportDate.Year == auditYearTwo.Year
                                                                                            ).AsNoTracking().FirstOrDefaultAsync();
                        if (auditTwoYearBack != null)
                        {
                            twoYearBack = auditTwoYearBack.ReportDate.Year;
                        }
                        var auditOneYearBack = await context.MemberFinancialAuditor.Where(x => x.Ecxcode == memberFinancial.Ecxcode &&
                                                                                               x.ReportDate.Year == auditYearOne.Year
                                                                                            ).AsNoTracking().FirstOrDefaultAsync();
                        if (auditOneYearBack != null)
                        {
                            oneYearBack = auditOneYearBack.ReportDate.Year;
                        }
                        diffBackTwoThree = twoYearBack - threeYearBack;
                        diffBackOneTwo = oneYearBack - twoYearBack;
                        diffBackZeroOne = currentYear - oneYearBack;
                        diffBackZeroThree = currentYear - threeYearBack;
                        if (diffBackTwoThree == 1 && diffBackOneTwo == 1 &&
                            diffBackZeroOne == 1 && diffBackZeroThree >= 3)
                        {
                            return Guid.Empty;
                        }
                        else
                        {
                            var annualFinancialAudit = await context.MemberFinancialAuditor.Where(x => x.Year == memberFinancial.Year &&
                                                                                                     x.ExchangeActorId == memberFinancial.ExchangeActorId &&
                                                                                                     (x.TradeExcutionStatusTypeId == 2) &&
                                                                                                     x.Ecxcode == memberFinancial.Ecxcode).AsNoTracking().FirstOrDefaultAsync();
                            if (annualFinancialAudit == null)
                            {
                                var memberFinancialAudit = await context.MemberFinancialAuditor.Where(x => x.Year == memberFinancial.Year &&
                                                                                                                                     x.ExchangeActorId == memberFinancial.ExchangeActorId &&
                                                                                                                                     (x.TradeExcutionStatusTypeId == 1) &&
                                                                                                                                     x.Ecxcode == memberFinancial.Ecxcode).AsNoTracking().FirstOrDefaultAsync();
                                if (memberFinancialAudit == null)
                                {
                                    var annualBudgetCloser = await context.AnnualBudgetCloser.Where(x => x.AnnualBudgetCloserId == memberFinancial.AnnualBudgetCloserId)
                                                                                             .AsNoTracking().FirstOrDefaultAsync();

                                    MemberFinancialAuditor financialAuditor = mapper.Map<MemberFinancialAuditor>(memberFinancial);
                                    if (annualBudgetCloser != null)
                                    {
                                        financialAuditor.StartDate = annualBudgetCloser.StartDate;
                                        financialAuditor.EndDate = annualBudgetCloser.EndDate;

                                    }
                                    financialAuditor.MemberFinancialAuditorId = Guid.NewGuid().ToString();
                                    context.Entry(financialAuditor).State = EntityState.Added;
                                    MultipleUpload(memberFinancial.ActualFile, financialAuditor.MemberFinancialAuditorId);
                                    await context.SaveChangesAsync();
                                    transaction.Commit();
                                    return financialAuditor.ExchangeActorId;
                                }
                                else
                                {
                                    //financialAuditor.MemberFinancialAuditorId = Guid.NewGuid().ToString();
                                    // context.Entry(memberFinancialAudit).State = EntityState.Added;
                                    MultipleUpload(memberFinancial.ActualFile, memberFinancialAudit.MemberFinancialAuditorId);
                                    await context.SaveChangesAsync();
                                    transaction.Commit();
                                    return memberFinancialAudit.ExchangeActorId;
                                }
                            }
                            else
                            {
                                return Guid.Empty;
                            }
                        }
                    }
                    else
                    {
                        return Guid.Empty;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        private int MultipleUpload(List<IFormFile> postedFiles, string Id)
        {

            foreach (IFormFile postedFile in postedFiles)
            {
                if (!ACCEPTED_FILE_TYPES.Any(s => s == Path.GetExtension(postedFile.FileName).ToLower()))
                {
                    return 0;
                }
                var uploadFilesPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Uploads");
                if (!Directory.Exists(uploadFilesPath))
                    Directory.CreateDirectory(uploadFilesPath);
                string fileName = Path.GetFileName(postedFile.FileName);
                string fileType = Path.GetFileName(postedFile.ContentType);
                var filePath = Path.Combine(uploadFilesPath, fileName);
                var fileUrl = Configuration["ApplicationUrl"] + "/uploads/" + fileName;
                var attachment = new FinancialAuditoredFileUpload
                {
                    FileLocation = filePath,
                    Url = fileUrl,
                    FileName = fileName,
                    FileType = fileType,
                    CreatedDateTime = DateTime.Now,
                    CreatedUserId = new Guid(),
                    IsActive = true,
                    IsDeleted = false,
                    UpdatedDateTime = DateTime.Now,
                    UpdatedUserId = new Guid(),
                    MemberFinancialAuditorId = Id
                };
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                    context.FinancialAuditoredFileUpload.Add(attachment);
                    context.SaveChanges();
                }
            }

            return 1;
        }
        public async Task<PagedResult<MemberFinancialAuditorView>> GetExechangeActorAnnulAuditor(NotificationQueryParameters notificationQuery)
        {
            try
            {
                var exachangeActorList = await context.MemberFinancialAuditor.Where(x => x.Status == 7 &&
                                                                                         (x.TradeExcutionStatusTypeId == 1))
                         .Select(n => new MemberFinancialAuditorView
                         {
                             MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                             ExchangeActorId = n.ExchangeActorId,
                             Ecxcode = n.Ecxcode,
                             OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                n.ExchangeActor.OrganizationNameEng,
                             DateReport = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate.Date).ToString()) :
                                                     n.ReportDate.Date.ToString(),
                             Remark = n.Remark,
                             AnnualAuditStatus = (notificationQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                    n.TradeExcutionStatusType.DescriptionEng,
                             TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                             Status = n.Status,
                             AnnualBudgetCloserId = n.AnnualBudgetCloserId,
                             Year = n.Year,
                             CustomerTypeId = n.CustomerTypeId,
                             ReportTypeId = n.ReportTypeId,
                             ReportPeriodId = n.ReportPeriodId,
                             EndDate = n.EndDate,
                             StartDate = n.StartDate
                         })
                         .OrderBy(n => n.TradeExcutionStatusTypeId)
                         .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                         .AsNoTracking()
                         .ToListAsync();
                return new PagedResult<MemberFinancialAuditorView>()
                {
                    Items = mapper.Map<List<MemberFinancialAuditorView>>(exachangeActorList),
                    ItemsCount = context.MemberFinancialAuditor.Where(x => x.Status == 7 &&
                                                                      (x.TradeExcutionStatusTypeId == 1)).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<MemberFinancialAuditorView>> GetMemberAnnulAuditor(NotificationQueryParameters notificationQuery, Guid ExchangeActorId)
        {
            try
            {
                var exachangeActorList = await context.MemberFinancialAuditor.Where(x => x.Status == 7 &&
                                                                                    x.TradeExcutionStatusTypeId == 2 &&
                                                                                    x.ExchangeActorId == ExchangeActorId)
                         .Select(n => new MemberFinancialAuditorView
                         {
                             MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                             ExchangeActorId = n.ExchangeActorId,
                             Ecxcode = n.Ecxcode,
                             OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                n.ExchangeActor.OrganizationNameEng,
                             DateReport = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate).ToString()) :
                                                     n.ReportDate.Date.ToString(),
                             AuditorName = (context.ExchangeActor.Where(x => x.Ecxcode == n.Ecxcode)
                                                                                                                       .Select(w => new AuditorOrganizationalName
                                                                                                                       {
                                                                                                                           OrganizationName = notificationQuery.Lang == "et" ? w.OrganizationNameAmh : w.OrganizationNameEng
                                                                                                                       }).FirstOrDefault()),
                             Remark = n.Remark,
                             AnnualAuditStatus = (notificationQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                    n.TradeExcutionStatusType.DescriptionEng,
                             TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                         })
                         .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                         .AsNoTracking()
                         .ToListAsync();
                return new PagedResult<MemberFinancialAuditorView>()
                {
                    Items = mapper.Map<List<MemberFinancialAuditorView>>(exachangeActorList),
                    ItemsCount = context.MemberFinancialAuditor.Where(x => x.Status == 7 &&
                                                                       x.ExchangeActorId == ExchangeActorId &&
                                                                       x.TradeExcutionStatusTypeId == 2).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateFinancialAuditor(MemberFinancialAuditorDTO memberFinancial)
        {
            try
            {

                var financialAuditor = await context.MemberFinancialAuditor
                       .Where(x => x.MemberFinancialAuditorId == memberFinancial.MemberFinancialAuditorId)
                       .AsNoTracking()
                       .FirstOrDefaultAsync();
                if (financialAuditor != null)
                {
                    var updateFinancialAuditor = mapper.Map(memberFinancial, financialAuditor);
                    context.Entry(financialAuditor).State = EntityState.Modified;
                }
                await context.SaveChangesAsync();
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<MemberFinancialAuditorView>> GetAllMemberFinancialAuditedList(ClientTradeQueryParameters clientTradeQuery)
        {
            try
            {
                int result = 0;
                if(clientTradeQuery.CustomerTypeId == 88 || clientTradeQuery.CustomerTypeId == 88)
                {
                    var financialAuditor = await context.MemberFinancialAuditor.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                                                                                                         ((string.IsNullOrEmpty(clientTradeQuery.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQuery.ECXCode)) &&
                                                                                                                                         (string.IsNullOrEmpty(clientTradeQuery.From) ||
                                                                                                                                          x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.From)) >= 0) &&
                                                                                                                                          (string.IsNullOrEmpty(clientTradeQuery.To) ||
                                                                                                                                          x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.To)) <= 0) &&
                                                                                                                                          (clientTradeQuery.AnnualBudgetCloserId == 0 || x.AnnualBudgetCloserId == clientTradeQuery.AnnualBudgetCloserId) &&
                                                                                                                                          (clientTradeQuery.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == clientTradeQuery.CustomerTypeId) &&
                                                                                                                                          (clientTradeQuery.FieldBusinessId == 0 || x.ExchangeActor.BuisnessFiledId == clientTradeQuery.FieldBusinessId) &&
                                                                                                                                          (clientTradeQuery.Year == 0 || x.Year == clientTradeQuery.Year))
                                                                                                .Select(n => new MemberFinancialAuditorView
                                                                                                {
                                                                                                    MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                                                                                                    ExchangeActorId = n.ExchangeActorId,
                                                                                                    Ecxcode = n.Ecxcode,
                                                                                                    OrganizationName = (clientTradeQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                                           n.ExchangeActor.OrganizationNameEng,
                                                                                                    DateReport = (clientTradeQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate).ToString()) :
                                                                                                                n.ReportDate.Date.ToString(),
                                                                                                    AuditorName = (context.ExchangeActor.Where(x => x.Ecxcode == n.Ecxcode)
                                                                                                                       .Select(w => new AuditorOrganizationalName
                                                                                                                       {
                                                                                                                           OrganizationName = clientTradeQuery.Lang == "et" ? w.OrganizationNameAmh: w.OrganizationNameEng
                                                                                                                       }).FirstOrDefault()),
                                                                                                    Remark = n.Remark,
                                                                                                    AnnualAuditStatus = (clientTradeQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                                                                               n.TradeExcutionStatusType.DescriptionEng,
                                                                                                    TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                                                                                                })
                                                                                                .Paging(clientTradeQuery.PageCount, clientTradeQuery.PageNumber)
                                                                                                .AsNoTracking()
                                                                                                .ToListAsync();
                    result = financialAuditor.Count();
                    return new PagedResult<MemberFinancialAuditorView>()
                    {
                        Items = mapper.Map<List<MemberFinancialAuditorView>>(financialAuditor),
                        ItemsCount = result
                    };
                }
                else if(clientTradeQuery.CustomerTypeId == 5 || clientTradeQuery.CustomerTypeId == 6 || clientTradeQuery.CustomerTypeId == 90)
                {
                    var financialAuditor = await context.MemberFinancialAuditor.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                                                                                     ((string.IsNullOrEmpty(clientTradeQuery.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQuery.ECXCode)) &&
                                                                                                                     (string.IsNullOrEmpty(clientTradeQuery.From) ||
                                                                                                                      x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.From)) >= 0) &&
                                                                                                                      (string.IsNullOrEmpty(clientTradeQuery.To) ||
                                                                                                                      x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.To)) <= 0) &&
                                                                                                                      (clientTradeQuery.AnnualBudgetCloserId == 0 || x.AnnualBudgetCloserId == clientTradeQuery.AnnualBudgetCloserId) &&
                                                                                                                      (clientTradeQuery.CustomerTypeId == 0 || x.ExchangeActor.CustomerTypeId == clientTradeQuery.CustomerTypeId) &&
                                                                                                                      (clientTradeQuery.FieldBusinessId == 0 || x.ExchangeActor.BuisnessFiledId == clientTradeQuery.FieldBusinessId) &&
                                                                                                                      (clientTradeQuery.Year == 0 || x.Year == clientTradeQuery.Year))
                                                                            .Select(n => new MemberFinancialAuditorView
                                                                            {
                                                                                MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                                                                                ExchangeActorId = n.ExchangeActorId,
                                                                                Ecxcode = n.Ecxcode,
                                                                                OrganizationName = (clientTradeQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                       n.ExchangeActor.OrganizationNameEng,
                                                                                DateReport = (clientTradeQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate).ToString()) :
                                                                                            n.ReportDate.Date.ToString(),
                                                                                AuditorName = (context.ExchangeActor.Where(x => x.Ecxcode == n.Ecxcode)
                                                                                                   .Select(w => new AuditorOrganizationalName
                                                                                                   {
                                                                                                       OrganizationName = clientTradeQuery.Lang == "et"? w.OrganizationNameAmh: w.OrganizationNameEng
                                                                                                   }).FirstOrDefault()
                                                                                                        ),
                                                                                Remark = n.Remark,
                                                                                AnnualAuditStatus = (clientTradeQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                                                           n.TradeExcutionStatusType.DescriptionEng,
                                                                                TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                                                                            })
                                                                            .Paging(clientTradeQuery.PageCount, clientTradeQuery.PageNumber)
                                                                            .AsNoTracking()
                                                                            .ToListAsync();
                    result = financialAuditor.Count();
                    return new PagedResult<MemberFinancialAuditorView>()
                    {
                        Items = mapper.Map<List<MemberFinancialAuditorView>>(financialAuditor),
                        ItemsCount = result
                    };
                }
                else
                {
                    var financialAuditor = await context.MemberFinancialAuditor.Where(x => x.TradeExcutionStatusTypeId == 2 &&
                                                                                                  ((string.IsNullOrEmpty(clientTradeQuery.ECXCode)) || x.ExchangeActor.Ecxcode.Contains(clientTradeQuery.ECXCode)) &&
                                                                                                  (string.IsNullOrEmpty(clientTradeQuery.From) ||
                                                                                                   x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.From)) >= 0) &&
                                                                                                   (string.IsNullOrEmpty(clientTradeQuery.To) ||
                                                                                                   x.ReportDate.Date.CompareTo(DateTime.Parse(clientTradeQuery.To)) <= 0)  &&
                                                                                                   (clientTradeQuery.AnnualBudgetCloserId == 0 || x.AnnualBudgetCloserId == clientTradeQuery.AnnualBudgetCloserId) &&
                                                                                                   (clientTradeQuery.FieldBusinessId == 0 || x.ExchangeActor.BuisnessFiledId == clientTradeQuery.FieldBusinessId) &&
                                                                                                   (clientTradeQuery.Year == 0 || x.Year == clientTradeQuery.Year))
                                                         .Select(n => new MemberFinancialAuditorView
                                                         {
                                                             MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                                                             ExchangeActorId = n.ExchangeActorId,
                                                             Ecxcode = n.Ecxcode,
                                                             OrganizationName = (clientTradeQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                    n.ExchangeActor.OrganizationNameEng,
                                                             DateReport = (clientTradeQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.ReportDate).ToString()) :
                                                                         n.ReportDate.Date.ToString(),
                                                             AuditorName = (context.ExchangeActor.Where(x => x.Ecxcode == n.Ecxcode)
                                                                                                   .Select(w => new AuditorOrganizationalName
                                                                                                   {
                                                                                                       OrganizationName = clientTradeQuery.Lang == "et" ? w.OrganizationNameAmh : w.OrganizationNameEng
                                                                                                   }).FirstOrDefault()
                                                                                                        ),
                                                             Remark = n.Remark,
                                                             AnnualAuditStatus = (clientTradeQuery.Lang == "et") ? n.TradeExcutionStatusType.DescriptionAmh :
                                                                                                        n.TradeExcutionStatusType.DescriptionEng,
                                                             TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId,
                                                         })
                                                         .Paging(clientTradeQuery.PageCount, clientTradeQuery.PageNumber)
                                                         .AsNoTracking()
                                                         .ToListAsync();
                    result = financialAuditor.Count();
                    return new PagedResult<MemberFinancialAuditorView>()
                    {
                        Items = mapper.Map<List<MemberFinancialAuditorView>>(financialAuditor),
                        ItemsCount = result
                    };
                }
                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberFinancialAuditorLateViewModel>> GetLateFinancialAuditorList(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                if(clientTrade.CustomerTypeId == 72 || clientTrade.CustomerTypeId == 88)
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                (x.TradeExcutionStatusTypeId == 2) &&
                                                                 x.ReportDate.Date.CompareTo(x.EndDate) > 0 &&
                                                                ((clientTrade.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTrade.CustomerTypeId)) &&
                                                                ((clientTrade.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTrade.CustomerTypeId)) &&
                                                               ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                               ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                               )
                                                 .Select(n => new MemberFinancialAuditorLateViewModel
                                                 {
                                                     CustomerFullName =  n.ExchangeActor.Ecxcode,
                                                     OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                     n.ExchangeActor.OrganizationNameEng,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                 n.ExchangeActor.MemberCategory.DescriptionEng,
                                                     MobileNo = n.ExchangeActor.MobileNo,
                                                     ReportPeriodId = n.ReportPeriodId,
                                                     ReportTypeId = n.ReportTypeId,
                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(x => x.Year == clientTrade.Year &&
                                                                                                                                      x.ReportTypeId == 4 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTrade.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()



                                                 })
                                                 .AsNoTracking()
                                                 .ToListAsync();
                    return financialauditor;


                }
                else if(clientTrade.CustomerTypeId == 90 || clientTrade.CustomerTypeId == 5 || clientTrade.CustomerTypeId == 6)
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                (x.TradeExcutionStatusTypeId == 2) &&
                                                                 x.ReportDate.Date.CompareTo(x.EndDate) > 0 &&
                                                                ((clientTrade.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTrade.CustomerTypeId)) &&
                                                                ((clientTrade.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTrade.CustomerTypeId)) &&
                                                               ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                               ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                               )
                                                 .Select(n => new MemberFinancialAuditorLateViewModel
                                                 {
                                                     CustomerFullName =  n.ExchangeActor.Ecxcode,
                                                     OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                     n.ExchangeActor.OrganizationNameEng,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                 n.ExchangeActor.CustomerType.DescriptionEng,
                                                     MobileNo = n.ExchangeActor.MobileNo,
                                                     ReportPeriodId = n.ReportPeriodId,
                                                     ReportTypeId = n.ReportTypeId,
                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(x => x.Year == clientTrade.Year &&
                                                                                                                                      x.ReportTypeId == 4 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTrade.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()




                                                 })
                                                 .AsNoTracking()
                                                 .ToListAsync();
                    return financialauditor;

                }
                else
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                (x.TradeExcutionStatusTypeId == 2) &&
                                                                 x.ReportDate.Date.CompareTo(x.EndDate) > 0 &&
                                                                (x.ExchangeActor.CustomerTypeId == 90 || x.ExchangeActor.CustomerTypeId == 6) &&
                                                               ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                               ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                               )
                                                 .Select(n => new MemberFinancialAuditorLateViewModel
                                                 {
                                                     CustomerFullName = n.ExchangeActor.Ecxcode,
                                                     OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                     n.ExchangeActor.OrganizationNameEng,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                 n.ExchangeActor.CustomerType.DescriptionEng,
                                                     MobileNo = n.ExchangeActor.MobileNo,
                                                     ReportPeriodId = n.ReportPeriodId,
                                                     ReportTypeId = n.ReportTypeId,
                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(x => x.Year == clientTrade.Year &&
                                                                                                                                      x.ReportTypeId == 4 && x.ReportAnalysisId == 132 &&
                                                                                                                                      x.ReportPeriodId == clientTrade.ReportPeriodId &&
                                                                                                                                      x.MemberTradeViolation.ExchangeActorId == n.ExchangeActorId).Count()




                                                 })
                                                 .AsNoTracking()
                                                 .ToListAsync();
                    return financialauditor;


                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberFinancialAuditorViewModel>> GetOnTimeReportFinancialAuditor(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                if(clientTrade.CustomerTypeId == 72 || clientTrade.CustomerTypeId == 88)
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                                    (x.TradeExcutionStatusTypeId == 2) &&
                                                                                   (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.EndDate) <= 0) &&
                                                                                   ((clientTrade.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == clientTrade.CustomerTypeId)) &&
                                                                                    ((clientTrade.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTrade.FieldBusinessId)) &&
                                                                                  ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                                                  ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                                                  )
                                                                    .Select(n => new MemberFinancialAuditorViewModel
                                                                    {
                                                                        CustomerFullName =  n.ExchangeActor.Ecxcode,
                                                                        OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                        n.ExchangeActor.OrganizationNameEng,
                                                                        ExchangeActorId = n.ExchangeActorId,
                                                                        CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                                    n.ExchangeActor.MemberCategory.DescriptionEng,
                                                                        MobileNo = n.ExchangeActor.MobileNo,
                                                      //ReportPeriodId = n.ReportPeriodId,
                                                      //ReportTypeId = n.ReportTypeId



                                                  })
                                                                    .AsNoTracking()
                                                                    .ToListAsync();
                    return financialauditor;

                }
                else if (clientTrade.CustomerTypeId == 6 || clientTrade.CustomerTypeId == 5 || clientTrade.CustomerTypeId == 90)
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                                    (x.TradeExcutionStatusTypeId == 2) &&
                                                                                   (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.EndDate) <= 0) &&
                                                                                   ((clientTrade.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == clientTrade.CustomerTypeId)) &&
                                                                                    ((clientTrade.FieldBusinessId == 0) || (x.ExchangeActor.BuisnessFiledId == clientTrade.FieldBusinessId)) &&
                                                                                  ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                                                  ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                                                  )
                                                                    .Select(n => new MemberFinancialAuditorViewModel
                                                                    {
                                                                        CustomerFullName =  n.ExchangeActor.Ecxcode,
                                                                        OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                                        n.ExchangeActor.OrganizationNameEng,
                                                                        ExchangeActorId = n.ExchangeActorId,
                                                                        CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                                    n.ExchangeActor.CustomerType.DescriptionEng,
                                                                        MobileNo = n.ExchangeActor.MobileNo,
                                                      //ReportPeriodId = n.ReportPeriodId,
                                                      //ReportTypeId = n.ReportTypeId



                                                  })
                                                                    .AsNoTracking()
                                                                    .ToListAsync();
                    return financialauditor;

                }
                else
                {
                    var financialauditor = await context.MemberFinancialAuditor.Where(x => x.ReportTypeId == 4 &&
                                                                (x.TradeExcutionStatusTypeId == 2) &&
                                                               (x.ReportDate.Date.CompareTo(x.StartDate) >= 0 && x.ReportDate.Date.CompareTo(x.EndDate) <= 0) &&
                                                              // ((clientTrade.CustomerTypeId == 0) || (x.CustomerTypeId == clientTrade.CustomerTypeId)) &&
                                                              ((clientTrade.Year == 0) || (x.Year == clientTrade.Year)) &&
                                                              ((clientTrade.AnnualBudgetCloserId == 0) || (x.AnnualBudgetCloserId == clientTrade.AnnualBudgetCloserId))
                                                              )
                                                .Select(n => new MemberFinancialAuditorViewModel
                                                {
                                                    CustomerFullName =  n.ExchangeActor.Ecxcode,
                                                    OrganizationName = (clientTrade.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                    n.ExchangeActor.OrganizationNameEng,
                                                    ExchangeActorId = n.ExchangeActorId,
                                                    CustomerType = (clientTrade.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh :
                                                                                                n.ExchangeActor.CustomerType.DescriptionEng,
                                                    MobileNo = n.ExchangeActor.MobileNo,
                                                      //ReportPeriodId = n.ReportPeriodId,
                                                      //ReportTypeId = n.ReportTypeId



                                                  })
                                                .AsNoTracking()
                                                .ToListAsync();
                    return financialauditor;


                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberFinancialAuditorViewModel> GetFinancialAuditorNotReportEng(ClientTradeQueryParameters clientTrade)
        {
            List<MemberFinancialAuditorViewModel> data;
            try
            {
                if(clientTrade.FieldBusinessId == 0)
                {
                    switch (clientTrade.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                      @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                       ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                                     clientTrade.CustomerTypeId,
                                     clientTrade.AnnualBudgetCloserId,
                                     clientTrade.Year,
                                     clientTrade.AnnualBudgetCloserId,
                                     clientTrade.Year)
                                    .OrderBy(x => x.OrganizationName)
                                    .AsNoTracking()
                                    .ToList();
                            break;
                        case 6:
                        case 5:
                        case 90:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                      @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                                                             clientTrade.CustomerTypeId,  
                                                             clientTrade.AnnualBudgetCloserId,
                                                             clientTrade.Year,
                                                             clientTrade.AnnualBudgetCloserId,
                                                             clientTrade.Year)
                                                            .OrderBy(x => x.OrganizationName)
                                                            .AsNoTracking()
                                                            .ToList();
                            break;
                        default:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                      @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {0} and Year = {1}) > 365)",
                                 clientTrade.AnnualBudgetCloserId,
                                 clientTrade.Year,
                                 clientTrade.AnnualBudgetCloserId,
                                 clientTrade.Year)
                                .OrderBy(x => x.OrganizationName)
                                .AsNoTracking()
                                .ToList();
                            break;
                    }
                
                }
                else
                {
                    switch (clientTrade.CustomerTypeId)
                    {
                        case 72:
                        case 88:

                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                                @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                       ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and  ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {2} and Year = {3}) > 365)",
                                     clientTrade.CustomerTypeId,
                                     clientTrade.FieldBusinessId,
                                     clientTrade.AnnualBudgetCloserId,
                                     clientTrade.Year,
                                     clientTrade.AnnualBudgetCloserId,
                                     clientTrade.Year)
                                    .OrderBy(x => x.OrganizationName)
                                    .AsNoTracking()
                                    .ToList();
                            break;
                        case 6:
                        case 5:
                        case 90:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                                                        @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                       ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionEng as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and  ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {2} and Year = {3}) > 365)",
                                                             clientTrade.CustomerTypeId,
                                                             clientTrade.FieldBusinessId,
                                                             clientTrade.AnnualBudgetCloserId,
                                                             clientTrade.Year,
                                                             clientTrade.AnnualBudgetCloserId,
                                                             clientTrade.Year)
                                                            .OrderBy(x => x.OrganizationName)
                                                            .AsNoTracking()
                                                            .ToList();
                            break;
                        default:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                            @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameEng as OrganizationName,
                       ecx.Ecxcode as CustomerFullName ,
                       l.DescriptionEng as CustomerType,
                       ecx.MobileNo as MobileNo,
                       ecx.Tel as RegularPhone,
                       (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90 OR ecx.CustomerTypeId = 5) and 
                      ecx.Status = 20 and  ecx.BuisnessFiledId = {0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                                 clientTrade.FieldBusinessId,
                                 clientTrade.AnnualBudgetCloserId,
                                 clientTrade.Year,
                                 clientTrade.AnnualBudgetCloserId,
                                 clientTrade.Year)
                                .OrderBy(x => x.OrganizationName)
                                .AsNoTracking()
                                .ToList();
                            break;
                    }

                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberFinancialAuditorViewModel> GetFinancialAuditorNotReportAmh(ClientTradeQueryParameters clientTrade)
        {
            List<MemberFinancialAuditorViewModel> data;
            try
            {
                if(clientTrade.FieldBusinessId == 0)
                {
                    switch (clientTrade.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                                                @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName , 
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                                                     clientTrade.CustomerTypeId,
                                                     clientTrade.AnnualBudgetCloserId,
                                                     clientTrade.Year,
                                                     clientTrade.AnnualBudgetCloserId,
                                                     clientTrade.Year)
                                                    .OrderBy(x => x.OrganizationName)
                                                    .AsNoTracking()
                                                    .ToList();
                            break;
                        case 5:
                        case 6:
                        case 90:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                    @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                         clientTrade.CustomerTypeId,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year)
                        .OrderBy(x => x.OrganizationName)
                        .AsNoTracking()
                        .ToList();
                            break;
                        default:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                    @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {3} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {2}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId=90) and ecx.Status = 20 and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {0} and Year = {1}) > 365)",
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year)
                        .OrderBy(x => x.OrganizationName)
                        .AsNoTracking()
                        .ToList();
                            break;

                    }
                }
                else
                {
                    switch (clientTrade.CustomerTypeId)
                    {
                        case 72:
                        case 88:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                                                @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName , 
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.MemberCategoryId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.MemberCategoryId = {0}) and ecx.Status = 20  and ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {2} and Year = {3}) > 365)",
                                                      clientTrade.CustomerTypeId,
                                                      clientTrade.FieldBusinessId,
                                                     clientTrade.AnnualBudgetCloserId,
                                                     clientTrade.Year,
                                                     clientTrade.AnnualBudgetCloserId,
                                                     clientTrade.Year)
                                                    .OrderBy(x => x.OrganizationName)
                                                    .AsNoTracking()
                                                    .ToList();
                            break;
                        case 5:
                        case 6:
                        case 90:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                    @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {5} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {4}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = {0}) and ecx.Status = 20 and ecx.BuisnessFiledId = {1} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {2} and Year = {3}) > 365)",
                         clientTrade.CustomerTypeId,
                         clientTrade.FieldBusinessId,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year)
                        .OrderBy(x => x.OrganizationName)
                        .AsNoTracking()
                        .ToList();
                            break;
                        default:
                            data = context.MemberFinancialAuditorViewModel.FromSqlRaw(
                    @"SELECT distinct ecx.ExchangeActorId, ecx.OrganizationNameAmh as OrganizationName,
                      ecx.Ecxcode as CustomerFullName ,
                      l.DescriptionAmh as CustomerType,
                      ecx.MobileNo as MobileNo,
                      ecx.Tel as RegularPhone,
                      (Select COUNT(*) from TradeExcutionViolationRecord tevr
					                         Join MemberTradeViolation mtv  on
						                     tevr.MemberTradeViolationId = mtv.MemberTradeViolationId
						                     where mtv.ExchangeActorId = ecx.ExchangeActorId and 
						                     tevr.Year = {4} and tevr.ReportTypeId = 4 and
								             tevr.ReportAnalysisId = 134 and tevr.ReportPeriodId = {3}) as TotalNoMembersVioaltion
                      from ExchangeActor as ecx 
                      join Lookup as l on ecx.CustomerTypeId = l.LookupId 
                      JOIN MemberFinancialAuditor mct on  mct.ExchangeActorId != ecx.ExchangeActorId
                      where (ecx.CustomerTypeId = 6 OR ecx.CustomerTypeId=90) and 
                      ecx.Status = 20 and ecx.BuisnessFiledId = {0} and
                      ecx.ExchangeActorId  NOT IN(select ExchangeActorId from MemberFinancialAuditor where
					  (select DATEDIFF(day, EndDate,SYSDATETIME()) AS 'Duration' from AnnualBudgetCloser where AnnualBudgetCloserId = {1} and Year = {2}) > 365)",
                         clientTrade.FieldBusinessId,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year,
                         clientTrade.AnnualBudgetCloserId,
                         clientTrade.Year)
                        .OrderBy(x => x.OrganizationName)
                        .AsNoTracking()
                        .ToList();
                            break;

                    }

                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

   public List<NetWorthBetweenMemberAuditor> GetNetWorthDiffBetweenMemberAuditor(ClientTradeQueryParameters clientTrade)
    {
     List<NetWorthBetweenMemberAuditor> data;
    try
     {
      if (clientTrade.Lang == "et")
       {
         data = context.NetWorthBetweenMemberAuditor.FromSqlRaw(
       @"select exchangeactor.OrganizationName as OrganizationName,
       exchangeactor.NetAssets as NetWorth,
	   exchangeactor.Ecxcode as ECXcode,
	   auditor.OrganizationName as AuditorName,
	   auditor.NetAssets as AuditorNetWorth from
       (select  ecx.OrganizationNameAmh as 'OrganizationName',mtf.NetAssets as 
       'NetAssets',ecx.Ecxcode as 'Ecxcode'
       from MemberFinancialAuditor as mfa
       join ExchangeActor as ecx on
       mfa.ExchangeActorId = ecx.ExchangeActorId
       join MemberTradeFinancial as mtf on
       mfa.MemberFinancialAuditorId = mtf.MemberFinancialAuditorId
       where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90)
       and mfa.AnnualBudgetCloserId = {0} and mfa.Year = {1}) as exchangeactor inner join 
      (select  ecx.OrganizationNameAmh as 'OrganizationName',mtf.NetAssets as 'NetAssets',
      mfa.Ecxcode as 'Ecxcode' from MemberFinancialAuditor as mfa
      join ExchangeActor as ecx on mfa.ExchangeActorId = ecx.ExchangeActorId
      join MemberTradeFinancial as mtf on mfa.MemberFinancialAuditorId = mtf.MemberFinancialAuditorId
      where ecx.CustomerTypeId = 5 and mfa.AnnualBudgetCloserId = {2} and mfa.Year = {3}) as auditor on
      exchangeactor.Ecxcode = auditor.Ecxcode Where exchangeactor.NetAssets != auditor.NetAssets",
      clientTrade.AnnualBudgetCloserId,
      clientTrade.Year,
      clientTrade.AnnualBudgetCloserId,
      clientTrade.Year)
      .OrderBy(x => x.OrganizationName)
      .AsNoTracking()
      .ToList();
      }
    else
     {
         data = context.NetWorthBetweenMemberAuditor.FromSqlRaw(
       @"select exchangeactor.OrganizationName as OrganizationName,
       exchangeactor.NetAssets as NetWorth,
	   exchangeactor.Ecxcode as ECXcode,
	   auditor.OrganizationName as AuditorName,
	   auditor.NetAssets as AuditorNetWorth from
       (select  ecx.OrganizationNameEng as 'OrganizationName',mtf.NetAssets as 
       'NetAssets',ecx.Ecxcode as 'Ecxcode'
       from MemberFinancialAuditor as mfa
       join ExchangeActor as ecx on
       mfa.ExchangeActorId = ecx.ExchangeActorId
       join MemberTradeFinancial as mtf on
       mfa.MemberFinancialAuditorId = mtf.MemberFinancialAuditorId
       where (ecx.CustomerTypeId = 6 or ecx.CustomerTypeId = 90)
       and mfa.AnnualBudgetCloserId = {0} and mfa.Year = {1}) as exchangeactor inner join 
      (select  ecx.OrganizationNameEng as 'OrganizationName',mtf.NetAssets as 'NetAssets',
      mfa.Ecxcode as 'Ecxcode' from MemberFinancialAuditor as mfa
      join ExchangeActor as ecx on mfa.ExchangeActorId = ecx.ExchangeActorId
      join MemberTradeFinancial as mtf on mfa.MemberFinancialAuditorId = mtf.MemberFinancialAuditorId
      where ecx.CustomerTypeId = 5 and mfa.AnnualBudgetCloserId = {2} and mfa.Year = {3}) as auditor on
      exchangeactor.Ecxcode = auditor.Ecxcode Where exchangeactor.NetAssets != auditor.NetAssets",
                clientTrade.AnnualBudgetCloserId,
                clientTrade.Year,
                clientTrade.AnnualBudgetCloserId,
                clientTrade.Year)
                .OrderBy(x => x.OrganizationName)
                .AsNoTracking()
                .ToList();
                }

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<MemberFinancialAuditorViewModel> GetNotMemberFinancialAuditor(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                List<MemberFinancialAuditorViewModel> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = GetFinancialAuditorNotReportAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = GetFinancialAuditorNotReportEng(clientTradeQueryParameters);
                }
                return tradeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<MemberFinancialAuditorView> GetMemberAnnualAuditerById(Guid memberClientId)
        {
            try
            {
                var clientTrade = await context.MemberFinancialAuditor
                    .Where(x => x.ExchangeActorId == memberClientId)
                    .Select(n => new MemberFinancialAuditorView
                    {
                        MemberFinancialAuditorId = n.MemberFinancialAuditorId,
                        ExchangeActorId = n.ExchangeActorId,
                        CustomerTypeId = n.CustomerTypeId,
                        OrganizationName = n.ExchangeActor.OrganizationNameAmh,
                        CustomerType = n.ExchangeActor.MemberCategory.DescriptionAmh,
                        ReportTypeId = n.ReportTypeId,
                        ReportPeriodId = n.ReportPeriodId,
                        Year = n.Year,
                        Remark = n.Remark,
                        Ecxcode = n.Ecxcode,
                        AnnualBudgetCloserId = n.AnnualBudgetCloserId,
                        TradeExcutionStatusTypeId = n.TradeExcutionStatusTypeId
                    })
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return clientTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<MemberFinancialAuditorViewModel> GetActivSuspendedCanceled(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<MemberFinancialAuditorViewModel> exchangeActorSt = null;
                var annualBudgetCloser = context.AnnualBudgetCloser.Where(x => x.AnnualBudgetCloserId == exchangeActorStatus.AnnualBudgetCloserId).FirstOrDefault();
                switch (exchangeActorStatus.ExchageActorStatus)
                {
                    case 1:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                                          x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                                                 .Select(n => new MemberFinancialAuditorViewModel
                                                                 {
                                                                     OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                     CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                                                     MobileNo = n.MobileNo,
                                                                     RegularPhone = n.Tel
                                                                 })
                                                                 .AsNoTracking()
                                                                 .ToList();
                        }
                        else if(exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                              x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                     .Select(n => new MemberFinancialAuditorViewModel
                                     {
                                         OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                         CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                         MobileNo = n.MobileNo,
                                         RegularPhone = n.Tel
                                     })
                                     .AsNoTracking()
                                     .ToList();
                        }
                        else
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                           (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                           x.CustomerTypeId == 5))
                                     .Select(n => new MemberFinancialAuditorViewModel
                                     {
                                         OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                         CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                         MobileNo = n.MobileNo,
                                         RegularPhone = n.Tel
                                     })
                                     .AsNoTracking()
                                     .ToList();
                        }

                        break;

                    case 2:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                        x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                                               .Select(n => new MemberFinancialAuditorViewModel
                                                               {
                                                                   OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                   CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                                                   MobileNo = n.MobileNo,
                                                                   RegularPhone = n.Tel
                                                               })
                                                               .AsNoTracking()
                                                               .ToList();
                        }
                        else if(exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                        x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                                               .Select(n => new MemberFinancialAuditorViewModel
                                                               {
                                                                   OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                   CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                   MobileNo = n.MobileNo,
                                                                   RegularPhone = n.Tel
                                                               })
                                                               .AsNoTracking()
                                                               .ToList();
                        }
                        else
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 31 && 
                                                                             (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                              x.CustomerTypeId == 5))
                                                                                           .Select(n => new MemberFinancialAuditorViewModel
                                                                                           {
                                                                                               OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                                               CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                                               MobileNo = n.MobileNo,
                                                                                               RegularPhone = n.Tel
                                                                                           })
                                                                                           .AsNoTracking()
                                                                                           .ToList();
                        }

                        break;
                    case 3:
                        if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                             x.MemberCategoryId == exchangeActorStatus.CustomerTypeId)
                                    .Select(n => new MemberFinancialAuditorViewModel
                                    {
                                        OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                        CustomerType = (exchangeActorStatus.Lang == "et") ? n.MemberCategory.DescriptionAmh : n.MemberCategory.DescriptionEng,
                                        MobileNo = n.MobileNo,
                                        RegularPhone = n.Tel
                                    })
                                    .AsNoTracking()
                                    .ToList();
                        }
                        else if(exchangeActorStatus.CustomerTypeId == 90 || exchangeActorStatus.CustomerTypeId == 5)
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                                         x.CustomerTypeId == exchangeActorStatus.CustomerTypeId)
                                                                .Select(n => new MemberFinancialAuditorViewModel
                                                                {
                                                                    OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                    CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                    MobileNo = n.MobileNo,
                                                                    RegularPhone = n.Tel
                                                                })
                                                                .AsNoTracking()
                                                                .ToList();
                        }
                        else
                        {
                            exchangeActorSt = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                          (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                           x.CustomerTypeId == 5))
                                                                                            .Select(n => new MemberFinancialAuditorViewModel
                                                                                            {
                                                                                                OrganizationName = (exchangeActorStatus.Lang == "et") ? n.OrganizationNameAmh : n.OrganizationNameEng,
                                                                                                CustomerType = (exchangeActorStatus.Lang == "et") ? n.CustomerType.DescriptionAmh : n.CustomerType.DescriptionEng,
                                                                                                MobileNo = n.MobileNo,
                                                                                                RegularPhone = n.Tel
                                                                                            })
                                                                                            .AsNoTracking()
                                                                                            .ToList();
                        }
                        break;
                    default:
                        exchangeActorSt = null;
                        break;
                }
                return exchangeActorSt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TotalQuantyOfExchangeactor> GetTotalQuantyOfExchangeactor(ExchangeActorStatusSearchQuery exchangeActorStatus)
        {
            try
            {
                List<TotalQuantyOfExchangeactor> listOfExchangeactorStatus = new List<TotalQuantyOfExchangeactor>();
                if(exchangeActorStatus.CustomerTypeId != 0)
                {
                    TotalQuantyOfExchangeactor activeExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorActive = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                  x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);
                    }
                    else
                    {
                        var exchangeactorActive = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                               x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);
                    }
                    TotalQuantyOfExchangeactor injectedExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorInjuction = context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                  x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);
                    }
                    else
                    {
                        var exchangeactorInjuction = context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                          x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);
                    }
                    TotalQuantyOfExchangeactor cancelegExchangeActor = new TotalQuantyOfExchangeactor();
                    if (exchangeActorStatus.CustomerTypeId == 72 || exchangeActorStatus.CustomerTypeId == 88)
                    {
                        var exchangeactorCanceled = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                                 x.MemberCategoryId == exchangeActorStatus.CustomerTypeId).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorCanceled;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);
                    }
                    else
                    {
                        var exchangeactorActive = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                             x.CustomerTypeId == exchangeActorStatus.CustomerTypeId).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);
                    }

                }
                else
                {
                    TotalQuantyOfExchangeactor activeExchangeActor = new TotalQuantyOfExchangeactor();
                    
                        var exchangeactorActive = context.ExchangeActor.Where(x => x.Status == 20 &&
                                                                                 (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                                  x.CustomerTypeId == 5)).Count();
                        activeExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "ሪፖርት የሚያደርጉ የግብይት ተሳታፊዎች" : "Active Exchange Actors";
                        activeExchangeActor.Quantity = exchangeactorActive;
                        listOfExchangeactorStatus.Add(activeExchangeActor);

                    TotalQuantyOfExchangeactor injectedExchangeActor = new TotalQuantyOfExchangeactor();
                       var exchangeactorInjuction = context.ExchangeActor.Where(x => x.Status == 31 &&
                                                                                 (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                                  x.CustomerTypeId == 5)).Count();
                        injectedExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "እግድ ላይ ያሉ የግብይት ተሳታፊዎች" : "Exchange Actors Under Injunction";
                        injectedExchangeActor.Quantity = exchangeactorInjuction;
                        listOfExchangeactorStatus.Add(injectedExchangeActor);

                    TotalQuantyOfExchangeactor cancelegExchangeActor = new TotalQuantyOfExchangeactor();                   
                        var exchangeactorCanceled = context.ExchangeActor.Where(x => x.Status == 64 &&
                                                                                     (x.CustomerTypeId == 6 || x.CustomerTypeId == 90 ||
                                                                                  x.CustomerTypeId == 5)).Count();
                        cancelegExchangeActor.ExchangeActorStatus = (exchangeActorStatus.Lang == "et") ? "የተሰረዙ የግብይት ተሳታፊዎች" : "Cancelled Exchange Actors";
                        cancelegExchangeActor.Quantity = exchangeactorCanceled;
                        listOfExchangeactorStatus.Add(cancelegExchangeActor);

                    }
               
                return listOfExchangeactorStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
