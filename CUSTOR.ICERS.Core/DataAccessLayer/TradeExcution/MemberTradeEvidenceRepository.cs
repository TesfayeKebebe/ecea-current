﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class MemberTradeEvidenceRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper _mapper;

        public MemberTradeEvidenceRepository(ECEADbContext _context, IMapper mapper)
        {
            context = _context;
            _mapper = mapper;
        }

        public async Task<int> Create(MemberTradeEvidence memberTradeEvidence)
        {
            try
            {
                MemberTradeEvidence newAttachment = _mapper.Map<MemberTradeEvidence>(memberTradeEvidence);
                context.MemberTradeEvidence.Add(newAttachment);

                await context.SaveChangesAsync();

                return newAttachment.MemberClientTradeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public async Task<List<AttachmentViewModel>> GetAllByTradeEvidenceId(int tradeExcutionId)
        {
            try
            {
                var caseEvidence = await context.MemberTradeEvidence.Where(x => x.MemberClientTradeId == tradeExcutionId
                && x.IsActive == true && x.IsDeleted == false)
                .OrderByDescending(s => s.CreatedDateTime)
                .Select(n => new AttachmentViewModel
                {
                    ParentId = n.MemberClientTradeId,
                    AttachmentContent = n.EvidenceContent,
                    AttachmentId = n.MemberTradeEvidenceId,
                    CreatedDate = n.CreatedDateTime,
                    UpdatedDate = n.UpdatedDateTime,
                    SelectedWord = n.Remark,
                    FileType = n.FileType,
                    Url = n.Url,
                    AuditorId = n.AuditorId
                })
                .AsNoTracking().ToListAsync();
                return caseEvidence;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<StaticData4>> GetListOfAuditors(string lang)
        {
            try
            {
                var auditorList = await context.ExchangeActor.Where(x => x.CustomerTypeId == 6)
                .Select(x => new StaticData4
                {
                    Id = x.ExchangeActorId,
                    Description = (lang == "et") ? x.OwnerManager.FirstNameAmh + " " +
                                                  x.OwnerManager.FatherNameAmh + " " +
                                                  x.OwnerManager.GrandFatherNameAmh :
                                                  x.OwnerManager.FirstNameEng + " " +
                                                  x.OwnerManager.FatherNameEng + " " +
                                                  x.OwnerManager.GrandFatherNameEng,

                })
                .AsNoTracking()
                .ToListAsync();
                return auditorList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
