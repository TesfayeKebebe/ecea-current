﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using MimeKit.Encodings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{

    public class MemberTradeFinancialRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public MemberTradeFinancialRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }

        public async Task<List<MemberTradeFinancialViewModel>> GetFinancialListById(int id)
        {
            try
            {
                var financialList = await context.MemberTradeFinancial.Where(f => f.MemberClientTradeId == id)
                                         .Select(x => new MemberTradeFinancialViewModel
                                         {
                                             MemberTradeFinancialId = x.MemberTradeFinancialId,
                                             MemberClientTradeId = x.MemberClientTradeId,
                                             AccountsPriority = x.AccountsPriority,
                                             AdvancePayment = x.AdvancePayment,
                                             Building = x.Building,
                                             CollectedPayment = x.CollectedPayment,
                                             DepositsMoney = x.DepositsMoney,
                                             LongTermloanFromfinancial = x.LongTermloanFromfinancial,
                                             LongTermLoanPayable = x.LongTermLoanPayable,
                                             MortgageLoan = x.MortgageLoan,
                                             OfficeFurniture = x.OfficeFurniture,
                                             OverDraft = x.OverDraft,
                                             PayableDebts = x.PayableDebts,
                                             PerShare = x.PerShare,
                                             ShortTermLoan = x.ShortTermLoan,
                                             Stock = x.Stock,
                                             Tools = x.Tools,
                                             Vehicle = x.Vehicle,
                                             NetAssets = x.NetAssets,
                                             TotalDebts = x.TotalDebts,
                                             TotalFixedAsset = x.TotalFixedAsset,
                                             TotalIncome = x.TotalIncome,
                                             TotalLongTermDebts = x.TotalLongTermDebts,
                                             TotalPerShare = x.TotalPerShare,
                                             TotalTemporaryDebts = x.TotalTemporaryDebts,
                                             TotalWealth = x.TotalWealth,
                                             Computerandaccessories = x.Computerandaccessories

                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                return financialList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<MemberTradeFinancialViewModel>> GetMemberFinancialReport(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            int totalResultList = 0;
            try
            {
                List<MemberTradeFinancialViewModel> memberFinancialList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    memberFinancialList = await GetMemberFinancialReportAmh(clientTradeQueryParameters);
                }
                else
                {
                    memberFinancialList = await GetMemberFinancialReportEng(clientTradeQueryParameters);
                }
                totalResultList = memberFinancialList.Count();
                var pagedResult = memberFinancialList.AsQueryable().Paging(clientTradeQueryParameters.PageCount, clientTradeQueryParameters.PageNumber);
                return new PagedResult<MemberTradeFinancialViewModel>()
                {
                    Items = mapper.Map<List<MemberTradeFinancialViewModel>>(pagedResult),
                    ItemsCount = totalResultList
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }


        public async Task<List<MemberTradeFinancialViewModel>> GetMemberFinancialReportAmh(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                  f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                                       f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId))&&
                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId))&&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth
                                                            })
                                                            .AsNoTracking()
                                                            .ToListAsync();
                    return financialList;
                }
                else if (clientTradeQueryParameters.CustomerTypeId == 5 || clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                              f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                              f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                   ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                   f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                    ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                              )
                                        .Select(x => new MemberTradeFinancialViewModel
                                        {
                                            MemberTradeFinancialId = x.MemberTradeFinancialId,
                                            OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                                            NetAssets = x.NetAssets,
                                            TotalDebts = x.TotalDebts,
                                            TotalFixedAsset = x.TotalFixedAsset,
                                            TotalIncome = x.TotalIncome,
                                            TotalLongTermDebts = x.TotalLongTermDebts,
                                            TotalPerShare = x.TotalPerShare,
                                            TotalTemporaryDebts = x.TotalTemporaryDebts,
                                            TotalWealth = x.TotalWealth
                                        })
                                        .AsNoTracking()
                                        .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                  f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                                       f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameAmh,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth
                                                            })
                                                            .AsNoTracking()
                                                            .ToListAsync();
                    return financialList;
                }
                   

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeFinancialViewModel>> GetMemberFinancialReportEng(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if (clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                  f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                                       f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth
                                                            })
                                                            .AsNoTracking()
                                                             .ToListAsync();
                    return financialList;
                }
                else if (clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 90)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                  f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                       ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                                       f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                       ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                       ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                       ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth
                                                            })
                                                            .AsNoTracking()
                                                             .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                   f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                   f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) ||
                                        f.MemberClientTrade.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId))&&
                                        ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId))
                                                                                   )
                                                             .Select(x => new MemberTradeFinancialViewModel
                                                             {
                                                                 MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                 OrganizationName = x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                 NetAssets = x.NetAssets,
                                                                 TotalDebts = x.TotalDebts,
                                                                 TotalFixedAsset = x.TotalFixedAsset,
                                                                 TotalIncome = x.TotalIncome,
                                                                 TotalLongTermDebts = x.TotalLongTermDebts,
                                                                 TotalPerShare = x.TotalPerShare,
                                                                 TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                 TotalWealth = x.TotalWealth
                                                             })
                                                             .AsNoTracking()
                                                              .ToListAsync();
                    return financialList;
                }
                    

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateMemberFinancial(MemberTradeFinancialDTO memberTradeFinancial)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var financialTradeInDb = await context.MemberTradeFinancial
                                            .Where(x => x.MemberTradeFinancialId == memberTradeFinancial.MemberTradeFinancialId)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
                    if (financialTradeInDb != null)
                    {
                        var updateFinancialTrade = mapper.Map(memberTradeFinancial, financialTradeInDb);
                        context.Entry(updateFinancialTrade).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return financialTradeInDb.MemberClientTradeId;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> DeleteMemberFinancialTrade(MemberTradeFinancialDTO memberTradeFinancial)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var financialTradeInDb = await context.MemberTradeFinancial
                                            .Where(x => x.MemberTradeFinancialId == memberTradeFinancial.MemberTradeFinancialId)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
                    if (financialTradeInDb != null)
                    {
                        var deleteFinancialTrade = mapper.Map(memberTradeFinancial, financialTradeInDb);
                        context.Entry(deleteFinancialTrade).State = EntityState.Deleted;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return financialTradeInDb.MemberClientTradeId;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public async Task<MemberTradeFinancial> GetMemberFinacialTradeById(int financialTradeId)
        {
            try
            {
                var financialTrade = await context.MemberTradeFinancial
                                     .Where(x => x.MemberTradeFinancialId == financialTradeId)
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();
                return financialTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<MemberTradeFinancial> GetMemberQuaterlyFinacialTradeById(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var financialTrade = await context.MemberTradeFinancial
                                     .Where(x => x.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                 x.MemberClientTrade.ReportTypeId == 2 &&
                                                 x.MemberClientTrade.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId &&
                                                 x.MemberClientTrade.Year == clientTradeQueryParameters.Year)
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();
                return financialTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<MemberTradeFinancial> GetMemberAnnuallyFinacialTradeById(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var financialTrade = await context.MemberTradeFinancial
                                     .Where(x => x.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId &&
                                                 x.MemberFinancialAuditor.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId &&
                                                 x.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year)
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync();
                return financialTrade;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberTradeFinancialViewModel>> GetListOfTradeFinancial(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            //int totalResultList = 0;
            try
            {
                if(clientTradeQueryParameters.MemeberCategoryId == 72 || clientTradeQueryParameters.MemeberCategoryId == 88)
                {
                    
                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                             f.MemberClientTrade.ReportTypeId == 2 &&
                                            ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                            ((clientTradeQueryParameters.MemeberCategoryId == 0) || (f.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.MemeberCategoryId)) &&
                                            f.NetAssets < clientTradeQueryParameters.FinancialPerformanceId &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberClientTrade.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberClientTrade.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 2 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;
                }
                else if(clientTradeQueryParameters.MemeberCategoryId == 90) {

                   
                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                             f.MemberClientTrade.ReportTypeId == 2 &&
                                            ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                           ((clientTradeQueryParameters.MemeberCategoryId == 0) || (f.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.MemeberCategoryId)) &&
                                           f.NetAssets < clientTradeQueryParameters.FinancialPerformanceId &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberClientTrade.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberClientTrade.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 2 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialDirectTrader = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 90).FirstOrDefault();
                    decimal directTraderFinance = (decimal)financialDirectTrader.Amount;
                    var finacialIntermidiater = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 72).FirstOrDefault();
                    decimal IntermidiaterFinancial = (decimal)(finacialIntermidiater.Amount);
                    var finacialTrader = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 88).FirstOrDefault();
                    decimal traderFinancial = (decimal)(finacialTrader.Amount);

                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                             f.MemberClientTrade.ReportTypeId == 2 &&
                                            ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                           ((f.MemberClientTrade.ExchangeActor.MemberCategoryId == 88 && f.NetAssets < traderFinancial) ||
                                            (f.MemberClientTrade.ExchangeActor.MemberCategoryId == 72 && f.NetAssets < IntermidiaterFinancial) ||
                                            (f.MemberClientTrade.ExchangeActor.CustomerTypeId == 90 && f.NetAssets < directTraderFinance)) &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberClientTrade.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberClientTrade.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 2 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;
                }
                

            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeFinancialViewModel>> GetQuartrlyFinancialReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if ((clientTradeQueryParameters.CustomerTypeId == 72) || (clientTradeQueryParameters.CustomerTypeId == 88))
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                   f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                   f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                        ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                        ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.MemeberCategoryId)) &&
                                        ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                        (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                        ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&
                                        ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                   )
                                                             .Select(x => new MemberTradeFinancialViewModel
                                                             {

                                                                 ExchangeActorId = x.MemberClientTrade.ExchangeActor.ExchangeActorId,
                                                                  OrganizationName = (clientTradeQueryParameters.Lang == "et")?x.MemberClientTrade.ExchangeActor.OrganizationNameAmh:
                                                                                                                              x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                 NetAssets = x.NetAssets,
                                                                 TotalDebts = x.TotalDebts,
                                                                 TotalFixedAsset = x.TotalFixedAsset,
                                                                 TotalIncome = x.TotalIncome,
                                                                 TotalLongTermDebts = x.TotalLongTermDebts,
                                                                 TotalPerShare = x.TotalPerShare,
                                                                 TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                 TotalWealth = x.TotalWealth,
                                                             })
                                                             .AsNoTracking()
                                                              .ToListAsync();
                    return financialList;
                }
                else if(clientTradeQueryParameters.CustomerTypeId == 90 || clientTradeQueryParameters.CustomerTypeId == 6)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                   f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                   f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                                                   ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                                                   ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                                                   (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                                                                   ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&

                                                                                   ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                   )
                                                             .Select(x => new MemberTradeFinancialViewModel
                                                             {

                                                                 ExchangeActorId = x.MemberClientTrade.ExchangeActor.ExchangeActorId,
                                                                 OrganizationName = (clientTradeQueryParameters.Lang == "et")?x.MemberClientTrade.ExchangeActor.OrganizationNameAmh:
                                                                                                                              x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                 NetAssets = x.NetAssets,
                                                                 TotalDebts = x.TotalDebts,
                                                                 TotalFixedAsset = x.TotalFixedAsset,
                                                                 TotalIncome = x.TotalIncome,
                                                                 TotalLongTermDebts = x.TotalLongTermDebts,
                                                                 TotalPerShare = x.TotalPerShare,
                                                                 TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                 TotalWealth = x.TotalWealth,
                                                             })
                                           .AsNoTracking()
                                           .ToListAsync();
                    return financialList;
                }
                else
                {

                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                   f => f.MemberClientTrade.ReportTypeId == 2 &&
                                                                                   f.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                                   ((clientTradeQueryParameters.ReportPeriodId == 0) || (f.MemberClientTrade.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId)) &&
                                                                                  // ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberClientTrade.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                                                   ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberClientTrade.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                                                   (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                                                                   ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&

                                                                                   ((clientTradeQueryParameters.Year == 0) || (f.MemberClientTrade.Year == clientTradeQueryParameters.Year))
                                                                                   )
                                                             .Select(x => new MemberTradeFinancialViewModel
                                                             {

                                                                 ExchangeActorId = x.MemberClientTrade.ExchangeActor.ExchangeActorId,
                                                                 OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberClientTrade.ExchangeActor.OrganizationNameAmh :
                                                                                                                              x.MemberClientTrade.ExchangeActor.OrganizationNameEng,
                                                                 NetAssets = x.NetAssets,
                                                                 TotalDebts = x.TotalDebts,
                                                                 TotalFixedAsset = x.TotalFixedAsset,
                                                                 TotalIncome = x.TotalIncome,
                                                                 TotalLongTermDebts = x.TotalLongTermDebts,
                                                                 TotalPerShare = x.TotalPerShare,
                                                                 TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                 TotalWealth = x.TotalWealth,
                                                             })
                                           .AsNoTracking()
                                           .ToListAsync();
                    return financialList;
                }
                
               

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<MemberTradeFinancialViewModel>> GetAnnualFinancialReportList(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if (clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                               f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                               ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                               ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                               ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                               (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                                               ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&
                                                               ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                               )
                                         .Select(x => new MemberTradeFinancialViewModel
                                         {

                                             ExchangeActorId = x.MemberFinancialAuditor.ExchangeActor.ExchangeActorId,
                                             OrganizationName = (clientTradeQueryParameters.Lang == "et")?x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh:
                                                                                                          x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                             NetAssets = x.NetAssets,
                                             TotalDebts = x.TotalDebts,
                                             TotalFixedAsset = x.TotalFixedAsset,
                                             TotalIncome = x.TotalIncome,
                                             TotalLongTermDebts = x.TotalLongTermDebts,
                                             TotalPerShare = x.TotalPerShare,
                                             TotalTemporaryDebts = x.TotalTemporaryDebts,
                                             TotalWealth = x.TotalWealth
                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                    return financialList;
                }
                else if (clientTradeQueryParameters.CustomerTypeId == 90 || clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.CustomerTypeId == 5 )
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                               f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                               ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                               ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                               ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                               (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                                               ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&
                                                               ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                               )
                                         .Select(x => new MemberTradeFinancialViewModel
                                         {

                                             ExchangeActorId = x.MemberFinancialAuditor.ExchangeActor.ExchangeActorId,
                                             OrganizationName = (clientTradeQueryParameters.Lang == "et")? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh:
                                                                                                           x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                             NetAssets = x.NetAssets,
                                             TotalDebts = x.TotalDebts,
                                             TotalFixedAsset = x.TotalFixedAsset,
                                             TotalIncome = x.TotalIncome,
                                             TotalLongTermDebts = x.TotalLongTermDebts,
                                             TotalPerShare = x.TotalPerShare,
                                             TotalTemporaryDebts = x.TotalTemporaryDebts,
                                             TotalWealth = x.TotalWealth
                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                               f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                               ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                              // ((clientTradeQueryParameters.CustomerTypeId == 0) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.CustomerTypeId)) &&
                                                               ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                               (((clientTradeQueryParameters.PriceFrom == 0) || (f.NetAssets >= clientTradeQueryParameters.PriceFrom)) &&
                                                               ((clientTradeQueryParameters.PriceTo == 0) || (f.NetAssets <= clientTradeQueryParameters.PriceTo))) &&
                                                               ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                               )
                                         .Select(x => new MemberTradeFinancialViewModel
                                         {

                                             ExchangeActorId = x.MemberFinancialAuditor.ExchangeActor.ExchangeActorId,
                                             OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                           x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                             NetAssets = x.NetAssets,
                                             TotalDebts = x.TotalDebts,
                                             TotalFixedAsset = x.TotalFixedAsset,
                                             TotalIncome = x.TotalIncome,
                                             TotalLongTermDebts = x.TotalLongTermDebts,
                                             TotalPerShare = x.TotalPerShare,
                                             TotalTemporaryDebts = x.TotalTemporaryDebts,
                                             TotalWealth = x.TotalWealth
                                         })
                                         .AsNoTracking()
                                          .ToListAsync();
                    return financialList;
                }
      

                    

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> UpdateLowFinancialBalanceStatus(MemberTradeFinancial memberTrade)
        {
            try
            {
                var reportedExchangeActor = await context.MemberTradeFinancial.Where(
                                       x => x.MemberClientTradeId == memberTrade.MemberClientTradeId)
                                      .AsNoTracking()
                                      .ToListAsync();
                foreach (var rec in reportedExchangeActor)
                {
                    rec.InJectedStatus = 1;
                    context.Entry(rec).State = EntityState.Modified;
                }
                await context.SaveChangesAsync();
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeFinancialViewModel>> GetMemberAnnualFinancialReport(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.MemeberCategoryId == 72 || clientTradeQueryParameters.MemeberCategoryId == 88)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                                  ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                                                                  ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                                                  ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                                                  ((clientTradeQueryParameters.MemeberCategoryId == 0) || (f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.MemeberCategoryId)) &&
                                                                                   ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                                             x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth,
                                                                ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                                                MemberFinancialAuditorId = x.MemberFinancialAuditorId
                                                            })
                                                            .AsNoTracking()
                                                            .ToListAsync();
                    return financialList;

                }
                else if (clientTradeQueryParameters.CustomerTypeId == 6 || clientTradeQueryParameters.MemeberCategoryId == 90 || clientTradeQueryParameters.MemeberCategoryId == 5)
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                                                  f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                                  ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                                                                  ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                                                  ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                                                  ((clientTradeQueryParameters.MemeberCategoryId == 0) || (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.MemeberCategoryId)) &&
                                                                                   ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                                                  )
                                                            .Select(x => new MemberTradeFinancialViewModel
                                                            {
                                                                MemberTradeFinancialId = x.MemberTradeFinancialId,
                                                                OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                                             x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                                                NetAssets = x.NetAssets,
                                                                TotalDebts = x.TotalDebts,
                                                                TotalFixedAsset = x.TotalFixedAsset,
                                                                TotalIncome = x.TotalIncome,
                                                                TotalLongTermDebts = x.TotalLongTermDebts,
                                                                TotalPerShare = x.TotalPerShare,
                                                                TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                TotalWealth = x.TotalWealth,
                                                                ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                                                MemberFinancialAuditorId = x.MemberFinancialAuditorId
                                                            })
                                                            .AsNoTracking()
                                                            .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialList = await context.MemberTradeFinancial.Where(
                                                              f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                              ((string.IsNullOrEmpty(clientTradeQueryParameters.ECXCode)) || f.MemberFinancialAuditor.ExchangeActor.Ecxcode.Contains(clientTradeQueryParameters.ECXCode)) &&
                                                              ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                                              ((clientTradeQueryParameters.FieldBusinessId == 0) || (f.MemberFinancialAuditor.ExchangeActor.BuisnessFiledId == clientTradeQueryParameters.FieldBusinessId)) &&
                                                               ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                              )
                                        .Select(x => new MemberTradeFinancialViewModel
                                        {
                                            MemberTradeFinancialId = x.MemberTradeFinancialId,
                                            OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                         x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                            NetAssets = x.NetAssets,
                                            TotalDebts = x.TotalDebts,
                                            TotalFixedAsset = x.TotalFixedAsset,
                                            TotalIncome = x.TotalIncome,
                                            TotalLongTermDebts = x.TotalLongTermDebts,
                                            TotalPerShare = x.TotalPerShare,
                                            TotalTemporaryDebts = x.TotalTemporaryDebts,
                                            TotalWealth = x.TotalWealth,
                                            ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                            MemberFinancialAuditorId = x.MemberFinancialAuditorId
                                        })
                                        .AsNoTracking()
                                        .ToListAsync();
                    return financialList;
                }
                   

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<MemberTradeFinancialViewModel>> GetAnnualFinancialPerformanceReport(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                if(clientTradeQueryParameters.CustomerTypeId == 72 || clientTradeQueryParameters.CustomerTypeId == 88)
                {
                    
                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                            ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || 
                                            (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                             (clientTradeQueryParameters.CustomerTypeId == 0 || 
                                             f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == clientTradeQueryParameters.CustomerTypeId) && 
                                            (f.NetAssets < clientTradeQueryParameters.FinancialPerformanceId) &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 4 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;

                }
                else if (clientTradeQueryParameters.CustomerTypeId == 90)
                {
                   
                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                            ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) ||
                                            (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                           (clientTradeQueryParameters.CustomerTypeId == 0 || 
                                           f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == clientTradeQueryParameters.FinancialPerformanceId) &&
                                           (f.NetAssets < clientTradeQueryParameters.FinancialPerformanceId) &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 4 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;
                }
                else
                {
                    var financialDirectTrader = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 90).FirstOrDefault();
                    decimal directTraderFinance = (decimal)financialDirectTrader.Amount;
                    var finacialIntermidiater = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 72).FirstOrDefault();
                    decimal IntermidiaterFinancial = (decimal)(finacialIntermidiater.Amount);
                    var finacialTrader = context.ExchangeActorFinanicial.Where(x => x.CustomerTypeId == 88).FirstOrDefault();
                    decimal traderFinancial = (decimal)(finacialTrader.Amount);

                    var financialList = await context.MemberTradeFinancial.Where(
                                             f => f.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                            ((clientTradeQueryParameters.AnnualBudgetCloserId == 0) || (f.MemberFinancialAuditor.AnnualBudgetCloserId == clientTradeQueryParameters.AnnualBudgetCloserId)) &&
                                           ((f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == 88 && f.NetAssets < traderFinancial) ||
                                            (f.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == 72 && f.NetAssets < IntermidiaterFinancial) ||
                                            (f.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == 90 && f.NetAssets < directTraderFinance)) &&
                                            ((clientTradeQueryParameters.Year == 0) || (f.MemberFinancialAuditor.Year == clientTradeQueryParameters.Year))
                                                                                       )
                                                                 .Select(x => new MemberTradeFinancialViewModel
                                                                 {

                                                                     ExchangeActorId = x.MemberFinancialAuditor.ExchangeActorId,
                                                                     OrganizationName = (clientTradeQueryParameters.Lang == "et") ? x.MemberFinancialAuditor.ExchangeActor.OrganizationNameAmh :
                                                                                                                                  x.MemberFinancialAuditor.ExchangeActor.OrganizationNameEng,
                                                                     NetAssets = x.NetAssets,
                                                                     TotalDebts = x.TotalDebts,
                                                                     TotalFixedAsset = x.TotalFixedAsset,
                                                                     TotalIncome = x.TotalIncome,
                                                                     TotalLongTermDebts = x.TotalLongTermDebts,
                                                                     TotalPerShare = x.TotalPerShare,
                                                                     TotalTemporaryDebts = x.TotalTemporaryDebts,
                                                                     TotalWealth = x.TotalWealth,
                                                                     TotalNoMembersVioaltion = context.TradeExcutionViolationRecord.Where(n => n.Year == clientTradeQueryParameters.Year &&
                                                                                                                                      n.ReportTypeId == 4 && n.ReportAnalysisId == 135 &&
                                                                                                                                      n.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                                                                      n.MemberTradeViolation.ExchangeActorId == x.MemberClientTrade.ExchangeActorId).Count()
                                                                 })

                                                                 .AsNoTracking()
                                                  .ToListAsync();
                    return financialList;
                } 
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<ExchangeActorFinacialComparative> GetQuarterlyFinancialComparativeList(FinancialReportSearchQuery financialReport)
        {
            try
            {
                List<ExchangeActorFinacialComparative> comparisonOffSites = new List<ExchangeActorFinacialComparative>();
                ExchangeActorFinacialComparative exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                if (financialReport.CustomerTypeId==72 || financialReport.CustomerTypeId == 88)
                {
                    var exchangeFinancialReport = context.MemberTradeFinancial.Include(y => y.MemberClientTrade).ThenInclude(z => z.ExchangeActor)
                                                                .Where(x => x.MemberClientTrade.ReportTypeId == 2 && x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                 (financialReport.CustomerTypeId == 0 || x.MemberClientTrade.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                 (financialReport.ReportPeriodId == 0 || x.MemberClientTrade.ReportPeriodId == financialReport.ReportPeriodId))
                                                                 .AsNoTracking()
                                                                 .ToList();
                    var totalWealth = exchangeFinancialReport.Sum(w => w.TotalWealth);
                    if(totalWealth == 0)
                    {
                        totalWealth = 1;
                    }
                    var totalDebet = exchangeFinancialReport.Sum(w => w.TotalDebts);
                    if(totalDebet == 0)
                    {
                        totalDebet = 1;
                    }
                    var netWorth = exchangeFinancialReport.Sum(w => w.NetAssets);
                    if (netWorth == 0)
                    {
                        netWorth = 1;
                    }
                 var fincompOffSiteMonitor = exchangeFinancialReport.Select(x => new { x.MemberClientTrade.Year,x.MemberClientTrade.ExchangeActor,x.MemberClientTrade.ExchangeActorId }).Distinct();
                foreach (var name in fincompOffSiteMonitor)
                 {
                        exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                        exchangeActorFinancialReport.Year = name.Year;
                        exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        exchangeActorFinancialReport.TotalWealth = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(y => y.TotalWealth);
                        exchangeActorFinancialReport.TotalDebts = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(x => x.TotalDebts);
                        exchangeActorFinancialReport.NetWorth = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(x => x.NetAssets);
                        exchangeActorFinancialReport.TotalWealthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeFinancialReport.Sum(y => y.TotalWealth) * 100) / totalWealth));
                        exchangeActorFinancialReport.TotalDebtsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeFinancialReport.Sum(x => x.TotalDebts) * 100) / totalDebet));
                        exchangeActorFinancialReport.NetWorthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeFinancialReport.Sum(x => x.NetAssets) * 100) / netWorth));
                        comparisonOffSites.Add(exchangeActorFinancialReport);
                 }
                    exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                    //exchangeActorFinancialReport.Year = com
                    exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeActorFinancialReport.TotalWealth = comparisonOffSites.Sum(x => x.TotalWealth);
                    exchangeActorFinancialReport.TotalDebts = comparisonOffSites.Sum(x => x.TotalDebts);
                    exchangeActorFinancialReport.NetWorth = comparisonOffSites.Sum(x => x.NetWorth);
                    exchangeActorFinancialReport.TotalWealthInPercent = comparisonOffSites.Sum(x => x.TotalWealthInPercent);
                    exchangeActorFinancialReport.TotalDebtsInPercent = comparisonOffSites.Sum(x => x.TotalDebtsInPercent);
                    exchangeActorFinancialReport.NetWorthInPercent = comparisonOffSites.Sum(x => x.NetWorthInPercent);
                    comparisonOffSites.Add(exchangeActorFinancialReport);
                    return comparisonOffSites;
                }
                else
                {                  
                  
                        var exchangeFinancialReport = context.MemberTradeFinancial.Include(y => y.MemberClientTrade).ThenInclude(z => z.ExchangeActor)
                                                            .Where(x => x.MemberClientTrade.ReportTypeId == 2 && x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                     (financialReport.CustomerTypeId == 0 || x.MemberClientTrade.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                     (financialReport.ReportPeriodId == 0 || x.MemberClientTrade.ReportPeriodId == financialReport.ReportPeriodId))
                                                                     .AsNoTracking()
                                                                     .ToList();
                        var totalWealth = exchangeFinancialReport.Sum(w => w.TotalWealth);
                        if (totalWealth == 0)
                        {
                            totalWealth = 1;
                        }
                        var totalDebet = exchangeFinancialReport.Sum(w => w.TotalDebts);
                        if (totalDebet == 0)
                        {
                            totalDebet = 1;
                        }
                        var netWorth = exchangeFinancialReport.Sum(w => w.NetAssets);
                        if (netWorth == 0)
                        {
                            netWorth = 1;
                        }
                        var fincompOffSiteMonitor = exchangeFinancialReport.Select(x => new { x.MemberClientTrade.Year, x.MemberClientTrade.ExchangeActor,x.MemberClientTrade.ExchangeActorId }).Distinct();
                        foreach (var name in fincompOffSiteMonitor)
                        {
                            exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                            exchangeActorFinancialReport.Year = name.Year;
                            exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                            exchangeActorFinancialReport.TotalWealth = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(y => y.TotalWealth);
                            exchangeActorFinancialReport.TotalDebts = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(x => x.TotalDebts);
                            exchangeActorFinancialReport.NetWorth = exchangeFinancialReport.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberClientTrade.Year == name.Year).Sum(x => x.NetAssets);
                            exchangeActorFinancialReport.TotalWealthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalWealth *100) / totalWealth));
                            exchangeActorFinancialReport.TotalDebtsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalDebts * 100) / totalDebet));
                            exchangeActorFinancialReport.NetWorthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.NetWorth *100) / netWorth));
                            comparisonOffSites.Add(exchangeActorFinancialReport);
                        }
                        exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                        exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                        exchangeActorFinancialReport.TotalWealth = comparisonOffSites.Sum(x => x.TotalWealth);
                        exchangeActorFinancialReport.TotalDebts = comparisonOffSites.Sum(x => x.TotalDebts);
                        exchangeActorFinancialReport.NetWorth = comparisonOffSites.Sum(x => x.NetWorth);
                        exchangeActorFinancialReport.TotalWealthInPercent = comparisonOffSites.Sum(x => x.TotalWealthInPercent);
                        exchangeActorFinancialReport.TotalDebtsInPercent = comparisonOffSites.Sum(x => x.TotalDebtsInPercent);
                        exchangeActorFinancialReport.NetWorthInPercent = comparisonOffSites.Sum(x => x.NetWorthInPercent);
                        comparisonOffSites.Add(exchangeActorFinancialReport);
                        
                        return comparisonOffSites;
                    }
                    
                }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<ExchangeActorFinacialComparative> GetAnnualyFinancialComparativeList(FinancialReportSearchQuery financialReport)
        {
            try
            {
                List<ExchangeActorFinacialComparative> comparisonOffSites = new List<ExchangeActorFinacialComparative>();
                ExchangeActorFinacialComparative exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                if (financialReport.CustomerTypeId == 72 || financialReport.CustomerTypeId == 88)
                {
                    var exchangeFinancialReport = context.MemberTradeFinancial.Include(y => y.MemberFinancialAuditor).ThenInclude(z => z.ExchangeActor)
                                                        .Where(x => x.MemberFinancialAuditor.ReportTypeId == 2 && x.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                 (financialReport.CustomerTypeId == 0 || x.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                 (financialReport.ReportPeriodId == 0 || x.MemberFinancialAuditor.AnnualBudgetCloserId == financialReport.ReportPeriodId))
                                                                 .AsNoTracking()
                                                                 .ToList();
                    var totalWealth = exchangeFinancialReport.Sum(w => w.TotalWealth);
                    if (totalWealth == 0)
                    {
                        totalWealth = 1;
                    }
                    var totalDebet = exchangeFinancialReport.Sum(w => w.TotalDebts);
                    if (totalDebet == 0)
                    {
                        totalDebet = 1;
                    }
                    var netWorth = exchangeFinancialReport.Sum(w => w.NetAssets);
                    if (netWorth == 0)
                    {
                        netWorth = 1;
                    }
                    var fincompOffSiteMonitor = exchangeFinancialReport.Select(x => new { x.MemberFinancialAuditor.Year, x.MemberFinancialAuditor.ExchangeActor, x.MemberFinancialAuditor.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                        exchangeActorFinancialReport.Year = name.Year;
                        exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        exchangeActorFinancialReport.TotalWealth = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId && 
                                                                                           x.MemberFinancialAuditor.Year==name.Year).Sum(y => y.TotalWealth);
                        exchangeActorFinancialReport.TotalDebts = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberFinancialAuditor.Year == name.Year).Sum(x => x.TotalDebts);
                        exchangeActorFinancialReport.NetWorth = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberFinancialAuditor.Year == name.Year).Sum(x => x.NetAssets);
                        exchangeActorFinancialReport.TotalWealthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalWealth *100) / totalWealth));
                        exchangeActorFinancialReport.TotalDebtsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalDebts * 100) / totalDebet));
                        exchangeActorFinancialReport.NetWorthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.NetWorth * 100) / netWorth));
                        comparisonOffSites.Add(exchangeActorFinancialReport);
                    }
                    exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                    exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeActorFinancialReport.TotalWealth = comparisonOffSites.Sum(x => x.TotalWealth);
                    exchangeActorFinancialReport.TotalDebts = comparisonOffSites.Sum(x => x.TotalDebts);
                    exchangeActorFinancialReport.NetWorth = comparisonOffSites.Sum(x => x.NetWorth);
                    exchangeActorFinancialReport.TotalWealthInPercent = comparisonOffSites.Sum(x => x.TotalWealthInPercent);
                    exchangeActorFinancialReport.TotalDebtsInPercent = comparisonOffSites.Sum(x => x.TotalDebtsInPercent);
                    exchangeActorFinancialReport.NetWorthInPercent = comparisonOffSites.Sum(x => x.NetWorthInPercent);
                    comparisonOffSites.Add(exchangeActorFinancialReport);
                    return comparisonOffSites;
                }
                else
                {

                    var exchangeFinancialReport = context.MemberTradeFinancial.Include(y => y.MemberFinancialAuditor).ThenInclude(z => z.ExchangeActor)
                                                        .Where(x => x.MemberFinancialAuditor.ReportTypeId == 2 && x.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                 (financialReport.CustomerTypeId == 0 || x.MemberFinancialAuditor.ExchangeActor.CustomerTypeId == financialReport.CustomerTypeId) &&
                                                                 (financialReport.ReportPeriodId == 0 || x.MemberFinancialAuditor.AnnualBudgetCloserId == financialReport.ReportPeriodId))
                                                                 .AsNoTracking()
                                                                 .ToList();
                    var totalWealth = exchangeFinancialReport.Sum(w => w.TotalWealth);
                    if (totalWealth == 0)
                    {
                        totalWealth = 1;
                    }
                    var totalDebet = exchangeFinancialReport.Sum(w => w.TotalDebts);
                    if (totalDebet == 0)
                    {
                        totalDebet = 1;
                    }
                    var netWorth = exchangeFinancialReport.Sum(w => w.NetAssets);
                    if (netWorth == 0)
                    {
                        netWorth = 1;
                    }
                    var fincompOffSiteMonitor = exchangeFinancialReport.Select(x => new { x.MemberFinancialAuditor.Year, x.MemberFinancialAuditor.ExchangeActor,x.MemberFinancialAuditor.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                        exchangeActorFinancialReport.Year = name.Year;
                        exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        exchangeActorFinancialReport.TotalWealth = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberFinancialAuditor.Year == name.Year).Sum(y => y.TotalWealth);
                        exchangeActorFinancialReport.TotalDebts = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberFinancialAuditor.Year == name.Year).Sum(x => x.TotalDebts);
                        exchangeActorFinancialReport.NetWorth = exchangeFinancialReport.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId &&
                                                                                           x.MemberFinancialAuditor.Year == name.Year).Sum(x => x.NetAssets);
                        exchangeActorFinancialReport.TotalWealthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalWealth *100) / totalWealth));
                        exchangeActorFinancialReport.TotalDebtsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.TotalDebts *100) / totalDebet));
                        exchangeActorFinancialReport.NetWorthInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (exchangeActorFinancialReport.NetWorth *100) / netWorth));
                        comparisonOffSites.Add(exchangeActorFinancialReport);
                    }
                    exchangeActorFinancialReport = new ExchangeActorFinacialComparative();
                    exchangeActorFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeActorFinancialReport.TotalWealth = comparisonOffSites.Sum(x => x.TotalWealth);
                    exchangeActorFinancialReport.TotalDebts = comparisonOffSites.Sum(x => x.TotalDebts);
                    exchangeActorFinancialReport.NetWorth = comparisonOffSites.Sum(x => x.NetWorth);
                    exchangeActorFinancialReport.TotalWealthInPercent = comparisonOffSites.Sum(x => x.TotalWealthInPercent);
                    exchangeActorFinancialReport.TotalDebtsInPercent = comparisonOffSites.Sum(x => x.TotalDebtsInPercent);
                    exchangeActorFinancialReport.NetWorthInPercent = comparisonOffSites.Sum(x => x.NetWorthInPercent);
                    comparisonOffSites.Add(exchangeActorFinancialReport);

                    return comparisonOffSites;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<FinancialNetWorthCalculation> GetQuarterFinancialNetWorthCalculationList(FinancialReportSearchQuery financialReport)
        {
            try
            {
                List<FinancialNetWorthCalculation> financialReportList = new List<FinancialNetWorthCalculation>();
                FinancialNetWorthCalculation exchangeFinancialReport = new FinancialNetWorthCalculation();
                if (financialReport.CustomerTypeId == 72 || financialReport.CustomerTypeId == 88)
                {
                    var quarterFinancial = context.MemberTradeFinancial.Include(y => y.MemberClientTrade).ThenInclude(w => w.ExchangeActor)
                                                                      .Where(x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                                x.MemberClientTrade.ReportTypeId == 2 &&
                                                                                (financialReport.Year == 0 || x.MemberClientTrade.Year == financialReport.Year) &&
                                                                                (financialReport.CustomerTypeId == 0 || x.MemberClientTrade.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                                (financialReport.ReportPeriodId == 0 || x.MemberClientTrade.ReportPeriodId == financialReport.ReportPeriodId))
                                                                                .AsNoTracking()
                                                                                .ToList();
                    var fincompOffSiteMonitor = quarterFinancial.Select(x => new { x.MemberClientTrade.ExchangeActor,x.MemberClientTrade.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeFinancialReport = new FinancialNetWorthCalculation();
                        exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        var netWorth = quarterFinancial.Where(x=>x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(y => y.TotalWealth);
                        var dep = quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(y => y.DepositsMoney);
                        exchangeFinancialReport.DepositsMoneyInPercent = Convert.ToDecimal(String.Format("{0:00.00}", (quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(y => y.DepositsMoney) * 100)/netWorth));
                        exchangeFinancialReport.CollectedPaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.CollectedPayment) * 100 / netWorth));
                        exchangeFinancialReport.StockInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Stock) * 100 / netWorth));
                        exchangeFinancialReport.AdvancePaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x=> x.AdvancePayment) * 100 / netWorth));
                        exchangeFinancialReport.PerShareInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x=>x.PerShare) * 100 / netWorth));
                        exchangeFinancialReport.BuildingInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x=>x.Building) * 100 / netWorth));
                        exchangeFinancialReport.VehicleInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Vehicle) * 100 / netWorth));
                        exchangeFinancialReport.ToolsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Tools) * 100 / netWorth));
                        exchangeFinancialReport.OfficeFurnitureInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.OfficeFurniture) * 100 / netWorth));
                        exchangeFinancialReport.ComputerandaccessoriesInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Computerandaccessories) * 100 / netWorth));
                        financialReportList.Add(exchangeFinancialReport);
                    }
                    exchangeFinancialReport = new FinancialNetWorthCalculation();
                    exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeFinancialReport.DepositsMoneyInPercent = financialReportList.Sum(x => x.DepositsMoneyInPercent);
                    exchangeFinancialReport.CollectedPaymentInPercent = financialReportList.Sum(x => x.CollectedPaymentInPercent);
                    exchangeFinancialReport.StockInPercent = financialReportList.Sum(x => x.StockInPercent);
                    exchangeFinancialReport.AdvancePaymentInPercent = financialReportList.Sum(x => x.AdvancePaymentInPercent);
                    exchangeFinancialReport.PerShareInPercent = financialReportList.Sum(x => x.PerShareInPercent);
                    exchangeFinancialReport.BuildingInPercent = financialReportList.Sum(x => x.BuildingInPercent);
                    exchangeFinancialReport.VehicleInPercent = financialReportList.Sum(x => x.VehicleInPercent);
                    exchangeFinancialReport.ToolsInPercent = financialReportList.Sum(x => x.ToolsInPercent);
                    exchangeFinancialReport.OfficeFurnitureInPercent = financialReportList.Sum(x => x.OfficeFurnitureInPercent);
                    exchangeFinancialReport.ComputerandaccessoriesInPercent = financialReportList.Sum(x => x.ComputerandaccessoriesInPercent);
                    financialReportList.Add(exchangeFinancialReport);

                    return financialReportList;
                }
                else
                {
                    var quarterFinancial = context.MemberTradeFinancial.Include(y => y.MemberClientTrade).ThenInclude(w => w.ExchangeActor)
                                                                                         .Where(x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                                                   x.MemberClientTrade.ReportTypeId == 2 &&
                                                                                                   (financialReport.Year == 0 || x.MemberClientTrade.Year == financialReport.Year) &&
                                                                                                   (financialReport.CustomerTypeId == 0 || x.MemberClientTrade.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                                                   (financialReport.ReportPeriodId == 0 || x.MemberClientTrade.ReportPeriodId == financialReport.ReportPeriodId))
                                                                                                   .AsNoTracking()
                                                                                                   .ToList();
                                          
                  
                    var fincompOffSiteMonitor = quarterFinancial.Select(x => new { x.MemberClientTrade.ExchangeActor,x.MemberClientTrade.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeFinancialReport = new FinancialNetWorthCalculation();                        
                        exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        var netWorth = quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(y => y.TotalWealth);
                        exchangeFinancialReport.DepositsMoneyInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(y => y.DepositsMoney) * 100 / netWorth));
                        exchangeFinancialReport.CollectedPaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.CollectedPayment) * 100 / netWorth));
                        exchangeFinancialReport.StockInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Stock) *100/ netWorth));
                        exchangeFinancialReport.AdvancePaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.AdvancePayment)*100 / netWorth));
                        exchangeFinancialReport.PerShareInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.PerShare)*100 / netWorth));
                        exchangeFinancialReport.BuildingInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Building)*100 / netWorth));
                        exchangeFinancialReport.VehicleInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Vehicle)*100 / netWorth));
                        exchangeFinancialReport.ToolsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Tools)*100 / netWorth));
                        exchangeFinancialReport.OfficeFurnitureInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.OfficeFurniture)*100 / netWorth));
                        exchangeFinancialReport.ComputerandaccessoriesInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberClientTrade.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Computerandaccessories) *100 / netWorth));
                        financialReportList.Add(exchangeFinancialReport);
                    }
                    exchangeFinancialReport = new FinancialNetWorthCalculation();
                    exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeFinancialReport.DepositsMoneyInPercent = financialReportList.Sum(x => x.DepositsMoneyInPercent);
                    exchangeFinancialReport.CollectedPaymentInPercent = financialReportList.Sum(x => x.CollectedPaymentInPercent);
                    exchangeFinancialReport.StockInPercent = financialReportList.Sum(x => x.StockInPercent);
                    exchangeFinancialReport.AdvancePaymentInPercent = financialReportList.Sum(x => x.AdvancePaymentInPercent);
                    exchangeFinancialReport.PerShareInPercent = financialReportList.Sum(x => x.PerShareInPercent);
                    exchangeFinancialReport.BuildingInPercent = financialReportList.Sum(x => x.BuildingInPercent);
                    exchangeFinancialReport.VehicleInPercent = financialReportList.Sum(x => x.VehicleInPercent);
                    exchangeFinancialReport.ToolsInPercent = financialReportList.Sum(x => x.ToolsInPercent);
                    exchangeFinancialReport.OfficeFurnitureInPercent = financialReportList.Sum(x => x.OfficeFurnitureInPercent);
                    exchangeFinancialReport.ComputerandaccessoriesInPercent = financialReportList.Sum(x => x.ComputerandaccessoriesInPercent);
                    financialReportList.Add(exchangeFinancialReport);

                    return financialReportList;                                                                          
                }
             
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
         }

        public List<FinancialNetWorthCalculation> GetAnnualFinancialNetWorthCalculationList(FinancialReportSearchQuery financialReport)
        {
            try
            {
                List<FinancialNetWorthCalculation> financialReportList = new List<FinancialNetWorthCalculation>();
                FinancialNetWorthCalculation exchangeFinancialReport = new FinancialNetWorthCalculation();
                if (financialReport.CustomerTypeId == 72 || financialReport.CustomerTypeId == 88)
                {
                    var quarterFinancial = context.MemberTradeFinancial.Include(y => y.MemberFinancialAuditor).ThenInclude(w => w.ExchangeActor)
                                                                      .Where(x => x.MemberFinancialAuditor.TradeExcutionStatusTypeId == 2 &&
                                                                                x.MemberFinancialAuditor.ReportTypeId == 2 &&
                                                                                (financialReport.Year == 0 || x.MemberFinancialAuditor.Year == financialReport.Year) &&
                                                                                (financialReport.CustomerTypeId == 0 || x.MemberFinancialAuditor.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                                (financialReport.ReportPeriodId == 0 || x.MemberFinancialAuditor.AnnualBudgetCloserId == financialReport.ReportPeriodId))
                                                                                .AsNoTracking()
                                                                                .ToList();
                    //var deposityMoney = quarterFinancial.Sum(x => x.DepositsMoney);
                    //if (deposityMoney == 0)
                    //{
                    //    deposityMoney = 1;
                    //}
                    //var collectPay = quarterFinancial.Sum(x => x.CollectedPayment);
                    //if (collectPay == 0)
                    //{
                    //    collectPay = 1;
                    //}
                    //var stock = quarterFinancial.Sum(x => x.Stock);
                    //if (stock == 0)
                    //{
                    //    stock = 1;
                    //}
                    //var advancePayment = quarterFinancial.Sum(x => x.AdvancePayment);
                    //if (advancePayment == 0)
                    //{
                    //    advancePayment = 1;
                    //}
                    //var perShare = quarterFinancial.Sum(x => x.PerShare);
                    //if (perShare == 0)
                    //{
                    //    perShare = 1;
                    //}
                    //var building = quarterFinancial.Sum(x => x.Building);
                    //if (building == 0)
                    //{
                    //    building = 1;
                    //}
                    //var vehicle = quarterFinancial.Sum(x => x.Vehicle);
                    //if (vehicle == 0)
                    //{
                    //    vehicle = 1;
                    //}
                    //var tools = quarterFinancial.Sum(x => x.Tools);
                    //if (tools == 0)
                    //{
                    //    tools = 1;
                    //}
                    //var officeFurniture = quarterFinancial.Sum(x => x.OfficeFurniture);
                    //if (officeFurniture == 0)
                    //{
                    //    officeFurniture = 1;
                    //}
                    //var computerandaccessories = quarterFinancial.Sum(x => x.Computerandaccessories);
                    //if (computerandaccessories == 0)
                    //{
                    //    computerandaccessories = 1;
                    //}
                    var fincompOffSiteMonitor = quarterFinancial.Select(x => new { x.MemberFinancialAuditor.ExchangeActor,x.MemberFinancialAuditor.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeFinancialReport = new FinancialNetWorthCalculation();
                        exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        var netWorth = quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(y => y.TotalWealth);
                        exchangeFinancialReport.DepositsMoneyInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(y => y.DepositsMoney)*100/ netWorth));
                        exchangeFinancialReport.CollectedPaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.CollectedPayment) * 100 / netWorth));
                        exchangeFinancialReport.StockInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Stock) * 100 / netWorth));
                        exchangeFinancialReport.AdvancePaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.AdvancePayment) * 100 / netWorth));
                        exchangeFinancialReport.PerShareInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.PerShare) * 100 / netWorth));
                        exchangeFinancialReport.BuildingInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Building) * 100 / netWorth));
                        exchangeFinancialReport.VehicleInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Vehicle) * 100 / netWorth));
                        exchangeFinancialReport.ToolsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Tools) * 100 / netWorth));
                        exchangeFinancialReport.OfficeFurnitureInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.OfficeFurniture) * 100 / netWorth));
                        exchangeFinancialReport.ComputerandaccessoriesInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Computerandaccessories) * 100 / netWorth));
                        financialReportList.Add(exchangeFinancialReport);
                    }
                    exchangeFinancialReport = new FinancialNetWorthCalculation();
                    exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeFinancialReport.DepositsMoneyInPercent = financialReportList.Sum(x => x.DepositsMoneyInPercent);
                    exchangeFinancialReport.CollectedPaymentInPercent = financialReportList.Sum(x => x.CollectedPaymentInPercent);
                    exchangeFinancialReport.StockInPercent = financialReportList.Sum(x => x.StockInPercent);
                    exchangeFinancialReport.AdvancePaymentInPercent = financialReportList.Sum(x => x.AdvancePaymentInPercent);
                    exchangeFinancialReport.PerShareInPercent = financialReportList.Sum(x => x.PerShareInPercent);
                    exchangeFinancialReport.BuildingInPercent = financialReportList.Sum(x => x.BuildingInPercent);
                    exchangeFinancialReport.VehicleInPercent = financialReportList.Sum(x => x.VehicleInPercent);
                    exchangeFinancialReport.ToolsInPercent = financialReportList.Sum(x => x.ToolsInPercent);
                    exchangeFinancialReport.OfficeFurnitureInPercent = financialReportList.Sum(x => x.OfficeFurnitureInPercent);
                    exchangeFinancialReport.ComputerandaccessoriesInPercent = financialReportList.Sum(x => x.ComputerandaccessoriesInPercent);
                    financialReportList.Add(exchangeFinancialReport);

                    return financialReportList;
                }
                else
                {
                    var quarterFinancial = context.MemberTradeFinancial.Include(y => y.MemberClientTrade).ThenInclude(w => w.ExchangeActor)
                                                                                         .Where(x => x.MemberClientTrade.TradeExcutionStatusTypeId == 2 &&
                                                                                                   x.MemberClientTrade.ReportTypeId == 2 &&
                                                                                                   (financialReport.Year == 0 || x.MemberClientTrade.Year == financialReport.Year) &&
                                                                                                   (financialReport.CustomerTypeId == 0 || x.MemberClientTrade.ExchangeActor.MemberCategoryId == financialReport.CustomerTypeId) &&
                                                                                                   (financialReport.ReportPeriodId == 0 || x.MemberClientTrade.ReportPeriodId == financialReport.ReportPeriodId))
                                                                                                   .AsNoTracking()
                                                                                                   .ToList();

                  
                    var fincompOffSiteMonitor = quarterFinancial.Select(x => new { x.MemberFinancialAuditor.ExchangeActor,x.MemberFinancialAuditor.ExchangeActorId }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        exchangeFinancialReport = new FinancialNetWorthCalculation();
                        exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? name.ExchangeActor.OrganizationNameAmh : name.ExchangeActor.OrganizationNameEng;
                        var netWorth = quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(y => y.TotalWealth);
                        exchangeFinancialReport.DepositsMoneyInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(y => y.DepositsMoney) *100 / netWorth));
                        exchangeFinancialReport.CollectedPaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.CollectedPayment) * 100 / netWorth));
                        exchangeFinancialReport.StockInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Stock) * 100 / netWorth));
                        exchangeFinancialReport.AdvancePaymentInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.AdvancePayment) * 100 / netWorth));
                        exchangeFinancialReport.PerShareInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.PerShare) * 100 / netWorth));
                        exchangeFinancialReport.BuildingInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Building) * 100 / netWorth));
                        exchangeFinancialReport.VehicleInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Vehicle) * 100 / netWorth));
                        exchangeFinancialReport.ToolsInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Tools) * 100 / netWorth));
                        exchangeFinancialReport.OfficeFurnitureInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.OfficeFurniture) * 100 / netWorth));
                        exchangeFinancialReport.ComputerandaccessoriesInPercent = Convert.ToDecimal(String.Format("{0:00.00}", quarterFinancial.Where(x => x.MemberFinancialAuditor.ExchangeActorId == name.ExchangeActorId).Sum(x => x.Computerandaccessories) * 100 / netWorth));
                        financialReportList.Add(exchangeFinancialReport);
                    }
                    exchangeFinancialReport = new FinancialNetWorthCalculation();
                    exchangeFinancialReport.OrganizationName = (financialReport.Lang == "et") ? "ድምር" : "Total";
                    exchangeFinancialReport.DepositsMoneyInPercent = financialReportList.Sum(x => x.DepositsMoneyInPercent);
                    exchangeFinancialReport.CollectedPaymentInPercent = financialReportList.Sum(x => x.CollectedPaymentInPercent);
                    exchangeFinancialReport.StockInPercent = financialReportList.Sum(x => x.StockInPercent);
                    exchangeFinancialReport.AdvancePaymentInPercent = financialReportList.Sum(x => x.AdvancePaymentInPercent);
                    exchangeFinancialReport.PerShareInPercent = financialReportList.Sum(x => x.PerShareInPercent);
                    exchangeFinancialReport.BuildingInPercent = financialReportList.Sum(x => x.BuildingInPercent);
                    exchangeFinancialReport.VehicleInPercent = financialReportList.Sum(x => x.VehicleInPercent);
                    exchangeFinancialReport.ToolsInPercent = financialReportList.Sum(x => x.ToolsInPercent);
                    exchangeFinancialReport.OfficeFurnitureInPercent = financialReportList.Sum(x => x.OfficeFurnitureInPercent);
                    exchangeFinancialReport.ComputerandaccessoriesInPercent = financialReportList.Sum(x => x.ComputerandaccessoriesInPercent);
                    financialReportList.Add(exchangeFinancialReport);

                    return financialReportList;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
