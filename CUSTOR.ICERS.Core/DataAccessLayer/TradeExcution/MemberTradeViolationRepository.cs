﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class MemberTradeViolationRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public MemberTradeViolationRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> CreateViolationRecord(MemberTradeViolationDTO memberTradeViolation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tradeViolation = await context.MemberTradeViolation.Where(
                                                                   x => x.ExchangeActorId == memberTradeViolation.ExchangeActorId &&
                                                                   x.ViolationStatus == 0)
                                                                   .AsNoTracking()
                                                                   .FirstOrDefaultAsync();
                    if (tradeViolation == null)
                    {
                        MemberTradeViolation newViolationRecord = mapper.Map<MemberTradeViolation>(memberTradeViolation);
                        context.Entry(newViolationRecord).State = EntityState.Added;
                        foreach (var clientInfo in newViolationRecord.TradeExcutionViolationRecord)
                        {
                            clientInfo.CreatedUserId = memberTradeViolation.CreatedUserId;
                            context.Entry(clientInfo).State = EntityState.Added;
                        }
                        foreach (var clientInfo in newViolationRecord.WorkFlow)
                        {
                            context.Entry(clientInfo).State = EntityState.Added;
                        }
                        context.SaveChanges();
                        transaction.Commit();
                        return newViolationRecord.MemberTradeViolationId;
                    }
                    else
                    {
                        foreach (var clientInfo in memberTradeViolation.TradeExcutionViolationRecord)
                        {
                            clientInfo.CreatedUserId = memberTradeViolation.CreatedUserId;
                            clientInfo.MemberTradeViolationId = tradeViolation.MemberTradeViolationId;
                            TradeExcutionViolationRecord mappedDetail = mapper.Map<TradeExcutionViolationRecord>(clientInfo);
                            context.TradeExcutionViolationRecord.Add(mappedDetail);
                        }
                        context.SaveChanges();
                        transaction.Commit();
                        return tradeViolation.MemberTradeViolationId;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }


            }

        }
    }
}