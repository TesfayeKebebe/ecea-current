﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class OffSiteMonitoringDetailRepository 
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public OffSiteMonitoringDetailRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<List<ExchangeActorOffSiteReportView>> GetOffSiteReportViewList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var offSiteMonitoring = await context.OffSiteMonitoringReportDetail.Where(
                                                                         x => x.OffSiteMonitoringReportId == notificationQuery.OffSiteMonitoringId)
                                                 .Select(n => new ExchangeActorOffSiteReportView
                                                 {
                                                     OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                     ReportPeriodId = n.OffSiteMonitoringReport.ReportPeriodId,
                                                     ReportTypeId = n.OffSiteMonitoringReport.ReportTypeId,
                                                     ReasonTypeId = n.OffSiteMonitoringReport.ReasonTypeId,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     ReasonType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReasonType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReasonType.DescriptionEng,
                                                     ReportType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                     ReportPeriod = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportPeriod.DescriptionAmh :
                                                                                                       n.OffSiteMonitoringReport.ReportPeriod.DescriptionEng,
                                                     Remark = n.OffSiteMonitoringReport.Remark,
                                                     Year = n.OffSiteMonitoringReport.Year,
                                                     OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                         n.ExchangeActor.OrganizationNameEng,
                                                     CustomerType = (notificationQuery.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                     n.ExchangeActor.MemberCategory.DescriptionEng,
                                                     MobliePhone = n.ExchangeActor.MobileNo,
                                                     RegularPhone = n.ExchangeActor.Tel,
                                                     ApprovalStatus = n.OffSiteMonitoringReport.Status,
                                                     WarningStatus = n.WarningStatus


                                                 })
                                                 .OrderBy(x => x.WarningStatus)
                                                 .AsNoTracking()
                                                 .ToListAsync();
                return offSiteMonitoring;
                //    new PagedResult<ExchangeActorOffSiteReportView>()
                //{
                //    Items = mapper.Map<List<ExchangeActorOffSiteReportView>>(offSiteMonitoring),
                //    ItemsCount = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReportId == notificationQuery.OffSiteMonitoringId).Count()
                //};


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<ExchangeActorOffSiteReportView>> GetExchangeActorOffSiteViolationList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {

                var offSiteMonitoring = await context.OffSiteMonitoringReportDetail.Where(
                                                                         x => x.OffSiteMonitoringReport.ReportTypeId == notificationQuery.ReportTypeId &&
                                                                              x.OffSiteMonitoringReport.ReportPeriodId == notificationQuery.ReportPeriodId &&
                                                                              x.OffSiteMonitoringReport.Year == notificationQuery.Year &&
                                                                              x.OffSiteMonitoringReport.ReasonTypeId == notificationQuery.ReportAnalysisId)
                                                 .Select(n => new ExchangeActorOffSiteReportView
                                                 {
                                                     OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                     ReportPeriodId = n.OffSiteMonitoringReport.ReportPeriodId,
                                                     ReportTypeId = n.OffSiteMonitoringReport.ReportTypeId,
                                                     ReasonTypeId = n.OffSiteMonitoringReport.ReasonTypeId,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     ReasonType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReasonType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReasonType.DescriptionEng,
                                                     ReportType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                     ReportPeriod = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportPeriod.DescriptionAmh :
                                                                                                       n.OffSiteMonitoringReport.ReportPeriod.DescriptionEng,
                                                     Remark = n.OffSiteMonitoringReport.Remark,
                                                     Year = n.OffSiteMonitoringReport.Year,
                                                     OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                         n.ExchangeActor.OrganizationNameEng,
                                                     CustomerType = (notificationQuery.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                     n.ExchangeActor.MemberCategory.DescriptionEng,
                                                     MobliePhone = n.ExchangeActor.MobileNo,
                                                     RegularPhone = n.ExchangeActor.Tel,
                                                     ApprovalStatus = n.OffSiteMonitoringReport.Status,
                                                     WarningStatus = n.WarningStatus


                                                 })
                                                 .OrderBy(x => x.WarningStatus)
                                                 .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                                                 .AsNoTracking()
                                                 .ToListAsync();
                return new PagedResult<ExchangeActorOffSiteReportView>()
                {
                    Items = mapper.Map<List<ExchangeActorOffSiteReportView>>(offSiteMonitoring),
                    ItemsCount = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == notificationQuery.ReportTypeId &&
                                                                             x.OffSiteMonitoringReport.ReportPeriodId == notificationQuery.ReportPeriodId &&
                                                                             x.OffSiteMonitoringReport.Year == notificationQuery.Year &&
                                                                             x.OffSiteMonitoringReport.ReasonTypeId == notificationQuery.ReportAnalysisId).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PagedResult<ExchangeActorOffSiteReportView>> GetExchangeActorAnnualViolationList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {

                var offSiteMonitoring = await context.OffSiteMonitoringReportDetail.Where(
                                                                              x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                              x.OffSiteMonitoringReport.ReportPeriodId == notificationQuery.ReportPeriodId &&
                                                                              x.OffSiteMonitoringReport.Year == notificationQuery.Year &&
                                                                              x.OffSiteMonitoringReport.ReasonTypeId == notificationQuery.ReportAnalysisId)
                                                 .Select(n => new ExchangeActorOffSiteReportView
                                                 {
                                                     OffSiteMonitoringReportId = n.OffSiteMonitoringReportId,
                                                     ReportPeriodId = n.OffSiteMonitoringReport.ReportPeriodId,
                                                     ReportTypeId = n.OffSiteMonitoringReport.ReportTypeId,
                                                     ReasonTypeId = n.OffSiteMonitoringReport.ReasonTypeId,
                                                     ExchangeActorId = n.ExchangeActorId,
                                                     ReasonType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReasonType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReasonType.DescriptionEng,
                                                     ReportType = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportType.DescriptionAmh :
                                                                                                     n.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                     ReportPeriod = (notificationQuery.Lang == "et") ? n.OffSiteMonitoringReport.ReportPeriod.DescriptionAmh :
                                                                                                       n.OffSiteMonitoringReport.ReportPeriod.DescriptionEng,
                                                     Remark = n.OffSiteMonitoringReport.Remark,
                                                     Year = n.OffSiteMonitoringReport.Year,
                                                     OrganizationName = (notificationQuery.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh :
                                                                                                         n.ExchangeActor.OrganizationNameEng,
                                                     CustomerType = (notificationQuery.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                                                     n.ExchangeActor.MemberCategory.DescriptionEng,
                                                     MobliePhone = n.ExchangeActor.MobileNo,
                                                     RegularPhone = n.ExchangeActor.Tel,
                                                     ApprovalStatus = n.OffSiteMonitoringReport.Status,
                                                     WarningStatus = n.WarningStatus


                                                 })
                                                 .OrderBy(x => x.WarningStatus)
                                                 .Paging(notificationQuery.PageCount, notificationQuery.PageNumber)
                                                 .AsNoTracking()
                                                 .ToListAsync();
                return new PagedResult<ExchangeActorOffSiteReportView>()
                {
                    Items = mapper.Map<List<ExchangeActorOffSiteReportView>>(offSiteMonitoring),
                    ItemsCount = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == notificationQuery.ReportTypeId &&
                                                                             x.OffSiteMonitoringReport.ReportPeriodId == notificationQuery.ReportPeriodId &&
                                                                             x.OffSiteMonitoringReport.Year == notificationQuery.Year &&
                                                                             x.OffSiteMonitoringReport.ReasonTypeId == notificationQuery.ReportAnalysisId).Count()
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<Guid> UpdateExchangeActorOffSiteStatus(Guid exchangeActorId, int reportTypeId,
                                                                 int reportPeriodId, int year, int reasonId,int StatusViewRecord)
        {
            try
            {
                switch (StatusViewRecord)
                {
                    case 1:
                        var violatedExchangeActor = await context.OffSiteMonitoringReportDetail
                                                      .Where(x => x.WarningStatus == 0 &&
                                                      x.OffSiteMonitoringReport.ReportTypeId == reportTypeId &&
                                                      x.OffSiteMonitoringReport.ReportPeriodId == reportPeriodId &&
                                                      x.ExchangeActorId == exchangeActorId &&
                                                      x.OffSiteMonitoringReport.Year == year &&
                                                      x.OffSiteMonitoringReport.ReasonTypeId == reasonId)
                                                      .AsNoTracking()
                                                      .ToListAsync();
                        foreach (var rec in violatedExchangeActor)
                        {
                            rec.WarningStatus = 1;
                            context.Entry(rec).State = EntityState.Modified;
                        }
                        await context.SaveChangesAsync();
                        return exchangeActorId;
                    case 2:
                        var violatedExchangeActorDirector = await context.OffSiteMonitoringReportDetail
                                                      .Where(x => x.WarningStatus == 1 &&
                                                      x.OffSiteMonitoringReport.ReportTypeId == reportTypeId &&
                                                      x.OffSiteMonitoringReport.ReportPeriodId == reportPeriodId &&
                                                      x.ExchangeActorId == exchangeActorId &&
                                                      x.OffSiteMonitoringReport.Year == year &&
                                                      x.OffSiteMonitoringReport.ReasonTypeId == reasonId)
                                                      .AsNoTracking()
                                                      .ToListAsync();
                        foreach (var rec in violatedExchangeActorDirector)
                        {
                            rec.WarningStatus = 2;
                            context.Entry(rec).State = EntityState.Modified;
                        }
                        await context.SaveChangesAsync();
                        return exchangeActorId;
                    default:
                        return Guid.Empty;
                }

               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> GetTotalMembersWarningStatus(Guid exchangeActorId, int reportTypeId,
                                                                int reportPeriodId, int year, int reasonId)
        {
            try
            {
                var violatedExchangeActor = await context.OffSiteMonitoringReportDetail
                               .Where(x => x.WarningStatus == 1 &&
                               x.OffSiteMonitoringReport.ReportTypeId == reportTypeId &&
                               x.OffSiteMonitoringReport.ReportPeriodId == reportPeriodId &&
                               x.ExchangeActorId == exchangeActorId &&
                               x.OffSiteMonitoringReport.Year == year &&
                               x.OffSiteMonitoringReport.ReasonTypeId == reasonId)
                               .AsNoTracking()
                               .ToListAsync();                
                return violatedExchangeActor.Count();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<ComparisonOffSiteMonitoring> OffSiteMonitoringClientComprisonList(ComparativeQueryParameterSearch offSite)
        {
            try
            {
                List<ComparisonOffSiteMonitoring> comparisonOffSites = new List<ComparisonOffSiteMonitoring>();
                ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();

                var offSiteMonit = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                    ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                    ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == 72)))
                                                                                  .AsNoTracking()
                                                                                  .ToList();
                if (offSiteMonit.Count() == 0)
                {
                    return null;
                }
                // ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                var compOffSiteMonitor = offSiteMonit.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                int totalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                   x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                  ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == 72)))
                                                                                  .Count();
                if (totalFirstQuarter == 0)
                {
                    totalFirstQuarter = 1;
                }
                int totalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                   x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                  ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == 72)))
                                                                                  .Count();
                if (totalSecondQuarter == 0)
                {
                    totalSecondQuarter = 1;
                }
                int totalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                   x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                  ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                  .Count();
                if (totalThiredQuarter == 0)
                {
                    totalThiredQuarter = 1;
                }
                int totalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                   x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                  ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                  .Count();
                if (totalFourthQuarter == 0)
                {
                    totalFourthQuarter = 1;
                }
                foreach (var name in compOffSiteMonitor)
                {
                    offSiteMonitoring = new ComparisonOffSiteMonitoring();
                    offSiteMonitoring.Year = name.Year;
                    offSiteMonitoring.FirstQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                           x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                    offSiteMonitoring.SecondQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                              x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                    offSiteMonitoring.ThiredQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                            x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                    offSiteMonitoring.FourthQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                              x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                    offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (totalFirstQuarter);
                    offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (totalSecondQuarter);
                    offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (totalThiredQuarter);
                    offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (totalFourthQuarter);
                    comparisonOffSites.Add(offSiteMonitoring);
                }
                return comparisonOffSites;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
                public List<ComparisonOffSiteMonitoring> OffSiteMonitoringComprisonList(ComparativeQueryParameterSearch offSite)
        {
            try
            {
                List<ComparisonOffSiteMonitoring> comparisonOffSites = new List<ComparisonOffSiteMonitoring>();
                ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                if(offSite.CustomerTypeId != 0) { 
                }
                switch (offSite.ReportTypeId)
                {
                    case 1:
                        if(offSite.CustomerTypeId == 72 || offSite.CustomerTypeId == 88)
                        {

                            var offSiteMonit = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                                ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .AsNoTracking()
                                                                                              .ToList();
                            if (offSiteMonit.Count() == 0)
                            {
                                return null;
                            }
                            // ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                            var compOffSiteMonitor = offSiteMonit.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int totalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalFirstQuarter == 0)
                            {
                                totalFirstQuarter = 1;
                            }
                            int totalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalSecondQuarter == 0)
                            {
                                totalSecondQuarter = 1;
                            }
                            int totalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalThiredQuarter == 0)
                            {
                                totalThiredQuarter = 1;
                            }
                            int totalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalFourthQuarter == 0)
                            {
                                totalFourthQuarter = 1;
                            }
                            foreach (var name in compOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (totalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (totalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (totalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (totalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }
                        else if(offSite.CustomerTypeId == 90 || offSite.CustomerTypeId == 6)
                        {

                            var offSiteMonit = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                                ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                              .AsNoTracking()
                                                                                              .ToList();
                            if (offSiteMonit.Count() == 0)
                            {
                                return null;
                            }
                            // ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                            var compOffSiteMonitor = offSiteMonit.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int totalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalFirstQuarter == 0)
                            {
                                totalFirstQuarter = 1;
                            }
                            int totalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalSecondQuarter == 0)
                            {
                                totalSecondQuarter = 1;
                            }
                            int totalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalThiredQuarter == 0)
                            {
                                totalThiredQuarter = 1;
                            }
                            int totalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategory.LookupId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (totalFourthQuarter == 0)
                            {
                                totalFourthQuarter = 1;
                            }
                            foreach (var name in compOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (totalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (totalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (totalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (totalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }
                        else
                        {

                            var offSiteMonit = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                                ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .AsNoTracking()
                                                                                              .ToList();
                            if (offSiteMonit.Count() == 0)
                            {
                                return null;
                            }
                            // ComparisonOffSiteMonitoring offSiteMonitoring = new ComparisonOffSiteMonitoring();
                            var compOffSiteMonitor = offSiteMonit.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int totalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (totalFirstQuarter == 0)
                            {
                                totalFirstQuarter = 1;
                            }
                            int totalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (totalSecondQuarter == 0)
                            {
                                totalSecondQuarter = 1;
                            }
                            int totalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (totalThiredQuarter == 0)
                            {
                                totalThiredQuarter = 1;
                            }
                            int totalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (totalFourthQuarter == 0)
                            {
                                totalFourthQuarter = 1;
                            }
                            foreach (var name in compOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonit.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (totalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (totalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (totalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (totalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }

                        break;

                    case 2:
                        if(offSite.CustomerTypeId == 72 || offSite.CustomerTypeId == 88)
                        {
                            var offSiteMonitor = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                                                       ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                                       ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                                                     .AsNoTracking()
                                                                                                                     .ToList();
                            if (offSiteMonitor.Count() == 0)
                            {
                                return null;
                            }

                            var fincompOffSiteMonitor = offSiteMonitor.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int finTotalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalFirstQuarter == 0)
                            {
                                finTotalFirstQuarter = 1;
                            }
                            int finTotalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalSecondQuarter == 0)
                            {
                                finTotalSecondQuarter = 1;
                            }
                            int finTotalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalThiredQuarter == 0)
                            {
                                finTotalThiredQuarter = 1;
                            }
                            int finTotalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalFourthQuarter == 0)
                            {
                                finTotalFourthQuarter = 1;
                            }
                            foreach (var name in fincompOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (finTotalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (finTotalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (finTotalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (finTotalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }
                        else if(offSite.CustomerTypeId == 6 || offSite.CustomerTypeId == 90)
                        {
                            var offSiteMonitor = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                                                                  ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                                                                                .AsNoTracking()
                                                                                                                                                .ToList();
                            if (offSiteMonitor.Count() == 0)
                            {
                                return null;
                            }

                            var fincompOffSiteMonitor = offSiteMonitor.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int finTotalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalFirstQuarter == 0)
                            {
                                finTotalFirstQuarter = 1;
                            }
                            int finTotalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalSecondQuarter == 0)
                            {
                                finTotalSecondQuarter = 1;
                            }
                            int finTotalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalThiredQuarter == 0)
                            {
                                finTotalThiredQuarter = 1;
                            }
                            int finTotalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((offSite.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId)))
                                                                                              .Count();
                            if (finTotalFourthQuarter == 0)
                            {
                                finTotalFourthQuarter = 1;
                            }
                            foreach (var name in fincompOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (finTotalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (finTotalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (finTotalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (finTotalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }
                        else
                        {
                            var offSiteMonitor = context.OffSiteMonitoringReportDetail.Include(x => x.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                                                                                  ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                                                                                   ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                                                                                .AsNoTracking()
                                                                                                                                                .ToList();
                            if (offSiteMonitor.Count() == 0)
                            {
                                return null;
                            }

                            var fincompOffSiteMonitor = offSiteMonitor.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                            int finTotalFirstQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 1 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                               ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (finTotalFirstQuarter == 0)
                            {
                                finTotalFirstQuarter = 1;
                            }
                            int finTotalSecondQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 2 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                               ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (finTotalSecondQuarter == 0)
                            {
                                finTotalSecondQuarter = 1;
                            }
                            int finTotalThiredQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 3 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90)))
                                                                                              .Count();
                            if (finTotalThiredQuarter == 0)
                            {
                                finTotalThiredQuarter = 1;
                            }
                            int finTotalFourthQuarter = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                               x.OffSiteMonitoringReport.ReportPeriodId == 4 &&
                                                                                              ((x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId)) &&
                                                                                              ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId == 90 )))
                                                                                              .Count();
                            if (finTotalFourthQuarter == 0)
                            {
                                finTotalFourthQuarter = 1;
                            }
                            foreach (var name in fincompOffSiteMonitor)
                            {
                                offSiteMonitoring = new ComparisonOffSiteMonitoring();
                                offSiteMonitoring.Year = name.Year;
                                offSiteMonitoring.FirstQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                       x.OffSiteMonitoringReport.ReportPeriodId == 1).Count();
                                offSiteMonitoring.SecondQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 2).Count();
                                offSiteMonitoring.ThiredQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                        x.OffSiteMonitoringReport.ReportPeriodId == 3).Count();
                                offSiteMonitoring.FourthQuarter = offSiteMonitor.Where(x => x.OffSiteMonitoringReport.Year == name.Year &&
                                                                                          x.OffSiteMonitoringReport.ReportPeriodId == 4).Count();
                                offSiteMonitoring.PercentageFirstQuarter = (offSiteMonitoring.FirstQuarter) * 100 / (finTotalFirstQuarter);
                                offSiteMonitoring.PercentageSecondQuarter = (offSiteMonitoring.SecondQuarter) * 100 / (finTotalSecondQuarter);
                                offSiteMonitoring.PercentageThiredQuarter = (offSiteMonitoring.ThiredQuarter) * 100 / (finTotalThiredQuarter);
                                offSiteMonitoring.PercentageFourthQuarter = (offSiteMonitoring.FourthQuarter) * 100 / (finTotalFourthQuarter);
                                comparisonOffSites.Add(offSiteMonitoring);
                            }
                        }


                       
                        break;
                    default:
                        return null;
                }

                return comparisonOffSites;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<DepulicateMemberReportingViolation> GetDuplicateReportingAnnualViolation(ComparativeQueryParameterSearch offSite)
        {
            try
            {
                if(offSite.CustomerTypeId == 72 || offSite.CustomerTypeId == 88)
                {
                    var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                                            (x.OffSiteMonitoringReport.ReportTypeId == 4) &&
                                                                                                                            (offSite.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId) &&
                                                                                                                            (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                                            (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                                            .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                                            .Select(b => new
                                                                                                                            {
                                                                                                                                Id = b.Key,
                                                                                                                                Count = b.Count()
                                                                                                                            })
                                                                                                                            .AsNoTracking()
                                                                                                                            .ToList();
                    var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                    join x in reportingViolations
                                                    on exa.ExchangeActorId equals x.Id
                                                    where exa.OffSiteMonitoringReport.ReportTypeId == 4
                                                    select new DepulicateMemberReportingViolation
                                                    {
                                                        Id = exa.ExchangeActorId,
                                                        OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                        ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                        Total = x.Count

                                                    })
                                     .AsNoTracking()
                                     .ToList();
                    return offsiteMonitoringDetails;
                }
                else if (offSite.CustomerTypeId == 6 || offSite.CustomerTypeId == 5 || offSite.CustomerTypeId == 90)
                {
                    var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                                            (x.OffSiteMonitoringReport.ReportTypeId == 4) &&
                                                                                                                            (offSite.CustomerTypeId == 0 || x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId) &&
                                                                                                                            (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                                            (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                                            .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                                            .Select(b => new
                                                                                                                            {
                                                                                                                                Id = b.Key,
                                                                                                                                Count = b.Count()
                                                                                                                            })
                                                                                                                            .AsNoTracking()
                                                                                                                            .ToList();
                    var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                    join x in reportingViolations
                                                    on exa.ExchangeActorId equals x.Id
                                                    where exa.OffSiteMonitoringReport.ReportTypeId == 4
                                                    select new DepulicateMemberReportingViolation
                                                    {
                                                        Id = exa.ExchangeActorId,
                                                        OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                        ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                        Total = x.Count

                                                    })
                                     .AsNoTracking()
                                     .ToList();
                    return offsiteMonitoringDetails;
                }
                else
                {
                    var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                                            (x.OffSiteMonitoringReport.ReportTypeId == 4) &&
                                                                                                                           // (offSite.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId) &&
                                                                                                                            (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                                            (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                                            .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                                            .Select(b => new
                                                                                                                            {
                                                                                                                                Id = b.Key,
                                                                                                                                Count = b.Count()
                                                                                                                            })
                                                                                                                            .AsNoTracking()
                                                                                                                            .ToList();
                    var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                    join x in reportingViolations
                                                    on exa.ExchangeActorId equals x.Id
                                                    where exa.OffSiteMonitoringReport.ReportTypeId == 4
                                                    select new DepulicateMemberReportingViolation
                                                    {
                                                        Id = exa.ExchangeActorId,
                                                        OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                        ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                        Total = x.Count

                                                    })
                                     .AsNoTracking()
                                     .ToList();
                    return offsiteMonitoringDetails;
                }
                    
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

     public List<DepulicateMemberReportingViolation> GetDuplicateReportingViolation(ComparativeQueryParameterSearch offSite)
             {
            try
            {
                switch (offSite.ReportTypeId)
                {
                    case 1:
                        if(offSite.CustomerTypeId == 72 || offSite.CustomerTypeId == 88)
                        {

                            var reportingViolation = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                (x.OffSiteMonitoringReport.ReportTypeId == 1) &&
                                                                                (offSite.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId) &&
                                                                                (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                .GroupBy(r => r.ExchangeActorId)
                                                                                .Select(b => new
                                                                                {
                                                                                    Id = b.Key,
                                                                                    Count = b.Count()
                                                                                })
                                                                                .AsNoTracking()
                                                                                .ToList();
                            var offsiteMonitoringDetail = (from exa in context.OffSiteMonitoringReportDetail
                                                           join x in reportingViolation
                                                           on exa.ExchangeActorId equals x.Id
                                                           where exa.OffSiteMonitoringReport.ReportTypeId == 1
                                                           select new DepulicateMemberReportingViolation
                                                           {
                                                               Id = exa.ExchangeActorId,
                                                               OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                               ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                               Total = x.Count

                                                           })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetail;
                        }
                        else if(offSite.CustomerTypeId == 90 || offSite.CustomerTypeId == 6)
                        {

                            var reportingViolation = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                (x.OffSiteMonitoringReport.ReportTypeId == 1) &&
                                                                                (offSite.CustomerTypeId == 0 || x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId) &&
                                                                                (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                .GroupBy(r => r.ExchangeActorId)
                                                                                .Select(b => new
                                                                                {
                                                                                    Id = b.Key,
                                                                                    Count = b.Count()
                                                                                })
                                                                                .AsNoTracking()
                                                                                .ToList();
                            var offsiteMonitoringDetail = (from exa in context.OffSiteMonitoringReportDetail
                                                           join x in reportingViolation
                                                           on exa.ExchangeActorId equals x.Id
                                                           where exa.OffSiteMonitoringReport.ReportTypeId == 1
                                                           select new DepulicateMemberReportingViolation
                                                           {
                                                               Id = exa.ExchangeActorId,
                                                               OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                               ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                               Total = x.Count

                                                           })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetail;
                        }
                        else
                        {

                            var reportingViolation = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                (x.OffSiteMonitoringReport.ReportTypeId == 1) &&                                                                                
                                                                                (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                .GroupBy(r => r.ExchangeActorId)
                                                                                .Select(b => new
                                                                                {
                                                                                    Id = b.Key,
                                                                                    Count = b.Count()
                                                                                })
                                                                                .AsNoTracking()
                                                                                .ToList();
                            var offsiteMonitoringDetail = (from exa in context.OffSiteMonitoringReportDetail
                                                           join x in reportingViolation
                                                           on exa.ExchangeActorId equals x.Id
                                                           where exa.OffSiteMonitoringReport.ReportTypeId == 1
                                                           select new DepulicateMemberReportingViolation
                                                           {
                                                               Id = exa.ExchangeActorId,
                                                               OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                               ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                               Total = x.Count

                                                           })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetail;

                        }
                    case 2:
                        if(offSite.CustomerTypeId == 72 || offSite.CustomerTypeId == 8)
                        {
                            var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                        (x.OffSiteMonitoringReport.ReportTypeId == 2 ) &&
                                                                                                        (offSite.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == offSite.CustomerTypeId ) &&
                                                                                                        (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                        (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                        .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                        .Select(b => new
                                                                                                        {
                                                                                                            Id = b.Key,
                                                                                                            Count = b.Count()
                                                                                                        })
                                                                                                        .AsNoTracking()
                                                                                                        .ToList();
                            var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                            join x in reportingViolations
                                                            on exa.ExchangeActorId equals x.Id
                                                            where exa.OffSiteMonitoringReport.ReportTypeId == 2 
                                                            select new DepulicateMemberReportingViolation
                                                            {
                                                                Id = exa.ExchangeActorId,
                                                                OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                                ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                                Total = x.Count

                                                            })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetails;
                        }
                        else if (offSite.CustomerTypeId == 90 || offSite.CustomerTypeId == 6)
                        {
                            var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                        (x.OffSiteMonitoringReport.ReportTypeId == 2 ) &&
                                                                                                        (offSite.CustomerTypeId == 0 || x.ExchangeActor.CustomerTypeId == offSite.CustomerTypeId) &&
                                                                                                        (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                        (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                        .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                        .Select(b => new
                                                                                                        {
                                                                                                            Id = b.Key,
                                                                                                            Count = b.Count()
                                                                                                        })
                                                                                                        .AsNoTracking()
                                                                                                        .ToList();
                            var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                            join x in reportingViolations
                                                            on exa.ExchangeActorId equals x.Id
                                                            where exa.OffSiteMonitoringReport.ReportTypeId == 2 
                                                            select new DepulicateMemberReportingViolation
                                                            {
                                                                Id = exa.ExchangeActorId,
                                                                OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                                ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                                Total = x.Count

                                                            })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetails;
                        }
                        else
                        {
                            var reportingViolations = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                                                        (x.OffSiteMonitoringReport.ReportTypeId == 2) &&                                                                                                       
                                                                                                        (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                                                        (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                                                        .GroupBy(r => r.ExchangeActorId, y => y.OffSiteMonitoringReport.ReportTypeId)
                                                                                                        .Select(b => new
                                                                                                        {
                                                                                                            Id = b.Key,
                                                                                                            Count = b.Count()
                                                                                                        })
                                                                                                        .AsNoTracking()
                                                                                                        .ToList();
                            var offsiteMonitoringDetails = (from exa in context.OffSiteMonitoringReportDetail
                                                            join x in reportingViolations
                                                            on exa.ExchangeActorId equals x.Id
                                                            where exa.OffSiteMonitoringReport.ReportTypeId == 2 
                                                            select new DepulicateMemberReportingViolation
                                                            {
                                                                Id = exa.ExchangeActorId,
                                                                OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                                ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                                Total = x.Count

                                                            })
                                             .AsNoTracking()
                                             .ToList();
                            return offsiteMonitoringDetails;
                        }
                    
                            
                    default:
                        return null;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DepulicateMemberReportingViolation> GetDuplicateReportingClientViolation(ComparativeQueryParameterSearch offSite)
        {
            try
            {
               
                        var reportingViolation = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.Status == 2 &&
                                                                            (x.OffSiteMonitoringReport.ReportTypeId == 3) &&
                                                                            (offSite.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == 72) &&
                                                                            (offSite.Year == 0 || x.OffSiteMonitoringReport.Year == offSite.Year) &&
                                                                            (offSite.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == offSite.ReasonTypeId))
                                                                            .GroupBy(r => r.ExchangeActorId)
                                                                            .Select(b => new
                                                                            {
                                                                                Id = b.Key,
                                                                                Count = b.Count()
                                                                            })
                                                                            .AsNoTracking()
                                                                            .ToList();
                        var offsiteMonitoringDetail = (from exa in context.OffSiteMonitoringReportDetail
                                                       join x in reportingViolation
                                                       on exa.ExchangeActorId equals x.Id
                                                       where exa.OffSiteMonitoringReport.ReportTypeId == 3
                                                       select new DepulicateMemberReportingViolation
                                                       {
                                                           Id = exa.ExchangeActorId,
                                                           OrganizationName = (offSite.Lang == "et") ? exa.ExchangeActor.OrganizationNameAmh : exa.ExchangeActor.OrganizationNameEng,
                                                           ReportType = (offSite.Lang == "et") ? exa.OffSiteMonitoringReport.ReportType.DescriptionAmh : exa.OffSiteMonitoringReport.ReportType.DescriptionEng,
                                                           Total = x.Count

                                                       })
                                         .AsNoTracking()
                                         .ToList();
                        return offsiteMonitoringDetail;                    
                

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ReportAccomplishedResposiblity> GetReportClientAccomplishResponsibilityList(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
               
                    List<ReportAccomplishedResposiblity> comparisonOffSites = new List<ReportAccomplishedResposiblity>();
                    ReportAccomplishedResposiblity offSiteMonitoring = new ReportAccomplishedResposiblity();
                    var annualReportAnanlysis = context.OffSiteMonitoringReportDetail.Include(y => y.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                                (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                                 ((x.ExchangeActor.MemberCategoryId == 72))
                                                                                                )
                                                                                               .AsNoTracking()
                                                                                               .ToList();
                    var lateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                    if (lateReport == 0)
                    {
                        lateReport = 1;
                    }

                    var onTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                    if (onTimeReport == 0)
                    {
                        onTimeReport = 1;
                    }
                    var notReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                    if (notReport == 0)
                    {
                        notReport = 1;
                    }
                    var fincompOffSiteMonitor = annualReportAnanlysis.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        offSiteMonitoring = new ReportAccomplishedResposiblity();
                        offSiteMonitoring.Year = name.Year;
                        offSiteMonitoring.OnTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                 x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                        offSiteMonitoring.LateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                        offSiteMonitoring.NotReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.OnTimeReport * 100) / onTimeReport;
                        offSiteMonitoring.LateReportInPercent = (offSiteMonitoring.LateReport * 100) / lateReport;
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.NotReport * 100) / notReport;
                        comparisonOffSites.Add(offSiteMonitoring);
                    }
                    offSiteMonitoring = new ReportAccomplishedResposiblity();
                    //offSiteMonitoring.Year = ((queryParameterSearch.Lang == "et")? "ድምር": "Total");
                    offSiteMonitoring.OnTimeReport = comparisonOffSites.Sum(x => x.OnTimeReport);
                    offSiteMonitoring.LateReport = comparisonOffSites.Sum(x => x.LateReport);
                    offSiteMonitoring.NotReport = comparisonOffSites.Sum(x => x.NotReport);
                    offSiteMonitoring.OnTimeReportInPercent = comparisonOffSites.Sum(x => x.OnTimeReportInPercent);
                    offSiteMonitoring.LateReportInPercent = comparisonOffSites.Sum(x => x.LateReportInPercent);
                    offSiteMonitoring.NotReportInPercent = comparisonOffSites.Sum(x => x.NotReportInPercent);
                    comparisonOffSites.Add(offSiteMonitoring);
                    return comparisonOffSites;
                
            
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ReportAccomplishedResposiblity> GetReportAccomplishResponsibilityList(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                {
                    List<ReportAccomplishedResposiblity> comparisonOffSites = new List<ReportAccomplishedResposiblity>();
                    ReportAccomplishedResposiblity offSiteMonitoring = new ReportAccomplishedResposiblity();
                    var annualReportAnanlysis = context.OffSiteMonitoringReportDetail.Include(y => y.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                                (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                                 ((queryParameterSearch.CustomerTypeId == 0) || (x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId))
                                                                                                )
                                                                                               .AsNoTracking()
                                                                                               .ToList();
                    var lateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                    if (lateReport == 0)
                    {
                        lateReport = 1;
                    }

                    var onTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                    if (onTimeReport == 0)
                    {
                        onTimeReport = 1;
                    }
                    var notReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                    if (notReport == 0)
                    {
                        notReport = 1;
                    }
                    var fincompOffSiteMonitor = annualReportAnanlysis.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        offSiteMonitoring = new ReportAccomplishedResposiblity();
                        offSiteMonitoring.Year = name.Year;
                        offSiteMonitoring.OnTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                 x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                        offSiteMonitoring.LateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                        offSiteMonitoring.NotReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.OnTimeReport *100) / onTimeReport;
                        offSiteMonitoring.LateReportInPercent = (offSiteMonitoring.LateReport * 100) / lateReport;
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.NotReport * 100) / notReport;
                        comparisonOffSites.Add(offSiteMonitoring);
                    }
                    offSiteMonitoring = new ReportAccomplishedResposiblity();
                    //offSiteMonitoring.Year = ((queryParameterSearch.Lang == "et")? "ድምር": "Total");
                    offSiteMonitoring.OnTimeReport = comparisonOffSites.Sum(x => x.OnTimeReport);
                    offSiteMonitoring.LateReport = comparisonOffSites.Sum(x => x.LateReport);
                    offSiteMonitoring.NotReport = comparisonOffSites.Sum(x => x.NotReport);
                    offSiteMonitoring.OnTimeReportInPercent = comparisonOffSites.Sum(x => x.OnTimeReportInPercent);
                    offSiteMonitoring.LateReportInPercent = comparisonOffSites.Sum(x => x.LateReportInPercent);
                    offSiteMonitoring.NotReportInPercent = comparisonOffSites.Sum(x => x.NotReportInPercent);
                    comparisonOffSites.Add(offSiteMonitoring);
                    return comparisonOffSites;
                }else if(queryParameterSearch.CustomerTypeId==90 || queryParameterSearch.CustomerTypeId == 6)
                {
                    List<ReportAccomplishedResposiblity> comparisonOffSites = new List<ReportAccomplishedResposiblity>();
                    ReportAccomplishedResposiblity offSiteMonitoring = new ReportAccomplishedResposiblity();
                    var annualReportAnanlysis = context.OffSiteMonitoringReportDetail.Include(y => y.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                                (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                                 ((queryParameterSearch.CustomerTypeId == 0) || (x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId))
                                                                                                )
                                                                                               .AsNoTracking()
                                                                                               .ToList();
                    var lateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                    if (lateReport == 0)
                    {
                        lateReport = 1;
                    }

                    var onTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                    if (onTimeReport == 0)
                    {
                        onTimeReport = 1;
                    }
                    var notReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                    if (notReport == 0)
                    {
                        notReport = 1;
                    }
                    var fincompOffSiteMonitor = annualReportAnanlysis.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        offSiteMonitoring = new ReportAccomplishedResposiblity();
                        offSiteMonitoring.Year = name.Year;
                        offSiteMonitoring.OnTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                 x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                        offSiteMonitoring.LateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                        offSiteMonitoring.NotReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.OnTimeReport * 100) / onTimeReport;
                        offSiteMonitoring.LateReportInPercent = (offSiteMonitoring.LateReport * 100) / lateReport;
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.NotReport * 100) / notReport;
                        comparisonOffSites.Add(offSiteMonitoring);
                    }
                    offSiteMonitoring = new ReportAccomplishedResposiblity();
                    //offSiteMonitoring.Year = ((queryParameterSearch.Lang == "et")? "ድምር": "Total");
                    offSiteMonitoring.OnTimeReport = comparisonOffSites.Sum(x => x.OnTimeReport);
                    offSiteMonitoring.LateReport = comparisonOffSites.Sum(x => x.LateReport);
                    offSiteMonitoring.NotReport = comparisonOffSites.Sum(x => x.NotReport);
                    offSiteMonitoring.OnTimeReportInPercent = comparisonOffSites.Sum(x => x.OnTimeReportInPercent);
                    offSiteMonitoring.LateReportInPercent = comparisonOffSites.Sum(x => x.LateReportInPercent);
                    offSiteMonitoring.NotReportInPercent = comparisonOffSites.Sum(x => x.NotReportInPercent);
                    comparisonOffSites.Add(offSiteMonitoring);
                    return comparisonOffSites;
                }

                else
                {
                    List<ReportAccomplishedResposiblity> comparisonOffSites = new List<ReportAccomplishedResposiblity>();
                    ReportAccomplishedResposiblity offSiteMonitoring = new ReportAccomplishedResposiblity();
                    var annualReportAnanlysis = context.OffSiteMonitoringReportDetail.Include(y => y.OffSiteMonitoringReport).Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                                (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                                 ((x.ExchangeActor.CustomerTypeId == 6 || x.ExchangeActor.CustomerTypeId==90))
                                                                                                )
                                                                                               .AsNoTracking()
                                                                                               .ToList();
                    var lateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                    if (lateReport == 0)
                    {
                        lateReport = 1;
                    }

                    var onTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                    if (onTimeReport == 0)
                    {
                        onTimeReport = 1;
                    }
                    var notReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                    if (notReport == 0)
                    {
                        notReport = 1;
                    }
                    var fincompOffSiteMonitor = annualReportAnanlysis.Select(x => new { x.OffSiteMonitoringReport.Year }).Distinct();
                    foreach (var name in fincompOffSiteMonitor)
                    {
                        offSiteMonitoring = new ReportAccomplishedResposiblity();
                        offSiteMonitoring.Year = name.Year;
                        offSiteMonitoring.OnTimeReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                 x.OffSiteMonitoringReport.ReasonTypeId == 133).Count();
                        offSiteMonitoring.LateReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132).Count();
                        offSiteMonitoring.NotReport = annualReportAnanlysis.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134).Count();
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.OnTimeReport * 100) / onTimeReport;
                        offSiteMonitoring.LateReportInPercent = (offSiteMonitoring.LateReport * 100) / lateReport;
                        offSiteMonitoring.OnTimeReportInPercent = (offSiteMonitoring.NotReport * 100) / notReport;
                        comparisonOffSites.Add(offSiteMonitoring);
                    }
                    offSiteMonitoring = new ReportAccomplishedResposiblity();
                    //offSiteMonitoring.Year = ((queryParameterSearch.Lang == "et")? "ድምር": "Total");
                    offSiteMonitoring.OnTimeReport = comparisonOffSites.Sum(x => x.OnTimeReport);
                    offSiteMonitoring.LateReport = comparisonOffSites.Sum(x => x.LateReport);
                    offSiteMonitoring.NotReport = comparisonOffSites.Sum(x => x.NotReport);
                    offSiteMonitoring.OnTimeReportInPercent = comparisonOffSites.Sum(x => x.OnTimeReportInPercent);
                    offSiteMonitoring.LateReportInPercent = comparisonOffSites.Sum(x => x.LateReportInPercent);
                    offSiteMonitoring.NotReportInPercent = comparisonOffSites.Sum(x => x.NotReportInPercent);
                    comparisonOffSites.Add(offSiteMonitoring);
                    return comparisonOffSites;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<MemberAnnualFinancialAuditorViewModel> GetListOfExchangeActorReportList(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                List<MemberAnnualFinancialAuditorViewModel> listOfReported = null;
                if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                {
                    listOfReported = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                      (queryParameterSearch.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == queryParameterSearch.ReasonTypeId) &&
                                                                                      (queryParameterSearch.CustomerTypeId == 0 || x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId) &&
                                                                                      (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId)
                                                                                      )
                              .Select(n => new MemberAnnualFinancialAuditorViewModel
                              {
                                  OrganizationName = (queryParameterSearch.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh : n.ExchangeActor.OrganizationNameEng,
                                  CustomerType = (queryParameterSearch.Lang == "et") ? n.ExchangeActor.MemberCategory.DescriptionAmh : n.ExchangeActor.MemberCategory.DescriptionEng,
                                  MobileNo = n.ExchangeActor.MobileNo,
                                  RegularPhone = n.ExchangeActor.Tel,
                              })
                              .AsNoTracking()
                              .ToList();

                }
                else
                {
                    listOfReported = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                                           (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year) &&
                                                                                                           (queryParameterSearch.ReasonTypeId == 0 || x.OffSiteMonitoringReport.ReasonTypeId == queryParameterSearch.ReasonTypeId) &&
                                                                                                           (queryParameterSearch.CustomerTypeId == 0 || x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId) &&
                                                                                                           (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId)
                                                                                                           )
                                                   .Select(n => new MemberAnnualFinancialAuditorViewModel
                                                   {
                                                       OrganizationName = (queryParameterSearch.Lang == "et") ? n.ExchangeActor.OrganizationNameAmh : n.ExchangeActor.OrganizationNameEng,
                                                       CustomerType = (queryParameterSearch.Lang == "et") ? n.ExchangeActor.CustomerType.DescriptionAmh : n.ExchangeActor.CustomerType.DescriptionEng,
                                                       MobileNo = n.ExchangeActor.MobileNo,
                                                       RegularPhone = n.ExchangeActor.Tel,
                                                   })
                                                   .AsNoTracking()
                                                   .ToList();
                }
                return listOfReported;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TradeExecutionAmountViewModel> GetNoOfExchangeactorReportStatus(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                List<TradeExecutionAmountViewModel> listOfExchangeactorReportStatus = new List<TradeExecutionAmountViewModel>();
                if(queryParameterSearch.CustomerTypeId != 0)
                {
                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    
                    else
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }

                }
                else
                {

                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                    var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                 // x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                    onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                    onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                    listOfExchangeactorReportStatus.Add(onTimeExchangeActor);

                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();                   
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                 // x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);

                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                    
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 4 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                // x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);

                    }

                return listOfExchangeactorReportStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TradeExecutionAmountViewModel> GetQuarterlyFinancialNoOfExchangeactorReport(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                List<TradeExecutionAmountViewModel> listOfExchangeactorReportStatus = new List<TradeExecutionAmountViewModel>();
                if (queryParameterSearch.CustomerTypeId != 0)
                {
                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }

                }
                else
                {
                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();

                    var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                 x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                             // x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                             (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                            (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                    NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                    NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                    listOfExchangeactorReportStatus.Add(NotReportExchangeActor);

                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();

                    var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                  x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                              // x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                              (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                             (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                    lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                    lateReportedExchangeActor.Amount = exchangeactorReportLate;
                    listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);


                    TradeExecutionAmountViewModel NotReportExchangeActors = new TradeExecutionAmountViewModel();

                    var exchangeactorReportNoReports = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 2 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                    NotReportExchangeActors.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                    NotReportExchangeActors.Amount = exchangeactorReportNoReports;
                    listOfExchangeactorReportStatus.Add(NotReportExchangeActors);

                }

                return listOfExchangeactorReportStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TradeExecutionAmountViewModel> GetQuarterlyClientNoOfExchangeactorReport(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                List<TradeExecutionAmountViewModel> listOfExchangeactorReportStatus = new List<TradeExecutionAmountViewModel>();
                
                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                   
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.MemberCategoryId == 72 &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት ያደረጉ የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                   
                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();
                    
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3  &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                  x.ExchangeActor.MemberCategoryId == 72 &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት ያደረጉ የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                   TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                   
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 3 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 x.ExchangeActor.MemberCategoryId == 72 &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                   

               
                
                return listOfExchangeactorReportStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TradeExecutionAmountViewModel> GetQuarterlyTradeNoOfExchangeactorReport(ComparativeQueryParameterSearch queryParameterSearch)
        {
            try
            {
                List<TradeExecutionAmountViewModel> listOfExchangeactorReportStatus = new List<TradeExecutionAmountViewModel>();               

                if(queryParameterSearch.CustomerTypeId != 0)
                {
                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                  x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                        onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                        listOfExchangeactorReportStatus.Add(onTimeExchangeActor);
                    }
                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                  x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                        lateReportedExchangeActor.Amount = exchangeactorReportLate;
                        listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);
                    }
                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                    if (queryParameterSearch.CustomerTypeId == 72 || queryParameterSearch.CustomerTypeId == 88)
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 x.ExchangeActor.MemberCategoryId == queryParameterSearch.CustomerTypeId &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }
                    else
                    {
                        var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                       x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                   x.ExchangeActor.CustomerTypeId == queryParameterSearch.CustomerTypeId &&
                                                                   (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                  (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                        NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                        NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                        listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    }

                }
                else
                {
                    TradeExecutionAmountViewModel onTimeExchangeActor = new TradeExecutionAmountViewModel();
                    var exchangeactorReportOnTime = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 133 &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                    onTimeExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "በወቅቱ ሪፖርት የግብይት ተሳታፊዎች" : "On Time Report Exchange Actors";
                    onTimeExchangeActor.Amount = exchangeactorReportOnTime;
                    listOfExchangeactorReportStatus.Add(onTimeExchangeActor);

                    TradeExecutionAmountViewModel lateReportedExchangeActor = new TradeExecutionAmountViewModel();
                   
                    var exchangeactorReportLate = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                      x.OffSiteMonitoringReport.ReasonTypeId == 132 &&
                                                                  (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                 (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                     lateReportedExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "አዘግይቶ ሪፖርት የግብይት ተሳታፊዎች" : "Late Reported Exchange Actors";
                     lateReportedExchangeActor.Amount = exchangeactorReportLate;
                     listOfExchangeactorReportStatus.Add(lateReportedExchangeActor);

                    TradeExecutionAmountViewModel NotReportExchangeActor = new TradeExecutionAmountViewModel();
                   
                    var exchangeactorReportNoReport = context.OffSiteMonitoringReportDetail.Where(x => x.OffSiteMonitoringReport.ReportTypeId == 1 &&
                                                                                     x.OffSiteMonitoringReport.ReasonTypeId == 134 &&
                                                                 (queryParameterSearch.ReportPeriodId == 0 || x.OffSiteMonitoringReport.ReportPeriodId == queryParameterSearch.ReportPeriodId) &&
                                                                (queryParameterSearch.Year == 0 || x.OffSiteMonitoringReport.Year == queryParameterSearch.Year)).Count();
                     NotReportExchangeActor.OrganizationName = (queryParameterSearch.Lang == "et") ? "ሙሉ ለሙሉ ሪፖርት ያላደረጉ የግብይት ተሳታፊዎች" : "Not Report Exchange Actors";
                     NotReportExchangeActor.Amount = exchangeactorReportNoReport;
                     listOfExchangeactorReportStatus.Add(NotReportExchangeActor);
                    

                }
                return listOfExchangeactorReportStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
