﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class OffSiteMonitoringRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public OffSiteMonitoringRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<int> CreateOffSite(OffSiteMonitoringReportDTO offSiteMonitoring)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    switch (offSiteMonitoring.CustomerTypeId)
                    {
                        case 5:
                            var offsiteMonitorings = await context.OffSiteMonitoringReport.Where(x => x.ReportPeriodId == offSiteMonitoring.ReportPeriodId &&
                                                                                                                 x.ReportTypeId == offSiteMonitoring.ReportTypeId &&
                                                                                                                 x.Year == offSiteMonitoring.Year &&
                                                                                                                  x.CustomerTypeId == offSiteMonitoring.CustomerTypeId &&
                                                                                                                 x.ReasonTypeId == offSiteMonitoring.ReasonTypeId)
                                                                                                                 .AsNoTracking()
                                                                                                                 .FirstOrDefaultAsync();
                            if (offsiteMonitorings != null)
                            {
                                return 0;
                            }
                            else
                            {
                                OffSiteMonitoringReport monitoringReport = mapper.Map<OffSiteMonitoringReport>(offSiteMonitoring);
                                context.Entry(monitoringReport).State = EntityState.Added;
                                foreach (var offSiteDetail in monitoringReport.OffSiteMonitoringReportDetail)
                                {
                                    offSiteDetail.CreatedUserId = offSiteMonitoring.CreatedUserId;
                                    context.Entry(offSiteDetail).State = EntityState.Added;
                                }
                                foreach (var workFlow in monitoringReport.WorkFlowModel)
                                {
                                    context.Entry(workFlow).State = EntityState.Added;
                                }
                                await context.SaveChangesAsync();
                                transaction.Commit();
                                return monitoringReport.OffSiteMonitoringReportId;

                            }
                           
                        default:
                            var offsiteMonitoring = await context.OffSiteMonitoringReport.Where(x => x.ReportPeriodId == offSiteMonitoring.ReportPeriodId &&
                                                                                                                  x.ReportTypeId == offSiteMonitoring.ReportTypeId &&
                                                                                                                  x.Year == offSiteMonitoring.Year &&
                                                                                                                  x.ReasonTypeId == offSiteMonitoring.ReasonTypeId)
                                                                                                                  .AsNoTracking()
                                                                                                                  .FirstOrDefaultAsync();
                            if (offsiteMonitoring != null)
                            {
                                return 0;
                            }
                            else
                            {
                                OffSiteMonitoringReport monitoringReport = mapper.Map<OffSiteMonitoringReport>(offSiteMonitoring);
                                context.Entry(monitoringReport).State = EntityState.Added;
                                foreach (var offSiteDetail in monitoringReport.OffSiteMonitoringReportDetail)
                                {
                                    offSiteDetail.CreatedUserId = offSiteMonitoring.CreatedUserId;
                                    context.Entry(offSiteDetail).State = EntityState.Added;
                                }
                                foreach (var workFlow in monitoringReport.WorkFlowModel)
                                {
                                    workFlow.CreatedDateTime = DateTime.Now;
                                    workFlow.UpdatedDateTime = DateTime.Now;
                                    workFlow.OffSiteMonitoringReportId = monitoringReport.OffSiteMonitoringReportId;
                                    context.Entry(workFlow).State = EntityState.Added;
                                }
                                await context.SaveChangesAsync();
                                transaction.Commit();
                                return monitoringReport.OffSiteMonitoringReportId;

                            }                           
                    }
                    
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
       
        }
       }
}
