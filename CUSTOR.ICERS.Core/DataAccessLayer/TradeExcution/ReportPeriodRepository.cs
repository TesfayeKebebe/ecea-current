﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class ReportPeriodRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public ReportPeriodRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }

        public async Task<IEnumerable<ReportPeriodDTO>> GetAllReportPeriod(string lang)
        {
            try
            {
                IEnumerable<ReportPeriodDTO> reportPeriod = null;
                reportPeriod = await context.ReportPeriod
                    .Select(n => new ReportPeriodDTO
                    {
                        Id = n.ReportPeriodId,
                        Description = lang == "et" ? n.DescriptionAmh : n.DescriptionEng

                    })
                    .AsNoTracking()
                    .ToListAsync();
                return reportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<ReportPeriodViewModel>> GetAllReportPeriodList(string lang)
        {

            try
            {
                var listOfReportPeriod = await context.ReportPeriod
                    .Select(n => new ReportPeriodViewModel
                    {
                        ReportPeriodId = n.ReportPeriodId,
                        DescriptionAmh = lang == "et" ? n.DescriptionAmh : n.DescriptionEng,
                        StartMonthAmh = lang == "et" ? n.StartMonthAmh : n.StartMonthEng,
                        EndMonthAmh = lang == "et" ? n.EndMonthAmh : n.EndMonthEng,
                        DeadLine = n.DeadLine,
                        StartDate = n.StartDate,
                        BeginningDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.StartDate.Date).ToString()) : n.StartDate.Date.ToString(),
                        EndDate = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.DeadLine.Date).ToString()) : n.DeadLine.Date.ToString(),
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return listOfReportPeriod;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Create(ReportPeriodSettingViewModel reportPeriod)
        {
            try
            {
                //var reportPeriod = await context.ReportPeriod.Where(x=> x.R)
                ReportPeriod newReportPeriod = mapper.Map<ReportPeriod>(mapReportperiod(reportPeriod));
                context.Add(newReportPeriod);
                await context.SaveChangesAsync();

                return newReportPeriod.ReportPeriodId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Update(ReportPeriodSettingViewModel reportPeriod)
        {
            try
            {
                var reportPrd = await context.ReportPeriod
                    .Where(x => x.ReportPeriodId == reportPeriod.ReportPeriodId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (reportPrd != null)
                {
                    reportPeriod.IsDeleted = reportPrd.IsDeleted;
                    reportPeriod.IsActive = reportPrd.IsActive;
                    reportPeriod.CreatedUserId = reportPrd.CreatedUserId;
                    reportPeriod.UpdatedDateTime = DateTime.Now;
                    reportPeriod.CreatedDateTime = reportPrd.CreatedDateTime;

                    var updateReportPeriod = mapper.Map<ReportPeriodViewModel, ReportPeriod>(mapReportperiod(reportPeriod), reportPrd);
                    context.Entry(updateReportPeriod).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return reportPrd.ReportPeriodId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> Delete(ReportPeriodViewModel reportPeriod)
        {
            try
            {
                var reportPrd = await context.ReportPeriod
                                    .Where(x => x.ReportPeriodId == reportPeriod.ReportPeriodId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (reportPrd != null)
                {
                    var deleteReportPeriod = mapper.Map(reportPeriod, reportPrd);
                    deleteReportPeriod.IsDeleted = true;
                    context.Entry(deleteReportPeriod).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return reportPeriod.ReportPeriodId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ReportPeriod> GetReportPeriodById(int reportPeriodId)
        {
            try
            {
                var reportPrd = await context.ReportPeriod
                    .Where(x => x.ReportPeriodId == reportPeriodId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ReportPeriodViewModel mapReportperiod(ReportPeriodSettingViewModel fullData)
        {

            ReportPeriodViewModel masterData = new ReportPeriodViewModel
            {
                ReportPeriodId = fullData.ReportPeriodId,
                StartDate = DateTime.Parse(fullData.StartDate),
                DeadLine = DateTime.Parse(fullData.DeadLine),
                DescriptionAmh = fullData.DescriptionAmh,
                DescriptionEng = fullData.DescriptionEng,
                StartMonthAmh = fullData.StartMonthAmh,
                StartMonthEng = fullData.StartMonthEng,
                EndMonthAmh = fullData.EndMonthAmh,
                EndMonthEng = fullData.EndMonthEng,
                EndDate = fullData.EndDate,
                BeginningDate = fullData.BeginningDate,

            };
            return masterData;
        }
    }
}
