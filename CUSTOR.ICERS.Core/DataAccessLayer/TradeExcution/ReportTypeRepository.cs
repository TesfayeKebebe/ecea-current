﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public partial class ReportTypeRepository
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public ReportTypeRepository(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }

        public async Task<int> Create(ReportTypeviewDTO ReportType)
        {
            try
            {
                //var ReportType = await context.ReportType.Where(x=> x.R)
                ReportType newReportType = mapper.Map<ReportType>(ReportType);
                context.Add(newReportType);
                await context.SaveChangesAsync();

                return newReportType.ReportTypeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<ReportTypeDTO>> GetReportTypes(string lang)
        {
            IEnumerable<ReportTypeDTO> caseStatus = null;

            try
            {
                caseStatus = await context.ReportType
                    .Select(n => new ReportTypeDTO
                    {
                        Id = n.ReportTypeId,
                        Description = lang == "et" ? n.DescriptionAmh : n.DescriptionEng
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return caseStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<ReportTypeviewDTO>> GetallReportTypes()
        {
            IEnumerable<ReportTypeviewDTO> caseStatus = null;

            try
            {
                caseStatus = await context.ReportType
                    .Select(n => new ReportTypeviewDTO
                    {
                        ReportTypeId = n.ReportTypeId,
                        DescriptionAmh = n.DescriptionAmh,
                        DescriptionEng = n.DescriptionEng
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return caseStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<ReportType> GetReportTypeById(int ReportTypeId)
        {
            try
            {
                var reportPrd = await context.ReportType
                    .Where(x => x.ReportTypeId == ReportTypeId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return reportPrd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Update(ReportTypeviewDTO ReportType)
        {
            try
            {
                var reportPrd = await context.ReportType
                    .Where(x => x.ReportTypeId == ReportType.ReportTypeId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (reportPrd != null)
                {
                    ReportType.IsDeleted = reportPrd.IsDeleted;
                    ReportType.IsActive = reportPrd.IsActive;
                    ReportType.CreatedUserId = reportPrd.CreatedUserId;
                    ReportType.UpdatedDateTime = DateTime.Now;
                    ReportType.CreatedDateTime = reportPrd.CreatedDateTime;
                    var updateReportType = mapper.Map((ReportType), reportPrd);
                    context.Entry(updateReportType).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                return reportPrd.ReportTypeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> Delete(ReportTypeviewDTO ReportType)
        {
            try
            {
                var reportPrd = await context.ReportType
                                    .Where(x => x.ReportTypeId == ReportType.ReportTypeId)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
                if (reportPrd != null)
                {
                    ReportType.IsActive = reportPrd.IsActive;
                    ReportType.CreatedUserId = reportPrd.CreatedUserId;
                    ReportType.UpdatedDateTime = DateTime.Now;
                    ReportType.CreatedDateTime = reportPrd.CreatedDateTime;
                    var deleteReportType = mapper.Map(ReportType, reportPrd);
                    deleteReportType.IsDeleted = true;
                    context.Entry(deleteReportType).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return ReportType.ReportTypeId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
