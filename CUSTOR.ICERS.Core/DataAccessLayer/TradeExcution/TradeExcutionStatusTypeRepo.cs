﻿using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class TradeExcutionStatusTypeRepo
    {
        private readonly ECEADbContext context;

        public TradeExcutionStatusTypeRepo(ECEADbContext _context)
        {
            context = _context;
        }
        public async Task<IEnumerable<TradeExcutionStatusTypeDTO>> GetExcutionStatusList(string lang)
        {
            IEnumerable<TradeExcutionStatusTypeDTO> tradeExcutionStatuses = null;
            try
            {
                tradeExcutionStatuses = await context.TradeExcutionStatusType
                                                .Select(n => new TradeExcutionStatusTypeDTO
                                                {
                                                    Id = n.TradeExcutionStatusTypeId,
                                                    Description = lang == "et" ? n.DescriptionAmh : n.DescriptionEng
                                                })
                                                 .AsNoTracking()
                                                  .ToListAsync();
                return tradeExcutionStatuses;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
