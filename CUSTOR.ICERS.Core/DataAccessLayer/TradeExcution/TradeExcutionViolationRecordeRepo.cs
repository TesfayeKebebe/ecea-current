﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class TradeExcutionViolationRecordeRepo
    {
        private readonly ECEADbContext context;
        private readonly IMapper mapper;

        public TradeExcutionViolationRecordeRepo(ECEADbContext _context, IMapper _mapper)
        {
            context = _context;
            mapper = _mapper;
        }
        public async Task<TradeExcutionViolationRecord> GetTradeViolationRecordById(int id)
        {
            try
            {
                var tradeViolation = await context.TradeExcutionViolationRecord
                          .Where(x => x.TradeExcutionViolationRecordId == id)
                          .AsNoTracking()
                          .FirstOrDefaultAsync();
                return tradeViolation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TradeExcutionViolationRecordView>> GetListOfViolationById(int exchangeActorId, string lang)
        {
            try
            {
                var tradeVilationRecord = await context.TradeExcutionViolationRecord
                    .Where(x => x.MemberTradeViolation.MemberTradeViolationId == exchangeActorId && x.MemberTradeViolation.ViolationStatus == 0 &&
                                x.Status == 0)
                    .OrderBy(o => o.CreatedDateTime)
                    .Include(k => k.MemberTradeViolation.ExchangeActor)
                    .Select(n => new TradeExcutionViolationRecordView
                    {
                        MemberTradeViolationId = n.MemberTradeViolationId,
                        TradeExcutionViolationRecordId = n.TradeExcutionViolationRecordId,
                        ReasonId = n.ReasonId,
                        ViolationRecordId = n.ViolationRecordId,
                        Remark = n.Remark,
                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.CreatedDateTime).ToString()) : n.CreatedDateTime.ToShortDateString(),
                    })
                    .AsNoTracking()
                    .ToListAsync();
                return tradeVilationRecord;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<int> GetMemberViolationById(int memberViolationId)
        {
            try
            {
                var memberViolation = await context.TradeExcutionViolationRecord
                    .Where(x => x.TradeExcutionViolationRecordId == memberViolationId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                return memberViolation.MemberTradeViolationId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<Guid> CreateViolationRecord(TradeExcutionViolationRecordDTO tradeExcutionViolation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    TradeExcutionViolationRecord newViolationRecord = mapper.Map<TradeExcutionViolationRecord>(tradeExcutionViolation);
                    context.Entry(newViolationRecord).State = EntityState.Added;
                    foreach (var clientInfo in newViolationRecord.MemberTradeViolation.WorkFlow)
                    {
                        context.Entry(clientInfo).State = EntityState.Added;
                    }
                    context.SaveChanges();
                    transaction.Commit();
                    return newViolationRecord.MemberTradeViolation.ExchangeActorId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }

        }
        public async Task<int> UpdateMemberViolation(TradeExcutionViolationRecordDTO tradeExcutionViolation)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var memberViolationInDb = await context.TradeExcutionViolationRecord
                                                      .Where(x => x.TradeExcutionViolationRecordId == tradeExcutionViolation.TradeExcutionViolationRecordId)
                                                      .AsNoTracking()
                                                      .FirstOrDefaultAsync();
                    if (memberViolationInDb != null)
                    {
                        var updateClientInfo = mapper.Map(tradeExcutionViolation, memberViolationInDb);
                        context.Entry(updateClientInfo).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                        transaction.Commit();
                    }
                    return memberViolationInDb.MemberTradeViolationId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> DeleteTradeViolation(TradeExcutionViolationRecordDTO tradeExcutionViolation)
        {
            try
            {
                var tradeDetail = await context.TradeExcutionViolationRecord
                    .Where(x => x.TradeExcutionViolationRecordId == tradeExcutionViolation.TradeExcutionViolationRecordId)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();
                if (tradeDetail != null)
                {
                    var deleteTradeExcution = mapper.Map(tradeExcutionViolation, tradeDetail);
                    context.Entry(deleteTradeExcution).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                }
                return tradeDetail.TradeExcutionViolationRecordId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<Guid> UpdateViolationRecordStatus(Guid exchangeActorId)
        {
            try
            {
                var violatedExchangeActor = await context.MemberTradeViolation
                .Where(x => x.ViolationStatus == 0 &&
                x.Status == 2 &&
                x.ExchangeActorId == exchangeActorId)
                .AsNoTracking()
                .ToListAsync();
                foreach (var rec in violatedExchangeActor)
                {
                    rec.ViolationStatus = 1;
                    rec.Status = 0;
                    context.Entry(rec).State = EntityState.Modified;
                }
                await context.SaveChangesAsync();
                return exchangeActorId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<TradeExcutionViolationRecordView>> GetListOfViolatedMembers(Guid memberClientId, string lang)
        {

            var violatedExchangeActor = await context.TradeExcutionViolationRecord
                .Where(x => x.MemberTradeViolation.ViolationStatus == 0 &&
                             x.MemberTradeViolation.ExchangeActorId == memberClientId)

                .Select(b => new TradeExcutionViolationRecordView
                {
                    ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                    OrganizationName = (lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                           b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                    CustomerType = (lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                       b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                    ReasonId = b.ReasonId,
                    CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                    ViolationRecordId = b.ViolationRecordId,
                    Remark = b.Remark
                })
                .AsNoTracking()
                .ToListAsync();


            return violatedExchangeActor;
        }

        public async Task<List<TradeExcutionViolationRecordView>> GetMembersViolationDecisionList(Guid memberClientId, string lang)
        {

            var violatedExchangeActor = await context.TradeExcutionViolationRecord
                .Where(x => x.MemberTradeViolation.ViolationStatus == 0 &&
                             x.MemberTradeViolation.Status == 2 &&
                             x.ApproverDecision == 2 &&
                             x.MemberTradeViolation.ExchangeActorId == memberClientId)

                .Select(b => new TradeExcutionViolationRecordView
                {
                    ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                    TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                    MemberTradeViolationId = b.MemberTradeViolationId,
                    OrganizationName = (lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                           b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                    CustomerType = (lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                       b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                    ReasonId = b.ReasonId,
                    CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                    ViolationRecordId = b.ViolationRecordId,
                    Remark = b.Remark,
                    ApprovedBy = b.ApprovedBy,
                    ApprovedDate = b.ApprovedDate,
                    ApproverDecision = b.ApproverDecision,
                    ApproverRemark = b.ApproverRemark,
                    CheckedBy = b.CheckedBy,
                    CheckedDate = b.CheckedDate,
                    CheckerDecision = b.CheckerDecision,
                    CheckerRemark = b.CheckerRemark,
                    PreparedBy = b.PreparedBy,
                    Status = b.Status,
                    CreatedDateTime = b.CreatedDateTime
                })
                .AsNoTracking()
                .ToListAsync();
            return violatedExchangeActor;
        }
        public async Task<List<TradeExcutionViolationRecordView>> GetMembersOffSiteDecisionList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var violatedExchangeActor = await context.TradeExcutionViolationRecord
               .Where(x => x.MemberTradeViolation.ExchangeActorId == notificationQuery.ExchangeActorId &&
                           x.ReportAnalysisId == notificationQuery.ReportAnalysisId &&
                           x.Year == notificationQuery.Year &&
                           x.ReportPeriodId == notificationQuery.ReportPeriodId &&
                           x.ReportTypeId == notificationQuery.ReportTypeId)

               .Select(b => new TradeExcutionViolationRecordView
               {
                   ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                   TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                   MemberTradeViolationId = b.MemberTradeViolationId,
                   OrganizationName = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                   CustomerType = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                      b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                   ReasonId = b.ReasonId,
                   CreatedDate = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                   ViolationRecordId = b.ViolationRecordId,
                   Remark = b.Remark,
                   ApprovedBy = b.ApprovedBy,
                   ApprovedDate = b.ApprovedDate,
                   ApproverDecision = b.ApproverDecision,
                   ApproverRemark = b.ApproverRemark,
                   CheckedBy = b.CheckedBy,
                   CheckedDate = b.CheckedDate,
                   CheckerDecision = b.CheckerDecision,
                   CheckerRemark = b.CheckerRemark,
                   PreparedBy = b.PreparedBy,
                   Status = b.Status,
                   CreatedDateTime = b.CreatedDateTime
               })
               .AsNoTracking()
               .ToListAsync();
                return violatedExchangeActor;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionPreparedList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var violatedExchangeActor = await context.TradeExcutionViolationRecord
               .Where(x => x.MemberTradeViolation.ExchangeActorId == notificationQuery.ExchangeActorId &&
                           x.ReportAnalysisId == notificationQuery.ReportAnalysisId &&
                           x.Year == notificationQuery.Year &&
                           x.Status == 0 &&
                           x.ReportPeriodId == notificationQuery.ReportPeriodId &&
                           x.ReportTypeId == notificationQuery.ReportTypeId)

               .Select(b => new TradeExcutionViolationRecordView
               {
                   ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                   TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                   MemberTradeViolationId = b.MemberTradeViolationId,
                   OrganizationName = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                   CustomerType = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                      b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                   ReasonId = b.ReasonId,
                   CreatedDate = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                   ViolationRecordId = b.ViolationRecordId,
                   Remark = b.Remark,
                   ApprovedBy = b.ApprovedBy,
                   ApprovedDate = b.ApprovedDate,
                   ApproverDecision = b.ApproverDecision,
                   ApproverRemark = b.ApproverRemark,
                   CheckedBy = b.CheckedBy,
                   CheckedDate = b.CheckedDate,
                   CheckerDecision = b.CheckerDecision,
                   CheckerRemark = b.CheckerRemark,
                   PreparedBy = b.PreparedBy,
                   Status = b.Status,
                   CreatedDateTime = b.CreatedDateTime
               })
               .AsNoTracking()
               .ToListAsync();
                return violatedExchangeActor;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionOfficerList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var violatedExchangeActor = await context.TradeExcutionViolationRecord
               .Where(x => x.MemberTradeViolation.ExchangeActorId == notificationQuery.ExchangeActorId &&                           
                           x.Status == 0 )

               .Select(b => new TradeExcutionViolationRecordView
               {
                   ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                   TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                   MemberTradeViolationId = b.MemberTradeViolationId,
                   OrganizationName = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                   CustomerType = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                      b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                   ReasonId = b.ReasonId,
                   CreatedDate = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                   ViolationRecordId = b.ViolationRecordId,
                   Remark = b.Remark,
                   ApprovedBy = b.ApprovedBy,
                   ApprovedDate = b.ApprovedDate,
                   ApproverDecision = b.ApproverDecision,
                   ApproverRemark = b.ApproverRemark,
                   CheckedBy = b.CheckedBy,
                   CheckedDate = b.CheckedDate,
                   CheckerDecision = b.CheckerDecision,
                   CheckerRemark = b.CheckerRemark,
                   PreparedBy = b.PreparedBy,
                   Status = b.Status,
                   CreatedDateTime = b.CreatedDateTime
               })
               .AsNoTracking()
               .ToListAsync();
                return violatedExchangeActor;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteDecisionApproverList(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var violatedExchangeActor = await context.TradeExcutionViolationRecord
               .Where(x => x.MemberTradeViolation.ExchangeActorId == notificationQuery.ExchangeActorId &&
                           x.ReportAnalysisId == notificationQuery.ReportAnalysisId &&
                           x.Year == notificationQuery.Year &&
                           (x.Status == 0 || x.Status == 1) &&
                           x.ReportPeriodId == notificationQuery.ReportPeriodId &&
                           x.ReportTypeId == notificationQuery.ReportTypeId)

               .Select(b => new TradeExcutionViolationRecordView
               {
                   ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                   TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                   MemberTradeViolationId = b.MemberTradeViolationId,
                   OrganizationName = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                   CustomerType = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                      b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                   ReasonId = b.ReasonId,
                   CreatedDate = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                   ViolationRecordId = b.ViolationRecordId,
                   Remark = b.Remark,
                   ApprovedBy = b.ApprovedBy,
                   ApprovedDate = b.ApprovedDate,
                   ApproverDecision = b.ApproverDecision,
                   ApproverRemark = b.ApproverRemark,
                   CheckedBy = b.CheckedBy,
                   CheckedDate = b.CheckedDate,
                   CheckerDecision = b.CheckerDecision,
                   CheckerRemark = b.CheckerRemark,
                   PreparedBy = b.PreparedBy,
                   Status = b.Status,
                   CreatedDateTime = b.CreatedDateTime,
                   ReportAnalysisId = b.ReportAnalysisId,
                   ReportPeriodId = b.ReportPeriodId,
                   ReportTypeId = b.ReportTypeId,
                   Year = b.Year
               })
               .AsNoTracking()
               .ToListAsync();
                return violatedExchangeActor;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<TradeExcutionViolationRecordView>> GetOffSiteTeamLeaderDecision(OffSiteMonitoringQueryParameters notificationQuery)
        {
            try
            {
                var violatedExchangeActor = await context.TradeExcutionViolationRecord
               .Where(x => x.MemberTradeViolation.ExchangeActorId == notificationQuery.ExchangeActorId &&
                           x.ReportAnalysisId == notificationQuery.ReportAnalysisId &&
                           x.Year == notificationQuery.Year &&
                           x.Status == 1 &&
                           x.ReportPeriodId == notificationQuery.ReportPeriodId &&
                           x.ReportTypeId == notificationQuery.ReportTypeId)

               .Select(b => new TradeExcutionViolationRecordView
               {
                   ExchangeActorId = b.MemberTradeViolation.ExchangeActorId,
                   TradeExcutionViolationRecordId = b.TradeExcutionViolationRecordId,
                   MemberTradeViolationId = b.MemberTradeViolationId,
                   OrganizationName = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.OrganizationNameAmh :
                                                                          b.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                   CustomerType = (notificationQuery.Lang == "et") ? b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionAmh :
                                                                      b.MemberTradeViolation.ExchangeActor.MemberCategory.DescriptionEng,
                   ReasonId = b.ReasonId,
                   CreatedDate = (notificationQuery.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(b.MemberTradeViolation.CreatedDateTime).ToString()) : b.MemberTradeViolation.CreatedDateTime.ToShortDateString(),
                   ViolationRecordId = b.ViolationRecordId,
                   Remark = b.Remark,
                   ApprovedBy = b.ApprovedBy,
                   ApprovedDate = b.ApprovedDate,
                   ApproverDecision = b.ApproverDecision,
                   ApproverRemark = b.ApproverRemark,
                   CheckedBy = b.CheckedBy,
                   CheckedDate = b.CheckedDate,
                   CheckerDecision = b.CheckerDecision,
                   CheckerRemark = b.CheckerRemark,
                   PreparedBy = b.PreparedBy,
                   Status = b.Status,
                   CreatedDateTime = b.CreatedDateTime
               })
               .AsNoTracking()
               .ToListAsync();
                return violatedExchangeActor;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<StaticData4>> GetListOfExchangeActor(string lang)
        {

            var violatedExchangeActor = await context.TradeExcutionViolationRecord
                .Where(x => x.MemberTradeViolation.ViolationStatus == 0)
                .GroupBy(r => r.MemberTradeViolation.ExchangeActorId)
                .Select(b => new
                {
                    Id = b.Key,
                    Count = b.Count()
                })
                .AsNoTracking()
                .ToListAsync();
            var violatRepo = await (from exa in context.ExchangeActor
                                    join x in violatedExchangeActor
                                    on exa.ExchangeActorId equals x.Id
                                    where x.Count > 1
                                    select new StaticData4
                                    {
                                        Id = exa.ExchangeActorId,
                                        Description = (lang == "et") ? exa.OrganizationNameAmh :
                                                                          exa.OrganizationNameEng,

                                    })
                                 .AsNoTracking()
                                 .ToListAsync();


            return violatRepo;
        }
        public async Task<List<TradeExcutionViolationRecordView>> GetMemberViolationSearchByCriteriaAmh(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                var memberViolation = await context.TradeExcutionViolationRecord.Where(
                                       x => x.Status == 2 && x.ApproverDecision == 2 &&
                                       (string.IsNullOrEmpty(clientTrade.ECXCode) || x.MemberTradeViolation.ExchangeActor.Ecxcode == clientTrade.ECXCode) &&
                                       (string.IsNullOrEmpty(clientTrade.From) || x.MemberTradeViolation.CreatedDateTime >= DateTime.Parse(clientTrade.From)) &&
                                      (string.IsNullOrEmpty(clientTrade.To) || x.MemberTradeViolation.CreatedDateTime <= DateTime.Parse(clientTrade.To)) &&
                                      (clientTrade.ViolationTypeId == 0 || x.ReasonId == clientTrade.ViolationTypeId) &&
                                      (clientTrade.DecisionTypeId == 0 || x.ViolationRecordId == clientTrade.DecisionTypeId))
                                      .Select(n => new TradeExcutionViolationRecordView
                                      {
                                          ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,
                                          CustomerType = n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionAmh,
                                          OrganizationName = n.MemberTradeViolation.ExchangeActor.OrganizationNameAmh,
                                          CreatedDate = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(n.MemberTradeViolation.CreatedDateTime.Date).ToString()),
                                          Remark = n.Remark,
                                          ReasonId = n.ReasonId,
                                          ViolationRecordId = n.ViolationRecordId,
                                          ApprovedBy = n.ApprovedBy,
                                          ApprovedDate = n.ApprovedDate,
                                          ApproverDecision = n.ApproverDecision,
                                          ApproverRemark = n.ApproverRemark,
                                          CheckedBy = n.CheckedBy,
                                          CheckedDate = n.CheckedDate,
                                          CheckerDecision = n.CheckerDecision,
                                          CheckerRemark = n.CheckerRemark,
                                          PreparedBy = n.PreparedBy,
                                          Status = n.Status,
                                          CreatedDateTime = n.CreatedDateTime,
                                      })
                                      .AsNoTracking()
                                      .ToListAsync();
                return memberViolation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<TradeExcutionViolationRecordView>> GetMemberViolationSearchByCriteriaEng(ClientTradeQueryParameters clientTrade)
        {
            try
            {
                var memberViolation = await context.TradeExcutionViolationRecord.Where(
                                       x => x.Status == 2 && x.ApproverDecision == 2 &&
                                       (string.IsNullOrEmpty(clientTrade.ECXCode) || x.MemberTradeViolation.ExchangeActor.Ecxcode == clientTrade.ECXCode) &&
                                       (string.IsNullOrEmpty(clientTrade.From) || x.MemberTradeViolation.CreatedDateTime >= DateTime.Parse(clientTrade.From)) &&
                                      (string.IsNullOrEmpty(clientTrade.To) || x.MemberTradeViolation.CreatedDateTime <= DateTime.Parse(clientTrade.To)) &&
                                      (clientTrade.ViolationTypeId == 0 || x.ReasonId == clientTrade.ViolationTypeId) &&
                                      (clientTrade.DecisionTypeId == 0 || x.ViolationRecordId == clientTrade.DecisionTypeId))
                                      .Select(n => new TradeExcutionViolationRecordView
                                      {
                                          ExchangeActorId = n.MemberTradeViolation.ExchangeActorId,
                                          CustomerType = n.MemberTradeViolation.ExchangeActor.CustomerType.DescriptionEng,
                                          OrganizationName = n.MemberTradeViolation.ExchangeActor.OrganizationNameEng,
                                          CreatedDate = n.MemberTradeViolation.CreatedDateTime.Date.ToString(),
                                          Remark = n.Remark,
                                          ReasonId = n.ReasonId,
                                          ViolationRecordId = n.ViolationRecordId,
                                          ApprovedBy = n.ApprovedBy,
                                          ApprovedDate = n.ApprovedDate,
                                          ApproverDecision = n.ApproverDecision,
                                          ApproverRemark = n.ApproverRemark,
                                          CheckedBy = n.CheckedBy,
                                          CheckedDate = n.CheckedDate,
                                          CheckerDecision = n.CheckerDecision,
                                          CheckerRemark = n.CheckerRemark,
                                          PreparedBy = n.PreparedBy,
                                          Status = n.Status,
                                          CreatedDateTime = n.CreatedDateTime

                                      })
                                      .AsNoTracking()
                                      .ToListAsync();
                return memberViolation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<TradeExcutionViolationRecordView>> GetMemberViolationByCriteria(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                List<TradeExcutionViolationRecordView> tradeList = null;
                if (clientTradeQueryParameters.Lang == "et")
                {
                    tradeList = await GetMemberViolationSearchByCriteriaAmh(clientTradeQueryParameters);
                }
                else
                {
                    tradeList = await GetMemberViolationSearchByCriteriaEng(clientTradeQueryParameters);
                }
                return tradeList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> GetMemberViolationTotalNumber(ClientTradeQueryParameters clientTradeQueryParameters)
        {
            try
            {
                var tradeList = await context.TradeExcutionViolationRecord.Where(x => x.MemberTradeViolation.ExchangeActorId == clientTradeQueryParameters.ExchangeActorId &&
                                                                                     x.Year == clientTradeQueryParameters.Year &&
                                                                                     x.ReportTypeId == clientTradeQueryParameters.ReportTypeId &&
                                                                                     x.ReportAnalysisId == clientTradeQueryParameters.ReportAnalysisId &&
                                                                                     x.ReportPeriodId == clientTradeQueryParameters.ReportPeriodId &&
                                                                                     (x.Status == 0 || x.Status == 1 || x.Status == 2))
                                                                          .AsNoTracking()
                                                                          .ToListAsync();
                return tradeList.Count();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
