﻿using CUSTOR.ICERS.Core.EntityLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.TradeExcution
{
    public class UnitMeasurementRepository
    {
        private readonly ECEADbContext context;

        public UnitMeasurementRepository(ECEADbContext _context)
        {
            context = _context;
        }
        public async Task<IEnumerable<StaticData>> GetUserMeasurementList(string lang)
        {
            IEnumerable<StaticData> unitMeasurements = null;
            try
            {
                unitMeasurements = await context.UnitMeasurement
                                          .Select(x => new StaticData
                                          {
                                              Id = x.UnitMeasurementId,
                                              Description = lang == "et" ? x.DescriptionAmh : x.DescriptionEng

                                          })
                                          .AsNoTracking()
                                          .ToListAsync();
                return unitMeasurements;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



    }
}
