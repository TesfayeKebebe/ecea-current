﻿using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class TradeTrendRepository
    {
        private readonly ECEADbContext _context;
        private readonly TradeTrendSettings _settings;
        public TradeTrendRepository(ECEADbContext context, IOptions<TradeTrendSettings> settings)
        {
            _context = context;
            _settings = settings.Value;
        }
        public async Task<IEnumerable<TradeTrend>> GetFilteredStockAsync(TrendQueryParameters qParam)
        {
            try
            {
                IEnumerable<TradeTrend> query = await _context.TradeTrend
                .Where(x => x.ProductType == qParam.ProductType && x.Symbol == qParam.Symbol && (x.TradeDate >= qParam.DateFrom && x.TradeDate <= qParam.DateTo))
                .OrderByDescending(d => d.TradeDate)
                .ToListAsync();
                return query;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw new Exception(ex.Message);
            }

        }
        public async Task<PagedResult<TradeTrendView>> GetFilteredStockWithPagingAsync(TrendQueryParameters qParam)
        {
            var query = await _context.TradeTrend
                .Where(x => x.CommodityId == qParam.CommodityId && (x.TradeDate.Date >= qParam.DateFrom.Date &&
                x.TradeDate.Date <= qParam.DateTo.Date))
                .Select(n => new TradeTrendView
                {
                    Symbol = n.Symbol,
                    Warehouse = n.Warehouse,
                    PreviousClosingPrice = n.Open,
                    ClosingPrice = n.Close,
                    ProductionYear = n.ProductionYear,
                    Differnce = n.Close - n.Open,
                    High = n.High,
                    Low = n.Low,
                    Volume = n.Volume,
                    TradeDate = n.TradeDate
                })
                .Paging(qParam.PageCount, qParam.PageNumber)
                .OrderByDescending(d => d.TradeDate)
                .ToListAsync();


            var cnt = Count(qParam);
            return new PagedResult<TradeTrendView>()
            {
                Items = query,
                ItemsCount = cnt
            };

        }
        public int Count(TrendQueryParameters qParam)
        {
            //to-do find a better way
            var query = _context.TradeTrend
                .Where(x => x.ProductType == qParam.ProductType && x.Symbol == qParam.Symbol && (x.TradeDate >= qParam.DateFrom && x.TradeDate <= qParam.DateTo))
                .ToList();
            return query.Count();
        }
        public async Task<IEnumerable<TradeTrend>> GetTradeTrend()
        {
            return await _context.TradeTrend
                    .AsNoTracking()
                    .ToListAsync();
        }
        public async Task<IEnumerable<TradeTrend>> GetTradeTrendByDate(DateTime dtFrom, DateTime dtTo)
        {
            return await _context.TradeTrend
                    .Where(x => x.TradeDate >= dtTo && x.TradeDate <= dtTo)
                    .AsNoTracking()
                    .ToListAsync();
        }
        public async Task<IEnumerable<TradeTrend>> GetTradeTrendByProductAndDate(string product, DateTime dtFrom, DateTime dtTo)
        {
            return await _context.TradeTrend
                    .Where(x => x.TradeDate >= dtTo && x.TradeDate <= dtTo && x.ProductType == product)
                    .AsNoTracking()
                    .ToListAsync();
        }
        public async Task<IEnumerable<TradeTrend>> GetTradeTrendByProductAndSymbol(string product, string symbol, DateTime dtFrom, DateTime dtTo)
        {
            return await _context.TradeTrend
                    .Where(x => x.TradeDate >= dtTo && x.TradeDate <= dtTo && x.ProductType == product && x.Symbol == symbol)
                    .AsNoTracking()
                    .ToListAsync();
        }
    }
}
