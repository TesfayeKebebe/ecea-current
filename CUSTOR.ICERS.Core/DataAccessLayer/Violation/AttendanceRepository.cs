using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class AttendanceRepository
    {
        public readonly ECXTradeDbContext _context;
        public AttendanceRepository(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<AttendanceVM>> getAttendance(DateTime From, DateTime To, bool IsTrade, string CommodityGradeId, Guid commodityId)
        {
            try
            {
                List<AttendanceVM> attendances = new List<AttendanceVM>();
                if (IsTrade && CommodityGradeId != "null")
                {
                    attendances = await _context.AttendanceVM.FromSqlRaw<AttendanceVM>(QueryConfiguration.GetQuery(Constants.AttendanceOfTradeWithCommodity), From, To, CommodityGradeId, commodityId).ToListAsync();

                }
                else if (IsTrade)
                {
                    attendances = await _context.AttendanceVM.FromSqlRaw<AttendanceVM>(QueryConfiguration.GetQuery(Constants.AttendanceOfTrade), From, To, commodityId).ToListAsync();
                }
                else
                {
                    attendances = await _context.AttendanceVM.FromSqlRaw<AttendanceVM>(QueryConfiguration.GetQuery(Constants.AttendanceByDate), From, To, CommodityGradeId, commodityId).ToListAsync();

                }
                return attendances;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<AttendanceBuyerSellerViewModel>> GetAttendanceBuyerSellerList(bool IsTrade, DateTime From, DateTime To, Guid CommodityId)
        {
            try
            {
                List<AttendanceBuyerSellerRatio> attendances = new List<AttendanceBuyerSellerRatio>();
                attendances = await _context.AttendanceBuyerSellerRatio.FromSqlRaw<AttendanceBuyerSellerRatio>(QueryConfiguration.GetQuery(Constants.BuyerSellerRatio), IsTrade, From, To, CommodityId).ToListAsync();
                var buyerSellerRatio = attendances.Select(n => new AttendanceBuyerSellerViewModel
                {
                    Buyer = n.Buyer,
                    Seller = n.Seller,
                    ClientBuyer = n.ClientBuyer,
                    ClientSeller = n.ClientSeller,
                    ClientRatio = n.ClientBuyer / n.ClientSeller,
                    Ratio = n.Buyer / n.Seller
                }).ToList();
                return buyerSellerRatio;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<AttendanceVolumeValueRatioView>> GetAttendanceVolumeValueList(bool IsTrade, DateTime From, DateTime To, Guid CommodityId)
        {
            try
            {
                List<AttendanceVolumeValueRatioView> attendanceVolumeValues = new List<AttendanceVolumeValueRatioView>();
                AttendanceVolumeValueRatioView attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                if (IsTrade == false)
                {
                    List<AttendanceVolumeValueRatio> volumeValueRatios = new List<AttendanceVolumeValueRatio>();
                    volumeValueRatios = await _context.AttendanceVolumeValueRatio.FromSqlRaw<AttendanceVolumeValueRatio>(QueryConfiguration.GetQuery(Constants.AttendanceVolumeValueRatio), From, To, CommodityId).ToListAsync();
                    attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                    attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Quantity);
                    attendanceVolumeValue.BuyerMemberPrice = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Price) * attendanceVolumeValue.BuyerMemberQuantity;
                    attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 2).Sum(z => z.Quantity);
                    attendanceVolumeValue.SellerMemberPrice = volumeValueRatios.Where(c => c.IsClientOrder == false && c.TransactionType == 2).Sum(s => s.Price) * attendanceVolumeValue.SellerMemberQuantity;
                    attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Quantity);
                    attendanceVolumeValue.BuyerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Price) * attendanceVolumeValue.BuyerClientQuantity;
                    attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Quantity);
                    attendanceVolumeValue.SellerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Price) * attendanceVolumeValue.SellerClientQuantity;
                    attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberQuantity / attendanceVolumeValue.BuyerMemberPrice;
                    attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                    attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                    attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                    attendanceVolumeValues.Add(attendanceVolumeValue);
                }
                else
                {

                    List<TradeAttendanceBuyerSellerVolumeValueRatio> volumeValueRatios = new List<TradeAttendanceBuyerSellerVolumeValueRatio>();
                    volumeValueRatios = await _context.TradeAttendanceBuyerSellerVolumeValueRatio.FromSqlRaw<TradeAttendanceBuyerSellerVolumeValueRatio>(QueryConfiguration.GetQuery(Constants.TradeAttendanceVolumeValueRatio), CommodityId, From, To).ToListAsync();
                    var totalListOfAttended = volumeValueRatios.Select(n => new { n.Commodity }).Distinct();
                    foreach (var item in totalListOfAttended)
                    {
                        attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                        attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerQuantity);
                        var buyerMemberPri = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerPrice);
                        attendanceVolumeValue.BuyerMemberPrice = buyerMemberPri * attendanceVolumeValue.BuyerMemberQuantity;
                        attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerQuantity);
                        var memberSellerQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerPrice);
                        attendanceVolumeValue.SellerMemberPrice = memberSellerQuantity * attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerQuantity);
                        var clientBuyerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerPrice);
                        attendanceVolumeValue.BuyerClientPrice = clientBuyerPrice * attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerQuantity);
                        var clientSellerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerPrice); ;
                        attendanceVolumeValue.SellerClientPrice = clientSellerPrice * attendanceVolumeValue.SellerClientQuantity;
                        attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberPrice / attendanceVolumeValue.BuyerMemberQuantity;
                        attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                        attendanceVolumeValues.Add(attendanceVolumeValue);
                    }

                }

                return attendanceVolumeValues;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<AttendanceRatio>> GetAttendanceRatioList(bool IsClient, bool IsTrade, DateTime From, DateTime To, Guid CommodityId)
        {
            try
            {

                AttendanceBuyerSellerRatio attendances = new AttendanceBuyerSellerRatio();
                attendances =  _context.AttendanceBuyerSellerRatio.FromSqlRaw<AttendanceBuyerSellerRatio>(QueryConfiguration.GetQuery(Constants.BuyerSellerRatio), IsTrade, From, To, CommodityId).AsNoTracking().AsEnumerable().FirstOrDefault();
                List<AttendanceRatio> attendanceRatiosList = new List<AttendanceRatio>();
                if (IsTrade == false)
                {
                    List<AttendanceVolumeValueRatio> volumeValueRatios = new List<AttendanceVolumeValueRatio>();
                    volumeValueRatios = await _context.AttendanceVolumeValueRatio.FromSqlRaw<AttendanceVolumeValueRatio>(QueryConfiguration.GetQuery(Constants.AttendanceVolumeValueRatio), From, To, CommodityId).ToListAsync();
                    if (IsClient == true)
                    {
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        attendanceRatio.BuyerSellerRatio = (attendances.ClientBuyer / attendances.ClientSeller);
                        AttendanceVolumeValueRatioView attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                        attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Quantity);
                        attendanceVolumeValue.BuyerMemberPrice = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Price) * attendanceVolumeValue.BuyerMemberQuantity;
                        attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 2).Sum(z => z.Quantity);
                        attendanceVolumeValue.SellerMemberPrice = volumeValueRatios.Where(c => c.IsClientOrder == false && c.TransactionType == 2).Sum(s => s.Price) * attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Quantity);
                        attendanceVolumeValue.BuyerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Price) * attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Quantity);
                        attendanceVolumeValue.SellerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Price) * attendanceVolumeValue.SellerClientQuantity;
                        attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberQuantity / attendanceVolumeValue.BuyerMemberPrice;
                        attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                        attendanceRatio.VolumeValueRatio = attendanceVolumeValue.BuyerClientRatio + attendanceVolumeValue.SellerClientRatio;
                        attendanceRatiosList.Add(attendanceRatio);
                    }
                    else
                    {
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        attendanceRatio.BuyerSellerRatio = (attendances.ClientBuyer / attendances.ClientSeller);
                        AttendanceVolumeValueRatioView attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                        attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Quantity);
                        attendanceVolumeValue.BuyerMemberPrice = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 1).Sum(y => y.Price) * attendanceVolumeValue.BuyerMemberQuantity;
                        attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(n => n.IsClientOrder == false && n.TransactionType == 2).Sum(z => z.Quantity);
                        attendanceVolumeValue.SellerMemberPrice = volumeValueRatios.Where(c => c.IsClientOrder == false && c.TransactionType == 2).Sum(s => s.Price) * attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Quantity);
                        attendanceVolumeValue.BuyerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 1).Sum(d => d.Price) * attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Quantity);
                        attendanceVolumeValue.SellerClientPrice = volumeValueRatios.Where(e => e.IsClientOrder == true && e.TransactionType == 2).Sum(d => d.Price) * attendanceVolumeValue.SellerClientQuantity;
                        attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberQuantity / attendanceVolumeValue.BuyerMemberPrice;
                        attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                        attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                        attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                        attendanceRatio.VolumeValueRatio = attendanceVolumeValue.BuyerMemberRatio + attendanceVolumeValue.SellerMemberRatio;
                        attendanceRatiosList.Add(attendanceRatio);
                    }

                }
                else
                {
                    AttendanceVolumeValueRatioView attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                    List<TradeAttendanceBuyerSellerVolumeValueRatio> volumeValueRatios = new List<TradeAttendanceBuyerSellerVolumeValueRatio>();
                    volumeValueRatios = await _context.TradeAttendanceBuyerSellerVolumeValueRatio.FromSqlRaw<TradeAttendanceBuyerSellerVolumeValueRatio>(QueryConfiguration.GetQuery(Constants.TradeAttendanceVolumeValueRatio), CommodityId, From, To).ToListAsync();
                    if (IsClient == true)
                    {
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        var totlalAttendance = volumeValueRatios.Select(n => new { n.Commodity }).Distinct();
                        foreach (var item in totlalAttendance)
                        {
                            attendanceRatio = new AttendanceRatio();
                            attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                            attendanceRatio.BuyerSellerRatio = (attendances.ClientBuyer / attendances.ClientSeller);
                            // AttendanceVolumeValueRatioView attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                            attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerQuantity);
                            var memberBuyerQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerPrice);
                            attendanceVolumeValue.BuyerMemberPrice = memberBuyerQuantity * attendanceVolumeValue.BuyerMemberQuantity;
                            attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerQuantity);
                            var memberSellerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerPrice);
                            attendanceVolumeValue.SellerMemberPrice = memberBuyerQuantity * attendanceVolumeValue.SellerMemberQuantity;
                            attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerQuantity);
                            var clientBuyerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerPrice);
                            attendanceVolumeValue.BuyerClientPrice = clientBuyerPrice * attendanceVolumeValue.BuyerClientQuantity;
                            attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerQuantity);
                            var clientSellerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerPrice);
                            attendanceVolumeValue.SellerClientPrice = clientSellerPrice * attendanceVolumeValue.SellerClientQuantity;
                            attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberPrice / attendanceVolumeValue.BuyerMemberQuantity;
                            attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                            attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                            attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                            attendanceRatio.VolumeValueRatio = attendanceVolumeValue.BuyerClientRatio + attendanceVolumeValue.SellerClientRatio;
                            attendanceRatiosList.Add(attendanceRatio);
                        }

                    }
                    else
                    {
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        var totlalAttendance = volumeValueRatios.Select(n => new { n.Commodity }).Distinct();
                        foreach (var item in totlalAttendance)
                        {
                            attendanceRatio = new AttendanceRatio();
                            attendanceVolumeValue = new AttendanceVolumeValueRatioView();
                            attendanceRatio.BuyerSellerRatio = (attendances.ClientBuyer / attendances.ClientSeller);
                            attendanceVolumeValue.BuyerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerQuantity);
                            var memberBuyerQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == false).Sum(y => y.BuyerPrice);
                            attendanceVolumeValue.BuyerMemberPrice = memberBuyerQuantity * attendanceVolumeValue.BuyerMemberQuantity;
                            attendanceVolumeValue.SellerMemberQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerQuantity);
                            var memberSellerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == false).Sum(y => y.SellerPrice);
                            attendanceVolumeValue.SellerMemberPrice = memberBuyerQuantity * attendanceVolumeValue.SellerMemberQuantity;
                            attendanceVolumeValue.BuyerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerQuantity);
                            var clientBuyerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.BuyerIsClient == true).Sum(y => y.BuyerPrice);
                            attendanceVolumeValue.BuyerClientPrice = clientBuyerPrice * attendanceVolumeValue.BuyerClientQuantity;
                            attendanceVolumeValue.SellerClientQuantity = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerQuantity);
                            var clientSellerPrice = volumeValueRatios.Where(x => x.Commodity == item.Commodity && x.SellerIsClient == true).Sum(y => y.SellerPrice);
                            attendanceVolumeValue.BuyerMemberRatio = attendanceVolumeValue.BuyerMemberPrice / attendanceVolumeValue.BuyerMemberQuantity;
                            attendanceVolumeValue.SellerMemberRatio = attendanceVolumeValue.SellerMemberPrice / attendanceVolumeValue.SellerMemberQuantity;
                            attendanceVolumeValue.BuyerClientRatio = attendanceVolumeValue.BuyerClientPrice / attendanceVolumeValue.BuyerClientQuantity;
                            attendanceVolumeValue.SellerClientRatio = attendanceVolumeValue.SellerClientPrice / attendanceVolumeValue.SellerClientQuantity;
                            attendanceRatio.VolumeValueRatio = attendanceVolumeValue.BuyerMemberRatio + attendanceVolumeValue.SellerMemberRatio;
                            attendanceRatiosList.Add(attendanceRatio);
                        }

                    }

                }

                return attendanceRatiosList;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<AttendanceRatio>> GetAttendanceRatioGraphList(bool IsTrade, DateTime From, DateTime To, Guid CommodityId)
        {
            try
            {
                List<AttendanceRatio> attendanceRatiosList = new List<AttendanceRatio>();
                if (IsTrade == false)
                {
                    List<OrderAttendanceSellerBuyer> orderAttendances = new List<OrderAttendanceSellerBuyer>();
                    orderAttendances = await _context.OrderAttendanceSellerBuyer.FromSqlRaw<OrderAttendanceSellerBuyer>(QueryConfiguration.GetQuery(Constants.OrderAttendanceRatioGraph), From, To, CommodityId).ToListAsync();
                    if (orderAttendances != null)
                    {
                        var totlalAttendance = orderAttendances.Select(n => new { n.OrderDate.Date }).Distinct();
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        foreach (var item in totlalAttendance)
                        {
                            attendanceRatio = new AttendanceRatio();
                            var buyer = orderAttendances.Where(x => x.TransactionType == 1 && x.OrderDate.Date == item.Date).Count();
                            var seller = orderAttendances.Where(x => x.TransactionType == 2 && x.OrderDate.Date == item.Date).Count();

                            var quantity = orderAttendances.Where(x => x.OrderDate.Date == item.Date).Sum(x => x.Quantity);
                            var price = orderAttendances.Where(x => x.OrderDate.Date == item.Date).Sum(x => x.Price);
                            var value = quantity * price;

                            attendanceRatio.BuyerSellerRatio = buyer / seller;
                            attendanceRatio.Date = item.Date.ToShortDateString();
                            attendanceRatio.VolumeValueRatio = Convert.ToDecimal(String.Format("{0:00.0000}", (value / quantity)));
                            attendanceRatiosList.Add(attendanceRatio);
                        }

                    }

                }
                else
                {

                    List<TradeAttendanceBuyerSeller> tradeAttendances = new List<TradeAttendanceBuyerSeller>();
                    tradeAttendances = await _context.TradeAttendanceBuyerSeller.FromSqlRaw<TradeAttendanceBuyerSeller>(QueryConfiguration.GetQuery(Constants.TradeAttendanceRatioGraph), From, To, CommodityId).ToListAsync();
                    if (tradeAttendances != null)
                    {
                        var totlalAttendance = tradeAttendances.Select(n => new { n.TradeDate.Date }).Distinct();
                        AttendanceRatio attendanceRatio = new AttendanceRatio();
                        foreach (var item in totlalAttendance)
                        {
                            attendanceRatio = new AttendanceRatio();
                            var buySell = tradeAttendances.Where(x => x.TradeDate.Date == item.Date).ToList();
                            var buyer = buySell.Select(x => x.BuyerId).Count();
                            var seller = buySell.Select(x => x.SellerId).Distinct().Count();

                            var volume = buySell.Sum(x => x.Quantity);
                            var price = buySell.Sum(x => x.Price);
                            var value = volume * price;

                            attendanceRatio.BuyerSellerRatio = buyer / seller;
                            attendanceRatio.Date = item.Date.ToShortDateString();
                            attendanceRatio.VolumeValueRatio = Convert.ToDecimal(String.Format("{0:00.0000}", (value / volume)));
                            attendanceRatiosList.Add(attendanceRatio);

                        }
                    }


                }
                return attendanceRatiosList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
