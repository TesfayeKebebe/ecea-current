using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class AuctionRepository
  {
    private ECXTradeDbContext _context;
    public AuctionRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<AuctionVM>> getbidoffers(DateTime From, DateTime To, bool IsAllOrder, decimal Price, string type)
    {
      try
      {
        int Sn = 1;
        List<AuctionVM> ecxOrders = new List<AuctionVM>();
        ecxOrders = await _context.AuctionVM.FromSqlRaw<AuctionVM>("exec dbo.pGetEcxAuctionByDate {0},{1}", From, To).ToListAsync();
        if (!IsAllOrder && Price > 0)
        {
          ecxOrders = ecxOrders.Where(x => x.IsViolation && x.Price >= Price ).ToList();
        }
        else if (!IsAllOrder)
        {
          ecxOrders = ecxOrders.Where(x => x.IsViolation).ToList();
        }
        else if (Price > 0)
        {
          ecxOrders = ecxOrders.Where(x => x.Price >= Price && x.OrderStatus == "Canceled").ToList();
        }
       
          if (type == "EX")
          {
          var recordedViolations = await _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.AuctionViolationRecordByDate), From, To).ToListAsync();
          if (recordedViolations.Count() > 0)
            {
              foreach (var t in ecxOrders)
              {
              t.SN = Sn++;
              foreach (var item in recordedViolations)
                {
                  switch (item.Type)
                  {
                    case Constants.IsNotRegistered:
                      item.Type = "Not Registered";
                      break;
                    case Constants.HasNewRequestAfterPreOpen:
                      item.Type = "New Request After PreOpen";
                      break;
                    case Constants.HasMarkingToClose:
                      item.Type = "Marking To Close";
                      break;
                    case Constants.HasCancelled:
                      item.Type = "Cancelled";
                      break;
                    case Constants.HasSuspended:
                      item.Type = "Suspended";
                      break;
                    case Constants.HasNotRenewed:
                      item.Type = "Not Renewed";
                      break;
                    case Constants.HasBucketing:
                      item.Type = "Bucketing";
                      break;
                    case Constants.HasPreArrangedDuringOpen:
                      item.Type = "PreArranged During Open";
                      break;
                    case Constants.HasFrontRunning:
                      item.Type = "Front Running";
                      break;
                    case Constants.HasAbnormalTrading:
                      item.Type = "Abnormal Trading";
                      break;
                    case Constants.HasPriceChange:
                      item.Type = "Price Change";
                      break;
                    case Constants.HasPreArragedTrade:
                      item.Type = "PreArraged Trade";
                      break;
                    case Constants.IsMatchTrade:
                      item.Type = "Match Trade";
                      break;
                    case Constants.HasSisterCompany:
                      item.Type = "Sister Company";
                      break;
                    case Constants.SellerNotRegisterd:
                      item.Type = "Seller Not Registerd";
                      break;
                    case Constants.BuyerNotRegisterd:
                      item.Type = "Buyer Not Registerd";
                      break;
                    case Constants.HasPriceVolumeBeyondLimit:
                      item.Type = "Price Volume Beyond Limit";
                      break;
                  case Constants.HasPriceLimit:
                    item.Type = "Price Limit";
                    break;
                }
                  if (!string.IsNullOrWhiteSpace(t.Violations))
                  {
                    if (!t.Violations.Contains(item.Type))
                    {
                      t.Violations = t.Violations + "," + item.Type;
                    }
                  }
                  else
                  {
                    t.Violations = item.Type;
                  }

                }

              }
            }
          }

          else 
          {
            foreach (var t in ecxOrders)
            {
              t.SN = Sn++;
            }
          }
        return ecxOrders;
          //  var countViewed = recordedViolations.Count(x => x.IsViewed);
          //  if (recordedViolations.Count() > 0)
          //  {
          //    foreach (var item in recordedViolations)
          //    {
          //      switch (item.Type)
          //      {
          //        case Constants.IsNotRegistered:
          //          item.Type = "Not Registered";
          //          break;
          //        caseConstants.HasNewRequestAfterPreOpen:
          //          item.Type = "New Request After PreOpen";
          //          break;
          //        case Constants.HasMarkingToClose:
          //          item.Type = "Marking To Close";
          //          break;
          //        case Constants.HasCancelled:
          //          item.Type = "Cancelled";
          //          break;
          //        case Constants.HasSuspended:
          //          item.Type = "Suspended";
          //          break;
          //        case Constants.HasNotRenewed:
          //          item.Type = "Not Renewed";
          //          break;
          //        case Constants.HasBucketing:
          //          item.Type = "Bucketing";
          //          break;
          //        case Constants.HasPreArrangedDuringOpen:
          //          item.Type = "PreArranged During Open";
          //          break;
          //        case Constants.HasFrontRunning:
          //          item.Type = "Front Running";
          //          break;
          //        case Constants.HasAbnormalTrading:
          //          item.Type = "Abnormal Trading";
          //          break;
          //        case Constants.HasPriceChange:
          //          item.Type = "Price Change";
          //          break;
          //        case Constants.HasPreArragedTrade:
          //          item.Type = "PreArraged Trade";
          //          break;
          //        case Constants.IsMatchTrade:
          //          item.Type = "Match Trade";
          //          break;
          //        case Constants.HasSisterCompany:
          //          item.Type = "Sister Company";
          //          break;
          //        case Constants.SellerNotRegisterd:
          //          item.Type = "Seller Not Registerd";
          //          break;
          //        case Constants.BuyerNotRegisterd:
          //          item.Type = "Buyer Not Registerd";
          //          break;
          //        case Constants.HasPriceVolumeBeyondLimit:
          //          item.Type = "Price Volume Beyond Limit";
          //          break;
          //      }
          //      if (!string.IsNullOrWhiteSpace(t.Violations))
          //      {
          //        t.Violations = t.Violations + "," + item.Type;
          //      }
          //      else
          //      {
          //        t.Violations = item.Type;
          //      }
          //    }


          //    if (recordedViolations.Count() == countViewed)
          //    {
          //      t.HasViolation = "Viewed";
          //    }
          //    else if (recordedViolations.Count() > countViewed && countViewed > 0)
          //    {
          //      t.HasViolation = "Incomplete Viewed";
          //    }
          //    else
          //    {
          //      t.HasViolation = "Not Viewed";

          //    }
          //  }
          //  else
          //  {
          //    t.HasViolation = "Has Not Violation";
          //  }




          //}

    

     
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }
  }
}
