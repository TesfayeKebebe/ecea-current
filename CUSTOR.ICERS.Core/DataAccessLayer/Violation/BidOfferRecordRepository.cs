using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class BidOfferRecordRepository
  {
    private ECXTradeDbContext _context;

    public BidOfferRecordRepository(ECXTradeDbContext context)
    {
      _context = context;
    }

    public async Task<List<EcxOrderVm>> getbidoffers(DateTime From, DateTime To, bool IsAllOrder, decimal Price, string type)
    {
      try
      {
        int Sn = 1;
        List<EcxOrderVm> ecxOrders = new List<EcxOrderVm>();
        List<EcxOrderVm> addedecxOrders = new List<EcxOrderVm>();
        if (type != "O")
        {
          ecxOrders = await _context.EcxOrderVm.FromSqlRaw<EcxOrderVm>("exec dbo.pGetEcxOrderByDate {0},{1}", From, To).OrderByDescending(x => x.SubmittedTimestamp).ToListAsync();
          addedecxOrders.AddRange(ecxOrders);
        }
        if(type == "O")
        {
          List<OpenedSession> openedSessions = new List<OpenedSession>();
          openedSessions = await _context.OpenedSession.FromSqlRaw<OpenedSession>(QueryConfiguration.GetQuery(Constants.OpenedSession)).ToListAsync();
          foreach (var open in openedSessions)
          {
           ecxOrders = new List<EcxOrderVm>();
            ecxOrders = await _context.EcxOrderVm.FromSqlRaw<EcxOrderVm>(QueryConfiguration.GetQuery(Constants.OnlineEcxOrderByDate),open.SessionId).OrderByDescending(x => x.SubmittedTimestamp).ToListAsync();
            addedecxOrders.AddRange(ecxOrders);
          }
        }
        if (!IsAllOrder && Price > 0)
        {
          addedecxOrders = addedecxOrders.Where(x => x.IsViolation && x.Price >= Price && x.OrderStatus == "Canceled").ToList();

        }
        else if (!IsAllOrder)
        {
          addedecxOrders = addedecxOrders.Where(x => x.IsViolation).ToList();
        }
        else if (Price > 0)
        {
          addedecxOrders = addedecxOrders.Where(x => x.Price >= Price && x.OrderStatus == "Canceled").ToList();
        }
        if (type == "Ex")
        {
          var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>("exec dbo.pGetViolationRecordBySubmittedDate {0},{1}", From, To);

          if (recordedViolations.Count() > 0)
          {
            foreach(var t in addedecxOrders)
            {
            foreach (var item in recordedViolations)
            {
              switch (item.Type)
              {
                case Constants.IsNotRegistered:
                  item.Type = "Not Registered";
                  break;
                case Constants.HasNewRequestAfterPreOpen:
                  item.Type = "New Request After PreOpen";
                  break;
                case Constants.HasMarkingToClose:
                  item.Type = "Marking To Close";
                  break;
                case Constants.HasCancelled:
                  item.Type = "Cancelled";
                  break;
                case Constants.HasSuspended:
                  item.Type = "Suspended";
                  break;
                case Constants.HasNotRenewed:
                  item.Type = "Not Renewed";
                  break;
                case Constants.HasBucketing:
                  item.Type = "Bucketing";
                  break;
                case Constants.HasPreArrangedDuringOpen:
                  item.Type = "PreArranged During Open";
                  break;
                case Constants.HasFrontRunning:
                  item.Type = "Front Running";
                  break;
                case Constants.HasAbnormalTrading:
                  item.Type = "Abnormal Trading";
                  break;
                case Constants.HasPriceChange:
                  item.Type = "Price Change";
                  break;
                case Constants.HasPreArragedTrade:
                  item.Type = "PreArraged Trade";
                  break;
                case Constants.IsMatchTrade:
                  item.Type = "Match Trade";
                  break;
                case Constants.HasSisterCompany:
                  item.Type = "Sister Company";
                  break;
                case Constants.SellerNotRegisterd:
                  item.Type = "Seller Not Registerd";
                  break;
                case Constants.BuyerNotRegisterd:
                  item.Type = "Buyer Not Registerd";
                  break;
                case Constants.HasPriceVolumeBeyondLimit:
                  item.Type = "Price Volume Beyond Limit";
                  break;
                  case Constants.HasPriceLimit:
                    item.Type = "Price Limit";
                    break;
                }
              if (!string.IsNullOrWhiteSpace(t.Violations))
              {
                  if (!t.Violations.Contains(item.Type))
                  {
                    t.Violations = t.Violations + "," + item.Type;
                  }
              }
              else
              {
                t.Violations = item.Type;
              }

            }

            }
          }

       

          //List<BidOfferRecordVM> bidOfferRecordVMs = new List<BidOfferRecordVM>();
          //List<BidOfferRecordDetail> addedBidOffers = new List<BidOfferRecordDetail>();
          //bidOfferRecordVMs = await _context.Query<BidOfferRecordVM>().FromSql(QueryConfiguration.GetQuery(Constants.Order ), From, To).ToListAsync();
          //foreach (var item in bidOfferRecordVMs)
          //{

          //  BidOfferRecordDetail bid = new BidOfferRecordDetail()
          //  {
          //    Price = item.Price,
          //    OrderType = item.OrderType,
          //    ClientName = item.ClientName,
          //    CommodityClass = item.CommodityClass,
          //    CommodityGrade = item.CommodityGrade,
          //    CommodityType = item.CommodityType,
          //    MemberCode = item.MemberCode,
          //    MemberId = item.MemberId,
          //    CommodityGradeId = item.CommodityGradeId,
          //    ProductionYear = item.ProductionYear,
          //    Quantity = item.Quantity,
          //    RepName = item.RepName,
          //    SubmittedTimestamp = item.SubmittedTimestamp,
          //    Warehouse = item.Warehouse,
          //    OrderId = item.OrderId,
          //    Member = item.Member
          //  };
          //  var recordedViolations = _context.ViolationRecord.FirstOrDefault(x => x.ViolationId == item.OrderId.ToString());
          //  if (recordedViolations != null)
          //  {
          //    bid.IsViolation = true;
          //  }
          //  addedBidOffers.Add(bid);
          //}
          //return addedBidOffers;
        }

        else if (type != "O")
        {
          foreach (var t in addedecxOrders)
          {
            t.SN = Sn++;
          }
        }
       
        return addedecxOrders;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }
    public async Task<List<EcxOrderVm>> getOrderInvestigation(DateTime From, DateTime To)
    {
      try
      {
        List<EcxOrderVm> ecxOrders = new List<EcxOrderVm>();
       // List<EcxOrderVm> investigations = new List<EcxOrderVm>();
        ecxOrders = await _context.EcxOrderVm.FromSqlRaw<EcxOrderVm>(QueryConfiguration.GetQuery(Constants.GetInvestigationByDate), From, To).OrderByDescending(x=>x.SubmittedTimestamp).ToListAsync();
        return ecxOrders;

        //List<BidOfferRecordVM> bidOfferRecordVMs = new List<BidOfferRecordVM>();
        //List<BidOfferRecordDetail> addedBidOffers = new List<BidOfferRecordDetail>();
        //bidOfferRecordVMs = await _context.Query<BidOfferRecordVM>().FromSql(QueryConfiguration.GetQuery(Constants.Order ), From, To).ToListAsync();
        //foreach (var item in bidOfferRecordVMs)
        //{

        //  BidOfferRecordDetail bid = new BidOfferRecordDetail()
        //  {
        //    Price = item.Price,
        //    OrderType = item.OrderType,
        //    ClientName = item.ClientName,
        //    CommodityClass = item.CommodityClass,
        //    CommodityGrade = item.CommodityGrade,
        //    CommodityType = item.CommodityType,
        //    MemberCode = item.MemberCode,
        //    MemberId = item.MemberId,
        //    CommodityGradeId = item.CommodityGradeId,
        //    ProductionYear = item.ProductionYear,
        //    Quantity = item.Quantity,
        //    RepName = item.RepName,
        //    SubmittedTimestamp = item.SubmittedTimestamp,
        //    Warehouse = item.Warehouse,
        //    OrderId = item.OrderId,
        //    Member = item.Member
        //  };
        //  var recordedViolations = _context.ViolationRecord.FirstOrDefault(x => x.ViolationId == item.OrderId.ToString());
        //  if (recordedViolations != null)
        //  {
        //    bid.IsViolation = true;
        //  }
        //  addedBidOffers.Add(bid);
        //}
        //return addedBidOffers;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

  }
}
