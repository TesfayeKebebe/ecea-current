﻿using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class BucketingRepository
    {
        private readonly ECXTradeDbContext context;

        public BucketingRepository(ECXTradeDbContext _context)
        {
            context = _context;
        }
        public async Task<List<MembersTradeExecution>> GetBucketingList()
        {
            try
            {
                var bucketinglist = await context.MembersTradeExecution.FromSqlRaw<MembersTradeExecution>("EXECUTE spGetBucketing").ToListAsync();
                return bucketinglist;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
