using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class BuyerSellerRatioRepository
  {
    private readonly ECXTradeDbContext _context;
    public BuyerSellerRatioRepository(ECXTradeDbContext context)
    {
     _context = context;
    }
    public async Task<List<BuyerSellerRatio>> BuyerSellerRatios(DateTime From, DateTime To)
    {
      try
      {
        List<BuyerSellerRatio> buyerSellerRatios = new List<BuyerSellerRatio>();
        List<BuyerSellerRatio> buyerSellerClientRatios = new List<BuyerSellerRatio>();
        buyerSellerRatios = await _context.BuyerSellerRatio.FromSqlRaw<BuyerSellerRatio>("exec dbo.pGetCommodityBuyerSellerMemberRatio {0},{1}", From, To).ToListAsync();
        buyerSellerClientRatios = await _context.BuyerSellerRatio.FromSqlRaw<BuyerSellerRatio>("exec dbo.pGetCommodityBuyerSellerClientRatio {0},{1}", From, To).ToListAsync();
        foreach (var item in buyerSellerRatios)
        {
          var client = buyerSellerClientRatios.Where(x => x.Description == item.Description).FirstOrDefault();
          item.ClientRatio = Convert.ToDecimal(client?.ClientRatio);
        }
        return buyerSellerRatios;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
  }
}
