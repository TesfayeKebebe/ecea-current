using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class CommonViolationRepository
  {
    private readonly ECXTradeDbContext _context;
    private readonly IMapper _mapper;
    private readonly ECEADbContext _dbContext;

    public CommonViolationRepository(ECXTradeDbContext context, IMapper mapper, ECEADbContext dbContext)
    {
      _context = context;
      _mapper = mapper;
      _dbContext = dbContext;
    }
    public CommonViolationRepository()
    {
    }
    public async Task<List<EcxTradeVM>> getTrade(DateTime From, DateTime To, decimal VolumLimit, decimal Price, bool IsAllTrade, string Type)
    {
      try
      {
        int sn = 1;
        List<EcxTradeVM> trades = new List<EcxTradeVM>();
        trades = await _context.EcxTradeVM.FromSqlRaw<EcxTradeVM>(QueryConfiguration.GetQuery(Constants.GetTradeByDate), From, To).ToListAsync();
        if (!IsAllTrade)
        {
          trades = trades.Where(x => x.IsVoilation).ToList();
        }

        if (VolumLimit > 0 && Price > 0)
        {
          trades = trades.Where(x => x.TradePrice >= Convert.ToDecimal(Price) && x.TradeQuantity >= VolumLimit).ToList();
        }
        else if (VolumLimit > 0)
        {
          trades = trades.Where(x => x.TradeQuantity >= VolumLimit).ToList();

        }
        else if (Price > 0)
        {
          trades = trades.Where(x => x.TradePrice >= Convert.ToDecimal(Price)).ToList();

        }
        if (Type == "Ex")
        {
          var recordedViolations = await _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.TradeViolationRecordByDate), From, To).ToListAsync();
          foreach (var t in trades)
          {
            t.SN = sn++;
            if (recordedViolations.Count() > 0)
            {
              foreach (var item in recordedViolations)
              {
                switch (item.Type)
                {
                  case Constants.IsNotRegistered:
                    item.Type = "Not Registered";
                    break;
                  case Constants.HasNewRequestAfterPreOpen:
                    item.Type = "New Request After PreOpen";
                    break;
                  case Constants.HasMarkingToClose:
                    item.Type = "Marking To Close";
                    break;
                  case Constants.HasCancelled:
                    item.Type = "Cancelled";
                    break;
                  case Constants.HasSuspended:
                    item.Type = "Suspended";
                    break;
                  case Constants.HasNotRenewed:
                    item.Type = "Not Renewed";
                    break;
                  case Constants.HasBucketing:
                    item.Type = "Bucketing";
                    break;
                  case Constants.HasPreArrangedDuringOpen:
                    item.Type = "PreArranged During Open";
                    break;
                  case Constants.HasFrontRunning:
                    item.Type = "Front Running";
                    break;
                  case Constants.HasAbnormalTrading:
                    item.Type = "Abnormal Trading";
                    break;
                  case Constants.HasPriceChange:
                    item.Type = "Price Change";
                    break;
                  case Constants.HasPreArragedTrade:
                    item.Type = "PreArraged Trade";
                    break;
                  case Constants.IsMatchTrade:
                    item.Type = "Match Trade";
                    break;
                  case Constants.HasSisterCompany:
                    item.Type = "Sister Company";
                    break;
                  case Constants.SellerNotRegisterd:
                    item.Type = "Seller Not Registerd";
                    break;
                  case Constants.BuyerNotRegisterd:
                    item.Type = "Buyer Not Registerd";
                    break;
                  case Constants.HasPriceVolumeBeyondLimit:
                    item.Type = "Price Volume Beyond Limit";
                    break;
                  case Constants.HasPriceLimit:
                    item.Type = "Price Limit";
                    break;
                }
                if (!string.IsNullOrWhiteSpace(t.Violations))
                {
                  if (!t.Violations.Contains(item.Type))
                  {
                    t.Violations = t.Violations + "," + item.Type;
                  }
                }
                else
                {
                  t.Violations = item.Type;
                }
              }




            }
          }
        }
        else
        {
          foreach (var t in trades)
          {
            t.SN = sn++;
          }
        }
        return trades;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<EcxTradeVM>> getTradeInvestigation(DateTime From, DateTime To)
    {
      try
      {

        List<EcxTradeVM> trades = new List<EcxTradeVM>();
        //List<EcxTradeVM> investigations = new List<EcxTradeVM>();
        trades = await _context.EcxTradeVM.FromSqlRaw<EcxTradeVM>(QueryConfiguration.GetQuery(Constants.GetTradeByDate), From, To).Where(x => x.ViewStatus == "Viewed" || x.ViewStatus == "Incomplete Viewed").OrderByDescending(x => x.TradedTimestamp).ToListAsync();
        //foreach (var t in trades)
        //{

        //  var recordedViolations = await _context.ViolationRecordVm.FromSql(QueryConfiguration.GetQuery(Constants.ViolationRecordByViolationId), t.TradeId.ToString()).ToListAsync();
        //  if (recordedViolations.Count() > 0)
        //  {
        //    var countViewed = recordedViolations.Count(x => x.IsViewed);
        //    if(countViewed>0)
        //    {
        //      EcxTradeVM vm = new EcxTradeVM()
        //      { Warehouse = t.Warehouse,
        //       BuyerMember= t.BuyerMember,
        //       SellerMember= t.SellerMember,
        //       BuyerClientName= t.BuyerClientName,
        //       SellerClientName= t.SellerClientName,
        //       BuyerRepName = t.BuyerRepName,
        //       CommodityGrade = t.CommodityGrade,
        //       SellerRepName = t.SellerRepName,
        //       TradePrice = t.TradePrice,
        //       TradeQuantity = t.TradeQuantity,
        //       TradeId= t.TradeId,
        //       Symbol = t.Symbol,
        //      TradedTimestamp = t.TradedTimestamp};
        //      investigations.Add(vm);

        //    }

        //  }


        //}



        return trades;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }

    public bool PostViolation(string type, string violationId)
    {
      var data = _dbContext.ViolationRecord.FirstOrDefault(x => x.ViolationId == violationId && x.Type == type);
      if (data == null)
      {
        ViolationRecord violation = new ViolationRecord()
        {
          CreatedDate = DateTime.Now,
          Id = Guid.NewGuid().ToString(),
          Type = type,
          ViolationId = violationId
        };
        _context.Add(violation);
        _context.SaveChanges();
      }
      return true;
    }
    public async Task<CommonViolationDetail> getOrdersTradeById(DateTime From, DateTime To, Guid OrderId, decimal VolumLimit, int Percentage, string Type, string Buysel = "")
    {
      try
      {
        bool isViolation = false;
        //CommonViolation oldCommon = new CommonViolation();
        CommonViolation common = new CommonViolation();
        List<CommonViolationDetail> addedDetails = new List<CommonViolationDetail>();

        //List<CommonOrderAndTradeVM> addedCommonOrderAndTrade = new List<CommonOrderAndTradeVM>();
        List<BidOfferRecordVM> addedBid = new List<BidOfferRecordVM>();
        List<BidOfferRecordVM> addedOpenBid = new List<BidOfferRecordVM>();
        List<Auction> addedOpenAuction = new List<Auction>();

        List<AbnormalTrading> addAbnormalTradeSeles = new List<AbnormalTrading>();
        List<AbnormalTrading> addAbnormalTradeBuyer = new List<AbnormalTrading>();
        if (Type == "OV")
        {
          BidOfferRecordVM item = new BidOfferRecordVM();
          item = _context.BidOfferRecordVM.FromSqlRaw<BidOfferRecordVM>(QueryConfiguration.GetQuery(Constants.BidOfferRecordById), OrderId).FirstOrDefault();
          if (item != null)
          {
            if (item.PreOpenStart.HasValue && item.PreOpenEnd.HasValue)
            {
              addedBid = _context.BidOfferRecordVM.FromSqlRaw<BidOfferRecordVM>(QueryConfiguration.GetQuery(Constants.BidOfferRecordBySession), item.PreOpenStart, item.PreOpenEnd).ToList();
            }
            if (item.OpenStart.HasValue && item.OpenEnd.HasValue)
            {
              addedOpenBid = _context.BidOfferRecordVM.FromSqlRaw<BidOfferRecordVM>(QueryConfiguration.GetQuery(Constants.BidOfferRecordBySession), item.OpenStart, item.OpenEnd).ToList();
            }

            var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.ViolationRecordByViolationId), item.OrderId.ToString());

            foreach (var reco in recordedViolations)
            {
              switch (reco.Type)
              {
                case Constants.IsNotRegistered:
                  var exchangeActor = await _context.ExchangeActorCode.FromSqlRaw<ExchangeActorCode>(QueryConfiguration.GetQuery(Constants.ExchangeActorCode ), item.MemberCode).FirstOrDefaultAsync();
                  if (exchangeActor == null)
                  {
                    //if (oldCommon != null)
                    //{
                    //  oldCommon.IsNotRegistered = true;
                    //}
                    //else
                    //{
                    isViolation = true;
                    common.IsNotRegistered = true;
                    //}
                  }
                  break;
                case Constants.HasNewRequestAfterPreOpen:
                  var preopen = addedBid.Where(x => item.OrderId != x.OrderId && x.MemberId == item.MemberId && item.CommodityGradeId == x.CommodityGradeId).FirstOrDefault();
                  if (preopen != null)
                  {
                    common.PreOpenEnd = preopen?.PreOpenEnd;
                    common.PreOpenQuantity = preopen.Quantity;
                    if (preopen.Quantity != item.Quantity)
                    {
                      var change = preopen.Quantity - (preopen.Quantity + item.Quantity);
                      if (change < 0)
                      {
                        change = Math.Abs(Convert.ToDecimal(change));
                      }
                      common.NewQuantityChange = change;
                      isViolation = true;
                      common.HasNewRequestAfterPreOpen = true;
                    }
                  }
                  //else
                  //{

                  //  if (item.SubmittedTimestamp > item.PreOpenEnd)
                  //  {

                  //    isViolation = true;
                  //    common.PreOpenEnd = item.PreOpenEnd;
                  //    common.HasNewRequestAfterPreOpen = true;

                  //  }


                  //}
                  break;
                case Constants.HasMarkingToClose:
                  var listData = addedBid.Where(x => x.CommodityGradeId == item.CommodityGradeId).OrderByDescending(x => x.SubmittedTimestamp);
                  if (listData.Any())
                  {
                    var first = listData.FirstOrDefault();
                    if (first.OrderId == item.OrderId)
                    {
                      var forWeigh = listData.Where(x => x.OrderId != first.OrderId);
                      var weightedAvg = forWeigh.Sum(x => x.Price) / forWeigh.Count();
                      if (item.CommodityType == "Coffee")
                      {
                        var coffee = _dbContext.Commodity.FirstOrDefault(x => x.DesciptionEng.Contains("Coffee"));
                        common.MinAvgPrice = weightedAvg - coffee?.WeightedAverage;
                        common.MaxAvgPrice = weightedAvg + coffee?.WeightedAverage;
                        if (item.Price < common.MinAvgPrice || item.Price > common.MaxAvgPrice)
                        {
                          isViolation = true;
                          common.HasMarkingToClose = true;
                        }

                      }
                      else
                      {
                        var other = _dbContext.Commodity.FirstOrDefault(x => !x.DesciptionEng.Contains("Coffee"));
                        common.MinAvgPrice = weightedAvg - other?.WeightedAverage;
                        common.MaxAvgPrice = weightedAvg + other?.WeightedAverage;
                        if (item.Price < common.MinAvgPrice || item.Price > common.MaxAvgPrice)
                        {
                          isViolation = true;
                          common.HasMarkingToClose = true;
                        }

                      }
                    }
                  }

                  break;
                case Constants.HasCancelled:
                  var canceledMember = await _context.CancellationInformation.FromSqlRaw<CancellationInformation>(QueryConfiguration.GetQuery(Constants.Cancel), item.MemberCode).FirstOrDefaultAsync();
                  if (canceledMember != null)
                  {
                    isViolation = true;
                    common.HasCancelled = true;
                    common.CancellationType = canceledMember.CancellationType;
                    common.CancellationDate = canceledMember.CreatedDateTime;
                  }
                  break;
                case Constants.HasSuspended:
                  var suspendedMember = await _context.SuspendedExchange.FromSqlRaw<SuspendedExchange>(QueryConfiguration.GetQuery(Constants.SuspendedMember), item.MemberCode).FirstOrDefaultAsync();
                  if (suspendedMember != null)
                  {
                    isViolation = true;
                    common.HasSuspended = true;
                    common.InjuctionBody = suspendedMember.DescriptionEng;
                    common.Reason = suspendedMember.Reason;
                    common.InjunctionEndDate = suspendedMember.InjunctionEndDate;
                    common.InjunctionStartDate = suspendedMember.InjunctionStartDate;
                  }
                  break;
                case Constants.HasNotRenewed:
                  var notRenewedMember = await _context.RenewalInformation.FromSqlRaw<RenewalInformation>(QueryConfiguration.GetQuery(Constants.NotRenewedMember), item.MemberCode, item.SubmittedTimestamp).FirstOrDefaultAsync();
                  if (notRenewedMember != null)
                  {
                    isViolation = true;
                    common.HasNotRenewed = true;
                    common.IssueDateTime = notRenewedMember.IssueDateTime;
                    common.ExpireDateTime = notRenewedMember.ExpireDateTime;
                  }
                  break;
                case Constants.HasBucketing:
                  var backeting = await _context.BacketingViolation.FromSqlRaw<BacketingViolation>(QueryConfiguration.GetQuery(Constants.Bucketing), item.WarehouseId, item.CommodityGradeId, From, To).ToListAsync();
                  if (backeting != null)
                  {
                    isViolation = true;
                    common.HasBucketing = true;
                  }
                  break;
                case Constants.HasPreArrangedDuringOpen:
                  if (item.MemberCode != null)
                  {
                    var cancellation = _context.ArrangedTrade.FromSqlRaw<ArrangedTrade>(QueryConfiguration.GetQuery(Constants.PrearrangedDuringOpen), item.OrderId, item.OpenStart, item.OpenEnd).FirstOrDefault();
                    if (cancellation != null)
                    {
                      var followerBuyer = addedBid.Where(x => x.MemberId != item.MemberId && x.CommodityGradeId == cancellation.CommodityGradeId && x.OpenStart >= item.SessionStart && x.SessionEnd <= item.SessionEnd).OrderByDescending(x => x.SubmittedTimestamp).FirstOrDefault();
                      if (followerBuyer != null)
                      {
                        if (followerBuyer.Price < cancellation.LimitPrice)
                        {
                          isViolation = true;
                          common.HasPreArrangedDuringOpen = true;
                          var change = cancellation.LimitPrice - followerBuyer.Price;
                          if (change < 0)
                          {
                            change = Math.Abs(change.Value);
                          }
                          common.ChangePrice = change;
                          common.FollowerPrice = followerBuyer.Price;
                          common.CancelledPrice = cancellation.LimitPrice;
                          common.Status = "Cancelled";
                          common.CancelledTimestamp = cancellation.CancelledTimestamp;
                        }

                      }

                    }
                  }
                  break;
                //case Constants.HasFrontRunning:
                //  isViolation = true;
                //  break;
                case Constants.HasAbnormalTrading:
                  if (item.OrderType.Equals("Buyer"))
                  {
                    decimal priceChange = 0;
                    var ordersOfBuyers = addedOpenBid.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.MemberId == item.MemberId).OrderByDescending(x => x.SubmittedTimestamp).ToList();
                    if (ordersOfBuyers.Count() > 2)
                    {
                      int count = 1;
                      foreach (var buyer in ordersOfBuyers)
                      {
                        if (count == 1)
                        {
                          priceChange = Convert.ToDecimal(ordersOfBuyers[1].Price - buyer.Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0)
                          {
                            AbnormalTrading abnormal = new AbnormalTrading()
                            {
                              Buyer = buyer.Member,
                              BuyerPrice = buyer.Price,
                              ChangePrice = priceChange,
                              CommodityGrade = buyer.CommodityGrade,
                              Counter = count

                            };

                            if (count < ordersOfBuyers.Count() - 1)
                              count++;
                            addAbnormalTradeBuyer.Add(abnormal);
                          }

                        }
                        else
                        {
                          priceChange = Convert.ToDecimal(ordersOfBuyers[count].Price - buyer.Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {
                            AbnormalTrading abnormal = new AbnormalTrading()
                            {
                              Buyer = buyer.Member,
                              BuyerPrice = buyer.Price,
                              ChangePrice = priceChange,
                              CommodityGrade = buyer.CommodityGrade,
                              Counter = count
                            };
                            if (count < ordersOfBuyers.Count() - 1)
                              count++;
                            addAbnormalTradeBuyer.Add(abnormal);
                          }

                        }


                      }
                    }
                    if (addAbnormalTradeBuyer.Any())
                      addAbnormalTradeBuyer[addAbnormalTradeBuyer.Count - 1].Counter = addAbnormalTradeBuyer[addAbnormalTradeBuyer.Count - 1].Counter + 1;
                    var ordersOfSeller = addedOpenBid.Where(x => x.OrderType.Equals("seller") && x.CommodityGradeId == item.CommodityGradeId && x.OpenStart >= item.OpenStart && x.OpenEnd <= item.OpenEnd).OrderByDescending(x => x.SubmittedTimestamp).ToList();
                    bool lastCount = false;
                    if (ordersOfSeller.Count > 2)
                    {
                      int count = 1;
                      foreach (var seller in ordersOfSeller)
                      {
                        if (count == 1)
                        {
                          priceChange = Convert.ToDecimal(seller.Price - ordersOfSeller[count].Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {
                            var buyersChanges = addAbnormalTradeBuyer.FirstOrDefault(x => x.ChangePrice == priceChange && x.Counter == (lastCount ? count + 1 : count));
                            if (buyersChanges != null)
                            {
                              AbnormalTrading abnormal = new AbnormalTrading()
                              {
                                Buyer = buyersChanges.Buyer,
                                BuyerPrice = buyersChanges.BuyerPrice,
                                ChangePrice = priceChange,
                                CommodityGrade = buyersChanges.CommodityGrade,
                                SelerPrice = seller.Price,
                                Seller = seller.Member
                              };
                              addAbnormalTradeSeles.Add(abnormal);
                            }
                          }
                          if (count == ordersOfSeller.Count() - 1)
                          {
                            lastCount = true;
                          }
                          else if (count < ordersOfSeller.Count() - 1)
                            count++;
                        }
                        else
                        {
                          priceChange = Convert.ToDecimal(seller.Price - ordersOfSeller[count].Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {

                            var buyersChanges = addAbnormalTradeBuyer.FirstOrDefault(x => x.ChangePrice == priceChange && x.Counter == (lastCount ? count + 1 : count));
                            if (buyersChanges != null)
                            {
                              common.HasAbnormalTrading = true;
                              AbnormalTrading abnormal = new AbnormalTrading()
                              {
                                Buyer = buyersChanges.Buyer,
                                BuyerPrice = buyersChanges.BuyerPrice,
                                ChangePrice = priceChange,
                                CommodityGrade = buyersChanges.CommodityGrade,
                                SelerPrice = seller.Price,
                                Seller = seller.Member
                              };
                              addAbnormalTradeSeles.Add(abnormal);
                            }
                          }
                          if (count == ordersOfSeller.Count() - 1)
                          {
                            lastCount = true;
                          }
                          else if (count < ordersOfSeller.Count() - 1)
                            count++;

                        }


                      }

                    }

                  }

                  break;
                case Constants.HasPriceChange:
                  PreviousSessionVM previousSession = new PreviousSessionVM();
                  previousSession = await _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.LastSession), item.CommodityGradeId, item.SessionId, item.WarehouseId).FirstOrDefaultAsync();
                  if (previousSession != null)
                  {
                    PreviousPriceVM previousPrice = new PreviousPriceVM();
                    previousPrice = await _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.WeightAveragePrice), item.CommodityGradeId, previousSession.LastSessionId, item.WarehouseId).FirstOrDefaultAsync();
                    if (previousPrice != null)
                    {
                      var priceChange = Math.Round(Convert.ToDecimal((item.Price - previousPrice.WeightAveragePrice)), 2);
                      if (priceChange < 0)
                      {
                        common.PriceChange = Math.Abs(priceChange);
                      }
                      else
                      {
                        common.PriceChange = priceChange;

                      }
                      common.LastPrice = previousPrice?.WeightAveragePrice;
                      if (priceChange != 0)
                      {
                        isViolation = true;
                        common.HasPriceChange = true;
                      }

                    }
                  }

                  break;
                case Constants.HasPriceLimit:
                  CommodityPriceLimit priceLimit = new CommodityPriceLimit();
                  priceLimit = _context.CommodityPriceLimit.FromSqlRaw<CommodityPriceLimit>(QueryConfiguration.GetQuery(Constants.PriceLimitForCommodity), item.SessionId, item.CommodityGradeId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
                  if (priceLimit != null)
                  {
                    isViolation = true;
                    common.UpperPriceLimit = priceLimit.UpperPriceLimit;
                    common.LowerPriceLimit = priceLimit.LowerPriceLimit;
                    common.HasPriceLimit = true;
                  }
                  break;
              }
            }
            List<Violations> violations = new List<Violations>();
            CommonViolationDetail commonDetail = new CommonViolationDetail();

            commonDetail = GetCommonViolaiton(item, common);
            //var countViewed = recordedViolations.Count(x => x.IsViewed);
            //if (recordedViolations.Count() == countViewed)
            //{
            //  commonDetail.ViolationStatus = "Viewed";
            //}
            //else if (recordedViolations.Count() > countViewed && countViewed > 0)
            //{
            //  commonDetail.ViolationStatus = "Incomplete Viewed";
            //}
            //else
            //{
            //  commonDetail.ViolationStatus = "Not Viewed";

            //}
            var prearr = recordedViolations.FirstOrDefault(x => x.Type == Constants.HasPreArragedTrade);
            if (prearr != null && item.PreOpenStart.HasValue && item.PreOpenEnd.HasValue)
            {
              var prearranged = addedBid.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.PreOpenStart.Value.Date >= item.PreOpenStart.Value.Date && x.PreOpenEnd.Value.Date <= item.PreOpenEnd.Value.Date && x.Price == item.Price && x.Quantity == item.Quantity).ToList();

              if (prearranged.Count >= 2)
              {
                foreach (var pre in prearranged)
                {
                  Violations v = new Violations()
                  {
                    CommodityGrade = pre.CommodityGrade,
                    MemberCode = pre.Member,
                    OrderPrice = pre.Price,
                    PreOpenStart = pre.PreOpenStart,
                    PreOpenEnd = pre.PreOpenEnd,
                    OrderQuantity = pre.Quantity,
                    SubmittedTimestamp = pre.SubmittedTimestamp
                  };
                  violations.Add(v);

                }
                isViolation = true;
                commonDetail.HasPreArragedTrade = true;
                //}
              }
            }

            if (isViolation)
            {
              commonDetail.IsVoilation = true;

            }
            commonDetail.Violations = violations;
            commonDetail.AbnormalTrading = addAbnormalTradeSeles;
            return commonDetail;
          }
          else
          {
            return new CommonViolationDetail();
          }
        }
        else if (Type == "AU")
        {
          Auction item = new Auction();
          if (Buysel == "seller")
          {
            item = _context.Auction.FromSqlRaw<Auction>(QueryConfiguration.GetQuery(Constants.AuctionSellerById), OrderId).FirstOrDefault();
          }
          else
          {
            item = _context.Auction.FromSqlRaw<Auction>(QueryConfiguration.GetQuery(Constants.AuctionBuyerById), OrderId).FirstOrDefault();

          }
          if (item != null)
          {



            var recordedViolations = _dbContext.AuctionViolationRecord.FromSqlRaw<AuctionViolationRecord>(QueryConfiguration.GetQuery(Constants.AuctiontViolationRecordByViolationId), item.OrderId.ToString());
            foreach (var reco in recordedViolations)
            {
              switch (reco.Type)
              {
                case Constants.IsNotRegistered:
                  var exchangeActor = await _context.ExchangeActorCode.FromSqlRaw<ExchangeActorCode>(QueryConfiguration.GetQuery(Constants.ExchangeActorCode ), item.MemberCode).FirstOrDefaultAsync();
                  if (exchangeActor == null)
                  {
                    isViolation = true;
                    common.IsNotRegistered = true;
                  }
                  break;

                case Constants.HasMarkingToClose:
                  if (!addedOpenAuction.Any())
                    addedOpenAuction = _context.Auction.FromSqlRaw<Auction>(QueryConfiguration.GetQuery(Constants.GetAuction), From, To).ToList();

                  var listData = addedOpenAuction.Where(x => x.CommodityGradeId == item.CommodityGradeId).OrderByDescending(x => x.SubmitedTimestamp);
                  if (listData.Any())
                  {
                    var first = listData.FirstOrDefault();
                    if (first.OrderId == item.OrderId)
                    {
                      var forWeigh = listData.Where(x => x.OrderId != first.OrderId);
                      var weightedAvg = forWeigh.Sum(x => x.Price) / forWeigh.Count();
                      if (item.CommodityType == "Coffee")
                      {
                        var coffee = _dbContext.Commodity.FirstOrDefault(x => x.DesciptionEng.Contains("Coffee"));
                        common.MinAvgPrice = weightedAvg - coffee?.WeightedAverage;
                        common.MaxAvgPrice = weightedAvg + coffee?.WeightedAverage;
                        if (item.Price < common.MinAvgPrice || item.Price > common.MaxAvgPrice)
                        {
                          isViolation = true;
                          common.HasMarkingToClose = true;
                        }

                      }

                    }
                  }

                  break;
                case Constants.HasCancelled:
                  var canceledMember = await _context.CancellationInformation.FromSqlRaw<CancellationInformation>(QueryConfiguration.GetQuery(Constants.Cancel), item.MemberCode).FirstOrDefaultAsync();
                  if (canceledMember != null)
                  {
                    isViolation = true;
                    common.HasCancelled = true;
                    common.CancellationType = canceledMember.CancellationType;
                    common.CancellationDate = canceledMember.CreatedDateTime;
                  }
                  break;
                case Constants.HasSuspended:
                  var suspendedMember = await _context.SuspendedExchange.FromSqlRaw<SuspendedExchange>(QueryConfiguration.GetQuery(Constants.SuspendedMember), item.MemberCode).FirstOrDefaultAsync();
                  if (suspendedMember != null)
                  {
                    isViolation = true;
                    common.HasSuspended = true;
                    common.InjuctionBody = suspendedMember.DescriptionEng;
                    common.Reason = suspendedMember.Reason;
                    common.InjunctionEndDate = suspendedMember.InjunctionEndDate;
                    common.InjunctionStartDate = suspendedMember.InjunctionStartDate;
                  }
                  break;
                case Constants.HasNotRenewed:
                  var notRenewedMember = await _context.RenewalInformation.FromSqlRaw<RenewalInformation>(QueryConfiguration.GetQuery(Constants.NotRenewedMember), item.MemberCode, item.SubmitedTimestamp).FirstOrDefaultAsync();
                  if (notRenewedMember != null)
                  {
                    isViolation = true;
                    common.HasNotRenewed = true;
                    common.IssueDateTime = notRenewedMember.IssueDateTime;
                    common.ExpireDateTime = notRenewedMember.ExpireDateTime;
                  }
                  break;
                case Constants.HasBucketing:
                  var backeting = await _context.BacketingViolation.FromSqlRaw<BacketingViolation>(QueryConfiguration.GetQuery(Constants.Bucketing), item.WarehouseId, item.CommodityGradeId, From, To).ToListAsync();
                  if (backeting != null)
                  {
                    isViolation = true;
                    common.HasBucketing = true;
                  }
                  break;

                //case Constants.HasFrontRunning:
                //  isViolation = true;
                //  break;
                case Constants.HasAbnormalTrading:
                  if (item.OrderType.Equals("Buyer"))
                  {
                    decimal priceChange = 0;
                    var ordersOfBuyers = addedOpenBid.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.MemberId == item.MemberGuid).OrderByDescending(x => x.SubmittedTimestamp).ToList();
                    if (ordersOfBuyers.Count() > 2)
                    {
                      int count = 1;
                      foreach (var buyer in ordersOfBuyers)
                      {
                        if (count == 1)
                        {
                          priceChange = Convert.ToDecimal(ordersOfBuyers[1].Price - buyer.Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0)
                          {
                            AbnormalTrading abnormal = new AbnormalTrading()
                            {
                              Buyer = buyer.Member,
                              BuyerPrice = buyer.Price,
                              ChangePrice = priceChange,
                              CommodityGrade = buyer.CommodityGrade,
                              Counter = count

                            };

                            if (count < ordersOfBuyers.Count() - 1)
                              count++;
                            addAbnormalTradeBuyer.Add(abnormal);
                          }

                        }
                        else
                        {
                          priceChange = Convert.ToDecimal(ordersOfBuyers[count].Price - buyer.Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {
                            AbnormalTrading abnormal = new AbnormalTrading()
                            {
                              Buyer = buyer.Member,
                              BuyerPrice = buyer.Price,
                              ChangePrice = priceChange,
                              CommodityGrade = buyer.CommodityGrade,
                              Counter = count
                            };
                            if (count < ordersOfBuyers.Count() - 1)
                              count++;
                            addAbnormalTradeBuyer.Add(abnormal);
                          }

                        }


                      }
                    }
                    if (addAbnormalTradeBuyer.Any())
                      addAbnormalTradeBuyer[addAbnormalTradeBuyer.Count - 1].Counter = addAbnormalTradeBuyer[addAbnormalTradeBuyer.Count - 1].Counter + 1;
                    var ordersOfSeller = addedOpenBid.Where(x => x.OrderType.Equals("seller") && x.CommodityGradeId == item.CommodityGradeId && x.OpenStart >= item.OpenStart && x.OpenEnd <= item.OpenEnd).OrderByDescending(x => x.SubmittedTimestamp).ToList();
                    bool lastCount = false;
                    if (ordersOfSeller.Count > 2)
                    {
                      int count = 1;
                      foreach (var seller in ordersOfSeller)
                      {
                        if (count == 1)
                        {
                          priceChange = Convert.ToDecimal(seller.Price - ordersOfSeller[count].Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {
                            var buyersChanges = addAbnormalTradeBuyer.FirstOrDefault(x => x.ChangePrice == priceChange && x.Counter == (lastCount ? count + 1 : count));
                            if (buyersChanges != null)
                            {
                              AbnormalTrading abnormal = new AbnormalTrading()
                              {
                                Buyer = buyersChanges.Buyer,
                                BuyerPrice = buyersChanges.BuyerPrice,
                                ChangePrice = priceChange,
                                CommodityGrade = buyersChanges.CommodityGrade,
                                SelerPrice = seller.Price,
                                Seller = seller.Member
                              };
                              addAbnormalTradeSeles.Add(abnormal);
                            }
                          }
                          if (count == ordersOfSeller.Count() - 1)
                          {
                            lastCount = true;
                          }
                          else if (count < ordersOfSeller.Count() - 1)
                            count++;
                        }
                        else
                        {
                          priceChange = Convert.ToDecimal(seller.Price - ordersOfSeller[count].Price);
                          if (priceChange < 0)
                          {
                            priceChange = Math.Abs(priceChange);
                          }
                          if (priceChange > 0 || count > 1)
                          {

                            var buyersChanges = addAbnormalTradeBuyer.FirstOrDefault(x => x.ChangePrice == priceChange && x.Counter == (lastCount ? count + 1 : count));
                            if (buyersChanges != null)
                            {
                              common.HasAbnormalTrading = true;
                              AbnormalTrading abnormal = new AbnormalTrading()
                              {
                                Buyer = buyersChanges.Buyer,
                                BuyerPrice = buyersChanges.BuyerPrice,
                                ChangePrice = priceChange,
                                CommodityGrade = buyersChanges.CommodityGrade,
                                SelerPrice = seller.Price,
                                Seller = seller.Member
                              };
                              addAbnormalTradeSeles.Add(abnormal);
                            }
                          }
                          if (count == ordersOfSeller.Count() - 1)
                          {
                            lastCount = true;
                          }
                          else if (count < ordersOfSeller.Count() - 1)
                            count++;

                        }


                      }

                    }

                  }
                  break;
                case Constants.HasPriceChange:
                  PreviousSessionVM previousSession = new PreviousSessionVM();
                  previousSession = await _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.LastSession), item.CommodityGradeId, item.SessionId, item.WarehouseId).FirstOrDefaultAsync();
                  if (previousSession != null)
                  {
                    PreviousPriceVM previousPrice = new PreviousPriceVM();
                    previousPrice = await _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.WeightAveragePrice), item.CommodityGradeId, previousSession.LastSessionId, item.WarehouseId).FirstOrDefaultAsync();
                    if (previousPrice != null)
                    {
                      var priceChange = Math.Round(Convert.ToDecimal((item.Price - previousPrice.WeightAveragePrice)), 2);
                      if (priceChange < 0)
                      {
                        common.PriceChange = Math.Abs(priceChange);
                      }
                      else
                      {
                        common.PriceChange = priceChange;

                      }
                      common.LastPrice = previousPrice?.WeightAveragePrice;
                      if (priceChange != 0)
                      {
                        isViolation = true;
                        common.HasPriceChange = true;
                      }

                    }
                  }

                  break;
                case Constants.HasPriceLimit:
                  CommodityPriceLimit priceLimit = new CommodityPriceLimit();
                  priceLimit = _context.CommodityPriceLimit.FromSqlRaw<CommodityPriceLimit>(QueryConfiguration.GetQuery(Constants.PriceLimitForCommodity), item.SessionId, item.CommodityGradeId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
                  if (priceLimit != null)
                  {
                    isViolation = true;
                    common.UpperPriceLimit = priceLimit.UpperPriceLimit;
                    common.LowerPriceLimit = priceLimit.LowerPriceLimit;
                    common.HasPriceLimit = true;
                  }
                  break;
              }
            }
            List<Violations> violations = new List<Violations>();
            CommonViolationDetail commonDetail = new CommonViolationDetail();
            commonDetail = GetCommonAuction(item, common);
            if (isViolation)
            {
              commonDetail.IsVoilation = true;

            }
            commonDetail.Violations = violations;
            commonDetail.AbnormalTrading = addAbnormalTradeSeles;
            return commonDetail;
          }
          else
          {
            return new CommonViolationDetail();
          }
        }
        else
        {
          CommonTradeVM item = new CommonTradeVM();
          item = _context.CommonTradeVM.FromSqlRaw<CommonTradeVM>(QueryConfiguration.GetQuery(Constants.CommonsTrade), OrderId).OrderByDescending(x => x.TradedTimestamp).FirstOrDefault();
          if (item != null)
          {
            List<OrderMember> orderMember = new List<OrderMember>();
            PreviousSessionVM previous = new PreviousSessionVM();
            var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.TradeViolationRecord), item.tradeId.ToString());
            foreach (var reco in recordedViolations)
            {
              switch (reco.Type)
              {
                case Constants.IsMatchTrade:
                  if (item.BuyerCode != null)
                  {
                    orderMember = _context.OrderMember.FromSqlRaw<OrderMember>(QueryConfiguration.GetQuery(Constants.OrderByMemberId), item.BuyerCode).ToList();
                    if (orderMember.Any())
                    {
                      var client = orderMember.FirstOrDefault(x => x.Id == item.SellOrderTicketId);
                      if (client != null)
                      {
                        common.BuyerClientName = client.ClientName;
                        common.SellerClientName = orderMember.FirstOrDefault(x => x.Id == item.BuyOrderTicketId)?.ClientName;
                        common.IsMatchTrade = true;
                        isViolation = true;
                      }
                    }
                  }
                  break;

                case Constants.HasPriceChange:
                  PreviousSessionVM previousSession = new PreviousSessionVM();
                  previousSession = await _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.LastSession), item.CommodityGradeId, item.SessionId, item.WarehouseId).FirstOrDefaultAsync();
                  if (previousSession != null)
                  {
                    PreviousPriceVM previousPrice = new PreviousPriceVM();
                    previousPrice = await _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.WeightAveragePrice), item.CommodityGradeId, previousSession.LastSessionId, item.WarehouseId).FirstOrDefaultAsync();
                    if (previousPrice != null)
                    {
                      var priceChange = Math.Round(Convert.ToDecimal((item.TradePrice - previousPrice.WeightAveragePrice)), 2);
                      if (priceChange < 0)
                      {
                        common.PriceChange = Math.Abs(priceChange);
                      }
                      else
                      {
                        common.PriceChange = priceChange;

                      }
                      common.LastPrice = previousPrice?.WeightAveragePrice;
                      if (priceChange != 0)
                      {
                        isViolation = true;
                        common.HasPriceChange = true;
                      }

                    }
                  }

                  break;
                case Constants.HasSisterCompany:
                  SisterCompanyMemberVM sisterCompanyMember = new SisterCompanyMemberVM();
                  sisterCompanyMember = await _context.SisterCompanyMemberVM.FromSqlRaw<SisterCompanyMemberVM>(QueryConfiguration.GetQuery(Constants.SisterCompanyViolationByCode), item.BuyerCode, item.SellerCode).FirstOrDefaultAsync();
                  if (sisterCompanyMember != null)
                  {
                    isViolation = true;
                    common.HasSisterCompany = true;
                  }
                  break;
                case Constants.SellerNotRegisterd:
                  var seller = await _context.ExchangeActorCode.FromSqlRaw<ExchangeActorCode>(QueryConfiguration.GetQuery(Constants.ExchangeActorCode ), item.SellerCode).FirstOrDefaultAsync();
                  if (seller == null)
                  {
                    isViolation = true;
                    common.SellerNotRegisterd = true;
                  }
                  break;
                case Constants.BuyerNotRegisterd:
                  var buyer = await _context.ExchangeActorCode.FromSqlRaw<ExchangeActorCode>(QueryConfiguration.GetQuery(Constants.ExchangeActorCode ), item.BuyerCode).FirstOrDefaultAsync();
                  if (buyer == null)
                  {
                    isViolation = true;
                    common.BuyerNotRegisterd = true;
                  }
                  break;
                case Constants.HasPriceLimit:
                  CommodityPriceLimit priceLimit = new CommodityPriceLimit();
                  priceLimit = _context.CommodityPriceLimit.FromSqlRaw<CommodityPriceLimit>(QueryConfiguration.GetQuery(Constants.PriceLimitForCommodity), item.SessionId, item.CommodityGradeId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
                  if (priceLimit != null)
                  {
                    isViolation = true;
                    common.UpperPriceLimit = priceLimit.UpperPriceLimit;
                    common.LowerPriceLimit = priceLimit.LowerPriceLimit;
                    common.HasPriceLimit = true;
                  }
                  break;
              }
            }
            if (Percentage > 0 || VolumLimit > 0)
            {
              PriceRangeVM previousRange = new PriceRangeVM();
              previousRange = await _context.PriceRangeVM.FromSqlRaw<PriceRangeVM>(QueryConfiguration.GetQuery(Constants.PriceRange), item.CommodityGradeId, item.SessionId, Percentage).FirstOrDefaultAsync();
              if (previousRange != null)
              {
                common.MaximumPrice = previousRange.MaxPrice;
                common.MinimumPrice = previousRange.MinPrice;
                common.VolumeLimit = VolumLimit;
                if (item.TradePrice < previousRange.MinPrice || item.TradePrice > previousRange.MaxPrice)
                {

                  isViolation = true;
                  common.HasPriceVolumeBeyondLimit = true;
                }
                if (item.TradeQuantity > VolumLimit && VolumLimit > 0)
                {
                  isViolation = true;
                  common.HasPriceVolumeBeyondLimit = true;
                }
              }
            }
            List<Violations> violations = new List<Violations>();
            CommonViolationDetail commonDetail = new CommonViolationDetail();
            commonDetail = GetCommonTradeViolaiton(item, common);
            var frontRunning = recordedViolations.FirstOrDefault(x => x.Type == Constants.HasFrontRunning);
            if (frontRunning != null && item.SellerCode != null && item.BuyerCode != null)
            {
              List<FrontRunning> frontRunningSeller = new List<FrontRunning>();
              List<FrontRunning> frontRunningBuyer = new List<FrontRunning>();
              List<FrontRunningDetail> frontRunnings = new List<FrontRunningDetail>();
              frontRunningSeller = await _context.FrontRunning.FromSqlRaw<FrontRunning>(QueryConfiguration.GetQuery(Constants.FrontRunningBySeller), item.SellerCode, item.CommodityGradeId, From, To).ToListAsync();
              var sellerFirst = frontRunningSeller.Where(x => !x.IsClientOrder).ToList();
              frontRunningBuyer = await _context.FrontRunning.FromSqlRaw<FrontRunning>(QueryConfiguration.GetQuery(Constants.FrontRunningByBuyer), item.BuyerCode, item.CommodityGradeId, From, To).ToListAsync();
              var BuyerFirst = frontRunningBuyer.Where(x => !x.IsClientOrder).ToList();
              foreach (var sell in sellerFirst)
              {
                var viol = frontRunningSeller.Where(x => x.IsClientOrder && x.CommodityGradeId == item.CommodityGradeId && (x.TradePrice < sell.TradePrice || x.TradeDate < sell.TradeDate)).ToList();
                if (viol.Any())
                {
                  foreach (var run in viol)
                  {
                    FrontRunningDetail d = new FrontRunningDetail()
                    {
                      Member = sell.Seller,
                      TransactionDate = sell.TradeDate,
                      Price = sell.TradePrice,
                      Seller = run.Seller,
                      TradePrice = run.TradePrice,
                      TradeDate = run.TradeDate,
                      Buyer = run.Buyer,
                      ClientName = run.ClientName
                    };

                    frontRunnings.Add(d);
                  }
                  commonDetail.HasFrontRunning = true;
                  isViolation = true;
                }
              }
              foreach (var sell in BuyerFirst)
              {
                var viol = frontRunningBuyer.Where(x => x.IsClientOrder && x.CommodityGradeId == item.CommodityGradeId && (x.TradePrice > sell?.TradePrice || x.TradeDate < sell?.TradeDate)).ToList();
                if (viol.Any())
                {
                  foreach (var run in viol)
                  {
                    FrontRunningDetail d = new FrontRunningDetail()
                    {
                      Member = sell.Buyer,
                      TransactionDate = sell.TradeDate,
                      Price = sell.TradePrice,
                      Seller = run.Seller,
                      TradePrice = run.TradePrice,
                      TradeDate = run.TradeDate,
                      Buyer = run.Buyer,
                      ClientName = run.ClientName

                    };

                    frontRunnings.Add(d);
                  }
                  commonDetail.HasFrontRunning = true;
                  isViolation = true;

                }

              }
              commonDetail.FrontRunning = frontRunnings;
            }
            if (isViolation)
            {
              commonDetail.IsVoilation = true;
            }
            commonDetail.Violations = violations;
            return commonDetail;
          }
          else
          {
            return new CommonViolationDetail();
          }

        }

      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }
    public CommonViolationDetail GetCommonOldViolaiton(CommonOrderAndTradeVM item, CommonViolation oldvalue)
    {

      CommonViolationDetail common = new CommonViolationDetail()
      {
        ClientCode = item.clientCode,
        CommodityGrade = item.CommodityGrade,
        CreatedTimestamp = item.CreatedTimestamp,
        OrderQuantity = item.Quantity,
        ProductionYear = item.ProductionYear,
        SessionStart = item.SessionStart,
        SessionEnd = item.SessionEnd,
        OrderPrice = item.LimitPrice,
        Warehouse = item.warehouse,
        TradeId = item.tradeId,
        Id = item.OrderId,
        TradeQuantity = item.TradeQuantity,
        TradePrice = item.TradePrice,
        MaxAvgPrice = oldvalue.MaxAvgPrice,
        MinAvgPrice = oldvalue.MinAvgPrice,
        HasSisterCompany = oldvalue.HasSisterCompany,
        IsNotRegistered = oldvalue.IsNotRegistered,
        HasPriceChange = oldvalue.HasPriceChange,
        HasMarkingToClose = oldvalue.HasMarkingToClose,
        HasNewRequestAfterPreOpen = oldvalue.HasNewRequestAfterPreOpen,
        HasPreArragedTrade = oldvalue.HasPreArragedTrade,
        PriceChange = oldvalue.PriceChange,
        PreOpenEnd = oldvalue.PreOpenEnd,
        PreOpenQuantity = oldvalue.PreOpenQuantity,
        MemberCode = item.memberCode,
        LastPrice = oldvalue.LastPrice,
        BuyerCode = oldvalue.BuyerCode,
        SellerCode = oldvalue.SellerCode,
        SubmittedTimestamp = item.SubmittedTimestamp,
        CommodityClass = item.CommodityClass,
        CommodityType = item.CommodityType,
        PreOpenStart = item.PreOpenStart,
        HasPriceVolumeBeyondLimit = oldvalue.HasPriceVolumeBeyondLimit,
        MaximumPrice = oldvalue.MaximumPrice,
        MinimumPrice = oldvalue.MinimumPrice,
        VolumeLimit = oldvalue.VolumeLimit,

      };
      if (item.tradeId != null)
      {
        common.SellerCode = item.SellerCode;
        common.BuyerCode = item.BuyerCode;
      }
      else
      {
        if (item.TransactionType == 2)
        {
          common.SellerCode = item.memberCode;
        }
        else
        {
          common.BuyerCode = item.memberCode;
        }
      }
      return common;
    }
    public CommonViolationDetail GetCommonViolaiton(BidOfferRecordVM item, CommonViolation NewValue)
    {

      CommonViolationDetail common = new CommonViolationDetail()
      {
        // ClientCode = item.clientCode,
        CommodityGrade = item.CommodityGrade,
        //CreatedTimestamp = item.,
        OrderQuantity = item.Quantity,
        ProductionYear = item.ProductionYear,
        SessionStart = item.SessionStart,
        SessionEnd = item.SessionEnd,
        OrderPrice = item.Price,
        Warehouse = item.Warehouse,
        //TradeId = item.tradeId,
        Id = item.OrderId,
        // TradeQuantity = item.TradeQuantity,
        //TradePrice = item.TradePrice,
        MaxAvgPrice = NewValue.MaxAvgPrice,
        MinAvgPrice = NewValue.MinAvgPrice,
        HasSisterCompany = NewValue.HasSisterCompany,
        IsNotRegistered = NewValue.IsNotRegistered,
        HasPriceChange = NewValue.HasPriceChange,
        HasMarkingToClose = NewValue.HasMarkingToClose,
        HasNewRequestAfterPreOpen = NewValue.HasNewRequestAfterPreOpen,
        HasPreArragedTrade = NewValue.HasPreArragedTrade,
        PriceChange = NewValue.PriceChange,
        PreOpenEnd = NewValue.PreOpenEnd,
        PreOpenQuantity = NewValue.PreOpenQuantity,
        MemberCode = item.Member,
        LastPrice = NewValue.LastPrice,
        //BuyerCode = item.BuyerName,
        //SellerCode = item.SellerName,
        SubmittedTimestamp = item.SubmittedTimestamp,
        CommodityClass = item.CommodityClass,
        CommodityType = item.CommodityType,
        PreOpenStart = item.PreOpenStart,
        HasPriceVolumeBeyondLimit = NewValue.HasPriceVolumeBeyondLimit,
        MaximumPrice = NewValue.MaximumPrice,
        MinimumPrice = NewValue.MinimumPrice,
        VolumeLimit = NewValue.VolumeLimit,
        BuyerNotRegisterd = NewValue.BuyerNotRegisterd,
        SellerNotRegisterd = NewValue.SellerNotRegisterd,
        HasNotRenewed = NewValue.HasNotRenewed,
        HasSuspended = NewValue.HasSuspended,
        HasCancelled = NewValue.HasCancelled,
        IsMatchTrade = NewValue.IsMatchTrade,
        SellerClientName = NewValue.SellerClientName,
        BuyerClientName = NewValue.BuyerClientName,
        FollowerPrice = NewValue.FollowerPrice,
        ChangePrice = NewValue.ChangePrice,
        CancelledPrice = NewValue.CancelledPrice,
        Status = NewValue.Status,
        HasPreArrangedDuringOpen = NewValue.HasPreArrangedDuringOpen,
        CancelledTimestamp = NewValue.CancelledTimestamp,
        //TradedTimestamp = item.TradedTimestamp,
        InjunctionStartDate = NewValue.InjunctionStartDate,
        InjunctionEndDate = NewValue.InjunctionEndDate,
        Reason = NewValue.Reason,
        InjuctionBody = NewValue.InjuctionBody,
        IssueDateTime = NewValue.IssueDateTime,
        ExpireDateTime = NewValue.ExpireDateTime,
        CancellationDate = NewValue.CancellationDate,
        CancellationType = NewValue.CancellationType,
        HasBucketing = NewValue.HasBucketing,
        HasFrontRunning = NewValue.HasFrontRunning,
        NewQuantityChange = NewValue.NewQuantityChange,
        HasAbnormalTrading = NewValue.HasAbnormalTrading,
        HasPriceLimit = NewValue.HasPriceLimit,
        LowerPriceLimit = NewValue.LowerPriceLimit,
        UpperPriceLimit = NewValue.UpperPriceLimit
      };
      //if (item.tradeId == null)
      //{
      //  if (item.TransactionType == 2)
      //  {
      //    common.SellerCode = item.MemberName;
      //  }
      //  else
      //  {
      //    common.BuyerCode = item.MemberName;
      //  }
      //}

      return common;
    }
    public CommonViolationDetail GetCommonAuction(Auction item, CommonViolation NewValue)
    {

      CommonViolationDetail common = new CommonViolationDetail()
      {
        CommodityGrade = item.CommodityGrade,
        OrderQuantity = item.Quantity,
        SessionStart = item.SessionStart,
        SessionEnd = item.SessionEnd,
        OrderPrice = item.Price,
        Warehouse = item.Warehouse,
        Id = item.OrderId,
        MaxAvgPrice = NewValue.MaxAvgPrice,
        MinAvgPrice = NewValue.MinAvgPrice,
        HasSisterCompany = NewValue.HasSisterCompany,
        IsNotRegistered = NewValue.IsNotRegistered,
        HasPriceChange = NewValue.HasPriceChange,
        HasMarkingToClose = NewValue.HasMarkingToClose,
        HasNewRequestAfterPreOpen = NewValue.HasNewRequestAfterPreOpen,
        HasPreArragedTrade = NewValue.HasPreArragedTrade,
        PriceChange = NewValue.PriceChange,
        PreOpenEnd = NewValue.PreOpenEnd,
        PreOpenQuantity = NewValue.PreOpenQuantity,
        MemberCode = item.Member,
        LastPrice = NewValue.LastPrice,
        //BuyerCode = item.BuyerName,
        //SellerCode = item.SellerName,
        SubmittedTimestamp = item.SubmitedTimestamp,
        CommodityClass = item.CommodityClass,
        CommodityType = item.CommodityType,
        HasPriceVolumeBeyondLimit = NewValue.HasPriceVolumeBeyondLimit,
        MaximumPrice = NewValue.MaximumPrice,
        MinimumPrice = NewValue.MinimumPrice,
        VolumeLimit = NewValue.VolumeLimit,
        BuyerNotRegisterd = NewValue.BuyerNotRegisterd,
        SellerNotRegisterd = NewValue.SellerNotRegisterd,
        HasNotRenewed = NewValue.HasNotRenewed,
        HasSuspended = NewValue.HasSuspended,
        HasCancelled = NewValue.HasCancelled,
        IsMatchTrade = NewValue.IsMatchTrade,
        SellerClientName = NewValue.SellerClientName,
        BuyerClientName = NewValue.BuyerClientName,
        FollowerPrice = NewValue.FollowerPrice,
        ChangePrice = NewValue.ChangePrice,
        CancelledPrice = NewValue.CancelledPrice,
        Status = NewValue.Status,
        HasPreArrangedDuringOpen = NewValue.HasPreArrangedDuringOpen,
        CancelledTimestamp = NewValue.CancelledTimestamp,
        //TradedTimestamp = item.TradedTimestamp,
        InjunctionStartDate = NewValue.InjunctionStartDate,
        InjunctionEndDate = NewValue.InjunctionEndDate,
        Reason = NewValue.Reason,
        InjuctionBody = NewValue.InjuctionBody,
        IssueDateTime = NewValue.IssueDateTime,
        ExpireDateTime = NewValue.ExpireDateTime,
        CancellationDate = NewValue.CancellationDate,
        CancellationType = NewValue.CancellationType,
        HasBucketing = NewValue.HasBucketing,
        HasFrontRunning = NewValue.HasFrontRunning,
        NewQuantityChange = NewValue.NewQuantityChange,
        HasAbnormalTrading = NewValue.HasAbnormalTrading,
        HasPriceLimit = NewValue.HasPriceLimit,
        LowerPriceLimit = NewValue.LowerPriceLimit,
        UpperPriceLimit = NewValue.UpperPriceLimit
      };


      return common;
    }

    public CommonViolationDetail GetCommonTradeViolaiton(CommonTradeVM item, CommonViolation NewValue)
    {

      CommonViolationDetail common = new CommonViolationDetail()
      {
        // ClientCode = item.clientCode,
        CommodityGrade = item.CommodityGrade,
        CreatedTimestamp = item.CreatedTimestamp,
        OrderQuantity = item.Quantity,
        //ProductionYear = item.ProductionYear,
        SessionStart = item.SessionStart,
        SessionEnd = item.SessionEnd,
        OrderPrice = item.LimitPrice,
        Warehouse = item.warehouse,
        TradeId = item.tradeId,
        Id = item.tradeId,
        TradeQuantity = item.TradeQuantity,
        TradePrice = item.TradePrice,
        MaxAvgPrice = NewValue.MaxAvgPrice,
        MinAvgPrice = NewValue.MinAvgPrice,
        HasSisterCompany = NewValue.HasSisterCompany,
        IsNotRegistered = NewValue.IsNotRegistered,
        HasPriceChange = NewValue.HasPriceChange,
        HasMarkingToClose = NewValue.HasMarkingToClose,
        HasNewRequestAfterPreOpen = NewValue.HasNewRequestAfterPreOpen,
        HasPreArragedTrade = NewValue.HasPreArragedTrade,
        PriceChange = NewValue.PriceChange,
        PreOpenEnd = NewValue.PreOpenEnd,
        PreOpenQuantity = NewValue.PreOpenQuantity,
        MemberCode = item.BuyerName,
        LastPrice = NewValue.LastPrice,
        BuyerCode = item.BuyerName,
        SellerCode = item.SellerName,
        //SubmittedTimestamp = item.SubmittedTimestamp,
        CommodityClass = item.CommodityClass,
        CommodityType = item.CommodityType,
        PreOpenStart = item.PreOpenStart,
        HasPriceVolumeBeyondLimit = NewValue.HasPriceVolumeBeyondLimit,
        MaximumPrice = NewValue.MaximumPrice,
        MinimumPrice = NewValue.MinimumPrice,
        VolumeLimit = NewValue.VolumeLimit,
        BuyerNotRegisterd = NewValue.BuyerNotRegisterd,
        SellerNotRegisterd = NewValue.SellerNotRegisterd,
        HasNotRenewed = NewValue.HasNotRenewed,
        HasSuspended = NewValue.HasSuspended,
        HasCancelled = NewValue.HasCancelled,
        IsMatchTrade = NewValue.IsMatchTrade,
        SellerClientName = NewValue.SellerClientName,
        BuyerClientName = NewValue.BuyerClientName,
        FollowerPrice = NewValue.FollowerPrice,
        CancelledPrice = NewValue.CancelledPrice,
        ChangePrice = NewValue.ChangePrice,
        Status = NewValue.Status,
        HasPreArrangedDuringOpen = NewValue.HasPreArrangedDuringOpen,
        CancelledTimestamp = NewValue.CancelledTimestamp,
        TradedTimestamp = item.TradedTimestamp,
        HasPriceLimit = NewValue.HasPriceLimit,
        LowerPriceLimit = NewValue.LowerPriceLimit,
        UpperPriceLimit = NewValue.UpperPriceLimit

      };

      common.SellerCode = item.SellerName;

      common.BuyerCode = item.BuyerName;



      return common;
    }


    public CommonViolationDetail GetCommonViolaitons(CommonTradeVM item)
    {

      CommonViolationDetail common = new CommonViolationDetail()
      {
        // ClientCode = item.clientCode,
        CommodityGrade = item.CommodityGrade,
        CreatedTimestamp = item.CreatedTimestamp,
        OrderQuantity = item.Quantity,
        //ProductionYear = item.ProductionYear,
        SessionStart = item.SessionStart,
        SessionEnd = item.SessionEnd,
        OrderPrice = item.LimitPrice,
        Warehouse = item.warehouse,
        TradeId = item.tradeId,
        Id = item.tradeId,
        TradeQuantity = item.TradeQuantity,
        TradePrice = item.TradePrice,
        //MaxAvgPrice = NewValue.MaxAvgPrice,
        //MinAvgPrice = NewValue.MinAvgPrice,
        //HasSisterCompany = NewValue.HasSisterCompany,
        //IsNotRegistered = NewValue.IsNotRegistered,
        //HasPriceChange = NewValue.HasPriceChange,
        //HasMarkingToClose = NewValue.HasMarkingToClose,
        //HasNewRequestAfterPreOpen = NewValue.HasNewRequestAfterPreOpen,
        // HasPreArragedTrade = NewValue.HasPreArragedTrade,
        //PriceChange = NewValue.PriceChange,
        // PreOpenEnd = NewValue.PreOpenEnd,
        //PreOpenQuantity = NewValue.PreOpenQuantity,
        MemberCode = item.BuyerName,
        //LastPrice = NewValue.LastPrice,
        BuyerCode = item.BuyerName,
        SellerCode = item.SellerName,
        //SubmittedTimestamp = item.SubmittedTimestamp,
        CommodityClass = item.CommodityClass,
        CommodityType = item.CommodityType,
        // PreOpenStart = item.PreOpenStart,
        //HasPriceVolumeBeyondLimit = NewValue.HasPriceVolumeBeyondLimit,
        Bags = item.Bags,
        BuyerRepName = item.BuyerRepName,
        ConsignmentType = item.ConsignmentType,
        IsNewModel = item.IsNewModel,
        IsOnline = item.IsOnline,
        Quintal = item.Quintal,
        SellerRepName = item.SellerRepName,
        VolumeKGUsingSTD = item.VolumeKGUsingSTD,
        Lot = item.Lot,
        BuyerClientName = item.BuyerClientName,
        SellerClientName = item.SellerClientName,
        TradedTimestamp = item.TradedTimestamp
        //MaximumPrice = NewValue.MaximumPrice,
        //MinimumPrice = NewValue.MinimumPrice,
        // VolumeLimit = NewValue.VolumeLimit



      };


      common.SellerCode = item.SellerName;

      common.BuyerCode = item.BuyerName;



      return common;
    }
    public async Task<bool> updateViolationStatus(ViolationStatus violation)
    {
      if (violation.IsPostOrLive == "L")
      {

        ViolationRecord current = new ViolationRecord();
        current = await _dbContext.ViolationRecord.FirstOrDefaultAsync(x => x.ViolationId == violation.ViolationId && x.Type == violation.ViolationType);
        if (current != null)
        {
          current.IsViewed = true;
          _context.SaveChanges();
        }
        var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.ViolationRecordByViolationId), violation.ViolationId);
        var countViewed = recordedViolations.Count(x => x.IsViewed);
        EcxOrder order = new EcxOrder();
        order = _dbContext.EcxOrder.FirstOrDefault(x => x.OrderId.ToString() == violation.ViolationId);
        if (order != null)
        {
          if (recordedViolations.Count() == countViewed)
          {
            order.ViewStatus = "Viewed";
          }
          else if (recordedViolations.Count() > countViewed && countViewed > 0)
          {
            order.ViewStatus = "Incomplete Viewed";
          }
          _context.SaveChanges();
          return true;
        }
      }
      else if (violation.IsPostOrLive == "P")
      {

        TradeViolationRecord current = new TradeViolationRecord();
        current = await _dbContext.TradeViolationRecord.FirstOrDefaultAsync(x => x.ViolationId == violation.ViolationId && x.Type == violation.ViolationType);
        if (current != null)
        {
          current.IsViewed = true;
          _context.SaveChanges();
        }
        var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.TradeViolationRecord), violation.ViolationId);
        var countViewed = recordedViolations.Count(x => x.IsViewed);
        EcxTrade trade = new EcxTrade();
        trade = _dbContext.EcxTrade.FirstOrDefault(x => x.TradeId.ToString() == violation.ViolationId);
        if (trade != null)
        {
          if (recordedViolations.Count() == countViewed)
          {
            trade.ViewStatus = "Viewed";
          }
          else if (recordedViolations.Count() > countViewed && countViewed > 0)
          {
            trade.ViewStatus = "Incomplete Viewed";
          }
          _context.SaveChanges();
          return true;
        }
      }
      else
      {
        AuctionViolationRecord current = new AuctionViolationRecord();
        current = await _dbContext.AuctionViolationRecord.FirstOrDefaultAsync(x => x.ViolationId == violation.ViolationId && x.Type == violation.ViolationType);
        if (current != null)
        {
          current.IsViewed = true;
          _context.SaveChanges();
        }
        var recordedViolations = _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.AuctiontViolationRecordByViolationId), violation.ViolationId);
        var countViewed = recordedViolations.Count(x => x.IsViewed);
        EcxAuction order = new EcxAuction();
        order = _context.EcxAuction.FirstOrDefault(x => x.OrderId.ToString() == violation.ViolationId);
        if (order != null)
        {
          if (recordedViolations.Count() == countViewed)
          {
            order.ViewStatus = "Viewed";
          }
          else if (recordedViolations.Count() > countViewed && countViewed > 0)
          {
            order.ViewStatus = "Incomplete Viewed";
          }
          _context.SaveChanges();
          return true;
        }
      }
      return false;
    }
    public async Task<ViolationType> GetViolationStatus(string violationId)
    {
      ViolationType Type = new ViolationType();
      List<ViolationRecordVm> v = new List<ViolationRecordVm>();
      v = await _context.ViolationRecordVm.FromSqlRaw<ViolationRecordVm>(QueryConfiguration.GetQuery(Constants.ViolationRecordByViolationId), violationId).ToListAsync();
      foreach (var reco in v)
      {
        switch (reco.Type)
        {
          case Constants.IsNotRegistered:
            Type.IsNotRegistered = reco.IsViewed;
            break;
          case Constants.HasNewRequestAfterPreOpen:
            Type.HasNewRequestAfterPreOpen = reco.IsViewed;
            break;
          case Constants.HasMarkingToClose:
            Type.HasMarkingToClose = reco.IsViewed;

            break;
          case Constants.HasCancelled:
            Type.HasCancelled = reco.IsViewed;
            break;
          case Constants.HasSuspended:
            Type.HasSuspended = reco.IsViewed;
            break;
          case Constants.HasNotRenewed:
            Type.HasNotRenewed = reco.IsViewed;
            break;
          case Constants.HasBucketing:
            Type.HasBucketing = reco.IsViewed;
            break;
          case Constants.HasPreArrangedDuringOpen:
            Type.HasPreArrangedDuringOpen = reco.IsViewed;
            break;
          case Constants.HasFrontRunning:
            Type.HasFrontRunning = reco.IsViewed;
            break;
          case Constants.HasAbnormalTrading:
            Type.HasAbnormalTrading = reco.IsViewed;
            break;
          case Constants.HasPriceChange:
            Type.HasPriceChange = reco.IsViewed;
            break;
          case Constants.IsMatchTrade:
            Type.IsMatchTrade = reco.IsViewed;
            break;
          case Constants.HasSisterCompany:
            Type.HasSisterCompany = reco.IsViewed;
            break;
          case Constants.SellerNotRegisterd:
            Type.SellerNotRegisterd = reco.IsViewed;
            break;
          case Constants.BuyerNotRegisterd:
            Type.BuyerNotRegisterd = reco.IsViewed;
            break;
          case Constants.HasPreArragedTrade:
            Type.HasPreArragedTrade = reco.IsViewed;
            break;
          case Constants.HasPriceVolumeBeyondLimit:
            Type.HasPriceVolumeBeyondLimit = reco.IsViewed;

            break;
          case Constants.HasPriceLimit:
            Type.HasPriceLimit = reco.IsViewed;

            break;
        }
      }

      return Type;
    }

  }


}
