
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class ComparisonTradeByCommodityTypeRepo
    {
        private readonly ECXTradeDbContext _context;

        public ComparisonTradeByCommodityTypeRepo(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<ComparisonTradeByCommodityTypeView>> GetComparisonTradeByCommodityTypeList(ComparisonSearchSqlQuery comparisonSearchSql)
        {
            try
            {
                List<ComparisonTradeByCommodityType> comparisonTradeCommodityBefore = new List<ComparisonTradeByCommodityType>();
                comparisonTradeCommodityBefore = await _context.ComparisonTradeByCommodityType.FromSqlRaw<ComparisonTradeByCommodityType>(QueryConfiguration.GetQuery(Constants.ComparisonTradeByCommodityType), comparisonSearchSql.CommodityId,
                                                                                                        comparisonSearchSql.BeforeFrom, comparisonSearchSql.BeforeTo).ToListAsync();
                List<ComparisonTradeByCommodityType> comparisonTradeCommodityCurrent = new List<ComparisonTradeByCommodityType>();
                comparisonTradeCommodityCurrent = await _context.ComparisonTradeByCommodityType.FromSqlRaw<ComparisonTradeByCommodityType>(QueryConfiguration.GetQuery(Constants.ComparisonTradeByCommodityType), comparisonSearchSql.CommodityId,
                                                                                                        comparisonSearchSql.CurrentFrom, comparisonSearchSql.CurrentTo).ToListAsync();
                List<ComparisonTradeByCommodityTypeView> listOfCommodityTypeComparison = new List<ComparisonTradeByCommodityTypeView>();
                foreach (var item in comparisonTradeCommodityBefore)
                {
                    var current = comparisonTradeCommodityCurrent.Where(x => x.CommodityType == item.CommodityType).FirstOrDefault();

                    ComparisonTradeByCommodityTypeView detail = new ComparisonTradeByCommodityTypeView();
                    detail.CommodityType = current.CommodityType;
                    detail.BeforeQuantity = item.Quantity;
                    detail.BeforePrice = item.Price;
                    detail.CurrentQuantity = current.Quantity;
                    detail.CurrentPrice = current.Price;
                }
                foreach (var item in comparisonTradeCommodityCurrent)
                {
                    var before = comparisonTradeCommodityBefore.Where(x => x.CommodityType == item.CommodityType).FirstOrDefault();
                         
                    ComparisonTradeByCommodityTypeView detail = new ComparisonTradeByCommodityTypeView();
                    detail.CommodityType = item.CommodityType;
                    detail.BeforeQuantity = before?.Quantity;
                    detail.BeforePrice = before?.Price;
                    detail.CurrentQuantity = item?.Quantity;
                    detail.CurrentPrice = item?.Price;
                    detail.DiffernceQuantity = detail?.CurrentQuantity - ((detail.BeforeQuantity.HasValue)? (detail?.BeforeQuantity): 0 );
                    detail.DifferncePrice = detail?.CurrentPrice - ((detail.BeforePrice.HasValue) ? (detail?.BeforePrice) : 0);
                    listOfCommodityTypeComparison.Add(detail);
                }
                return listOfCommodityTypeComparison;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TradeComparisonByCommodityGradeView>> GetTradeComparisonByCommodityGradeList(ComparisonSearchSqlQuery comparisonSearchSql)
        {
            try
            {
                List<TradeComparisonByCommodityGrade> comparisonTradeCommodity = new List<TradeComparisonByCommodityGrade>();
                comparisonTradeCommodity = await _context.TradeComparisonByCommodityGrade.FromSqlRaw<TradeComparisonByCommodityGrade>(QueryConfiguration.GetQuery(Constants.ComparisonTradeByCommodityGrade), comparisonSearchSql.CommodityId,
                                                                                                        comparisonSearchSql.BeforeFrom, comparisonSearchSql.BeforeTo).ToListAsync();
                var totalQuantity = comparisonTradeCommodity.Select(x=>x.Quantity).Sum();
                List<TradeComparisonByCommodityGradeView> listOfCommodityTypeComparison = new List<TradeComparisonByCommodityGradeView>();
                foreach (var item in comparisonTradeCommodity)
                {
                    TradeComparisonByCommodityGradeView comparisonByCommodityGrade = new TradeComparisonByCommodityGradeView();
                    comparisonByCommodityGrade.CommodityGrade = item.CommodityGrade;
                    comparisonByCommodityGrade.Quantity = item.Quantity;
                    comparisonByCommodityGrade.Symbol = item.Symbol;
                    comparisonByCommodityGrade.ShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (comparisonByCommodityGrade.Quantity * 100) / totalQuantity));
                    listOfCommodityTypeComparison.Add(comparisonByCommodityGrade);
                }
                List<TradeComparisonByCommodityGradeView> listOfTradeComparisonCommodityType = new List<TradeComparisonByCommodityGradeView>();
                if(comparisonSearchSql.Order == true)
                {
                    listOfTradeComparisonCommodityType = listOfCommodityTypeComparison.OrderByDescending(x => x.ShareInPercent).Take(comparisonSearchSql.TopValue).ToList();
                }
                else
                {
                    listOfTradeComparisonCommodityType = listOfCommodityTypeComparison.OrderBy(x => x.ShareInPercent).Take(comparisonSearchSql.TopValue).ToList();
                }
                TradeComparisonByCommodityGradeView comparisonByCommodity = new TradeComparisonByCommodityGradeView();
                var totalSharePercent = listOfTradeComparisonCommodityType.Sum(x => x.ShareInPercent);
                comparisonByCommodity.CommodityGrade = "Others";
                comparisonByCommodity.ShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (100 - (listOfTradeComparisonCommodityType.Sum(x=>x.ShareInPercent)))));
                listOfTradeComparisonCommodityType.Add(comparisonByCommodity);
                return listOfTradeComparisonCommodityType;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
