using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class ContractMarketSharesRepository
  {
    private readonly ECXTradeDbContext _context;

    public ContractMarketSharesRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<MarketShareDetail>> MarketShareDetails( ContratMarketParamter searchParameter)
    {
      try
      {
        List<MarketShareDetail> marketShareDetails = new List<MarketShareDetail>();
        List<ContractMarketShare> contractMarketShares = new List<ContractMarketShare>();
        TotalSalesForMarket totalSalesForMarket = new TotalSalesForMarket();
        contractMarketShares = await _context.ContractMarketShare.FromSqlRaw<ContractMarketShare>(QueryConfiguration.GetQuery(Constants.BuyedCommodity), searchParameter.From, searchParameter.To, searchParameter.Commodity).ToListAsync();
        foreach (var item in contractMarketShares)
        {

          MarketShareDetail detail = new MarketShareDetail()
          {
            Symbol = item.Symbol,
            CommodityGrade = item.CommodityGrade,
            CreatedTimestamp = item.CreatedTimestamp,
            Commodity = item.Commodity,
            Quantity = item.Quantity
          };
          var existedTotal = marketShareDetails.FirstOrDefault(x => x.CreatedTimestamp == item.CreatedTimestamp);
          if (existedTotal != null)
          {
            detail.TotalQuantity = existedTotal.TotalQuantity;
            detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(existedTotal.TotalQuantity)) * 100, 2);
            marketShareDetails.Add(detail);
          }
          else
          {
            totalSalesForMarket =  _context.TotalSalesForMarket.FromSqlRaw<TotalSalesForMarket>(QueryConfiguration.GetQuery(Constants.ItemForSales), item.CreatedTimestamp).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (totalSalesForMarket.TotalQuantity.HasValue)
            {
              detail.TotalQuantity = totalSalesForMarket.TotalQuantity;
              detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity)) * 100, 2);
              marketShareDetails.Add(detail);
            }
          }

        }
        if(searchParameter.Order)
        {                                        
          marketShareDetails = marketShareDetails.OrderByDescending(x => x.Share).Take(searchParameter.Top).ToList();
        }
        else
        {
          marketShareDetails = marketShareDetails.Take(searchParameter.Top).ToList();
        }
        return marketShareDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<WarehouseMarketShareDetail>> WarehouseMarketShareDetails(ContratMarketParamter searchParam)
    {
      try
      {
        List<WarehouseMarketShareDetail> marketShareDetails = new List<WarehouseMarketShareDetail>();
        List<WarehouseMarketShare> contractMarketShares = new List<WarehouseMarketShare>();
        List<WarehouseMarketShare> SelectedcontractMarketShares = new List<WarehouseMarketShare>();
        TotalSalesForMarket totalSalesForMarket = new TotalSalesForMarket();
        List<string> wareHouse = new List<string>();
        if(searchParam.warehouseId!=null)
        {
          wareHouse = searchParam.warehouseId.Split(",").ToList();
          foreach (var item in wareHouse)
          {
            SelectedcontractMarketShares = new List<WarehouseMarketShare>();
            SelectedcontractMarketShares = await _context.WarehouseMarketShare.FromSqlRaw<WarehouseMarketShare>(QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouse), searchParam.From, searchParam.To, item).ToListAsync();
            contractMarketShares.AddRange(SelectedcontractMarketShares);
          }
        }
        else
        {
          contractMarketShares = await _context.WarehouseMarketShare.FromSqlRaw<WarehouseMarketShare>(QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouse), searchParam.From, searchParam.To, searchParam.warehouseId).ToListAsync();
        }

        foreach (var item in contractMarketShares)
        {

          WarehouseMarketShareDetail detail = new WarehouseMarketShareDetail()
          {
            //Symbol = item.Symbol,
            // CommodityGrade = item.CommodityGrade,
            //PreOpenEnd = item.PreOpenEnd,
            CreatedTimestamp = item.CreatedTimestamp,
            Quantity = item.Quantity,
            Warehouse = item.Warehouse
          };
          var existedTotal = marketShareDetails.FirstOrDefault(x =>  x.CreatedTimestamp == item.CreatedTimestamp);
          if (existedTotal != null)
          {
            detail.TotalQuantity = existedTotal.TotalQuantity;
            detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(existedTotal.TotalQuantity)) * 100, 2);
            marketShareDetails.Add(detail);
          }
          else
          {
            totalSalesForMarket = await _context.TotalSalesForMarket.FromSqlRaw<TotalSalesForMarket>(QueryConfiguration.GetQuery(Constants.ItemForSales), item.CreatedTimestamp).FirstOrDefaultAsync();
               if(totalSalesForMarket!=null)
            {
              if (totalSalesForMarket.TotalQuantity.HasValue)
              {
                detail.TotalQuantity = totalSalesForMarket.TotalQuantity;
                detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity)) * 100, 2);
                marketShareDetails.Add(detail);
              }

            }
          }

        }
        if (searchParam.Order)
        {
          marketShareDetails = marketShareDetails.OrderByDescending(x => x.Share).Take(searchParam.Top).ToList();
        }
        else
        {
          marketShareDetails = marketShareDetails.Take(searchParam.Top).ToList();
        }

        return marketShareDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<MarketShareDetail>> CommodityMarketShareDetails(ContratMarketParamter searchParam)
    {
      try
      {
        List<MarketShareDetail> marketShareDetails = new List<MarketShareDetail>();
        List<MarketShareByCommodity> contractMarketShares = new List<MarketShareByCommodity>();
        List<MarketShareByCommodity> selectedcontractMarketShares = new List<MarketShareByCommodity>();
        TotalSalesForMarket totalSalesForMarket = new TotalSalesForMarket();
        List<string> wareHouse = new List<string>();
        if (searchParam.warehouseId != null)
        {
          wareHouse = searchParam.warehouseId.Split(",").ToList();
          foreach (var item in wareHouse)
          {
            selectedcontractMarketShares = new List<MarketShareByCommodity>();
            selectedcontractMarketShares = await _context.MarketShareByCommodity.FromSqlRaw<MarketShareByCommodity>(QueryConfiguration.GetQuery(Constants.BuyedCommodityForMarketShare), searchParam.From, searchParam.To, searchParam.Commodity, item).ToListAsync();
            contractMarketShares.AddRange(selectedcontractMarketShares);
          }
        }
        else
        {
          contractMarketShares = await _context.MarketShareByCommodity.FromSqlRaw<MarketShareByCommodity>(QueryConfiguration.GetQuery(Constants.BuyedCommodityForMarketShare), searchParam.From, searchParam.To, searchParam.Commodity, searchParam.warehouseId).ToListAsync();

        }
        foreach (var item in contractMarketShares)
        {

          MarketShareDetail detail = new MarketShareDetail()
          {
            Symbol = item.Symbol,
            CommodityGrade = item.CommodityGrade,
            CreatedTimestamp = item.CreatedTimestamp,
            Quantity = item.Quantity,
            Commodity = item.Commodity

          };
          var existedTotal = marketShareDetails.FirstOrDefault(x => x.CreatedTimestamp==item.CreatedTimestamp);
          if (existedTotal != null)
          {
           
            var countPrev = marketShareDetails.Where(x => x.CommodityGrade == item.CommodityGrade).ToList();
            if (countPrev.Any())
            {
              foreach (var prev in countPrev)
              {
                prev.TotalQuantity = prev.TotalQuantity + totalSalesForMarket.TotalQuantity;
                if(prev.TotalQuantity>0)
                prev.Share = Math.Round((item.Quantity + prev.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity + prev.TotalQuantity)) * 100, 2);
              }
            }
            else
            {
              detail.TotalQuantity = existedTotal.TotalQuantity;
              if(detail.TotalQuantity>0)
              detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(existedTotal.TotalQuantity)) * 100, 2);
              marketShareDetails.Add(detail);
            }
          }
          else
          {
            totalSalesForMarket = await _context.TotalSalesForMarket.FromSqlRaw<TotalSalesForMarket>(QueryConfiguration.GetQuery(Constants.ItemForSales), item.CreatedTimestamp).FirstOrDefaultAsync();
            if (totalSalesForMarket.TotalQuantity.HasValue)
            {
              var countPrev = marketShareDetails.Where(x => x.CommodityGrade == item.CommodityGrade).ToList();
              if (countPrev.Any())
              {
                foreach (var prev in countPrev)
                {
                  prev.TotalQuantity = prev.TotalQuantity + totalSalesForMarket.TotalQuantity;
                  if(prev.TotalQuantity>0)
                  prev.Share = Math.Round((item.Quantity + prev.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity + prev.TotalQuantity)) * 100, 2);
                }
              }
              else
              {
                detail.TotalQuantity = totalSalesForMarket.TotalQuantity;
                if(detail.TotalQuantity>0)
                detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity)) * 100, 2);
                marketShareDetails.Add(detail);
              }
            
            }
          }

        }
        return marketShareDetails.OrderByDescending(x => x.Share).Take(searchParam.Top).ToList();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<WarehouseMarketShareDetail>> CommodityWarehouseMarketShareDetails(ContratMarketParamter searchParam)
    {
      try
      {
        List<WarehouseMarketShareDetail> marketShareDetails = new List<WarehouseMarketShareDetail>();
        List<CommodityWarehouseMarketShare> contractMarketShares = new List<CommodityWarehouseMarketShare>();
        List<CommodityWarehouseMarketShare> SelectedcontractMarketShares = new List<CommodityWarehouseMarketShare>();
        TotalSalesForMarket totalSalesForMarket = new TotalSalesForMarket();
        List<string> wareHouse = new List<string>();
        if (searchParam.warehouseId != null)
        {
          wareHouse = searchParam.warehouseId.Split(",").ToList();
          foreach (var item in wareHouse)
          {
            SelectedcontractMarketShares = new List<CommodityWarehouseMarketShare>();
            SelectedcontractMarketShares = await _context.CommodityWarehouseMarketShare.FromSqlRaw<CommodityWarehouseMarketShare>(QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouseForMarketShare), searchParam.From, searchParam.To, item, searchParam.Commodity).ToListAsync();
            contractMarketShares.AddRange(SelectedcontractMarketShares);
          }
        }
        else
        {
          contractMarketShares = await _context.CommodityWarehouseMarketShare.FromSqlRaw<CommodityWarehouseMarketShare>(QueryConfiguration.GetQuery(Constants.BuyedCommodityWarehouseForMarketShare), searchParam.From, searchParam.To, searchParam.warehouseId, searchParam.Commodity).ToListAsync();

        }
        foreach (var item in contractMarketShares)
        {                                                                       
          WarehouseMarketShareDetail detail = new WarehouseMarketShareDetail()
          {
            //Symbol = item.Symbol,
            //CommodityGrade = item.CommodityGrade,
           CreatedTimestamp = item.CreatedTimestamp,
            Quantity = item.Quantity,
            Warehouse = item.Warehouse,
            // Commodity = item.Commodity
          };
          var existedTotal = marketShareDetails.FirstOrDefault(x => x.CreatedTimestamp == item.CreatedTimestamp);
          if (existedTotal != null)
          {
         
            var countPrev = marketShareDetails.Where(x => x.Warehouse == item.Warehouse).ToList();
            if (countPrev.Any())
            {
              foreach (var prev in countPrev)
              {
                prev.TotalQuantity = prev.TotalQuantity + totalSalesForMarket.TotalQuantity;
                if(prev.TotalQuantity>0)
                prev.Share = Math.Round((item.Quantity + prev.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity + prev.TotalQuantity)) * 100, 2);
              }
            }
            else
            {
              detail.TotalQuantity = existedTotal.TotalQuantity;
              if(detail.TotalQuantity>0)
              detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(existedTotal.TotalQuantity)) * 100, 2);
              marketShareDetails.Add(detail);
            }
          }
          else
          {
            totalSalesForMarket = await _context.TotalSalesForMarket.FromSqlRaw<TotalSalesForMarket>(QueryConfiguration.GetQuery(Constants.ItemForSales), item.CreatedTimestamp).FirstOrDefaultAsync();
             if(totalSalesForMarket!=null)
            {
              if (totalSalesForMarket.TotalQuantity.HasValue)
              {
                var countPrev = marketShareDetails.Where(x => x.Warehouse == item.Warehouse).ToList();
                if (countPrev.Any())
                {
                  foreach (var prev in countPrev)
                  {
                    prev.TotalQuantity = prev.TotalQuantity + totalSalesForMarket.TotalQuantity;
                         if(prev.TotalQuantity>0)
                    prev.Share = Math.Round((item.Quantity + prev.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity + prev.TotalQuantity)) * 100, 2);
                  }
                }
                else
                {
                  detail.TotalQuantity = totalSalesForMarket.TotalQuantity;
                   if(detail.TotalQuantity>0)
                  detail.Share = Math.Round((item.Quantity / Convert.ToDecimal(totalSalesForMarket.TotalQuantity)) * 100, 2);
                  marketShareDetails.Add(detail);
                }

              }

            }
          }

        }
        return marketShareDetails.OrderByDescending(x => x.Share).Take(searchParam.Top).ToList();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }

  }
}
