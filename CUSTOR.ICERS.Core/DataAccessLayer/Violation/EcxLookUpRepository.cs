using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public class EcxLookUpRepository
  {
    private readonly ECEADbContext _context;

    public EcxLookUpRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<StaticData4>> GetListOfCommodityGrade()
    {
      try
      {
        var gradeList = await _context.TblCommodityGrade
               .Select(n => new StaticData4
               {
                 Id = n.Guid,
                 Description = n.Symbol

               })
               .AsNoTracking()
               .ToListAsync();
        return gradeList;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }

    public async Task<List<StaticData4>> GetListOfWarehouse()
    {
      try
      {
        var gradeList = await _context.TblWarehouse
               .Select(n => new StaticData4
               {
                 Id = n.Guid,
                 Description = n.Description

               })
               .AsNoTracking()
               .ToListAsync();
        return gradeList;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
    public async Task<List<StaticData4>> GetListOfCommodity()
    {
      try
      {
        var gradeList = await _context.TblCommodity
               .Select(n => new StaticData4
               {
                 Id = n.Guid,
                 Description = n.Description

               })
               .AsNoTracking()
               .ToListAsync();
        return gradeList;

      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
    public async Task<List<CommodityGradeLookUp>> getCommoditGrade()
    {
      List<CommodityGradeLookUp> commodityGrades = new List<CommodityGradeLookUp>();
      commodityGrades = await _context.CommodityGradeLookUp.FromSqlRaw<CommodityGradeLookUp>("exec dbo.pGetCommodityGrade").ToListAsync();
      return commodityGrades;
    }
  }
}
