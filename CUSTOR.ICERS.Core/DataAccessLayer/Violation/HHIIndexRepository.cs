using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class HHIIndexRepository
    {
        private readonly ECEADbContext _context;

        public HHIIndexRepository(ECEADbContext context)
        {
            _context = context;
        }
        public async Task<List<HHIIndexViewModel>> GetSellerHHIIndexList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<HHIIndex> hhiIndex = new List<HHIIndex>();
                hhiIndex = await _context.HHIIndex.FromSqlRaw<HHIIndex>(QueryConfiguration.GetQuery(Constants.BuyerMemberMarketShare),
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.CommodityTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<HHIIndexViewModel> listOfHHIIndex = new List<HHIIndexViewModel>();
                HHIIndexViewModel hhiIndexView = new HHIIndexViewModel();
                decimal totalSumSellQuantity = hhiIndex.Sum(x => x.Quantity);
                var sellerHHIndex = hhiIndex.Select(x => new
                {
                    x.MemberName,
                    x.Commodity
                }).Distinct();
                foreach (var item in sellerHHIndex)
                {
                    hhiIndexView = new HHIIndexViewModel();
                    hhiIndexView.MemberName = item.MemberName;
                    hhiIndexView.Commodity = item.Commodity;
                    hhiIndexView.VolumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", hhiIndex.Where(x => x.MemberName == item.MemberName &&
                                                                   x.Commodity == item.Commodity).Sum(x => x.Quantity)));
                    hhiIndexView.MerketShare = Convert.ToDecimal(String.Format("{0:00.0000}", (hhiIndexView.VolumeInLot * 100) / totalSumSellQuantity));
                    hhiIndexView.CalculateHHI = Convert.ToDecimal(String.Format("{0:00.0000}", hhiIndexView.MerketShare * hhiIndexView.MerketShare));
                    listOfHHIIndex.Add(hhiIndexView);
                }
                List<HHIIndexViewModel> hhiIndexList = new List<HHIIndexViewModel>();
                if (searchSqlQuery.Order == true)
                {
                    hhiIndexList = listOfHHIIndex.OrderByDescending(x => x.MerketShare).ToList();
                }
                else
                {
                    hhiIndexList = listOfHHIIndex.OrderBy(x => x.MerketShare).ToList();
                }
                return hhiIndexList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<HHIIndexViewModel>> GetBuyerHHIIndexList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<HHIIndex> hhiIndex = new List<HHIIndex>();
                hhiIndex = await _context.HHIIndex.FromSqlRaw<HHIIndex>(QueryConfiguration.GetQuery(Constants.SellerMemberMarketShare),
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.CommodityTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<HHIIndexViewModel> listOfHHIIndex = new List<HHIIndexViewModel>();
                HHIIndexViewModel hhiIndexView = new HHIIndexViewModel();
                decimal totalSumSellQuantity = hhiIndex.Sum(x => x.Quantity);
                var sellerHHIndex = hhiIndex.Select(x => new
                {
                    x.MemberName,
                    x.Commodity
                }).Distinct();
                foreach (var item in sellerHHIndex)
                {

                    hhiIndexView = new HHIIndexViewModel();
                    hhiIndexView.MemberName = item.MemberName;
                    hhiIndexView.Commodity = item.Commodity;
                    hhiIndexView.VolumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", hhiIndex.Where(x => x.MemberName == item.MemberName &&
                                                                   x.Commodity == item.Commodity).Sum(x => x.Quantity)));
                    hhiIndexView.MerketShare = Convert.ToDecimal(String.Format("{0:00.0000}", (hhiIndexView.VolumeInLot * 100) / totalSumSellQuantity));
                    hhiIndexView.CalculateHHI = Convert.ToDecimal(String.Format("{0:00.0000}", hhiIndexView.MerketShare * hhiIndexView.MerketShare));
                    listOfHHIIndex.Add(hhiIndexView);
                }
                List<HHIIndexViewModel> hhiIndexList = new List<HHIIndexViewModel>();
                if (searchSqlQuery.Order == true)
                {
                    hhiIndexList = listOfHHIIndex.OrderByDescending(x => x.MerketShare).ToList();
                }
                else
                {
                    hhiIndexList = listOfHHIIndex.OrderBy(x => x.MerketShare).ToList();
                }
                return hhiIndexList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<TopMarketShareViewModel>> GetBuyerMarketShareList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopMarketShare> topMarketShare = new List<TopMarketShare>();
                topMarketShare = await _context.TopMarketShare.FromSqlRaw<TopMarketShare>(QueryConfiguration.GetQuery(Constants.TopBuyerMarketShare), searchSqlQuery.CommodityId,
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopMarketShareViewModel> listOfTopMarketShare = new List<TopMarketShareViewModel>();
                TopMarketShareViewModel topMarketShareView = new TopMarketShareViewModel();
                decimal totalSumBuyQuantity = topMarketShare.Sum(x => x.Quantity);
                var buyerTopMarketShare = topMarketShare.Select(x => new
                {
                    x.MemberName
                }).Distinct();
                foreach (var item in buyerTopMarketShare)
                {

                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = item.MemberName;
                    var volumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", topMarketShare.Where(x => x.MemberName == item.MemberName).Sum(x => x.Quantity)));
                    topMarketShareView.MarketShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (volumeInLot * 100) / totalSumBuyQuantity));
                    topMarketShareView.HHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (topMarketShareView.MarketShareInPercent * topMarketShareView.MarketShareInPercent)));
                    listOfTopMarketShare.Add(topMarketShareView);
                }
                List<TopMarketShareViewModel> topMarketShareList = new List<TopMarketShareViewModel>();
                topMarketShareView = new TopMarketShareViewModel();

                if (searchSqlQuery.Order == true)
                {
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareList = listOfTopMarketShare.OrderByDescending(o => o.MarketShareInPercent)
                                       .Take(searchSqlQuery.TopValue).ToList();
                    topMarketShareView.MemberName = "Others";
                    topMarketShareView.MarketShareInPercent = 100 - topMarketShareList.Sum(x => x.MarketShareInPercent);
                    topMarketShareList.Add(topMarketShareView);
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = "HHI";
                    topMarketShareView.MarketShareInPercent = topMarketShareList.Sum(x => x.HHIIndex);
                    topMarketShareList.Add(topMarketShareView);
                }
                else
                {
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareList = listOfTopMarketShare.OrderBy(o => o.MarketShareInPercent)
                                       .Take(searchSqlQuery.TopValue).ToList();
                    topMarketShareView.MemberName = "Others";
                    topMarketShareView.MarketShareInPercent = 100 - topMarketShareList.Sum(x => x.MarketShareInPercent);
                    topMarketShareList.Add(topMarketShareView);
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = "HHI";
                    topMarketShareView.MarketShareInPercent = topMarketShareList.Sum(x => x.HHIIndex);
                    topMarketShareList.Add(topMarketShareView);
                }
                return topMarketShareList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<TopMarketShareViewModel>> GetBuyerMarketShareListGraph(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopMarketShare> topMarketShare = new List<TopMarketShare>();
                topMarketShare = await _context.TopMarketShare.FromSqlRaw<TopMarketShare>(QueryConfiguration.GetQuery(Constants.TopBuyerMarketShare), searchSqlQuery.CommodityId,
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopMarketShareViewModel> listOfTopMarketShare = new List<TopMarketShareViewModel>();
                TopMarketShareViewModel topMarketShareView = new TopMarketShareViewModel();
                decimal totalSumBuyQuantity = topMarketShare.Sum(x => x.Quantity);
                var buyerTopMarketShare = topMarketShare.Select(x => new
                {
                    x.MemberName
                }).Distinct();
                foreach (var item in buyerTopMarketShare)
                {

                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = item.MemberName;
                    var volumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", topMarketShare.Where(x => x.MemberName == item.MemberName).Sum(x => x.Quantity)));
                    topMarketShareView.MarketShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (volumeInLot * 100) / totalSumBuyQuantity));
                    topMarketShareView.HHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (topMarketShareView.MarketShareInPercent * topMarketShareView.MarketShareInPercent)));
                    listOfTopMarketShare.Add(topMarketShareView);
                }
                List<TopMarketShareViewModel> topMarketShareList = new List<TopMarketShareViewModel>();
                if (searchSqlQuery.Order == true)
                {
                    topMarketShareList = listOfTopMarketShare.OrderByDescending(o => o.MarketShareInPercent)
                                       .Take(searchSqlQuery.TopValue).ToList();
                }
                else
                {
                    topMarketShareList = listOfTopMarketShare.OrderBy(o => o.MarketShareInPercent)
                                       .Take(searchSqlQuery.TopValue).ToList();
                }
                return topMarketShareList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<List<TopMarketShareViewModel>> GetSellerMarketShareList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopMarketShare> topMarketShare = new List<TopMarketShare>();
                topMarketShare = await _context.TopMarketShare.FromSqlRaw<TopMarketShare>(QueryConfiguration.GetQuery(Constants.TopSellerMarketShare), searchSqlQuery.CommodityId,
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopMarketShareViewModel> listOfTopMarketShare = new List<TopMarketShareViewModel>();
                TopMarketShareViewModel topMarketShareView = new TopMarketShareViewModel();
                decimal totalSumSellerQuantity = topMarketShare.Sum(x => x.Quantity);
                var buyerTopMarketShare = topMarketShare.Select(x => new
                {
                    x.MemberName
                }).Distinct();
                foreach (var item in buyerTopMarketShare)
                {

                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = item.MemberName;
                    var volumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", topMarketShare.Where(x => x.MemberName == item.MemberName).Sum(x => x.Quantity)));
                    topMarketShareView.MarketShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (volumeInLot * 100) / totalSumSellerQuantity));
                    topMarketShareView.HHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (topMarketShareView.MarketShareInPercent * topMarketShareView.MarketShareInPercent)));
                    listOfTopMarketShare.Add(topMarketShareView);
                }
                List<TopMarketShareViewModel> topMarketShareList = new List<TopMarketShareViewModel>();
                topMarketShareView = new TopMarketShareViewModel();
                if (searchSqlQuery.Order == true)
                {
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareList = listOfTopMarketShare.OrderByDescending(o => o.MarketShareInPercent)
                                        .Take(searchSqlQuery.TopValue).ToList();
                    topMarketShareView.MemberName = "Others";
                    topMarketShareView.MarketShareInPercent = 100 - topMarketShareList.Sum(x => x.MarketShareInPercent);
                    topMarketShareList.Add(topMarketShareView);

                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = "HHI";
                    topMarketShareView.MarketShareInPercent = topMarketShareList.Sum(x => x.HHIIndex);
                    topMarketShareList.Add(topMarketShareView);
                }
                else
                {
                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareList = listOfTopMarketShare.OrderBy(o => o.MarketShareInPercent)
                                                           .Take(searchSqlQuery.TopValue).ToList();
                    topMarketShareView.MemberName = "Others";
                    topMarketShareView.MarketShareInPercent = 100 - topMarketShareList.Sum(x => x.MarketShareInPercent);
                    topMarketShareList.Add(topMarketShareView);
                    TopMarketShareViewModel hhi = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = "HHI";
                    topMarketShareView.MarketShareInPercent = topMarketShareList.Sum(x => x.HHIIndex);
                    topMarketShareList.Add(topMarketShareView);
                }

                return topMarketShareList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<TopMarketShareViewModel>> GetSellerMarketShareListGraph(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopMarketShare> topMarketShare = new List<TopMarketShare>();
                topMarketShare = await _context.TopMarketShare.FromSqlRaw<TopMarketShare>(QueryConfiguration.GetQuery(Constants.TopSellerMarketShare), searchSqlQuery.CommodityId,
                                                           searchSqlQuery.TradeActorTypeId, searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopMarketShareViewModel> listOfTopMarketShare = new List<TopMarketShareViewModel>();
                TopMarketShareViewModel topMarketShareView = new TopMarketShareViewModel();
                decimal totalSumSellerQuantity = topMarketShare.Sum(x => x.Quantity);
                var buyerTopMarketShare = topMarketShare.Select(x => new
                {
                    x.MemberName
                }).Distinct();
                foreach (var item in buyerTopMarketShare)
                {

                    topMarketShareView = new TopMarketShareViewModel();
                    topMarketShareView.MemberName = item.MemberName;
                    var volumeInLot = Convert.ToDecimal(String.Format("{0:00.0000}", topMarketShare.Where(x => x.MemberName == item.MemberName).Sum(x => x.Quantity)));
                    topMarketShareView.MarketShareInPercent = Convert.ToDecimal(String.Format("{0:00.0000}", (volumeInLot * 100) / totalSumSellerQuantity));
                    topMarketShareView.HHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (topMarketShareView.MarketShareInPercent * topMarketShareView.MarketShareInPercent)));
                    listOfTopMarketShare.Add(topMarketShareView);
                }
                List<TopMarketShareViewModel> topMarketShareList = new List<TopMarketShareViewModel>();
                topMarketShareView = new TopMarketShareViewModel();
                if (searchSqlQuery.Order == true)
                {
                    topMarketShareList = listOfTopMarketShare.OrderByDescending(o => o.MarketShareInPercent)
                                        .Take(searchSqlQuery.TopValue).ToList();
                }
                else
                {
                    topMarketShareList = listOfTopMarketShare.OrderBy(o => o.MarketShareInPercent)
                                                           .Take(searchSqlQuery.TopValue).ToList();
                }

                return topMarketShareList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
