using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class LiveTradeHub : Hub
  {
    private LiveTradeRepository _stockTicker;

    public LiveTradeHub(LiveTradeRepository stockTicker)
    {
      this._stockTicker = stockTicker;
    }

    public IEnumerable<EcxOrderVm> GetLiveTrade()
    {
      return _stockTicker.GetLiveTrade();
    }

    public ChannelReader<EcxOrderVm> StreamStocks()
    {
      return _stockTicker.StreamStocks().AsChannelReader(10);
    }

    public string GetMarketState()
    {
      return _stockTicker.MarketState.ToString();
    }

    public async Task OpenMarket()
    {
      await _stockTicker.OpenMarket();
    }

    public async Task CloseMarket()
    {
      await _stockTicker.CloseMarket();
    }

    public async Task Reset()
    {
      await _stockTicker.Reset();
    }
  }
  }
