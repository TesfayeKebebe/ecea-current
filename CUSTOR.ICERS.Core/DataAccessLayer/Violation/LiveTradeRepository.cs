using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class LiveTradeRepository
    {

        private readonly SemaphoreSlim _marketStateLock = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim _updateStockPricesLock = new SemaphoreSlim(1, 1);

        private readonly ConcurrentDictionary<int, EcxOrderVm> _stocks = new ConcurrentDictionary<int, EcxOrderVm>();

        private readonly Subject<EcxOrderVm> _subject = new Subject<EcxOrderVm>();
        private ECEADbContext _context;
        private ECXTradeDbContext _dbContext;
        // Stock can go up or down by a percentage of this factor on each change
        // private readonly double _rangePercent = 0.002;

        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(250);
        //private readonly Random _updateOrNotRandom = new Random();

        private Timer _timer;
        private volatile bool _updatingStockPrices;
        private volatile MarketState _marketState;
        public IConfiguration _configuration { get; }
        public LiveTradeRepository(IHubContext<LiveTradeHub> hub, IConfiguration configuration)
        {
            Hub = hub;
            // _context = context;
            _configuration = configuration;
            LoadDefaultStocks();
        }

        private IHubContext<LiveTradeHub> Hub
        {
            get;
            set;
        }

        public MarketState MarketState
        {
            get { return _marketState; }
            private set { _marketState = value; }
        }

        public IEnumerable<EcxOrderVm> GetLiveTrade()
        {
            var data = LoadDefaultStocks();
            foreach (var stock in data.Values)
            {
                _subject.OnNext(stock);
            }
            return _stocks.Values;
        }

        public IObservable<EcxOrderVm> StreamStocks()
        {
            var data = LoadDefaultStocks();
            foreach (var stock in data.Values)
            {
                _subject.OnNext(stock);
            }
            return _subject;
        }

        public async Task OpenMarket()
        {
            await _marketStateLock.WaitAsync();
            try
            {
                if (MarketState != MarketState.Open)
                {
                    _timer = new Timer(null, null, _updateInterval, _updateInterval);

                    MarketState = MarketState.Open;

                    await BroadcastMarketStateChange(MarketState.Open);
                }
            }
            finally
            {
                _marketStateLock.Release();
            }
        }

        public async Task CloseMarket()
        {
            await _marketStateLock.WaitAsync();
            try
            {
                if (MarketState == MarketState.Open)
                {
                    if (_timer != null)
                    {
                        _timer.Dispose();
                    }

                    MarketState = MarketState.Closed;

                    await BroadcastMarketStateChange(MarketState.Closed);
                }
            }
            finally
            {
                _marketStateLock.Release();
            }
        }

        public async Task Reset()
        {
            await _marketStateLock.WaitAsync();
            try
            {
                if (MarketState != MarketState.Closed)
                {
                    throw new InvalidOperationException("Market must be closed before it can be reset.");
                }

                LoadDefaultStocks();
                await BroadcastMarketReset();
            }
            finally
            {
                _marketStateLock.Release();
            }
        }

        private ConcurrentDictionary<int, EcxOrderVm> LoadDefaultStocks()
        {
            _stocks.Clear();
            string connectionString = _configuration["ConnectionStrings:DefaultConnection"];
            var optionBuilder = new DbContextOptionsBuilder<ECXTradeDbContext>();
            optionBuilder.UseSqlServer(connectionString);
            _dbContext = new ECXTradeDbContext(optionBuilder.Options);
            int Sn = 1;
            List<EcxOrderVm> ecxOrders = new List<EcxOrderVm>();
            //List<EcxOrderVm> addedecxOrders = new List<EcxOrderVm>();

            List<OpenedSession> openedSessions = new List<OpenedSession>();
            openedSessions = _context.OpenedSession.FromSqlRaw<OpenedSession>(QueryConfiguration.GetQuery(Constants.OpenedSession)).ToList();
            foreach (var open in openedSessions)
            {
                ecxOrders = new List<EcxOrderVm>();
                ecxOrders = _context.EcxOrderVm.FromSqlRaw<EcxOrderVm>(QueryConfiguration.GetQuery(Constants.OnlineEcxOrderByDate), open.SessionId).OrderByDescending(x => x.SubmittedTimestamp).ToList();
                //addedecxOrders.AddRange(ecxOrders);
                ecxOrders.ForEach(stock => _stocks.TryAdd(stock.SN = Sn++, stock));

            }
            return _stocks;
            //var stocks = new List<Stock>
            //{
            //  new Stock { Symbol = "HDFC Bank", Price = 2049.35m },
            //  new Stock { Symbol = "Bharti Airtel", Price = 377.55m },
            //  new Stock { Symbol = "SBI", Price = 273.00m },
            //  new Stock { Symbol = "Reliance", Price = 984.35m }
            //};

        }

        //private async void UpdateStockPrices(object state)
        //{
        //  // This function must be re-entrant as it's running as a timer interval handler
        //  await _updateStockPricesLock.WaitAsync();
        //  try
        //  {
        //    if (!_updatingStockPrices)
        //    {
        //      _updatingStockPrices = true;

        //      foreach (var stock in _stocks.Values)
        //      {
        //        TryUpdateStockPrice(stock);

        //        _subject.OnNext(stock);
        //      }

        //      _updatingStockPrices = false;
        //    }
        //  }
        //  finally
        //  {
        //    _updateStockPricesLock.Release();
        //  }
        //}

        //private bool TryUpdateStockPrice(Stock stock)
        //{
        //  // Randomly choose whether to udpate this stock or not
        //  var r = _updateOrNotRandom.NextDouble();
        //  if (r > 0.1)
        //  {
        //    return false;
        //  }

        //  // Update the stock price by a random factor of the range percent
        //  var random = new Random((int)Math.Floor(stock.Price));
        //  var percentChange = random.NextDouble() * _rangePercent;
        //  var pos = random.NextDouble() > 0.51;
        //  var change = Math.Round(stock.Price * (decimal)percentChange, 2);
        //  change = pos ? change : -change;

        //  stock.Price += change;
        //  return true;
        //}

        private async Task BroadcastMarketStateChange(MarketState marketState)
        {
            switch (marketState)
            {
                case MarketState.Open:
                    await Hub.Clients.All.SendAsync("marketOpened");
                    break;
                case MarketState.Closed:
                    await Hub.Clients.All.SendAsync("marketClosed");
                    break;
                default:
                    break;
            }
        }

        private async Task BroadcastMarketReset()
        {
            await Hub.Clients.All.SendAsync("marketReset");
        }
    }

    public enum MarketState
    {
        Closed,
        Open
    }
}
