using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class MarkingTheCloseRepository
  {
    private readonly ECXTradeDbContext _context;
    public MarkingTheCloseRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<MarkingTheCloseDetail>>  getMarkingTheCloses(DateTime From, DateTime To)
    {
      try
      {
        List<MarkingTheCloseDetail> markingTheCloseDetails = new List<MarkingTheCloseDetail>();
        List<MarkingTheCloseVM> markingTheCloses = new List<MarkingTheCloseVM>();
        markingTheCloses = await _context.MarkingTheCloseVM.FromSqlRaw<MarkingTheCloseVM>("exec dbo.prGetMarkingTheClose {0},{1}", From, To).ToListAsync();
        foreach (var item in markingTheCloses)
        {
          PreviousSessionVM previous = new PreviousSessionVM();
          previous = await _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.PreviousSession), item.CommodityGradeId,item.SessionId).FirstOrDefaultAsync();
          if (previous != null)
          {
            WeightedAveragePriceVM previousPrice = new WeightedAveragePriceVM();
            previousPrice = await _context.WeightedAveragePriceVM.FromSqlRaw<WeightedAveragePriceVM>("exec dbo.pGetWeightAveragePrice {0},{1}", item.CommodityGradeId, previous.LastSessionId).FirstOrDefaultAsync();
            if (previousPrice != null)
            {
              CommodityVM commodityVM = new CommodityVM();
              commodityVM = await _context.CommodityVM.FromSqlRaw<CommodityVM>("exec dbo.prGetCommodity {0}", item.CommodityGradeId).FirstOrDefaultAsync();
              if (commodityVM != null)
              {
                decimal? minimum = 0;
                decimal? maximum = 0;
                if (commodityVM.commodityName == "Coffee")
                {
                  minimum = previousPrice.WeightAveragePrice - 20;
                  maximum = previousPrice.WeightAveragePrice + 20;
                  if (item.Price < minimum || item.Price > maximum)
                  {
                    MarkingTheCloseDetail mcd = new MarkingTheCloseDetail()
                    {
                      buyer = item.buyer,
                      CommodityGrade = item.CommodityGrade,
                      CreatedDate = item.CreatedDate,
                      MaximumAveragePrice = maximum,
                      MinimumAveragePrice = minimum,
                      CommodityGradeId = item.CommodityGradeId,
                      Price = item.Price,
                      ProductionYear = item.ProductionYear,
                      Quantity = item.Quantity,
                      seller = item.seller,
                      SessionId = item.SessionId,
                      sessionName = item.sessionName
                    };
                    markingTheCloseDetails.Add(mcd);

                  }

                }
                else
                {
                  minimum = previousPrice.WeightAveragePrice - 50;
                  maximum = previousPrice.WeightAveragePrice + 50;
                  if (item.Price < minimum || item.Price > maximum)
                  {
                    MarkingTheCloseDetail mcd = new MarkingTheCloseDetail()
                    {
                      buyer = item.buyer,
                      CommodityGrade = item.CommodityGrade,
                      CreatedDate = item.CreatedDate,
                      MaximumAveragePrice = maximum,
                      MinimumAveragePrice = minimum,
                      CommodityGradeId = item.CommodityGradeId,
                      Price = item.Price,
                      ProductionYear = item.ProductionYear,
                      Quantity = item.Quantity,
                      seller = item.seller,
                      SessionId = item.SessionId,
                      sessionName = item.sessionName
                    };
                    markingTheCloseDetails.Add(mcd);

                  }

                }
              }
            }
          }
        }
        return markingTheCloseDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
  }
}
