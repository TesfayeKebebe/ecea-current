﻿using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class MatchingTradeOrderRepository
    {
        private readonly ECEADbContext context;

        public MatchingTradeOrderRepository(ECEADbContext context)
        {
            this.context = context;
        }
        public async Task<List<NotRegistered>> GetMatchingOrderList()
        {
            try
            {
                var notRenewed = await context.NotRegistered.FromSqlRaw<NotRegistered>("EXECUTE spGetMatchingTradeOrder").ToListAsync();
                return notRenewed;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
