﻿using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
   public class MemberNotRenewedRepository
    {
        private readonly ECEADbContext context;

        public MemberNotRenewedRepository(ECEADbContext _context)
        {
            context = _context;
        }
        public async Task<List<NotRegistered>> GetMembersNotRenewedList()
        {
            try
            {
                var notRenewed = await context.NotRegistered.FromSqlRaw<NotRegistered>("EXECUTE spGetRenewedCustomerTrade").ToListAsync();
                return notRenewed;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
