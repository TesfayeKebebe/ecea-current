using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class NewRequestAfterPreOpenRepository
  {
    private readonly ECXTradeDbContext _context;
    public NewRequestAfterPreOpenRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<PreOpenDetails>> getNewRequest(DateTime From, DateTime To)
    {
      List<PreOpenDetails> listOfRequest = new List<PreOpenDetails>();
      try
      {
        List<BidOfferRecordVM> bidOfferRecords = new List<BidOfferRecordVM>();
        bidOfferRecords = await _context.BidOfferRecordVM.FromSqlRaw<BidOfferRecordVM>(QueryConfiguration.GetQuery(Constants.Order ), From, To).ToListAsync();
        foreach (var order in bidOfferRecords)
        {
          PreopenSessionVM preopen = new PreopenSessionVM();
          preopen = await _context.PreopenSessionVM.FromSqlRaw<PreopenSessionVM>("EXEC dbo.pGetPreopenDateBySession {0},{1}", order.SessionId, order.MemberId).FirstOrDefaultAsync();
          if (preopen != null)
          {
            if(preopen.Quantity!= order.Quantity && order.SubmittedTimestamp > preopen.sessionEnd)
            {
              PreOpenDetails preOpenDetails = new PreOpenDetails()
              {
                PreOpenQuantity = preopen.Quantity,
                Quantity = order.Quantity,
                //TransactionOwnerName = order.TransactionOwnerName,
                CommodityGrade = order.CommodityGrade,
                //CreatedDate = order.CreatedDate,
                MemberId = order.MemberId,
                Price = order.Price,
                ProductionYear = order.ProductionYear,
                //MemberCode = order.MemberCode,
                SessionId = order.SessionId,
                //OnlineStartActualTime = order.OnlineStartActualTime,
                PreopenEndDate = preopen.sessionEnd,
                SubmittedTimestamp = order.SubmittedTimestamp,
                SessionStart = preopen.sessionStart,
                Status ="Quantity difference"
              };
              listOfRequest.Add(preOpenDetails);
            }
          }
          else
          {
            preopen = await _context.PreopenSessionVM.FromSqlRaw<PreopenSessionVM>("EXEC dbo.pGetPreopenDateBySession {0}", order.SessionId).FirstOrDefaultAsync();
             if(preopen!=null)
            {
              if (order.SubmittedTimestamp > preopen.sessionEnd)
              {
                PreOpenDetails preOpenDetails = new PreOpenDetails()
                {
                  PreOpenQuantity = preopen.Quantity,
                  Quantity = order.Quantity,
                  CommodityGrade = order.CommodityGrade,
                  //MemberCode = order.MemberCode,
                  MemberId = order.MemberId,
                  Price = order.Price,
                  ProductionYear = order.ProductionYear,
                  //BuyerOrSeller = order.BuyerOrSeller,
                  SessionId = order.SessionId,
                  //OnlineStartActualTime = order.OnlineStartActualTime,
                  PreopenEndDate = preopen.sessionEnd,
                  SubmittedTimestamp = order.SubmittedTimestamp,
                  SessionStart = preopen.sessionStart,
                  Status = "Submit after session end"
                };
                listOfRequest.Add(preOpenDetails);

              }

            }
          

          }
        }
        return listOfRequest;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
  }
}
