using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class NotRegisteredMemberRepository
  {
    private readonly ECEADbContext _context;
    public NotRegisteredMemberRepository(ECEADbContext eCEADbContext)
    {
      _context = eCEADbContext;
    }
     public async Task<List<NotRegistered>> GetNotRegistered(DateTime from, DateTime to)
      {
      try
      {
        List<NotRegistered> addedNotRegistered = new List<NotRegistered>();
        addedNotRegistered = await _context.NotRegistered.FromSqlRaw<NotRegistered>("EXECUTE dbo.prGetNotRegisteredMember {0},{1}",from,to).ToListAsync();
        return addedNotRegistered;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
  
    }
  }
}
