﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty; 
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation.Penalty
{
    public class BlackListRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public BlackListRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<int> PostBlackList(BlackListDto postedBlackList)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    int BlackListId = 0; 
                    BlackListDto newPenality = new BlackListDto
                    {
                        ExchangeActorId = postedBlackList.ExchangeActorId,
                        Issue = postedBlackList.Issue,
                        BlackListLevel = postedBlackList.BlackListLevel,
                        Source = postedBlackList.Source,
                        Remark = postedBlackList.Remark, 
                        CreatedUserId = postedBlackList.CreatedUserId
                    };

                    // maping
                    var _BlackList = _mapper.Map<BlackList>(newPenality);


                    _context.BlackList.Add(_BlackList);
                    await _context.SaveChangesAsync();
                    // updated Id
                    BlackListId = _BlackList.BlackListId;
                     
                    transaction.Commit();
                    return BlackListId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<PagedResult<BlackListViewDto>> GetBlackLists(BlackListQueryParameters parameters)
        {
            try
            {

                int totalSearchResult = 0;
                List<BlackListViewDto> penalityList = null;
                // 
                penalityList = await (from p in _context.BlackList 
                                        join ex in _context.ExchangeActor on p.ExchangeActorId equals ex.ExchangeActorId 
                                        where p.IsDeleted == false && p.IsActive == true
                                        where (
                                        (parameters.BlackListLevel == null ||
                                        (p.BlackListLevel == parameters.BlackListLevel)) &&
                                        (parameters.From == null ||
                                        p.CreatedDateTime.Date >= DateTime.Parse(parameters.From)) &&
                                        (parameters.To == null ||
                                        p.CreatedDateTime.Date <= DateTime.Parse(parameters.To))
                                       )
                                      orderby p.BlackListId descending
                                        select new BlackListViewDto
                                        {
                                            BlackListId = p.BlackListId,
                                            ExchangeActorId = p.ExchangeActorId, 
                                            ExchangeActorName = p.ExchangeActor.OrganizationNameAmh,
                                            ExchangeActor = p.ExchangeActor,
                                            Issue = p.Issue,
                                            BlackListLevel = p.BlackListLevel,
                                            Source = p.Source,
                                            Remark = p.Remark, 
                                        })
                                 .ToListAsync();

                totalSearchResult = penalityList.Count();
                var pagedResult = penalityList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);

                return new PagedResult<BlackListViewDto>()
                {
                    Items = _mapper.Map<List<BlackListViewDto>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<BlackListViewDto> GetBlackListById(string lang, int id)
        {
            try
            {
                Guid exchangeActorId = _context.BlackList.Where(d => d.BlackListId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();
                
                var data = await (from p in _context.BlackList
                                  join ex in _context.ExchangeActor on p.ExchangeActorId equals ex.ExchangeActorId
                                  where p.BlackListId == id
                                  select new BlackListViewDto
                                  {
                                      BlackListId = p.BlackListId,
                                      ExchangeActorId = p.ExchangeActorId,
                                      ExchangeActorName = p.ExchangeActor.OrganizationNameAmh, 
                                      ExchangeActor = p.ExchangeActor, 
                                      Issue = p.Issue,
                                      BlackListLevel = p.BlackListLevel,
                                      Source = p.Source,
                                      Remark = p.Remark,
                                  })
                             .FirstOrDefaultAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

    }
}
