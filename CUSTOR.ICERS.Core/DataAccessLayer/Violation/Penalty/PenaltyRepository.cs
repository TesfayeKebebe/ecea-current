﻿using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation.Penalty
{
    public class PenaltyRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public PenaltyRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<int> PostPenalty(PenaltyDto postedPenalty)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    int penaltyId = 0; 
                    PenaltyDto newPenality = new PenaltyDto
                    {
                        ExchangeActorId = postedPenalty.ExchangeActorId,
                        TypeOfPenaltyId = postedPenalty.TypeOfPenaltyId,
                        DateOfPenalty = postedPenalty.DateOfPenalty,
                        ReasonOfPenalty = postedPenalty.ReasonOfPenalty,
                        PenaltyLetterNo = postedPenalty.PenaltyLetterNo,
                        AmountOfCashPenalty = postedPenalty.AmountOfCashPenalty,
                        PenaltyStartDate = postedPenalty.PenaltyStartDate,
                        PenaltyEndDate = postedPenalty.PenaltyEndDate,
                        CreatedUserId = postedPenalty.CreatedUserId
                    };

                    // maping
                    var _penalty = _mapper.Map<EntityLayer.Violation.Penalty.Penalty>(newPenality);


                    _context.Penalty.Add(_penalty);
                    await _context.SaveChangesAsync();
                    // updated Id
                    penaltyId = _penalty.PenaltyId;
                     
                    transaction.Commit();
                    return penaltyId;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<PagedResult<PenaltyViewDto>> GetPenaltys(PenaltyQueryParameters parameters)
        {
            try
            {

                int totalSearchResult = 0;
                List<PenaltyViewDto> penalityList = null;
                // 
                penalityList = await (from p in _context.Penalty
                                        join cl in _context.Lookup on p.TypeOfPenaltyId equals cl.LookupId 
                                        join ex in _context.ExchangeActor on p.ExchangeActorId equals ex.ExchangeActorId 
                                        where p.IsDeleted == false && p.IsActive == true
                                        where (
                                        (parameters.TypeOfPenalty == null ||
                                        (p.TypeOfPenaltyId == parameters.TypeOfPenalty)) &&
                                        (parameters.From == null || 
                                        p.CreatedDateTime.Date >= DateTime.Parse(parameters.From)) &&
                                        (parameters.To == null ||
                                        p.CreatedDateTime.Date <= DateTime.Parse(parameters.To))
                                        )
                                        orderby p.PenaltyId descending
                                        select new PenaltyViewDto
                                        {
                                            PenaltyId = p.PenaltyId,
                                            ExchangeActorName = p.ExchangeActor.OrganizationNameAmh,
                                            TypeOfPenaltyId =p.TypeOfPenaltyId,
                                            TypeOfPenaltyName= (parameters.Lang == "et") ? cl.DescriptionAmh : cl.DescriptionEng,
                                            PenaltyLetterNo = p.PenaltyLetterNo,
                                            ExchangeActorId = p.ExchangeActorId, 
                                            AmountOfCashPenalty = p.AmountOfCashPenalty,
                                            ReasonOfPenalty = p.ReasonOfPenalty,
                                            ExchangeActor = p.ExchangeActor,
                                            DateOfPenaltyAm = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.DateOfPenalty).ToString()) 
                                            : p.DateOfPenalty.ToString(),
                                            //PenaltyStartDate = (parameters.Lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.PenaltyStartDate).ToString())
                                            //: p.PenaltyStartDate.ToString(),
                                            //PenaltyEndDate = (parameters.Lang == "et") ? EthiopicDateTime
                                            //.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.PenaltyEndDate).ToString()) : p.PenaltyEndDate.ToString(),
                                        })
                                 .ToListAsync();

                totalSearchResult = penalityList.Count();
                var pagedResult = penalityList.AsQueryable().Paging(parameters.PageCount, parameters.PageNumber);

                return new PagedResult<PenaltyViewDto>()
                {
                    Items = _mapper.Map<List<PenaltyViewDto>>(pagedResult),
                    ItemsCount = totalSearchResult
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<PenaltyViewDto> GetPenaltyById(string lang, int id)
        {
            try
            {
                Guid exchangeActorId = _context.Penalty.Where(d => d.PenaltyId == id).FirstOrDefault().ExchangeActorId;
                OwnerManager customerLegalBody = await _context.OwnerManager.Where(clb => clb.ExchangeActorId == exchangeActorId && clb.IsManager == true).AsNoTracking().FirstOrDefaultAsync();



                var data = await (from p in _context.Penalty
                                  join cl in _context.Lookup on p.TypeOfPenaltyId equals cl.LookupId
                                  join ex in _context.ExchangeActor on p.ExchangeActorId equals ex.ExchangeActorId
                                  where p.PenaltyId == id
                                  select new PenaltyViewDto
                                  {
                                      PenaltyId = p.PenaltyId,
                                      TypeOfPenaltyId =p.TypeOfPenaltyId,
                                      TypeOfPenaltyName = (lang == "et") ? cl.DescriptionAmh : cl.DescriptionEng,
                                      PenaltyLetterNo = p.PenaltyLetterNo,
                                      ExchangeActorId = p.ExchangeActorId,
                                      ExchangeActorName = p.ExchangeActor.OrganizationNameAmh,
                                      AmountOfCashPenalty = p.AmountOfCashPenalty,
                                      ReasonOfPenalty = p.ReasonOfPenalty,
                                      ExchangeActor = p.ExchangeActor,
                                      DateOfPenalty = p.DateOfPenalty,
                                      PenaltyStartDate = p.PenaltyStartDate,
                                      PenaltyEndDate = p.PenaltyEndDate,
                                      DateOfPenaltyAm = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.DateOfPenalty).ToString())
                                            : p.DateOfPenalty.ToString(),
                                      //PenaltyStartDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.PenaltyStartDate).ToString())
                                      //      : p.PenaltyStartDate.ToString(),
                                      //PenaltyEndDate = (lang == "et") ? EthiopicDateTime
                                      //      .TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(p.PenaltyEndDate).ToString()) : p.PenaltyEndDate.ToString(),
                                  })
                             .FirstOrDefaultAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }

    }
}
