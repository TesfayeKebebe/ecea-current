using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class PrearrangedTradeRepository
  {
    private readonly ECXTradeDbContext _context;
    public PrearrangedTradeRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<PrearrangedTradeVM>>   GetPrearrangedTrades(DateTime From, DateTime To)
    {
      List<DistinctOrderVM> listOfDistinct = new List<DistinctOrderVM>();
      List<PrearrangedTradeVM> addedPrearrangedTradeVMs = new List<PrearrangedTradeVM>();
      try
      {
        listOfDistinct = await _context.DistinctOrderVM.FromSqlRaw<DistinctOrderVM>("exec dbo.pDistictValueOfOrder {0},{1}", From, To).ToListAsync();
        foreach (var dist in listOfDistinct)
        {
          List<PrearrangedTradeVM> listPrearrangedTradeVMs = new List<PrearrangedTradeVM>();
          PreopenSessionDate preopenDate = new PreopenSessionDate();
          preopenDate = await _context.PreopenSessionDate.FromSqlRaw<PreopenSessionDate>("exec dbo.pGetPreopenSessionDate {0}", dist.SessionId).FirstOrDefaultAsync();
          if (preopenDate != null)
          {
            listPrearrangedTradeVMs = await _context.PrearrangedTradeVM.FromSqlRaw<PrearrangedTradeVM>("exec dbo.pGetPrearrangedTrade {0},{1},{2},{3},{4}", dist.CommodityGradeId, dist.Quantity, dist.LimitPrice, preopenDate.sessionStart, preopenDate.sessionEnd).ToListAsync();
            if (listPrearrangedTradeVMs.Count >= 2)
            {
              addedPrearrangedTradeVMs.AddRange(listPrearrangedTradeVMs);
            }
          }
        }
        return addedPrearrangedTradeVMs;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    
    }
  }
}
