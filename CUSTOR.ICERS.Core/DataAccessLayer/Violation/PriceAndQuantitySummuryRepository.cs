using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class PriceAndQuantitySummuryRepository
  {
    private readonly ECXTradeDbContext _context;
    public PriceAndQuantitySummuryRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<summuryOfQuantityPriceDetail> PriceAndQuantities(DateTime From,DateTime To)
    {
      try
      {
        summuryOfQuantityPriceDetail added = new summuryOfQuantityPriceDetail();
        List<PriceAndQuantitySummury> priceAndQuantities = new List<PriceAndQuantitySummury>();
        priceAndQuantities = await _context.PriceAndQuantitySummury.FromSqlRaw<PriceAndQuantitySummury>(QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantity), From, To).ToListAsync();
        added.PriceAndQuantitySummuries = priceAndQuantities;
        List<PriceAndQuantitySummuryByClass>  classes = new List<PriceAndQuantitySummuryByClass>();
        classes = await _context.PriceAndQuantitySummuryByClass.FromSqlRaw<PriceAndQuantitySummuryByClass>(QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantityByCommodityClass), From, To).ToListAsync();
        added.PriceAndQuantitySummuryByClass = classes;
        List<PriceAndQuantitySummuryByCommodity> commodities = new List<PriceAndQuantitySummuryByCommodity>();
        commodities = await _context.PriceAndQuantitySummuryByCommodity.FromSqlRaw<PriceAndQuantitySummuryByCommodity>(QueryConfiguration.GetQuery(Constants.SummuryOfPriceAndQuantityByCommodity), From, To).ToListAsync();
        added.PriceAndQuantitySummuryByCommodity = commodities;

        return added;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
  }
}
