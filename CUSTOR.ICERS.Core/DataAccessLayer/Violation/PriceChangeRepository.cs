using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class PriceChangeRepository
  {
    public readonly ECXTradeDbContext _context;
    public PriceChangeRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<PriceChangeVM>> getPriceChange(DateTime from, DateTime to)
    {
      try
      {
        List<PriceChangeVM> priceChanges = new List<PriceChangeVM>();
        List<PriceChangeVM> addedPriceChanges = new List<PriceChangeVM>();

        priceChanges = await _context.PriceChangeVM.FromSqlRaw<PriceChangeVM>(QueryConfiguration.GetQuery(Constants.GetPriceByDate), from, to).ToListAsync();
        foreach (var trade in priceChanges)
        {
          PreviousPriceVM previousPrice = new PreviousPriceVM();
          previousPrice = await _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.GetPrice), trade.CommodityGradeId, trade.SessionId).FirstOrDefaultAsync();
          if (previousPrice != null)
          {
            var priceChange = Math.Round(Convert.ToDecimal((trade.tradePrice - previousPrice.WeightAveragePrice) / previousPrice.WeightAveragePrice), 2);
            if (priceChange != 0)
            {
              trade.PriceChange = priceChange;
              addedPriceChanges.Add(trade);
            }
          }
        }
        return addedPriceChanges;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
     

    }
  }
}
