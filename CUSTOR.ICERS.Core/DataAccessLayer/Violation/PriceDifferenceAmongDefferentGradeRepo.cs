using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class PriceDifferenceAmongDefferentGradeRepo
    {
        private readonly ECXTradeDbContext _context;

        public PriceDifferenceAmongDefferentGradeRepo(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<PriceDifferenceAmongDiffGradeView>> GetPriceDifferenceAmongGradeList(PriceDifferenceSqlSearch searchParms)
        {
            try
            {
                List<PriceDifferenceAmongDiffGradeView> priceDifferenceDetails = new List<PriceDifferenceAmongDiffGradeView>();
                var priceHighDiffernceGradeList = await _context.PriceDifferenceAmongDifferentGrade.FromSqlRaw<PriceDifferenceAmongDifferentGrade>(QueryConfiguration.GetQuery(Constants.PriceDifferenceAmongDifferentGrade),
                                                                                        searchParms.From.Date, searchParms.To.Date, searchParms.HighGradeId,searchParms.WareHouseId).ToListAsync();

                var priceLowDiffernceGradeList = await _context.PriceDifferenceAmongDifferentGrade.FromSqlRaw<PriceDifferenceAmongDifferentGrade>(QueryConfiguration.GetQuery(Constants.PriceDifferenceAmongDifferentGrade),
                                                                                                   searchParms.From.Date, searchParms.To.Date, searchParms.LowGradeId, searchParms.WareHouseId).ToListAsync();
                PriceDifferenceAmongDiffGradeView amongDiffGradeView = new PriceDifferenceAmongDiffGradeView();
                foreach (var item in priceHighDiffernceGradeList)
                {
                     amongDiffGradeView = new PriceDifferenceAmongDiffGradeView();
                    var priceDistnictData = priceLowDiffernceGradeList.FirstOrDefault(x => x.Price >= item.Price && 
                                                                                    item.Commodity == x.Commodity &&
                                                                                    x.Symbol != item.Symbol &&
                                                                                    x.TradeDate.Date == item.TradeDate.Date);
                    if(priceDistnictData != null)
                    {
                        amongDiffGradeView.TradeDate = item.TradeDate;
                        amongDiffGradeView.ProductionYear = item.ProductionYear;
                        amongDiffGradeView.Commodity = item.Commodity;
                        amongDiffGradeView.CommodityType = item.CommodityType;
                        amongDiffGradeView.HighGrade = item.Symbol;
                        amongDiffGradeView.HighGradePrice = item.Price;
                        amongDiffGradeView.LowGrade = priceDistnictData.Symbol;
                        amongDiffGradeView.LowGradePrice = priceDistnictData.Price;
                        amongDiffGradeView.Deviation = amongDiffGradeView.LowGradePrice - amongDiffGradeView.HighGradePrice;
                        priceDifferenceDetails.Add(amongDiffGradeView);
                    }

                }
                List<PriceDifferenceAmongDiffGradeView> listOfPriceDifferenceDetails = new List<PriceDifferenceAmongDiffGradeView>();
                listOfPriceDifferenceDetails = priceDifferenceDetails.Where(x => x.Deviation >= searchParms.Deviation).ToList();
                return listOfPriceDifferenceDetails;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
