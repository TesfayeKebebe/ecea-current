using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class PriceDifferncentWarehousesRepository
  {
    private readonly ECXTradeDbContext _context;
    public PriceDifferncentWarehousesRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<WarehouseDetail>> getWareHouse(DateTime From, DateTime To, string WareHouseFrom, string WareHouseTo, decimal Price, string Grade)
    {
      try
      {
        List<WarehouseDetail> warehouseDetails = new List<WarehouseDetail>();
        List<PriceDiffernceBetweenWarehouses> wareHouses = new List<PriceDiffernceBetweenWarehouses>();
        List<PriceDiffernceBetweenWarehouses> wareHouse2 = new List<PriceDiffernceBetweenWarehouses>();
        if(Grade!=null)
        {
          wareHouses = await _context.PriceDiffernceBetweenWarehouses.FromSqlRaw<PriceDiffernceBetweenWarehouses>(QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouse), From, To, WareHouseFrom,Grade).ToListAsync();
          wareHouse2 = await _context.PriceDiffernceBetweenWarehouses.FromSqlRaw<PriceDiffernceBetweenWarehouses>(QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouse), From, To, WareHouseTo,Grade).ToListAsync();
        }
        else
        {
          wareHouses = await _context.PriceDiffernceBetweenWarehouses.FromSqlRaw<PriceDiffernceBetweenWarehouses>(QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouses), From, To, WareHouseFrom).ToListAsync();
          wareHouse2 = await _context.PriceDiffernceBetweenWarehouses.FromSqlRaw<PriceDiffernceBetweenWarehouses>(QueryConfiguration.GetQuery(Constants.PriceDifferenceBetweenWarehouses), From, To, WareHouseTo).ToListAsync();
        }
        foreach (var item in wareHouses)
        {

          var DistnictData = wareHouse2.FirstOrDefault(x => x.TradePrice != item.TradePrice && item.Symbol==x.Symbol && item.Date==x.Date);
          if (DistnictData != null)
          {
            var diff = item.TradePrice - DistnictData.TradePrice;
            if (diff < 0)
            {
              diff = Math.Abs(diff.Value);
            }
            if (diff > Price)
            {
              WarehouseDetail hD = new WarehouseDetail()
              {
                WareHouse = item.WareHouse,
                WareHouseTo = DistnictData.WareHouse,
                WRNo = item.WRNo,
                Quantity = item.Quantity,
                TradePrice = item.TradePrice,
                TradePrice2 = DistnictData.TradePrice,
                Difference = diff,
                QuantityTo = DistnictData.Quantity,
                BuyerMember = item.BuyerMember,
                //SellerClientName = item.SellerClientName,
                SellerMember = item.SellerMember,
                Symbol = item.Symbol,
                 Date = item.Date
              };
              warehouseDetails.Add(hD);
            }
          }
        }

        return warehouseDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
 

    }
    public async Task<List<WareHouse>> getAllWareHouse()
    {
      List<WareHouse> wareHouses = new List<WareHouse>();
      wareHouses = await _context.WareHouse.FromSqlRaw<WareHouse>(QueryConfiguration.GetQuery(Constants.AllWareHouse)).ToListAsync();
      return wareHouses;
    }

  }
}
