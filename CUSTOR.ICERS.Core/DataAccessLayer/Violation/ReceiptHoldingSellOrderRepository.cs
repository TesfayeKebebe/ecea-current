using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
   public class ReceiptHoldingSellOrderRepository
    {
        private readonly ECXTradeDbContext _context;

        public ReceiptHoldingSellOrderRepository(ECXTradeDbContext context)
        {
           _context = context;
        }
        public async Task<ReceiptHoldingSellOrderView> GetReceiptHoldingSellOrderList(SearchParmsQuery searchParms)
        {
            try
            {
                ReceiptHoldingSellOrderView details = new ReceiptHoldingSellOrderView();
                if (searchParms.WarehouseId != "undefined")
                {
                    List<string> warehouseList = searchParms.WarehouseId.ToUpper().Split(",").ToList();

                    warehouseList.Remove("");

                    List<string> wareHouse = new List<string>();

                    foreach (string ware in warehouseList)
                    {
                        wareHouse.Add(ware);
                    }
                    var parameters = new[] { wareHouse };
                    var receiptHoldingList = await _context.ReceiptHoldingSellOrder.FromSqlRaw<ReceiptHoldingSellOrder>(QueryConfiguration.GetQuery(Constants.ReceiptHoldingVsSellOrder),
                                                                                             searchParms.CommodityId,
                                                                                             searchParms.From.Date, searchParms.To.Date, searchParms.Ratio).ToListAsync();
                    var wareHouseList = receiptHoldingList.Where(x => x.Ratio >= searchParms.Ratio &&
                                                                 wareHouse.Contains(x.WarehouseId)).ToList();
                   
                    if (wareHouseList.Any())
                    {
                        decimal sumOfWareHouseQ = Convert.ToDecimal(wareHouseList.Sum(x => x.WarehouseQuantity));
                        decimal sumOfOrderQ = wareHouseList.Sum(x => x.OrderQuantity);
                        details.ReceiptHoldingSellOrder = wareHouseList.ToList();
                        Totals rate = new Totals();
                        rate.Total = Convert.ToDecimal(String.Format("{0:00.0000000}", sumOfOrderQ / sumOfWareHouseQ));
                        details.Totals = rate;
                    }
                }
                else
                {
                    var receiptHoldingList = await _context.ReceiptHoldingSellOrder.FromSqlRaw<ReceiptHoldingSellOrder>(QueryConfiguration.GetQuery(Constants.ReceiptHoldingVsSellOrder),
                                                                                             searchParms.CommodityId,
                                                                                             searchParms.From.Date, searchParms.To.Date, searchParms.Ratio).ToListAsync();
                    if (receiptHoldingList.Any())
                    {
                        decimal sumOfWareHouseQ = Convert.ToDecimal(receiptHoldingList.Sum(x => x.WarehouseQuantity));
                        decimal sumOfOrderQ = receiptHoldingList.Sum(x => x.OrderQuantity);
                        details.ReceiptHoldingSellOrder = receiptHoldingList.ToList();
                        Totals rate = new Totals();
                        rate.Total = Convert.ToDecimal(String.Format("{0:00.0000000}", sumOfOrderQ / sumOfWareHouseQ));
                        details.Totals = rate;
                    }
                }
              
                return details;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ReceiptHoldingSellOderGraph>> GetReceiptHoldingSellOrderGraph(SearchParmsQuery searchParms)
        {
            try
            {
                var receiptHoldingList = await _context.ReceiptHoldingSellOderGraph.FromSqlRaw<ReceiptHoldingSellOderGraph>(QueryConfiguration.GetQuery(Constants.ReceiptHoldingvsSellOrderGraph),searchParms.WarehouseId,
                                                                                         searchParms.CommodityId, searchParms.From.Date, searchParms.To.Date).ToListAsync();
                return receiptHoldingList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
