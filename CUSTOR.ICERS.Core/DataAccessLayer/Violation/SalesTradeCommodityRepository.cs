using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
public  class SalesTradeCommodityRepository
  {
    private readonly ECXTradeDbContext _context;

    public SalesTradeCommodityRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<SalesTradeCommodity>> GetSalesTrades(DateTime From, DateTime To)
    {
      List<SalesTradeCommodity> commodities = new List<SalesTradeCommodity>();
      commodities = await _context.SalesTradeCommodity.FromSqlRaw<SalesTradeCommodity>(QueryConfiguration.GetQuery(Constants.TradeVsSalesCommodity), From,To).ToListAsync();
      return commodities;

    }
  }
}
