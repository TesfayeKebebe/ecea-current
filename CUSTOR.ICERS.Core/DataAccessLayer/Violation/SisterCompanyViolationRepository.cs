using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class SisterCompanyViolationRepository
  {
    private readonly ECEADbContext _context;
    public SisterCompanyViolationRepository(ECEADbContext eCEADbContext)
    {
      _context = eCEADbContext;
    }
    public async Task<List<SisterCompanyViolation>> GetSisterCompanyViolations(DateTime From, DateTime To)
    {
      try
      {
        List<SisterCompanyViolation> addedSisterCompanyViolation = new List<SisterCompanyViolation>();
        addedSisterCompanyViolation = await _context.SisterCompanyViolation.FromSqlRaw<SisterCompanyViolation>("EXECUTE dbo.prGetSisterCompanyViolation {0},{1}", From, To).ToListAsync();
        return addedSisterCompanyViolation;
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }

    }
  }
}
