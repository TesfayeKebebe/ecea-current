﻿using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class SuspendedExchangeactorRepository
    {
        private readonly ECEADbContext context;

        public SuspendedExchangeactorRepository(ECEADbContext _context)
        {
            context = _context;
        }
        public async Task<List<NotRegistered>> GetSuspendedExchangeActorList()
        {
            try
            {
                var suspendedactorlist = await context.NotRegistered.FromSqlRaw<NotRegistered>("EXECUTE spGetSuspendedExchangeactor").ToListAsync();
                return suspendedactorlist;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
