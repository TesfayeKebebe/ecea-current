using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class TopLinkRepository
    {
        private readonly ECXTradeDbContext _context;

        public TopLinkRepository(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<TopLinkViewModel>> GetTopLinkList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopLink> topLink = new List<TopLink>();
                topLink = await _context.TopLink.FromSqlRaw<TopLink>(QueryConfiguration.GetQuery(Constants.TopLink),
                                                         searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopLinkViewModel> listOfTopLink = new List<TopLinkViewModel>();
                TopLinkViewModel topLinkView = new TopLinkViewModel();
                decimal totalSumSellQuantity = topLink.Sum(x => x.SellerQuantity);
                var topLinkViewModel = topLink.Select(x => new
                {
                    x.BuyerCommodity,
                    x.BuyerMemberId,
                    x.BuyerMemberName,
                    x.SellerCommodity,
                    x.SellerMemberId,
                    x.SellerMemberName
                }).Distinct();
                foreach (var item in topLinkViewModel)
                {
                    topLinkView = new TopLinkViewModel();
                    var sellQuantity = topLink.Where(x => x.BuyerCommodity == item.BuyerCommodity &&
                                                                  x.SellerMemberId == item.SellerMemberId).ToList();
                    var totalQuantity = sellQuantity.Sum(x => x.SellerQuantity);
                    topLinkView.BuyerMemberId = item.BuyerMemberId;
                    topLinkView.BuyerMemberName = item.BuyerMemberName;
                    topLinkView.BuyerCommodity = item.BuyerCommodity;
                    topLinkView.SellerMemberId = item.SellerMemberId;
                    topLinkView.SellerMemberName = item.SellerMemberName;
                    topLinkView.SellerCommodity = item.SellerCommodity;
                    topLinkView.FrequencyOfTrade = topLink.Where(x => x.BuyerCommodity == item.BuyerCommodity &&
                                                                    x.BuyerMemberId == item.BuyerMemberId &&
                                                                    x.BuyerMemberName == item.BuyerMemberName &&
                                                                    x.SellerCommodity == item.SellerCommodity &&
                                                                    x.SellerMemberId == item.SellerMemberId &&
                                                                    x.SellerMemberName == item.SellerMemberName).Count();
                    topLinkView.Quantity = topLink.Where(x => x.BuyerCommodity == item.BuyerCommodity &&
                                                                    x.BuyerMemberId == item.BuyerMemberId &&
                                                                    x.BuyerMemberName == item.BuyerMemberName &&
                                                                    x.SellerCommodity == item.SellerCommodity &&
                                                                    x.SellerMemberId == item.SellerMemberId &&
                                                                    x.SellerMemberName == item.SellerMemberName).Sum(x => x.BuyerQuantity);

                    topLinkView.ShareQuantity = (topLinkView.Quantity * 100) / totalQuantity;
                    listOfTopLink.Add(topLinkView);

                }
                var topLinkOrder = listOfTopLink.Where(x => x.FrequencyOfTrade >= searchSqlQuery.NoOfFrequency).OrderByDescending(x => x.FrequencyOfTrade).ToList();
                return topLinkOrder;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TopLinkViewModel>> GetTopLinkClientToClientList(HHISearchSqlQuery searchSqlQuery)
        {
            try
            {
                List<TopLink> topLink = new List<TopLink>();
                topLink = await _context.TopLink.FromSqlRaw<TopLink>(QueryConfiguration.GetQuery(Constants.TopLinkClientToClient),
                                                         searchSqlQuery.From, searchSqlQuery.To).ToListAsync();
                List<TopLinkViewModel> listOfTopLink = new List<TopLinkViewModel>();
                TopLinkViewModel topLinkView = new TopLinkViewModel();
                decimal totalSumSellQuantity = topLink.Sum(x => x.SellerQuantity);
                var topLinkViewModel = topLink.Select(x => new
                {
                    x.BuyerCommodity,
                    x.BuyerMemberId,
                    x.BuyerMemberName,
                    x.SellerCommodity,
                    x.SellerMemberId,
                    x.SellerMemberName
                }).Distinct();
                foreach (var item in topLinkViewModel)
                {
                    topLinkView = new TopLinkViewModel();
                    var sellQuantity = topLink.Where(x => x.SellerCommodity == item.SellerCommodity &&
                                                         x.SellerMemberId == item.SellerMemberId).ToList();
                    var totalQuantity = sellQuantity.Sum(x => x.SellerQuantity);
                    topLinkView.BuyerMemberId = item.BuyerMemberId;
                    topLinkView.BuyerMemberName = item.BuyerMemberName;
                    topLinkView.BuyerCommodity = item.BuyerCommodity;
                    topLinkView.SellerMemberId = item.SellerMemberId;
                    topLinkView.SellerMemberName = item.SellerMemberName;
                    topLinkView.SellerCommodity = item.SellerCommodity;
                    topLinkView.FrequencyOfTrade = topLink.Where(x => x.BuyerCommodity == item.BuyerCommodity &&
                                                                    x.BuyerMemberId == item.BuyerMemberId &&
                                                                    x.BuyerMemberName == item.BuyerMemberName &&
                                                                    x.SellerCommodity == item.SellerCommodity &&
                                                                    x.SellerMemberId == item.SellerMemberId &&
                                                                    x.SellerMemberName == item.SellerMemberName).Count();
                    topLinkView.Quantity = topLink.Where(x => x.BuyerCommodity == item.BuyerCommodity &&
                                                              x.BuyerMemberId == item.BuyerMemberId &&
                                                               x.BuyerMemberName == item.BuyerMemberName &&
                                                                    x.SellerCommodity == item.SellerCommodity &&
                                                                    x.SellerMemberId == item.SellerMemberId &&
                                                                    x.SellerMemberName == item.SellerMemberName).Sum(x => x.BuyerQuantity);

                    topLinkView.ShareQuantity = (topLinkView.Quantity * 100) / totalQuantity;
                    listOfTopLink.Add(topLinkView);

                }
                var topLinkOrder = listOfTopLink.Where(x => x.FrequencyOfTrade >= searchSqlQuery.NoOfFrequency).OrderByDescending(x => x.FrequencyOfTrade).ToList();
                return topLinkOrder;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
