using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class TopTradeExecutionRepository
    {
        private readonly ECXTradeDbContext _context;

        public TopTradeExecutionRepository(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<TopTradeExecution>> GetTopTradeExecutionList(TopSearchSQLParms searchSQLParms)
        {
            try
            {
                List<TopTradeExecution> topTradeExcution = new List<TopTradeExecution>();
                switch (searchSQLParms.TopDescription)
                {
                    case 1:
                        topTradeExcution = await _context.TopTradeExecution.FromSqlRaw<TopTradeExecution>(QueryConfiguration.GetQuery(Constants.TopGenerateByComodity), 
                                                                                   searchSQLParms.TopType,searchSQLParms.WarehouseId,
                                                                                   searchSQLParms.From, searchSQLParms.To,searchSQLParms.TopValue).ToListAsync();
                        break;
                    case 2:
                        topTradeExcution = await _context.TopTradeExecution.FromSqlRaw<TopTradeExecution>(QueryConfiguration.GetQuery(Constants.TopGenerateByComodityGrade),
                                                                                    searchSQLParms.TopType, searchSQLParms.WarehouseId,
                                                                                   searchSQLParms.From, searchSQLParms.To, searchSQLParms.TopValue).ToListAsync();
                        break;
                    case 3:
                        if(searchSQLParms.SellerBuyer == 1)
                        {
                            topTradeExcution = await _context.TopTradeExecution.FromSqlRaw<TopTradeExecution>(QueryConfiguration.GetQuery(Constants.TopGenerateBuyerExchangeActor),
                                                                                       searchSQLParms.TopType, searchSQLParms.WarehouseId, searchSQLParms.From,
                                                                                         searchSQLParms.To, searchSQLParms.TopValue).ToListAsync();
                        }
                        else
                        {
                            topTradeExcution = await _context.TopTradeExecution.FromSqlRaw<TopTradeExecution>(QueryConfiguration.GetQuery(Constants.TopGenerateSellerExchangeActor), 
                                                                                         searchSQLParms.TopType,searchSQLParms.WarehouseId,searchSQLParms.From, 
                                                                                         searchSQLParms.To,searchSQLParms.TopValue).ToListAsync();
                        }                        
                        break;
                    case 4:
                        topTradeExcution = await _context.TopTradeExecution.FromSqlRaw<TopTradeExecution>(QueryConfiguration.GetQuery(Constants.TopGenerateSellerExchangeActor),
                                                                                     searchSQLParms.TopType,searchSQLParms.CommodityId ,
                                                                                     searchSQLParms.From, searchSQLParms.To,searchSQLParms.TopValue).ToListAsync();
                        break;
                    default:
                        break;
                }

                return topTradeExcution;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
