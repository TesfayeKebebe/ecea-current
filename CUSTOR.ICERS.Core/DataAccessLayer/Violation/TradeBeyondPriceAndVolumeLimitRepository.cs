using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class BeyondPriceAndVolumeRepository
  {
    public readonly ECXTradeDbContext _context;
    public BeyondPriceAndVolumeRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task<List<PriceVolumeLimitDetail>> getTradePriceWithVolum(DateTime From, DateTime To, decimal VolumLimit, int percentage)
    {
      try
      {
        List<PriceChangeVM> priceChanges = new List<PriceChangeVM>();
        List<PriceVolumeLimitDetail> BeyondPriceVolumLimit = new List<PriceVolumeLimitDetail>();

        priceChanges = await _context.PriceChangeVM.FromSqlRaw<PriceChangeVM>(QueryConfiguration.GetQuery(Constants.TradeBeyondPrice), From, To).ToListAsync();
        foreach (var trade in priceChanges)
        {
          PreviousSessionVM previous = new PreviousSessionVM();
          previous = await _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.PreviousSession), trade.CommodityGradeId, trade.SessionId).FirstOrDefaultAsync();
          if (previous != null)
          {
            PriceRangeVM previousPrice = new PriceRangeVM();
            previousPrice = await _context.PriceRangeVM.FromSqlRaw<PriceRangeVM>(QueryConfiguration.GetQuery(Constants.PriceRange), trade.CommodityGradeId, previous.LastSessionId, percentage).FirstOrDefaultAsync();
            if (previousPrice != null)
            {

              if (trade.tradePrice < previousPrice.MinPrice || trade.tradePrice > previousPrice.MaxPrice)
              {
                PriceVolumeLimitDetail priceChangeVM = new PriceVolumeLimitDetail()
                {
                  tradePrice = trade.tradePrice,
                  buyerMember = trade.buyerMember,
                  buyTicketNo = trade.buyTicketNo,
                  CommodityGrade = trade.CommodityGrade,
                  CommodityGradeId = trade.CommodityGradeId,
                  productionYear = trade.productionYear,
                  TradeDate = trade.TradeDate,
                  tradeQuantity = trade.tradeQuantity,
                  MaxPrice = previousPrice.MaxPrice,
                  MinPrice = previousPrice.MinPrice

                };

                BeyondPriceVolumLimit.Add(priceChangeVM);
              }
              if (trade.tradeQuantity > VolumLimit && VolumLimit > 0)
              {
                PriceVolumeLimitDetail priceChangeVM = new PriceVolumeLimitDetail()
                {
                  tradePrice = trade.tradePrice,
                  buyerMember = trade.buyerMember,
                  buyTicketNo = trade.buyTicketNo,
                  CommodityGrade = trade.CommodityGrade,
                  CommodityGradeId = trade.CommodityGradeId,
                  productionYear = trade.productionYear,
                  TradeDate = trade.TradeDate,
                  tradeQuantity = trade.tradeQuantity,
                  MaxPrice = previousPrice.MaxPrice,
                  MinPrice = previousPrice.MinPrice

                };

                BeyondPriceVolumLimit.Add(priceChangeVM);
              }
            }
          }
        
        }
        return BeyondPriceVolumLimit;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }
  }
}
