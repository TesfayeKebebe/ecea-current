using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class TradeCancellationRepository
    {
        private readonly ECXTradeDbContext _context;

        public TradeCancellationRepository(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<TradeCancellationView>> GetTradeCancellationList(TradeCancellationSqlQuery sqltradeCancellation)
        {
            try
            {
                List<TradeCancellation> tradeCancellation = new List<TradeCancellation>();
                tradeCancellation = await _context.TradeCancellation.FromSqlRaw<TradeCancellation>(QueryConfiguration.GetQuery(Constants.TradeCancellation),
                                                         sqltradeCancellation.From, sqltradeCancellation.To).ToListAsync();
                List<TradeCancellationView> tradeCancellationViews = new List<TradeCancellationView>();
                var tradeCancellationList = tradeCancellation.Select(x => new { x.Id }).Distinct();
                foreach (var item in tradeCancellationList)
                {
                    TradeCancellationView cancellationView = new TradeCancellationView();
                    cancellationView.IssueDescription = tradeCancellation.Where(x => x.Id == item.Id).Select(x => x.IssueDescription).FirstOrDefault();
                    cancellationView.IssueType = tradeCancellation.Where(x => x.Id == item.Id).Select(x => x.IssueType).FirstOrDefault();
                    var reason = tradeCancellation.Where(x => x.Id == item.Id).Select(x => x.RejectionReason).FirstOrDefault();
                    cancellationView.RejectionReason = reason != null ? reason : "No Rejection Reason Record on the System";
                    tradeCancellationViews.Add(cancellationView);

                }
                return tradeCancellationViews;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<TradeCancellationFigure>> GetTradeCancellationFigureList(TradeCancellationSqlQuery sqltradeCancellation)
        {
            try
            {
                var cancellationFigure = await _context.TradeCancellationFigure.FromSqlRaw<TradeCancellationFigure>(QueryConfiguration.GetQuery(Constants.TradeCancellationFigure),
                                                        sqltradeCancellation.IssueTypeId, sqltradeCancellation.From, sqltradeCancellation.To).ToListAsync();
                return cancellationFigure;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
