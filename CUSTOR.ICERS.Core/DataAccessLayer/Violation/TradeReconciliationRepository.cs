using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using CUSTOR.ICERS.Core.Common;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class TradeReconciliationRepository
    {
        private readonly ECXTradeDbContext _context;
        private readonly ECEADbContext _dbContext;

        public TradeReconciliationRepository(ECXTradeDbContext context,ECEADbContext dbContext)
        {
            _context = context;
            _dbContext = dbContext;
        }
        public async Task<List<ComparingTradeReconciliation>> GetTradeReconcilationList(SearchParmsQuery searchParms)
        {
            try
            {
                var reconcilationList = await _context.ComparingTradeReconciliation.FromSqlRaw<ComparingTradeReconciliation>(QueryConfiguration.GetQuery(Constants.TradeReconciliation),
                                              searchParms.From.Date, searchParms.To.Date).ToListAsync();
                return reconcilationList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TradeReconcilationFigureViewModel> GetTradeFigureList(SearchParmsQuery searchParms)
        {
            try
            {
                var reconcilationList = _context.TradeReconcilationFigure.FromSqlRaw<TradeReconcilationFigure>(QueryConfiguration.GetQuery(Constants.TradeReconcilationFigure)
                                                                                       , searchParms.From.Date, searchParms.To.Date).ToList();
                List<TradeReconcilationFigureViewModel> listOfTradeReconcile = new List<TradeReconcilationFigureViewModel>();
                TradeReconcilationFigureViewModel reconcilationFigure = new TradeReconcilationFigureViewModel();
                reconcilationFigure.Accepted = reconcilationList.Where(x => x.StatusId == 2).Count();
                reconcilationFigure.Rejected = reconcilationList.Where(x => x.StatusId == 3).Count();
                reconcilationFigure.AcceptedReconciled = reconcilationList.Where(x => x.StatusId == 4).Count();
                reconcilationFigure.RejectedReconciled = reconcilationList.Where(x => x.StatusId == 5).Count();
                reconcilationFigure.Settled = reconcilationList.Where(x => x.StatusId == 6).Count();
                listOfTradeReconcile.Add(reconcilationFigure);
                return listOfTradeReconcile;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TradeReconcilationFigureViewModel> GetTradeExecutionFigureList(SearchParmsQuery searchParms)
        {
            try
            {
                var reconcilationList = _dbContext.TradeExecution.Where(x => x.TradedTimestamp.Date >= searchParms.From.Date &&
                                                                         x.TradedTimestamp.Date <= searchParms.To.Date).ToList();
                List<TradeReconcilationFigureViewModel> listOfTradeReconcile = new List<TradeReconcilationFigureViewModel>();
                TradeReconcilationFigureViewModel reconcilationFigure = new TradeReconcilationFigureViewModel();
                reconcilationFigure.Accepted = reconcilationList.Where(x => x.StatusId == 2).Count();
                reconcilationFigure.Rejected = reconcilationList.Where(x => x.StatusId == 3).Count();
                listOfTradeReconcile.Add(reconcilationFigure);
                return listOfTradeReconcile;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TradeExecutionView> GetTradeExecutionList(SearchParmsQuery searchParms)
        {
            try
            {
                var reconcilationList = _dbContext.TradeExecution.Where(x =>x.TradedTimestamp.Date >= searchParms.From.Date &&
                                                                         x.TradedTimestamp.Date <= searchParms.To.Date)
                                               .Select(n=> new TradeExecutionView
                                               {
                                                   TradeDate = n.TradedTimestamp,
                                                   BuyerOrganizationName = n.BuyerOrganizationName,
                                                   BuyerRepresentativeName = n.BuyerRepresentativeName,
                                                   Commodity = n.Commodity,
                                                   CommodityGrade = n.CommodityGrade,
                                                   CommodityType = n.CommodityType,
                                                   Price = n.Price,
                                                   Quantity = n.Quantity,
                                                   SellerOrganizationName = n.SellerOrganizationName,
                                                   SellerRepresentativeName = n.SellerRepresentativeName,
                                                   StatusId = n.StatusId == 2? "Accepted": "Rejected"

                                               })
                    
                    .ToList();
               
                return reconcilationList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<TradeReconcilationFigureViewModel> GetTradeReconcilationDifferenceList(SearchParmsQuery searchParms)
        {
            try
            {
                var tradeExecutionList = _dbContext.TradeExecution.Where(x => x.TradedTimestamp.Date >= searchParms.From.Date &&
                                                                         x.TradedTimestamp.Date <= searchParms.To.Date).ToList();
                var reconcilationList = _context.TradeReconcilationFigure.FromSqlRaw<TradeReconcilationFigure>(QueryConfiguration.GetQuery(Constants.TradeReconcilationFigure)
                                                                                       , searchParms.From.Date, searchParms.To.Date).ToList();
                List<TradeReconcilationFigureViewModel> listOfTradeReconcile = new List<TradeReconcilationFigureViewModel>();
                var previousAccepted = tradeExecutionList.Where(x => x.StatusId == 2).Count();
                var previousRejected = tradeExecutionList.Where(x => x.StatusId == 3).Count();
                TradeReconcilationFigureViewModel reconcilationFigure = new TradeReconcilationFigureViewModel();
               var currentAccepted = reconcilationList.Where(x => x.StatusId == 2).Count();
                var currentRejected = reconcilationList.Where(x => x.StatusId == 3).Count();
                reconcilationFigure.Accepted = previousAccepted - currentAccepted;
                reconcilationFigure.Rejected = previousRejected - currentRejected;
                reconcilationFigure.AcceptedReconciled = reconcilationList.Where(x => x.StatusId == 4).Count();
                reconcilationFigure.RejectedReconciled = reconcilationList.Where(x => x.StatusId == 5).Count();
                reconcilationFigure.Settled = reconcilationList.Where(x => x.StatusId == 6).Count();
                reconcilationFigure.ExpectedReconciled = Math.Abs(reconcilationFigure.Settled - reconcilationFigure.RejectedReconciled);
                reconcilationFigure.ExpectedSettled =Math.Abs((reconcilationFigure.Accepted + reconcilationFigure.AcceptedReconciled) - reconcilationFigure.RejectedReconciled);
                reconcilationFigure.Difference = Math.Abs(previousAccepted -reconcilationFigure.ExpectedReconciled);
                listOfTradeReconcile.Add(reconcilationFigure);
                return listOfTradeReconcile;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
