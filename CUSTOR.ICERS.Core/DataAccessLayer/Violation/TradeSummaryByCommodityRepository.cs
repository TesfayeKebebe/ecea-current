using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class TradeSummaryByCommodityRepository
  {
    public readonly ECXTradeDbContext _context;
    public TradeSummaryByCommodityRepository(ECXTradeDbContext context)
    {
      _context = context;
    }
    public async Task< List<TradeSummaryDetail>>   GetTradeSummaryDetails(DateTime BeforeFrom,DateTime BeforeTo,DateTime CurrentFrom, DateTime CurrentTo)
    {
      try
      {
        List<TradeSummaryDetail> addedDetails = new List<TradeSummaryDetail>();
        List<TradeSummaryByCommodity> current = new List<TradeSummaryByCommodity>();
        List<TradeSummaryByCommodity> before = new List<TradeSummaryByCommodity>();
        current = await _context.TradeSummaryByCommodity.FromSqlRaw<TradeSummaryByCommodity>("exec dbo.pGetTradeSummaryByCommodity {0},{1}", CurrentFrom, CurrentTo).ToListAsync();
        before = await _context.TradeSummaryByCommodity.FromSqlRaw<TradeSummaryByCommodity>("exec dbo.pGetTradeSummaryByCommodity {0},{1}", BeforeFrom, BeforeTo).ToListAsync();
        foreach (var item in current)
        {
          var bef = before.Where(x => x.Description == item.Description).FirstOrDefault();
          TradeSummaryDetail detail = new TradeSummaryDetail();
          detail.BeforePrice = bef?.Price;
          detail.BeforeQuantity = bef?.Quantity;
          detail.QuantityChange = item.Quantity - Convert.ToDouble(bef?.Quantity);
          detail.PriceChange = item.Price - Convert.ToDecimal(bef?.Price);
          detail.Quantity = item.Quantity;
          detail.Price = item?.Price;
          detail.Description = item.Description;
          addedDetails.Add(detail);
        }
        return addedDetails;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
     
    }

  }
}
