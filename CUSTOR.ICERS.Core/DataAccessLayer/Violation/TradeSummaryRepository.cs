using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
    public class TradeSummaryRepository
    {
        private readonly ECXTradeDbContext _context;

        public TradeSummaryRepository(ECXTradeDbContext context)
        {
            _context = context;
        }
        public async Task<List<TradeSummaryViewModel>> GetTradeSummaryList(SearchParmsQuery searchParms)
        {
            try
            {
                var tradeSummaryList = await _context.TradeSummary.FromSqlRaw<TradeSummary>(QueryConfiguration.GetQuery(Constants.TradeSummaryByCommodity),
                                                                                         searchParms.From.Date, searchParms.To.Date).ToListAsync();
                List<TradeSummaryViewModel> listOfTradeSummary = new List<TradeSummaryViewModel>();
                TradeSummaryViewModel tradeSummaryView = new TradeSummaryViewModel();
                var tradeSummary = tradeSummaryList.Select(x => new
                {
                    x.Symbol,
                    x.Commodity
                }).Distinct();
                var tradeCommditGradeSummary = tradeSummaryList.Select(x => new
                {                   
                   // x.CommodityGrade
                }).Distinct();
                var clientTotalSellerQuantity = tradeSummaryList.Where(x => x.IsSellerClient == true).Sum(x => x.SellerQuantity);
                if(clientTotalSellerQuantity == 0)
                {
                    clientTotalSellerQuantity = 1;
                }
                var memberTotalSellerQuantity = tradeSummaryList.Where(x => x.IsSellerClient == false).Sum(x => x.SellerQuantity);
                if(memberTotalSellerQuantity == 0)
                {
                    memberTotalSellerQuantity = 1;
                }
                var clientTotalbuyerQuantity = tradeSummaryList.Where(x => x.IsBuyerClient == true).Sum(x => x.Quantity);
                if(clientTotalbuyerQuantity == 0)
                {
                    clientTotalbuyerQuantity = 1;
                }
                var memberTotalBuyerQuantity = tradeSummaryList.Where(x => x.IsBuyerClient == false).Sum(x => x.Quantity);
                if(memberTotalBuyerQuantity == 0)
                {
                    memberTotalBuyerQuantity = 1;
                }
                foreach (var item in tradeSummary)
                {
                    tradeSummaryView = new TradeSummaryViewModel();
                    tradeSummaryView.Commodity = item.Commodity;
                    tradeSummaryView.NoOfBuyerClient = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                                 x.IsBuyerClient == true).Count();
                    tradeSummaryView.NoOfBuyerMember = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                                 x.IsBuyerClient == false).Count();
                    tradeSummaryView.NoOfSellerClient = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                                 x.IsSellerClient == true).Count();
                    tradeSummaryView.NoOfSellerMember = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                                 x.IsSellerClient == false).Count();
                    tradeSummaryView.Price = tradeSummaryList.Where(x => x.Commodity == item.Commodity).Sum(x => x.Price);
                    tradeSummaryView.Quantity = tradeSummaryList.Where(x => x.Commodity == item.Commodity).Sum(x => x.Quantity);
                    tradeSummaryView.TransactionValue = tradeSummaryView.Quantity * tradeSummaryView.Price;
                    if (item.Symbol == "Co")
                    {
                        tradeSummaryView.WeightAveragePrice = ((tradeSummaryView.Price * tradeSummaryView.Quantity) / tradeSummaryView.Quantity) * 17;
                    }
                    else
                    {
                        tradeSummaryView.WeightAveragePrice = ((tradeSummaryView.Price * tradeSummaryView.Quantity) / tradeSummaryView.Quantity) * 100;
                    }
                    if(tradeSummaryView.NoOfSellerMember == 0)
                    {
                        tradeSummaryView.NoOfSellerMember = 1;
                    }
                    tradeSummaryView.MemberBuySellRatio = (tradeSummaryView.NoOfBuyerMember / tradeSummaryView.NoOfSellerMember);
                    if(tradeSummaryView.NoOfSellerClient == 0)
                    {
                        tradeSummaryView.NoOfSellerClient = 1;
                    }
                    tradeSummaryView.ClientBuySellRatio = (tradeSummaryView.NoOfBuyerClient / tradeSummaryView.NoOfSellerClient);
                    if (tradeSummaryView.NoOfBuyerMember == 0)
                    {
                        tradeSummaryView.NoOfBuyerMember = 1;
                    }
                    tradeSummaryView.PerCapitaBuyMember = tradeSummaryView.Quantity / tradeSummaryView.NoOfBuyerMember;
                    if (tradeSummaryView.NoOfBuyerClient == 0)
                    {
                        tradeSummaryView.NoOfBuyerClient = 1;
                    }
                    tradeSummaryView.PerCapitaSellMember = tradeSummaryView.Quantity / tradeSummaryView.NoOfSellerMember;
                    if (tradeSummaryView.NoOfBuyerClient == 0)
                    {
                        tradeSummaryView.NoOfBuyerClient = 1;
                    }
                    tradeSummaryView.PerCapitaBuyClient = tradeSummaryView.Quantity / tradeSummaryView.NoOfBuyerClient;
                    if (tradeSummaryView.NoOfSellerClient == 0)
                    {
                        tradeSummaryView.NoOfSellerClient = 1;
                    }
                    tradeSummaryView.PerCapitaSellClient = tradeSummaryView.Quantity / tradeSummaryView.NoOfSellerClient;
                    var clientBuyerVolume = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                        x.IsBuyerClient == true).Sum(x => x.Quantity);
                    tradeSummaryView.BuyerClientHHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (((clientBuyerVolume)/(clientTotalbuyerQuantity)) * ((clientBuyerVolume) / (clientTotalbuyerQuantity)))));
                    var memberBuyerVolume = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                        x.IsBuyerClient == false).Sum(x => x.Quantity);
                    tradeSummaryView.BuyerMemberHHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (((memberBuyerVolume) / (memberTotalBuyerQuantity)) * ((memberBuyerVolume) / (memberTotalBuyerQuantity)))));

                    var clientSellerVolume = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                       x.IsSellerClient == true).Sum(x => x.SellerQuantity);
                    tradeSummaryView.SellerClientHHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (((clientSellerVolume) / (clientTotalSellerQuantity)) * ((clientSellerVolume) / (clientTotalSellerQuantity)))));
                    var memberSellerVolume = tradeSummaryList.Where(x => x.Commodity == item.Commodity &&
                                                                        x.IsBuyerClient == false).Sum(x => x.SellerQuantity);
                    tradeSummaryView.SellerMemberHHIIndex = Convert.ToDecimal(String.Format("{0:00.0000}", (((memberSellerVolume) / (memberTotalSellerQuantity)) * ((memberSellerVolume) / (memberTotalSellerQuantity)))));
                    var seasonLowerQuantity = tradeSummaryList.Where(x => x.Commodity == item.Commodity
                                                                        ).OrderByDescending(x => x.Quantity).ToList();
                    tradeSummaryView.HigherAmount = seasonLowerQuantity[0].Quantity;
                    var seasonHigherQuantity = tradeSummaryList.Where(x => x.Commodity == item.Commodity 
                                                                           ).OrderBy(x => x.Quantity).ToList();
                    tradeSummaryView.LowerAmount = seasonHigherQuantity[0].Quantity;
                    var seasonLowerPrice = tradeSummaryList.Where(x => x.Commodity == item.Commodity
                                                                      ).OrderBy(x => x.Price).ToList();
                    tradeSummaryView.LowerPrice = seasonLowerPrice[0].Price;
                    var seasonHigherPrice= tradeSummaryList.Where(x => x.Commodity == item.Commodity 
                                                                     ).OrderByDescending(x => x.Price).ToList();
                    tradeSummaryView.HigerPrice = seasonHigherPrice[0].Price;
                    listOfTradeSummary.Add(tradeSummaryView);
                }
                return listOfTradeSummary;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
