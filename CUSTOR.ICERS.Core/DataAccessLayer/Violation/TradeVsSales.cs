using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
 public class TradeVsSales
  {
    public string Commodity { get; set; }
    public string Class { get; set; }
    public string Grade { get; set; }
    public decimal? OrderQuantity { get; set; }
    public decimal? TradeQuantity { get; set; }
    public string WareHouse { get; set; }
    public decimal? Ratio { get; set; }

  }
  public class Ratios
  {
    public decimal? Ratio { get; set; }
  }
  public class SalesVsTradeDetails
  {
    public List<TradeVsSales> TradeVsSales { get; set; }
    public Ratios Ratios { get; set; }
  }
}
