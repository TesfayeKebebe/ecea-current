using CUSTOR.ICERS.Core.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class TradeVsSalesRepository
  {
    private readonly ECXTradeDbContext _context;

    public TradeVsSalesRepository(ECXTradeDbContext context)
    {

      _context = context;
    }
    public async Task<SalesVsTradeDetails> TradeVsSales(DateTime From, DateTime To, decimal Ratio, decimal MaximumRatio)
    {
      try
      {
        List<TradeVsSales> tradeVsSales = new List<TradeVsSales>();
        List<TradeVsSales> AddedSales = new List<TradeVsSales>();

        SalesVsTradeDetails details = new SalesVsTradeDetails();
        //List<TradeVsSales> addedTradeVsSales = new List<TradeVsSales>();
        //List<TradeVsSales> lessTradeVsSales = new List<TradeVsSales>();
        //List<TradeVsSales> greatterTradeVsSales = new List<TradeVsSales>();
        tradeVsSales = await _context.TradeVsSales.FromSqlRaw<TradeVsSales>(QueryConfiguration.GetQuery(Constants.SalesVsTradeable), From, To).ToListAsync();
        tradeVsSales = tradeVsSales.Where(x => x.Ratio >= Ratio && x.Ratio <= MaximumRatio).ToList();
        if(tradeVsSales.Any())
        {
          var sumOr = tradeVsSales.Sum(x => x.OrderQuantity);
          var sumTr = tradeVsSales.Sum(x => x.TradeQuantity);
          details.TradeVsSales =tradeVsSales.ToList();
          Ratios r = new Ratios();
          r.Ratio = sumOr / sumTr;
          details.Ratios = r;
        }
        //if (Ratio == 0)
        //{
        return details;
        //}

        //else
        //{
        //  lessTradeVsSales = tradeVsSales.Where(x => x.Ratio < Ratio).ToList();
        //  greatterTradeVsSales = tradeVsSales.Where(x => x.Ratio > Ratio).ToList();
        //  addedTradeVsSales.AddRange(lessTradeVsSales);
        //  addedTradeVsSales.AddRange(greatterTradeVsSales);
        //  return addedTradeVsSales;

        //}
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    

    }
  }
}
