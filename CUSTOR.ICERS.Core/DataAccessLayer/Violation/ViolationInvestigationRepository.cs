using AutoMapper;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
  public class ViolationInvestigationRepository
  {
    private readonly ECEADbContext _context;
    private readonly IMapper _mapper;
    private readonly ECXTradeDbContext _dbContext;

    public ViolationInvestigationRepository(ECEADbContext context, IMapper mapper, ECXTradeDbContext dbContext)
    {
      _context = context;
      _mapper = mapper;
      _dbContext = dbContext;
    }
    public bool Create(ViolationInvestigationDto investigationDto)
    {
      try
      {
        ViolationInvestigation investigation = _mapper.Map<ViolationInvestigation>(investigationDto);
        investigation.Id = Guid.NewGuid();
        investigation.CreatedDateTime = DateTime.Now;
        investigation.UpdatedDateTime = DateTime.Now;
        _context.Add(investigation);
        _context.SaveChanges();
        return true;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public bool Update(ViolationInvestigationDto investigationDto)
    {
      try
      {
        var oldInv = _context.ViolationInvestigation.FirstOrDefault(x => x.Id == investigationDto.Id);
        investigationDto.IsActive = oldInv.IsActive;
        investigationDto.IsDeleted = oldInv.IsDeleted;
        investigationDto.CreatedUserId = oldInv.CreatedUserId;
        oldInv = _mapper.Map<ViolationInvestigationDto, ViolationInvestigation>(investigationDto, oldInv);
        _context.SaveChanges();
        return true;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public async Task<List<ViolationRecordCount>> GetViolation(Guid ViolationId, string Type)
    {
      List<ViolationRecordCount> violations = new List<ViolationRecordCount>();
      if (Type == "OV")
      {
        violations = await _context.ViolationRecordCount.FromSqlRaw<ViolationRecordCount>(QueryConfiguration.GetQuery(Constants.GetViolation), ViolationId).ToListAsync();
      }
      else if (Type == "T")
      {
        violations = await _context.ViolationRecordCount.FromSqlRaw<ViolationRecordCount>(QueryConfiguration.GetQuery(Constants.TradeViolationRecords), ViolationId).ToListAsync();
      }
      //else if(Type =="AU")
      //{
      //  violations = await _context.ViolationRecordCount.FromSql("exec dbo.pGetAuctionViolationRecord {0}", ViolationId).ToListAsync();

      //}
      foreach (var item in violations)
      {
        switch (item.Type)
        {
          case Constants.IsNotRegistered:
            item.Type = "Not Registered";
            break;
          case Constants.HasNewRequestAfterPreOpen:
            item.Type = "New Request After PreOpen";
            break;
          case Constants.HasMarkingToClose:
            item.Type = "Marking To Close";
            break;
          case Constants.HasCancelled:
            item.Type = "Cancelled";
            break;
          case Constants.HasSuspended:
            item.Type = "Suspended";
            break;
          case Constants.HasNotRenewed:
            item.Type = "Not Renewed";
            break;
          case Constants.HasBucketing:
            item.Type = "Bucketing";
            break;
          case Constants.HasPreArrangedDuringOpen:
            item.Type = "PreArranged During Open";
            break;
          case Constants.HasFrontRunning:
            item.Type = "Front Running";
            break;
          case Constants.HasAbnormalTrading:
            item.Type = "Abnormal Trading";
            break;
          case Constants.HasPriceChange:
            item.Type = "Price Change";
            break;
          case Constants.HasPreArragedTrade:
            item.Type = "PreArraged Trade";
            break;
          case Constants.IsMatchTrade:
            item.Type = "Match Trade";
            break;
          case Constants.HasSisterCompany:
            item.Type = "Sister Company";
            break;
          case Constants.SellerNotRegisterd:
            item.Type = "Seller Not Registerd";
            break;
          case Constants.BuyerNotRegisterd:
            item.Type = "Buyer Not Registerd";
            break;
          case Constants.HasPriceVolumeBeyondLimit:
            item.Type = "Price Volume Beyond Limit";
            break;
          case Constants.HasPriceLimit:
            item.Type = "Price Limit";
            break;

        }
      }
      return violations;
    }
    public async Task<List<ViolationInvestigationDetail>> GetInvestigations(Guid ViolationId)
    {
      List<ViolationInvestigationDetail> details = new List<ViolationInvestigationDetail>();
      List<ViolationInvestigation> violations = new List<ViolationInvestigation>();
      violations = await _context.ViolationInvestigation.Where(x => x.ViolationId == ViolationId).ToListAsync();
      foreach (var item in violations)
      {
        ViolationInvestigationDetail d = new ViolationInvestigationDetail()
        {
          Description = System.Enum.GetName(typeof(InvestigationType), item.Type),
          Id = item.Id,
          Type = item.Type,
          CreatedDateTime = item.CreatedDateTime,
          CreatedUserId = item.CreatedUserId,
          IsActive = item.IsActive,
          IsDeleted = item.IsDeleted,
          Remark = item.Remark,
          UpdatedDateTime = item.UpdatedDateTime,
          UpdatedUserId = item.UpdatedUserId,
          ViolationId = item.ViolationId,
          BuyerMemberId = item.BuyerMemberId,
          SellerMemberId = item.SellerMemberId,
          MemberId = item.MemberId

        };
        details.Add(d);
      }
      return details;
    }

  }
}
