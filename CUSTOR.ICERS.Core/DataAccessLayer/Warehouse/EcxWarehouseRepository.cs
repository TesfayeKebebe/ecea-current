﻿using AutoMapper;
using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Warehouse
{
   public  class EcxWarehouseRepository
    {
        private readonly ECEADbContext _context;
        private readonly ECXWarehouseApplicationVersion2DbContext _ecxwarehousecontext;
        private readonly IMapper _mapper;

        public EcxWarehouseRepository(ECEADbContext context, ECXWarehouseApplicationVersion2DbContext ecxwarehousecontext, IMapper mapper)
        {
            _context = context;
            _ecxwarehousecontext = ecxwarehousecontext;
            _mapper = mapper;
        }
        public async Task<bool> CreateWarehouse()
        {
            try
            {
                var status = _ecxwarehousecontext.ClWarehouses.ToList();
               
                var newReports = _mapper.Map<List<ClWarehouses>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateGINReport()
        {
            try
            {
                var status = _ecxwarehousecontext.TblGins.ToList();

                var newReports = _mapper.Map<List<TblGins>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateGrnReport()
        {
            try
            {
                var status = _ecxwarehousecontext.TblGrns.ToList();

                var newReports = _mapper.Map<List<TblGrns>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateArrivalsReport()
        {
            try
            {
                var status = _ecxwarehousecontext.TblArrivals.ToList();

                var newReports = _mapper.Map<List<TblArrivals>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<bool> CreatePickupNoticeReport()
        {
            try
            {
                var status = _ecxwarehousecontext.TblPickupNotices.ToList();

                var newReports = _mapper.Map<List<TblPickupNotices>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public async Task<bool> CreatePickupNoticeGinReport()
        {
            try
            {
                var status = _ecxwarehousecontext.TblPungin.ToList();

                var newReports = _mapper.Map<List<TblPungin>>(status);
                _context.AddRange(newReports);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


    }
}
