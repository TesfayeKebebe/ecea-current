﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Warehouse
{
    public class InventoryRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        public InventoryRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<IEnumerable<SampleLeftOver>> GetLeftOverData(string lang)
        {
            try
            {
          List<SampleLeftOver> data = new List<SampleLeftOver>();
            data = await _context.SampleLeftOver.ToListAsync();
            return  data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
  
        }
        public async Task<IEnumerable<ExcessProduct>> GetExcessProducts(string lang)
        {
            try
            {
                List<ExcessProduct> data = new List<ExcessProduct>();
                data = await _context.ExcessProduct.ToListAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<ShortFallDTO>> GetShortFallData(string lang)
        {
            try
            {
                var data = await (from sh in _context.ShortFall
                                  where
                                  sh.IsActive == true && sh.IsDeleted == false
                                  select new ShortFallDTO
                                  {
                                      BranchLocation= sh.BranchLocation,
                                      FirstWeight=sh.FirstWeight,
                                      GradeOfCommodity= sh.GradeOfCommodity,
                                      Id= sh.Id,
                                      InventoryController=sh.InventoryController,
                                      NameofLeadInventory=sh.NameofLeadInventory,
                                      NetWeight=sh.NetWeight,
                                      NoticedAgainstPun=sh.NoticedAgainstPun,
                                      OperationSuperVisor=sh.OperationSuperVisor,
                                      PUN=sh.PUN,
                                      PunWeight= sh.PunWeight,
                                      ScaleTicketNumber= sh.ScaleTicketNumber,
                                      SecondWeight=sh.SecondWeight,
                                      Short_Fall=sh.Short_Fall,
                                      WareHouseNo=sh.WareHouseNo
                                  }).ToArrayAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }

}
