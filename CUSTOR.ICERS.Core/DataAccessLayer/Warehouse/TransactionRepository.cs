﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Warehouse
{
  public class TransactionRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;

        public TransactionRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<EcexTradeDataForECEA>> GetTransactionStatusOne(string Language)
        {
            try
            {
                List<EcexTradeDataForECEA> transaction = new List<EcexTradeDataForECEA>();
               // transaction = await (from a in _context.TradeData select a).ToListAsync();
                transaction = await _context.EcexTradeDataForECEA.ToListAsync();

                return transaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }


        }

        public async Task<List<TransactionStatusTwo>> GetTransactionStatusTwo()
        {
            try
            {
                List<TransactionStatusTwo> transaction = new List<TransactionStatusTwo>();
                 transaction = await (from tr in _context.EcexTradeDataForECEA
                                         select new TransactionStatusTwo
                                         {
                                            BuyerClientName= tr.buyerClient,
                                            BuyerMemberName= tr.buyerMember,
                                            SellerMemberName= tr.sellerMember,
                                            StDValueSmall= "10",
                                            StDValueLarge="20",
                                         }).ToListAsync();
                return transaction;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }
    }
}
