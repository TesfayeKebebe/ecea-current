﻿using AutoMapper;
using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace CUSTOR.ICERS.Core.DataAccessLayer
{
    public class WarehouseRepository
    {
        private readonly EcxdbCentralDepositoryDbContext _centralDepositorycontext;
        private readonly ECXWarehouseApplicationVersion2DbContext _warehouseApplicationcontext;
        private readonly ECXLookupDbContext _ecxLookupcontext;

        private readonly IMapper _mapper;

        public WarehouseRepository(EcxdbCentralDepositoryDbContext centralDepositorycontext, ECXWarehouseApplicationVersion2DbContext warehouseApplicationcontext, ECXLookupDbContext ecxLookupcontext, IMapper mapper)
        {
            _centralDepositorycontext = centralDepositorycontext;
            _warehouseApplicationcontext = warehouseApplicationcontext;
            _ecxLookupcontext = ecxLookupcontext;
            _mapper = mapper;
        }

        public async Task<bool> CreateWarehouse(ClWarehouses commodity)
        {
            try
            {
                ClWarehouses newReports = _mapper.Map<ClWarehouses>(commodity);
                _warehouseApplicationcontext.Add(newReports);
                await _warehouseApplicationcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateGINReport(TblGins commodity)
        {
            try
            {
                TblGins newReports = _mapper.Map<TblGins>(commodity);
                _warehouseApplicationcontext.Add(newReports);
                await _warehouseApplicationcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateGrnReport(TblGrns commodity)
        {
            try
            {
                TblGrns newReports = _mapper.Map<TblGrns>(commodity);
                _warehouseApplicationcontext.Add(newReports);
                await _warehouseApplicationcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<bool> CreateArrivalsReport(TblArrivals commodity)
        {
            try
            {
                TblArrivals newReports = _mapper.Map<TblArrivals>(commodity);
                _warehouseApplicationcontext.Add(newReports);
                await _warehouseApplicationcontext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<ClWarehousesDTO>> GetWarehouse()
        {
            IEnumerable<ClWarehousesDTO> alls = null;


            try
            {
                var locations = await _ecxLookupcontext.TblLocation.ToListAsync();

                alls = await (from n in _warehouseApplicationcontext.ClWarehouses
                              join ws in locations
                              on n.LocationId equals ws.Guid
                              orderby (n.Description) ascending
                              select new ClWarehousesDTO
                              {
                                  Id =n.Id,
                                  Code = n.Code,
                                  Description = n.Description,
                                  StorageCharge = n.StorageCharge,
                                  GinqueueNo = n.GinqueueNo,
                                  GrnqueueNo = n.GrnqueueNo,
                                  ShortName = n.ShortName,
                                  StorageChargeAfterNdays = n.StorageChargeAfterNdays,
                                  Location = ws.Description,
                                  QueueDate = n.QueueDate,
                                  ActiveStatus = (n.ActiveStatus == 1) ? "Active" : "Not Active"
                              })
                 .AsNoTracking()
                 .ToListAsync();

                return alls;


            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public async Task<List<ClCommodityDTO>> GetAllCommodites()
        {
            List<ClCommodityDTO> alls = null;


            try
            {

                alls = await (from n in _warehouseApplicationcontext.ClCommodity
                              select new ClCommodityDTO
                              {
                                  Id = n.Id,
                                  Description = n.Description,
                                 
                              })
                 .AsNoTracking()
                 .ToListAsync();

                return alls;


            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public async Task<List<ClCommodityDTO>> GetAllCommoditesGrade()
        {
            List<ClCommodityDTO> alls = null;


            try
            {

                alls = await (from n in _warehouseApplicationcontext.TblCommodityGrade
                              select new ClCommodityDTO
                              {
                                  Id = n.Guid,
                                  Description = n.Description,

                              })
                 .AsNoTracking()
                 .ToListAsync();

                return alls;


            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public async Task<IEnumerable<TblslaDTO>> GetWSLA(string start, string end, Guid? commodityId)
        {

            try
            {
               // var status =  _warehouseApplicationcontext.TblGrns.Where(fd => (fd.GrncreatedDate >= DateTime.Parse(start) && fd.GrncreatedDate <= DateTime.Parse(end)));


                var wslas = await (from n in _warehouseApplicationcontext.TblGrns
                                   join rc in _warehouseApplicationcontext.TblArrivals
                                  on n.CommodityReceivingId equals rc.Id
                                  into c
                                   from rci in c.DefaultIfEmpty()
                                   join wh in _warehouseApplicationcontext.ClWarehouses
                                   on n.WarehouseId equals wh.Id
                                   where n.GrncreatedDate >= DateTime.Parse(start) && n.GrncreatedDate <= DateTime.Parse(end) && rci.CommodityId== commodityId

                                   select new TblGrnsforslaDTO
                                   {
                                       sla = (rci.DateTimeReceived!=null)?(int)Math.Round((n.GrncreatedDate - rci.DateTimeReceived).Value.TotalDays):0,
                                       Warehouse = wh.Description,
                                   })
                     .GroupBy(c => new
                     {
                         c.Warehouse,
                     })

                     .AsNoTracking()
                     .ToListAsync();

                List<TblslaDTO> alls = new List<TblslaDTO>(); ;

                foreach (var grp in wslas)
                {
                    TblslaDTO slavalues = new TblslaDTO();
                    var A = grp.Where(s => s.sla == 0).Count();
                    var A1 = grp.Where(s => s.sla == 1).Count();
                    var A2 = grp.Where(s => s.sla == 2).Count();
                    var A3 = grp.Where(s => s.sla == 3).Count();
                    var A4 = grp.Where(s => s.sla == 4).Count();
                    var A5 = grp.Where(s => s.sla == 5).Count();
                    var A6 = grp.Where(s => s.sla == 6).Count();
                    var A7 = grp.Where(s => s.sla == 7).Count();
                    var A8 = grp.Where(s => s.sla == 8).Count();
                    var A9 = grp.Where(s => s.sla == 9).Count();
                    var A10 = grp.Where(s => s.sla == 10).Count();
                    var A11 = grp.Where(s => s.sla == 11).Count();
                    var A12 = grp.Where(s => s.sla >= 12).Count();

                    var NumberofTruckOutsideSla = (A4 + A5 + A6 + A7 + A8 + A9 + A10 + A11 + A12);
                    var NumberofTruckWithInSla = (A + A1 + A2 + A3);
                    var PercentOutsideSla = Math.Round(NumberofTruckOutsideSla * 100 / (NumberofTruckOutsideSla + NumberofTruckWithInSla + 0.0000000001));
                    var PercentwithInSla = Math.Round(NumberofTruckWithInSla * 100 / (NumberofTruckOutsideSla + NumberofTruckWithInSla + 0.0000000001));
                    slavalues.Warehouse = grp.Key.Warehouse;
                    slavalues.A = (A != 0) ? A : (int?)null;
                    slavalues.A1 = (A1 != 0) ? A1 : (int?)null;
                    slavalues.A2 = (A2 != 0) ? A2 : (int?)null;
                    slavalues.A3 = (A3 != 0) ? A3 : (int?)null;
                    slavalues.A4 = (A4 != 0) ? A4 : (int?)null;
                    slavalues.A5 = (A5 != 0) ? A5 : (int?)null;
                    slavalues.A6 = (A6 != 0) ? A6 : (int?)null;
                    slavalues.A7 = (A7 != 0) ? A7 : (int?)null;
                    slavalues.A8 = (A8 != 0) ? A8 : (int?)null;
                    slavalues.A9 = (A9 != 0) ? A9 : (int?)null;
                    slavalues.A10 = (A10 != 0) ? A10 : (int?)null;
                    slavalues.A11 = (A11 != 0) ? A11 : (int?)null;
                    slavalues.A12 = (A12 != 0) ? A12 : (int?)null;
                
                    slavalues.NumberofTruckOutsideSla = (NumberofTruckOutsideSla != 0) ? NumberofTruckOutsideSla : (int?)null;
                    slavalues.NumberofTruckWithInSla = (NumberofTruckWithInSla != 0) ? NumberofTruckWithInSla : (int?)null;
                    slavalues.PercentOutsideSla = (PercentOutsideSla != 0) ? PercentOutsideSla : (double?)null;
                    slavalues.PercentwithInSla = (PercentwithInSla != 0) ? PercentwithInSla : (double?)null;
                    alls.Add(slavalues);
                }
                alls.OrderBy(s => s.NumberofTruckWithInSla).ToList();
                return alls;



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<TblslaReportBywarehouseDTO> GetSLAReportByWarehouse(string start, string end, Guid? commodityId,Guid? warehouseId)
        {

            try
            {
               // var status = _warehouseApplicationcontext.TblGrns.Where(fd => fd.GrncreatedDate >= DateTime.Parse(start) && fd.GrncreatedDate <= DateTime.Parse(end)&&fd.WarehouseId==warehouseId);

                var wslas = await (from n in _warehouseApplicationcontext.TblGrns
                                   join rc in _warehouseApplicationcontext.TblArrivals
                                   on n.CommodityReceivingId equals rc.Id
                                   into c
                                   from rci in c.DefaultIfEmpty()
                                   join wh in _warehouseApplicationcontext.ClWarehouses
                                   on n.WarehouseId equals wh.Id
                                   where n.GrncreatedDate >= DateTime.Parse(start) && n.GrncreatedDate <= DateTime.Parse(end) && rci.CommodityId == commodityId && n.WarehouseId == warehouseId

                                   select new TblGrnsforslaDTO
                                   {
                                       sla = (rci.DateTimeReceived != null) ? (int)Math.Round((n.GrncreatedDate - rci.DateTimeReceived).Value.TotalDays) : 0,
                                       Warehouse = wh.Description,
                                       Code = wh.Code
                                   })
                     .GroupBy(c => new
                     {
                         c.Warehouse,
                     })
                     
                     .AsNoTracking()
                     .SingleOrDefaultAsync();

                TblslaReportBywarehouseDTO slavalues = new TblslaReportBywarehouseDTO();

                if (wslas != null)
                {
                    var A = wslas.Where(s => s.sla <= 3).Count();
                    var A1 = wslas.Where(s => s.sla == 4).Count();
                    var A2 = wslas.Where(s => s.sla == 5).Count();
                    var A3 = wslas.Where(s => s.sla == 6).Count();
                    var A4 = wslas.Where(s => s.sla == 7).Count();
                    var A5 = wslas.Where(s => s.sla == 8).Count();
                    var A6 = wslas.Where(s => s.sla >= 9).Count();


                    slavalues.A = (A != 0) ? A : (int?)null;
                    slavalues.A1 = (A1 != 0) ? A1 : (int?)null;
                    slavalues.A2 = (A2 != 0) ? A2 : (int?)null;
                    slavalues.A3 = (A3 != 0) ? A3 : (int?)null;
                    slavalues.A4 = (A4 != 0) ? A4 : (int?)null;
                    slavalues.A5 = (A5 != 0) ? A5 : (int?)null;
                    slavalues.A6 = (A6 != 0) ? A5 : (int?)null;
                }








                return slavalues;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblArrivalsDTO>> GetArrival(string start, string end)
        {
            IEnumerable<TblArrivalsDTO> alls = null;

            try
            {
                var status = (start == "null" && end == "null") ? _warehouseApplicationcontext.TblArrivals : (start != "null" && end == "null") ? _warehouseApplicationcontext.TblArrivals.Where(fd => (fd.DateTimeReceived >= DateTime.Parse(start))) : (start == "null" && end != "null") ? _warehouseApplicationcontext.TblArrivals.Where(fd => (fd.DateTimeReceived <= DateTime.Parse(end))) : _warehouseApplicationcontext.TblArrivals.Where(fd => (fd.DateTimeReceived >= DateTime.Parse(start) && fd.DateTimeReceived <= DateTime.Parse(end)));


                alls = await (from tbr in status
                              join sa in _warehouseApplicationcontext.TblWashingStation on tbr.WashingStation equals sa.Id into k
                              from b in k.DefaultIfEmpty()
                              join ic in _warehouseApplicationcontext.ClCommodity on tbr.CommodityId equals ic.Id into g
                              from e in g.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on tbr.WarehouseId equals wa.Id
                              orderby tbr.DateTimeReceived descending
                              select new TblArrivalsDTO
                              {
                                  Commodity = e.Description,
                                  Warehouse = wa.Description,
                                  ClientName = tbr.ClientName,
                                  NumberofBags = tbr.NumberofBags,
                                  VoucherNumber = tbr.VoucherNumber,
                                  SpecificArea = tbr.SpecificArea,
                                  TruckPlateNumber = tbr.TruckPlateNumber,
                                  WashingStation = b.Description,
                                  DateTimeReceived = (tbr.DateTimeReceived != null) ? tbr.DateTimeReceived.Value.ToString() :
                                 null,
                                  Woreda = tbr.VoucherNumber,
                                  LicenseNumber = tbr.LicenseNumber,
                                  WarehouseNo = wa.Code,
                                  Sampletype = tbr.Sampletype,
                                  VoucherWeight = tbr.VoucherWeight,
                                  Remark = tbr.Remark,
                                  MoistureDeduct = (tbr.MoistureDeduct == true) ? "Yes" : "No",
                                  GrossWeight = tbr.GrossWeight,
                                  ProcessingCenter = tbr.ProcessingCenter,
                                  ProductionYear = tbr.ProductionYear,
                                  TrackingNumber = tbr.TrackingNumber,
                                  VoucherCertificateNo = tbr.VoucherCertificateNo,
                                  DriverName = tbr.DriverName,
                                  VoucherNumberOfBags = tbr.VoucherNumberOfBags,
                                  LicenseIssuedPlace = tbr.LicenseIssuedPlace,
                                  TrailerPlateNumber = tbr.TrailerPlateNumber,
                                  VoucherNumberOfPlomps = tbr.VoucherNumberOfPlomps,
                                  IsTruckInCompound = (tbr.IsTruckInCompound) ? "Yes" : "No",
                                  VehicleSize = tbr.VehicleSize,
                                  HasVoucher = tbr.HasVoucher,
                                  Status = tbr.Status,



                              })
                   .AsNoTracking()
                   .ToListAsync();


                return alls;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblArrivalsBywarehouseandcommodityDTO>> GetArrivalByCommodityAndWarehouse(string start, string end)
        {
            IEnumerable<TblArrivalsBywarehouseandcommodityDTO> alls = null;

            try
            {
                var status =  _warehouseApplicationcontext.TblArrivals.Where(fd => (fd.DateTimeReceived >= DateTime.Parse(start) && fd.DateTimeReceived <= DateTime.Parse(end)));


                alls = await (from tbr in status
                              group tbr by new
                              {
                                  tbr.CommodityId,
                                  tbr.WarehouseId,
                              } into arrival
                              join ic in _warehouseApplicationcontext.ClCommodity on arrival.Key.CommodityId equals ic.Id into g
                              from e in g.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on arrival.Key.WarehouseId equals wa.Id into k
                              from war in k.DefaultIfEmpty()
                              
                              
                              orderby war.Description
                              select new TblArrivalsBywarehouseandcommodityDTO
                              {
                                  Commodity = e.Description,
                                  NumberofBags = arrival.Sum(y => y.NumberofBags),
                                  Weight = arrival.Sum(dw => dw.VoucherWeight),
                                  Warehouse = war.Description,
                                  NumberofTruk = arrival.Count()


                              })
                   .AsNoTracking()
                   .ToListAsync();
             //var   allsaggregate = await (from tbr in alls
             //                       group tbr by new
             //                       {
             //                           tbr.Commodity,
             //                           tbr.Warehouse
             //                       } into k
             //                 select new TblArrivalsBywarehouseandcommodityDTO
             //                 {
             //                     Commodity = k.Description,
             //                     NumberofBags = tbr.NumberofBags,
             //                     Weight = tbr.VoucherWeight,
             //                     ZoneWoreda = zor.Description + "/" + wrr.Description,
             //                     Warehouse = war.Description


             //                 })
             //          .AsNoTracking()
             //          .ToListAsync();


                return alls;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblArrivalsBywarehouseandcommodityDTO>> GetArrivalByCommodity(string start, string end, Guid? commodityId)
        {
            IEnumerable<TblArrivalsBywarehouseandcommodityDTO> alls = null;

            try
            {
                var status = _warehouseApplicationcontext.TblArrivals.Where(fd => (fd.DateTimeReceived >= DateTime.Parse(start) && fd.DateTimeReceived <= DateTime.Parse(end))&& fd.CommodityId==commodityId);


                alls = await (from tbr in status
                              group tbr by new
                              {
                                  tbr.CommodityId,
                                  tbr.WarehouseId,
                              } into arrival
                              join ic in _warehouseApplicationcontext.ClCommodity on arrival.Key.CommodityId equals ic.Id into g
                              from e in g.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on arrival.Key.WarehouseId equals wa.Id into k
                              from war in k.DefaultIfEmpty()
                              
                              select new TblArrivalsBywarehouseandcommodityDTO
                              {
                                  Commodity = e.Description,
                                  NumberofBags = arrival.Sum(y => y.NumberofBags),
                                  Weight = arrival.Sum(dw => dw.VoucherWeight),
                                  //ZoneWoreda = zor.Description,
                                  Warehouse = war.Description,
                                  NumberofTruk = arrival.Count(),
                                  NumberofClient = arrival.Count()

                              })
                   .OrderBy(k=>k.NumberofBags)
                   .AsNoTracking()
                   .ToListAsync();
                //var   allsaggregate = await (from tbr in alls
                //                       group tbr by new
                //                       {
                //                           tbr.Commodity,
                //                           tbr.Warehouse
                //                       } into k
                //                 select new TblArrivalsBywarehouseandcommodityDTO
                //                 {
                //                     Commodity = k.Description,
                //                     NumberofBags = tbr.NumberofBags,
                //                     Weight = tbr.VoucherWeight,
                //                     ZoneWoreda = zor.Description + "/" + wrr.Description,
                //                     Warehouse = war.Description


                //                 })
                //          .AsNoTracking()
                //          .ToListAsync();


                return alls;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblGrnsDTO>> GetGRNReport(string start, string end, int selld)
        {
            IEnumerable<TblGrnsDTO> alls = null;

            try
            {
                //var status = _warehouseApplicationcontext.TblGrns.Where(fd => (fd.GrncreatedDate >= DateTime.Parse(start) && fd.GrncreatedDate <= DateTime.Parse(end)));
                var Cdwarehouserecipt = await ((selld == 1) ? _centralDepositorycontext.TblWarehouseReciept.Where(fd => fd.DateDeposited >= DateTime.Parse(start).AddDays(-15) && fd.DateDeposited <= DateTime.Parse(end) && fd.CurrentQuantity == 0).ToListAsync() :
                    (selld == 2) ? _centralDepositorycontext.TblWarehouseReciept.Where(fd => fd.DateDeposited >= DateTime.Parse(start).AddDays(-15) && fd.DateDeposited <= DateTime.Parse(end) && fd.CurrentQuantity != 0).ToListAsync()
                    : _centralDepositorycontext.TblWarehouseReciept.Where(fd => fd.DateDeposited >= DateTime.Parse(start).AddDays(-15) && fd.DateDeposited <= DateTime.Parse(end)).ToListAsync());
                var CdwarehousereciptStatus = await _centralDepositorycontext.TblWarehouseRecieptStatus.ToListAsync();

                if (selld == 0 || selld == 1 || selld == 2)
                {
                    alls = await (from tbr in _warehouseApplicationcontext.TblGrns
                                  join cg in _warehouseApplicationcontext.TblCommodityGrade
                                  on tbr.CommodityGradeId equals cg.Guid
                                  join wa in _warehouseApplicationcontext.ClWarehouses
                                  on tbr.WarehouseId equals wa.Id
                                  join wst in _warehouseApplicationcontext.TblWashingStation
                                  on tbr.WashingandMillingStation equals wst.Id into k
                                  from ws in k.DefaultIfEmpty()
                                  join rc in _warehouseApplicationcontext.TblArrivals
                                  on tbr.CommodityReceivingId equals rc.Id
                                  into c
                                  from rci in c.DefaultIfEmpty()
                                  join cdw in Cdwarehouserecipt
                                  on tbr.Grn_Number equals cdw.Grnnumber into j
                                  from wr in j.DefaultIfEmpty()
                                  join cds in CdwarehousereciptStatus
                                  on wr.Grnstatus equals cds.Id into g
                                  from wrs in g.DefaultIfEmpty()
                                  join st in _warehouseApplicationcontext.TblGrnsStatus
                                  on tbr.Status equals st.ID
                                  where tbr.GrncreatedDate >= DateTime.Parse(start) && tbr.GrncreatedDate <= DateTime.Parse(end) 
                                  orderby tbr.DateDeposited descending
                                  select new TblGrnsDTO
                                  {
                                      ClientName = rci.ClientName,
                                      Warehouse = wa.Description,
                                      CommodityGrade = cg.Description,
                                      TotalNumberOfBags = tbr.TotalNumberOfBags,
                                      GrncreatedDate = (tbr.GrncreatedDate != null) ? tbr.GrncreatedDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      DateDeposited = (tbr.DateDeposited != null) ? tbr.DateDeposited.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ArrivalDate = (rci.DateTimeReceived != null) ? rci.DateTimeReceived.ToString() :
                                                null,
                                      GrnNumber = tbr.Grn_Number,
                                      NetWeight = tbr.NetWeight,
                                      WashingandMillingStation = ws.Description,
                                      CarPlateNumber = tbr.CarPlateNumber,
                                      Quantity = tbr.Quantity,
                                      IsTraceable = tbr.IsTraceable,
                                      ProductionYear = tbr.ProductionYear,
                                      TotalValue = tbr.TotalValue,
                                      Symbol = tbr.Symbol,
                                      Remark = tbr.Remark,
                                      TruckWeight = tbr.TruckWeight,
                                      Shade = tbr.Shade,
                                      Status = st.Description,
                                      sla = (rci.DateTimeReceived != null) ? ((int)Math.Round((tbr.GrncreatedDate - rci.DateTimeReceived).Value.TotalDays)) : (int?)null,
                                      cdGrnstatus = wrs.Name,
                                      DateApproved = (wr.DateApproved != null) ? wr.DateApproved.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ExpiredDate =(rci.CommodityId.ToString() == "71604275-DF23-4449-9DAE-36501B14CC3B")? tbr.GrncreatedDate.Value.AddDays(20).ToString("MM/dd/yyyy"): tbr.GrncreatedDate.Value.AddDays(30).ToString("MM/dd/yyyy"),
                                      DateClosed = (wr.DateClosed != null) ? wr.DateClosed.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ReciteExpiryDate = (wr.ExpiryDate != null) ? wr.ExpiryDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      CurrentQuantity = wr.CurrentQuantity,
                                      CupValue = tbr.CupValue,
                                      CDRemark = wr.Remark,
                                      TradeDate = (wr.TradeDate != null) ? wr.TradeDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ManualApprovalDate = (wr.ManualApprovalDate != null) ? wr.ManualApprovalDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      TradeNo = wr.TradeNo,
                                      TransactionNo = wr.TransactionNo,
                                      NetWeightAdjusted = wr.NetWeightAdjusted,
                                      TempQuantity = wr.TempQuantity,
                                      RawValue = tbr.RawValue,
                                      RebagingQuantity = tbr.RebagingQuantity,
                                      GrossWeight = tbr.GrossWeight,
                                      OriginalQuantity = wr.OriginalQuantity,
                                      WeightBeforeMoisture =wr.WeightBeforeMoisture,
                                      ProcessingCenter = tbr.ProcessingCenter,
                                      TrailerPlateNumber =tbr.TrailerPlateNumber,
                                      NumberOfBags =wr.NumberOfBags,
                                      SourceType = wr.SourceType,
                                      Quadrant = tbr.Quadrant,
                                      Tare = tbr.Tare,
                                      ConsignmentType = tbr.ConsignmentType,
                                      ClientCert = tbr.ClientCert,
                                      AutoNumber = tbr.AutoNumber,
                                      ScreenSize = tbr.ScreenSize,
                                      
                                      

                                  })
                       .AsNoTracking()
                       .ToListAsync();
                    return alls;

                }
                else
                {
                    alls = await (from tbr in _warehouseApplicationcontext.TblGrns

                                  join rc in _warehouseApplicationcontext.TblArrivals
                                  on tbr.CommodityReceivingId equals rc.Id
                                  into c
                                  from rci in c.DefaultIfEmpty()
                                  join cg in _warehouseApplicationcontext.TblCommodityGrade
                                  on tbr.CommodityGradeId equals cg.Guid
                                  join wa in _warehouseApplicationcontext.ClWarehouses
                                  on tbr.WarehouseId equals wa.Id
                                  join wst in _warehouseApplicationcontext.TblWashingStation
                                  on tbr.WashingandMillingStation equals wst.Id into k
                                  from ws in k.DefaultIfEmpty()
                                  join cdw in Cdwarehouserecipt
                                  on tbr.Grn_Number equals cdw.Grnnumber into j
                                  from wr in j.DefaultIfEmpty()
                                  join cds in CdwarehousereciptStatus
                                  on wr.Grnstatus equals cds.Id into g
                                  from wrs in g.DefaultIfEmpty()
                                  join st in _warehouseApplicationcontext.TblGrnsStatus
                                  on tbr.Status equals st.ID
                                  where tbr.GrncreatedDate >= DateTime.Parse(start) && tbr.GrncreatedDate <= DateTime.Parse(end)
                                  orderby tbr.DateDeposited descending
                                  select new TblGrnsDTO
                                  {
                                      ClientName = rci.ClientName,
                                      Warehouse = wa.Description,
                                      CommodityID = rci.CommodityId.ToString(),
                                      CommodityGrade = cg.Description,
                                      TotalNumberOfBags = tbr.TotalNumberOfBags,
                                      GrncreatedDate = (tbr.GrncreatedDate != null) ? tbr.GrncreatedDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      DateDeposited = (tbr.DateDeposited != null) ? tbr.DateDeposited.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ArrivalDate = (rci.DateTimeReceived != null) ? rci.DateTimeReceived.ToString() :
                                                null,
                                      GrnNumber = tbr.Grn_Number,
                                      NetWeight = tbr.NetWeight,
                                      WashingandMillingStation = ws.Description,
                                      CarPlateNumber = tbr.CarPlateNumber,
                                      Quantity = tbr.Quantity,
                                      IsTraceable = tbr.IsTraceable,
                                      ProductionYear = tbr.ProductionYear,
                                      TotalValue = tbr.TotalValue,
                                      Symbol = tbr.Symbol,
                                      Remark = tbr.Remark,
                                      TruckWeight = tbr.TruckWeight,
                                      Shade = tbr.Shade,
                                      Status = st.Description,
                                      sla = (rci.DateTimeReceived != null) ? ((int)Math.Round((tbr.GrncreatedDate - rci.DateTimeReceived).Value.TotalDays)) : (int?)null,
                                      cdGrnstatus = wrs.Name,
                                      DateApproved = (wr.DateApproved != null) ? wr.DateApproved.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ExpiredDate = (rci.CommodityId.ToString() == "71604275-DF23-4449-9DAE-36501B14CC3B") ? tbr.GrncreatedDate.Value.AddDays(20).ToString("MM/dd/yyyy") : tbr.GrncreatedDate.Value.AddDays(30).ToString("MM/dd/yyyy"),
                                      DateClosed = (wr.DateClosed != null) ? wr.DateClosed.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ReciteExpiryDate = (wr.ExpiryDate != null) ? wr.ExpiryDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      CurrentQuantity = wr.CurrentQuantity,
                                      CupValue = tbr.CupValue,
                                      CDRemark = wr.Remark,
                                      TradeDate = (wr.TradeDate != null) ? wr.TradeDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      ManualApprovalDate = (wr.ManualApprovalDate != null) ? wr.ManualApprovalDate.Value.ToString("MM/dd/yyyy") :
                                                null,
                                      TradeNo = wr.TradeNo,
                                      TransactionNo = wr.TransactionNo,
                                      NetWeightAdjusted = wr.NetWeightAdjusted,
                                      TempQuantity = wr.TempQuantity,
                                      RawValue = tbr.RawValue,
                                      RebagingQuantity = tbr.RebagingQuantity,
                                      GrossWeight = tbr.GrossWeight,
                                      OriginalQuantity = wr.OriginalQuantity,
                                      WeightBeforeMoisture = wr.WeightBeforeMoisture,
                                      ProcessingCenter = tbr.ProcessingCenter,
                                      TrailerPlateNumber = tbr.TrailerPlateNumber,
                                      NumberOfBags = wr.NumberOfBags,
                                      SourceType = wr.SourceType,
                                      Quadrant = tbr.Quadrant,
                                      Tare = tbr.Tare,
                                      ConsignmentType = tbr.ConsignmentType,
                                      ClientCert = tbr.ClientCert,
                                      AutoNumber = tbr.AutoNumber,
                                      ScreenSize = tbr.ScreenSize,
                                  })
                      .AsNoTracking()
                      .ToListAsync();

                    List<TblGrnsDTO> ExtendedCoffee = new List<TblGrnsDTO>();
                    foreach (var grp in alls)
                    {
                        if (grp.ReciteExpiryDate == null || grp.GrncreatedDate == null)
                        {

                        }
                        else if (grp.CommodityID == "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 20)
                        {

                            ExtendedCoffee.Add(grp);

                        }
                        else if (grp.CommodityID != "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 30)
                        {

                            ExtendedCoffee.Add(grp);

                        }
                    }

                    return ExtendedCoffee;

                }



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<List<TblExtendedGrnsDTO>> GetExtendedNumberofGRNCommodit(string start, string end)
        {
            IEnumerable<TblGrnsDTO> alls = null;

            try
            {
                var status = _warehouseApplicationcontext.TblGrns.Where(fd => (fd.GrncreatedDate >= DateTime.Parse(start) && fd.GrncreatedDate <= DateTime.Parse(end)));
               var Cdwarehouserecipt = await _centralDepositorycontext.TblWarehouseReciept.Where(fd => fd.DateDeposited >= DateTime.Parse(start).AddDays(-15) && fd.DateDeposited <= DateTime.Parse(end)).ToListAsync();

                alls = await (from tbr in status

                              join rc in _warehouseApplicationcontext.TblArrivals
                              on tbr.CommodityReceivingId equals rc.Id
                              into c
                              from rci in c.DefaultIfEmpty()
                              join cg in _warehouseApplicationcontext.TblCommodityGrade
                              on tbr.CommodityGradeId equals cg.Guid
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on tbr.WarehouseId equals wa.Id
                              join cdw in Cdwarehouserecipt
                              on tbr.Grn_Number equals cdw.Grnnumber into j
                              from wr in j.DefaultIfEmpty()


                              select new TblGrnsDTO
                              {
                                  ClientName = rci.ClientName,
                                  NetWeight = tbr.NetWeight,
                                  Warehouse = wa.Description,
                                  CommodityGrade = cg.Symbol,
                                  CommodityID = rci.CommodityId.ToString(),
                                  GrncreatedDate = (tbr.GrncreatedDate != null) ? tbr.GrncreatedDate.Value.ToString("MM/dd/yyyy") :
                                            null,
                                  ReciteExpiryDate = (wr.ExpiryDate != null) ? wr.ExpiryDate.Value.ToString("MM/dd/yyyy") :
                                            null,
                      
                              })
                      .AsNoTracking()
                      .ToListAsync();

                List<TblGrnsDTO> ExtendedCoffee = new List<TblGrnsDTO>();
                foreach (var grp in alls)
                {
                    if (grp.ReciteExpiryDate == null || grp.GrncreatedDate == null)
                    {

                    }
                    else if (grp.CommodityID == "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 20)
                    {
                        grp.ExtendedNumberOfDay = (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays;
                        ExtendedCoffee.Add(grp);

                    }
                    else if (grp.CommodityID != "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 30)
                    {
                        grp.ExtendedNumberOfDay = (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays;
                        ExtendedCoffee.Add(grp);

                    }
                }

                var ExtendedData =  (from tbr in ExtendedCoffee
                             group tbr by new
                                  {
                                      tbr.CommodityGrade,
                                      tbr.Warehouse,
                                      tbr.ClientName
                                  } into arrival

                                  select new TblExtendedGrnsDTO
                                  {
                                      CommodityGrade = arrival.Key.CommodityGrade,
                                      TotalNumberOfExtendedGRN = arrival.Count(),
                                      Warehouse = arrival.Key.Warehouse,
                                      NetWeight = arrival.Sum(s => s.NetWeight),
                                      ClientName = arrival.Key.ClientName,
                                      ExtendedNumberOfDay =arrival.Sum(s=>s.ExtendedNumberOfDay)


                                  })
                                 .ToList();


                return ExtendedData;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<List<TblExtendedGrnsDTO>> GetExtendedNumberofGRNByWarehouseAndCommodity(string start, string end, Guid? commodityId)
        {
            IEnumerable<TblGrnsDTO> alls = null;

            try
            {
                //var status = _warehouseApplicationcontext.TblGrns.Where(fd => (fd.GrncreatedDate >= DateTime.Parse(start) && fd.GrncreatedDate <= DateTime.Parse(end)));
                var Cdwarehouserecipt = await _centralDepositorycontext.TblWarehouseReciept.Where(fd => fd.DateDeposited >= DateTime.Parse(start).AddDays(-15) && fd.DateDeposited <= DateTime.Parse(end)).ToListAsync();

                alls = await (from tbr in _warehouseApplicationcontext.TblGrns
                              join rc in _warehouseApplicationcontext.TblArrivals
                             on tbr.CommodityReceivingId equals rc.Id
                             into c
                              from rci in c.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on tbr.WarehouseId equals wa.Id
                              join cdw in Cdwarehouserecipt
                              on tbr.Grn_Number equals cdw.Grnnumber into j
                              from wr in j.DefaultIfEmpty()
                              where tbr.GrncreatedDate >= DateTime.Parse(start) && tbr.GrncreatedDate <= DateTime.Parse(end) && rci.CommodityId ==commodityId

                              select new TblGrnsDTO
                              {
                                  Warehouse = wa.Description,
                                  NetWeight = tbr.NetWeight,
                                  CommodityID = rci.CommodityId.ToString(),
                                  GrncreatedDate = (tbr.GrncreatedDate != null) ? tbr.GrncreatedDate.Value.ToString("MM/dd/yyyy") :
                                            null,
                                  ReciteExpiryDate = (wr.ExpiryDate != null) ? wr.ExpiryDate.Value.ToString("MM/dd/yyyy") :
                                            null,

                              })
                      .AsNoTracking()
                      .ToListAsync();

                List<TblGrnsDTO> ExtendedCoffee = new List<TblGrnsDTO>();
                foreach (var grp in alls)
                {
                    if (grp.ReciteExpiryDate == null || grp.GrncreatedDate == null)
                    {

                    }
                    else if (grp.CommodityID == "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 20)
                    {

                        ExtendedCoffee.Add(grp);

                    }
                    else if (grp.CommodityID != "71604275-DF23-4449-9DAE-36501B14CC3B" && (DateTime.Parse(grp.ReciteExpiryDate).Subtract(DateTime.Parse(grp.GrncreatedDate))).TotalDays > 30)
                    {

                        ExtendedCoffee.Add(grp);

                    }
                }

                var nice = (from tbr in ExtendedCoffee
                            group tbr by new
                            {
                                tbr.Warehouse,
                            } into arrival

                            select new TblExtendedGrnsDTO
                            {
                                TotalNumberOfExtendedGRN = arrival.Count(),
                                Warehouse = arrival.Key.Warehouse,
                                NoClientName = arrival.Count(),
                                NetWeight = arrival.Sum(s=>s.NetWeight)

                            })
                            .OrderBy(s => s.NetWeight)
                            .ToList();


                return nice;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblPickupNoticesDTO>> GetExpiredPickupNotices(string start, string end, int stat)
        {
            IEnumerable<TblPickupNoticesDTO> alls = null;

            try
            {
                var status =(stat == 0) ? _warehouseApplicationcontext.TblPickupNotices.Where(fd => (fd.ExpectedPickupDateTime <= DateTime.Parse(end) && fd.PunprintDateTime >= DateTime.Parse(start)))
                    : _warehouseApplicationcontext.TblPickupNotices.Where(fd => (fd.ExpectedPickupDateTime >= DateTime.Parse(start) && fd.ExpectedPickupDateTime <= DateTime.Parse(end)) && fd.StatusName == "Being Issued" && fd.ExpirationDate < DateTime.Now);

                alls = await (from n in status
                              join cg in _warehouseApplicationcontext.TblPungin
                               on n.Id equals cg.Punid into k
                              from ws in k.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on n.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join gin in _warehouseApplicationcontext.TblGins
                              on ws.Ginid equals gin.Id into g
                              from gil in g.DefaultIfEmpty()
                              orderby (n.ExpirationDate) descending
                              select new TblPickupNoticesDTO
                              {
                                  GinNo = gil.Ginnumber,
                                  Grnno = n.Grnno,
                                  StatusName = n.StatusName,
                                  WarehouseName = n.WarehouseName,
                                  ExpirationDate = (n.ExpirationDate != null) ? n.ExpirationDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  ExpectedPickupDateTime = (n.ExpectedPickupDateTime != null) ? n.ExpectedPickupDateTime.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  ClientName = n.ClientName,
                                  MemberName = n.MemberName,
                                  SellerName = n.SellerName,
                                  WashingMillingStation = n.WashingMillingStation,
                                  ConsignmentType = n.ConsignmentType,
                                  CommodityName = n.CommodityName,
                                  CommodityGradeName = com.Description,
                                  AgentName = n.AgentName,
                                  ProductionYear = n.ProductionYear,
                                  PunprintDateTime = (n.PunprintDateTime != null) ? n.PunprintDateTime.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  WarehouseReceiptNo = n.WarehouseReceiptNo,
                                  WeightInKg = n.WeightInKg,
                                  IssueDate = (gil.DateIssued != null) ? gil.DateIssued.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  TrailerPlateNumber = n.TrailerPlateNumber,
                                  PlateNumber = n.PlateNumber,
                                  AgentIdnumber = n.AgentIdnumber,
                                  AgentTel = n.AgentTel,
                                  QuantityInLot = n.QuantityInLot,
                                  ShedName = n.ShedName,
                                  Cupvalue = n.Cupvalue,
                                  Exported = (n.Exported == true) ? "Yes" : "No",
                                  RawValue = n.RawValue,
                                  TotalValue = n.TotalValue,


                              })
                    .AsNoTracking()
                    .ToListAsync();


                return alls;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblExtendPunexpiryDateDTO>> GetExtendedPickupNotices(string start, string end)
        {
            IEnumerable<TblExtendPunexpiryDateDTO> alls = null;

            try
            {
               // var users = _warehouseApplicationcontext.TblPickupNotices.Where(fd => (fd.ExpirationDate <= DateTime.Parse(end) && fd.ExpirationDate >= DateTime.Parse(start)));
                var status = await _centralDepositorycontext.TblExtendPunexpiryDate.Where(fd => (fd.DateRequested <= DateTime.Parse(end) && fd.DateRequested >= DateTime.Parse(start))).ToListAsync();
                var extendedstatus = await _centralDepositorycontext.TblExtendExpiryDateStatus.ToListAsync();
                alls = await (from n in _warehouseApplicationcontext.TblPickupNotices
                              join pun in status
                              on n.Id equals pun.Punid into k
                              from ws in k.DefaultIfEmpty()
                              join cg in _warehouseApplicationcontext.TblPungin
                               on n.Id equals cg.Punid into s
                              from ks in s.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on n.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join gin in _warehouseApplicationcontext.TblGins
                              on ks.Ginid equals gin.Id into g
                              from gil in g.DefaultIfEmpty()
                              join exs in extendedstatus
                              on ws.Status equals exs.Id into w
                              from exstat in w.DefaultIfEmpty()
                              where n.ExpirationDate <= DateTime.Parse(end) && n.ExpirationDate >= DateTime.Parse(start)

                              select new TblExtendPunexpiryDateDTO
                              {

                                  Status = exstat.Name,
                                  GinNo = gil.Ginnumber,
                                  Grnno = n.Grnno,
                                  WarehouseName = n.WarehouseName,
                                  ClientName = n.ClientName,
                                  MemberName = n.MemberName,
                                  CommodityName = n.CommodityName,
                                  SellerName = n.SellerName,
                                  PunprintDateTime = (n.PunprintDateTime != null) ? n.PunprintDateTime.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  DateApproved = (ws.DateApproved != null) ? ws.DateApproved.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  DateRejected = (ws.DateRejected != null) ? ws.DateRejected.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  NumberOfDays = ws.NumberOfDays,
                                  DateRequested = (ws.DateRequested != null) ? ws.DateRequested.ToString("MM/dd/yyyy") :
                                  null,



                              })
                                .ToListAsync();


                return alls;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO>> GetExtendedPickupNoticesBywarhouse(string start, string end)
        {
            IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO> alls = null;

            try
            {
                //var users = _warehouseApplicationcontext.TblPickupNotices.Where(fd => (fd.ExpirationDate <= DateTime.Parse(end) && fd.ExpirationDate >= DateTime.Parse(start)));
                var status = await _centralDepositorycontext.TblExtendPunexpiryDate.Where(fd => (fd.DateRequested <= DateTime.Parse(end) && fd.DateRequested >= DateTime.Parse(start) && fd.Status ==2)).ToListAsync();
                var extendedstatus = await _centralDepositorycontext.TblExtendExpiryDateStatus.ToListAsync();
                alls = await (from n in _warehouseApplicationcontext.TblPickupNotices
                              join pun in status
                              on n.Id equals pun.Punid into k
                              from ws in k.DefaultIfEmpty()
                              join cg in _warehouseApplicationcontext.TblPungin
                               on n.Id equals cg.Punid into s
                              from ks in s.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on n.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join gin in _warehouseApplicationcontext.TblGins
                              on ks.Ginid equals gin.Id into g
                              from gil in g.DefaultIfEmpty()
                              join exs in extendedstatus
                              on ws.Status equals exs.Id into w
                              from exstat in w.DefaultIfEmpty()
                              where n.ExpirationDate <= DateTime.Parse(end) && n.ExpirationDate >= DateTime.Parse(start)
                              orderby n.WarehouseName
                              select new TblExtendPunexpiryDateBywarehouseNaDTO
                              {

                                  QuantityInLot= n.QuantityInLot,
                                  WarehouseName = n.WarehouseName,
                                  CommodityGradeName = com.Symbol,
                                  



                              })
                                .ToListAsync();

                var extended =  (from tbr in alls
                              group tbr by new
                              {
                                  tbr.WarehouseName,
                                  tbr.CommodityGradeName
                              } into arrival
                          


                              select new TblExtendPunexpiryDateBywarehouseNaDTO
                              {
                                  CommodityGradeName = arrival.Key.CommodityGradeName,
                                 
                                  //ZoneWoreda = zor.Description,
                                  WarehouseName = arrival.Key.WarehouseName,
                                  NumberofPunExtended = arrival.Count(),
                                  QuantityInLot = arrival.Sum(s=>s.QuantityInLot)
                                  

                              })
                   .ToList();



                return extended;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO>> GetExtendedPickupNoticesByClientName(string start, string end)
        {
            IEnumerable<TblExtendPunexpiryDateBywarehouseNaDTO> alls = null;

            try
            {
                //var users = _warehouseApplicationcontext.TblPickupNotices.Where(fd => (fd.ExpirationDate <= DateTime.Parse(end) && fd.ExpirationDate >= DateTime.Parse(start)));
                var status = await _centralDepositorycontext.TblExtendPunexpiryDate.Where(fd => (fd.DateRequested <= DateTime.Parse(end) && fd.DateRequested >= DateTime.Parse(start)&& fd.Status == 2)).ToListAsync();
                var extendedstatus = await _centralDepositorycontext.TblExtendExpiryDateStatus.ToListAsync();
                alls = await (from n in _warehouseApplicationcontext.TblPickupNotices
                              join pun in status
                              on n.Id equals pun.Punid into k
                              from ws in k.DefaultIfEmpty()
                              join cg in _warehouseApplicationcontext.TblPungin
                               on n.Id equals cg.Punid into s
                              from ks in s.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on n.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join gin in _warehouseApplicationcontext.TblGins
                              on ks.Ginid equals gin.Id into g
                              from gil in g.DefaultIfEmpty()
                              join exs in extendedstatus
                              on ws.Status equals exs.Id into w
                              from exstat in w.DefaultIfEmpty()
                              where n.ExpirationDate <= DateTime.Parse(end) && n.ExpirationDate >= DateTime.Parse(start)
                              orderby n.ClientName
                              select new TblExtendPunexpiryDateBywarehouseNaDTO
                              {

                                  QuantityInLot = n.QuantityInLot,
                                  WarehouseName = n.WarehouseName,
                                  CommodityGradeName = com.Symbol,
                                  ClientName = n.ClientName


                              })
                                .ToListAsync();

                var extended = (from tbr in alls
                                group tbr by new
                                {
                                    tbr.WarehouseName,
                                    tbr.CommodityGradeName,
                                    tbr.ClientName
                                } into arrival



                                select new TblExtendPunexpiryDateBywarehouseNaDTO
                                {
                                    CommodityGradeName = arrival.Key.CommodityGradeName,
                                    WarehouseName = arrival.Key.WarehouseName,
                                    NumberofPunExtended = arrival.Count(),
                                    ClientName = arrival.Key.ClientName,
                                    QuantityInLot = arrival.Sum(s => s.QuantityInLot)


                                })
                   .ToList();



                return extended;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<IEnumerable<ReconcilationDTO>> GetReconcilationReport(string start, string end)
        {
            List<ReconcilationDTO> alls = null;

            try
            {
                var centralPsa = await _centralDepositorycontext.TblPsa.Where(fd => fd.TradeDate >= DateTime.Parse(start) && fd.TradeDate <= DateTime.Parse(end)).ToListAsync();
                //var status =_warehouseApplicationcontext.TblPsa.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));
                alls = await (from n in _warehouseApplicationcontext.TblPsa
                              join cg in _warehouseApplicationcontext.TblPunpsa
                              on n.Psaid equals cg.Psaid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.PunId equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on gr.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join gin in _warehouseApplicationcontext.TblPungin
                             on pu.PunId equals gin.Punid into g
                              from gil in g.DefaultIfEmpty()
                              join gn in _warehouseApplicationcontext.TblGins
                              on gil.Ginid equals gn.Id into gl
                              from gne in gl.DefaultIfEmpty()
                              join ps in centralPsa
                              on n.Psaid equals ps.Id into y
                              from psa in y.DefaultIfEmpty()
                              join psstat in _warehouseApplicationcontext.TblPsastatus
                              on n.PsastatusId equals psstat.Id into a
                              from psasta in a.DefaultIfEmpty()
                              orderby (n.DateIssued) descending
                              select new ReconcilationDTO
                              {
                                  AutoNumber = n.AutoNumber,
                                  CommodityGrade = com.Symbol,
                                  GinNo = gne.Ginnumber,
                                  Grnno = gr.Grnno,
                                  WarehouseName = gr.WarehouseName,
                                  ClientName = gr.ClientName,
                                  MemberName = gr.MemberName,
                                  CommodityGradeName = com.Description,
                                  IssuedWeight = n.IssuedWeight,
                                  Punweight = n.Punweight,
                                  WeightDiff = n.WeightDiff,
                                  Psanumber = n.Psanumber,
                                  Symbol = n.Symbol,
                                  Psastatus = psasta.Description,
                                  BagAdjustment = n.BagAdjustment,
                                  ClientSignedName = n.ClientSignedName,
                                  BWhr = n.BWhr,
                                  DateTimeLoaded = (n.DateTimeLoaded != null) ? n.DateTimeLoaded.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  DateIssued = (n.DateIssued != null) ? n.DateIssued.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  CdmanagerRemark = psa.CdmanagerRemark,
                                  InsertedDueTo = n.InsertedDueTo,
                                  IntiatedDueTo = n.InsertedDueTo,
                                  Licname = n.Licname,
                                  LicsignedDate = (n.LicsignedDate != null) ? n.LicsignedDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  NoOfRebags = n.NoOfRebags,
                                  LicsignedName = n.LicsignedName,
                                  NoteType = n.NoteType,
                                  OcapprovedName = n.OcapprovedName,
                                  Ocremark = n.Ocremark,
                                  TradeDate = (n.TradeDate != null) ? n.TradeDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  TradeNo = n.TradeNo,
                                  TransactionId = n.TransactionId,
                                  CdmanagerCreatedDate = (psa.CdmanagerCreatedDate != null) ? psa.CdmanagerCreatedDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  Warehouse = gr.WarehouseName,
                                  WhmanagerRemark = n.WhmanagerRemark,
                                  WhbranchManagerName = n.WhbranchManagerName,
                                  WhmanagerName = n.WhmanagerName,
                                  WhsupervisorName = n.WhsupervisorName,
                                  WhsupervisorPsadescription = n.WhsupervisorPsadescription,
                                  WhsupervisorRemark = n.WhsupervisorRemark,


                              })
                    .AsNoTracking()
                    .ToListAsync();


                return alls;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationReportByWarehouse(string start, string end, Guid? commodityId)
        {
            List<TblShortFallReconcilationDTO> alls = null;

            try
            {
                //var centralPsa = await _centralDepositorycontext.TblPsa.Where(fd => fd.TradeDate >= DateTime.Parse(start) && fd.TradeDate <= DateTime.Parse(end)).ToListAsync();
                //var status = _warehouseApplicationcontext.TblPsa.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));
                alls = await (from n in _warehouseApplicationcontext.TblPsa
                              join cg in _warehouseApplicationcontext.TblPunpsa
                              on n.Psaid equals cg.Psaid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.PunId equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on gr.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join comclass in _warehouseApplicationcontext.ClCommodityClass
                              on com.CommodityClassGuid equals comclass.Guid into z
                              from comcl in z.DefaultIfEmpty()
                              where n.DateIssued >= DateTime.Parse(start) && n.DateIssued <= DateTime.Parse(end) && comcl.CommodityGuid ==commodityId && n.PsastatusId ==8
                              select new TblShortFallReconcilationDTO
                              {
                                  CommodityGrade = com.Symbol,
                                  Warehouse = gr.WarehouseName,
                                  Weight = n.WeightDiff


                              })
                    .AsNoTracking()
                    .ToListAsync();
                var filterdData = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                            } into arrival

                            select new TblShortFallReconcilationDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                Weight = Math.Round(arrival.Sum(dw => dw.Weight).Value, 2),
                                Warehouse = arrival.Key.Warehouse,
                                NoOfClient =arrival.Count()

                            })
                         .ToList();

                return filterdData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationReportByClientName(string start, string end, Guid? commodityId)
        {
            List<TblShortFallReconcilationDTO> alls = null;

            try
            {
                //var centralPsa = await _centralDepositorycontext.TblPsa.Where(fd => fd.TradeDate >= DateTime.Parse(start) && fd.TradeDate <= DateTime.Parse(end)).ToListAsync();
                //var status =_warehouseApplicationcontext.TblPsa.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)&& fd.PsastatusId ==8));
                alls = await (from n in _warehouseApplicationcontext.TblPsa
                              join cg in _warehouseApplicationcontext.TblPunpsa
                              on n.Psaid equals cg.Psaid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.PunId equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on gr.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join comclass in _warehouseApplicationcontext.ClCommodityClass
                              on com.CommodityClassGuid equals comclass.Guid into z
                              from comcl in z.DefaultIfEmpty()
                              where comcl.CommodityGuid ==commodityId && n.PsastatusId == 8 && n.DateIssued >= DateTime.Parse(start) && n.DateIssued <= DateTime.Parse(end)
                              select new TblShortFallReconcilationDTO
                              {
                                  CommodityGrade = com.Symbol,
                                  Warehouse = gr.WarehouseName,
                                  Weight = n.WeightDiff,
                                  ClientName =gr.ClientName
                                  

                              })
                    .AsNoTracking()
                    .ToListAsync();
                var nice = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                                tbr.ClientName
                            } into arrival

                            select new TblShortFallReconcilationDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                Weight = Math.Round(arrival.Sum(dw => dw.Weight).Value, 2),
                                Warehouse = arrival.Key.Warehouse,
                                ClientName = arrival.Key.ClientName

                            })
                         .ToList();

                return alls;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationReportByCommodityGrade(string start, string end,Guid? commodityId)
        {
            List<TblShortFallReconcilationDTO> alls = null;

            try
            {
                //var centralPsa = await _centralDepositorycontext.TblPsa.Where(fd => fd.TradeDate >= DateTime.Parse(start) && fd.TradeDate <= DateTime.Parse(end)).ToListAsync();
                //var status = _warehouseApplicationcontext.TblPsa.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));
                alls = await (from n in _warehouseApplicationcontext.TblPsa
                              join cg in _warehouseApplicationcontext.TblPunpsa
                              on n.Psaid equals cg.Psaid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.PunId equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on gr.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join comclass in _warehouseApplicationcontext.ClCommodityClass
                              on com.CommodityClassGuid equals comclass.Guid into z
                              from comcl in z.DefaultIfEmpty()
                              where n.DateIssued >= DateTime.Parse(start) && n.DateIssued <= DateTime.Parse(end) && comcl.CommodityGuid == commodityId && n.PsastatusId == 8
                              select new TblShortFallReconcilationDTO
                              {
                                  CommodityGrade = com.Symbol,
                                  Warehouse = gr.WarehouseName,
                                  Weight = n.WeightDiff


                              })
                    .AsNoTracking()
                    .ToListAsync();
                var reconcilationdata = (from tbr in alls
                            group tbr by new
                            {
                                tbr.Warehouse,
                            } into arrival

                            select new TblShortFallReconcilationDTO
                            {
                                Weight = Math.Round(arrival.Sum(dw => dw.Weight).Value, 2),
                                Warehouse = arrival.Key.Warehouse,
                                NoOfClient = arrival.Count()
                            })
                         .OrderBy(s=>s.Weight)
                         .ToList();

                return reconcilationdata;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblShortFallReconcilationDTO>> GetReconcilationReportByRemark(string start, string end, Guid? commodityGradeId, Guid? warehouseId)
        {
            List<TblShortFallReconcilationDTO> alls = null;

            try
            {
                //var centralPsa = await _centralDepositorycontext.TblPsa.Where(fd => fd.TradeDate >= DateTime.Parse(start) && fd.TradeDate <= DateTime.Parse(end)).ToListAsync();
                //var status = _warehouseApplicationcontext.TblPsa.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));

                alls = await (from n in _warehouseApplicationcontext.TblPsa
                              join cg in _warehouseApplicationcontext.TblPunpsa
                              on n.Psaid equals cg.Psaid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.PunId equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              join cog in _warehouseApplicationcontext.TblCommodityGrade
                              on gr.CommodityGradeId equals cog.Guid into j
                              from com in j.DefaultIfEmpty()
                              join sa in _warehouseApplicationcontext.ClCommodityClass
                              on com.CommodityClassGuid equals sa.Guid into l
                              from cmc in l.DefaultIfEmpty()
                              where cmc.CommodityGuid==commodityGradeId && n.DateIssued >= DateTime.Parse(start) && n.DateIssued <= DateTime.Parse(end) && n.WarehouseId == warehouseId && n.PsastatusId ==8
                              select new TblShortFallReconcilationDTO
                              {
                                
                                  Remark = n.WhsupervisorRemark.ToUpper()

                              })
                    .AsNoTracking()
                    .ToListAsync();
                var filterdData = (from tbr in alls
                            group tbr by new
                            {
                                tbr.Remark,
                            } into arrival

                            select new TblShortFallReconcilationDTO
                            {
                                //CommodityGrade = arrival.Key.CommodityGrade,
                                //Weight = arrival.Sum(dw => dw.Weight),
                                //Warehouse = arrival.Key.Warehouse,
                                Remark = arrival.Key.Remark,
                                NoOfClient = arrival.Count()

                            })
                         .ToList();

                return filterdData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        //public async Task<IEnumerable<EcxInventoryPositionDTO>> GetWarehouseInventoryPosition(string start, string end)
        //{
        //    IEnumerable<EcxInventoryPositionDTO> alls = null;

        //    try
        //    {
        //        if (start == "null" && end == "null")
        //        {
        //            alls = await _context.EcxInventoryPosition
        //           .Select(n => new EcxInventoryPositionDTO
        //           {
        //               No = n.No,
        //               WarehouseLocation = n.WarehouseLocation,
        //               WarehouseName = n.WarehouseName,
        //               WarehouseNo = n.WarehouseNo,
        //               CurrentInventoryNoOfbags = n.CurrentInventoryNoOfbags,
        //               CommodityType = n.CommodityType,
        //               CurrentInventoryWeight = n.CurrentInventoryWeight,
        //               ExcessProductsWeight = n.ExcessProductsWeight,
        //               SampleLeftoversNoOfbags = n.SampleLeftoversNoOfbags,
        //               ShortfallProductsWeight = n.ShortfallProductsWeight,
        //               CommodityGrade = n.CommodityGrade,
        //               ExcessProductsNoOfbags = n.ExcessProductsNoOfbags,
        //               SampleLeftoversWeight = n.SampleLeftoversWeight,
        //               ShortfallProductsNoOfbags = n.ShortfallProductsNoOfbags
        //           })
        //           .AsNoTracking()
        //           .ToListAsync();

        //            return alls;
        //        }
        //        else
        //        {
        //            alls = await (from n in _context.EcxInventoryPosition
        //                          where (
        //                          (start == null || (n.CreatedDateTime >= DateTime.Parse(start))) &&
        //                          (end == null || (n.CreatedDateTime <= DateTime.Parse(end)))
        //                          )
        //                          orderby (n.CreatedDateTime) descending
        //                          select new EcxInventoryPositionDTO
        //                          {
        //                              No = n.No,
        //                              WarehouseLocation = n.WarehouseLocation,
        //                              WarehouseName = n.WarehouseName,
        //                              WarehouseNo = n.WarehouseNo,
        //                              CurrentInventoryNoOfbags = n.CurrentInventoryNoOfbags,
        //                              CommodityType = n.CommodityType,
        //                              CurrentInventoryWeight = n.CurrentInventoryWeight,
        //                              ExcessProductsWeight = n.ExcessProductsWeight,
        //                              SampleLeftoversNoOfbags = n.SampleLeftoversNoOfbags,
        //                              ShortfallProductsWeight = n.ShortfallProductsWeight,
        //                              CommodityGrade = n.CommodityGrade,
        //                              ExcessProductsNoOfbags = n.ExcessProductsNoOfbags,
        //                              SampleLeftoversWeight = n.SampleLeftoversWeight,
        //                              ShortfallProductsNoOfbags = n.ShortfallProductsNoOfbags
        //                          })
        //          .AsNoTracking()
        //          .ToListAsync();

        //            return alls;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //}

        public async Task<IEnumerable<TblGinsDTO>> GetWORDNGIN(string start, string end)
        {
            IEnumerable<TblGinsDTO> alls = null;

            try
            {
                var status = _warehouseApplicationcontext.TblGins.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));

                alls = await (from gin in status
                              join sa in _warehouseApplicationcontext.ClGinstatus
                              on gin.GinstatusId equals sa.GinstatusId
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              join cg in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals cg.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join grn in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals grn.Id into r
                              from gr in r.DefaultIfEmpty()
                              orderby gin.DateIssued descending
                              select new TblGinsDTO
                              {
                                  CommodityGrade = gr.CommodityName,
                                  ClientSignedName = gin.ClientSignedName,
                                  Ginnumber = gin.Ginnumber,
                                  Ginstatus = sa.EnumName,
                                  
                                  AutoNumber = gin.AutoNumber,
                                  BagAdjustment = gin.BagAdjustment,
                                  ClientSignedDate = (gin.ClientSignedDate != null) ? gin.ClientSignedDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  DateIssued = (gin.DateIssued != null) ? gin.DateIssued.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  IsPsa = (gin.IsPsa == true) ? "Yes" : "No",
                                  DriverName = gin.DriverName,
                                  DateTimeLoaded = (gin.DateTimeLoaded != null) ? gin.DateTimeLoaded.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  Warehouse = wa.Description,
                                  WeightAdjustment = gin.WeightAdjustment,
                                  IssuedBags = gin.IssuedBags,
                                  IssuedWeight = gin.IssuedWeight,
                                  LeadInventoryController = gin.LeadInventoryController,
                                  TruckRequestTime = (gin.TruckRegisterTime != null) ? gin.TruckRegisterTime.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  TransactionId = gin.TransactionId,
                                  TruckRegisterTime = gin.TruckRegisterTime,
                                  TrailerPlateNumber = gin.TrailerPlateNumber,
                                  LicenseNumber = gin.LicenseNumber,
                                  Remark = gin.Remark,
                                  Psaremark = gin.Psaremark,
                                  PlateNumber = gin.PlateNumber,
                                  NoOfRebags = gin.NoOfRebags,
                                  LicsignedName = gin.LicsignedName,

                                  LicsignedDate = (gin.LicsignedDate != null) ? gin.LicsignedDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  ManagerSignedName = gin.ManagerSignedName,
                                  ManagerSignedDate = (gin.ManagerSignedDate != null) ? gin.ManagerSignedDate.Value.ToString("MM/dd/yyyy") :
                                  null,
                                  AdjustmentTypeId = gin.AdjustmentTypeId,
                                  ScaleTicketNumber = gin.ScaleTicketNumber,
                                  LoadUnloadTicketNo = gin.LoadUnloadTicketNo,

                              })
                  .AsNoTracking()
                  .ToListAsync();


                return alls;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGINByWarehouse(string start, string end)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
                //var status = _warehouseApplicationcontext.TblGins.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));

                alls = await (from gin in _warehouseApplicationcontext.TblGins
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id  into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end)
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,
                                  

                              })
                  .AsNoTracking()
                  .ToListAsync();
                var nice = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value,2),
                                //ZoneWoreda = zor.Description,
                                Warehouse = arrival.Key.Warehouse,


                            })
                         .ToList();


                return nice;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGINByCommodityGrade(string start, string end, Guid? commdityGradeId)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
              //  var status = _warehouseApplicationcontext.TblGins.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));

                alls = await (from gin in _warehouseApplicationcontext.TblGins
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join sa in _warehouseApplicationcontext.ClCommodityClass
                              on cg.CommodityClassGuid equals sa.Guid into l
                              from cmc in l.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where cmc.CommodityGuid==commdityGradeId && pun.StatusId == 3 && gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end)
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,


                              })
                  .AsNoTracking()
                  .ToListAsync();
                var ginbywarehoue = (from tbr in alls
                            group tbr by new
                            {
                                tbr.Warehouse,
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value, 2),
                                //ZoneWoreda = zor.Description,
                                Warehouse = arrival.Key.Warehouse,
                                NumberOfClient = arrival.Count()

                            })
                         .OrderBy(s=>s.QuantityInLot)
                         .ToList();


                return ginbywarehoue;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGINByClientName(string start, string end)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
               // var status = _warehouseApplicationcontext.TblGins.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end)));

                alls = await (from gin in _warehouseApplicationcontext.TblGins
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where pun.StatusId ==3 && gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end)
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,
                                  ClientName =pun.ClientName

                              })
                  .AsNoTracking()
                  .ToListAsync();
                var nice = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                                tbr.ClientName
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value, 2),
                                ClientName = arrival.Key.ClientName,
                                Warehouse = arrival.Key.Warehouse,


                            })
                         .ToList();


                return nice;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinCancledByWarehouseName(string start, string end)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
               // var status = _warehouseApplicationcontext.TblGins.Where(fd => fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end) && fd.GinstatusId == 170);

                alls = await (from gin in _warehouseApplicationcontext.TblGins
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end) && gin.GinstatusId == 170
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,

                              })
                  .AsNoTracking()
                  .ToListAsync();
                var CancedGin = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value, 2),
                                Warehouse = arrival.Key.Warehouse,
                                CanceldGIN = arrival.Count()


                            })
                          .OrderBy(s=>s.CanceldGIN)
                         .ToList();


                return CancedGin;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGinCancledByClientName(string start, string end)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
                //var status = _warehouseApplicationcontext.TblGins.Where(fd => fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end) && fd.GinstatusId == 170);

                alls = await (from gin in _warehouseApplicationcontext.TblGins
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end)&& gin.GinstatusId == 170
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,
                                  ClientName = pun.ClientName

                              })
                  .AsNoTracking()
                  .ToListAsync();
                var nice = (from tbr in alls
                            group tbr by new
                            {
                                tbr.CommodityGrade,
                                tbr.Warehouse,
                                tbr.ClientName
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                CommodityGrade = arrival.Key.CommodityGrade,
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value, 2),
                                ClientName = arrival.Key.ClientName,
                                Warehouse = arrival.Key.Warehouse,
                                CanceldGIN = arrival.Count()


                            })
                         .OrderBy(s=>s.CanceldGIN)
                         .ToList();


                return nice;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<IEnumerable<TblIssudeGinsDTO>> GetGINCancledByCommodityGrade(string start, string end, Guid? commdityGradeId)
        {
            IEnumerable<TblIssudeGinsDTO> alls = null;

            try
            {
                var status = _warehouseApplicationcontext.TblGins.Where(fd => (fd.DateIssued >= DateTime.Parse(start) && fd.DateIssued <= DateTime.Parse(end))&& fd.GinstatusId == 170);

                alls = await (from gin in status
                              join sa in _warehouseApplicationcontext.TblPungin
                              on gin.Id equals sa.Ginid into k
                              from pu in k.DefaultIfEmpty()
                              join tr in _warehouseApplicationcontext.TblPickupNotices
                              on pu.Punid equals tr.Id into j
                              from pun in j.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.TblCommodityGrade
                              on pun.CommodityGradeId equals wa.Guid into z
                              from cg in z.DefaultIfEmpty()
                              join sa in _warehouseApplicationcontext.ClCommodityClass
                              on cg.CommodityClassGuid equals sa.Guid into l
                              from cmc in l.DefaultIfEmpty()
                              join wa in _warehouseApplicationcontext.ClWarehouses
                              on gin.WarehouseId equals wa.Id
                              where cmc.CommodityGuid==commdityGradeId&&gin.DateIssued >= DateTime.Parse(start) && gin.DateIssued <= DateTime.Parse(end)&&gin.GinstatusId==170 
                              orderby gin.DateIssued descending
                              select new TblIssudeGinsDTO
                              {
                                  CommodityGrade = cg.Symbol,
                                  QuantityInLot = pun.QuantityInLot,
                                  Warehouse = wa.Description,


                              })
                  .AsNoTracking()
                  .ToListAsync();
                var nice = (from tbr in alls
                            group tbr by new
                            {
                                tbr.Warehouse,
                            } into arrival

                            select new TblIssudeGinsDTO
                            {
                                QuantityInLot = Math.Round(arrival.Sum(dw => dw.QuantityInLot).Value, 2),
                                //ZoneWoreda = zor.Description,
                                Warehouse = arrival.Key.Warehouse,
                                CanceldGIN = arrival.Count()


                            })
                         .OrderBy(s=>s.QuantityInLot)
                         .ToList();


                return nice;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }

}
