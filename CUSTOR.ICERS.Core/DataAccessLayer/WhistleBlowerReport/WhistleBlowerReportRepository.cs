﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTORCommon.Ethiopic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.DataAccessLayer.WhistleBlowerReport
{
   public class WhistleBlowerReportRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;


        public WhistleBlowerReportRepository(ECEADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> PostNewWhisterBlowerReport(WhistleBlowerDTO whistleBlowerReport)
        {
            try
            {
                WhistleblowerReport newReport = _mapper.Map<WhistleblowerReport>(whistleBlowerReport);
                _context.Add(newReport);
                await _context.SaveChangesAsync();
                return newReport.Id;
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<int> UpdateWhisterBlowerReport(WhistleBlowerDTO whistleBlowerReport)
        {
            try
            {

                WhistleblowerReport oldreport = await _context.WhistleblowerReport.Where(s=>s.Id == whistleBlowerReport.Id).FirstOrDefaultAsync();

                oldreport.UpdatedDateTime = DateTime.Now;
                oldreport.Url = whistleBlowerReport.Url;
                oldreport.UpdatedUserId = whistleBlowerReport.UpdatedUserId;
                oldreport.InformationType = whistleBlowerReport.InformationType;
                oldreport.Name = whistleBlowerReport.Name;
                oldreport.SourceOfInformation = whistleBlowerReport.SourceOfInformation;
                oldreport.Remark = whistleBlowerReport.Remark;
                oldreport.Eventdate = whistleBlowerReport.Eventdate;
                oldreport.RegisterdDate = whistleBlowerReport.RegisterdDate;


                _context.Update(oldreport);
                await _context.SaveChangesAsync();
                return oldreport.Id;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }


        }

        public async Task<List<WhistleBlowerListDTO>> GetAllWhisterBlowerReport(string lang)
        {
            List<WhistleBlowerListDTO> reportList = null;
            try
            {
                 reportList = await (from c in _context.WhistleblowerReport
                            join sa in _context.Lookup
                            on c.SourceOfInformation equals sa.LookupId
                            where c.IsDeleted == false
                            select new WhistleBlowerListDTO
                            {
                                Id = c.Id,
                                InformationType = c.InformationType,
                                InformationTypeDescription = (c.InformationType == 1 && lang == "et") ? "ድርጅት" : (c.InformationType == 2 && lang == "et") ? "በግል" :
                                (c.InformationType == 1 && lang == "et") ? "Organization" : "Sole",
                                EventdateDec = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.Eventdate).ToString()) : c.Eventdate.ToString("MMMM dd, yyyy"),
                                RegisterdDateDec = lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.RegisterdDate).ToString()) : c.RegisterdDate.ToString("MMMM dd, yyyy"),
                                Eventdate = c.Eventdate,
                                RegisterdDate = c.RegisterdDate,
                                Name = c.Name,
                                SourceOfInformationDescription = lang == "et" ? sa.DescriptionAmh : sa.DescriptionEng,
                                SourceOfInformation = c.SourceOfInformation,
                                Remark = c.Remark,
                                Url =c.Url

                        
                    })
                    .AsNoTracking()
                    .ToListAsync();

                return reportList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
          


        }
        public async Task<List<WhistleBlowerListDTO>> GetAllSearchWhisterBlowerList(SearchSqlBlowersQuery sqlBlowersQuery)
        {
            List<WhistleBlowerListDTO> reportList = null;
            try
            {

                reportList = await (from c in _context.WhistleblowerReport
                                    join sa in _context.Lookup
                                    on c.SourceOfInformation equals sa.LookupId
                                    where c.IsDeleted == false && c.Eventdate.Date >= sqlBlowersQuery.From.Date &&
                                          c.Eventdate.Date <= sqlBlowersQuery.To.Date
                                    select new WhistleBlowerListDTO
                                    {
                                        Id = c.Id,
                                        InformationType = c.InformationType,
                                        InformationTypeDescription = (c.InformationType == 1 && sqlBlowersQuery.Lang == "et") ? "ድርጅት" : (c.InformationType == 2 && sqlBlowersQuery.Lang == "et") ? "በግል" :
                                        (c.InformationType == 1 && sqlBlowersQuery.Lang == "et") ? "Organization" : "Sole",
                                        EventdateDec = sqlBlowersQuery.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.Eventdate).ToString()) : c.Eventdate.ToString("MMMM dd, yyyy"),
                                        RegisterdDateDec = sqlBlowersQuery.Lang == "et" ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(c.RegisterdDate).ToString()) : c.RegisterdDate.ToString("MMMM dd, yyyy"),
                                        Eventdate = c.Eventdate,
                                        RegisterdDate = c.RegisterdDate,
                                        Name = c.Name,
                                        SourceOfInformationDescription = sqlBlowersQuery.Lang == "et" ? sa.DescriptionAmh : sa.DescriptionEng,
                                        SourceOfInformation = c.SourceOfInformation,
                                        Remark = c.Remark,
                                        Url =c.Url


                                    })
                   .AsNoTracking()
                   .ToListAsync();

                return reportList;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }



        }

        public async Task<bool> DeleteReport(WhistleBlowerDTO whistleBlowerReport)
        {
            try
            {
                var oldreport = await _context.WhistleblowerReport.FirstOrDefaultAsync(Inj =>
                    Inj.Id == whistleBlowerReport.Id);
                if (oldreport != null)
                {
                    oldreport.IsDeleted = true;
                    await _context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
