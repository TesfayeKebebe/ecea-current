﻿using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.Enum;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CUSTOR.ICERS.Core.DataAccessLayer.WorkFlowProcess
{

    public class WorkFlowRepository
    {
        private readonly ECEADbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public WorkFlowRepository(ECEADbContext context, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<bool> PostRecognitionWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                ServiceApplication serviceApplication = await _context.ServiceApplication
                    .FirstOrDefaultAsync(sa => sa.ServiceApplicationId == workFLow.ServiceApplicationId);

                if (serviceApplication == null)
                {
                    return false;
                }


                // approved by general director
                if ((workFLow.CustomerTypeId == (int)CustomerType.NationalTradeAssociation || workFLow.CustomerTypeId == (int)CustomerType.Auditor || workFLow.CustomerTypeId == (int)CustomerType.SeetlementBank || workFLow.CustomerTypeId == (int)CustomerType.NonMemberTrading || workFLow.CustomerTypeId == (int)CustomerType.Member) && workFLow.FinalDec == 3)
                {
                    serviceApplication.Status = (int)ServiceApplicaitonStatus.Approved;
                    serviceApplication.ApprovalFinished = true;
                    serviceApplication.ApprovalStatus = workFLow.FinalDec;


                }
                else if ((workFLow.CustomerTypeId == (int)CustomerType.Representative || workFLow.CustomerTypeId == (int)CustomerType.NonMemberDirectTraderTradeReprsentative) && workFLow.FinalDec == 2)
                {
                    // appored by Department director
                    serviceApplication.Status = (int)ServiceApplicaitonStatus.Approved;
                    serviceApplication.ApprovalFinished = true;
                    serviceApplication.ApprovalStatus = workFLow.FinalDec;
                }
                else
                {
                    serviceApplication.ApprovalStatus = workFLow.FinalDec;
                    serviceApplication.ApprovalFinished = false;
                    serviceApplication.Status = (int)ServiceApplicaitonStatus.WaitingForApproval;
                }



                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);

                        _context.WorkFlow.Add(newWorkFlow);

                        _context.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<bool> PostMemberViolationWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                MemberTradeViolation memberViolation = await _context.MemberTradeViolation
                    .FirstOrDefaultAsync(sa => sa.MemberTradeViolationId == workFLow.MemberTradeViolationId);

                if (memberViolation == null)
                {
                    return false;
                }
                if(workFLow.FinalDec == 1 || workFLow.FinalDec == 4)
                {
                    TradeExcutionViolationRecord tradeExcutionViolation = await _context.TradeExcutionViolationRecord.Where(
                                                                                             x => x.Status == 0 && x.MemberTradeViolationId == workFLow.MemberTradeViolationId)
                                                                                             .FirstOrDefaultAsync();
                    if(tradeExcutionViolation == null)
                    {
                        return false;
                    }
                    tradeExcutionViolation.CheckedBy = workFLow.ProcessedByName;
                    tradeExcutionViolation.Status = workFLow.FinalDec;
                    tradeExcutionViolation.CheckerRemark = workFLow.Comment;
                    if(workFLow.FinalDec == 4)
                    {
                        tradeExcutionViolation.CheckerDecision = 1;
                    }
                    else
                    {
                        tradeExcutionViolation.CheckerDecision = 2;
                    }
                    
                    tradeExcutionViolation.CheckedDate = DateTime.Now;
                }else if(workFLow.FinalDec == 2 || workFLow.FinalDec == 5)
                {
                    TradeExcutionViolationRecord tradeExcutionViolation = await _context.TradeExcutionViolationRecord.Where(
                                                                                                                 x => x.Status == 1 && x.MemberTradeViolationId == workFLow.MemberTradeViolationId)
                                                                                                                 .FirstOrDefaultAsync();
                    if (tradeExcutionViolation == null)
                    {
                        return false;
                    }
                    tradeExcutionViolation.ApprovedBy = workFLow.ProcessedByName;
                    tradeExcutionViolation.Status = workFLow.FinalDec;
                    tradeExcutionViolation.ApproverRemark = workFLow.Comment;
                    if(workFLow.FinalDec == 5)
                    {
                        tradeExcutionViolation.ApproverDecision = 1;
                    }
                    else
                    {
                        tradeExcutionViolation.ApproverDecision = 2;
                    }                    
                    tradeExcutionViolation.ApprovedDate = DateTime.Now;
                }
                
                memberViolation.Status = workFLow.FinalDec;

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);

                        _context.WorkFlow.Add(newWorkFlow);

                        _context.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<bool> PostInjunctionWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                Injunction injunction = await _context.Injunction
                    .FirstOrDefaultAsync(inj => inj.InjunctionId == workFLow.InjunctionId);


                if (injunction == null)
                {
                    return false;
                }
                ExchangeActor exchangeActor = await _context.ExchangeActor.FirstOrDefaultAsync(ex => ex.ExchangeActorId == injunction.ExchangeActorId);


            



                if (exchangeActor == null)
                {
                    return false;
                }

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        exchangeActor.Status = (workFLow.FinalDec == 1
                            || workFLow.FinalDec == 2
                            || workFLow.FinalDec == 3) ? (int)ExchangeActorStatus.Under_Injunction : (int)ExchangeActorStatus.Active;
                        await _context.SaveChangesAsync();


                        injunction.ApprovalDecision = workFLow.FinalDec;
                        injunction.DecisionMadeBy = workFLow.CreatedUserId;
                        injunction.InjunctionStatus = (workFLow.FinalDec == 1 
                            || workFLow.FinalDec == 2 
                            || workFLow.FinalDec == 3) ? 1 : 0;
                        injunction.ApprovalFinished = (workFLow.FinalDec == 1 
                            || workFLow.FinalDec == 2 || workFLow.FinalDec == 3);
                        injunction.InjunctionStatus = (workFLow.FinalDec == 1 
                            || workFLow.FinalDec == 2 || workFLow.FinalDec == 3) ? 1 : 0;
                        await _context.SaveChangesAsync();

                        var user = await _context.Users.Where(us => us.ExchangeActorId == exchangeActor.ExchangeActorId.ToString()).FirstOrDefaultAsync();

                        if (user != null)
                        {
                            user.IsEnabled = false;
                        }
                        await _context.SaveChangesAsync();

                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);
                        _context.WorkFlow.Add(newWorkFlow);
                        _context.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<bool> PostMemberClientWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                var memberViolation = await _context.MemberClientTrade
                    .FirstOrDefaultAsync(sa => sa.MemberClientTradeId == workFLow.MemberClientTradeId);

                if (memberViolation == null)
                {
                    return false;
                }
                if(workFLow.FinalDec == 0) {
                    memberViolation.TradeExcutionStatusTypeId = 3;
                }

                memberViolation.WorkFlowStatus = workFLow.FinalDec;

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);

                        _context.WorkFlow.Add(newWorkFlow);

                        _context.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }


                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<WorkFlowViewModel>> GetTradeExecutionWorkFlow(int memberClientId, string lang)
        {
            try
            {
                var memberViolation = await _context.WorkFlow.Where(x => x.MemberClientTradeId == memberClientId).OrderByDescending(x => x.CreatedDateTime)
                                    .Select(x => new WorkFlowViewModel
                                    {
                                        ProcessedByName = x.ProcessedByName,
                                        FinalDec = x.FinalDec,
                                        Comment = x.Comment,
                                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.CreatedDateTime).ToString()) :
                                                     x.CreatedDateTime.ToString("MMMM DD, YYYY"),
                                    })
                                   .AsNoTracking()
                                   .ToListAsync();
                return memberViolation;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<WorkFlowViewModel>> GetTradeViolationWorkFlow(int violationId, string lang)
        {
            try
            {
                var memberViolation = await _context.WorkFlow.Where(x => x.MemberTradeViolationId == violationId).OrderByDescending(x => x.CreatedDateTime)
                                    .Select(x => new WorkFlowViewModel
                                    {
                                        ProcessedByName = x.ProcessedByName,
                                        FinalDec = x.FinalDec,
                                        Comment = x.Comment,
                                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.CreatedDateTime).ToString()) :
                                                     x.CreatedDateTime.Date.ToString(),
                                    })
                                   .AsNoTracking()
                                   .ToListAsync();
                return memberViolation;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<WorkFlowViewModel>> GetServiceApplicationWorkFlow(Guid serviceApplicationId, string lang)
        {
            try
            {
                var memberViolation = await _context.WorkFlow.Where(x => x.ServiceApplicationId == serviceApplicationId).OrderByDescending(x => x.CreatedDateTime)
                                    .Select(x => new WorkFlowViewModel
                                    {
                                        ProcessedByName = x.ProcessedByName,
                                        FinalDec = x.FinalDec,
                                        Comment = x.Comment,
                                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.CreatedDateTime).ToString()) :
                                                     x.CreatedDateTime.ToString("MMMM dd, yyyy"),
                                    })
                                   .AsNoTracking()
                                   .ToListAsync();
                return memberViolation;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<WorkFlowViewModel>> GetOffSiteMonitoringWorkFlow(int offSiteMonitoringId, string lang)
        {
            try
            {
                var offSiteMonitoring = await _context.WorkFlow.Where(x => x.OffSiteMonitoringReportId == offSiteMonitoringId).OrderByDescending(x => x.CreatedDateTime)
                                    .Select(x => new WorkFlowViewModel
                                    {
                                        ProcessedByName = x.ProcessedByName,
                                        FinalDec = x.FinalDec,
                                        Comment = x.Comment,
                                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.CreatedDateTime.Date).ToString()) :
                                                     x.CreatedDateTime.Date.ToString(),
                                    })
                                   .AsNoTracking()
                                   .ToListAsync();
                return offSiteMonitoring;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostOffSiteMonitoringWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                var offSiteMonitoring = await _context.OffSiteMonitoringReport
                    .FirstOrDefaultAsync(sa => sa.OffSiteMonitoringReportId == workFLow.OffSiteMonitoringReportId);

                if (offSiteMonitoring == null)
                {
                    return false;
                }


                offSiteMonitoring.Status = workFLow.FinalDec;

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);

                        _context.WorkFlow.Add(newWorkFlow);

                        _context.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }


                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<bool> PostBankOverSightViolationWorkFlow(WorkFlowDTO workFLow)
        {
            try
            {
                BankOversightViolation memberViolation = await _context.BankOversightViolation
                    .FirstOrDefaultAsync(sa => sa.BankOversightViolationId == workFLow.BankOversightViolationId);

                if (memberViolation == null)
                {
                    return false;
                }
                if (workFLow.FinalDec == 1 || workFLow.FinalDec == 4)
                {
                    BankOverSightViolationDetail tradeExcutionViolation = await _context.BankOverSightViolationDetail.Where(
                                                                                             x => x.Status == 0 && x.BankOversightViolationId == workFLow.BankOversightViolationId)
                                                                                             .FirstOrDefaultAsync();
                    if (tradeExcutionViolation == null)
                    {
                        return false;
                    }
                    tradeExcutionViolation.Status = workFLow.FinalDec;
                    if (workFLow.FinalDec == 4)
                    {
                        tradeExcutionViolation.Status = 4;
                    }
                    

                }
                else if (workFLow.FinalDec == 2 || workFLow.FinalDec == 5)
                {
                    BankOverSightViolationDetail tradeExcutionViolation = await _context.BankOverSightViolationDetail.Where(
                                                                                             x => x.Status == 1 && x.BankOversightViolationId == workFLow.BankOversightViolationId)
                                                                                             .FirstOrDefaultAsync();
                    if (tradeExcutionViolation == null)
                    {
                        return false;
                    }
                    tradeExcutionViolation.Status = workFLow.FinalDec;
                    if (workFLow.FinalDec == 4)
                    {
                        tradeExcutionViolation.Status = 5;
                    }
                }

                    memberViolation.Status = workFLow.FinalDec;

                using (var transaction = await _context.Database.BeginTransactionAsync())
                {

                    try
                    {
                        WorkFlow newWorkFlow = _mapper.Map<WorkFlow>(workFLow);

                        _context.WorkFlow.Add(newWorkFlow);

                        _context.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.ToString());
            }
        }
        public async Task<List<WorkFlowViewModel>> GetBankViolationDecisionWorkFlow(int bankViolationId, string lang)
        {
            try
            {
                var offSiteMonitoring = await _context.WorkFlow.Where(x => x.BankOversightViolationId == bankViolationId).OrderByDescending(x => x.CreatedDateTime)
                                    .Select(x => new WorkFlowViewModel
                                    {
                                        ProcessedByName = x.ProcessedByName,
                                        FinalDec = x.FinalDec,
                                        Comment = x.Comment,
                                        CreatedDate = (lang == "et") ? EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(x.CreatedDateTime.Date).ToString()) :
                                                     x.CreatedDateTime.Date.ToString(),
                                    })
                                   .AsNoTracking()
                                   .ToListAsync();
                return offSiteMonitoring;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}