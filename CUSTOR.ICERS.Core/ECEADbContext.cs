
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.DataChange;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.Security;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CUSTOR.OTRLS.Core.EntityLayer.ECX;
using CUSTOR.ICERS.Core.EntityLayer.ECX;
using CUSTOR.ICERS.Core.EntityLayer.ViewModel;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using CUSTOR.ICERS.Core.EntityLayer.Setting;
using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.API.Controllers.TradeExcution;
using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.DAL.Entity.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty;

namespace CUSTOR.ICERS.Core
{
    public partial class ECEADbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ECEADbContext(DbContextOptions<ECEADbContext> options)
            : base(options)
        {
        }
        public string CurrentUserId { get; set; }
        public string CurrentUserName { get; set; }
        public virtual DbSet<ExchangeActor> ExchangeActor { get; set; }
        public virtual DbSet<GeneralSetting> GeneralSetting { get; set; }
        public virtual DbSet<RecoginationTotalCapital> RecoginationTotalCapital { get; set; }
        public virtual DbSet<PeriodicDurationSetting> PeriodicDurationSetting { get; set; }
        public virtual DbSet<OwnerManager> OwnerManager { get; set; }
        public virtual DbSet<Prerequisite> Prerequisite { get; set; }
        public virtual DbSet<CustomerPrerequisite> CustomerPrerequisite { get; set; }
        public virtual DbSet<ServiceApplication> ServiceApplication { get; set; }
        public virtual DbSet<ServiceTariff> ServiceTariff { get; set; }
        public virtual DbSet<Lookup> Lookup { get; set; }
        public virtual DbSet<PaymentAuthorized> PaymentAuthorized { get; set; }
        public virtual DbSet<PaymentOrder> PaymentOrder { get; set; }
        public virtual DbSet<PaymentOrderDetail> PaymentOrderDetail { get; set; }
        public virtual DbSet<Receipt> Receipt { get; set; }
        public virtual DbSet<Certificate> Certificate { get; set; }
        public virtual DbSet<EntityLayer.Commodity> Commodity { get; set; }
        public virtual DbSet<SisterCompany> SisterCompany { get; set; }
        public virtual DbSet<EcxMemberSisterCompany> EcxMemberSisterCompany { get; set; }
        public virtual DbSet<Injunction> Injunction { get; set; }
        public virtual DbSet<Oversight_report_criteria> Oversight_report_criteria { get; set; }
        public virtual DbSet<OversightFollowUpReport> OversightFollowUpReport { get; set; }
        public virtual DbSet<OversightReportDetail> OversightReportDetail { get; set; }
        public virtual DbSet<OversightReportSetting> OversightReportSetting { get; set; }
        public virtual DbSet<OversightSchedule> OversightSchedule { get; set; }
        public virtual DbSet<SettlementBankContact> SettlementBankContact { get; set; }
        //notification table
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<ApplyTo> ApplyTo { get; set; }

        //warehouse 

        public virtual DbSet<TblArrivals> TblArrivals { get; set; }
        public virtual DbSet<WhistleblowerReport> WhistleblowerReport { get; set; }
        public virtual DbSet<ClWarehouses> ClWarehouses { get; set; }
        public virtual DbSet<ClCommodity> ClCommodity { get; set; }
        public virtual DbSet<TblLocation> TblLocation { get; set; }
        public virtual DbSet<TblGrnsStatus> TblGrnsStatus { get; set; }
        public virtual DbSet<TblGrns> TblGrns { get; set; }
        public virtual DbSet<TblPungin> TblPungin { get; set; }
        public virtual DbSet<TblPickupNotices> TblPickupNotices { get; set; }
        public virtual DbSet<TblVoucher> TblVoucher { get; set; }
        public virtual DbSet<TblGins> TblGins { get; set; }
        public virtual DbSet<ClGinstatus> ClGinstatus { get; set; }
        public virtual DbSet<TblCommodityGrade> TblCommodityGrade { get; set; }
        public virtual DbSet<TblWashingStation> TblWashingStation { get; set; }
        public virtual DbSet<EcxExpiredSeller> EcxExpiredSeller { get; set; }
        public virtual DbSet<EcxArrivalToGRN> EcxArrivalToGRN { get; set; }
        public virtual DbSet<EcxDeliveryNoticeToGIN> EcxDeliveryNoticeToGIN { get; set; }
        public virtual DbSet<EcxServiceLevelAgreement> EcxServiceLevelAgreement { get; set; }
        public virtual DbSet<EcxExpiredPickupNoticesReport> EcxExpiredPickupNoticesReport { get; set; }
        public virtual DbSet<EcxInventoryPosition> EcxInventoryPosition { get; set; }
        public virtual DbSet<EcxWarehouse> EcxWarehouse { get; set; }
        public DbSet<TblWarehouse> TblWarehouse { get; set; }

        // lookup tables
        public virtual DbSet<Kebele> Kebele { get; set; }
        public virtual DbSet<Nationality> Nationality { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Woreda> Woreda { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }
        public virtual DbSet<EntityLayer.Lookup.LookupType> LookupType { get; set; }
        public virtual DbSet<MemberRepresentativeType> MemberRepresentativeType { get; set; }
        public virtual DbSet<CustomerBussinessFiled> CustomerBussinessFiled { get; set; }
        public virtual DbSet<CustomerType> CustomerType { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<MemberProduct> MemberProduct { get; set; }
        public virtual DbSet<ServicePrerequisite> ServicePrerequisite { get; set; }
        public virtual DbSet<Cancellation> Cancellation { get; set; }
        public virtual DbSet<Renewal> Renewal { get; set; }
        public virtual DbSet<Replacement> Replacement { get; set; }

        // Auto insert values


        //Trade Excution Client
        public virtual DbSet<MemberClientTrade> MemberClientTrade { get; set; }
        public virtual DbSet<MemberClientTradeDetail> MemberClientTradeDetail { get; set; }
        public virtual DbSet<MemberClientInformation> MemberClientInformation { get; set; }
        public virtual DbSet<TradeType> TradeType { get; set; }
        // public virtual DbSet<Commodity> Commodity { get; set; }
        public virtual DbSet<CommodityGrade> CommodityGrade { get; set; }
        public virtual DbSet<CommodityType> CommodityType { get; set; }
        public virtual DbSet<ReportType> ReportType { get; set; }
        public virtual DbSet<MemberTradeEvidence> MemberTradeEvidence { get; set; }
        public virtual DbSet<MemberTradeFinancial> MemberTradeFinancial { get; set; }
        public virtual DbSet<UnitMeasurement> UnitMeasurement { get; set; }
        public virtual DbSet<ReportPeriod> ReportPeriod { get; set; }
        public virtual DbSet<TradeExcutionStatusType> TradeExcutionStatusType { get; set; }
        public virtual DbSet<TradeExcutionViolationRecord> TradeExcutionViolationRecord { get; set; }
        public virtual DbSet<ExchangeActorFinanicial> ExchangeActorFinanicial { get; set; }
        public virtual DbSet<FinancialReportReminder> FinancialReportReminder { get; set; }
        public virtual DbSet<ClientInformationReminder> ClientInformationReminder { get; set; }
        public virtual DbSet<MemberTradeViolation> MemberTradeViolation { get; set; }
        public virtual DbSet<ClientProduct> ClientProduct { get; set; }
        public virtual DbSet<MemberFinancialAuditor> MemberFinancialAuditor { get; set; }
        public virtual DbSet<FinancialAuditoredFileUpload> FinancialAuditoredFileUpload { get; set; }
        public virtual DbSet<OffSiteMonitoringReport> OffSiteMonitoringReport { get; set; }
        public virtual DbSet<OffSiteMonitoringReportDetail> OffSiteMonitoringReportDetail { get; set; }
        public virtual DbSet<AnnualBudgetCloser> AnnualBudgetCloser { get; set; }
        //strart Settlement Bank
        public virtual DbSet<SvgBankUpload> SvgBankUpload { get; set; }
        public virtual DbSet<SvgSettlementAccount> SvgSettlementAccount { get; set; }
        public bool AutoSaveAuditFields = true;
        public virtual DbSet<BankUpload> BankUpload { get; set; }
        public virtual DbSet<BankTransaction> BankTransaction { get; set; }
        public virtual DbSet<Site> Site { get; set; }
        public virtual DbSet<BankUploadDTO> BankUploadDTO { get; set; }
        public virtual DbSet<SiteDTO> SiteDTO { get; set; }
        public virtual DbSet<BankOversightViolation> BankOversightViolation { get; set; }
        public virtual DbSet<BankOverSightViolationDetail> BankOverSightViolationDetail { get; set; }
        public object HttpContext { get; internal set; }
        public DbSet<MemberTradeExecutionViewModel> MemberTradeExecutionViewModel { get; set; }
        public DbSet<MemberFinancialAuditorViewModel> MemberFinancialAuditorViewModel { get; set; }
        public DbSet<BankTransactionTopTen> BankTransactionTopTen { get; set; }
        public DbSet<ViolationBankTransaction> ViolationBankTransaction { get; set; }
        public DbSet<TradeExecutionOrderViewModel> TradeExecutionOrderViewModel { get; set; }
        public DbSet<TradeExecutionAmountViewModel> TradeExecutionAmountViewModel { get; set; }
        public DbSet<OversightReportMOnthlySummaryModel> OversightReportMOnthlySummaryModel { get; set; }
        public DbSet<SettlementAccount> SettlementAccount { get; set; }
        public DbSet<MonthlyOnsiteFollowupSummary> MonthlyOnsiteFollowupSummary { get; set; }
        //End Settlement Bank
        //LawEnforcement
        public virtual DbSet<Case> Case { get; set; }
        public virtual DbSet<CaseApplication> CaseApplication { get; set; }
        public virtual DbSet<CaseDocument> CaseDocument { get; set; }
        public virtual DbSet<MemberAuditorAgrement> MemberAuditorAgrement { get; set; }
        public virtual DbSet<CaseFeedback> CaseFeedback { get; set; }
        public virtual DbSet<CaseInvestigation> CaseInvestigation { get; set; }
        public virtual DbSet<CaseLookUp> CaseLookUp { get; set; }
        public virtual DbSet<CaseLookUpType> CaseLookUpType { get; set; }
        public virtual DbSet<CaseParty> CaseParty { get; set; }
        public virtual DbSet<CasePartyTracking> CasePartyTracking { get; set; }
        public virtual DbSet<CasePartyInvestigation> CasePartyInvestigation { get; set; }
        public virtual DbSet<CaseStatusTransition> CaseStatusTransition { get; set; }
        public virtual DbSet<CaseStatusType> CaseStatusType { get; set; }
        public virtual DbSet<CaseTracking> CaseTracking { get; set; }
        public virtual DbSet<CaseAppointment> CaseAppointment { get; set; }
        public virtual DbSet<CaseDecisionTracking> CaseDecisionTracking { get; set; }
        public virtual DbSet<CAUnit> CAUnit { get; set; }
        public virtual DbSet<SampleRole> SampleRole { get; set; }
        public virtual DbSet<SampleUser> SampleUser { get; set; }
        public virtual DbSet<SampleUserRole> SampleUserRole { get; set; }

        // Data Change
        public virtual DbSet<RepresentativeChange> RepresentativeChange { get; set; }
        public virtual DbSet<DataChange> DataChange { get; set; }
        public virtual DbSet<OrganizationDataChange> OrganizationDataChange { get; set; }
        public virtual DbSet<ApplicationUser> ApplicationUser { get; set; }
        public virtual DbSet<ApplicationRole> ApplicationRole { get; set; }

        // work flow
        public virtual DbSet<WorkFlow> WorkFlow { get; set; }
        public virtual DbSet<CaseTribunalFeedBack> CaseTribunalFeedBack { get; set; }
        public DbSet<AuditDMV> AuditViewModel { get; set; }
        public DbSet<AuditAmendmentDMV> AuditAmendmentDMV { get; set; }
        public DbSet<LetterContent> LetterContent { get; set; }
        public DbSet<TableInformation> TableInformationModel { get; set; }
        public DbSet<NotRegistered> NotRegistered { get; set; }
        public DbSet<MembersTradeExecution> MembersTradeExecution { get; set; }
        public DbSet<NetWorthBetweenMemberAuditor> NetWorthBetweenMemberAuditor { get; set; }
        public DbSet<SisterCompanyViolation> SisterCompanyViolation { get; set; }
        public virtual DbSet<CasePartyDocument> CasePartyDocument { get; set; }
        public virtual DbSet<Owner> Owner { get; set; }
        public virtual DbSet<EXE> EXE { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public DbSet<TradeViewModel> TradeViewModels { get; set; }
        public virtual DbSet<EcexTradeDataForECEA> EcexTradeDataForECEA { get; set; }
        public virtual DbSet<SampleLeftOver> SampleLeftOver { get; set; }
        public virtual DbSet<ExcessProduct> ExcessProduct { get; set; }
        public virtual DbSet<ShortFall> ShortFall { get; set; }
        public DbSet<CaseAppealDocument> CaseAppealDocument { get; set; }
        public DbSet<CommodityLookupVM> CommodityLookupVM { get; set; }
        public DbSet<DailyPriceLimitVM> DailyPriceLimitVM { get; set; }
        public DbSet<OrderStatusVM> OrderStatusVM { get; set; }
        public DbSet<OrderValidityVM> OrderValidityVM { get; set; }
        public DbSet<SessionDataVM> SessionDataVM { get; set; }
        public DbSet<SessionStatusVM> SessionStatusVM { get; set; }
        public DbSet<SubSessionVM> SubSessionVM { get; set; }
        public DbSet<TradeDataVM> TradeDataVM { get; set; }
        public DbSet<TradeStatusVM> TradeStatusVM { get; set; }
        public DbSet<WarehouseVM> WarehouseVM { get; set; }
        public DbSet<WHRVM> WHRVM { get; set; }
        public DbSet<SubSessionStatusVM> SubSessionStatusVM { get; set; }
        public DbSet<PriceChangeVM> PriceChangeVM { get; set; }
        public DbSet<PreviousPriceVM> PreviousPriceVM { get; set; }
        public DbSet<AttendanceVM> AttendanceVM { get; set; }
        public DbSet<PriceRangeVM> PriceRangeVM { get; set; }
        public DbSet<PreopenSessionVM> PreopenSessionVM { get; set; }
        public DbSet<QuantityBeforeOpen> QuantityBeforeOpen { get; set; }
        public DbSet<PreopenSessionDate> PreopenSessionDate { get; set; }
        public DbSet<PrearrangedTradeVM> PrearrangedTradeVM { get; set; }
        public DbSet<DistinctOrderVM> DistinctOrderVM { get; set; }
        public DbSet<PreviousSessionVM> PreviousSessionVM { get; set; }
        public DbSet<MarkingTheCloseVM> MarkingTheCloseVM { get; set; }
        public DbSet<CommodityVM> CommodityVM { get; set; }
        public DbSet<WeightedAveragePriceVM> WeightedAveragePriceVM { get; set; }
        public DbSet<BidOfferRecordVM> BidOfferRecordVM { get; set; }
        public DbSet<CommonOrderAndTradeVM> CommonOrderAndTradeVM { get; set; }
        public DbSet<CommonTradeVM> CommonTradeVM { get; set; }
        public DbSet<ExchangeActorCode> ExchangeActorCode { get; set; }
        public DbSet<SisterCompanyMemberVM> SisterCompanyMemberVM { get; set; }
        public DbSet<TradeVM> TradeVM { get; set; }
        public DbSet<OrderMember> OrderMember { get; set; }
        public DbSet<ArrangedTrade> ArrangedTrade { get; set; }
        public DbSet<BacketingViolation> BacketingViolation { get; set; }
        public DbSet<PriceDiffernceBetweenWarehouses> PriceDiffernceBetweenWarehouses { get; set; }
        public DbSet<ComparingTradeReconciliation> ComparingTradeReconciliation { get; set; }
        public DbSet<FrontRunning> FrontRunning { get; set; }
        public DbSet<ReceiptHoldingSellOrder> ReceiptHoldingSellOrder { get; set; }
        public DbSet<WareHouse> WareHouse { get; set; }
        public DbSet<TradeVsSales> TradeVsSales { get; set; }
        public virtual DbSet<SettlementGuarnatteFund> SettlementGuarnatteFund { get; set; }
        public virtual DbSet<SettlementBankPerformance> SettlementBankPerformance { get; set; }
        public virtual DbSet<GuaranteeFundUpload> GuaranteeFundUpload { get; set; }
        public virtual DbSet<UploadBankPerformance> UploadBankPerformance { get; set; }
        public virtual DbSet<DelayRegisterPerBank> DelayRegisterPerBank { get; set; }
        public virtual DbSet<UploadDelayRegisterPerBank> UploadDelayRegisterPerBank { get; set; }
        public DbSet<ReceiptHoldingSellOderGraph> ReceiptHoldingSellOderGraph { get; set; }
        public DbSet<TblCommodity> TblCommodity { get; set; }
        public DbSet<SalesTradeCommodity> SalesTradeCommodity { get; set; }
        public DbSet<SuspendedExchange> SuspendedExchange { get; set; }
        public DbSet<RenewalInformation> RenewalInformation { get; set; }
        public DbSet<CancellationInformation> CancellationInformation { get; set; }
        public DbSet<ViolationRecord> ViolationRecord { get; set; }
        public DbSet<EcxTrade> EcxTrade { get; set; }
        public DbSet<EcxTradeVM> EcxTradeVM { get; set; }
        public DbSet<EcxOrder> EcxOrder { get; set; }
        public DbSet<EcxOrderVm> EcxOrderVm { get; set; }
        public DbSet<TradeReconcilationFigure> TradeReconcilationFigure { get; set; }
        public DbSet<AttendanceBuyerSellerRatio> AttendanceBuyerSellerRatio { get; set; }
        public DbSet<AttendanceVolumeValueRatio> AttendanceVolumeValueRatio { get; set; }
        public DbSet<TopTradeExecution> TopTradeExecution { get; set; }
        public DbSet<SettlementTransactionMonthly> SettlementTransactionMonthly { get; set; }
        public DbSet<SettlementTransactionDaily> SettlementTransactionDaily { get; set; }
        public DbSet<HHIIndex> HHIIndex { get; set; }
        public DbSet<TopLink> TopLink { get; set; }
        public DbSet<TradeSummary> TradeSummary { get; set; }
        public DbSet<WarehouseSummary> WarehouseSummary { get; set; }
        public DbSet<TopMarketShare> TopMarketShare { get; set; }
        public virtual DbSet<TradeTrend> TradeTrend { get; set; }
        public DbSet<PriceAndQuantitySummury> PriceAndQuantitySummury { get; set; }
        public DbSet<PriceAndQuantitySummuryByClass> PriceAndQuantitySummuryByClass { get; set; }
        public DbSet<PriceAndQuantitySummuryByCommodity> PriceAndQuantitySummuryByCommodity { get; set; }
        public DbSet<ContractMarketShare> ContractMarketShare { get; set; }
        public DbSet<TotalSalesForMarket> TotalSalesForMarket { get; set; }
        public DbSet<WarehouseMarketShare> WarehouseMarketShare { get; set; }
        public DbSet<MarketShareByCommodity> MarketShareByCommodity { get; set; }
        public DbSet<CommodityWarehouseMarketShare> CommodityWarehouseMarketShare { get; set; }
        public DbSet<ViolationRecordVm> ViolationRecordVm { get; set; }
        public DbSet<TradeSummaryByCommodity> TradeSummaryByCommodity { get; set; }
        public DbSet<BuyerSellerRatio> BuyerSellerRatio { get; set; }
        public DbSet<FlashCrash> FlashCrash { get; set; }
        public DbSet<ViolationInvestigation> ViolationInvestigation { get; set; }
        public DbSet<ViolationRecordCount> ViolationRecordCount { get; set; }
        public DbSet<TradeExecution> TradeExecution { get; set; }
        public DbSet<TradeExecutionViewModel> TradeExecutionViewModel { get; set; }
        public DbSet<TradeAttendanceBuyerSellerVolumeValueRatio> TradeAttendanceBuyerSellerVolumeValueRatio { get; set; }
        public DbSet<ComparisonTradeByCommodityType> ComparisonTradeByCommodityType { get; set; }
        //Penality
        public DbSet<Penalty> Penalty { get; set; }
        public DbSet<TradeComparisonByCommodityGrade> TradeComparisonByCommodityGrade { get; set; }
        public DbSet<BlackList> BlackList { get; set; }

        //public DbQuery<TradeSummaryByCommodity> TradeSummaryByCommodity { get; set; }
        //public DbQuery<BuyerSellerRatio> BuyerSellerRatio { get; set; }
        public DbSet<TradeCancellation> TradeCancellation { get; set; }
        public DbSet<TradeCancellationFigure> TradeCancellationFigure { get; set; }
        public DbSet<CommodityGradeLookUp> CommodityGradeLookUp { get; set; }
        public DbSet<PriceDifferenceAmongDifferentGrade> PriceDifferenceAmongDifferentGrade { get; set; }
        public DbSet<EcxAuction> EcxAuction { get; set; }
        public DbSet<Auction> Auction { get; set; }
        public DbSet<AuctionVM> AuctionVM { get; set; }
        public DbSet<AuctionViolationRecord> AuctionViolationRecord { get; set; }
        public DbSet<TradeV‭iolationRecord> TradeV‭iolationRecord { get; set; }
        public DbSet<OpenedSession> OpenedSession { get; set; }
        public DbSet<OpenningClosingPrice> OpenningClosingPrice { get; set; }
        public DbSet<TradeAttendanceBuyerSeller> TradeAttendanceBuyerSeller { get; set; }
        public DbSet<OrderAttendanceSellerBuyer> OrderAttendanceSellerBuyer { get; set; }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddCommonFiledValue();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            AddCommonFiledValue();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddCommonFiledValue()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Added || x.State == EntityState.Modified);

            // Same value at initial creation
            var createdDateTime = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        ((BaseEntity)entity.Entity).IsActive = true;
                        ((BaseEntity)entity.Entity).IsDeleted = false;
                        ((BaseEntity)entity.Entity).CreatedDateTime = createdDateTime;
                        ((BaseEntity)entity.Entity).UpdatedDateTime = createdDateTime;
                        break;
                    case EntityState.Modified:
                        //  ((BaseEntity)entity.Entity).UpdatedDateTime =createdDateTime;
                        break;
                    case EntityState.Deleted:
                        entity.State = EntityState.Modified;
                        ((BaseEntity)entity.Entity).IsDeleted = true;
                        break;
                }

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");
            modelBuilder.Entity<ClCommodity>(entity =>
            {
                entity.ToTable("clCommodity");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StandardUnitOfMeasureId).HasColumnName("StandardUnitOfMeasureID");
            });
            modelBuilder.Entity<ClGinstatus>(entity =>
            {
                entity.HasKey(e => e.EnumValue);

                entity.ToTable("clGINStatus");

                entity.Property(e => e.EnumValue).ValueGeneratedNever();

                entity.Property(e => e.EnumName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.GinstatusId).HasColumnName("GINStatusID");
            });
            modelBuilder.Entity<TblVoucher>(entity =>
            {
                entity.HasKey(e => e.VoucherId);

                entity.ToTable("tblVoucher");

                entity.Property(e => e.VoucherId).ValueGeneratedNever();

                entity.Property(e => e.CertificateNo).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SpecificArea).HasMaxLength(50);

                entity.Property(e => e.VoucherNo)
                    .IsRequired()
                    .HasMaxLength(50);
            });
            modelBuilder.Entity<TblPickupNotices>(entity =>
            {
                entity.ToTable("tblPickupNotices");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AgentIdnumber)
                    .HasColumnName("AgentIDNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.AgentName).HasMaxLength(150);

                entity.Property(e => e.AgentTel)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientIdno)
                    .HasColumnName("ClientIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CommodityGradeId).HasColumnName("CommodityGradeID");

                entity.Property(e => e.CommodityName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConsignmentType).HasMaxLength(50);

                entity.Property(e => e.CreatedTimestamp).HasColumnType("datetime");

                entity.Property(e => e.Cupvalue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpectedPickupDateTime).HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate).HasColumnType("datetime");

                entity.Property(e => e.Grnno)
                    .HasColumnName("GRNNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Idtype)
                    .HasColumnName("IDType")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdtypeId).HasColumnName("IDTypeID");

                entity.Property(e => e.LastModifiedTimestamp).HasColumnType("datetime");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.MemberIdno)
                    .HasColumnName("MemberIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MemberName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlateNumber).HasMaxLength(50);

                entity.Property(e => e.PunprintDateTime)
                    .HasColumnName("PUNPrintDateTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Punprinted).HasColumnName("PUNPrinted");

                entity.Property(e => e.PunprintedBy).HasColumnName("PUNPrintedBy");

                entity.Property(e => e.RawValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RepId).HasColumnName("RepID");

                entity.Property(e => e.RepIdno)
                    .HasColumnName("RepIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RepName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RowVersionStamp).HasColumnType("datetime");

                entity.Property(e => e.SellerName).HasMaxLength(100);

                entity.Property(e => e.Shade).HasMaxLength(50);

                entity.Property(e => e.ShedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SustainableCertification).HasMaxLength(100);

                entity.Property(e => e.TotalValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrailerPlateNumber).HasMaxLength(50);

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WarehouseName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseReceiptId).HasColumnName("WarehouseReceiptID");

                entity.Property(e => e.WashingMillingStation).HasMaxLength(100);

                entity.Property(e => e.WeightInKg).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Woreda).HasMaxLength(100);
            });
            modelBuilder.Entity<TblGins>(entity =>
            {
                entity.ToTable("tblGINS");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdjustmentTypeId).HasColumnName("AdjustmentTypeID");

                entity.Property(e => e.ClientSignedDate).HasColumnType("datetime");

                entity.Property(e => e.ClientSignedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.DailyLabourersAssociationId).HasColumnName("DailyLabourersAssociationID");

                entity.Property(e => e.DateIssued).HasColumnType("datetime");

                entity.Property(e => e.DateTimeLoaded).HasColumnType("datetime");

                entity.Property(e => e.DriverName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ginnumber)
                    .HasColumnName("GINNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GinstatusId).HasColumnName("GINStatusID");

                entity.Property(e => e.InventoryControlId).HasColumnName("InventoryControlID");

                entity.Property(e => e.IsPsa).HasColumnName("IsPSA");

                entity.Property(e => e.LeadInventoryController)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LeadInventoryControllerId).HasColumnName("LeadInventoryControllerID");

                entity.Property(e => e.LicenseIssuedBy)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LicenseNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LicsignedDate)
                    .HasColumnName("LICSignedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.LicsignedName)
                    .HasColumnName("LICSignedName")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LoadUnloadTicketNo)
                    .HasColumnName("LoadUnloadTicketNO")
                    .HasMaxLength(50);

                entity.Property(e => e.ManagerSignedDate).HasColumnType("datetime");

                entity.Property(e => e.ManagerSignedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PlateNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Psaremark).HasColumnName("PSARemark");

                entity.Property(e => e.PsastackId).HasColumnName("PSAStackID");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.RowVersionStamp).HasColumnType("datetime");

                entity.Property(e => e.ScaleTicketNumber).HasMaxLength(50);

                entity.Property(e => e.ShedId).HasColumnName("ShedID");

                entity.Property(e => e.TrailerPlateNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TruckRegisterTime).HasColumnType("datetime");

                entity.Property(e => e.TruckRequestTime).HasColumnType("datetime");

                entity.Property(e => e.UpdatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WbserviceProviderId).HasColumnName("WBServiceProviderID");
            });
            modelBuilder.Entity<ExchangeActor>().ToTable("ExchangeActor");
            modelBuilder.Entity<OwnerManager>().ToTable("OwnerManager");
            modelBuilder.Entity<Prerequisite>().ToTable("Prerequisite");
            modelBuilder.Entity<CustomerPrerequisite>().ToTable("CustomerPrerequisite");
            modelBuilder.Entity<ServiceApplication>().ToTable("ServiceApplication");
            modelBuilder.Entity<ServiceTariff>().ToTable("ServiceTariff");
            modelBuilder.Entity<GeneralSetting>().ToTable("GeneralSetting");
            modelBuilder.Entity<ApplicationUser>().HasMany(u => u.Claims).WithOne().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ApplicationUser>().HasMany(u => u.IdentityUserRole).WithOne().HasForeignKey(r => r.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ApplicationRole>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(r => r.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TblArrivals>(entity =>
            {
                entity.ToTable("tblArrivals");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientName).HasMaxLength(200);

                entity.Property(e => e.CommodityId).HasColumnName("CommodityID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateTimeReceived).HasColumnType("datetime");

                entity.Property(e => e.DriverName).HasMaxLength(50);

                entity.Property(e => e.GrossWeight).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LicenseIssuedPlace).HasMaxLength(50);

                entity.Property(e => e.LicenseNumber).HasMaxLength(50);

                entity.Property(e => e.ProcessingCenter).HasMaxLength(250);

                entity.Property(e => e.Remark).HasColumnType("text");

                entity.Property(e => e.Sampletype)
                    .HasColumnName("sampletype")
                    .HasMaxLength(50);

                entity.Property(e => e.ScaleTicketNo).HasMaxLength(50);

                entity.Property(e => e.SpecificArea).HasMaxLength(100);

                entity.Property(e => e.TrackingNumber).HasMaxLength(50);

                entity.Property(e => e.TrailerPlateNumber).HasMaxLength(50);

                entity.Property(e => e.TruckPlateNumber).HasMaxLength(50);

                entity.Property(e => e.VehicleSize).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VoucherCertificateNo).HasMaxLength(50);

                entity.Property(e => e.VoucherCommodityTypeId).HasColumnName("VoucherCommodityTypeID");

                entity.Property(e => e.VoucherNumber).HasMaxLength(50);

                entity.Property(e => e.VoucherWeight).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WoredaId).HasColumnName("WoredaID");
            });
            modelBuilder.Entity<ArrangedTrade>().HasNoKey();
            modelBuilder.Entity<MemberTradeExecutionViewModel>().HasNoKey();
            modelBuilder.Entity<MemberFinancialAuditorViewModel>().HasNoKey();
            modelBuilder.Entity<BankTransactionTopTen>().HasNoKey();
            modelBuilder.Entity<AuctionVM>().HasNoKey();
            modelBuilder.Entity<Auction>().HasNoKey();
            modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
            modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
            modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
            modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
            modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
            modelBuilder.Entity<OrderStatusVM>().HasNoKey();
            modelBuilder.Entity<OrderValidityVM>().HasNoKey();
            modelBuilder.Entity<SessionDataVM>().HasNoKey();
            modelBuilder.Entity<SessionStatusVM>().HasNoKey();
            modelBuilder.Entity<OpenningClosingPrice>().HasNoKey();
            modelBuilder.Entity<OpenedSession>().HasNoKey();
            modelBuilder.Entity<OrderAttendanceSellerBuyer>().HasNoKey();
            modelBuilder.Entity<TradeAttendanceBuyerSeller>().HasNoKey();
            modelBuilder.Entity<SubSessionVM>().HasNoKey();
            modelBuilder.Entity<TradeDataVM>().HasNoKey();
            modelBuilder.Entity<TradeStatusVM>().HasNoKey();
            modelBuilder.Entity<AuditDMV>().HasNoKey();
            modelBuilder.Entity<AuditAmendmentDMV>().HasNoKey();
            modelBuilder.Entity<LetterContent>().HasNoKey();
            modelBuilder.Entity<TableInformation>().HasNoKey();
            modelBuilder.Entity<NotRegistered>().HasNoKey();
            modelBuilder.Entity<MembersTradeExecution>().HasNoKey();
            modelBuilder.Entity<NetWorthBetweenMemberAuditor>().HasNoKey();
            modelBuilder.Entity<SisterCompanyViolation>().HasNoKey();
            modelBuilder.Entity<TradeViewModel>().HasNoKey();
            modelBuilder.Entity<FlashCrash>().HasNoKey();
            modelBuilder.Entity<ViolationRecordCount>().HasNoKey();
            modelBuilder.Entity<TradeExecutionViewModel>().HasNoKey();
            modelBuilder.Entity<TradeAttendanceBuyerSellerVolumeValueRatio>().HasNoKey();
            modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
            modelBuilder.Entity<TradeComparisonByCommodityGrade>().HasNoKey();
            modelBuilder.Entity<TradeCancellation>().HasNoKey();
            modelBuilder.Entity<TradeCancellationFigure>().HasNoKey();
            modelBuilder.Entity<CommodityGradeLookUp>().HasNoKey();
            modelBuilder.Entity<PriceDifferenceAmongDifferentGrade>().HasNoKey();
            modelBuilder.Entity<MemberTradeExecutionViewModel>().HasNoKey();
            modelBuilder.Entity<MemberFinancialAuditorViewModel>().HasNoKey();
            modelBuilder.Entity<BankTransactionTopTen>().HasNoKey();
            modelBuilder.Entity<AuctionVM>().HasNoKey();
            modelBuilder.Entity<Auction>().HasNoKey();
            //modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
            modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
            modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
            modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
            modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
            modelBuilder.Entity<OrderStatusVM>().HasNoKey();
            modelBuilder.Entity<OrderValidityVM>().HasNoKey();
            modelBuilder.Entity<SessionDataVM>().HasNoKey();
            modelBuilder.Entity<SessionStatusVM>().HasNoKey();
            modelBuilder.Entity<SubSessionVM>().HasNoKey();
            modelBuilder.Entity<TradeDataVM>().HasNoKey();
            modelBuilder.Entity<TradeStatusVM>().HasNoKey();
            modelBuilder.Entity<WarehouseVM>().HasNoKey();
            modelBuilder.Entity<WHRVM>().HasNoKey();
            modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
            modelBuilder.Entity<PriceChangeVM>().HasNoKey();
            modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
            modelBuilder.Entity<AttendanceVM>().HasNoKey();
            modelBuilder.Entity<PriceRangeVM>().HasNoKey();
            modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
            modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
            modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
            modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
            modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
            modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
            modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
            modelBuilder.Entity<CommodityVM>().HasNoKey();
            modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
            modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
            modelBuilder.Entity<CommonTradeVM>().HasNoKey();
            modelBuilder.Entity<ExchangeActorCode>().HasNoKey();
            modelBuilder.Entity<SisterCompanyMemberVM>().HasNoKey();
            modelBuilder.Entity<TradeVM>().HasNoKey();
            modelBuilder.Entity<OrderMember>().HasNoKey();
            modelBuilder.Entity<BacketingViolation>().HasNoKey();
            modelBuilder.Entity<PriceDiffernceBetweenWarehouses>().HasNoKey();
            modelBuilder.Entity<ComparingTradeReconciliation>().HasNoKey();
            modelBuilder.Entity<FrontRunning>().HasNoKey();
            modelBuilder.Entity<WareHouse>().HasNoKey();
            modelBuilder.Entity<TradeVsSales>().HasNoKey();
            modelBuilder.Entity<ReceiptHoldingSellOderGraph>().HasNoKey();
            modelBuilder.Entity<TblCommodity>().HasNoKey();
            modelBuilder.Entity<SalesTradeCommodity>().HasNoKey();
            modelBuilder.Entity<SuspendedExchange>().HasNoKey();
            modelBuilder.Entity<RenewalInformation>().HasNoKey();
            modelBuilder.Entity<CancellationInformation>().HasNoKey();
            modelBuilder.Entity<EcxTradeVM>().HasNoKey();
            modelBuilder.Entity<EcxOrderVm>().HasNoKey();
            modelBuilder.Entity<WarehouseVM>().HasNoKey();
            modelBuilder.Entity<WHRVM>().HasNoKey();
            modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
            modelBuilder.Entity<PriceChangeVM>().HasNoKey();
            modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
            modelBuilder.Entity<AttendanceVM>().HasNoKey();
            modelBuilder.Entity<PriceRangeVM>().HasNoKey();
            modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
            modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
            modelBuilder.Entity<TradeExecutionAmountViewModel>().HasNoKey();
            modelBuilder.Entity<ReceiptHoldingSellOrder>().HasNoKey();
            modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
            modelBuilder.Entity<TradeExecutionOrderViewModel>().HasNoKey();
            modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
            modelBuilder.Entity<CommodityWarehouseMarketShare>().HasNoKey();
            modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
            modelBuilder.Entity<ViolationBankTransaction>().HasNoKey();
            modelBuilder.Entity<CommonTradeVM>().HasNoKey();
            modelBuilder.Entity<EcxOrderVm>().HasNoKey();
            modelBuilder.Entity<TradeReconcilationFigure>().HasNoKey();
            modelBuilder.Entity<AttendanceBuyerSellerRatio>().HasNoKey();
            modelBuilder.Entity<AttendanceVolumeValueRatio>().HasNoKey();
            modelBuilder.Entity<TopTradeExecution>().HasNoKey();
            modelBuilder.Entity<SettlementTransactionMonthly>().HasNoKey();
            modelBuilder.Entity<SettlementTransactionDaily>().HasNoKey();
            modelBuilder.Entity<HHIIndex>().HasNoKey();
            modelBuilder.Entity<TopLink>().HasNoKey();
            modelBuilder.Entity<TradeSummary>().HasNoKey();
            modelBuilder.Entity<WarehouseSummary>().HasNoKey();
            modelBuilder.Entity<TopMarketShare>().HasNoKey();
            modelBuilder.Entity<PriceAndQuantitySummury>().HasNoKey();
            modelBuilder.Entity<PriceAndQuantitySummuryByClass>().HasNoKey();
            modelBuilder.Entity<PriceAndQuantitySummuryByCommodity>().HasNoKey();
            modelBuilder.Entity<ContractMarketShare>().HasNoKey();
            modelBuilder.Entity<TotalSalesForMarket>().HasNoKey();
            modelBuilder.Entity<WarehouseMarketShare>().HasNoKey();
            modelBuilder.Entity<MarketShareByCommodity>().HasNoKey();
            modelBuilder.Entity<ViolationRecordVm>().HasNoKey();
            modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
            modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
            modelBuilder.Entity<AuctionVM>().HasNoKey();
            modelBuilder.Entity<Auction>().HasNoKey();
            //modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
            modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
            modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
            modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
            modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
            modelBuilder.Entity<OrderStatusVM>().HasNoKey();
            modelBuilder.Entity<OrderValidityVM>().HasNoKey();
            modelBuilder.Entity<SessionDataVM>().HasNoKey();
            modelBuilder.Entity<SessionStatusVM>().HasNoKey();
            modelBuilder.Entity<SubSessionVM>().HasNoKey();
            modelBuilder.Entity<TradeDataVM>().HasNoKey();
            modelBuilder.Entity<TradeStatusVM>().HasNoKey();
            modelBuilder.Entity<WarehouseVM>().HasNoKey();
            modelBuilder.Entity<WHRVM>().HasNoKey();
            modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
            modelBuilder.Entity<PriceChangeVM>().HasNoKey();
            modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
            modelBuilder.Entity<AttendanceVM>().HasNoKey();
            modelBuilder.Entity<PriceRangeVM>().HasNoKey();
            modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
            modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
            modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
            modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
            modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
            modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
            modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
            modelBuilder.Entity<CommodityVM>().HasNoKey();
            modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
            modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
            modelBuilder.Entity<CommonTradeVM>().HasNoKey();
            modelBuilder.Entity<ExchangeActorCode>().HasNoKey();
            modelBuilder.Entity<SisterCompanyMemberVM>().HasNoKey();
            modelBuilder.Entity<TradeVM>().HasNoKey();
            modelBuilder.Entity<OrderMember>().HasNoKey();
            modelBuilder.Entity<BacketingViolation>().HasNoKey();
            modelBuilder.Entity<PriceDiffernceBetweenWarehouses>().HasNoKey();
            modelBuilder.Entity<ComparingTradeReconciliation>().HasNoKey();
            modelBuilder.Entity<FrontRunning>().HasNoKey();
            modelBuilder.Entity<WareHouse>().HasNoKey();
            modelBuilder.Entity<TradeVsSales>().HasNoKey();
            modelBuilder.Entity<ReceiptHoldingSellOderGraph>().HasNoKey();
            modelBuilder.Entity<TblCommodity>().HasNoKey();
            modelBuilder.Entity<SalesTradeCommodity>().HasNoKey();
            modelBuilder.Entity<SuspendedExchange>().HasNoKey();
            modelBuilder.Entity<RenewalInformation>().HasNoKey();
            modelBuilder.Entity<EcxTradeVM>().HasNoKey();
            modelBuilder.Entity<EcxOrderVm>().HasNoKey();
            modelBuilder.Entity<WarehouseVM>().HasNoKey();
            modelBuilder.Entity<WHRVM>().HasNoKey();
            modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
            modelBuilder.Entity<PriceChangeVM>().HasNoKey();
            modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
            modelBuilder.Entity<AttendanceVM>().HasNoKey();
            modelBuilder.Entity<PriceRangeVM>().HasNoKey();
            modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
            modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
            modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
            modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
            modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
            modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
            modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
            modelBuilder.Entity<CommodityVM>().HasNoKey();
            modelBuilder.Entity<OversightReportMOnthlySummaryModel>().HasNoKey();
            modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
            modelBuilder.Entity<CommonTradeVM>().HasNoKey();
            modelBuilder.Entity<CommodityLookupVM>().HasNoKey();
            base.OnModelCreating(modelBuilder);



        }
    }
}
