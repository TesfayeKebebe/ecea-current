using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CUSTOR.ICERS.Core
{
    public class ECXLookupDbContext: DbContext
    {
        public ECXLookupDbContext(DbContextOptions<ECXLookupDbContext> options)
          : base(options)
        {


        }
        public virtual DbSet<TblLocation> TblLocation { get; set; }
        public virtual DbSet<TblWarehouse> TblWarehouse { get; set; }
        public virtual DbSet<TblZone> TblZone { get; set; }
        public virtual DbSet<TblWoreda> TblWoreda { get; set; }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddCommonFiledValue();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            AddCommonFiledValue();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddCommonFiledValue()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Added || x.State == EntityState.Modified);

            // Same value at initial creation
            var createdDateTime = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        ((BaseEntity)entity.Entity).IsActive = true;
                        ((BaseEntity)entity.Entity).IsDeleted = false;
                        ((BaseEntity)entity.Entity).CreatedDateTime = createdDateTime;
                        ((BaseEntity)entity.Entity).UpdatedDateTime = createdDateTime;
                        break;
                    case EntityState.Modified:
                        //  ((BaseEntity)entity.Entity).UpdatedDateTime =createdDateTime;
                        break;
                    case EntityState.Deleted:
                        entity.State = EntityState.Modified;
                        ((BaseEntity)entity.Entity).IsDeleted = true;
                        break;
                }

            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");
            modelBuilder.Entity<TblLocation>(entity =>
            {
                entity.ToTable("tblLocation");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });
            modelBuilder.Entity<TblWarehouse>(entity =>
            {
                entity.ToTable("tblWarehouse");

                entity.Property(e => e.BuyHandlingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Code).HasMaxLength(3);

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.LocationDifferential).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SellHandlingCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ShortName).HasMaxLength(3);

                entity.Property(e => e.StorageCharge).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StorageChargeAfterNdays).HasColumnName("StorageChargeAfterNDays");

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });
            modelBuilder.Entity<TblWoreda>(entity =>
            {
                entity.ToTable("tblWoreda");

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });
            modelBuilder.Entity<TblZone>(entity =>
            {
                entity.ToTable("tblZone");

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });

            base.OnModelCreating(modelBuilder);



        }

    }
}
