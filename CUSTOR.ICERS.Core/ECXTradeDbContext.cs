using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.ViewModel;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using CUSTOR.ICERS.Report.ViewModel.TradeExcution;
using Microsoft.EntityFrameworkCore;

namespace CUSTOR.ICERS.Core
{
  public class ECXTradeDbContext : DbContext
  {
    public ECXTradeDbContext(DbContextOptions<ECXTradeDbContext> options)
       : base(options)
    {

    }
    public DbSet<CommodityLookupVM> CommodityLookupVM { get; set; }
    public DbSet<DailyPriceLimitVM> DailyPriceLimitVM { get; set; }
    public DbSet<OrderStatusVM> OrderStatusVM { get; set; }
    public DbSet<OrderValidityVM> OrderValidityVM { get; set; }
    public DbSet<SessionDataVM> SessionDataVM { get; set; }
    public DbSet<SessionStatusVM> SessionStatusVM { get; set; }
    public DbSet<SubSessionVM> SubSessionVM { get; set; }
    public DbSet<TradeDataVM> TradeDataVM { get; set; }
    public DbSet<TradeStatusVM> TradeStatusVM { get; set; }
    public DbSet<WarehouseVM> WarehouseVM { get; set; }
    public DbSet<WHRVM> WHRVM { get; set; }
    public DbSet<SubSessionStatusVM> SubSessionStatusVM { get; set; }
    public DbSet<PriceChangeVM> PriceChangeVM { get; set; }
    public DbSet<PreviousPriceVM> PreviousPriceVM { get; set; }
    public DbSet<AttendanceVM> AttendanceVM { get; set; }
    public DbSet<PriceRangeVM> PriceRangeVM { get; set; }
    public DbSet<PreopenSessionVM> PreopenSessionVM { get; set; }
    public DbSet<QuantityBeforeOpen> QuantityBeforeOpen { get; set; }
    public DbSet<PreopenSessionDate> PreopenSessionDate { get; set; }
    public DbSet<PrearrangedTradeVM> PrearrangedTradeVM { get; set; }
    public DbSet<DistinctOrderVM> DistinctOrderVM { get; set; }
    public DbSet<PreviousSessionVM> PreviousSessionVM { get; set; }
    public DbSet<MarkingTheCloseVM> MarkingTheCloseVM { get; set; }
    public DbSet<CommodityVM> CommodityVM { get; set; }
    public DbSet<WeightedAveragePriceVM> WeightedAveragePriceVM { get; set; }
    public DbSet<BidOfferRecordVM> BidOfferRecordVM { get; set; }
    public DbSet<CommonOrderAndTradeVM> CommonOrderAndTradeVM { get; set; }
    public DbSet<CommonTradeVM> CommonTradeVM { get; set; }
    public DbSet<ExchangeActorCode> ExchangeActorCode { get; set; }
    public DbSet<SisterCompanyMemberVM> SisterCompanyMemberVM { get; set; }
    public DbSet<TradeVM> TradeVM { get; set; }
    public DbSet<OrderMember> OrderMember { get; set; }
    public DbSet<ArrangedTrade> ArrangedTrade { get; set; }
    public DbSet<BacketingViolation> BacketingViolation { get; set; }
    public DbSet<PriceDiffernceBetweenWarehouses> PriceDiffernceBetweenWarehouses { get; set; }
    public DbSet<ComparingTradeReconciliation> ComparingTradeReconciliation { get; set; }
    public DbSet<FrontRunning> FrontRunning { get; set; }
    public DbSet<ReceiptHoldingSellOrder> ReceiptHoldingSellOrder { get; set; }
    public DbSet<WareHouse> WareHouse { get; set; }
    public DbSet<TradeVsSales> TradeVsSales { get; set; }
    public DbSet<ReceiptHoldingSellOderGraph> ReceiptHoldingSellOderGraph { get; set; }
    public DbSet<TblCommodity> TblCommodity { get; set; }
    public DbSet<SalesTradeCommodity> SalesTradeCommodity { get; set; }
    public DbSet<SuspendedExchange> SuspendedExchange { get; set; }
    public DbSet<RenewalInformation> RenewalInformation { get; set; }
    public DbSet<CancellationInformation> CancellationInformation { get; set; }
    public DbSet<EcxTradeVM> EcxTradeVM { get; set; }
    public DbSet<EcxOrderVm> EcxOrderVm { get; set; }
    public DbSet<TradeReconcilationFigure> TradeReconcilationFigure { get; set; }
    public DbSet<AttendanceBuyerSellerRatio> AttendanceBuyerSellerRatio { get; set; }
    public DbSet<AttendanceVolumeValueRatio> AttendanceVolumeValueRatio { get; set; }
    public DbSet<TopTradeExecution> TopTradeExecution { get; set; }
    public DbSet<SettlementTransactionMonthly> SettlementTransactionMonthly { get; set; }
    public DbSet<SettlementTransactionDaily> SettlementTransactionDaily { get; set; }
    public DbSet<HHIIndex> HHIIndex { get; set; }
    public DbSet<TopLink> TopLink { get; set; }
    public DbSet<TradeSummary> TradeSummary { get; set; }
    public DbSet<WarehouseSummary> WarehouseSummary { get; set; }
    public DbSet<TopMarketShare> TopMarketShare { get; set; }
    public DbSet<PriceAndQuantitySummury> PriceAndQuantitySummury { get; set; }
    public DbSet<PriceAndQuantitySummuryByClass> PriceAndQuantitySummuryByClass { get; set; }
    public DbSet<PriceAndQuantitySummuryByCommodity> PriceAndQuantitySummuryByCommodity { get; set; }
    public DbSet<ContractMarketShare> ContractMarketShare { get; set; }
    public DbSet<TotalSalesForMarket> TotalSalesForMarket { get; set; }
    public DbSet<WarehouseMarketShare> WarehouseMarketShare { get; set; }
    public DbSet<MarketShareByCommodity> MarketShareByCommodity { get; set; }
    public DbSet<CommodityWarehouseMarketShare> CommodityWarehouseMarketShare { get; set; }
    public DbSet<ViolationRecordVm> ViolationRecordVm { get; set; }
    public DbSet<TradeSummaryByCommodity> TradeSummaryByCommodity { get; set; }
    public DbSet<BuyerSellerRatio> BuyerSellerRatio { get; set; }
    public DbSet<TradeExecution> TradeExecution { get; set; }
    public DbSet<TradeExecutionViewModel> TradeExecutionViewModel { get; set; }
    public DbSet<TradeAttendanceValueVolumeRatio> TradeAttendanceValueVolumeRatio { get; set; }
    public DbSet<ComparisonTradeByCommodityType> ComparisonTradeByCommodityType { get; set; }
    public DbSet<TradeComparisonByCommodityGrade> TradeComparisonByCommodityGrade { get; set; }
    public DbSet<TradeCancellation> TradeCancellation { get; set; }
    public DbSet<TradeCancellationFigure> TradeCancellationFigure { get; set; }
    public DbSet<CommodityGradeLookUp> CommodityGradeLookUp { get; set; }
    public DbSet<PriceDifferenceAmongDifferentGrade> PriceDifferenceAmongDifferentGrade { get; set; }
    public DbSet<EcxAuction> EcxAuction { get; set; }
    public DbSet<Auction> Auction { get; set; }
    public DbSet<AuctionVM> AuctionVM { get; set; }
    public DbSet<TradeAttendanceBuyerSellerVolumeValueRatio> TradeAttendanceBuyerSellerVolumeValueRatio { get; set; }
    public DbSet<OpenedSession> OpenedSession { get; set; }
    public DbSet<OpenningClosingPrice> OpenningClosingPrice { get; set; }
    public DbSet<TradeAttendanceBuyerSeller> TradeAttendanceBuyerSeller { get; set; }
    public DbSet<OrderAttendanceSellerBuyer> OrderAttendanceSellerBuyer { get; set; }
    public DbSet<MembersTradeExecution> MembersTradeExecution { get; set; }
    public DbSet<CommodityPriceLimit> CommodityPriceLimit { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

      modelBuilder.Entity<ArrangedTrade>().HasNoKey();
      modelBuilder.Entity<MemberTradeExecutionViewModel>().HasNoKey();
      modelBuilder.Entity<MemberFinancialAuditorViewModel>().HasNoKey();
      modelBuilder.Entity<BankTransactionTopTen>().HasNoKey();
      modelBuilder.Entity<AuctionVM>().HasNoKey();
      modelBuilder.Entity<Auction>().HasNoKey();
      modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
      modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
      modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
      modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
      modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
      modelBuilder.Entity<OrderStatusVM>().HasNoKey();
      modelBuilder.Entity<OrderValidityVM>().HasNoKey();
      modelBuilder.Entity<SessionDataVM>().HasNoKey();
      modelBuilder.Entity<SessionStatusVM>().HasNoKey();
      modelBuilder.Entity<OpenningClosingPrice>().HasNoKey();
      modelBuilder.Entity<OpenedSession>().HasNoKey();
      modelBuilder.Entity<OrderAttendanceSellerBuyer>().HasNoKey();
      modelBuilder.Entity<TradeAttendanceBuyerSeller>().HasNoKey();
      modelBuilder.Entity<SubSessionVM>().HasNoKey();
      modelBuilder.Entity<TradeDataVM>().HasNoKey();
      modelBuilder.Entity<TradeStatusVM>().HasNoKey();
      modelBuilder.Entity<AuditDMV>().HasNoKey();
      modelBuilder.Entity<AuditAmendmentDMV>().HasNoKey();
      modelBuilder.Entity<LetterContent>().HasNoKey();
      modelBuilder.Entity<TableInformation>().HasNoKey();
      modelBuilder.Entity<NotRegistered>().HasNoKey();
      modelBuilder.Entity<MembersTradeExecution>().HasNoKey();
      modelBuilder.Entity<NetWorthBetweenMemberAuditor>().HasNoKey();
      modelBuilder.Entity<SisterCompanyViolation>().HasNoKey();
      modelBuilder.Entity<TradeViewModel>().HasNoKey();
      modelBuilder.Entity<FlashCrash>().HasNoKey();
      modelBuilder.Entity<ViolationRecordCount>().HasNoKey();
      modelBuilder.Entity<TradeExecutionViewModel>().HasNoKey();
      modelBuilder.Entity<TradeAttendanceBuyerSellerVolumeValueRatio>().HasNoKey();
      modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
      modelBuilder.Entity<TradeComparisonByCommodityGrade>().HasNoKey();
      modelBuilder.Entity<TradeCancellation>().HasNoKey();
      modelBuilder.Entity<TradeCancellationFigure>().HasNoKey();
      modelBuilder.Entity<CommodityGradeLookUp>().HasNoKey();
      modelBuilder.Entity<PriceDifferenceAmongDifferentGrade>().HasNoKey();
      modelBuilder.Entity<MemberTradeExecutionViewModel>().HasNoKey();
      modelBuilder.Entity<MemberFinancialAuditorViewModel>().HasNoKey();
      modelBuilder.Entity<BankTransactionTopTen>().HasNoKey();
      modelBuilder.Entity<AuctionVM>().HasNoKey();
      modelBuilder.Entity<Auction>().HasNoKey();
      //modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
      modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
      modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
      modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
      modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
      modelBuilder.Entity<OrderStatusVM>().HasNoKey();
      modelBuilder.Entity<OrderValidityVM>().HasNoKey();
      modelBuilder.Entity<SessionDataVM>().HasNoKey();
      modelBuilder.Entity<SessionStatusVM>().HasNoKey();
      modelBuilder.Entity<SubSessionVM>().HasNoKey();
      modelBuilder.Entity<TradeDataVM>().HasNoKey();
      modelBuilder.Entity<TradeStatusVM>().HasNoKey();
      modelBuilder.Entity<WarehouseVM>().HasNoKey();
      modelBuilder.Entity<WHRVM>().HasNoKey();
      modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
      modelBuilder.Entity<PriceChangeVM>().HasNoKey();
      modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
      modelBuilder.Entity<AttendanceVM>().HasNoKey();
      modelBuilder.Entity<PriceRangeVM>().HasNoKey();
      modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
      modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
      modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
      modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
      modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
      modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
      modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
      modelBuilder.Entity<CommodityVM>().HasNoKey();
      modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
      modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
      modelBuilder.Entity<CommonTradeVM>().HasNoKey();
      modelBuilder.Entity<ExchangeActorCode>().HasNoKey();
      modelBuilder.Entity<SisterCompanyMemberVM>().HasNoKey();
      modelBuilder.Entity<TradeVM>().HasNoKey();
      modelBuilder.Entity<OrderMember>().HasNoKey();
      modelBuilder.Entity<BacketingViolation>().HasNoKey();
      modelBuilder.Entity<PriceDiffernceBetweenWarehouses>().HasNoKey();
      modelBuilder.Entity<ComparingTradeReconciliation>().HasNoKey();
      modelBuilder.Entity<FrontRunning>().HasNoKey();
      modelBuilder.Entity<WareHouse>().HasNoKey();
      modelBuilder.Entity<TradeVsSales>().HasNoKey();
      modelBuilder.Entity<ReceiptHoldingSellOderGraph>().HasNoKey();
      modelBuilder.Entity<TblCommodity>().HasNoKey();
      modelBuilder.Entity<SalesTradeCommodity>().HasNoKey();
      modelBuilder.Entity<SuspendedExchange>().HasNoKey();
      modelBuilder.Entity<RenewalInformation>().HasNoKey();
      modelBuilder.Entity<CancellationInformation>().HasNoKey();
      modelBuilder.Entity<EcxTradeVM>().HasNoKey();
      modelBuilder.Entity<EcxOrderVm>().HasNoKey();
      modelBuilder.Entity<WarehouseVM>().HasNoKey();
      modelBuilder.Entity<WHRVM>().HasNoKey();
      modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
      modelBuilder.Entity<PriceChangeVM>().HasNoKey();
      modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
      modelBuilder.Entity<AttendanceVM>().HasNoKey();
      modelBuilder.Entity<PriceRangeVM>().HasNoKey();
      modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
      modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
      modelBuilder.Entity<TradeExecutionAmountViewModel>().HasNoKey();
      modelBuilder.Entity<ReceiptHoldingSellOrder>().HasNoKey();
      modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
      modelBuilder.Entity<TradeExecutionOrderViewModel>().HasNoKey();
      modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
      modelBuilder.Entity<CommodityWarehouseMarketShare>().HasNoKey();
      modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
      modelBuilder.Entity<ViolationBankTransaction>().HasNoKey();
      modelBuilder.Entity<CommonTradeVM>().HasNoKey();
      modelBuilder.Entity<EcxOrderVm>().HasNoKey();
      modelBuilder.Entity<TradeReconcilationFigure>().HasNoKey();
      modelBuilder.Entity<AttendanceBuyerSellerRatio>().HasNoKey();
      modelBuilder.Entity<AttendanceVolumeValueRatio>().HasNoKey();
      modelBuilder.Entity<TopTradeExecution>().HasNoKey();
      modelBuilder.Entity<SettlementTransactionMonthly>().HasNoKey();
      modelBuilder.Entity<SettlementTransactionDaily>().HasNoKey();
      modelBuilder.Entity<HHIIndex>().HasNoKey();
      modelBuilder.Entity<TopLink>().HasNoKey();
      modelBuilder.Entity<TradeSummary>().HasNoKey();
      modelBuilder.Entity<WarehouseSummary>().HasNoKey();
      modelBuilder.Entity<TopMarketShare>().HasNoKey();
      modelBuilder.Entity<PriceAndQuantitySummury>().HasNoKey();
      modelBuilder.Entity<PriceAndQuantitySummuryByClass>().HasNoKey();
      modelBuilder.Entity<PriceAndQuantitySummuryByCommodity>().HasNoKey();
      modelBuilder.Entity<ContractMarketShare>().HasNoKey();
      modelBuilder.Entity<TotalSalesForMarket>().HasNoKey();
      modelBuilder.Entity<WarehouseMarketShare>().HasNoKey();
      modelBuilder.Entity<MarketShareByCommodity>().HasNoKey();
      modelBuilder.Entity<ViolationRecordVm>().HasNoKey();
      modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
      modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
      modelBuilder.Entity<AuctionVM>().HasNoKey();
      modelBuilder.Entity<Auction>().HasNoKey();
      //modelBuilder.Entity<BidOfferRecordVM>().HasNoKey();
      modelBuilder.Entity<ComparisonTradeByCommodityType>().HasNoKey();
      modelBuilder.Entity<TradeSummaryByCommodity>().HasNoKey();
      modelBuilder.Entity<BuyerSellerRatio>().HasNoKey();
      modelBuilder.Entity<DailyPriceLimitVM>().HasNoKey();
      modelBuilder.Entity<OrderStatusVM>().HasNoKey();
      modelBuilder.Entity<OrderValidityVM>().HasNoKey();
      modelBuilder.Entity<SessionDataVM>().HasNoKey();
      modelBuilder.Entity<SessionStatusVM>().HasNoKey();
      modelBuilder.Entity<SubSessionVM>().HasNoKey();
      modelBuilder.Entity<TradeDataVM>().HasNoKey();
      modelBuilder.Entity<TradeStatusVM>().HasNoKey();
      modelBuilder.Entity<WarehouseVM>().HasNoKey();
      modelBuilder.Entity<WHRVM>().HasNoKey();
      modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
      modelBuilder.Entity<PriceChangeVM>().HasNoKey();
      modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
      modelBuilder.Entity<AttendanceVM>().HasNoKey();
      modelBuilder.Entity<PriceRangeVM>().HasNoKey();
      modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
      modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
      modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
      modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
      modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
      modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
      modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
      modelBuilder.Entity<CommodityVM>().HasNoKey();
      modelBuilder.Entity<WeightedAveragePriceVM>().HasNoKey();
      modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
      modelBuilder.Entity<CommonTradeVM>().HasNoKey();
      modelBuilder.Entity<ExchangeActorCode>().HasNoKey();
      modelBuilder.Entity<SisterCompanyMemberVM>().HasNoKey();
      modelBuilder.Entity<TradeVM>().HasNoKey();
      modelBuilder.Entity<OrderMember>().HasNoKey();
      modelBuilder.Entity<BacketingViolation>().HasNoKey();
      modelBuilder.Entity<PriceDiffernceBetweenWarehouses>().HasNoKey();
      modelBuilder.Entity<ComparingTradeReconciliation>().HasNoKey();
      modelBuilder.Entity<FrontRunning>().HasNoKey();
      modelBuilder.Entity<WareHouse>().HasNoKey();
      modelBuilder.Entity<TradeVsSales>().HasNoKey();
      modelBuilder.Entity<ReceiptHoldingSellOderGraph>().HasNoKey();
      modelBuilder.Entity<TblCommodity>().HasNoKey();
      modelBuilder.Entity<SalesTradeCommodity>().HasNoKey();
      modelBuilder.Entity<SuspendedExchange>().HasNoKey();
      modelBuilder.Entity<RenewalInformation>().HasNoKey();
      modelBuilder.Entity<EcxTradeVM>().HasNoKey();
      modelBuilder.Entity<EcxOrderVm>().HasNoKey();
      modelBuilder.Entity<WarehouseVM>().HasNoKey();
      modelBuilder.Entity<WHRVM>().HasNoKey();
      modelBuilder.Entity<SubSessionStatusVM>().HasNoKey();
      modelBuilder.Entity<PriceChangeVM>().HasNoKey();
      modelBuilder.Entity<PreviousPriceVM>().HasNoKey();
      modelBuilder.Entity<AttendanceVM>().HasNoKey();
      modelBuilder.Entity<PriceRangeVM>().HasNoKey();
      modelBuilder.Entity<PreopenSessionVM>().HasNoKey();
      modelBuilder.Entity<QuantityBeforeOpen>().HasNoKey();
      modelBuilder.Entity<PreopenSessionDate>().HasNoKey();
      modelBuilder.Entity<PrearrangedTradeVM>().HasNoKey();
      modelBuilder.Entity<DistinctOrderVM>().HasNoKey();
      modelBuilder.Entity<PreviousSessionVM>().HasNoKey();
      modelBuilder.Entity<MarkingTheCloseVM>().HasNoKey();
      modelBuilder.Entity<CommodityVM>().HasNoKey();
      modelBuilder.Entity<OversightReportMOnthlySummaryModel>().HasNoKey();
      modelBuilder.Entity<CommonOrderAndTradeVM>().HasNoKey();
      modelBuilder.Entity<CommonTradeVM>().HasNoKey();
      modelBuilder.Entity<CommodityLookupVM>().HasNoKey();
      modelBuilder.Entity<TradeAttendanceValueVolumeRatio>().HasNoKey();
      modelBuilder.Entity<CommodityPriceLimit>().HasNoKey();
      base.OnModelCreating(modelBuilder);
    }

  }

}
