﻿using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core
{
    public class ECXWarehouseApplicationVersion2DbContext : DbContext
    {
        public ECXWarehouseApplicationVersion2DbContext(DbContextOptions<ECXWarehouseApplicationVersion2DbContext> options)
           : base(options)
        {

        }

        public virtual DbSet<TblArrivals> TblArrivals { get; set; }
        public virtual DbSet<ClWarehouses> ClWarehouses { get; set; }
        public virtual DbSet<ClCommodity> ClCommodity { get; set; }
        public virtual DbSet<TblGrnsStatus> TblGrnsStatus { get; set; }
        public virtual DbSet<TblGrns> TblGrns { get; set; }
        public virtual DbSet<TblWarehouseReciept> TblWarehouseReciept { get; set; }
        public virtual DbSet<TblPungin> TblPungin { get; set; }
        public virtual DbSet<ClCommodityClass> ClCommodityClass { get; set; }

        
        public virtual DbSet<TblPickupNotices> TblPickupNotices { get; set; }
        public virtual DbSet<TblVoucher> TblVoucher { get; set; }
        public virtual DbSet<TblGins> TblGins { get; set; }
        public virtual DbSet<TblPsa> TblPsa { get; set; }
        public virtual DbSet<TblPsastatus> TblPsastatus { get; set; }
        public virtual DbSet<TblPunpsa> TblPunpsa { get; set; }
        public virtual DbSet<ClGinstatus> ClGinstatus { get; set; }
        public virtual DbSet<TblCommodityGrade> TblCommodityGrade { get; set; }
        public virtual DbSet<TblWashingStation> TblWashingStation { get; set; }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddCommonFiledValue();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            AddCommonFiledValue();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddCommonFiledValue()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Added || x.State == EntityState.Modified);

            // Same value at initial creation
            var createdDateTime = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        ((BaseEntity)entity.Entity).IsActive = true;
                        ((BaseEntity)entity.Entity).IsDeleted = false;
                        ((BaseEntity)entity.Entity).CreatedDateTime = createdDateTime;
                        ((BaseEntity)entity.Entity).UpdatedDateTime = createdDateTime;
                        break;
                    case EntityState.Modified:
                        //  ((BaseEntity)entity.Entity).UpdatedDateTime =createdDateTime;
                        break;
                    case EntityState.Deleted:
                        entity.State = EntityState.Modified;
                        ((BaseEntity)entity.Entity).IsDeleted = true;
                        break;
                }

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");
            modelBuilder.Entity<ClCommodity>(entity =>
            {
                entity.ToTable("clCommodity");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StandardUnitOfMeasureId).HasColumnName("StandardUnitOfMeasureID");
            });
            modelBuilder.Entity<ClGinstatus>(entity =>
            {
                entity.HasKey(e => e.EnumValue);

                entity.ToTable("clGINStatus");

                entity.Property(e => e.EnumValue).ValueGeneratedNever();

                entity.Property(e => e.EnumName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.GinstatusId).HasColumnName("GINStatusID");
            });

            modelBuilder.Entity<ClCommodityClass>(entity =>
            {
                entity.ToTable("clCommodityClass");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Auto).HasColumnName("auto");

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });

      

            modelBuilder.Entity<TblVoucher>(entity =>
            {
                entity.HasKey(e => e.VoucherId);

                entity.ToTable("tblVoucher");

                entity.Property(e => e.VoucherId).ValueGeneratedNever();

                entity.Property(e => e.CertificateNo).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SpecificArea).HasMaxLength(50);

                entity.Property(e => e.VoucherNo)
                    .IsRequired()
                    .HasMaxLength(50);
            });
            modelBuilder.Entity<TblPickupNotices>(entity =>
            {
                entity.ToTable("tblPickupNotices");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AgentIdnumber)
                    .HasColumnName("AgentIDNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.AgentName).HasMaxLength(150);

                entity.Property(e => e.AgentTel)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientIdno)
                    .HasColumnName("ClientIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CommodityGradeId).HasColumnName("CommodityGradeID");

                entity.Property(e => e.CommodityName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConsignmentType).HasMaxLength(50);

                entity.Property(e => e.CreatedTimestamp).HasColumnType("datetime");

                entity.Property(e => e.Cupvalue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ExpectedPickupDateTime).HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate).HasColumnType("datetime");

                entity.Property(e => e.Grnno)
                    .HasColumnName("GRNNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Idtype)
                    .HasColumnName("IDType")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdtypeId).HasColumnName("IDTypeID");

                entity.Property(e => e.LastModifiedTimestamp).HasColumnType("datetime");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.MemberIdno)
                    .HasColumnName("MemberIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MemberName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PlateNumber).HasMaxLength(50);

                entity.Property(e => e.PunprintDateTime)
                    .HasColumnName("PUNPrintDateTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Punprinted).HasColumnName("PUNPrinted");

                entity.Property(e => e.PunprintedBy).HasColumnName("PUNPrintedBy");

                entity.Property(e => e.RawValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RepId).HasColumnName("RepID");

                entity.Property(e => e.RepIdno)
                    .HasColumnName("RepIDNo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RepName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RowVersionStamp).HasColumnType("datetime");

                entity.Property(e => e.SellerName).HasMaxLength(100);

                entity.Property(e => e.Shade).HasMaxLength(50);

                entity.Property(e => e.ShedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SustainableCertification).HasMaxLength(100);

                entity.Property(e => e.TotalValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TrailerPlateNumber).HasMaxLength(50);

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WarehouseName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseReceiptId).HasColumnName("WarehouseReceiptID");

                entity.Property(e => e.WashingMillingStation).HasMaxLength(100);

                entity.Property(e => e.WeightInKg).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Woreda).HasMaxLength(100);
            });
            modelBuilder.Entity<TblGins>(entity =>
            {
                entity.ToTable("tblGINS");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdjustmentTypeId).HasColumnName("AdjustmentTypeID");

                entity.Property(e => e.ClientSignedDate).HasColumnType("datetime");

                entity.Property(e => e.ClientSignedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.DailyLabourersAssociationId).HasColumnName("DailyLabourersAssociationID");

                entity.Property(e => e.DateIssued).HasColumnType("datetime");

                entity.Property(e => e.DateTimeLoaded).HasColumnType("datetime");

                entity.Property(e => e.DriverName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ginnumber)
                    .HasColumnName("GINNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GinstatusId).HasColumnName("GINStatusID");

                entity.Property(e => e.InventoryControlId).HasColumnName("InventoryControlID");

                entity.Property(e => e.IsPsa).HasColumnName("IsPSA");

                entity.Property(e => e.LeadInventoryController)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LeadInventoryControllerId).HasColumnName("LeadInventoryControllerID");

                entity.Property(e => e.LicenseIssuedBy)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LicenseNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LicsignedDate)
                    .HasColumnName("LICSignedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.LicsignedName)
                    .HasColumnName("LICSignedName")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LoadUnloadTicketNo)
                    .HasColumnName("LoadUnloadTicketNO")
                    .HasMaxLength(50);

                entity.Property(e => e.ManagerSignedDate).HasColumnType("datetime");

                entity.Property(e => e.ManagerSignedName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PlateNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Psaremark).HasColumnName("PSARemark");

                entity.Property(e => e.PsastackId).HasColumnName("PSAStackID");

                entity.Property(e => e.Remark).HasMaxLength(100);

                entity.Property(e => e.RowVersionStamp).HasColumnType("datetime");

                entity.Property(e => e.ScaleTicketNumber).HasMaxLength(50);

                entity.Property(e => e.ShedId).HasColumnName("ShedID");

                entity.Property(e => e.TrailerPlateNumber)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TruckRegisterTime).HasColumnType("datetime");

                entity.Property(e => e.TruckRequestTime).HasColumnType("datetime");

                entity.Property(e => e.UpdatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WbserviceProviderId).HasColumnName("WBServiceProviderID");
            });
            modelBuilder.Entity<TblArrivals>(entity =>
            {
                entity.ToTable("tblArrivals");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientName).HasMaxLength(200);

                entity.Property(e => e.CommodityId).HasColumnName("CommodityID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateTimeReceived).HasColumnType("datetime");

                entity.Property(e => e.DriverName).HasMaxLength(50);

                entity.Property(e => e.GrossWeight).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LicenseIssuedPlace).HasMaxLength(50);

                entity.Property(e => e.LicenseNumber).HasMaxLength(50);

                entity.Property(e => e.ProcessingCenter).HasMaxLength(250);

                entity.Property(e => e.Remark).HasColumnType("text");

                entity.Property(e => e.Sampletype)
                    .HasColumnName("sampletype")
                    .HasMaxLength(50);

                entity.Property(e => e.ScaleTicketNo).HasMaxLength(50);

                entity.Property(e => e.SpecificArea).HasMaxLength(100);

                entity.Property(e => e.TrackingNumber).HasMaxLength(50);

                entity.Property(e => e.TrailerPlateNumber).HasMaxLength(50);

                entity.Property(e => e.TruckPlateNumber).HasMaxLength(50);

                entity.Property(e => e.VehicleSize).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VoucherCertificateNo).HasMaxLength(50);

                entity.Property(e => e.VoucherCommodityTypeId).HasColumnName("VoucherCommodityTypeID");

                entity.Property(e => e.VoucherNumber).HasMaxLength(50);

                entity.Property(e => e.VoucherWeight).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WoredaId).HasColumnName("WoredaID");
            });
            modelBuilder.Entity<TblPsa>(entity =>
            {
                entity.HasKey(e => e.Psaid);

                entity.ToTable("tblPSA");

                entity.Property(e => e.Psaid)
                    .HasColumnName("PSAId")
                    .ValueGeneratedNever();

                entity.Property(e => e.BWhr).HasColumnName("bWHR");

                entity.Property(e => e.ClientSignedDate).HasColumnType("datetime");

                entity.Property(e => e.ClientSignedName).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DailyLabroursAssociationId).HasColumnName("DailyLabroursAssociationID");

                entity.Property(e => e.DateIssued).HasColumnType("datetime");

                entity.Property(e => e.DateTimeLoaded).HasColumnType("datetime");

                entity.Property(e => e.InsertedDueTo).HasMaxLength(100);

                entity.Property(e => e.Licid).HasColumnName("LICId");

                entity.Property(e => e.Licname)
                    .HasColumnName("LICName")
                    .HasMaxLength(50);

                entity.Property(e => e.LicsignedDate)
                    .HasColumnName("LICSignedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.LicsignedName)
                    .HasColumnName("LICSignedName")
                    .HasMaxLength(50);

                entity.Property(e => e.NoteType).HasMaxLength(50);

                entity.Property(e => e.OcapprovalStatus).HasColumnName("OCApprovalStatus");

                entity.Property(e => e.OcapprovedName)
                    .HasColumnName("OCApprovedName")
                    .HasMaxLength(100);

                entity.Property(e => e.OccreatedDate)
                    .HasColumnName("OCCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Ocremark)
                    .HasColumnName("OCRemark")
                    .HasMaxLength(500);

                entity.Property(e => e.PsamodifiedBy).HasColumnName("PSAmodifiedBy");

                entity.Property(e => e.Psanumber)
                    .IsRequired()
                    .HasColumnName("PSANumber")
                    .HasMaxLength(50);

                entity.Property(e => e.PsastatusId).HasColumnName("PSAStatusID");

                entity.Property(e => e.PsaupdatedTimeStamp)
                    .HasColumnName("PSAUpdatedTimeStamp")
                    .HasColumnType("datetime");

                entity.Property(e => e.Swhr).HasColumnName("swhr");

                entity.Property(e => e.Symbol).HasMaxLength(10);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TransactionId).HasMaxLength(50);

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");

                entity.Property(e => e.WhbranchManagerApprovalStatus).HasColumnName("WHBranchManagerApprovalStatus");

                entity.Property(e => e.WhbranchManagerCreatedDate)
                    .HasColumnName("WHBranchManagerCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.WhbranchManagerName)
                    .HasColumnName("WHBranchManagerName")
                    .HasMaxLength(100);

                entity.Property(e => e.WhbranchManagerRemark)
                    .HasColumnName("WHBranchManagerRemark")
                    .HasMaxLength(500);

                entity.Property(e => e.WhmanagerApprovalStatus).HasColumnName("WHManagerApprovalStatus");

                entity.Property(e => e.WhmanagerCreatedDate)
                    .HasColumnName("WHManagerCreatedDate")
                    .HasMaxLength(100);

                entity.Property(e => e.WhmanagerName)
                    .HasColumnName("WHManagerName")
                    .HasMaxLength(100);

                entity.Property(e => e.WhmanagerRemark)
                    .HasColumnName("WHManagerRemark")
                    .HasMaxLength(500);

                entity.Property(e => e.WhsupervisorApprovalStatus).HasColumnName("WHSupervisorApprovalStatus");

                entity.Property(e => e.WhsupervisorApprovedDate)
                    .HasColumnName("WHSupervisorApprovedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.WhsupervisorName)
                    .HasColumnName("WHSupervisorName")
                    .HasMaxLength(100);

                entity.Property(e => e.WhsupervisorPsadescription)
                    .HasColumnName("WHSupervisorPSADescription")
                    .HasMaxLength(100);

                entity.Property(e => e.WhsupervisorRemark)
                    .HasColumnName("WHSupervisorRemark")
                    .HasMaxLength(100);
            });
            modelBuilder.Entity<TblPunpsa>(entity =>
            {
                entity.ToTable("tblPUNPSA");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Psaid).HasColumnName("PSAId");

                entity.Property(e => e.PunId).HasColumnName("PunID");

                entity.Property(e => e.PunPrintedTime).HasColumnType("datetime");
            });
            modelBuilder.Entity<TblWarehouseReciept>(entity =>
            {
                entity.ToTable("tblWarehouseReciept");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.Grnid).HasColumnName("GRNID");

                entity.Property(e => e.Grnnumber)
                    .HasColumnName("GRNNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.LastModifiedTimeStamp).HasColumnType("datetime");
            });
            modelBuilder.Entity<TblPsastatus>(entity =>
            {
                entity.ToTable("tblPSAStatus");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });
            base.OnModelCreating(modelBuilder);



        }

    }


}
