using CUSTOR.ICERS.API.EntityLayer.Warehouse;
using CUSTOR.ICERS.API.ModelsdbCentralDepository;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core
{
 

    public class EcxdbCentralDepositoryDbContext : DbContext
    {
        public EcxdbCentralDepositoryDbContext(DbContextOptions<EcxdbCentralDepositoryDbContext> options)
           : base(options)
        {

        }

        public virtual DbSet<TblExtendExpiryDateStatus> TblExtendExpiryDateStatus { get; set; }
        public virtual DbSet<TblExtendPunexpiryDate> TblExtendPunexpiryDate { get; set; }
        public virtual DbSet<TblNotApprovedWarehouseReceipts> TblNotApprovedWarehouseReceipts { get; set; }
        public virtual DbSet<TblNotApprovedWarehouseReceipts> TblPickUpNoticeWarehouseReciept { get; set; }
        public virtual DbSet<API.ModelsdbCentralDepository.TblPsa> TblPsa { get; set; }
        public virtual DbSet<API.ModelsdbCentralDepository.TblWarehouseReciept> TblWarehouseReciept { get; set; }
        public virtual DbSet<TblWarehouseRecieptStatus> TblWarehouseRecieptStatus { get; set; }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddCommonFiledValue();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            AddCommonFiledValue();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddCommonFiledValue()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Added || x.State == EntityState.Modified);

            // Same value at initial creation
            var createdDateTime = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        ((BaseEntity)entity.Entity).IsActive = true;
                        ((BaseEntity)entity.Entity).IsDeleted = false;
                        ((BaseEntity)entity.Entity).CreatedDateTime = createdDateTime;
                        ((BaseEntity)entity.Entity).UpdatedDateTime = createdDateTime;
                        break;
                    case EntityState.Modified:
                        //  ((BaseEntity)entity.Entity).UpdatedDateTime =createdDateTime;
                        break;
                    case EntityState.Deleted:
                        entity.State = EntityState.Modified;
                        ((BaseEntity)entity.Entity).IsDeleted = true;
                        break;
                }

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");
            modelBuilder.Entity<TblExtendPunexpiryDate>(entity =>
            {
                entity.ToTable("tblExtendPUNExpiryDate");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.DateRejected).HasColumnType("datetime");

                entity.Property(e => e.DateRequested).HasColumnType("datetime");

                entity.Property(e => e.Punid).HasColumnName("PUNId");
            });
            modelBuilder.Entity<TblExtendExpiryDateStatus>(entity =>
            {
                entity.ToTable("tblExtendExpiryDateStatus");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<API.ModelsdbCentralDepository.TblPsa>(entity =>
            {
                entity.ToTable("tblPSA");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BWhr).HasColumnName("bWHR");

                entity.Property(e => e.CdmanagerApprovalStatus).HasColumnName("CDManagerApprovalStatus");

                entity.Property(e => e.CdmanagerApprovedBy).HasColumnName("CDManagerApprovedBy");

                entity.Property(e => e.CdmanagerCreatedDate)
                    .HasColumnName("CDManagerCreatedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CdmanagerRemark)
                    .HasColumnName("CDManagerRemark")
                    .HasMaxLength(100);

                entity.Property(e => e.IntiatedDueTo).HasMaxLength(100);

                entity.Property(e => e.NoteType).HasMaxLength(50);

                entity.Property(e => e.Psanumber)
                    .IsRequired()
                    .HasColumnName("PSANumber")
                    .HasMaxLength(20);

                entity.Property(e => e.Swhr).HasColumnName("swhr");

                entity.Property(e => e.Symbol).HasMaxLength(10);

                entity.Property(e => e.TradeDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<API.ModelsdbCentralDepository.TblWarehouseReciept>(entity =>
            {
                //entity.HasKey(e => e.Id)
                //    .ForSqlServerIsClustered(false);

                entity.ToTable("tblWarehouseReciept");

                entity.HasIndex(e => e.WarehouseRecieptId)
                    .HasName("IX_tblWarehouseReciept")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CarPlateNumber).HasMaxLength(50);

                entity.Property(e => e.ClientCertificate).HasMaxLength(150);

                entity.Property(e => e.CreatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.CupValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.DateClosed).HasColumnType("datetime");

                entity.Property(e => e.DateDeposited).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.Grnid).HasColumnName("GRNID");

                entity.Property(e => e.Grnnumber)
                    .IsRequired()
                    .HasColumnName("GRNNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Grnstatus).HasColumnName("GRNStatus");

                entity.Property(e => e.GrntypeId).HasColumnName("GRNTypeId");

                entity.Property(e => e.LastModifiedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.ManualApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.RawValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RejectionReason).IsUnicode(false);

                entity.Property(e => e.Remark)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ScreenSize).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Shade).HasMaxLength(50);

                entity.Property(e => e.TotalValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TradeDate).HasColumnType("datetime");

                entity.Property(e => e.TradeId).HasColumnName("TradeID");

                entity.Property(e => e.TrailerPlateNumber).HasMaxLength(50);

                entity.Property(e => e.WarehouseRecieptId).ValueGeneratedOnAdd();

                entity.Property(e => e.WashingandMillingStation).HasMaxLength(50);

                entity.Property(e => e.WrstatusId).HasColumnName("WRStatusId");
            });

            modelBuilder.Entity<TblWarehouseRecieptStatus>(entity =>
            {
                entity.ToTable("tblWarehouseRecieptStatus");

                entity.Property(e => e.CreatedDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("smalldatetime");
            });
            base.OnModelCreating(modelBuilder);

        }

    }

}
