using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Address
{
    public partial class Kebele : BaseEntity
    {
        public Kebele()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KebeleId { get; set; }
        public int WoredaId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }

        public virtual Woreda Wereda { get; set; }
        public ICollection<SisterCompany> SisterCompany { get; set; }

    }

    public partial class KebeleDTO
    {
        public KebeleDTO()
        {
        }

        [Key]
        public int KebeleId { get; set; }

        public string WoredaId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
