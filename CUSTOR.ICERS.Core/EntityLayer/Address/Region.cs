﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Address
{
    public partial class Region : BaseEntity
    {
        public Region()
        {
            Zone = new HashSet<Zone>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegionId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }

        public virtual ICollection<Zone> Zone { get; set; }
        public ICollection<SisterCompany> SisterCompany { get; set; }
    }

    public partial class RegionDTO
    {
        public RegionDTO()
        {
        }

        [Key]
        public int RegionId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
