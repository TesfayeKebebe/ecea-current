﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Address
{
    public partial class Woreda : BaseEntity
    {
        public Woreda()
        {
            Kebele = new HashSet<Kebele>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WoredaId { get; set; }
        public int ZoneId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }

        public virtual Zone Zone { get; set; }
        public virtual ICollection<Kebele> Kebele { get; set; }
        public ICollection<SisterCompany> SisterCompany { get; set; }

    }

    public partial class WoredaDTO
    {
        public WoredaDTO()
        {
        }

        [Key]
        public int WoredaId { get; set; }

        public string ZoneId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
