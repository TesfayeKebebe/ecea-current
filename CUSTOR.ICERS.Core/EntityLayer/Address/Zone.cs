﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Address
{
    public partial class Zone : BaseEntity
    {
        public Zone()
        {
            Woreda = new HashSet<Woreda>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ZoneId { get; set; }
        public int RegionId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }


        public virtual Region Region { get; set; }
        public virtual ICollection<Woreda> Woreda { get; set; }
        public ICollection<SisterCompany> SisterCompany { get; set; }

    }

    public partial class ZoneDTO
    {
        public ZoneDTO()
        {
        }

        [Key]
        public int ZoneId { get; set; }

        public string RegionId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
