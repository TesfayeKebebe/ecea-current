namespace CUSTOR.ICERS.Core.EntityLayer.Audit
{
    public class AuditViewModel
    {
        public string TableName { get; set; }
        public string ColumnName { get; set; }

        public string Comment { get; set; }
        public string UserFullName { get; set; }
        public string Date { get; set; }

    }
}
