﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class Certificate : BaseEntity
    {
        public int CertificateId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        public int ServiceId { get; set; }
        public Service.Service Service { get; set; }
        public DateTime IssuedDateTime { get; set; }
        public DateTime ExpireDateTime { get; set; }
    }


    public class CertificateForProfile
    {
        public Guid? ExchangeActorId { get; set; }
        public Guid? ServiceApplicationId { get; set; }      
        public DateTime IssuedDateTime { get; set; }
        public DateTime ExpireDateTime { get; set; }
        public int RenewalType { get; set; }

    }
    public class CertificateDTO: BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int ServiceId { get; set; }
    }

    public class CertificateDataDTO
    {
        public string Name { get; set; }
        public string NameEng { get; set; }

        public string DateOfIssueEth { get; set; }
        public string DateOfIssue { get; set; }
        public string DirectorGeneralName { get; set; }
        public string DirectorGeneralEng { get; set; }

    }

    public class AuditorCertificateDataDTO
    {
        public string Name { get; set; }
        public string NameEng { get; set; }

        public string DateOfIssueEth { get; set; }
        public string DateOfIssue { get; set; }
        public string TradeName { get; set; }
        public string TradeNameEng { get; set; }
        public string Nationality { get; set; }
        public string NationalityEng { get; set; }
        public int RegistrationNo { get; set; }
        public string DirectorGeneralName { get; set; }
        public string DirectorGeneralEng { get; set; }

    }

    public class ReprsentativeCertificateDataDTO
    {
        public string Name { get; set; }
        public string NameEng { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationNameEng { get; set; }
        public string DateOfIssueEth { get; set; }
        public string DateOfIssue { get; set; }
        public string DirectorGeneralName { get; set; }
        public string DirectorGeneralEng { get; set; }
    }
}
