using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer
{
  //public class Commodity: BaseEntity
  //{
  //    [Key]
  //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  //    public int CommodityId { get; set; }
  //    public string Description { get; set; }
  //    public string DescriptionEng { get; set; }
  //}

  public class Commodity : BaseEntity
  {
    public int CommodityId { get; set; }
    public string DescriptionAmh { get; set; }
    public string DesciptionEng { get; set; }
    //public int CommodityTypeId { get; set; }
    //public int CommodityGradeId { get; set; }
    public int? CommodityUnit { get; set; }
    public int? PriceTollerance { get; set; }
    public decimal? WeightedAverage { get; set; }
    public int? UnitMeasurementId { get; set; }
    public virtual UnitMeasurement UnitMeasurement { get; set; }
    public virtual ICollection<CommodityType> CommodityTypes { get; set; }
  }
  public class CommodityDTO
  {
    public int CommodityId { get; set; }
    public string DescriptionAmh { get; set; }
    public string DesciptionEng { get; set; }
    public int? UnitMeasurementId { get; set; }
    public string CommodityUnitDesc { get; set; }
    public int? PriceTollerance { get; set; }
    public decimal? WeightedAverage { get; set; }
  }
}
