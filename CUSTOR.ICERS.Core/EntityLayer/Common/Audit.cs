using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Common
{
    public class Audit
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public string UserFullName { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public ActionResult ActionResult { get; set; }
        public Guid UserId { get; set; }

    }
    public enum ActionResult
    {
        Save, Update, Delete
    }
    public class AuditDTO
    {
        public string Message { get; set; }
        public string UserFullName { get; set; }
        public ActionResult ActionResult { get; set; }
        public Guid UserId { get; set; }
    }
    public class AuditDMV
    {
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string UserFullName { get; set; }
        public DateTime ActionDateTime { get; set; }
        public string ActionDescription { get; set; }
        public string Audit_Description { get; set; }
        public string OldValue { get; set; }
        public string OldValue_Decode { get; set; }
        public string NewValue { get; set; }
        public string NewValue_Decode { get; set; }

    }
    public class AuditAmendmentDMV
    {
        public string ColumnName { get; set; }
        public string UserFullName { get; set; }
        public DateTime ActionDateTime { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

    }
    public class TableInformation
    {
        public string TableName { get; set; }
    }

}
