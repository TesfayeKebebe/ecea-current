﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Common

{
    public partial class BaseEntity
    {

        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }

    }
}
