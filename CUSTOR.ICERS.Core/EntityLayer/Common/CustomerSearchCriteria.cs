﻿namespace CUSTOR.ICERS.Core.EntityLayer.Common
{
    public partial class CustomerSearchCriteria
    {
        public string ECXCode { get; set; }
        public string Tin { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public int CustomerTypeId { get; set; }

        public CustomerSearchCriteria()
        {

        }
    }
}
