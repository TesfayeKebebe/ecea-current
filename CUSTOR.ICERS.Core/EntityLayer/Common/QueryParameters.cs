﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Common
{
    public class QueryParameters
    {
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
        public string SiteCode { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
    public class SearchParameters
    {
        public string BankName { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
    public class NotificationQueryParameters
    {
        public int WorkFlowUserRoleId { get; set; }
        public string Lang { get; set; }
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
    }

    public class ServicePrerequisiteQueryParameters
    {
        public int ExchangeActorType { get; set; }
        public int ServiceGuid { get; set; }
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
    }
    public class SearchEcxParameters
    {
        public int PageNumber { get; set; } = 1;
        public int PageCount { get; set; } = 10;
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string SiteCode { get; set; }
    }
    public class TrendQueryParameters
    {
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
        public string ProductType { get; set; }
        public string Symbol { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public Guid CommodityId { get; set; }
    }
}
