﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public partial class StaticData
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public partial class StaticData2
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Description { get; set; }
    }


    public partial class StaticData3
    {
        public int Id { get; set; }
        public Guid ParentId { get; set; }
        public string Description { get; set; }
    }
    public partial class StaticData4
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
    public partial class StaticData5
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public double Amount { get; set; }
        public int CustomerTypeId { get; set; }
    }

    public partial class StaticData6
    {
        public int Id { get; set; }
    }
    public partial class StaticData7
    {

        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Description { get; set; }
    }

    public partial class StaticData8
    {

        public int Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
    }
}
