﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Service;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public partial class CustomerPrerequisite : BaseEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerPrerequisiteId { get; set; }

        public Guid ServiceApplicationId { get; set; }

        public int ServicePrerequisiteId { get; set; }

        public string Title { get; set; }

        public string FileName { get; set; }

        public ServicePrerequisite ServicePrerequisite { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
    }

    public partial class CustomerPrerequisiteDTO
    {
        public Guid ServiceApplicationId { get; set; }
        public int ServicePrerequisiteId { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
    }

    public class PrerequisteDocumentDTO
    {
        public string Name { get; set; }
        public int ServicePrerequisite { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ServicePrerequisiteId { get; set; }
        public IFormFile File { get; set; }
        public string FileType { get; set; }
    }
}
