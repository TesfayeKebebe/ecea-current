namespace CUSTOR.ICERS.Core.EntityLayer.Dashboard
{
    public class DashboardModel
    {
        public int TotalActiveExchangactor { get; set; }
        public int TotalApprovedInjection { get; set; }
        public int TotalNonApprovedInjection { get; set; }
        public int TotalCancellation { get; set; }
        public int TotalNotFinishedForrecognation { get; set; }
        public int TotalPrecancellation { get; set; }
        public int notFinisedCancellation { get; set; }
        public int notFinisedRenewal { get; set; }
        public int notFinisedReplacment { get; set; }
        public int totalRenewal { get; set; }
        public int totalReplacment { get; set; }
        public int totalCancellation { get; set; }
        public int totalNewCase { get; set; }
    }

    public class DashboardLawModel
    {
        public int TotalNewCase { get; set; }
        public int TotalReporterClosedCase { get; set; } 
        public int TotalInvestigationSelectedCase { get; set; }
        public int TotalNewInspectionRecordCase { get; set; } 
        public int TotalInvestigatorClosedCase { get; set; }
        public int TotalInvestigatorHeadSelectedCase { get; set; }
        public int TotalInvestigatorHeadClosedCase { get; set; }
        public int TotalLawEnDirectorClosedCase { get; set; } 
        public int TotalLawEnDirectorIncompleteCase { get; set; }
        public int TotalLawEnMainDirectorClosedCase { get; set; }
        public int TotalLawEnMainDirectorIncompleteCase { get; set; } 
        public int TotalCaseTeamApprovedCase { get; set; }
        public int TotalCaseTeamIncompleteCase { get; set; }

    }

    public class DashboardAtModel
    {
        public int TotalRegistrarApproved { get; set; }
        public int TotalRegistrarClosed { get; set; }
        public int TotalAppointment { get; set; } 
        public int TotalCreatedCase { get; set; }
    }
  public class CommonModel
  {
    public int Recognition { get; set; }
    public int Case { get; set; }
    public int OverSite { get; set; }
  }
}
