﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.DataChanges
{
    public class DataChange : BaseEntity
    {
        [Key]
        public int DataChangeId { get; set; }
        public int DataChangeTypeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string OldEmail { get; set; }
        public int? OldRegionId { get; set; }
        public int? OldZoneId { get; set; }
        public int? OldWoredaId { get; set; }
        public int? OldKebeleId { get; set; }
        public string OldMobileNo { get; set; }
        public string OldTel { get; set; }
        public string OldFaxNo { get; set; }
        public string OldPobox { get; set; }
        public string OldHouseNo { get; set; }
        public string OldHeadOffice { get; set; }
        public string OldHeadOfficeAmh { get; set; }
        public string OldOrganizationAreaNameAmh { get; set; }
        public string OldOrganizationAreaNameEng { get; set; }
        public string OldOrganizationNameEng { get; set; }
        public string OldOrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public Guid ServiceApplicationId { get; set; }
    }

    // address change DTO
    public class AddressChangeDTO
    {
        public int DataChangeTypeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string OldEmail { get; set; }
        public int? OldRegionId { get; set; }
        public int? OldZoneId { get; set; }
        public int? OldWoredaId { get; set; }
        public int? OldKebeleId { get; set; }
        public string OldMobileNo { get; set; }
        public string OldTel { get; set; }
        public string OldFaxNo { get; set; }
        public string OldPobox { get; set; }
        public string OldHouseNo { get; set; }
        public string OldHeadOffice { get; set; }
        public string OldHeadOfficeAmh { get; set; }
        public string OldOrganizationAreaNameAmh { get; set; }
        public string OldOrganizationAreaNameEng { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int CustomerTypeId { get; set; }
        public string OldRegion { get; set; }
        public string OldZone { get; set; }
        public string OldWoreda { get; set; }
        public string OldKebele { get; set; }
        public string OldOrganizationNameEng { get; set; }
        public string OldOrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
    }

    public class AddressChangeDTOForProfile
    {
        public string RegionId { get; set; }
        public string ZoneId { get; set; }
        public string WoredaId { get; set; }
        public string KebeleId { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public string Email { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string CreatedDateTime { get; set; }

    }

    public class NameChangeDTOForProfile
    {
        public string OldOrganizationNameEng { get; set; }
        public string OldOrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string CreatedDateTime { get; set; }

    }
    public class FinancilaInstutiutionDataChangeDTOForProfile
    {
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public string CreatedDateTime { get; set; }


    }

}
