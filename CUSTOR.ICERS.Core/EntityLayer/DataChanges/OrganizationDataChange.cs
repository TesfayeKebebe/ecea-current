﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.DataChange
{
    public class OrganizationDataChange : BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public int OrganizationDataChangeId { get; set; }
        public Guid DataChangeId { get; set; }
        public string OrganizationNameEngOld { get; set; }
        public string OrganizationNameAmhOld { get; set; }
        public int? OrganizationTypeOld { get; set; }
        public int? BusinessFiledOld { get; set; }
        public string OrganizationNameEngNew { get; set; }
        public string OrganizationNameAmhNew { get; set; }
        public int? OrganizationTypeNew { get; set; }
        public int? BusinessFiledNew { get; set; }
    }

    public class OrganizationDataChangeDTO
    {
        public int OrganizationDataChangeId { get; set; }
        //public Guid DataChangeId { get; set; }
        public string OrganizationNameEngOld { get; set; }
        public string OrganizationNameAmhOld { get; set; }
        public int? OrganizationTypeOld { get; set; }
        public int? BusinessFiledOld { get; set; }
        public string OrganizationNameEngNew { get; set; }
        public string OrganizationNameAmhNew { get; set; }
        public int? OrganizationTypeNew { get; set; }
        public int? BusinessFiledNew { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ChangeType { get; set; }
    }
}
