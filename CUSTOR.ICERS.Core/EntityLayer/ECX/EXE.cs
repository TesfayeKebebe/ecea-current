﻿using CUSTOR.ICERS.Core.EntityLayer.ECX;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.OTRLS.Core.EntityLayer.ECX
{
    public class EXE
    {
        [Key]
        public int EXEId { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? Date { get; set; }   
        public string TradeNo { get; set; }
        public string WRNo { get; set; }
        public string ConsignmentType { get; set; }
        public bool IsNewModel { get; set; }
        public string ProductType { get; set; }
        public string Grade { get; set; }
        public string Symbol { get; set; }
        public string BuyRepID { get; set; }
        public string BuyRepName { get; set; }
        public string BuyMemberId { get; set; }
        public string BuyMemberName { get; set; }
        public string BuyMemberTIN { get; set; }
        public string BuyClientId { get; set; }
        public string SellRepID { get; set; }
        public string SellRepName { get; set; }
        public string SellMemberId { get; set; }
        public string SellMemberName { get; set; }
        public string SellMemberTIN { get; set; }
        public string SellClientId { get; set; }
        public string SellClientName { get; set; }
        public string SellClientTIN { get; set; }
        public string Lot { get; set; }
        public double Quintal { get; set; }
        public double UnitPrice { get; set; }
        public double ValueInBirrSTD { get; set; }
        public double Bags { get; set; }
        public double VolumeKGUsingSTD { get; set; }
        public string Warehouse { get; set; }
        public string PY { get; set; }
        public bool IsOnline { get; set; }
        public double OpeningPrice { get; set; }
        public double ClosingPrice { get; set; }
        public double PrevClosingprice { get; set; }
        public double RangeinPercent { get; set; }
        public double LowerLimitPrice { get; set; }
        public int UpperLimitPrice { get; set; }
        public string SessionId { get; set; }
        public Session Session { get; set; }



    }
    public class EcexDTo
    {
        public int EXEId { get; set; }
        public string ProductType { get; set; }
        public string Date { get; set; }
        public string TradeNo { get; set; }
        public string WRNo { get; set; }
        public string ConsignmentType { get; set; }
        public bool IsNewModel { get; set; }
        public string Grade { get; set; }
        public string Symbol { get; set; }
        public string BuyRepID { get; set; }
        public string BuyRepName { get; set; }
        public string BuyMemberId { get; set; }
        public string BuyMemberName { get; set; }
        public string BuyMemberTIN { get; set; }
        public string BuyClientId { get; set; }
        public string SellRepID { get; set; }
        public string SellRepName { get; set; }
        public string SellMemberId { get; set; }
        public string SellMemberName { get; set; }
        public string SellMemberTIN { get; set; }
        public string SellClientId { get; set; }
        public string SellClientName { get; set; }
        public string SellClientTIN { get; set; }
        public string Lot { get; set; }
        public double Quintal { get; set; }
        public double UnitPrice { get; set; }
        public double ValueInBirrSTD { get; set; }
        public double Bags { get; set; }
        public double VolumeKGUsingSTD { get; set; }
        public string Warehouse { get; set; }
        public string PY { get; set; }
        public bool IsOnline { get; set; }
        public double OpeningPrice { get; set; }
        public double ClosingPrice { get; set; }
        public double PrevClosingprice { get; set; }
        public double RangeinPercent { get; set; }
        public double LowerLimitPrice { get; set; }
        public int UpperLimitPrice { get; set; }
        public string SessionId { get; set; }
    }
   public class EcexProductDto
    {
        public string ProductType { get; set; }
    }
}
