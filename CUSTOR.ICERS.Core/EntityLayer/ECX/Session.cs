﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.ECX
{
   public class Session
    {
        [Key]
        public string Id { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public TimeSpan? StartTime { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
    public TimeSpan? EndTime { get; set; }
    }    
    public class SessionDTo
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
