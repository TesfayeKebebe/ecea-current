using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
  public class CommodityLookupVM
  {
    public string Commodity { get; set; }
    public string CommodityClass { get; set; }
    public string Symbol { get; set; }
    public Guid CommodityGradeID { get; set; }
    public string CommodityGrade { get; set; }
  }
}
