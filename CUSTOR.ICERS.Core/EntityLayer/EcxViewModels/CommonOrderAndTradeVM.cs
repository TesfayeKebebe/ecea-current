using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class CommonOrderAndTradeVM
  {
    public Guid OrderId { get; set; }
    public Guid? tradeId { get; set; }
    public byte TransactionType { get; set; }
    public string warehouse { get; set; }
    public string memberCode { get; set; }
    public string clientCode { get; set; }
    public bool IsClientOrder { get; set; }
    public Guid CommodityGradeId { get; set; }
    public Guid? MemberId { get; set; }
    public DateTime? SubmittedTimestamp { get; set; }
    public Guid? ClientId { get; set; }
    public Guid? SessionId { get; set; }
    public decimal? Quantity { get; set; }
    public decimal? LimitPrice { get; set; }
    public DateTime? CreatedTimestamp { get; set; }
    public int ProductionYear { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public decimal? TradeQuantity { get; set; }
    public string BuyerCode { get; set; }
    public string SellerCode { get; set; }
    public decimal? TradePrice { get; set; }
    public DateTime? PreOpenStart { get; set; }
    public DateTime? PreOpenEnd { get; set; }
    public string MemberName { get; set; }
    public string BuyerName { get; set; }
    public string SellerName { get; set; }
    public string BuyerRepName { get; set; }
    public string SellerRepName { get; set; }
    public double? Lot { get; set; }
    public double? Quintal { get; set; }
    public double? Bags { get; set; }
    public decimal? VolumeKGUsingSTD { get; set; }
    public string IsOnline { get; set; }
    public string ConsignmentType { get; set; }
    public string IsNewModel { get; set; }
    public string SellerClientName { get; set; }
    public string BuyerClientName { get; set; }
    public Guid? WarehouseId { get; set; }

    public Guid? BuyOrderId { get; set; }
    public Guid? SellOrderId { get; set; }
    public DateTime? OpenStart { get; set; }
    public DateTime? OpenEnd { get; set; }
    public DateTime? TradedTimestamp { get; set; }
  }
  public class CommonTradeVM
  {
    public Guid tradeId { get; set; }
    //public byte TransactionType { get; set; }
    public string warehouse { get; set; }
    //public string memberCode { get; set; }
    public string clientCode { get; set; }
    //public bool IsClientOrder { get; set; }
    public Guid CommodityGradeId { get; set; }
    //public Guid? MemberId { get; set; }
    //public DateTime? SubmittedTimestamp { get; set; }
    //public Guid? ClientId { get; set; }
    public Guid? SessionId { get; set; }
    public decimal? Quantity { get; set; }
    public decimal? LimitPrice { get; set; }
    public DateTime? CreatedTimestamp { get; set; }
    //public int ProductionYear { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public decimal? TradeQuantity { get; set; }
    public string BuyerCode { get; set; }
    public string SellerCode { get; set; }
    public decimal? TradePrice { get; set; }
    public DateTime? PreOpenStart { get; set; }
    public DateTime? PreOpenEnd { get; set; }
    public string BuyerName { get; set; }
    public string SellerName { get; set; }
    public string BuyerRepName { get; set; }
    public string SellerRepName { get; set; }
    public double? Lot { get; set; }
    public double? Quintal { get; set; }
    public double? Bags { get; set; }
    public decimal? VolumeKGUsingSTD { get; set; }
    public string IsOnline { get; set; }
    public string ConsignmentType { get; set; }
    public string IsNewModel { get; set; }
    public string SellerClientName { get; set; }
    public string BuyerClientName { get; set; }
    public Guid? WarehouseId { get; set; }
    public Guid? BuyOrderTicketId { get; set; }
    public Guid? SellOrderTicketId { get; set; }
    public DateTime? OpenStart { get; set; }
    public DateTime? OpenEnd { get; set; }
    public DateTime? TradedTimestamp { get; set; } 
    public string Symbol { get; set; }
  }

  public class CommonOrderTradeDetail:CommonOrderAndTradeVM
  {
    public string Buyer { get; set; }
    public string Seller { get; set; }
  }
  public class OrderMember
  {
    public Guid? Id { get; set; }
    public string ClientName { get; set; }
  }
 public class ArrangedTrade
  {
    public decimal? LimitPrice { get; set; }
    public DateTime? CancelledTimestamp { get; set; }
    public Guid? CommodityGradeId { get; set; }
  }
}
