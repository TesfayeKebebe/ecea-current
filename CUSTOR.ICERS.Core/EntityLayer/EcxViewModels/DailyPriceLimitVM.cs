using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
  public class DailyPriceLimitVM
  {
    public string SessionName { get; set; }
    public DateTime TradeDate { get; set; }
    public int ProductionYear { get; set; }
    public string CommodityGrade { get; set; }
    public string Warehouse { get; set; }
    public decimal? UpperPriceLimit { get; set; }
    public decimal? LowerPriceLimit { get; set; }
    public decimal? ClosingPrice { get; set; }




  }
}
