using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class OrderStatusVM
  {
    public byte OrderStatusID { get; set; }
    public string OrderStatus { get; set; }
  }
}
