using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class OrderValidityVM
  {
    public byte OrderValidityID { get; set; }
    public string OrderValidity { get; set; }
  }
}
