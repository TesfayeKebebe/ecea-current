using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class SessionDataVM
  {
    public Guid? SessionId { get; set; }
    public string Name { get; set; }
    public DateTime? TradeDate { get; set; }
    public DateTime? OnlineStartActualTime { get; set; }
    public DateTime? OnlineEndActualTime { get; set; }
    public string OnlineStatus { get; set; }
  }
}
