using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class SessionStatusVM
  {
    public byte SessionStatusID { get; set; }
    public string SessionStatus { get; set; }
  }
}
