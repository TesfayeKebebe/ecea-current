using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public  class SubSessionStatusVM
  {
    public byte SubSessionStatusID { get; set; }
    public string SubSessionStatus { get; set; }
  }
}
