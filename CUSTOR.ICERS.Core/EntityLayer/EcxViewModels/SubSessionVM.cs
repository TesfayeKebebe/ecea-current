using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class SubSessionVM
  {
    public string Session { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public string SubSessionStatus { get; set; }
    public DateTime? SubSessionStartTime { get; set; }
    public DateTime? SubSessionEndTime { get; set; }

  }
}
