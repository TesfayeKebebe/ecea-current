using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class TradeDataVM
  {
    public string SessionName { get; set; }
    public string  BuyerClient { get; set; }
    public long? TradeNo { get; set; }
    public string CommodityGrade { get; set; }
    public string BuyerMember { get; set; }
    public string BuyerRep { get; set; }
    public string BuyTicketNo { get; set; }
    public bool? BuyIsClientTrade { get; set; }
    public string SellerClient { get; set; }
    public string SellerMember { get; set; }
    public string SellerRep { get; set; }
    public string SellTicketNo { get; set; }
    public bool? SellIsClientTrade { get; set; }
    public string Warehouse { get; set; }
    public decimal? TradePrice { get; set; }
    public decimal? TradeQuantity { get; set; }
    public int? ProductionYear { get; set; }
    public string TradeStatus { get; set; }
    public DateTime? TradedTimestamp { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public Guid? SessionId { get; set; }







  }
}
