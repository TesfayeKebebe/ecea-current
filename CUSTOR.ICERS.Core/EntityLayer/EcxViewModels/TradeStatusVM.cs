using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class TradeStatusVM
  {
    public byte TradeStatusID { get; set; }
    public string TradeStatus { get; set; }
  }
}
