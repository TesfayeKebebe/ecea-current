using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class TradeVM
  {
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public string BuyerCode { get; set; }
    public string SellerCode { get; set; }
  }
}
