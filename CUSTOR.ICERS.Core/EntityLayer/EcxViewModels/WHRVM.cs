using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class WHRVM
  {
    public Guid? Id { get; set; }
    public int? WarehouseRecieptId { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public Guid? WarehouseId { get; set; }
    public int? ProductionYear { get; set; }
    public Guid? ClientId { get; set; }
    public double OriginalQuantity { get; set; }

  }
}
