using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.EcxViewModels
{
 public class WarehouseVM
  {
    public Guid WarehouseID { get; set; }
    public string Warehouse { get; set; }
    public string ShortName { get; set; }
    public string Region { get; set; }
    public string Zone { get; set; }
  }
}
