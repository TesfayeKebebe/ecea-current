﻿namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
