using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CAUnit : BaseEntity
    {
        public CAUnit()
        {
            CaseDocument = new HashSet<CaseDocument>();
            CaseParty = new HashSet<CaseParty>();
            CaseFeedback = new HashSet<CaseFeedback>();
        }
        [Key]
        public Guid CAUnitId { get; set; }
        public ICollection<CaseDocument> CaseDocument { get; set; }
        public ICollection<CaseParty> CaseParty { get; set; }
        public ICollection<CaseFeedback> CaseFeedback { get; set; }
    }

    public class CAUnitInfo
    {
        public string CaseApplicationId { get; set; }
        public string InvestigationId { get; set; }
        public string LegalCaseId { get; set; }
        public string CaseTrackingId { get; set; }
        public CaseLookUpDTO Status { get; set; }
        public CaseAdministrationUnitType CAUnitType { get; set; }
    }
}
