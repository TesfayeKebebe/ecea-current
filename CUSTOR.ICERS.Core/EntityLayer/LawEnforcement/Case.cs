using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
  public partial class Case : BaseEntity
  {
    public Case()
    {
      CaseTracking = new HashSet<CaseTracking>();
    }
    [Key]
    public Guid CaseId { get; set; }

    [ForeignKey("CaseId")]
    public CAUnit CAUnit { get; set; }
    [Required]
    public DateTime OpeningDate { get; set; }
    [Required]
    public string OpeningReason { get; set; }
    [Required]
    public string FileNo { get; set; }

    public string CaseName { get; set; }
    public int? DecisionTypeId { get; set; }
    public Lookup.Lookup DecisionType { get; set; }
    public string DecisionRemark { get; set; }
    public Guid? DecisionCourtSessionId { get; set; }
    public DateTime? ClosingDate { get; set; }
    public string ClosingReason { get; set; }


    public string LawyerId { get; set; }
    public ApplicationUser Lawyer { get; set; }

    public Guid CaseInvestigationId { get; set; }
    public CaseInvestigation CaseInvestigation { get; set; }

    public CaseApplication CaseApplication { get; set; }

    public ICollection<CaseTracking> CaseTracking { get; set; }
    public Guid? ParentCaseId { get; set; }
    public Guid? CaseApplicationId { get; set; }
    public Guid? LastAppointmentId { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }
    public DateTime? MaxInjuctionDate { get; set; }
    public DateTime? MinInjuctionDate { get; set; }

  }

  public class LegalCaseDTO : BaseEntity
  {
    public Guid? CaseId { get; set; }
    [Required]
    public DateTime OpeningDate { get; set; }
    public string OpeningDateET { get; set; }
    [Required]
    public string OpeningReason { get; set; }
    [Required]
    public string FileNo { get; set; }

    public string CaseName { get; set; }
    public int? DecisionTypeId { get; set; }
    public LookUpDisplayView DecisionType { get; set; }
    public string DecisionRemark { get; set; }
    public Guid? DecisionCourtSessionId { get; set; }
    public DateTime? ClosingDate { get; set; }
    public string ClosingDateET { get; set; }
    public string ClosingReason { get; set; }
    public string AmhCaseName { get; set; }

    public string LawyerId { get; set; }
    public ApplicationUser Lawyer { get; set; }
    [Required]
    public Guid CaseApplicationId { get; set; }
    [Required]
    public Guid CaseInvestigationId { get; set; }
    public Guid? LastAppointmentId { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }
    public DateTime? MaxInjuctionDate { get; set; }
    public DateTime? MinInjuctionDate { get; set; }
    public string EthiopMaxInjuctionDate { get; set; }
    public string EthiopMinInjuctionDate { get; set; }
    public string AttachmentContent { get; set; }
    public string FileName { get; set; }

  }
  public partial class LegalCaseListModel
  {
    public Guid? CaseId { get; set; }
    public Guid CaseApplicationId { get; set; }
    public Guid CaseInvestigationId { get; set; }
    public string InvestigationFileNo { get; set; }
    public string FileNo { get; set; }
    public string CaseName { get; set; }

    public DateTime? OpeningDate { get; set; }
    public string OpeningDateET { get; set; }
    public int CaseTypeId { get; set; }
    public LookUpDisplayView CaseType { get; set; }
    public DateTime EventDate { get; set; }
    public string EventDateET { get; set; }
    public DateTime ApplicationDate { get; set; }
    public string ApplicationDateET { get; set; }
    public string Description { get; set; }
    public LookUpDisplayView Department { get; set; }
    public string CaseStatusCode { get; set; }
    public int CaseStatusTypeId { get; set; }
    public LookUpDisplayView CaseStatusType { get; set; }
    public string SuspectOrganizationName { get; set; }
    public string SuspectOrganizationNameAmh { get; set; }
    public string SuspectFullName { get; set; }
    public string SuspectFullNameAmh { get; set; }
    public string PlaintiffFullName { get; set; }
    public string PlaintiffFullNameAmh { get; set; }
    public Guid? SuspectPartyId { get; set; }
    public Guid? PlaintIffPartyId { get; set; }
    public DateTime CreatedDate { get; set; }
    public string CreatedDateET { get; set; }
    public DateTime UpdatedDate { get; set; }
    public string UpdatedDateET { get; set; }
    public string DecisionAmh { get; set; }
    public string DecisionEng { get; set; }
  }

  public class LegalCaseSearchCriteriaModel
  {
    public int? CaseType { get; set; }
    public int? StatusType { get; set; }
    public string FileNo { get; set; }
    public string CaseName { get; set; }
    public string From { get; set; }
    public string To { get; set; }
    public string Lang { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
  }
}
