using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
 public class CaseAppealDocument : BaseEntity
    {
   [Key]
  public string Id { get; set; }
  public string AttachmentContent { get; set; }
  public string FileName { get; set; }
  public Guid CaseId { get; set; }
  public Case Case { get; set; }
}
public class CaseAppealDocumentDto : BaseEntity
{
    public string Id { get; set; }
    public string AttachmentContent { get; set; }
    public string FileName { get; set; }
    [Required]
  public IFormFile ActualFile { get; set; }
  public Guid CaseId { get; set; }
  public Case Case { get; set; }


  }
}
