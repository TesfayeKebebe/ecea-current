using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseApplication : BaseEntity
    {
        public CaseApplication()
        {
            Case = new HashSet<Case>();
        }
        [Key]
        public Guid CaseApplicationId { get; set; }
        [ForeignKey("CaseApplicationId")]
        public CAUnit CAUnit { get; set; }
        public int CaseTypeId { get; set; }
        [ForeignKey("CaseTypeId")]
        public Lookup.Lookup CaseType { get; set; }
        [Required]
        public DateTime EventDate { get; set; }
        [Required]
        public DateTime ApplicationDate { get; set; }
        [Required]
        public string Description { get; set; }
        public string CaseInvestigationNo { get; set; }
        [Required]
        public int CaseStatusTypeId { get; set; }
        [ForeignKey("CaseStatusTypeId")]
        public CaseStatusType CaseStatusType { get; set; }
        [Required]
        public int DepartmentId { get; set; }
        public Lookup.Lookup Department { get; set; }

        [Required]
        public Guid SuspectId { get; set; }
        public CaseParty Suspect { get; set; }
        public Guid? PlaintiffId { get; set; }
        public CaseParty Plaintiff { get; set; }
        public CaseInvestigation CaseInvestigation { get; set; }
        public ICollection<Case> Case { get; set; }

    }

    public partial class CaseApplicationDTO
    {
        public Guid CaseApplicationId { get; set; }
        public int CaseTypeId { get; set; }
        public LookUpDisplayView CaseType { get; set; }
        public string EventDateET { get; set; }
        public DateTime EventDate { get; set; }
        public string ApplicationDateET { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Description { get; set; }
        public LookUpDisplayView CaseStatusType { get; set; }
        public int DepartmentId { get; set; }
        public LookUpDisplayView Department { get; set; }
        public CasePartyDTO Suspect { get; set; }
        public CasePartyDTO Plaintiff { get; set; }

        public string CreatedDateTimeET { get; set; }
        public DateTime CreatedDateTime { get; set; }

        public string UpdatedDateTimeET { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }

    public partial class CaseApplicationEditDTO : BaseEntity
    {
        [Required]
        public Guid CaseApplicationId { get; set; }
        [Required]
        public int CaseTypeId { get; set; }
        [Required]
        public DateTime EventDate { get; set; }
        [Required]
        public DateTime ApplicationDate { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int DepartmentId { get; set; }
        [Required]
        public CasePartyEditDTO Suspect { get; set; }
        [Required]
        public CasePartyEditDTO Plaintiff { get; set; }
    }

    public class CaseApplicationListModel : BaseEntity
    {
        public Guid CaseApplicationId { get; set; }
        public LookUpDisplayView CaseType { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDateET { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string ApplicationDateET { get; set; }
        public string Description { get; set; }
        public int ApplicationPartyType { set; get; }
        public int CaseStatusTypeId { get; set; }
        public Guid SuspectId { get; set; }
        public Guid? PlaintiffId { get; set; }
        public LookUpDisplayView Department { get; set; }
        public string CaseStatusCode { get; set; }
        public LookUpDisplayView CaseStatusType { get; set; }
        public string SuspectOrganizationName { get; set; }
        public string SuspectOrganizationNameAmh { get; set; }
        public string ExchangeActorOrganizationName { get; set; }
        public string ExchangeActorOrganizationNameEng { get; set; }
        public int CustomerTypeId { get; set; }
        public string ExchangeActorFullName { get; set; } 
        public string SuspectFullName { get; set; }
        public Guid? ExchangeActorId { get; set; } 
        public string SuspectFullNameAmh { get; set; }
        public string PlaintiffFullName { get; set; }
        public string PlaintiffFullNameAmh { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateET { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedDateET { get; set; }
    }

    public class CaseApplicationSearchCriteriaModel
    {
        public int? CaseType { get; set; }
        public int? Department { get; set; }
        public int? StatusType { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Lang { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
