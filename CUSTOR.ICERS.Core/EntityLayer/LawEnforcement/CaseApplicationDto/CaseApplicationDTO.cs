﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto
{

    public partial class CaseApplicationPartyDTO : BaseEntity
    {
        public CaseApplicationPartyDTO()
        {
            ExchangeActor = new ExchangeActorCasePartyModel();
        }
        public ExchangeActorCasePartyModel ExchangeActor { get; set; }
        [Key]
        public string CaseApplicationId { get; set; }
        public int CaseTypeId { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Description { get; set; }
        public int DepartmentId { get; set; }
        public int CaseStatusTypeId { get; set; }
        public Guid SuspectId { get; set; }
        public string CAUnitId { get; set; }
        public Guid? PlaintiffId { get; set; }
        public string CaseInvestigationNo { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public int? Age { get; set; }
        public int? NationalityId { get; set; }
        public int? EducationalLevelId { get; set; }
        public string Occupation { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string CasePartyId { get; set; }
        public int? GenderId { get; set; }
        public int PartyType { get; set; }
        public int PartyCategory { get; set; }
        public int ExchangeActorType { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public string OrganizationName { get; set; }
        public string FullName { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
        public bool? IsECEA { get; set; }
        public bool? IsECE { get; set; }
        public string Testimony { get; set; }
    }

    public partial class PartyDTO : BaseEntity 
    {
        public PartyDTO()
        {
            ExchangeActor = new ExchangeActorCasePartyModel();
        } 
        public ExchangeActorCasePartyModel ExchangeActor { get; set; }
        [Key]
        public string CasePartyId { get; set; }
        public string CaseApplicationId { get; set; } 
        public Guid CAUnitId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public int? Age { get; set; }
        public int? NationalityId { get; set; }
        public int? EducationalLevelId { get; set; }
        public string Occupation { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public int? GenderId { get; set; }
        public int PartyType { get; set; }
        public int PartyCategory { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public string OrganizationName { get; set; }
        public string FullName { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
        public bool? IsECEA { get; set; }
        public bool? IsECE { get; set; }
        public string Testimony { get; set; }
    }

}
