﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto
{
    public static class CaseApplicationHelper
    {
        // case application start
        public static CaseApplication GetCaseApplication(CaseApplicationPartyDTO caseApplicationDTO)
        {  
            try
            {
                return new CaseApplication
                { 
                    //CaseApplicationId = new Guid(caseApplicationDTO.CaseApplicationId),
                    CaseTypeId = caseApplicationDTO.CaseTypeId,
                    DepartmentId = caseApplicationDTO.DepartmentId,
                    CaseStatusTypeId = caseApplicationDTO.CaseStatusTypeId,
                    ApplicationDate = caseApplicationDTO.ApplicationDate,
                    EventDate = caseApplicationDTO.EventDate,
                    Description = caseApplicationDTO.Description,
                    CaseInvestigationNo = caseApplicationDTO.CaseInvestigationNo,
                    IsActive = true,
                    IsDeleted = false,
                    SuspectId = caseApplicationDTO.SuspectId,
                    PlaintiffId = caseApplicationDTO.PlaintiffId,
                    CreatedDateTime = DateTime.Now,
                    CreatedUserId = caseApplicationDTO.CreatedUserId,
                    UpdatedUserId = caseApplicationDTO.UpdatedUserId
                };
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return null;
            }

        }
        // case application start
        public static CaseApplication GetEditCaseApplication(CaseApplicationPartyDTO caseApplicationDTO)
        {
            try
            {
                return new CaseApplication
                {
                    CaseApplicationId = new Guid(caseApplicationDTO.CaseApplicationId),
                    CaseTypeId = caseApplicationDTO.CaseTypeId,
                    DepartmentId = caseApplicationDTO.DepartmentId,
                    CaseStatusTypeId = caseApplicationDTO.CaseStatusTypeId,
                    ApplicationDate = caseApplicationDTO.ApplicationDate,
                    EventDate = caseApplicationDTO.EventDate,
                    Description = caseApplicationDTO.Description,
                    IsActive = true,
                    IsDeleted = false,
                    SuspectId = caseApplicationDTO.SuspectId,
                    PlaintiffId = caseApplicationDTO.PlaintiffId,
                    CreatedDateTime = DateTime.Now
                };
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return null;
            }

        }
        public static CaseParty GetCaseParty(CaseApplicationPartyDTO caseApplicationDTO)
        {
            return new CaseParty
            {
                //CasePartyId = new Guid(caseApplicationDTO.CasePartyId),
                PartyType = caseApplicationDTO.PartyType,
                //CAUnitId = new Guid(caseApplicationDTO.CAUnitId),
                FirstNameAmh = caseApplicationDTO.FirstNameAmh,
                FatherNameAmh = caseApplicationDTO.FatherNameAmh,
                GrandFatherNameAmh = caseApplicationDTO.GrandFatherNameAmh,
                FirstNameEng = caseApplicationDTO.FirstNameEng,
                FatherNameEng = caseApplicationDTO.FatherNameEng,
                GrandFatherNameEng = caseApplicationDTO.GrandFatherNameEng,
                OrganizationNameAmh = caseApplicationDTO.OrganizationNameAmh,
                OrganizationNameEng = caseApplicationDTO.OrganizationNameEng,
                ExchangeActorId = caseApplicationDTO.ExchangeActor.ExchangeActorId,
                GenderId = caseApplicationDTO.GenderId,
                NationalityId = caseApplicationDTO.NationalityId,
                Age = caseApplicationDTO.Age,
                MobileNo = caseApplicationDTO.MobileNo,
                Email = caseApplicationDTO.Email,
                Fax = caseApplicationDTO.Fax,
                HouseNo = caseApplicationDTO.HouseNo,
                IsActive = true,
                IsDeleted = false,
                KebeleId = caseApplicationDTO.KebeleId,
                RegionId = caseApplicationDTO.RegionId,
                Pobox = caseApplicationDTO.Pobox,
                Tel = caseApplicationDTO.Tel,
                WoredaId = caseApplicationDTO.WoredaId,
                ZoneId = caseApplicationDTO.ZoneId,
                IsECE = caseApplicationDTO.IsECE,
                IsECEA = caseApplicationDTO.IsECEA,
                Testimony = caseApplicationDTO.Testimony
            };
        }
        public static CaseApplicationPartyDTO GetCaseApplicationDTO(CaseApplication ca, CaseParty cpty)
        {
            return new CaseApplicationPartyDTO
            {
                CaseApplicationId = ca.CaseApplicationId.ToString(),
                CaseTypeId = ca.CaseTypeId,
                CaseStatusTypeId = ca.CaseStatusTypeId,
                DepartmentId = ca.DepartmentId,
                ApplicationDate = ca.ApplicationDate,
                EventDate = ca.EventDate,
                Description = ca.Description,
                SuspectId = ca.SuspectId,
                PlaintiffId = ca.PlaintiffId,
                CaseInvestigationNo = ca.CaseInvestigationNo,
                CAUnitId = ca.CAUnit?.CAUnitId.ToString(),
                IsActive = ca.IsActive,
                IsDeleted = ca.IsDeleted,
                PartyType = cpty.PartyType,
                PartyCategory = cpty.PartyCategory,
                FirstNameAmh = cpty.FirstNameAmh,
                FatherNameAmh = cpty.FatherNameAmh,
                GrandFatherNameAmh = cpty.GrandFatherNameAmh,
                OrganizationNameAmh = cpty.OrganizationNameAmh,
                OrganizationNameEng = cpty.OrganizationNameEng,
                CasePartyId = cpty.CasePartyId.ToString(),
                NationalityId = cpty.NationalityId,
                GenderId = cpty.GenderId,
                MobileNo = cpty.MobileNo,
                Email = cpty.Email,
                Fax = cpty.Fax,
                HouseNo = cpty.HouseNo,
                KebeleId = cpty.KebeleId,
                RegionId = cpty.RegionId,
                Pobox = cpty.Pobox,
                Tel = cpty.Tel,
                WoredaId = cpty.WoredaId,
                ZoneId = cpty.ZoneId,
                IsECEA = cpty.IsECEA,
                IsECE = cpty.IsECE,
                Testimony = cpty.Testimony,
                ExchangeActorId = cpty?.ExchangeActor?.ExchangeActorId,
                Ecxcode = cpty?.ExchangeActor?.Ecxcode,
                OrganizationName = cpty?.ExchangeActor?.OrganizationNameAmh,
                FullName = cpty?.ExchangeActor?.OwnerManager?.FirstNameAmh
                +' '+cpty?.ExchangeActor?.OwnerManager?.FatherNameAmh
                +' ' + cpty?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                Tin = cpty?.ExchangeActor?.Tin
            };
        }
        // case application end

        // case party start CaseApplicationHelper.cs:line 113 at
        public static CaseParty GetParty(PartyDTO caseApplicationDTO)
        {
            return new CaseParty
            {
                //CasePartyId = new Guid(caseApplicationDTO.CasePartyId),
                PartyType = caseApplicationDTO.PartyType,
                PartyCategory = caseApplicationDTO.PartyCategory,
                CAUnitId = caseApplicationDTO.CAUnitId,
                FirstNameAmh = caseApplicationDTO.FirstNameAmh,
                FatherNameAmh = caseApplicationDTO.FatherNameAmh,
                GrandFatherNameAmh = caseApplicationDTO.GrandFatherNameAmh,
                FirstNameEng = caseApplicationDTO.FirstNameEng,
                FatherNameEng = caseApplicationDTO.FatherNameEng,
                GrandFatherNameEng = caseApplicationDTO.GrandFatherNameEng,
                OrganizationNameAmh = caseApplicationDTO.OrganizationNameAmh,
                OrganizationNameEng = caseApplicationDTO.OrganizationNameEng,
                GenderId = caseApplicationDTO.GenderId,
                NationalityId = caseApplicationDTO.NationalityId,
                Age = caseApplicationDTO.Age,
                ExchangeActorId = caseApplicationDTO.ExchangeActor.ExchangeActorId,
                MobileNo = caseApplicationDTO.MobileNo,
                //AddressType = (int)AddressType.eManager,
                Email = caseApplicationDTO.Email,
                Fax = caseApplicationDTO.Fax,
                HouseNo = caseApplicationDTO.HouseNo,
                IsActive = true,
                IsDeleted = false,
                KebeleId = caseApplicationDTO.KebeleId,
                RegionId = caseApplicationDTO.RegionId,
                Pobox = caseApplicationDTO.Pobox,
                Tel = caseApplicationDTO.Tel,
                WoredaId = caseApplicationDTO.WoredaId,
                ZoneId = caseApplicationDTO.ZoneId,
                IsECEA = caseApplicationDTO.IsECEA,
                IsECE = caseApplicationDTO.IsECE,
                Testimony = caseApplicationDTO.Testimony
            };
        }
        public static PartyDTO GetPartyDTO(CaseParty cpty)
        {
            return new PartyDTO
            {
                //CaseApplicationId = ca.CaseApplicationId.ToString(),
                PartyType = cpty.PartyType,
                PartyCategory = cpty.PartyCategory,
                FirstNameAmh = cpty.FirstNameAmh,
                FatherNameAmh = cpty.FatherNameAmh,
                GrandFatherNameAmh = cpty.GrandFatherNameAmh,
                OrganizationNameAmh = cpty.OrganizationNameAmh,
                OrganizationNameEng = cpty.OrganizationNameEng,
                CasePartyId = cpty.CasePartyId.ToString(),
                NationalityId = cpty.NationalityId,
                GenderId = cpty.GenderId,
                MobileNo = cpty.MobileNo,
                Email = cpty.Email,
                Fax = cpty.Fax,
                HouseNo = cpty.HouseNo,
                KebeleId = cpty.KebeleId,
                RegionId = cpty.RegionId,
                Pobox = cpty.Pobox,
                Tel = cpty.Tel,
                WoredaId = cpty.WoredaId,
                ZoneId = cpty.ZoneId,
                ExchangeActorId = cpty?.ExchangeActor?.ExchangeActorId,
                Ecxcode = cpty?.ExchangeActor?.Ecxcode,
                OrganizationName = cpty?.ExchangeActor?.OrganizationNameAmh,
                FullName = cpty?.ExchangeActor?.OwnerManager?.FirstNameAmh
                + ' ' + cpty?.ExchangeActor?.OwnerManager?.FatherNameAmh
                + ' ' + cpty?.ExchangeActor?.OwnerManager?.GrandFatherNameAmh,
                Tin = cpty?.ExchangeActor?.Tin,
                IsECEA = cpty.IsECEA,
                IsECE = cpty.IsECE,
                Testimony = cpty.Testimony,
            };
        }
        // case party end
    }
}
