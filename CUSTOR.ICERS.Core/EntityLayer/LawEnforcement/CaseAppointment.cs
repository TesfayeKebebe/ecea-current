using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTORCommon.Ethiopic;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseAppointment : BaseEntity
    {
        [Key]
        public Guid CaseAppointmentId { get; set; }
        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        [Required]
        public DateTime AppointmentDate { get; set; }
        [Required]
        public TimeSpan AppointmentTime { get; set; }
        [Required]
        public int AppointmentTypeId { get; set; }
        public Lookup.Lookup AppointmentType { get; set; }

        public string AppointmentReason { get; set; }
        public string AmhAppointmentReason { get; set; }
        public string Location { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }

  }

    public class AppointmentDTO : BaseEntity
    {
        public Guid? CaseAppointmentId { get; set; }
        //[Required]
        public Guid CaseId { get; set; }
        //[Required]
        public TimeSpan AppointmentTime { get; set; }
        //[Required]
        public int AppointmentTypeId { get; set; }
        //[Required]
        public string AppointmentReason { get; set; }
        public string AmhAppointmentReason { get; set; }
        public LookUpDisplayView AppointmentType { get; set; }

        private DateTime appointmentDate;
        //[Required]
        public DateTime AppointmentDate
        {
            get => appointmentDate;
            set
            {
                appointmentDate = value;
                if (value != null)
                {
                    AppointmentDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string AppointmentDateET { get; set; }
        public string Location { get; set; }
        public bool IsPrint { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }

  }

}
