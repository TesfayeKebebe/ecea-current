using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseDecisionTracking : BaseEntity
    {
        [Key]
        public Guid CaseDecisionTrackingId { get; set; }
        [ForeignKey("CaseDecisionTrackingId")]
        public CAUnit CAUnit { get; set; }

        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public DateTime DecisionDate { get; set; }
        public string Remark { get; set; }
        public string AmhRemark { get; set; }
        public string FirstJudgeId { get; set; }
        public string SecondJudgeId { get; set; }
        public string ThirdJudgeId { get; set; }
        public bool IsAppointment { get; set; }
        public Guid? AppointmentId { get; set; }
        public CaseAppointment Appointment { get; set; }
        public string Url { get; set; }
        public virtual ApplicationUser FirstJudge { get; set; }
        public virtual ApplicationUser SecondJudge { get; set; }
        public virtual ApplicationUser ThirdJudge { get; set; }
  
  }

    public class CaseDecisionTrackingDTO : BaseEntity
    {
        [Required]
        public Guid CaseDecisionTrackingId { get; set; }
        [Required]
        public Guid CaseId { get; set; }
        private DateTime decisionDate;
        [Required]
        public DateTime DecisionDate
        {
            get => decisionDate;
            set
            {
                decisionDate = value;
                if (value != null)
                {
                    DecisionDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string DecisionDateET { get; set; }
        //[Required]
        public bool IsAppointment { get; set; }
        //[Required]
        public bool IsClosed { get; set; }

        public string AmhRemark { get; set; }
        public string Remark { get; set; }
        public string FirstJudgeId { get; set; }
        public string SecondJudgeId { get; set; }
        public string ThirdJudgeId { get; set; }

        public AppointmentDTO Appointment { get; set; }

        public string FirstJudge { get; set; }
        public string SecondJudge { get; set; }
        public string ThirdJudge { get; set; }
        public bool? IsActive { get; set; }
        public int? DecisionTypeId { get; set; }
        public LookUpDisplayView DecisionType { get; set; }
        public string DecisionRemark { get; set; }
        public Guid? DecisionCourtSessionId { get; set; }
        public DateTime? ClosingDate { get; set; }
        public string ClosingDateET { get; set; }
        public string ClosingReason { get; set; }
        public string Url { get; set; }
    public Guid? ParentCaseId { get; set; }
    public DateTime? MaxInjuctionDate { get; set; }
    public DateTime? MinInjuctionDate { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }




  }

  public class CaseTrackingVideoDTO
    {
        [Required]
        public Guid CaseDecisionTrackingId { get; set; }
        public string Url { get; set; }
        public IFormFile ActualFile { get; set; }
    }
}
