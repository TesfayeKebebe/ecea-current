using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTORCommon.Ethiopic;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseDocument : BaseEntity
    {
        [Key]
        public Guid CaseDocumentId { get; set; }
        [Required]
        public Guid CAUnitId { get; set; }
        [ForeignKey("CAUnitId")]
        public CAUnit CAUnit { get; set; }
        [Required]
        public string AttachmentContent { get; set; }
        [Required]
        public string FileName { get; set; }
        //[Required]
        public string Url { get; set; }
        //[Required]
        public string FileLocation { get; set; }
        //public byte[] BinaryFile { get; set; }
    }

    public partial class CaseDocumentDTO : BaseEntity
    {
        private DateTime updatedDateTime;
        private DateTime createdDateTime;
        public Guid? CaseDocumentId { get; set; }
        [Required]
        public Guid CAUnitId { get; set; }
        [Required]
        public string AttachmentContent { get; set; }
        [Required]
        public string FileName { get; set; }
        public string Url { get; set; }
        [Required]
        public IFormFile ActualFile { get; set; }
        public string FileLocation { get; set; }

        public string CreatedDateTimeET { get; set; }
        public DateTime CreatedDateTime
        {
            get => createdDateTime;
            set
            {
                createdDateTime = value;
                if (value != null)
                {
                    CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string UpdatedDateTimeET { get; set; }
        public DateTime UpdatedDateTime
        {
            get => updatedDateTime;
            set
            {
                updatedDateTime = value;
                if (value != null)
                {
                    UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        //public byte[] BinaryFile { get; set; }

    }

}
