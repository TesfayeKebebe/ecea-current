using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseFeedback : BaseEntity
    {
        public int CaseFeedbackId { get; set; }
        [Required]
        public Guid CAUnitId { get; set; }
        public CAUnit CAUnit { get; set; }
        [Required]
        public int FeedbackTypeId { get; set; }
        public virtual CaseLookUp FeedbackType { get; set; }
        [Required]
        public string Feedback { get; set; }
        public string Summary { get; set; }
        [Required]
        public string EmployeeId { get; set; }
        public ApplicationUser Employee { get; set; }
        [Required]
        public DateTime FeedbackDate { get; set; }
        public DateTime? DateOfCaseInvestigation { get; set; }
        public DateTime? DateOfProsecution { get; set; }
        public bool? IsProsecution { get; set; }
    }

    public class CaseFeedbackDTO : BaseEntity
    {
        public CaseFeedbackDTO()
        {
            FeedbackDate = DateTime.Now;
            FeedbackDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(FeedbackDate).ToString());
        }
        public string CaseInvestigationNo { get; set; }
        public int CaseFeedbackId { get; set; }
        [Required]
        public int CaseStatusTypeId { get; set; }
        [Required]
        public Guid CAUnitId { get; set; }
        [Required]
        public int FeedbackTypeId { get; set; }
        public LookUpDisplayView FeedbackType { get; set; }
        public string Summary { get; set; }
        public string Feedback { get; set; }

        public string EmployeeId { get; set; }
        public ApplicationUser Employee { get; set; }
        [Required]
        public DateTime FeedbackDate { get; set; }
        public DateTime? DateOfCaseInvestigation { get; set; }
        public DateTime? DateOfProsecution { get; set; } 
        public string FeedbackDateET { get; set; }
        public string FullUserName { get; set; }
        public string FullEmployeeName { get; set; }
        public bool? CurrentLoggedId { get; set; }
        public bool? IsProsecution { get; set; }
    }
    public partial class CaseFeedbackListDTO
    {
        public CaseFeedbackListDTO()
        {
            Feedbacks = new List<CaseFeedbackDTO>();
        }
        public CaseFeedbackDTO CurrentFeedback { get; set; }
        public List<CaseFeedbackDTO> Feedbacks { get; set; }
    }
}
