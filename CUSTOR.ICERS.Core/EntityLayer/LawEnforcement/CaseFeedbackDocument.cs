﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public class CaseFeedbackDocument : BaseEntity
    {
        [Key]
        public string Id { get; set; }
        public string AttachmentContent { get; set; }
        public string FileName { get; set; }
        public int CaseFeedbackId { get; set; }
        public CaseFeedback CaseFeedback { get; set; }
    }
    public class CaseFeedbackDocumentDto : BaseEntity
    { 
        public string Id { get; set; }
        public string AttachmentContent { get; set; }
        public string FileName { get; set; }
        [Required]
        public IFormFile ActualFile { get; set; }
        public int CaseFeedbackId { get; set; } 
        public string FullName { get; set; }

    }
}
