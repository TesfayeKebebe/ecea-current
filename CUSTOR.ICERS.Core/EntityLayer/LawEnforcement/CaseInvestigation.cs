using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseInvestigation : BaseEntity
    {
        [Key]
        public Guid CaseInvestigationId { get; set; }
        [ForeignKey("CaseInvestigationId")]
        public CAUnit CAUnit { get; set; }
        [Required]
        public string FileNo { get; set; }
        [Required]
        public DateTime OpeningDate { get; set; }
        public Guid CaseApplicationId { get; set; }
        [ForeignKey("CaseApplicationId")]
        public CaseApplication CaseApplication { get; set; }
        public string Remark { get; set; }

        public Case Case { get; set; }

    }

    public partial class CaseInvestigationDTO : BaseEntity
    {
        public Guid? CaseInvestigationId { get; set; }
        [Required]
        public string FileNo { get; set; }
        [Required]
        public DateTime OpeningDate { get; set; }
        public string OpeningDateET { get; set; }
        [Required]
        public Guid CaseApplicationId { get; set; }
        public string Remark { get; set; }
    }

    public partial class CaseInvestigationListModel
    {
        public Guid CaseInvestigationId { get; set; }
        public string FileNo { get; set; }
        public DateTime? OpeningDate { get; set; }
        public string OpeningDateET { get; set; }
        public Guid CaseApplicationId { get; set; }
        public Guid SuspectId { get; set; }
        public Guid? PlaintiffId { get; set; }
        public string Remark { get; set; }
        public int CaseTypeId { get; set; }
        public LookUpDisplayView CaseType { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDateET { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string ApplicationDateET { get; set; }
        public string Description { get; set; }
        public LookUpDisplayView Department { get; set; }
        public int CaseStatusTypeId { get; set; }
        public string CaseStatusCode { get; set; }
        public LookUpDisplayView CaseStatusType { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public string SuspectOrganizationName { get; set; }
        public string SuspectOrganizationNameAmh { get; set; }
        public string ExchangeActorOrganizationName { get; set; }
        public string ExchangeActorOrganizationNameEng { get; set; }
        public string SuspectFullName { get; set; }
        public string SuspectFullNameAmh { get; set; }
        public string PlaintiffFullName { get; set; }
        public string PlaintiffFullNameAmh { get; set; }
        public int ApplicationPartyType { set; get; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateET { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedDateET { get; set; }
    }

    public class CaseInvestigationSearchCriteriaModel
    {
        public int? CaseType { get; set; }
        public int? Department { get; set; }
        public int? StatusType { get; set; }
        public string FileNo { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Lang { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
