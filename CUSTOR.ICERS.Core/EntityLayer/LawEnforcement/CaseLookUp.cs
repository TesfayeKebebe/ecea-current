﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseLookUp : BaseEntity
    {
        public int CaseLookUpId { get; set; }
        [Required]
        public string DescriptionAmh { get; set; }
        [Required]
        public string DescriptionEng { get; set; }
        [Required]
        public string TypeCode { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public CaseLookUpType Category { get; set; }
        public int SortOrder { get; set; }

    }

    public partial class CaseLookUpDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string GroupCode { get; set; }
        public string GroupDescription { get; set; }
        public string SubGroupCode { get; set; }
        public string SubGroupDescription { get; set; }

    }

    public partial class CaseLookUpDTO2
    {
        public int CaseLookUpId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string TypeCode { get; set; }
        public int CategoryId { get; set; }
        public int SortOrder { get; set; }
    }
    public class CaseLookupDTO4:BaseEntity
    {
        public int CategoryId { get; set; }
        public int CaseLookUpId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string TypeCode { get; set; }

    }
    public class CaseLookupDTO5
    {
        public int CategoryId { get; set; }
        public int CaseLookUpId { get; set; }
        public string Description { get; set; }

    }
}
