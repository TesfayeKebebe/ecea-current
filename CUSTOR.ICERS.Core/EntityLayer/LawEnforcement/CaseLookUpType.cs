﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseLookUpType : BaseEntity
    {
        public CaseLookUpType()
        {
            CaseLookUp = new HashSet<CaseLookUp>();
        }

        public int CaseLookUpTypeId { get; set; }
        [Required]
        public string DescriptionAmh { get; set; }
        [Required]
        public string DescriptionEng { get; set; }
        [Required]
        public string TypeCode { get; set; }

        public virtual ICollection<CaseLookUp> CaseLookUp { get; set; }
    }
    public class CaseLookupTypeDTO : BaseEntity
    {
        public int CaseLookUpTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string TypeCode { get; set; }
    }
}
