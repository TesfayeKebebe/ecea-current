using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseParty : BaseEntity
    {
        public CaseParty()
        {
            CasePartyInvestigation = new HashSet<CasePartyInvestigation>();
        }
        [Key]
        public Guid CasePartyId { get; set; }
        //case 
        public Guid CAUnitId { get; set; }
        [ForeignKey("CAUnitId")]
        public CAUnit CAUnit { get; set; }
        public int PartyType { get; set; }
        public int PartyCategory { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public int ExchangeActorType { get; set; } //0 for case party 1 for private 2 for organazation 
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public int? GenderId { get; set; }
        public int? Age { get; set; }
        public int? NationalityId { get; set; }
        public Nationality Nationality { get; set; }
        public int? EducationalLevelId { get; set; }
        public Lookup.Lookup EducationalLevel { get; set; }
        public string Occupation { get; set; }
        public int? RegionId { get; set; }
        public Region Region { get; set; }
        public int? ZoneId { get; set; }
        public Zone Zone { get; set; }
        public int? WoredaId { get; set; }
        public Woreda Woreda { get; set; }
        public int? KebeleId { get; set; }
        public Kebele Kebele { get; set; }
        public string HouseNo { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public bool SuspectWitness { get; set; }
        public bool? IsECEA { get; set; }
        public bool? IsECE { get; set; }
        public string Testimony { get; set; }
        public ICollection<CasePartyInvestigation> CasePartyInvestigation { get; set; }
    }

    public class ExchangeActorCasePartyModel
    {
        public Guid? ExchangeActorId { get; set; }
        public string OrganizationName { get; set; }
        public string FullName { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
    }
    public class ExchangeActorCasePartyDTO
    {
        public Guid? ExchangeActorId { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string FullNameAmh { get; set; }
        public string FullNameEng { get; set; }
        public LookUpDisplayView CustomerType { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
    }
    public class CasePartyDTO
    {
        private int? genderId;
        private CasePartyType partyType;
        public Guid CasePartyId { get; set; }
        //public CasePartyType PartyType { get; set; }
        public CasePartyType PartyType
        {
            get => partyType;
            set
            {
                partyType = value;
                if (value == CasePartyType.ExchangeActor)
                {
                    PartyFullNameAmh = ExchangeActor != null ? ExchangeActor.OrganizationNameAmh : " ";
                    PartyFullNameEng = ExchangeActor != null ? ExchangeActor.OrganizationNameEng : " ";
                }
                else
                {
                    PartyFullNameAmh = FirstNameAmh + " " + FatherNameAmh + " " + GrandFatherNameAmh;
                    PartyFullNameEng = FirstNameAmh + " " + FatherNameAmh + " " + GrandFatherNameAmh;
                }
            }
        }
        public CasePartyCategory PartyCategory { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public ExchangeActorCasePartyDTO ExchangeActor { get; set; }
        public string PartyFullNameAmh { get; set; }
        public string PartyFullNameEng { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string GenderAmh { get; set; }
        public string GenderEng { get; set; }
        public int? Age { get; set; }
        public int? NationalityId { get; set; }
        public CountryDisplayView Nationality { get; set; }
        public int? EducationalLevelId { get; set; }
        public LookUpDisplayView EducationalLevel { get; set; }
        public string Occupation { get; set; }
        public int? RegionId { get; set; }
        public LookUpDisplayView Region { get; set; }
        public int? ZoneId { get; set; }
        public LookUpDisplayView Zone { get; set; }
        public int? WoredaId { get; set; }
        public LookUpDisplayView Woreda { get; set; }
        public int? KebeleId { get; set; }
        public LookUpDisplayView Kebele { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public bool? IsECEA { get; set; }
        public bool? IsECE { get; set; }
        public int? GenderId
        {
            get => genderId;
            set
            {
                genderId = value;
                if (value != null)
                {
                    GenderAmh = value == 1 ? "ወንድ" : "ሴት";
                    GenderEng = value == 1 ? "Male" : "Female";
                }
            }
        }
    }

    public class CasePartyEditDTO : BaseEntity
    {
        public CasePartyEditDTO()
        {
            ExchangeActor = new ExchangeActorCasePartyModel();
        }
        public Guid? CasePartyId { get; set; }
        public CasePartyType PartyType { get; set; }
        public CasePartyCategory PartyCategory { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public ExchangeActorCasePartyModel ExchangeActor { get; set; }
        public string FirstNameAmh { get; set; }
        public string FatherNameAmh { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameEng { get; set; } 
        public string GrandFatherNameEng { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public int? GenderId { get; set; }
        public int? Age { get; set; }
        public int? NationalityId { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public bool SuspectWitness { get; set; }
        public bool? IsECEA { get; set; }
        public bool? IsECE { get; set; }
    }
    public class LookUpDisplayView
    {
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionEnglish { get; set; }
        public int StateCategoryId { get; set; }
    }
    public class CountryDisplayView
    {
        public string CountryNameAmh { get; set; }
        public string CountryNameEng { get; set; }
    }
    public class CasePartyAppointment
    {
        public string Name { get; set; }
        public Guid CasePartyId { get; set; }
        public Guid CAUnitId { get; set; }
    public string Category { get; set; }
    public int PartyCategory { get; set; }
  }

}
