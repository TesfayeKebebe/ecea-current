using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public class CasePartyDocument : BaseEntity
    {
        [Key]
        public string Id { get; set; }
        public string AttachmentContent { get; set; }
        public string FileName { get; set; }
        public Guid CasePartyId { get; set; }
        public CaseParty CaseParty { get; set; }
    }
    public class CasePartyDocumentDto : BaseEntity
    {
        public string Id { get; set; }
        public string AttachmentContent { get; set; }
        public string FileName { get; set; }
        [Required]
        public IFormFile ActualFile { get; set; }
        public Guid CasePartyId { get; set; }
        public string FullName { get; set; }

    }

}
