using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public class CasePartyInvestigation : BaseEntity
    {
        public int CasePartyInvestigationId { get; set; }
        public Guid CasePartyId { get; set; }
        public CaseParty CaseParty { get; set; }
        public string InvestigatorId { get; set; }
        public ApplicationUser Investigator { get; set; }

        public string Investigation { get; set; }
        public string AmhInvestigation { get; set; }
        public string AmhSuggestion { get; set; }
        public string Suggestion { get; set; }

        [Required]
        public DateTime InvestigationDate { get; set; }
        [Required]
        public int InvestigationTime { get; set; }
    }

    public class CasePartyInvestigationDTO : BaseEntity
    {
        private DateTime updatedDateTime;
        private DateTime createdDateTime;
        private DateTime investigationDate;
        private SampleEmployeeDTO investigator;
        public int CasePartyInvestigationId { get; set; }
        public Guid CasePartyId { get; set; }
        public int InvestigatorId { get; set; }
        public string InvestigatorNameAmh { get; set; }
        public string InvestigatorNameEng { get; set; }
        public string AmhInvestigation { get; set; }
        public string AmhSuggestion { get; set; }
        public SampleEmployeeDTO Investigator
        {
            get => investigator;
            set
            {
                investigator = value;
                if (value != null)
                {
                    InvestigatorNameAmh = investigator.FirstNameAmh + " " + investigator.FatherNameAmh + " " + investigator.GrandFatherNameAmh;
                    InvestigatorNameEng = investigator.FirstNameEng + " " + investigator.FatherNameEng + " " + investigator.GrandFatherNameEng;
                }
            }
        }
        public string Investigation { get; set; }
        public string Suggestion { get; set; }
        public string InvestigationDateET { get; set; }
        public int InvestigationTime { get; set; }
        public string CreatedDateTimeET { get; set; }
        public string UpdatedDateTimeET { get; set; }
        public DateTime CreatedDateTime
        {
            get => createdDateTime;
            set
            {
                createdDateTime = value;
                if (value != null)
                {
                    CreatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public DateTime UpdatedDateTime
        {
            get => updatedDateTime;
            set
            {
                updatedDateTime = value;
                if (value != null)
                {
                    UpdatedDateTimeET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public DateTime InvestigationDate
        {
            get => investigationDate;
            set
            {
                investigationDate = value;
                if (value != null)
                {
                    InvestigationDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }

    }
    public class CasePartyInvestigationListModel : CasePartyDTO
    {
        public List<CasePartyInvestigationDTO> CasePartyInvestigation { get; set; }
    }
}
