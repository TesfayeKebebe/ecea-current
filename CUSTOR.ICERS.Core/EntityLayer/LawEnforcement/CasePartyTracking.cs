using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CasePartyTracking : BaseEntity
    {
        [Key]
        public Guid CasePartyTrackingId { get; set; }
        public Guid CasePartyId { get; set; }
        public Guid CaseDecisionTrackingId { get; set; }
        public string Remark { get; set; }
        public string AmhRemark { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public string Url { get; set; }
    }

    public class CasePartyTrackingDTO : BaseEntity
    {
        public Guid CasePartyTrackingId { get; set; }
        public Guid CasePartyId { get; set; }
        public Guid CaseDecisionTrackingId { get; set; }
        public string Remark { get; set; }
        public string AmhRemark { get; set; }
    }
}
