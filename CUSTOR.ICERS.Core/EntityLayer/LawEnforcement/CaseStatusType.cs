using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseStatusType
    {
        [Key]
        public int CaseStatusTypeId { get; set; }
        public int CategoryId { get; set; }
        public int? RoleTypeId { get; set; }
        [Required]
        public string DescriptionAmh { get; set; }
        [Required]
        public string DescriptionEng { get; set; }
        [Required]
        public string OptionDescriptionAmh { get; set; }
        [Required]
        public string OptionDescriptionEng { get; set; }
        [Required]
        public string TypeCode { get; set; }
        public int? RequiredFeedbackTypeId { get; set; }
        public int StateCategoryId { get; set; }

        public virtual CaseLookUp StateCategory { get; set; }
        public virtual CaseLookUp Category { get; set; }
        //public virtual User RoleType { get; set; }
        public virtual CaseLookUp RequiredFeedbackType { get; set; }
    }
    public partial class CaseStatusTransition
    {
        [Key]
        public int CaseStatusTransitionId { get; set; }
        public int ParentStatusTypeId { get; set; }
        [ForeignKey("ParentStatusTypeId")]
        public CaseStatusType ParentStatusType { get; set; }
        public int ChildStatusTypeId { get; set; }
        [ForeignKey("ChildStatusTypeId")]
        public CaseStatusType ChildStatusType { get; set; }
    }
}
