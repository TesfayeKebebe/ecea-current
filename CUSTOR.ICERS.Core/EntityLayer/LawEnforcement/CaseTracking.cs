using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.Security;
using CUSTORCommon.Ethiopic;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class CaseTracking : BaseEntity
    {
        [Key]
        public Guid CaseTrackingId { get; set; }
        [ForeignKey("CaseTrackingId")]
        public CAUnit CAUnit { get; set; }

        public Guid CaseId { get; set; }
        [ForeignKey("CaseId")]
        public Case Case { get; set; }
        public DateTime DecisionDate { get; set; }
        public string Remark { get; set; }
        public string FirstJudgeId { get; set; }
        public string SecondJudgeId { get; set; }
        public string ThirdJudgeId { get; set; }
        [Required]
        public DateTime AppointmentDate { get; set; }
        [Required]
        public int AppointmentTime { get; set; }
        [Required]
        public int AppointmentTypeId { get; set; }
        [Required]
        public string AppointmentReason { get; set; }
        public Lookup.Lookup AppointmentType { get; set; }

        public virtual ApplicationUser FirstJudge { get; set; }
        public virtual ApplicationUser SecondJudge { get; set; }
        public virtual ApplicationUser ThirdJudge { get; set; }
    }

    public class CaseAppointmentDTO
    {
        public Guid? CaseTrackingId { get; set; }
        [Required]
        public Guid CaseId { get; set; }
        [Required]
        public int AppointmentTime { get; set; }
        [Required]
        public int AppointmentTypeId { get; set; }
        [Required]
        public string AppointmentReason { get; set; }
        public LookUpDisplayView AppointmentType { get; set; }

        private DateTime appointmentDate;
        [Required]
        public DateTime AppointmentDate
        {
            get => appointmentDate;
            set
            {
                appointmentDate = value;
                if (value != null)
                {
                    AppointmentDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string AppointmentDateET { get; set; }
        public Guid CaseInvestigationId { get; set; }
        public string InvestigationFileNo { get; set; }
        public string FileNo { get; set; }
        public string CaseName { get; set; }
        public string OpeningDateET { get; set; }
        private DateTime? openingDate;
        public DateTime? OpeningDate
        {
            get => openingDate;
            set
            {
                openingDate = value;
                if (value != null)
                {
                    OpeningDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value.Value).ToString());
                }
            }
        }

        public int CaseTypeId { get; set; }
        public LookUpDisplayView CaseType { get; set; }
    }
    public class CaseCourtSessionDTO
    {
        [Required]
        public Guid CaseTrackingId { get; set; }
        [Required]
        public Guid CaseId { get; set; }

        private DateTime decisionDate;
        [Required]
        public DateTime DecisionDate
        {
            get => decisionDate;
            set
            {
                decisionDate = value;
                if (value != null)
                {
                    DecisionDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string DecisionDateET { get; set; }
        [Required]
        public bool IsClosed { get; set; }
        [Required]
        public string Remark { get; set; }
        [Required]
        public int FirstJudgeId { get; set; }
        [Required]
        public int SecondJudgeId { get; set; }
        [Required]
        public int ThirdJudgeId { get; set; }
        public SampleEmployeeDTO FirstJudge { get; set; }
        public SampleEmployeeDTO SecondJudge { get; set; }
        public SampleEmployeeDTO ThirdJudge { get; set; }
    }
    public class CaseTrackingDTO
    {
        [Required]
        public Guid CaseDecisionTrackingId { get; set; }
        public string CaseTrackingId { get; set; }
        [Required]
        public Guid AppointmentId { get; set; }
        [Required]
        public Guid CaseId { get; set; }
        private DateTime decisionDate;
        [Required]
        public DateTime DecisionDate
        {
            get => decisionDate;
            set
            {
                decisionDate = value;
                if (value != null)
                {
                    DecisionDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string DecisionDateET { get; set; }
        public bool IsAppointment { get; set; }
        public bool IsClosed { get; set; }
        public string Remark { get; set; }
        public string AmhRemark { get; set; }
        public string FirstJudgeId { get; set; }
        public string SecondJudgeId { get; set; }
        public string ThirdJudgeId { get; set; }
        [Required]
        public TimeSpan AppointmentTime { get; set; }
        [Required]
        public int AppointmentTypeId { get; set; }
        [Required]
        public string AppointmentReason { get; set; }
        public LookUpDisplayView AppointmentType { get; set; }

        private DateTime appointmentDate;
        [Required]
        public DateTime AppointmentDate
        {
            get => appointmentDate;
            set
            {
                appointmentDate = value;
                if (value != null)
                {
                    AppointmentDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value).ToString());
                }
            }
        }
        public string AppointmentDateET { get; set; }
        public Guid CaseInvestigationId { get; set; }
        public string InvestigationFileNo { get; set; }
        public string FileNo { get; set; }
        public string CaseName { get; set; }
        public string AmhCaseName { get; set; }
        public string OpeningDateET { get; set; }
        private DateTime? openingDate;
        public DateTime? OpeningDate
        {
            get => openingDate;
            set
            {
                openingDate = value;
                if (value != null)
                {
                    OpeningDateET = EthiopicDateTime.TranslateEthiopicDateMonth(EthiopicDateTime.GetEthiopicDate(value.Value).ToString());
                }
            }
        }
        public string OpeningReason { get; set; }

        public int? DecisionTypeId { get; set; }
        public LookUpDisplayView DecisionType { get; set; }
        public string DecisionRemark { get; set; }
        public Guid? DecisionCourtSessionId { get; set; }
        public DateTime? ClosingDate { get; set; }
        public string ClosingDateET { get; set; }
        public string ClosingReason { get; set; }

        public int CaseTypeId { get; set; }
        public LookUpDisplayView CaseType { get; set; }

        public string FirstJudge { get; set; }
        public string SecondJudge { get; set; }
        public string ThirdJudge { get; set; }
        public bool? IsActive { get; set; }
        public string Url { get; set; }
        public Guid? CaseApplicationId { get; set; }
    public string Location { get; set; }
    public string AmhAppointmentReason { get; set; }
       public Guid? ParentCaseId { get; set; }
    public Guid? ExchangeActorId { get; set; }
    public string EngOtherRemark { get; set; }
    public string AmhOtherRemark { get; set; }
    public string EngAppointmentOtherRemark { get; set; }
    public string AmhAppointmentOtherRemark { get; set; }
  }

    public class CaseTrackingListDTO
    {
        public Guid CaseTrackingId { get; set; }
        public Guid CaseId { get; set; }
        public DateTime DecisionDate { get; set; }
        public string DecisionDateET { get; set; }
        public bool IsAppointment { get; set; }
        public bool IsClosed { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string AppointmentDateET { get; set; }
        public int? AppointmentTime { get; set; }
        public int? AppointmentTypeId { get; set; }
        public LookUpDisplayView AppointmentType { get; set; }
        public Guid CaseInvestigationId { get; set; }
        public string InvestigationFileNo { get; set; }
        public string FileNo { get; set; }
        public string CaseName { get; set; }
        public DateTime? OpeningDate { get; set; }
        public string OpeningDateET { get; set; }
        public int CaseTypeId { get; set; }
        public LookUpDisplayView CaseType { get; set; }
    }
    public class CaseTrackingSearchCriteriaModel
    {
        public string FileNo { get; set; }
        public string CaseName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Lang { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
