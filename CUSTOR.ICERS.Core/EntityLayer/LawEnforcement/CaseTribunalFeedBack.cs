using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public class CaseTribunalFeedBack : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string FeedBack { get; set; }
        public int? CaseLookUpId { get; set; }
        public Guid CaseInvestigationId { get; set; }
        public CaseLookUp Lookup { get; set; }
        public CaseInvestigation CaseInvestigation { get; set; }
    }
    public class CaseTribunalFeedBackDto : BaseEntity
    {
        public int Id { get; set; }
        public string FeedBack { get; set; }
        public int? CaseLookUpId { get; set; }
        public Guid CaseInvestigationId { get; set; }

    }
    public class CaseTribunalFeedBackDetail : BaseEntity
    {
        public int Id { get; set; }
        public string FeedBack { get; set; }
        public int? CaseLookUpId { get; set; }
        public Guid CaseInvestigationId { get; set; }
        public string FeedBackType { get; set; }
        public string FeedBackTypeEng { get; set; }
        public string CreatedDate { get; set; }
       public string FullName { get; set; }
    }
    public class CaseLookUpDropDown
    {
        public int CaseLookUpId { get; set; }
        public string FeedBackType { get; set; }

    }


}
