using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class SampleRole
    {
        public SampleRole()
        {
            SampleUserRole = new HashSet<SampleUserRole>();
        }

        public int SampleRoleId { get; set; }
        public string RoleName { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }

        public virtual ICollection<SampleUserRole> SampleUserRole { get; set; }
    }

    public class SampleRoleDTO
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}
