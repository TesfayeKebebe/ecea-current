using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class SampleUser
    {
        public SampleUser()
        {
            SampleUserRole = new HashSet<SampleUserRole>();
        }

        public int SampleUserId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FatherNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public int DepartmentId { get; set; }

        public virtual Lookup.Lookup Department { get; set; }
        public virtual ICollection<SampleUserRole> SampleUserRole { get; set; }
    }
    public class SampleUserDTO
    {
        public SampleUserDTO()
        {
            Roles = new HashSet<SampleRoleDTO>();
        }

        public string UserId { get; set; }
        public string DisplayName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public virtual ICollection<SampleRoleDTO> Roles { get; set; }
    }

    public class SampleEmployeeDTO
    {
        public int SampleUserId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FatherNameAmh { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }

        private string fullNameEng;
        public string FullNameEng
        {
            get { return fullNameEng; }
            private set { fullNameEng = FirstNameEng + " " + FatherNameEng + " " + GrandFatherNameEng; }
        }

        private string fullNameAmh;
        public string FullNameAmh
        {
            get { return fullNameAmh; }
            private set { fullNameAmh = FirstNameAmh + " " + FatherNameAmh + " " + GrandFatherNameAmh; }
        }

    }
}
