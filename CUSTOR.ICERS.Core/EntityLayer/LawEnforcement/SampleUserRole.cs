namespace CUSTOR.ICERS.Core.EntityLayer.LawEnforcement
{
    public partial class SampleUserRole
    {
        public int SampleUserRoleId { get; set; }
        public int SampleUserId { get; set; }
        public int SampleRoleId { get; set; }

        public virtual SampleRole SampleRole { get; set; }
        public virtual SampleUser SampleUser { get; set; }
    }
}
