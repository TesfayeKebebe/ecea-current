namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class LetterContent
    {
        public string BodyHeader { get; set; }
        public string Category { get; set; }
    }
}
