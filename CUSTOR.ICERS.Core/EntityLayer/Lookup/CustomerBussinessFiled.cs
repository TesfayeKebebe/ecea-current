﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Lookup
{
    public class CustomerBussinessFiled: BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerBusinessFildeId { get; set; }
        public int BusinessFiledId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
    }

    public class CustomerBusinessFiledDto
    {
        public int BusinessFiledId { get; set; }
        // public int ServiceApplicationId { get; set; }
    }
    public class CustomerBusinessFiledNameVM
    {
        public string BusinessFiled { get; set; }
    }
}
