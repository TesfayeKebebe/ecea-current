﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Lookup
{
    public partial class Lookup : BaseEntity
    {
        [Key]
        public int LookupId { get; set; }
        public string DescriptionEng { get; set; }
        [Required]
        public string DescriptionAmh { get; set; }
        public int LookupTypeId { get; set; }
        public LookupType LookupType { get; set; }
    }

    public class LookupDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class LookupDTO2
    {
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }

    public class LookupDTO4:BaseEntity
    {
        public int LookupTypeId { get; set; }
        public int LookupId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }

    public class LookupDTO3
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
