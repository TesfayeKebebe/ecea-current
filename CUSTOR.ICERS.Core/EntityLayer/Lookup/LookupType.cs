﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Lookup
{
    public partial class LookupType : BaseEntity
    {
        public int LookupTypeId { get; set; }
        public string DescriptionEng { get; set; }
        [Required]
        public string DescriptionAmh { get; set; }
        public ICollection<Lookup> Lookup { get; set; }
    }

    public class LookupTypeDTO:BaseEntity
    {
        public int LookupTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }
}
