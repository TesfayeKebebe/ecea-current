﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Lookup
{
    public class MemberRepresentativeType : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberRepresentativeTypeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
        public int RepresentativeTypeId { get; set; }
        [ForeignKey("RepresentativeTypeId")]
        public Lookup ReprsentativeType { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }

    }

    public class MemberRepresentativeTypeDTO
    {
        public int RepresentativeTypeId { get; set; }
    }

    public class MemberRepresentativeTypeListDTO
    {
        public List<MemberRepresentativeType> MemberRepresentativeType { get; set; }
    }
}
