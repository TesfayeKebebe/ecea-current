﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Lookup
{
    public partial class Nationality : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CountryId { get; set; }
        public string CountryNameAmh { get; set; }
        public string CountryNameEng { get; set; }
    }

    public partial class NationalityDTO
    {
        public NationalityDTO()
        {
        }

        public int Id { get; set; }
        public string Description { get; set; }
    }

}
