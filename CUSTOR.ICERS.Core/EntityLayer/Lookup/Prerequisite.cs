﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class Prerequisite : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrerequisiteId { get; set; }
        [MaxLength(250)]
        public string DescriptionAmh { get; set; }
        [MaxLength(250)]
        public string DescriptionEng { get; set; }
    }

    public partial class PrerequisiteDTO
    {

        public int PrerequisiteId { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public bool IsRequired { get; set; }
    }
}
