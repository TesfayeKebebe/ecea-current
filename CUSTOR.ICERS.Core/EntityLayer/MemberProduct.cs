﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class MemberProduct : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberProductId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
        public int CommodityId { get; set; }
    }

    public class MemberProductDTO
    {
        public int CommodityId { get; set; }
    }

    public class ProducNameVM
    {
        public string Name { get; set; }
    }
}
