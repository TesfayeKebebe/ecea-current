﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class Notification : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NotificationId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

    }

}
