﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class NotificationViewModel
    {
        public Guid ServiceApplicationId { get; set; }
        public int ServiceTypeId { get; set; }
        public string PreparedDate { get; set; }
        public string StartDate { get; set; }
        public int ApprovalStatus { get; set; }
        public int CustomerTypeId { get; set; }
        public string OrganizationName { get; set; }
        public string MangagerName { get; set; }

    }
    public class MemberClientTradeViewModel
    {
        public int MemberClientTradeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string OrganizationName { get; set; }
        public string ReportDate { get; set; }
        public string ReportType { get; set; }
        public string ReportPeriod { get; set; }
        public int ApprovalStatus { get; set; }
        
    }
    public class OffSiteMonitoringReportNotification
    {
        public int OffSiteMonitoringReportId { get; set; }
        public string ReportType { get; set; }
        public string ReportPeriod { get; set; }
        public int ApprovalStatus { get; set; }
    }
    public class BankTransActionNotification
    {
        public string SiteCode { get; set; }
        public string ReportDate { get; set; }
        public int ApprovalStatus { get; set; }
    }

    public class RecognitionExpirNotificationViewModel
    {

        public string ExchangeActorId { get; set; }
        public string EcxCode { get; set; }
        public string FullName { get; set; }
        public string CustomerType { get; set; }
        public string OrganizationName { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
        public List<RecognitionExpirNotificationViewModel> RepresentativeList { get; set; }
    }
    public class MemberFinancialAuditorNotificationView
    {
        public Guid ExchangeActorId { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public string AuditorId { get; set; }
        public string OrganizationName { get; set; }
        public string DateReport { get; set; }
        public string Remark { get; set; }
        public string AuditorName { get; set; }
        public string AnnualAuditStatus { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public int ApprovalStatus { get; set; }
    }
}
