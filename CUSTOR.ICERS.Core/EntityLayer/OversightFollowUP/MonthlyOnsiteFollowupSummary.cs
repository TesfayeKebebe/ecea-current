﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP
{
    public class MonthlyOnsiteFollowupSummary : BaseEntity
    {
        [Key]
        public int ReportId { get; set; }
        public int RptCriteriaId { get; set; }
        public int RptMonth { get; set; }
        public int RptYear { get; set; }
        public int RptFollowUPType { get; set; }
        public int RptStatus { get; set; }
        public int RptFinalDecision { get; set; }


        public Guid RptSubmitedBy { get; set; }
        public DateTime RptSubmitedDate { get; set; }
        public string RptProblemsFaced { get; set; }
        public string RptMeasurementsTaken { get; set; }
        public string RptGeneralDescription { get; set; }
        public string RptDepartmentDecision { get; set; }
        public string RptLeaderDecision { get; set; }


    }
    public partial class FollowupSummaryViewModel
    {
        public int FollowUpId { get; set; }
        public string DateOfInspection { get; set; }
        public string LastRecord { get; set; }
        public string TeamLeaderDecision { get; set; }
        public string TeamDecision { get; set; }
        public string DepartmentDecision { get; set; }
        public string ExchangeActor { get; set; }
    }
    public partial class SendForApprovalModel
    {

        public int RptMonth { get; set; }
        public int RptYear { get; set; }
        public int RptStatus { get; set; }
        public string TeamDecision { get; set; }


    }
    public partial class ApprovalModel
    {
        public int Decision { get; set; }
        public int FollowUpId { get; set; }
        public int Role { get; set; }

    }
    public partial class FilterCriteria{
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public string Language { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int followupType { get; set; }


    }
    public partial class ComparisonData
    {
        public int TotalEvaluationCount { get; set; }
        public int orallWarned { get; set; }
        public int GivenwrittenWarning { get; set; }
        public int SentForLawEnforcement { get; set; }
        public int RecordedAsGoAhead { get; set; }
        public string FilterType { get; set; }



    }
    public partial class OversightComparisonData
    {
        public ComparisonData FirstCase { get; set; }
        public ComparisonData SecondCase { get; set; }
      

    }
    public partial class MonthlySummaryModel
    {
        public int ReportId { get; set; }
        public string RptCriteriaId { get; set; }
        public int RptMonth { get; set; }
        public int RptYear { get; set; }
        public string RptFollowUPType { get; set; }
        public int RptStatus { get; set; }
        public int RptFinalDecision { get; set; }


        public string RptSubmitedBy { get; set; }
        public DateTime RptSubmitedDate { get; set; }
        public string RptProblemsFaced { get; set; }
        public string RptMeasurementsTaken { get; set; }
        public string RptGeneralDescription { get; set; }
        public string RptDepartmentDecision { get; set; }
        public string RptLeaderDecision { get; set; }

    }

}

