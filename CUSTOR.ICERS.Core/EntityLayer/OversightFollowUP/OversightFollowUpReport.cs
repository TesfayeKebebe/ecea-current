﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp
{
    public class OversightFollowUpReport : BaseEntity
    {
        [Key]
        public int FollowUpId { get; set; }
        [Required]
        public string Synopsis { get; set; }
        public string Remark { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int FollowUpTypeId { get; set; }
        public Guid ReportedBy { get; set; }
        public int TeamDecision { get; set; }
        public int? DepartmentDecison { get; set; }
        public int? TeamLeaderDecision { get; set; }
        public DateTime DateOfInspection { get; set; }
        public int NoOfEvaluators { get; set; }
        public bool? Currentstatus { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public string InfoProvidersName { get;  set; }
        public string EvaluationTeamMembers { get;  set; }
        public int InfoProvidersCount { get;  set; }
        public bool? OfficeClosed { get; set; }

    }

    

    public partial class OversightReportViewModel
    {
        public int FollowUpId { get; set; }

        public string ExchangeActorId { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public string DateOfInspection { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
      
        public string TeamDecision { get; set; }
        public string Synopsis { get; set; }
        public bool? Currentstatus { get; set; }

        
    }
    public partial class OversightReportDTO
    {
        public int FollowUpId { get; set; }
        [Required]
        public Guid ExchangeActorId { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public DateTime DateOfInspection { get; set; }
        public int TeamDecision { get; set; }
        public int DetailsWeightPerCritiera { get; set; }

        public string Synopsis { get; set; }
        public bool? Currentstatus { get; set; }

        // Evaluation Details
        public int OversightFollowUpDetaiId { get; set; }
        public int DetailsCriteriaId { get; set; }
        public string DetailsGivenSuggestion { get; set; }
        public string DetailInvestigation { get; set; }
        public string DetailsRemark { get; set; }
        public string DetailDecision { get; set; }
        public int? DetalisFollowUpId { get; set; }
        public string Investigation { get; set; }
        public int FollowUpTypeId { get; set; }

    }
    public partial class ReportStatusResult
    {
        public int FollowUpId { get; set; }
        public int OvRDetailId { get; set; }
        public bool Currentstatus { get; set; }
    }
    

          public partial class EvaluationsByDate
    {
        public EvaluationsByDate()
        {

        }
        public int FollowUpCount { get; set; }
        public string Date { get; set; }
    }
    public partial class OversightReportDecisionRecord
    {
        public OversightReportDecisionRecord()
        {

        }
        public string DecisionDescription { get; set; }
        public int DecisionId { get; set; }
        public string LastDateOfInspection { get; set; }

    }
    public partial class ReportSearchResult
    {
        public int FollowUpId { get; set; }

        public int OvRDetailId { get; set; }
        public bool Currentstatus { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public string DateOfInspection { get; set; }
    }

    public partial class OversighrReportDetailDTO : BaseEntity
    {
        public int OvRDetailId { get; set; }
        public int CriteriaId { get; set; }
        public string Investigation { get; set; }
        public string GivenSuggestion { get; set; }
        public string Decision { get; set; }
        public string Remark { get; set; }
        public int FollowUpId { get; set; }


    }
    public partial class OversightReportMasterData
    {
        public int FollowUpId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public string DateOfInspection { get; set; }

        public int? Weight { get; set; }
      
        public string TeamDecision { get; set; }
        public string TeamLeaderDecision { get; set; }
        public string DepartmentDecison { get; set; }

        public string Synopsis { get; set; }
        public bool? Currentstatus { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual Oversight_report_criteria OversightReportCriteria { get; set; }


    }
    public partial class OversightReportMasterDataViewModel
    {
        public OversightReportMasterDataViewModel()
        {

        }

        public int FollowUpId { get; set; }
        public string ExchangeActorId { get; set; }
        public String ReportedBy { get; set; }
        public string Remark { get; set; }
        public string DateOfInspection { get; set; }
 
        public string TeamDecision { get; set; }
        public string Synopsis { get; set; }
        public bool? Currentstatus { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public float? Weight { get; set; }

    }
    public partial class OversighrReportMasterDataViewModel
    {
        [Key]
        public int FollowUpId { get; set; }
        [Required]
        public string ExchangeActorId { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public string DateOfInspection { get; set; }
        public string TeamDecision { get; set; }
        public string Synopsis { get; set; }
        public bool? Currentstatus { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        

    }
    public partial class OversighrReportData
    {
        public virtual OversightReportDetail OversightReportDetail { get; set; }
        public virtual OversightReportDTO OversightReportDTO { get; set; }
    }
    public partial class CompletedoversitReportData
    {
        public int FollowUpId { get; set; }
        public string Synopsis { get; set; }
        public Guid ReportedBy { get; set; }
        public string Remark { get; set; }
        public bool Status { get; set; }
        public int NoOfEvaluators { get; set; }
        public int FinalDecision { get; set; }
        public int InfoProvidersCount { get;  set; }
        public string EvaluationTeamMembers { get;  set; }
        public string InfoProvidersName { get;  set; }
      
    }
    public partial class OfficeClosedData
    {
        public int FollowUpId { get; set; }
        public string Synopsis { get; set; }
        public Guid ExchangeActorId { get; set; }
        public Guid ReportedBy { get; set; }
        public DateTime DateOfInspection { get; set; }
        public string Remark { get; set; }
        public bool Status { get; set; }
        public int NoOfEvaluators { get; set; }
        public int FinalDecision { get; set; }
        public int InfoProvidersCount { get; set; }
        public string EvaluationTeamMembers { get; set; }
        public string InfoProvidersName { get; set; }
        public int FollowUpTypeId { get; set; }
    }
    public partial class ReportDetaitPerCriteriaDTO
    {
        public int OvRDetailId { get; set; }
        public int CriteriaId { get; set; }
        public string Investigation { get; set; }
        public string GivenSuggestion { get; set; }
        public string Remark { get; set; }
        public int WeightPerCriteria { get; set; }
        public string CriteriaName { get; set; }
    }
    public partial class OversightDashbordDataByDecision
    {
        public string Decision { get; set; }
        public int DecisionCount { get; set; }
    }


    public partial class OversightReportDashboardDataViewModel
    {
        public OversightReportDashboardDataViewModel()
        {

        }
        public int CompletedEvaluations { get; set; }
        public int DaysOnMission { get; set; }
        public int WarningsGiven { get; set; }
        public int ExchangeActors { get; set; }
        public int ExchangeActorsEvaluated { get; set; }
        public int Totalcriteria { get; set; }
    }
    public partial class OversightReportMOnthlySummaryModel
    {
        public string EvaluationCriteria { get; set; }
        public int? UnderPerformingCount { get; set; }
        public int? AbovePerformingCount { get; set; } 


    }

    public partial class ComparisionByCheckListModel
    {
        public int FirstYear { get; set; }
        public int SecondYear { get; set; }
        public string Criteria { get; set; }
        public List<ComparisionCheckListModel> checkListModel { get; set; }


    }
    public partial class ComparisionCheckListModel
    {
        public string CheckListName { get; set; }
        public int AbovePerForming { get; set; }
        public int BelowPerforming { get; set; }
    }

    public partial class OversiteFollowUpFinalDecisionModel
    {
        public int NoOfEvaluators { get; set; }
        public int FinalDecision { get; set; }
        public int InfoProvidersCount { get; set; }
        public string EvaluationTeamMembers { get; set; }
        public string InfoProvidersName { get; set; }
        public string Synopsis { get; set; }
        public string Remark { get; set; }


    }

}
