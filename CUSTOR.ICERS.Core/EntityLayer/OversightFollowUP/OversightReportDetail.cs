﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP
{
    public class OversightReportDetail : BaseEntity
    {
        [Key]
        public int OversightFollowUpDetaiId { get; set; }
        public int CriteriaId { get; set; }
        public string GivenSuggesiton { get; set; }
        public string Remark { get; set; }
        public int WeightPerCriteria { get; set; }
        public string Investigation { get; set; }
        public int FollowUpId { get; set; }
        public virtual Oversight_report_criteria Oversight_Report_Criteria { get; set; }
    }
    public class OversightReportDetailViewModel
    {
        public int OversightFollowUpDetaiId { get; set; }
        public string CriteriaId { get; set; }
        public string GivenSuggesiton { get; set; }
        public string Remark { get; set; }
        public string score { get; set; }
        public string Investigation { get; set; }
        public int FollowUpId { get; set; }
    }

}
