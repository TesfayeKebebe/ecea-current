using System;
namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp
{
    public partial class OversightReportSearchCriteria
    {
     
        public Guid ExchangeActorId { get; set; }
        public DateTime DateOfInspection { get; set; }
        public string Lang { get; set; }
        public int? FollowUpTypeId { get; set; }
    }
    public partial class OversightDetailReportSearchCriteria
    {
        public int FollowupId { get; set; }
        public int CriteriaId { get; set; }
    }
    public partial class OvsersightReportGeneralSearchCriteria
    {
        public Guid ExchangeActorId { get; set; }
        public DateTime DateOfInspection { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool? FollowupStatus { get; set; }
        public int FollowUpType { get; set; }
        public int FinalDecision { get; set; }
        public string Language { get; set; }

        public int PageCount { get; set; }
        public int PageNumber { get; set; }

    }
}
