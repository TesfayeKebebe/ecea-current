﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP
{
    public class OversightReportSetting : BaseEntity
    {
        [Key]
        public int DecisionId { get; set; }
        public string DecisionNameAmh { get; set; }
        public string DecisionNameEng { get; set; }
        public string DecisionDescriptionAmh { get; set; }
        public string DecisionDescriptionEng { get; set; }
    }
    public partial class OversightReportSettingDTO
    {
        public int DecisionId { get; set; }
        public string DecisionNameAmh { get; set; }
        public string DecisionNameEng { get; set; }
        public string DecisionDescriptionAmh { get; set; }
        public string DecisionDescriptionEng { get; set; }
    }
}
