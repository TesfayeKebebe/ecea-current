﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP
{

    public class OversightSchedule: BaseEntity
    {
        [Key]
        public int ScheduleId { get; set; }
        public string ScheduleDate { get; set; }
        public int ScheduleYear { get; set; }
        public int ScheduleMonth { get; set; }
        public string ExchangeActor { get; set; }
        public string Address { get; set; }
        public string Ecxcode { get; set; }
        public Guid? ExchangeActorId { get; set; }
    }
   
    public partial class PaginateModel
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public string Language { get; set; }
    }
    public partial class SearchScheduleModel
    {
        public int ScheduleYear { get; set; }
        public int ScheduleMonth { get; set; }
        public int PageCount { get;  set; }
        public int PageNumber { get;  set; }
    }
    public partial class SearchScheduleByMonthsModel
    {
        public int ScheduleYear { get; set; }
        public int ScheduleMonth { get; set; }
        public int scheduleCount { get; set; }
    }

}
