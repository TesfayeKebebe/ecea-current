﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP
{
    [Table("oversight_report_criteria")]
    public partial class Oversight_report_criteria : BaseEntity
    {
        public Oversight_report_criteria()
        {
        }
        [Key]
        public int CriteriaId { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public string CriteriaNameEng { get; set; }
        public string CriteriaNameAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public int? Weight { get; set; }
        public int CriteriaTypeId { get; set; }
    }
    public partial class Oversight_report_criteriaDTO
    {
        public int CriteriaId { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public string CriteriaNameEng { get; set; }
        public string CriteriaNameAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public int? Weight { get; set; }
        public Guid CreatedUserId { get; set; }
        public string ExchangeActorDescriptionEng { get; set; }
        public string ExchangeActorDescriptionAmh { get; set; }
        public int CriteriaType { get; set; }


    }
    public partial class Oversight_report_criteria_viewModel
    {
        public Oversight_report_criteria_viewModel()
        {

        }
        [Key]
        public int CriteriaId { get; set; }
        public String ExchangeActorTypeId { get; set; }
        public string CriteriaName { get; set; }
        public string Description { get; set; }
        public int? Weight { get; set; }
        public string CriteriaType { get; set; }

    }

}