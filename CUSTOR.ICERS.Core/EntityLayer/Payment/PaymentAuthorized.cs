﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Payment
{
    public class PaymentAuthorized : BaseEntity
    {
        public int PaymentAuthorizedId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public bool IsPaymentAuthorized { get; set; }
        public bool IsPaid { get; set; }

        public virtual ServiceApplication ServiceApplication { get; set; }
    }

    public partial class PaymentAuthorizedDTO
    {
        public Guid ServiceApplicationId { get; set; }
        public bool IsPaymentAuthorized { get; set; }
        public bool IsPaid { get; set; }
    }

    public class AuthorizedPaymentOrderSearchDTO
    {
        public int ServiceId { get; set; }
        public string OrganizationName { get; set; }
        public string ServiceType { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public bool IsPaid { get; set; }
        public string AuthorizedDate { get; set; }
        public Guid ExchangeActorId { get; set; }

        public string ExchangeActorType {get; set;}
        public int CustomerTypeId { get; set; }
    }

    public partial class PaymentAuthorizationStatusDTO
    {
        public bool DisableNextButton { get; set; }
    }

    public class PaymentQueryParameters
    {
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }

        // For srearch
        //public string LegalBodyName { get; set; }
        //public string LegalBodyFatherName { get; set; }
        //public string LegalBodyGrandFatherName { get; set; }
        public string OrganizationName { get; set; }
        public int? ServiceType { get; set; }
        public int? PaymentStatus { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Ecxcode { get; set; }
    }
}
