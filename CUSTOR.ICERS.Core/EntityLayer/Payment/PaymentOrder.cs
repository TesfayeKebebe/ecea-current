﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Payment
{
    public class PaymentOrder : BaseEntity
    {
        public Guid PaymentOrderId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public PaymentOrderDetail PaymentOrderDetail { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public int ServiceId { get; set; }
    }

    public class PaymentOrderDTO: BaseEntity
    {
        public Guid PaymentOrderId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string CheckNo { get; set; }
        public string ReceiptNo { get; set; }
        public double TotalCost { get; set; }
        public int PaymentType { get; set; }
        public int ExchangeActorTypeId { get; set; }
    }
}
