﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Payment
{
    public class PaymentOrderDetail : BaseEntity
    {
        public Guid PaymentOrderDetailId { get; set; }
        public double TotalAmount { get; set; }
        public Guid PaymentOrderId { get; set; }
        public PaymentOrder Order { get; set; }
        public string CheckNo { get; set; }
        public string ReceiptNo { get; set; }
        public int PaymentType { get; set; }
    }
}
