﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Payment
{
    public class Receipt : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReceiptId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
    }

    public partial class ReceiptDTO
    {
        public int ReceiptId { get; set; }
        public int ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
    }
}
