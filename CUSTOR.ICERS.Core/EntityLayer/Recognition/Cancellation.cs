﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Recognition
{
    public class Cancellation : BaseEntity
    {
        public int CancellationId { get; set; }
        public int CancellationType { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }

        [ForeignKey("CancellationType")]
        public Lookup.Lookup Lookup { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public Guid ExchangeActorId { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
    }


    public class CancellationDTO: BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public int CancellationType { get; set; }
        public string ExchangeActorType { get; set; }
        public string ExchangeActorName { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public string Description { get; set; }

    }

    public class CancellationVM
    {
        public string CreatedDateTime { get; set; }
        public string CancellationType { get; set; }
        public string Description { get; set; }

    }
}
