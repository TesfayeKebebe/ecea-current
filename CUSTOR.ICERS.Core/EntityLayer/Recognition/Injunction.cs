﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Recognition
{
    public partial class Injunction : BaseEntity
    {
        [MaxLength(15)]
        public int InjunctionId { get; set; }
        [MaxLength(15)]
        public virtual ExchangeActor ExchangeActor { get; set; }
        [MaxLength(15)]
        public int InjunctionBodyId { get; set; }
        [MaxLength(15)]
        public Guid ExchangeActorId { get; set; }
        [MaxLength(15)]
        public int InjunctionStatus { get; set; }

        public int Reason { get; set; }
        public DateTime InjunctionEndDate { get; set; }
        [MaxLength(250)]
        public string InjunctionLetterNo { get; set; }
        [MaxLength(250)]
        public string InjunctionLiftedLetterNo { get; set; }
        public DateTime InjunctionStartDate { get; set; }
        public DateTime InjunctionLiftedDate { get; set; }
        public string InjunctionLiftReason { get; set; }
        public int InjunctionLiftedBy { get; set; }
        public string SpecialReason { get; set; }
        public int ApprovalDecision { get; set; } // 1 approved 2 is declined 0 not decided
        public Guid? DecisionMadeBy { get; set; }
        public string DecisionSummary { get; set; }
        public DateTime? DecissionDate { get; set; }
        public bool ApprovalFinished { get; set; }
    }
     public class InjunctionQueryParameters
    {
        public InjunctionQueryParameters()
        {

        }
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
    }
    public partial class InjunctionViewDTO
    {
        public InjunctionViewDTO()
        {

        }
        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string SpecialReason { get; set; }

        public string InjunctionBodyId { get; set; }
        public int InjunctionStatus { get; set; }
        public string Reason { get; set; }
        public int ReasonId { get; set; }
        public string InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionStartDate { get; set; }
        public string InjunctionLiftReason { get; set; }
        public string InjunctionLiftedBy { get; set; }
        public string InjunctionLiftedDate { get; set; }
        public int ApprovalDecision { get; set; } // 1 approved 2 is declined 0 not decided
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual OwnerManager OwnerManager { get; set; }
    }

    public partial class CancellationViewDTO:BaseEntity
    {
        public CancellationViewDTO()
        {

        }
        public int CancellationId { get; set; }
        public int CancellationType { get; set; }
        public string CancellationTypeName { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        [ForeignKey("CancellationType")]
        public Lookup.Lookup Lookup { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public Guid ExchangeActorId { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public string CancellationDate { get; set; }
        //public virtual OwnerManager OwnerManager { get; set; }
    }
    public partial class PendingInjunctionViewDTO
    {


        public PendingInjunctionViewDTO()
        {

        }
        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string SpecialReason { get; set; }
        public string InjunctionBodyId { get; set; }
        public int InjunctionStatus { get; set; }
        public string Reason { get; set; }
        public string InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public string InjunctionStartDate { get; set; }
        public int CurrentStep { get; set; }
        public int NextStep { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public bool? IsFinished { get; set; }
        public bool ApprovalFinished { get; set; }
    }
    public partial class InjunctionInfoDTO
    {
      
        public int? InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; } 
        public int InjunctionBodyId { get; set; }
        public int Reason { get; set; }
        public int InjunctionStatus { get; set; }
        public DateTime InjunctionStartDate { get; set; }
        public DateTime InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid? UpdatedUserId { get; set; }
        public DateTime? InjunctionLiftedDate { get; set; }
        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionLiftReason { get; set; }
        public string InjunctionLiftedBy { get; set; }
        public int ApprovalDecision { get; set; }  // by default 0 
        public string SpecialReason { get; set; }
        public int? NextStep { get; set; }
        public bool IsFinished { get; set; }
        public int? CurrentStep { get; set; }
        public bool ApprovalFinished { get; set; }
        public bool ApprovalStatus { get; set; }
        public DateTime? ServiceEndDateTime { get; set; }
        public string Comment { get; set; }
        public string ProcessedByName { get; set; }
        public int? FinalDec { get; set; }
    }
    
    public partial class InjunctionInfoDTOForDataEntry
    {

        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int InjunctionBodyId { get; set; }
        public int Reason { get; set; }
        public int InjunctionStatus { get; set; }
        public DateTime InjunctionStartDate { get; set; }
        public DateTime InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }
        public DateTime? InjunctionLiftedDate { get; set; }
        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionLiftReason { get; set; }
        public int? InjunctionLiftedBy { get; set; }
        public int? ApprovalDecision { get; set; }  // by default 0 
        public string SpecialReason { get; set; }
        public int NextStep { get; set; }
        public bool IsFinished { get; set; }
        public int CurrentStep { get; set; }
        public bool ApprovalFinished { get; set; }
        public bool ApprovalStatus { get; set; }
        public DateTime ServiceEndDateTime { get; set; }
        public string Comment { get; set; }
        public string ProcessedByName { get; set; }
        public int FinalDec { get; set; }
        public int DataType { get; set; }
    }

    public partial class PostInjunctionDTOForDataEntry : BaseEntity
    {

        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int InjunctionBodyId { get; set; }
        public int InjunctionStatus { get; set; }
        public int Reason { get; set; }
        public DateTime InjunctionStartDate { get; set; }
        public DateTime InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public Guid? CreatedUserId { get; set; }
        public string SpecialReason { get; set; }
        public bool ApprovalFinished { get; set; }
        public int ApprovalDecision { get; set; }

        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionLiftReason { get; set; }
        public int? InjunctionLiftedBy { get; set; }
        public DateTime InjunctionLiftedDate { get; set; }
        public Guid? DecisionMadeBy { get; set; }
        public string DecisionSummary { get; set; }
        public DateTime? DecissionDate { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }

    public partial class InjunctionLiftDTO
    {
        public InjunctionLiftDTO()
        {

        }
        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionLiftReason { get; set; }
        public int InjunctionLiftedBy { get; set; }
        public int InjunctionStatus { get; set; }
        public DateTime InjunctionLiftedDate { get; set; }
        public Guid? UpdatedUserId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }

    public partial class PostInjunctionDTO
    {
        public PostInjunctionDTO()
        {

        }
        public int InjunctionId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string ExchangeActorName { get; set; }
        public int InjunctionBodyId { get; set; }
        public int InjunctionStatus { get; set; }
        public int Reason { get; set; }
        public DateTime InjunctionStartDate { get; set; }
        public DateTime InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public Guid? CreatedUserId { get; set; }
        public string SpecialReason { get; set; }
        public bool ApprovalFinished { get; set; }
        public int ApprovalDecision { get; set; }

    }

    public partial class InjunctionApprovalDTO
    {
        public int InjunctionId { get; set; }
        public int ApprovalDecision { get; set; } // 1 approved 2 is declined 0 not decided
        public Guid DecisionMadeBy { get; set; }
        public string DecisionSummary { get; set; }
        public DateTime DecissionDate { get; set; }
        public int customerStatus { get; set; }
        public Guid ExchangeActorId { get; set; }
        public bool ApprovalFinished { get; set; }

    }

    public partial class InjunctionForProfile
    {


        public string SpecialReason { get; set; }

        public string InjunctionBodyId { get; set; }
        public string InjunctionStatus { get; set; }
        public string Reason { get; set; }
        public string ReasonId { get; set; }
        public string InjunctionEndDate { get; set; }
        public string InjunctionLetterNo { get; set; }
        public string InjunctionLiftedLetterNo { get; set; }
        public string InjunctionStartDate { get; set; }
        public string InjunctionLiftReason { get; set; }
        public string InjunctionLiftedBy { get; set; }
        public string InjunctionLiftedDate { get; set; }
    }

}
