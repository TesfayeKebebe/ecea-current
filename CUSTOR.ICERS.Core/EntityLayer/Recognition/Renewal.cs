﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Recognition
{
    public class Renewal : BaseEntity
    {
        public int RenewalId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public Guid ExchangeActorId { get; set; }
    }
    public class RenewalDTO: BaseEntity
    {
        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string ExchangeActorName { get; set; }
        public string ExchangeActorType { get; set; }
        public int RenewalServieType { get; set; }
        public int CustomerTypeId { get; set; }

    }

    public class RenewalVm : BaseEntity
    {
        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string ExchangeActorName { get; set; }
        public string ExchangeActorType { get; set; }
        public int RenewalServieType { get; set; }
        public int CustomerTypeId { get; set; }
        public string ExpireDateTime { get; set; }
        public string IssueDate { get; set; }
        public string WithPenalityRenewal { get; set; }

    }

}
