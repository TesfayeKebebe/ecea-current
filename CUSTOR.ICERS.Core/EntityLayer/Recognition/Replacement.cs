﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Recognition
{
    public class Replacement : BaseEntity
    {
        public int ReplacementId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }

        public ExchangeActor ExchangeActor { get; set; }
        public Guid ExchangeActorId { get; set; }
    }


    public class ReplacementDTO: BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public string ExchangeActorType { get; set; }
        public string ExchangeActorName { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int CustomerTypeId { get; set; }
    }
    public class ReplacementVM 
    {
        public string CreatedDateTime { get; set; }

    }
}
