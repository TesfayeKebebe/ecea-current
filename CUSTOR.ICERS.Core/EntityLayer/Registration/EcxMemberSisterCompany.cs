﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class EcxMemberSisterCompany : BaseEntity
    {
        public EcxMemberSisterCompany()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ECXMemberSisterCompanyId { get; set; }
        public Guid ExchagneActorId { get; set; }
        public Guid MemberExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }

    }

    public class EcxMemberSistetCompanyDTO: BaseEntity
    {
        public int ECXMemberSisterCompanyId { get; set; }
        public Guid ExchagneActorId { get; set; }
        public Guid MemberExchangeActorId { get; set; }
    }
}
