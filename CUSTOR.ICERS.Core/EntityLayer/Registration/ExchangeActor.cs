﻿using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class ExchangeActor : BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public string Tin { get; set; }
        public string Ecxcode { get; set; }
        public int CustomerTypeId { get; set; }
        [ForeignKey("CustomerTypeId")]
        public Lookup.Lookup CustomerType { get; set; }
        public int? MemberCategoryId { get; set; }
        [ForeignKey("MemberCategoryId")]
        public Lookup.Lookup MemberCategory { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEngSort { get; set; }
        public string OrganizationNameAmhSort { get; set; }
        public int? OrganizationTypeId { get; set; }
        public int? BuisnessFiledId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Region Region { get; set; }
        public int? ZoneId { get; set; }
        [ForeignKey("ZoneId")]
        public Zone Zone { get; set; }
        public int? WoredaId { get; set; }
        [ForeignKey("WoredaId")]
        public Woreda Woreda { get; set; }
        public int? KebeleId { get; set; }
        [ForeignKey("KebeleId")]
        public Kebele Kebele { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string OrganizationRemark { get; set; }
        public DateTime? OrganizationEstablishedDate { get; set; }
        public OwnerManager OwnerManager { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? NetWorthCapital { get; set; }
        public ICollection<MemberRepresentativeType> MemberRepresentativeType { get; set; }
        public Guid? DelegatorId { get; set; }
        [ForeignKey("DelegatorId")]
        public ExchangeActor Delegator { get; set; }
        public ICollection<ServiceApplication> ServiceApplications { get; set; }
        public ICollection<PaymentOrder> PaymentOrder { get; set; }
        public ICollection<MemberProduct> MemberProduct { get; set; }
        public ICollection<SisterCompany> SisterCompany { get; set; }
        public int Status { get; set; }
        [ForeignKey("Status")]
        public Lookup.Lookup ExchangeActorStatus { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public DateTime? IssueDateTime { get; set; }
        public DateTime? ExpireDateTime { get; set; }
        public virtual ICollection<CustomerBussinessFiled> CustomerBussinessFiled { get; set; }
    }
    public class CustomerRecognitionListDTO
    {
        public string ExchangeActorType { get; set; }
        public string CustomerFullName { get; set; }
        // newely added attributes
        public string MobilePhone { get; set; }
        public string CertificateIssueDate { get; set; }
        public string CertificateExpireDate { get; set; }
        public byte[] Photo { get; set; }
        public string OrganizationName { get; set; }
    }
    public class ExchangeActorGeneralInfoDTO: BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
        public ServiceApplication ServiceApplication { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int Status { get; set; }
        public string Tin { get; set; }
        public string Ecxcode { get; set; }
        public int CustomerTypeId { get; set; }
        public int? MemberCategoryId { get; set; }
        public Guid? DelegatorId { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public int? OrganizationTypeId { get; set; }
        public int? BuisnessFiledId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetWorthCapital { get; set; }
        public DateTime? OrganizationEstablishedDate { get; set; }
        public List<MemberRepresentativeTypeDTO> MemberRepresentativeType { get; set; }
        public List<MemberProductDTO> MemberProduct { get; set; }
        public string DelgatorOrganizationName { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
        public int? DelegatorTypeId { get; set; }
        public List<CustomerBusinessFiledDto> CustomerBussinessFiled { get; set; }
        
    }
    public class CusteromSearchDTO
    {
        public Guid ExchangeActorId { get; set; }
        public string LegalBodyName { get; set; }
        public string LegalBodyFatherName { get; set; }
        public string LegalBodyGrandFatherName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public int CustomerTypeId { get; set; }
        public string FullName { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int Status { get; set; }
        public int ActiveReprsentativeCount { get; set; }
        public bool IsLicenceExpired { get; set; }
        public Guid DelegatorId { get; set; }
        public string MemberCategory { get; set; }
        public int MemberCategoryId { get; set; }
        public List<MemberRepresentativeTypeDTO> MemberRepresentativeType { get; set; }

    }
    //public class CompletedApplicationaDTO
    //{
    //    public Guid ExchangeActorId { get; set; }
    //}
    public class CustomerQueryParameters
    {
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }

        // For srearch
        //public string LegalBodyName { get; set; }
        //public string LegalBodyFatherName { get; set; }
        //public string LegalBodyGrandFatherName { get; set; }
        public string OrganizationName { get; set; }
        public int? CustomerType { get; set; }
        public string Ecxcode { get; set; }
        public string Tin { get; set; }
        public int? MemberCategory { get; set; }
    }
    public class ExchangeActorProfile
    {
        public string FullName { get; set; }
        public string CustomerStatus { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
        public string OrganizationName { get; set; }
        public string MemberType { get; set; }
        public string EcxCode { get; set; }
        public byte[] Photo { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string MemberCategory { get; set; }
        public int SatusCode { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int CustomerTypeId { get; set; }
    }

    public class ExchangeActorGeneralInfoForPRofile
    {

        public string OrganizationNameEng { get; set; }
        public int CustomerTypeId { get; set; }
    }
    public class AddressDTO
    {
        public string Email { get; set; }
        public int? RegionId { get; set; }
        public string Region { get; set; }
        public int? ZoneId { get; set; }
        public string Zone { get; set; }
        public int? WoredaId { get; set; }
        public string Woreda { get; set; }
        public int? KebeleId { get; set; }
        public string Kebele { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string FaxNo { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string OrganizationAreaNameAmh { get; set; }
        public string OrganizationAreaNameEng { get; set; }
        public string HeadOffice { get; set; }
        public string HeadOfficeAmh { get; set; }
    }

    public class CustomerOrganizationVM
    {
        public string OrganizationName { get; set; }
        public string EcxCode { get; set; }
        public string MemberCategory { get; set; }
        public string OrganizationType { get; set; }
        public List<CustomerBusinessFiledNameVM> BusinessFiled { get; set; }
        public List<ProducNameVM> Products { get; set; }
        public string Address { get; set; }
        public string CustomerType { get; set; }
        public byte[] Photo { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
    }

    public class RepresntativeVM
    {
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string EducationalLevel { get; set; }
        public string Tel { get; set; }
        public string IssuedDate { get; set; }
        public string MobileNo { get; set; }
        public string Status { get; set; }

    }

    public class ExchangeActorServicesDto
    {
        public List<RenewalVm> Renewals { get; set; }
        public List<InjunctionForProfile> Ban { get; set; }
        
        public List<CancellationVM> Cancel { get; set; }
        public List<ReplacementVM> Replacment { get; set; }
        public List<RepresentativeViewModelForProfile> RepresentativeChange { get; set; }
        public List<AddressChangeDTOForProfile> AddressChange { get; set; }
        public List<NameChangeDTOForProfile> OrganizationNamechange { get; set; }
        public List<AddressChangeDTOForProfile> OwnerManagerAddressChange { get; set; }     
        public List<FinancilaInstutiutionDataChangeDTOForProfile> FinacilaInsititutionAddressChange { get; set; }



    }

}
