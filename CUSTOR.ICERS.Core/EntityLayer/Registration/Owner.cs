﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class Owner : BaseEntity
    {
        [Key]
        public int OwnerId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string OwnerFullNameAmh {get; set;}
        public string OwnerFullNameEng { get; set; }
        public int? Gender { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string MobileNo { get; set; }
        public string JobPosition { get; set; }
    }

    public class OwnerDTO
    {
        public int OwnerId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int? Gender { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }

        public string MobileNo { get; set; }
        public string JobPosition { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public bool IsEceaContact { get; set; }
        public bool IsEcxContact { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }
    }

    //public class OwnerListDTO
    //{
    //    public int OwnerId { get; set; }
    //    public string OwnerFullName { get; set; }
    //    public string MobileNo { get; set; }
    //    public string JobPosition { get; set; }
    //}
}
