﻿using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class OwnerManager : BaseEntity
    {
        public int OwnerManagerId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FirstNameAmhSort { get; set; }
        public string FirstNameEngSort { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameAmhSort { get; set; }
        public string FatherNameEng { get; set; }
        public string FatherNameEngSort { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameAmhSort { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameEngSort { get; set; }
        public string IdentityCard { get; set; }
        public byte[] Photo { get; set; }
        public int? NationalityId { get; set; }
        public int? GenderId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Region Region { get; set; }
        public int? ZoneId { get; set; }
        [ForeignKey("ZoneId")]
        public Zone Zone { get; set; }
        public int? WoredaId { get; set; }
        [ForeignKey("WoredaId")]
        public Woreda Woreda { get; set; }
        public int? KebeleId { get; set; }
        [ForeignKey("KebeleId")]
        public Kebele Kebele { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string Remark { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
        public virtual Nationality Nationalty { get; set; }
        public int? IdentityCardType { get; set; }
        public int EducationalLevelId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int? ManagerTypeId { get; set; }
        // public int? OwnerId { get; set; }
        // public Owner Owner { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum length for jobpositon is 100 charcter.")]
        public string Jobposition  { get; set; }
        public bool IsOwner { get; set; }
        public bool IsManager { get; set; }
        public bool IsEceaContact { get; set; }
        public bool IsEcxContact  { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }


    }

    public class OwnerManagerDTO : BaseEntity
    {
        public int OwnerManagerId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FirstNameAmhSort { get; set; }
        public string FirstNameEngSort { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameAmhSort { get; set; }
        public string FatherNameEng { get; set; }
        public string FatherNameEngSort { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameAmhSort { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameEngSort { get; set; }
        public string IdentityCard { get; set; }
        public int IdentityCardType { get; set; }
        public int EducationalLevelId { get; set; }
        public int NationalityId { get; set; }
        public int? GenderId { get; set; }
        public string Email { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? WoredaId { get; set; }
        public int? KebeleId { get; set; }
        public string MobileNo { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string HouseNo { get; set; }
        public string Remark { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int? ManagerTypeId { get; set; }
        //public int? OwnerId { get; set; }
        public byte[] Photo { get; set; }
        public bool IsOwner { get; set; }
        public string Jobposition { get; set; }
        public bool IsManager { get; set; }
        public bool IsEceaContact { get; set;}
        public bool IsEcxContact { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }

    }

    public class ManaerName
    {
        public string FullName { get; set; }
    }

    public class OwnerViewModel
    {
        public int OwnerId { get; set; }
        public string OwnerFullName { get; set; }
        public string MobileNo { get; set; }
        public string JobPosition { get; set; }
        public bool IsEceaContact { get; set; }
        public bool IsEcxContact { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }


    }

    public class OwnerDataChangeVM
    {
        public string FirstNameAmh { get; set; }
        public string FatherNameAmh { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }
        public int GenderId { get; set; }
        public string Gender { get; set; }
        public string JobPosition { get; set; }
        public string ManagerType { get; set; }
        public int ManagerTypeId { get; set; }
        public int ManagerId { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public int EducationalLevelId { get; set; }
        public string EducationalLevel { get; set; }
        public int IdentityCardTypeId { get; set; }
        public string IdentityNumber { get; set; }
        public byte[] Photo { get; set; }
        public string IdentityCardType { get; set; }
    }

    public class ManagerProfileVM
    {
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string EducationalLevel { get; set; }
        public string Tel { get; set; }
        public byte[] Photo { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool IsManager { get; set; }
        public string ManagerType { get; set; }
    }
}
