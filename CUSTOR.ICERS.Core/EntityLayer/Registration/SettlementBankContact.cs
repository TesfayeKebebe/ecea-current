﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class SettlementBankContact: BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public Guid ExchangeActorId { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum length for jobpositon is 100 charcter.")]
        public string Jobposition { get; set; }
        public bool IsEceaContact { get; set; }
        public bool IsEcxContact { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }
    }

    public class SettlementBankContactDTO: BaseEntity
    {
        public int OwnerManagerId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string Jobposition { get; set; }
        public bool IsEceaContact { get; set; }
        public bool IsEcxContact { get; set; }
        public bool IsSettlementTeam { get; set; }
        public bool IsTrained { get; set; }
    }
}
