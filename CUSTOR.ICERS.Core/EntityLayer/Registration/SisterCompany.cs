﻿using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Registration
{
    public class SisterCompany : BaseEntity
    {
        [Key]
        public int SisterCompanyId { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public int BusinessFiledId { get; set; }

        [ForeignKey("BusinessFiledId")]
        public virtual Lookup.Lookup Lookup { get; set; }
        public bool IsEcxMember { get; set; }
        public Guid ExchangeActorId { get; set; }
        public ExchangeActor Customer { get; set; }
        public int RegionId { get; set; }
        public Region Region { get; set; }
        public int ZoneId { get; set; }
        public Zone Zone { get; set; }
        public int KebeleId { get; set; }
        public Kebele Kebele { get; set; }
        public int WoredaId { get; set; }
        public Woreda Woreda { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string Tel { get; set; }
        public string MobileNo { get; set; }
        public string HouseNo { get; set; }
    }

    public class SisterCompanyPostDTO: BaseEntity
    {
        public int SisterCompanyId { get; set; }
        public string OrganizationNameEng { get; set; }
        public string OrganizationNameAmh { get; set; }
        public int? BusinessFiledId { get; set; }
        public bool IsEcxMember { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int? RegionId { get; set; }
        public int? ZoneId { get; set; }
        public int? KebeleId { get; set; }
        public int? WoredaId { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Pobox { get; set; }
        public string Tel { get; set; }
        public string MobileNo { get; set; }
        public string HouseNo { get; set; }
        public string BusinessDescription { get; set; }
        public Guid MemberSisteCompanyId { get; set; }
    }

    public class SisterCompanyGetDTO
    {
        public int SisterCompanyId { get; set; }
        public string OrganizationName { get; set; }
        public bool IsEcxMember { get; set; }
        public string MobileNo { get; set; }
    }
}
