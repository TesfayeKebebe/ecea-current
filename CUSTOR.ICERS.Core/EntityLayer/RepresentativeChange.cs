﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class RepresentativeChange : BaseEntity
    {
        [Key]
        public int RepresentativeChangeId { get; set; }
        public Guid OldDelegatorId { get; set; }
        public Guid NewDelegatorId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public Guid RepresentativeExchangeActorId { get; set; }
    }

    public class RepresentativeChangeDTO
    {
        public int? RepresentativeChangeId { get; set; }
        public Guid OldDelegatorId { get; set; }
        public Guid NewDelegatorId { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public Guid RepresentativeExchangeActorId { get; set; }
        public int CustomerTypeId { get; set; }
    }

    public class RepresentativeViewModel
    {
        public int? RepresentativeChangeId { get; set; }
        public Guid OldDelegatorId { get; set; }
        public Guid NewDelegatorId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public Guid RepresentativeExchangeActorId { get; set; }
        public string RepresentativeName { get; set; }
        public string NewDelegatorName { get; set; }
    }
    public class RepresentativeViewModelForProfile
    {
        public string OldDelegatorName { get; set; }
        public string NewDelegatorName { get; set; }
        public string RepresentativeName { get; set; }
        public string CreatedDateTime { get; set; }

    }
}
