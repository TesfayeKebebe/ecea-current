﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class ResultResponse
    {
        public Boolean Status { get; set; }
        public bool IsExisted { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsSaved { get; set; }
        public int StatusCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsUnderInjunction { get; set; }
        public bool HaveMax { get; set; }
        public bool IsExpired { get; set; }
        public bool IsFinalExpirationDate { get; set; }
        public bool IsLicenceNotExpired { get; set; }
        public bool IsDelgatorNotRenewed { get; set; }
        public bool IsMemberIntermidaryCapitalBelow { get; set; }
        public bool IsMemberTraderCapitalBelow { get; set; }
        public bool IsNonMemberDirectTraderCapitalBelow { get; set; }
    }

    public partial class ResultResponse2
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public int NewRecordId { get; set; }
    }
}
