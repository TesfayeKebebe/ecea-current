﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Service
{
    public class Service : BaseEntity

    {
        public int ServiceId { get; set; }
        [MaxLength(100)]
        public string DescriptionAmh { get; set; }
        [MaxLength(100)]
        public string DescriptionEng { get; set; }

        public ICollection<ServiceApplication> ServiceApplication;
        public ServiceTariff ServiceTariff { get; set; }

    }

    public class ServiceStatusDTO
    {
        public int ExchangeActorStatus { get; set; }
        public int ServiceApplicationStatus { get; set; }
    }
}
