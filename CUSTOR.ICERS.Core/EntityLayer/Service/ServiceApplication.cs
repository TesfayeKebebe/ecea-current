﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class ServiceApplication : BaseEntity
    {
        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public bool? IsPaid { get; set; }
        public int ServiceId { get; set; }
        public DateTime? ServiceEndDateTime { get; set; }
        public int Status { get; set; }
        public int CurrentStep { get; set; }
        public int NextStep { get; set; }
        public bool? IsFinished { get; set; }
        public bool? IsPaymentAuthorized { get; set; }
        public DateTime? AuthorizedDateTime { get; set; }
        public int? ApprovalStatus { get; set; }
        public virtual Service.Service Service { get; set; }
        public ExchangeActor ExchangeActor { get; set; }
        public PaymentAuthorized PaymentAuthorized { get; set; }
        public PaymentOrder PaymentOrder { get; set; }
        public bool ApprovalFinished { get; set; }
        public int OrganizationTypeId { get; set; }
        public int CustomerTypeId { get; set; }
        public string Ecxcode { get; set; }
    }


    // Service Application DTO
    public class ServiceApplicationDTO: BaseEntity
    {

        public Guid ServiceApplicationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int Status { get; set; }
        public int CurrentStep { get; set; }
        public int NextStep { get; set; }
        public int ServiceId { get; set; }
        public bool IsFinished { get; set; }
        public int? CustomerTypeId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public bool? IsPaymentAuthorized { get; set; }
        public bool? IsPaid { get; set; }
        public DateTime? AuthorizedDateTime { get; set; }
        public int MemberCategoryId { get; set; }
        public int? ApprovalStatus { get; set; }
        public bool ApprovalFinished { get; set; }
        public int OrganizationTypeId {get; set;}
        public string Ecxcode { get; set; }
    }

    public class ServiceApplicationSearchDTO
    {
        public string FullName { get; set; }
        public string ServiceType { get; set; }
        public int CurrentStep { get; set; }
        public int NextStep { get; set; }
        public Guid ExchangeActorId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public string StartDate { get; set; }
        public int Status { get; set; }
        public int ServiceId { get; set; }
        public bool IsPaymentAuthorized { get; set; }
        public bool IsPaid { get; set; }
        public ManaerName ManagerName { get; set; }
        public string EndDate { get; set; }
        public string Ecxcode { get; set; }
    }

    public class ServiceApplicationQueryParameters
    {
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }

        // For srearch
        public int? ServiceType { get; set; }
        public int? ServiceStatus { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        //public string Name { get; set; }
        //public string FatherName { get; set; }
        //public string GrandFatherName { get; set; }
        public string Ecxcode { get; set; }
        public string OrganizationName { get; set; }
        public string Tin { get; set; }
        public int? CustomerType { get; set; }
    }

}
