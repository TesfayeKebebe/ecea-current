﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Service
{
    public class ServicePrerequisite : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServicePrerequisiteId { get; set; }
        public int PrerequisiteId { get; set; }
        public Prerequisite Prerequisite { get; set; }
        public int CustomerTypeId { get; set; }
        [ForeignKey("CustomerTypeId")]
        public Lookup.Lookup CustomerType { get; set; }
        public bool IsRequired { get; set; }
        public bool IsDocument { get; set; }

        public int ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public Service Service { get; set; }
    }

    public class ServicePrerequisiteWithDocumentDTO
    {
        public int ServicePrerequisiteId { get; set; }
        public int ServiceId { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public bool IsDocument { get; set; }
        public int PrerequisiteId { get; set; }
    }

    public class ServicePrerequisteDTO
    {
        public int ServicePrerequisiteId { get; set; }
        public int PrerequisiteId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public bool IsRequired { get; set; }
        public bool IsDocument { get; set; }
        public bool IsActive { get; set; }
    }


    public class UploadedDocumentVM
    {
        public string FileName { get; set; }
        public string Description { get; set; }
    }
}
