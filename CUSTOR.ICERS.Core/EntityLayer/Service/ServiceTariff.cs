﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public partial class ServiceTariff : BaseEntity
    {

        public ServiceTariff() { }
        [Key]
        public int ServiceTariffId { get; set; }
        public int ServiceId { get; set; }
        public Service.Service Service { get; set; }
        [Column(TypeName = "decmial(5,2)")]
        public decimal TariffAmount { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public int ExchangeActorTypeId { get; set; }

    }

    public partial class ServiceTariffDTO
    {
        public decimal TariffAmount { get; set; }
        public string Description { get; set; }
    }
    public partial class ServiceForTariffDTO
    {

        public int ServiceId { get; set; }
        public int TariffId { get; set; }

    }

    public partial class AllServiceTariffDTO
    {
        public int ServiceId { get; set; }
        public decimal TariffAmount { get; set; }
        public int ServiceTariffId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public string DescriptionExchange { get; set; }
        public string DescriptionService { get; set; }

    }
    public partial class ServicetariffDTO:BaseEntity
    {
        public int ServiceId { get; set; }
        public decimal TariffAmount { get; set; }
        public int ServiceTariffId { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionAmh { get; set; }
        public int ExchangeActorTypeId { get; set; }

    }
}
