﻿
using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer
{

    public class GeneralSetting:BaseEntity
    {

        [Key]
        public int ID { get; set; }
        public string ReportTitle { get; set; }
        public string ReportTitleAmaric { get; set; }
        public string DirectorName { get; set; }
        public string DirectorNameAmharic { get; set; }
        public byte[] OrganizationLogo { get; set; }
        public byte[] DirectorSigniture { get; set; }

    }
    public class GeneralSettingDTO: BaseEntity
    {
        public int ID { get; set; }
        public string ReportTitle { get; set; }
        public string ReportTitleAmaric { get; set; }
        public string DirectorName { get; set; }
        public string DirectorNameAmharic { get; set; }
        public byte[] OrganizationLogo { get; set; }
        public byte[] DirectorSigniture { get; set; }

    }



}
