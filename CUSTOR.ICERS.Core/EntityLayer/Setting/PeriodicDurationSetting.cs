﻿
using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer
{

    public class PeriodicDurationSetting:BaseEntity
    {

        [Key]
        public int ID { get; set; }
        public float? RenewalDuration { get; set; }
        public int? RenewalDurationUnit { get; set; }
        public int? TemporaryRenewalDurationUnit { get; set; }
        public float? TemporaryRenewalDuration { get; set; }
        public int? GracePeriodUnit { get; set; }
        public float? GracePeriodDuration { get; set; }
        public int? LawEnforcmentGracePeriodUnit { get; set; }
        public float? LawEnforcmentGracePeriodDuration { get; set; }
        public int? InjectionDurationUnit { get; set; }
        public float? InjectionDuration { get; set; }

    }

    public class PeriodicDurationSettingDTO : BaseEntity
    {
        public int ID { get; set; }
        public float? RenewalDuration { get; set; }
        public int? RenewalDurationUnit { get; set; }
        public int? TemporaryRenewalDurationUnit { get; set; }
        public float? TemporaryRenewalDuration { get; set; }
        public int? GracePeriodUnit { get; set; }
        public float? GracePeriodDuration { get; set; }
        public int? LawEnforcmentGracePeriodUnit { get; set; }
        public float? LawEnforcmentGracePeriodDuration { get; set; }
        public int? InjectionDurationUnit { get; set; }
        public float? InjectionDuration { get; set; }

    }


}
