﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Setting
{
    
    public partial class RecoginationTotalCapital : BaseEntity
    {

        [Key]
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public int MemberCategoryId { get; set; }


    }

    public partial class RecoginationTotalCapitalDTO : BaseEntity
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public int MemberCategoryId { get; set; }

    }

    public partial class AllRecoginationTotalCapitalDTO
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int ExchangeActorTypeId { get; set; }
        public string DescriptionExchange { get; set; }
        public string DescriptionMemberCatagory { get; set; }
        public int MemberCategoryId { get; set; }

    }
}
