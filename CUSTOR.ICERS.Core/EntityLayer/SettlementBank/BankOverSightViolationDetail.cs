﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class BankOverSightViolationDetail: BaseEntity
    {
        public int BankOverSightViolationDetailId { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        public int FaultTypeId { get; set; }
        public int DecisionTypeId { get; set; }
        public string ErrorOcuredDueto { get; set; }
        public int BankOversightViolationId { get; set; }
        public DateTime DateOfError { get; set; }
        public int Status { get; set; }
        public virtual BankOversightViolation BankOversightViolation { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class OverSightViolationDetailChangeDTO : BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
    }
    public class BankOverSightViolationDetailDTO : BaseEntity
    {
        public int BankOverSightViolationDetailId { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        public int FaultTypeId { get; set; }
        public int DecisionTypeId { get; set; }
        public string ErrorOcuredDueto { get; set; }
        public int BankOversightViolationId { get; set; }
        public DateTime DateOfError { get; set; }
        public virtual BankOversightViolation BankOversightViolation { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class BankOverSightViolationViewModels
    {
        public int BankOversightViolationId { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public int BankOverSightViolationDetailId { get; set; }
        public DateTime DateOfError { get; set; }
        public string OrganizationName { get; set;  }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        public int FaultTypeId { get; set; }
        public int DecisionTypeId { get; set; }
        public string ErrorOcuredDueto { get; set; }
        public string ErrorOfDate { get; set; }
        public string ECXCode { get; set; }
        public string CustomerType { get; set; }
        public string FullName { get; set; }
    }
}
