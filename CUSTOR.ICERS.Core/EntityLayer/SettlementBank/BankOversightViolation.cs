﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
   public class BankOversightViolation: BaseEntity
    {
        public BankOversightViolation()
        {
            WorkFlow = new HashSet<WorkFlow>();
            BankOverSightViolationDetail = new HashSet<BankOverSightViolationDetail>();
        }
        public int BankOversightViolationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ViolationStatus { get; set; }
        public int Status { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
        public virtual ICollection<BankOverSightViolationDetail> BankOverSightViolationDetail { get; set; }
    }
    public class BankOversightViolationDTO: BaseEntity
    {
        public int BankOversightViolationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ViolationStatus { get; set; }
        public int Status { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
        public virtual ICollection<BankOverSightViolationDetail> BankOverSightViolationDetail { get; set; }
    }
}
