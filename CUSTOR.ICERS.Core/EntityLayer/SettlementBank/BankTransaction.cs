﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    [Serializable]
    [XmlRoot("BankTransactions")]
    public partial class BankTransaction : IAudit
    {
        public int BankTransactionId { get; set; }
        public bool? IsThroughUpload { get; set; }
        [ForeignKey("SiteCode")]       
        public string SiteCode { get; set; }
        [ForeignKey("SiteCode")]
        public Site Site { get; set; }
        public string MemberCode { get; set; }
        public string MemberType { get; set; }
        public string BranchName { get; set; }
        public DateTime TransactionDate { get; set; }
        public string AccountHolderName { get; set; }
        //public string Description { get; set; }
        public string AccountNo { get; set; }
        public string AccountType { get; set; }
        public string Reference { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Debit { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Credit { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BBF { get; set; }
        public bool UnAuthorized { get; set; }

    }
    public partial class BankTransactionDTO
    {
        public int BankTransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        //public string BankName { get; set; }
        public string SiteCode { get; set; }        
        public string AccountHolderName { get; set; }
        //public string Description { get; set; }
        public string AccountNo { get; set; }
        public string AccountType { get; set; }
        //public string Reference { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
    [Serializable()]
    [XmlRoot("BankTransactions")]
    public class Transactions
    {
        [XmlArray("BankTransactions")]
        [XmlArrayItem("BankTransaction", typeof(BankTransaction))]
        public BankTransaction[] BankTransaction { get; set; }
    }
    public class ClearingSettlementBank
    {
        public string AccountType { get; set; }
        public decimal BBF { get; set; }
        public decimal TotalDebet { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal Balance { get; set; }
    }
    public class AllBankTransactionReportView
    {
        public string BankName { get; set; }
        public decimal BBF { get; set; }
        public decimal TotalDebet { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal Balance { get; set; }
        public string Total { get; set; }
        public decimal TotalDebitPercentage { get; set; }
        public decimal TotalCreditPercentage { get; set; }
        public decimal BalancePercentage { get; set; }
    }
    public class UnAuthorizedBankTransaction
    {
        public string MemberCode { get; set; }
        public string MemberType { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountType { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BBF { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Credit { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Debit { get; set; }        
        [Column(TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }        
    }
    public class SearchBankTransactionCriteria
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public string MemberCode { get; set; }
        public string AccountType { get; set; }
        public string MemberType { get; set; }
        public string SiteCode { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int AmountType { get; set; }
        public int Top { get; set; }
        public string Lang { get; set; }
        public int ViolationTypeId { get; set; }
    }
    public class BankTransactionTopTen
    {
        public string AccountHolderName { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Amount { get; set; }
    }
    public class BankTransactionMonthlyReport
    {
        public string BankName { get; set; }
        public int MemberPayIn { get; set; }
        public int ClientPayIn { get; set; }
        public int NonMemberPayIn { get; set; }
        public int MemberPayOut { get; set; }
        public int ClientPayOut { get; set; }
        public int NonMemberPayOut { get; set; }
        public int TotalPayIn { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal PayInPercentage { get; set; }
        public int TotalPayOut { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal PayOutPercentage { get; set; }

    }
    public class ViolationBankTransaction
    {
        public string SiteCode { get; set; }
        public string AccountHolderName { get; set; }
        public string MemberCode { get; set; }
        public string MemberType { get; set; }
        public string AccountType { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Credit { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Debit { get; set; }
    }
    public class BankTransactionViewModel
    {
        public string AccountHolderName { get; set; }
        public string AccountNo { get; set; }
        public string MemberType { get; set; }
        public string AccountType { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
    public class DailySettlementTransactionReport
    {
        public string BankName { get; set; }
        public string TransactionDate { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNo { get; set; }
        public decimal MemberDebitPayIn { get; set; }
        public decimal ClientDebitPayIn { get; set; }
        public decimal NonMembersDebitPayIn { get; set; }
        public decimal MemberCreditPayOut { get; set; }
        public decimal ClientCreditPayOut { get; set; }
        public decimal NonMembersCreditPayOut { get; set; }
        public string PreparedBy { get; set; }
        public string ApprovedBy { get; set; }

    }
    public class SettlementTransactionReportSummery
    {
        public string BankName { get; set; }
        public decimal MemberDebitPayIn { get; set; }
        public decimal ClientDebitPayIn { get; set; }
        public decimal NonMembersDebitPayIn { get; set; }
        public decimal MemberCreditPayOut { get; set; }
        public decimal ClientCreditPayOut { get; set; }
        public decimal NonMembersCreditPayOut { get; set; }
    }
}
