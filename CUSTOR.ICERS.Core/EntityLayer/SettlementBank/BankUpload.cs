﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public partial class BankUpload : IAudit
    {
        [Key]
        [Required]
        public int BankUploadId { get; set; }

        [ForeignKey("SiteCode")]
        public string SiteCode { get; set; }

        public int? NoOfRecords { get; set; }

        public string FileName { get; set; }

        [ForeignKey("SiteCode")]
        public virtual Site Site { get; set; }
        public int Status { get; set; }
    }
    public partial class BankUploadDTO
    {
        [Key]
        public int BankUploadId { get; set; }
        public string SiteCode { get; set; }
        public int? NoOfRecords { get; set; }
        public string BankName { get; set; }
        public string FileName { get; set; }
        //public string CreatedUserId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
