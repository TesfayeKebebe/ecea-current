﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.Entity.Ecx
{
   public class DelayRegisterPerBank: IAudit
    {
        public int DelayRegisterPerBankId { get; set; }
        public string BankName { get; set; }
        public decimal PayIn { get; set; }
        public decimal BalanceOne { get; set; }
        public decimal PayOut { get; set; }
        public decimal BalanceTwo { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
