﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.Entity.Ecx
{
   public class GuaranteeFundUpload: IAudit
    {
        public int GuaranteeFundUploadId { get; set; }
        public string FileName { get; set; }
        public int NoOfRecords { get; set; }
    }
    public class GuaranteeFundUploadDTO
    {
        public string FileName { get; set; }
        public int NoOfRecords { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
