﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.Entity.Ecx
{
   public class SettlementBankPerformance: IAudit
    {
        public int SettlementBankPerformanceId { get; set; }
        public string BankName { get; set; }
        public TimeSpan PayOutSLATimeInHours { get; set; }
        public TimeSpan PayInSLATimeInHours { get; set; }
        public TimeSpan BalanceOneSLAInHours { get; set; }
        public TimeSpan BalanceTwoSLAInHours { get; set; }
        public TimeSpan PayInMin { get; set; }
        public TimeSpan PayInMax { get; set; }
        public TimeSpan PayInAvg { get; set; }
        public TimeSpan BalanceOneMin { get; set; }
        public TimeSpan BalanceOneMax { get; set; }
        public TimeSpan BalanceOneAvg { get; set; }
        public TimeSpan PayOutMin { get; set; }
        public TimeSpan PayOutMax { get; set; }
        public TimeSpan PayOutAvg { get; set; }
        public TimeSpan BalanceTwoMin { get; set; }
        public TimeSpan BalanceTwoMax { get; set; }
        public TimeSpan BalanceTwoAvg { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
