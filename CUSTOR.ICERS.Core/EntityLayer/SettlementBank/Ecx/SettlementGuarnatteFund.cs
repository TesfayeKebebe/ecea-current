﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.Entity.Ecx
{
   public class SettlementGuarnatteFund: IAudit
    {
        public int SettlementGuarnatteFundId { get; set; }
        public string Description { get; set; }
        public string Sub_Description { get; set; }
        public string MemberName { get; set; }
        public string MemberType { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }
        public DateTime ReportDate { get; set; }

    }
}
