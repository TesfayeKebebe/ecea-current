﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.DTO.ECX
{
   public class SettlementGuarnatteFundDTO
    {
        public int SettlementGuarnatteFundId { get; set; }
        public string Description { get; set; }
        public string Sub_Description { get; set; }
        public string MemberName { get; set; }
        public string MemberType { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }
        public DateTime ReportDate { get; set; }

    }
    public class GuaranteeFundViewModel
    {
        public string BankName { get; set; }
        public decimal Total { get; set; }
    }
}
