﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx
{
   public class SettlementTransactionDaily
    {
        public string BankName { get; set; }
        public string AccountType { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal Amount { get; set; }
    }
    public class SettlementTransactionDailyView
    {
        public string BankName { get; set; }
        public string AccountType { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal Amount { get; set; }
        public decimal MemberPayIn { get; set; }
        public decimal ClientPayIn { get; set; }
        public decimal MemberPayOut { get; set; }
        public decimal ClientPayOut { get; set; }
    }
}
