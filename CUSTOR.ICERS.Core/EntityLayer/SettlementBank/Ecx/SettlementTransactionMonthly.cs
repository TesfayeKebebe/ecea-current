﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank.Ecx
{
   public class SettlementTransactionMonthly
    {
        public string BankName { get; set; }
        public string OrganizationName { get; set; }
        public string MemberId { get; set; }
        public string AccountType { get; set; }
        public DateTime TradeDate { get; set; }
        public string AccountNumber { get; set; }
        public decimal Amount { get; set; }
    }
    public class SettlementTransactionMonthlyView
    {
        public string BankName { get; set; }
        public string OrganizationName { get; set; }
        public string MemberId { get; set; }
        public string AccountType { get; set; }
        public DateTime TradeDate { get; set; }
        public string AccountNumber { get; set; }
        public decimal MemberPayIn { get; set; }
        public decimal ClientPayIn { get; set; }
        public decimal MemberPayOut { get; set; }
        public decimal ClientPayOut { get; set; }
    }
    public class SearchSqlQueryParms
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string SiteCode { get; set; }
    }
}
