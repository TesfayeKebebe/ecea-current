﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.DAL.Entity.Ecx
{
   public class UploadDelayRegisterPerBank: IAudit
    {
        public int UploadDelayRegisterPerBankId { get; set; }
        public string FileName { get; set; }
        public int NoOfRecords { get; set; }        
    }
}
