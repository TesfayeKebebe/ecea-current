﻿using System;

namespace CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank
{

    public class IAudit
    {
        public DateTime CreatedDateTime { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public string CreatedUserId { get; set; }
        public string UpdatedUserId { get; set; }
    }
}
