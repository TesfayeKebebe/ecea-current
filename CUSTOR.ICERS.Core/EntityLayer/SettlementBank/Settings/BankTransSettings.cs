﻿namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class BankTransSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
