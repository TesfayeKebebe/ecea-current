﻿namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class BankUploadSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
