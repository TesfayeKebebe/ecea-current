﻿namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class GeneralSettings
    {
        public string OrganizationName { get; set; }
        public string ReturnURL { get; set; }
        public string AutoSaveAuditFields { get; set; }

    }
}
