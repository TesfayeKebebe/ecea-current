﻿namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class SiteSettings
    {
        public int CachingExpirationPeriod { get; set; }
        public int DefaultPageSize { get; set; }
    }
}
