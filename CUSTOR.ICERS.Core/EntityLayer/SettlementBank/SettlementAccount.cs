﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class SettlementAccount: BaseEntity
    {
        public int SettlementAccountId { get; set; }
        public DateTime TransactionDate { get; set; }
        public string SiteCode { get; set; }
        public string NameOfAccount { get; set; }
        public string AccountNo { get; set; }
        public string Reference { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
    public class SettlementAccountDTO {
        public string NameOfAccount { get; set; }
        public string AccountNo { get; set; }
        public string Reference { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
}
