﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    public class Site : IAudit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SiteId { get; set; }
        public int SiteType { get; set; }
        public string Name { get; set; }
        public string NameEnglish { get; set; }
        [Key]
        [Required]
        public string SiteCode { get; set; }
        public bool IsActive { get; set; }
        [JsonIgnore]
        public virtual List<BankUpload> BankUploads { get; set; }
    }
    public class SiteDTO
    {
        public int SiteId { get; set; }
        public int SiteType { get; set; }
        public string Name { get; set; }
        public string NameEnglish { get; set; }
        [Key]
        public string SiteCode { get; set; }
        public bool IsActive { get; set; }
    }
}
