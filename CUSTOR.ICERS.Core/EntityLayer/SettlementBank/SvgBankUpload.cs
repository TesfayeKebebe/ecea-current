﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
   public partial class SvgBankUpload: IAudit
    {
        public int SvgBankUploadId { get; set; }
        [ForeignKey("SiteCode")]
        public string SiteCode { get; set; }
        public int NoOfRecords { get; set; }
        public string FileName { get; set; }
        [ForeignKey("SiteCode")]
        public virtual Site Site { get; set; }
    }
}
