﻿using CUSTOR.SettlementBankICERS.Core.EntityLayer.SettlementBank;
using System;
using System.Xml.Serialization;

namespace CUSTOR.ICERS.Core.EntityLayer.SettlementBank
{
    [Serializable]
    [XmlRoot("SvgSettlementAccount")]
    public partial class SvgSettlementAccount: IAudit
    {
        public int SvgSettlementAccountId { get; set; }
        public string SiteCode { get; set; }
        public string NameOfAccount { get; set; }
        public string AccountNo { get; set; }
        public string Reference { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
   public class SvgSettlementAccountViewModel
    {       
        public string NameOfAccount { get; set; }
        public string AccountNo { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal BBF { get; set; }
    }
}
