﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Trade
{
    public class Ecx_TradeReport
    {
		public float TradeNo { get; set; }
		public DateTime Date { get; set; }
		public DateTime Time { get; set; }
		public float WRNo { get; set; }
		[MaxLength (255)]
		public string ConsignmentType { get; set; }
		[MaxLength(255)]
		public string IsNewModel { get; set; }
		[MaxLength(255)]
		public string ProductType { get; set; }
		[MaxLength(255)]
		public string Grade { get; set; }
		[MaxLength(255)]
		public string Symbol { get; set; }
		[MaxLength(255)]
		public string BuyRepID { get; set; }
		[MaxLength(255)]
		public string BuyRepName { get; set; }
		[MaxLength(255)]
		public string BuyMemberId { get; set; }
		public float BuyMemberTINNo { get; set; }
		[MaxLength(255)]
		public string BuyMemberName { get; set; }
		[MaxLength(255)]
		public string BuyClientId { get; set; }
		[MaxLength(255)]
		public string BuyClientName { get; set; }
		public float BuyClientTINNo { get; set; }
		[MaxLength(255)]
		public string SellRepID { get; set; }
		[MaxLength(255)]
		public string SellRepName { get; set; }
		[MaxLength(255)]
		public string SellMemberId { get; set; }
		[MaxLength(255)]
		public string SellMemberName { get; set; }
		public float SellMemberTINNo { get; set; }
		[MaxLength(255)]
		public string SellClientId { get; set; }
		[MaxLength(255)]
		public string SellClientName { get; set; }
		public float SellClientTINNo { get; set; }
		public float Lot { get; set; }
		public float Quintal { get; set; }
		public float UnitPrice { get; set; }
		public float ValueInBirrSTD { get; set; }
		public float Bags { get; set; }
		public float VolumeKGUsingSTD { get; set; }
		[MaxLength(255)]
		public string Warehouse { get; set; }
		public float ProductionYear { get; set; }
		[MaxLength(255)]
		public string IsOnline { get; set; }
		public float OpeningPrice { get; set; }
		public float ClosingPrice { get; set; }
	}

	public class TradeSearchCritira
    {
		public DateTime From { get; set; }
		public DateTime To { get; set; }
    }
}
