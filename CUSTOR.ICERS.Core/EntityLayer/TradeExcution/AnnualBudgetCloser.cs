﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class AnnualBudgetCloser: BaseEntity
    {
        public int AnnualBudgetCloserId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class AnnualBudgetCloserDTO: BaseEntity
    {
        public int AnnualBudgetCloserId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class AnnualBudgetCloserNewDTO: BaseEntity
    {
        public int AnnualBudgetCloserId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
    public class AnnualBudgetCloserView
    {
        public int AnnualBudgetCloserId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BeginningDate { get; set; }
        public string DeadLine { get; set; }
    }
    public class AnnualBudgetCloserViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
