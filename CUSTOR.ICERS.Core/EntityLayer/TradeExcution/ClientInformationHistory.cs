﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
   public class ClientInformationHistory: BaseEntity
    {
        public int ClientInformationHistoryId { get; set; }
        public DateTime AgreementDate { get; set; }
        public int Gender { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        public string HouseNo { get; set; }
        public string RegularPhone { get; set; }
        public string MobilePhone { get; set; }
        public int Year { get; set; }
        public int ReportTypeId { get; set; }
        public DateTime ReportDate { get; set; }
        public int ReportPeriodId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int Status { get; set; }
        public int TradeTypeId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public int BusinessFiledId { get; set; }
        public int ClientTypeId { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string EcxClientCode { get; set; }
    }
}
