﻿using CUSTOR.ICERS.Core.EntityLayer.Common;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class ClientInformationReminder : BaseEntity
    {
        public int ClientInformationReminderId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }
    public class ClientInformationReminderDTO: BaseEntity
    {
        public int ClientInformationReminderId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }
    public class ClientInformationReminderView
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
