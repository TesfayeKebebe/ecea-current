﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
   public class ClientProduct: BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientProductId { get; set; }
        public int CommodityId { get; set; }
        public int MemberClientInformationId { get; set; }
        public virtual MemberClientInformation MemberClientInformation { get; set; }
        public virtual Commodity Commodity { get; set; }
    }
    public class ClientProductDTO
    {
        public int ClientProductId { get; set; }
        public int CommodityId { get; set; }
      //  public string CommodityName { get; set; }
    }
    public class ClientProductViewModel
    {
        public int CommodityId { get; set; }
        public string CommodityName { get; set; }
    }
}
