﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
  public  class ClientProductHistory: BaseEntity
    {
        public int ClientProductHistoryId { get; set; }
        public int CommodityId { get; set; }
        public int ClientInformationHistoryId { get; set; }
    }
}
