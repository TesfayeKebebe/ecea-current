﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class CommodityGrade : BaseEntity
    {
        public CommodityGrade()
        {
            this.MemberClientTradeDetails = new HashSet<MemberClientTradeDetail>();
        }
        public int CommodityGradeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptoinEng { get; set; }
        public int CommodityTypeId { get; set; }
        public virtual CommodityType CommodityType { get; set; }
        public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetails { get; set; }
    }
    public class CommodityGradeDTO: BaseEntity
    {
        public int CommodityGradeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptoinEng { get; set; }
        public int CommodityTypeId { get; set; }
        public string CommodityName { get; set; }
        public string CommodityTypeName { get; set; }
        public virtual CommodityType CommodityType { get; set; }
    }
    public class CommodityGradeByIdDTO
    {

        public int CommodityGradeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptoinEng { get; set; }
        public int CommodityTypeId { get; set; }
        public string CommodityName { get; set; }
        public int CommodityId { get; set; }

        public string Description { get; set; }
    }
}
