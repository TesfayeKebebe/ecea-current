﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class CommodityType : BaseEntity
    {
        public CommodityType()
        {
            CommodityGrades = new HashSet<CommodityGrade>();
        }

        public int CommodityTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public int CommodityId { get; set; }
        public virtual Commodity Commodity { get; set; }
        public virtual ICollection<CommodityGrade> CommodityGrades { get; set; }
    }

    public class CommodityTypeDTO: BaseEntity
    {
        public int CommodityTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public int CommodityId { get; set; }
        public string CommodityName { get; set; }
        public virtual Commodity Commodity { get; set; }
    }
}
