﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Report.ViewModel.TradeExcution
{
    public class ComparisonOffSiteMonitoring
    {
        public int Year { get; set; }        
        public int FirstQuarter { get; set; }
        public int SecondQuarter { get; set; }
        public int ThiredQuarter { get; set; }
        public int FourthQuarter { get; set; }
        public int PercentageFirstQuarter { get; set; }
        public int PercentageSecondQuarter { get; set; }
        public int PercentageThiredQuarter { get; set; }
        public int PercentageFourthQuarter { get; set; }
    }
    public class ComparativeQueryParameterSearch
    {
        public int CustomerTypeId { get; set; }
        public int ToWhomDoesTradeId { get; set; }
        public string EcxCode { get; set; }
        public int BussinessFieldId { get; set; }
        public double Quantity { get; set; }
        public int CommodityId { get; set; }
        public int TradeTypeId { get; set; }
        public int UnitMeasurementId { get; set; }
        public double UnitPrice { get; set; }
        public int ReasonTypeId { get; set; }
        public int Year { get; set; }
        public string Lang { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportAnalysisId { get; set; }
    }
    public class DepulicateMemberReportingViolation
    {
     public string OrganizationName { get; set; }
     public int Total { get; set; }
     public string ReportType { get; set; }
     public Guid Id { get; set; }
    }
    public class TradeExecutionOrderViewModel
    {
        public string OrganizationName { get; set; }
        public double Amount { get; set; }
    }
    public class TradeExecutionAmountViewModel
    {
        public string OrganizationName { get; set; }
        public int Amount { get; set; }
    }
    public class TradeExecutionOrderSearchQuery
    {
        public string Lang { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int TopLevel { get; set; }
        public string OrderTypeId { get; set; }
        public int CommodityId { get; set; }
        public int TradeOrderNameId { get; set; }
        public int Status { get; set; }
        public int ToWhomDoesTradeId { get; set; }
    }
    public class ReportAccomplishedResposiblity
    {
        public int Year { get; set; }
        public int OnTimeReport { get; set; }
        public int LateReport { get; set; }
        public int NotReport { get; set; }
        public int OnTimeReportInPercent { get; set; }
        public int LateReportInPercent { get; set; }
        public int NotReportInPercent { get; set; }
    }
    public class MemberAnnualFinancialAuditorViewModel
    {     
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobileNo { get; set; }
        public string RegularPhone { get; set; }
    }
}
