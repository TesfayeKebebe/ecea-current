﻿using CUSTOR.ICERS.Core.EntityLayer.Common;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class ExchangeActorFinanicial : BaseEntity
    {
        public int ExchangeActorFinanicialId { get; set; }
        public double Amount { get; set; }
        public int CustomerTypeId { get; set; }
    }
    public class ExchangeActorFinanicialDTO: BaseEntity
    {
        public int ExchangeActorFinanicialId { get; set; }
        public double Amount { get; set; }
        public int CustomerTypeId { get; set; }
    }
    public class FinancialPerformanceViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
