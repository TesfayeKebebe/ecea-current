﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
   public class FinancialAuditoredFileUpload: BaseEntity
    {
        public int FinancialAuditoredFileUploadId { get; set; }
        public string Url { get; set; }
        public string FileLocation { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public virtual MemberFinancialAuditor MemberFinancialAuditor { get; set; }
        [NotMappedAttribute]
        public List<IFormFile> ActualFile { get; set; }
    }
    public class FinancialAuditoredFileUploadDTO: BaseEntity
    {
        public int FinancialAuditoredFileUploadId { get; set; }
        public string Url { get; set; }
        public string FileLocation { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        [NotMappedAttribute]
        public List<IFormFile> ActualFile { get; set; }
    }
    public class FinancialAuditoredFileUploadView
    {
       // public string MemberFinancialAuditorId { get; set; }
        public string Url { get; set; }
    }
    public class AuditedAnnualDocument
    {
        public int FinancialAuditoredFileUploadId { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public string FileName { get; set; }
        public string CreatedDate { get; set; }
    }
}
