﻿using CUSTOR.ICERS.Core.EntityLayer.Common;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class FinancialReportReminder : BaseEntity
    {
        public int FinancialReportReminderId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }
    public class FinancialReportReminderDTO: BaseEntity
    {
        public int FinancialReportReminderId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
    }
}
