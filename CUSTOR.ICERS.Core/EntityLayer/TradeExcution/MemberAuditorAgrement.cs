﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.API.Controllers.TradeExcution
{
    public class MemberAuditorAgrement: BaseEntity
    {
        [Key]
        public int MemberAuditorAgrementId { get; set; }
        [Required]
        public string EcxCode { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        [Required]
        public DateTime ToDate { get; set; }
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class MemberAuditorAgreementView
    {
        public int id { get; set; }
        public string EcxCode { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string OrganiztionName { get; set; }
        public AuditorName AuditorName { get; set; }
    }
    public class AuditorName
    {
        public string AuditorNames { get; set; }
    }
}
