﻿using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class MemberClientInformation : BaseEntity
    {
        public MemberClientInformation()
        {
            MemberClientTradeDetails = new HashSet<MemberClientTradeDetail>();
        }
        public int MemberClientInformationId { get; set; }
        public string BusinessSector { get; set; }
        public DateTime AgreementDate { get; set; }
        public int Gender { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        public string HouseNo { get; set; }
        public string RegularPhone { get; set; }
        public string MobilePhone { get; set; }
       // public int CommidityTypeId { get; set; }
        public int Status { get; set; }
        public int TradeTypeId { get; set; }
        public int BusinessFiledId { get; set; }
      //  public int CommodityId { get; set; }
        public string Remark { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int MemberClientTradeId { get; set; }
        // public virtual ExchangeActor ExchangeActor { get; set; }
        public int ClientTypeId { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
       // public virtual Commodity Commodity { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public Region Region { get; set; }
        public Zone Zone { get; set; }
        public Woreda Woreda { get; set; }
        public Kebele Kebele { get; set; }
        public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetails { get; set; }
        public virtual ICollection<ClientProduct> ClientProduct { get; set; }

    }
    public class MemberClientInformationDTO: BaseEntity
    {
        //  public int Id { get; set; }
        //   public string Description { get; set; }
        public int MemberClientInformationId { get; set; }
        public string BusinessSector { get; set; }
        public DateTime AgreementDate { get; set; }
        public int Gender { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        public string HouseNo { get; set; }
        public string RegularPhone { get; set; }
        public string MobilePhone { get; set; }
        public string TradeService { get; set; }
       // public int CommodityId { get; set; }
        public int BusinessFiledId { get; set; }
        public int Status { get; set; }
        public int TradeTypeId { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int MemberClientTradeId { get; set; }
        public int ClientTypeId { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        // public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public List<ClientProductDTO> ClientProduct { get; set; }
    }
    public class ClientInformationViewModel
    {
        public int MemberClientTradeId { get; set; }
        public int MemberClientInformationId { get; set; }
        public string  FullName { get; set; }
        public int BusinessSector { get; set; }
        public string DateAgreement { get; set; }
        public DateTime? AgreementDate { get; set; }
        public string RegularPhone { get; set; }
        public string MobilePhone { get; set; }
        public int TradeTypeId { get; set; }
       // public int CommidityTypeId { get; set; }
        //public string CommodityName { get; set; }
        public int BusinessFiledId { get; set; }
        public int Gender { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        public string RegionName { get; set; }
        public string ZoneName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string HouseNo { get; set; }
        public int Status { get; set; }
       // public int CommodityId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string FirstNameAmh { get; set; }
        public string FirstNameEng { get; set; }
        public string FatherNameAmh { get; set; }
        public string FatherNameEng { get; set; }
        public string GrandFatherNameEng { get; set; }
        public string GrandFatherNameAmh { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ClientTypeId { get; set; }
        public string OrganizationNameAmh { get; set; }
        public string OrganizationNameEng { get; set; }
        public string PreparedByFullName { get; set; }
        public string PreparedDate { get; set; }
        public string Position { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public string ECXCode { get; set; }
        public List<ClientProductViewModel> CommodityName { get; set; }
        public List<ClientProductDTO> ClientProduct { get; set; }

    }
    public class MembersTotalClient
    {
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public int ClientActive { get; set; }
        public int ClientPassive { get; set; }
    }
}
