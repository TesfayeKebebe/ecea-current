﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class MemberClientTrade : BaseEntity
    {
        public MemberClientTrade()
        {
            MemberClientTradeDetail = new HashSet<MemberClientTradeDetail>();
            MemberClientInformation = new HashSet<MemberClientInformation>();
            MemberTradeEvidence = new HashSet<MemberTradeEvidence>();
            MemberTradeFinancial = new HashSet<MemberTradeFinancial>();
            WorkFlow = new HashSet<WorkFlow>();
        }
        public int MemberClientTradeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ReportTypeId { get; set; }
        public string Remark { get; set; }
        public int TradeExcutionNotAccomplish { get; set; }
        public int IsTradeExcutionAccomplished { get; set; }
        public DateTime ReportDate { get; set; }
        public int ReportPeriodId { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public int Year { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
        public string PreparedByFirstName { get; set; }
        public string PreparedByFirstNameAmh { get; set; }
        public string PreparedByFatherName { get; set; }
        public string PreparedByFatherNameAmh { get; set; }
        public string PreparedByGrandFatherName { get; set; }
        public string PreparedByGrandFatherNameAmh { get; set; }
        public DateTime PreparedDate { get; set; }
        public string Position { get; set; }
        public string PositionAmh { get; set; }
        public virtual ReportType ReportType { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual ReportPeriod ReportPeriod { get; set; }
        public virtual TradeExcutionStatusType TradeExcutionStatusType { get; set; }
        public int? WorkFlowStatus { get; set; }
        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
        public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetail { get; set; }
        public virtual ICollection<MemberClientInformation> MemberClientInformation { get; set; }
        public virtual ICollection<MemberTradeEvidence> MemberTradeEvidence { get; set; }
        public virtual ICollection<MemberTradeFinancial> MemberTradeFinancial { get; set; }
    }
    public class MemberClientTradeDTO
    {
        public int MemberClientTradeId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string ReportTypeName { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
        public string DateReport { get; set; }
        public string MobilePhone { get; set; }
        public string RegularPhone { get; set; }
        public bool TradeNotAccomplished { get; set; }
        public bool SelfTradeNotAccomplished { get; set; }
        public bool ClientTradeNotAccomplished { get; set; }
        public string Remark { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int? TradeExcutionStatusTypeId { get; set; }
        public int TradeExcutionNotAccomplish { get; set; }
        public int IsTradeExcutionAccomplished { get; set; }
        public int Year { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int WorkFlowStatus { get; set; }
        public string PreparedByFirstName { get; set; }
        public string PreparedByFirstNameAmh { get; set; }
        public string PreparedByFatherName { get; set; }
        public string PreparedByFatherNameAmh { get; set; }
        public string PreparedByGrandFatherName { get; set; }
        public string PreparedByGrandFatherNameAmh { get; set; }
        public DateTime PreparedDate { get; set; }
        public string Position { get; set; }
        public string PositionAmh { get; set; }
        public virtual ReportType ReportType { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual List<WorkFlowDTO> WorkFlow { get; set; }
        public int TotalNoMembersVioaltion { get; set; }
        public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetails { get; set; }

    }
    public class MemberTradeLateReportView
    {
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string DateReport { get; set; }
        public string MobilePhone { get; set; }
        public string RegularPhone { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int TotalNoMembersVioaltion { get; set; }
    }
    
    public class MemberTradeExecutionViewModel
    {
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobliePhone { get; set; }
        public string RegularPhone { get; set; }
        public int TotalNoMembersVioaltion { get; set; }
        // public int ReportTypeId { get; set; }
    }
    public class TradeExcutionReportDTO
    {
        public int MemberClientTradeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ReportTypeId { get; set; }
        public int TradeExcutionNotAccomplish { get; set; }
        public int IsTradeExcutionAccomplished { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
        public string Remark { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int ReportPeriodId { get; set; }
        public int? TradeExcutionStatusTypeId { get; set; }
        public int Year { get; set; }
        public int WorkFlowStatus { get; set; }
        public string PreparedByFirstName { get; set; }
        public string PreparedByFirstNameAmh { get; set; }
        public string PreparedByFatherName { get; set; }
        public string PreparedByFatherNameAmh { get; set; }
        public string PreparedByGrandFatherName { get; set; }
        public string PreparedByGrandFatherNameAmh { get; set; }
        public DateTime PreparedDate { get; set; }
        public string Position { get; set; }
        public string PositionAmh { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public virtual List<WorkFlowDTO> WorkFlow { get; set; }
        public List<MemberClientTradeDetailDTO> MemberClientTradeDetail { get; set; }
        public virtual ICollection<MemberClientInformation> MemberClientInformation { get; set; }
        public virtual ICollection<MemberTradeFinancial> MemberTradeFinancial { get; set; }

    }

    public class MemberTradeExcutionDTO
    {
        public int MemberClientTradeId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string ReportType { get; set; }
        public Guid ExchangeActorId { get; set; }
        public DateTime? ReportDate { get; set; }
        public string DateReport { get; set; }
        public string ReportPeriod { get; set; }
        public int Year { get; set; }
        public string TradeStatus { get; set; }
        public int CustomerId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid UpdatedUserId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int ReportTypeId { get; set; }
        public string Remark { get; set; }
        public int WorkFlowStatus { get; set; }
        public int TradeExcutionNotAccomplish { get; set; }
        public int IsTradeExcutionAccomplished { get; set; }
        public int ReportPeriodId { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public string PreparedByFirstName { get; set; }
        public string PreparedByFirstNameAmh { get; set; }
        public string PreparedByFatherName { get; set; }
        public string PreparedByFatherNameAmh { get; set; }
        public string PreparedByGrandFatherName { get; set; }
        public string PreparedByGrandFatherNameAmh { get; set; }
        public DateTime PreparedDate { get; set; }
        public string Position { get; set; }
        public string PositionAmh { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
    }

}
