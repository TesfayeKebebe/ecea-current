﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class MemberClientTradeDetail : BaseEntity
    {
        public int MemberClientTradeDetailId { get; set; }
        public int? MemberClientInformationId { get; set; }
        public int TradeTypeId { get; set; }
        public int CommudityTypeId { get; set; }
        public string MemberTradeRemark { get; set; }
        public DateTime TradeDate { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public int MemberClientTradeId { get; set; }
        public int CommodityGradeId { get; set; }
        // public int CustomerLegalBodyId { get; set; }
        public int OwnerManagerId { get; set; }
        public string OrderPrice { get; set; }
        public double CommissionInPrice { get; set; }
        public string Warehouse { get; set; }
        public string ClientTradeRemark { get; set; }
        // public bool SelfTradeExcuation { get; set; }
        public int CommodityId { get; set; }
        public int UnitMeasurementId { get; set; }

        public int TradeExcutionReport { get; set; }

        public virtual CommodityGrade CommodityGrade { get; set; }
        public virtual OwnerManager OwnerManager { get; set; }
        public virtual MemberClientInformation MemberClientInformation { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public virtual UnitMeasurement UnitMeasurement { get; set; }
    }
    public class MemberClientTradeDetailDTO: BaseEntity
    {
        public int MemberClientTradeDetailId { get; set; }
        public int? MemberClientInformationId { get; set; }
        public int TradeTypeId { get; set; }
        public int CommudityTypeId { get; set; }
        public string MemberTradeRemark { get; set; }
        public DateTime TradeDate { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public int MemberClientTradeId { get; set; }
        public int CommodityGradeId { get; set; }
        public string Warehouse { get; set; }
        public string ClientTradeRemark { get; set; }
        public int CommodityId { get; set; }
        public int OwnerManagerId { get; set; }
        public int UnitMeasurementId { get; set; }
        public int TradeExcutionReport { get; set; }
        public double OrderPrice { get; set; }
        public double CommissionInPrice { get; set; }
        public virtual CommodityGrade CommodityGrade { get; set; }
        //public virtual CommodityType CommodityType { get; set; }
        public virtual OwnerManager OwnerManager { get; set; }
        //public virtual MemberClientInformation MemberClientInformation { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public virtual UnitMeasurement UnitMeasurement { get; set; }

    }
    public class MembersTradeExecutionReportPeriod
    {
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string DateReport { get; set; }
        public string MobilePhone { get; set; }
        public string RegularPhone { get; set; }
        public int MemberClientTradeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ReportPeriodId { get; set; }
        public int Year { get; set; }
    }
    public class BoughtSelfTradeExecutionDTO
    {
        public int MemberClientTradeId { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string DateReport { get; set; }
        public string MobilePhone { get; set; }
        public string RegularPhone { get; set; }
        public string EcxCode { get; set; }
        public string TradeDate { get; set; }
        public string BayerCommodityType { get; set; }
        public string BayerCommodityGrade { get; set; }
        public string BayerOrderPrice { get; set; }
        public double BayerUnitPrice { get; set; }
        public string Reperesentative { get; set; }
        public string Warehouse { get; set; }
        public double CommissionInPercent { get; set; }
        public double LotQuantity { get; set; }
        public string CommodityName { get; set; }
        public string ClientFullName { get; set; }
        public int MemberClientTradeDetailId { get; set; }
        public Guid ExchangeActorId { get; set; }
    }
    public class SoldSelfTradeExecutionDTO
    {
        public string TradeDate { get; set; }
        public string SellerCommodityType { get; set; }
        public string SellerCommodityGrade { get; set; }
        public string SellerOrderPrice { get; set; }
        public double SellerUnitPrice { get; set; }
        public string Reperesentative { get; set; }
        public string Warehouse { get; set; }
        public double CommissionInPercent { get; set; }
        public double LotQuantity { get; set; }
    }
    public class MemberTradeDetailViewModel
    {
        public Guid ExchangeActorId { get; set; }
        public int MemberClientTradeId { get; set; }
        public int? MemberClientInformationId { get; set; }
        public int MemberClientTradeDetailId { get; set; }
        public string ClientFullName { get; set; }
        public int TradeTypeId { get; set; }
        public string CommodityName { get; set; }
        public string CommodityTypeName { get; set; }
        public string CommodityGradeName { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public string DateTrade { get; set; }
        public DateTime? TradeDate { get; set; }
        public string FloorRepresentativeName { get; set; }
        public int CommudityTypeId { get; set; }
        public int CommodityGradeId { get; set; }
        //   public int CustomerLegalBodyId { get; set; }
        public int OwnerManagerId { get; set; }
        public string OrderPrice { get; set; }
        public double CommissionInPrice { get; set; }
        public string Warehouse { get; set; }
        public string ClientTradeRemark { get; set; }
        public string MemberTradeRemark { get; set; }
        public bool SelfTradeExcuation { get; set; }
        public int CommodityId { get; set; }
        public int UnitMeasurementId { get; set; }
        public int TradeExcutionReport { get; set; }
        public int ClientTypeId { get; set; }

    }
    public class TradeExcutionReportViewModel
    {
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string MobilePhone { get; set; }
        public string RegularPhone { get; set; }
        public string CommodityName { get; set; }
        public string CommodityTypeName { get; set; }
        public string CommodityGradeName { get; set; }
        public double OrderPrice { get; set; }
        public double UnitPrice { get; set; }
        public DateTime? TradeDate { get; set; }
        public string FloorRepresentativeName { get; set; }
        public double Quantity { get; set; }
        public string ClientFullName { get; set; }
    }
    public class ClientTradeQueryParameters
    {
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public int UnitMeasurment { get; set; }
        public int ReportTypeId { get; set; }
        public string OrganizationName { get; set; }
        public string ECXCode { get; set; }
        public int TradeExcutionReport { get; set; }
        public int TradeExcutionNotAccomplish { get; set; }
        public int Status { get; set; }
        public int TradeType { get; set; }
        public int ReportPeriodId { get; set; }
        public int Year { get; set; }
        public string FullName { get; set; }
        public int FieldBusinessId { get; set; }
        public int IsTradeExcutionAccomplished { get; set; }
        public int MemeberCategoryId { get; set; }
        public int FinancialPerformanceId { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public int CommodityId { get; set; }
        public int CommodityTypeId { get; set; }
        public int CommodityGradeId { get; set; }
        public double Quantity { get; set; }
        public string Lang { get; set; }
        public int ViolationTypeId { get; set; }
        public int DecisionTypeId { get; set; }
        public int CustomerTypeId { get; set; }
        public int AnnualBudgetCloserId { get; set; }
        public int MemberClientTradeId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int QuantityFrom { get; set; }
        public int QuantityTo { get; set; }
        public int ReportAnalysisId { get; set; }
    }

}
