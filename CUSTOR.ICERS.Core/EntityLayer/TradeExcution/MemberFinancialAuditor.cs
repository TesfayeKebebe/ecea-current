﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class MemberFinancialAuditor: BaseEntity
    {
        public MemberFinancialAuditor()
        {
            FinancialAuditoredFileUpload = new HashSet<FinancialAuditoredFileUpload>();
        }        
        public string MemberFinancialAuditorId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string Ecxcode { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public int AnnualBudgetCloserId { get; set; }
        public int Year { get; set; }
        public int CustomerTypeId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        [NotMappedAttribute]
        public List<IFormFile> ActualFile { get; set; }
        public virtual TradeExcutionStatusType TradeExcutionStatusType { get; set; }
        public virtual ICollection<FinancialAuditoredFileUpload> FinancialAuditoredFileUpload { get; set; }
    }
    public class MemberFinancialAuditorDTO: BaseEntity
    {
        public string MemberFinancialAuditorId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string Ecxcode { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int AnnualBudgetCloserId { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public int Year { get; set; }
        public int CustomerTypeId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [NotMappedAttribute]
        public List<IFormFile> ActualFile { get; set; }
        public virtual TradeExcutionStatusType TradeExcutionStatusType { get; set; }
        public virtual ICollection<FinancialAuditoredFileUpload> FinancialAuditoredFileUpload { get; set; }
    }
   public class MemberFinancialAuditorView
    {
        public Guid ExchangeActorId { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public string Ecxcode { get; set; }
        public string OrganizationName { get; set; }
        public string DateReport { get; set; }
        public string Remark { get; set; }
        public AuditorOrganizationalName AuditorName { get; set; }
        public string AnnualAuditStatus { get; set; }
        public int TradeExcutionStatusTypeId { get; set; }
        public int Status { get; set; }
        public int AnnualBudgetCloserId { get; set; }
        public int Year { get; set; }
        public int CustomerTypeId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CustomerType { get; set; }
        public DateTime ReportDate { get; set; }
    } 
    public class MemberFinancialAuditorViewModel
    {
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobileNo { get; set; }
        public string RegularPhone { get; set; }
        //public int ReportTypeId { get; set; }
        //public int ReportPeriodId { get; set; }
        public int TotalNoMembersVioaltion { get; set; }
    }
    public class MemberFinancialAuditorLateViewModel
    {
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobileNo { get; set; }
        public string RegularPhone { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int TotalNoMembersVioaltion { get; set; }
    }
    public class AuditorOrganizationalName
    {
        public string OrganizationName { get; set; }
    }
    public class ExchangeActorStatusSearchQuery
    {
        public int CustomerTypeId { get; set; }
        public int AnnualBudgetCloserId { get; set; }
        public int ExchageActorStatus { get; set; }
        public string Lang { get; set; }
    }
     public class TotalQuantyOfExchangeactor
    {
        public string ExchangeActorStatus { get; set; }
        public int Quantity { get; set; }
    }
    public class NetWorthBetweenMemberAuditor
    {
        public string OrganizationName { get; set; }
        public decimal NetWorth { get; set; }
        public string ECXcode { get; set; }
        public string AuditorName { get; set; }
        public decimal AuditorNetWorth { get; set; }
    }
}
