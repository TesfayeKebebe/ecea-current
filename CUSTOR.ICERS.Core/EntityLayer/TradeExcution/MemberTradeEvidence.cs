﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
  public partial  class MemberTradeEvidence: BaseEntity
    {
        public MemberTradeEvidence()
        {

        }
        public int MemberTradeEvidenceId { get; set; }
         public int MemberClientTradeId { get; set; }
        public string EvidenceContent { get; set; }
        public string Url { get; set; }
        public string FileLocation { get; set; }
        public int FileType { get; set; }
        public string Remark { get; set; }
        public int AuditorId { get; set; }
    }
    public partial class MemberTradeEvidenceDTo
    {
        public int MemberTradeEvidenceId { get; set; }
        public int MemberClientTradeId { get; set; }
        //  public int MemberTradeFinancialId { get; set; }
        public string EvidenceContent { get; set; }
        public string Url { get; set; }
        public string FileLocation { get; set; }
        public int FileType { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedUserId { get; set; }
        public int UpdatedUserId { get; set; }
        public int AuditorId { get; set; }

    }
    public partial class AttachmentPostModel
    {
        public AttachmentPostModel()
        {

        }
        public int? AttachmentId { get; set; }
        [Required]
        public int ParentId { get; set; }
        public string AttachmentContent { get; set; }
        public string SelectedWord { get; set; }
        public int? FileType { get; set; }
        public string Remark { get; set; }
        public IFormFile ActualFile { get; set; }
        public int AuditorId { get; set; }

    }

    public partial class AttachmentViewModel
    {
        public int AttachmentId { get; set; }
        public int ParentId { get; set; }
        public string AttachmentContent { get; set; }
        public string SelectedWord { get; set; }
        public string Remark { get; set; }
        public string Url { get; set; }
        public int? FileType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int AuditorId { get; set; }
    }
}
