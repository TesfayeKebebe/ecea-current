using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class MemberTradeFinancial : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MemberTradeFinancialId { get; set; }
        public int MemberClientTradeId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal DepositsMoney { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal CollectedPayment { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Stock { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AdvancePayment { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PerShare { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Building { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Vehicle { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Tools { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal OfficeFurniture { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PayableDebts { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal OverDraft { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AccountsPriority { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal LongTermLoanPayable { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ShortTermLoan { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MortgageLoan { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal LongTermloanFromfinancial { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalIncome { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalPerShare { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalFixedAsset { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalWealth { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalTemporaryDebts { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalDebts { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetAssets { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalLongTermDebts { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Computerandaccessories { get; set; }
        public int InJectedStatus { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public virtual MemberFinancialAuditor MemberFinancialAuditor { get; set; }
    }
    public class MemberTradeFinancialDTO: BaseEntity
    {

        public int MemberTradeFinancialId { get; set; }
        public int MemberClientTradeId { get; set; }
        public decimal DepositsMoney { get; set; }
        public decimal CollectedPayment { get; set; }
        public decimal Stock { get; set; }
        public decimal AdvancePayment { get; set; }
        public decimal PerShare { get; set; }
        public decimal Building { get; set; }
        public decimal Vehicle { get; set; }
        public decimal Tools { get; set; }
        public decimal OfficeFurniture { get; set; }
        public decimal PayableDebts { get; set; }
        public decimal OverDraft { get; set; }
        public decimal AccountsPriority { get; set; }
        public decimal LongTermLoanPayable { get; set; }
        public decimal ShortTermLoan { get; set; }
        public decimal MortgageLoan { get; set; }
        public decimal LongTermloanFromfinancial { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalPerShare { get; set; }
        public decimal TotalFixedAsset { get; set; }
        public decimal TotalWealth { get; set; }
        public decimal TotalTemporaryDebts { get; set; }
        public decimal TotalDebts { get; set; }
        public decimal NetAssets { get; set; }
        public decimal TotalLongTermDebts { get; set; }        
        public int InJectedStatus { get; set; }
        public decimal Computerandaccessories { get; set; }
    }
    public class MemberTradeFinancialViewModel
    {
        public Guid ExchangeActorId { get; set; }
        public int ReportPeriodId { get; set; }
        public int MemberTradeFinancialId { get; set; }
        public int MemberClientTradeId { get; set; }
        public decimal DepositsMoney { get; set; }
        public decimal CollectedPayment { get; set; }
        public decimal Stock { get; set; }
        public decimal AdvancePayment { get; set; }
        public decimal PerShare { get; set; }
        public decimal Building { get; set; }
        public decimal Vehicle { get; set; }
        public decimal Tools { get; set; }
        public decimal OfficeFurniture { get; set; }
        public decimal PayableDebts { get; set; }
        public decimal OverDraft { get; set; }
        public decimal AccountsPriority { get; set; }
        public decimal LongTermLoanPayable { get; set; }
        public decimal ShortTermLoan { get; set; }
        public decimal MortgageLoan { get; set; }
        public decimal LongTermloanFromfinancial { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalPerShare { get; set; }
        public decimal TotalFixedAsset { get; set; }
        public decimal TotalWealth { get; set; }
        public decimal TotalTemporaryDebts { get; set; }
        public decimal TotalDebts { get; set; }
        public decimal NetAssets { get; set; }
        public string CustomerFullName { get; set; }
        public decimal TotalLongTermDebts { get; set; }
        public string OrganizationName { get; set; }
        public decimal Computerandaccessories { get; set; }
        public string MemberFinancialAuditorId { get; set; }
        public int TotalNoMembersVioaltion { get; set; }

    }
    public class MemberTradeFinancialPerformanceModel
    {
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalPerShare { get; set; }
        public decimal TotalFixedAsset { get; set; }
        public decimal TotalWealth { get; set; }
        public decimal TotalTemporaryDebts { get; set; }
        public decimal TotalDebts { get; set; }
        public decimal TotalLongTermDebts { get; set; }
        public decimal NetAssets { get; set; }
    }
    public class ExchangeActorFinacialComparative
    {
        public int Year { get; set; }
        public string OrganizationName { get; set; }
        public decimal TotalWealth { get; set; }
        public decimal TotalDebts { get; set; }
        public decimal NetWorth { get; set; }
        public decimal TotalWealthInPercent { get; set; }
        public decimal TotalDebtsInPercent { get; set; }
        public decimal NetWorthInPercent { get; set; }
    }
    public class FinancialReportSearchQuery
    {
        public string Lang { get; set; }
        public int CustomerTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int Year { get; set; }
    }
    public class FinancialNetWorthCalculation
    {
        public string OrganizationName { get; set; }
        public decimal DepositsMoneyInPercent { get; set; }
        public decimal CollectedPaymentInPercent { get; set; }
        public decimal StockInPercent { get; set; }
        public decimal AdvancePaymentInPercent { get; set; }
        public decimal PerShareInPercent { get; set; }
        public decimal BuildingInPercent { get; set; }
        public decimal VehicleInPercent { get; set; }
        public decimal ToolsInPercent { get; set; }
        public decimal OfficeFurnitureInPercent { get; set; }
        public decimal ComputerandaccessoriesInPercent { get; set; }
    }
}
