﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class MemberTradeViolation : BaseEntity
    {
        public MemberTradeViolation()
        {
            TradeExcutionViolationRecord = new HashSet<TradeExcutionViolationRecord>();
            WorkFlow = new HashSet<WorkFlow>();
        }
        public int MemberTradeViolationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ViolationStatus { get; set; }
        public int? Status { get; set; }
        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
        public virtual ICollection<TradeExcutionViolationRecord> TradeExcutionViolationRecord { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class MemberTradeViolationDTO: BaseEntity
    {
        public int MemberTradeViolationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int ViolationStatus { get; set; }
        public int Status { get; set; }
        public virtual List<WorkFlowDTO> WorkFlow { get; set; }
        public virtual List<TradeExcutionViolationRecord> TradeExcutionViolationRecord { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }

    }
}
