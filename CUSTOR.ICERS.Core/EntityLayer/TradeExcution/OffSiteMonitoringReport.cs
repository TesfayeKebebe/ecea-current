﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class OffSiteMonitoringReport: BaseEntity
    {
        public OffSiteMonitoringReport()
        {
            OffSiteMonitoringReportDetail = new HashSet<OffSiteMonitoringReportDetail>();
            WorkFlowModel = new HashSet<WorkFlow>();
        }
        public int OffSiteMonitoringReportId { get; set; }
        public int ReportPeriodId { get; set; }
        public int Year { get; set; }
        public int ReportTypeId { get; set; }
        public int ReasonTypeId { get; set; }
        [ForeignKey("ReasonTypeId")]
        public Lookup.Lookup ReasonType { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int CustomerTypeId { get; set; }
        public virtual ReportType ReportType { get; set; }
        public virtual ReportPeriod ReportPeriod { get; set; }
        public virtual ICollection<WorkFlow> WorkFlowModel { get; set; }
        public virtual ICollection<OffSiteMonitoringReportDetail> OffSiteMonitoringReportDetail { get; set; }
    }
    public class OffSiteMonitoringReportDTO: BaseEntity
    {    
    public int OffSiteMonitoringReportId { get; set; }
    public int ReportPeriodId { get; set; }
    public int Year { get; set; }
    public int ReportTypeId { get; set; }
    public int ReasonTypeId { get; set; }
    public string Remark { get; set; }
    public int Status { get; set; }
    public int CustomerTypeId { get; set; }
    public virtual ICollection<WorkFlow> WorkFlowModel { get; set; }
    public virtual ICollection<OffSiteMonitoringReportDetail> OffSiteMonitoringReportDetail { get; set; }
}
}
