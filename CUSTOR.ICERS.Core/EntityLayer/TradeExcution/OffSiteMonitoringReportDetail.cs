﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
   public class OffSiteMonitoringReportDetail: BaseEntity
    {
        public int OffSiteMonitoringReportDetailId { get; set; }
        public int OffSiteMonitoringReportId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int WarningStatus { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public  OffSiteMonitoringReport OffSiteMonitoringReport { get; set; }
    }
    public class OffSiteMonitoringReportDetailDTO 
    {
        public int OffSiteMonitoringReportDetailId { get; set; }
        public int OffSiteMonitoringReportId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public int WarningStatus { get; set; }
        public virtual OffSiteMonitoringReport OffSiteMonitoringReport { get; set; }

    }
    public class ExchangeActorOffSiteReportView
    {
        public int OffSiteMonitoringReportId { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReasonTypeId { get; set; }
        public string ReasonType { get; set; }
        public int Year { get; set; }
        public string ReportType { get; set; }
        public string ReportPeriod { get; set; }
        public string Remark { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobliePhone { get; set; }
        public string RegularPhone { get; set; }
        public int ApprovalStatus { get; set; }
        public int WarningStatus { get; set; }
    }
    public class OffSiteMonitoringQueryParameters
    {
        public Guid ExchangeActorId { get; set; }
        public int OffSiteMonitoringId { get; set; }
        public string Lang { get; set; }
        public int PageCount { get; set; }
        public int PageNumber { get; set; }
        public int ReportAnalysisId { get; set; }
        public int CustomerTypeId { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportTypeId { get; set; }
    }
    public class OffSetMonitoringWarningStatus
    {
        public Guid ExchangeActorId { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportPeriodId { get; set; }
        public int Year { get; set; }
        public int ReasonTypeId { get; set; }
        public int StatusViewRecord { get; set; }
    }
}
