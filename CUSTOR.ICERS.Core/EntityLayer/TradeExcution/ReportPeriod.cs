﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class ReportPeriod : BaseEntity
    {
        public ReportPeriod()
        {
            MemberClientTrades = new HashSet<MemberClientTrade>();
        }
        public int ReportPeriodId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
        public string StartMonthAmh { get; set; }
        public string StartMonthEng { get; set; }
        public string EndMonthAmh { get; set; }
        public string EndMonthEng { get; set; }
        public virtual ICollection<MemberClientTrade> MemberClientTrades { get; set; }
    }
    public class ReportPeriodDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class ReportPeriodViewModel: BaseEntity
    {
        public int ReportPeriodId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime StartDate { get; set; }
        public string StartMonthAmh { get; set; }
        public string StartMonthEng { get; set; }
        public string EndMonthAmh { get; set; }
        public string EndMonthEng { get; set; }
        public string EndDate { get; set; }
        public string BeginningDate { get; set; }
    }
    public class ReportPeriodSettingViewModel: BaseEntity
    {
        public int ReportPeriodId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public string DeadLine { get; set; }
        public string StartDate { get; set; }
        public string StartMonthAmh { get; set; }
        public string StartMonthEng { get; set; }
        public string EndMonthAmh { get; set; }
        public string EndMonthEng { get; set; }
        public string EndDate { get; set; }
        public string BeginningDate { get; set; }
    }
}
