﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class ReportType: BaseEntity
    {
        public ReportType()
        {
            this.MemberClientTrades = new HashSet<MemberClientTrade>();
        }

        public int ReportTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public virtual ICollection<MemberClientTrade> MemberClientTrades { get; set; }



    }
    public class ReportTypeDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class ReportTypeviewDTO:BaseEntity
    {
        public int ReportTypeId { get; set; }
        public string DescriptionAmh { get; set;}
        public string DescriptionEng { get; set; }


    }
}
