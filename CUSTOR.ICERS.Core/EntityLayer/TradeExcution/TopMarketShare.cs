﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class TopMarketShare
    {
        public string MemberName { get; set; }
        public decimal Quantity { get; set; }
    }
    public class TopMarketShareViewModel
    {
        public string MemberName { get; set; }
        public decimal MarketShareInPercent { get; set; }
        public decimal Others { get; set; }
        public decimal HHIIndex { get; set; }
    }
}
