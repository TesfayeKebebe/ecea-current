﻿using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class TradeExcutionStatusType
    {
        public TradeExcutionStatusType()
        {
            MemberClientTrades = new HashSet<MemberClientTrade>();
        }
        public int TradeExcutionStatusTypeId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public virtual ICollection<MemberClientTrade> MemberClientTrades { get; set; }
    }
    public class TradeExcutionStatusTypeDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
