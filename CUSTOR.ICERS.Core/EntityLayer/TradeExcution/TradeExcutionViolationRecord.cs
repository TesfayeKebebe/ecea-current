﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public class TradeExcutionViolationRecord : BaseEntity
    {

        public int TradeExcutionViolationRecordId { get; set; }
        public int ReasonId { get; set; }
        public int ReportAnalysisId { get; set; }
        public int ViolationRecordId { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int CheckerDecision { get; set; }
        public int ApproverDecision { get; set; }
        public string PreparedBy { get; set; }
        public string CheckedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string CheckerRemark { get; set; }
        public string ApproverRemark { get; set; }
        public DateTime CheckedDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int MemberTradeViolationId { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportTypeId { get; set; }
        //public Guid ExchangeActorId { get; set; }
        //public string AccountNo { get; set; }
        //public decimal Amount { get; set; }
        //public DateTime DateOfError { get; set; }
        public virtual MemberTradeViolation MemberTradeViolation { get; set; }
        //public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class ViolationRecordStatusChangeDTO : BaseEntity
    {
        public Guid ExchangeActorId { get; set; }
    }
    public class TradeExcutionViolationRecordDTO : BaseEntity
    {
        public int TradeExcutionViolationRecordId { get; set; }
        public int ReasonId { get; set; }
        public int ViolationRecordId { get; set; }
        public string Remark { get; set; }
        public int MemberTradeViolationId { get; set; }
        public int Status { get; set; }
        public int CheckerDecision { get; set; }
        public int ApproverDecision { get; set; }
        public string PreparedBy { get; set; }
        public string CheckedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string CheckerRemark { get; set; }
        public string ApproverRemark { get; set; }
        public DateTime CheckedDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportAnalysisId { get; set; }
        public int ReportTypeId { get; set; }
        //public Guid ExchangeActorId { get; set; }
        //public string AccountNo { get; set; }
        //public decimal Amount { get; set; }
        //public DateTime DateOfError { get; set; }
        public virtual MemberTradeViolation MemberTradeViolation { get; set; }
       // public virtual ExchangeActor ExchangeActor { get; set; }
    }
    public class TradeExcutionViolationRecordView
    {
        public int MemberTradeViolationId { get; set; }
        public int TradeExcutionViolationRecordId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public int ReasonId { get; set; }
        public int ViolationRecordId { get; set; }
        public string ReportPeriod { get; set; }
        public string CreatedDate { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int CheckerDecision { get; set; }
        public int ApproverDecision { get; set; }
        public string PreparedBy { get; set; }
        public string CheckedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string CheckerRemark { get; set; }
        public string ApproverRemark { get; set; }
        public DateTime CheckedDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportAnalysisId { get; set; }
        public int ReportTypeId { get; set; }
    }
    public class MemberTradeViolationView
    {
        public int TradeExcutionViolationRecordId { get; set; }
        public int MemberTradeViolationId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public string CustomerFullName { get; set; }
        public string OrganizationName { get; set; }
        public string CustomerType { get; set; }
        public string MobliePhone { get; set; }
        public string RegularPhone { get; set; }
        public string PreparedDate { get; set; }
        public string StartDate { get; set; }
        public int ApprovalStatus { get; set; }
        public int DecisionType { get; set; }
        public int BankOversightViolationId { get; set; }
        public int Year { get; set; }
        public int ReportPeriodId { get; set; }
        public int ReportTypeId { get; set; }
    }
    public class BankOverSightViolationViewModel
    {
        public int MemberTradeViolationId { get; set; }
        public Guid? ExchangeActorId { get; set; }
        public int TradeExcutionViolationRecordId { get; set; }
        public DateTime DateOfError { get; set; }
        public string OrganizationName { get; set; }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        public int FaultTypeId { get; set; }
        public int DecisionTypeId { get; set; }
        public string ErrorOcuredDueto { get; set; }
        public string ErrorOfDate { get; set; }
        public string ECXCode { get; set; }
        public string CustomerType { get; set; }
        public string FullName { get; set; }
    }
}