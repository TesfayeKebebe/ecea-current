﻿using System;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class TradeType
    {
        //public TradeType()
        //{
        //    this.MemberClientTradeDetails = new HashSet<MemberClientTradeDetail>();
        //}

        public int TradeTypeId { get; set; }
        public string TradeTypeAmh { get; set; }
        public string TradeTypeEng { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int CreatedUserId { get; set; }
        public int UpdatedUserId { get; set; }
        //public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetails { get; set; }
    }
}
