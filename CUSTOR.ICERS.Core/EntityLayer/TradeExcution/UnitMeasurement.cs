﻿using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.TradeExcution
{
    public partial class UnitMeasurement
    {
        public UnitMeasurement()
        {
            MemberClientTradeDetails = new HashSet<MemberClientTradeDetail>();
        }

        public int UnitMeasurementId { get; set; }
        public string DescriptionAmh { get; set; }
        public string DescriptionEng { get; set; }
        public virtual ICollection<MemberClientTradeDetail> MemberClientTradeDetails { get; set; }
    }
    public class UnitMeasurementDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
