using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class TradeTrend
    {
        [Key]
        public int TradeTrendId { get; set; }
        public DateTime TradeDate { get; set; }
        public string ProductType { get; set; }
        public string Symbol { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Volume { get; set; }
        public Guid TradeId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid CommodityGradeId { get; set; }
        public string Warehouse { get; set; }
        public int ProductionYear { get; set; }
        public double Differnce { get; set; }
        public Guid? CommodityId { get; set; }
        public Guid? CommodityTypeId { get; set; }
    }
    public class TradeTrendDTO
    {
        public int TradeTrendId { get; set; }
        public DateTime TradeDate { get; set; }
        public string ProductType { get; set; }
        public string Symbol { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Volume { get; set; }
        public Guid TradeId { get; set; }
        public Guid WarehouseId { get; set; }
        public string Warehouse { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public Guid CommodityId { get; set; }
        public Guid CommodityTypeId { get; set; }
        public Guid CommodityGradeId { get; set; }
        public int ProductionYear { get; set; }
    }
    public class TradeTrendView
    {
        public Guid TradeId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid CommodityId { get; set; }
        public Guid CommodityTypeId { get; set; }
        public Guid CommodityGradeId { get; set; }
        public string Warehouse { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public int ProductionYear { get; set; }
        public double PreviousClosingPrice { get; set; }
        public double ClosingPrice { get; set; }
        public double Differnce { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Volume { get; set; }
        public string Symbol { get; set; }
        public DateTime TradeDate { get; set; }
    }
    public class OpenningClosingPrice
    {
        public Guid CommodityId { get; set; }
        public Guid CommodityTypeId { get; set; }
        public int ProductionYear { get; set; }
        public decimal? OpenningPrice { get; set; }
        public decimal? ClosingPrice { get; set; }
    }
}
