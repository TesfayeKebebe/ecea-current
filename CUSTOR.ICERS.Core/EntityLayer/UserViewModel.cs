using CUSTOR.ICERS.Core.Security;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.Security
{

    public class UserEditViewModel4 : UserViewModel3
    {
        public string CurrentPassword { get; set; }

        //[MinLength(6, ErrorMessage = "New Password must be at least 6 characters")]
        public string NewPassword { get; set; }

        //private new bool? IsLockedOut { get; } //Hide base member
    }
    public class UserViewModel3
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [StringLength(200, MinimumLength = 2, ErrorMessage = "Username must be between 2 and 200 characters")]
        public string UserName { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(200, ErrorMessage = "Email must be at most 200 characters")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string ExchangeActorId { get; set; }

        public string Email { get; set; }

        public string Tin { get; set; }
        public string SiteCode { get; set; }

        public string PhoneNumber { get; set; }

        public string Configuration { get; set; }

        public bool? IsEnabled { get; set; }
        public bool? IsExisting { get; set; }

        public bool? IsLockedOut { get; set; }

        public List<string> Roles { get; set; }
    }
    //public class UsersViewModel3
    //{
    //    [Display(Name = "User")]
    //    public ApplicationUser User { get; set; }

    //    [Display(Name = "Roles")]
    //    public string Roles { get; set; }
    //}
    ////Todo: ***Using DataAnnotations for validations until Swashbuckle supports FluentValidation***
    //public class UserViewModelValidator : AbstractValidator<UserViewModel>
    //{
    //    public UserViewModelValidator()
    //    {
    //        //Validation logic here
    //        RuleFor(user => user.UserName).NotEmpty().WithMessage("Username cannot be empty");
    //        RuleFor(user => user.Email).EmailAddress().NotEmpty();
    //        RuleFor(user => user.Password).NotEmpty().WithMessage("Password cannot be empty").Length(4, 20);
    //    }
    //}
}
