﻿//using DevExpress.XtraRichEdit.Import.Doc;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.ViewModel
{
    public class TradeViewModel
    {
        public string commodity { get; set; }
        public string commodityType { get; set; }
        public string tradeDate { get; set; }
		public string symbol { get; set; }
		public double lot { get; set; }
		public double unitPrice { get; set; }
        public string buyerMemberName { get; set; }
        public string buyerRepName { get; set; }
        public string sellerMemberName { get; set; }
        public string sellerRepName { get; set; }
	}
}
