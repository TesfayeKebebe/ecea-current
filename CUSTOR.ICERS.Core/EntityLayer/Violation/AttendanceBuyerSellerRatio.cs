﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class AttendanceBuyerSellerRatio
    {
        public int Buyer { get; set; }
        public int Seller { get; set; }
        public int ClientBuyer { get; set; }
        public int ClientSeller { get; set; }
    }
    public class AttendanceBuyerSellerViewModel
    {
        public int Buyer { get; set; }
        public int Seller { get; set; }
        public int ClientBuyer { get; set; }
        public int ClientSeller { get; set; }
        public double ClientRatio { get; set; }
        public double Ratio { get; set; }
    }
}
