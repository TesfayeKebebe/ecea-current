using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class AttendanceVM
  {
    public DateTime? CreatedDate { get; set; }
    public string OrderType { get; set; }
    public string Warehouse { get; set; }
    public string MemberCode { get; set; }
    public string ClientName { get; set; }
    public string RepresentativeName { get; set; }
    public string CommodityGrade { get; set; }
    public string Class { get; set; }
    public string Commodity { get; set; }
    public string Symbol { get; set; }
  }
  public class PreopenSessionVM
  {
    public DateTime? sessionStart { get; set; }
    public DateTime? sessionEnd { get; set; }

    public decimal? Quantity { get; set; }
  }
  public class PreOpenDetails : BidOfferRecordVM
  {
    public decimal? PreOpenQuantity { get; set; }
    public DateTime? PreopenEndDate { get; set; }
    public DateTime? SessionStart { get; set; }
    public string Status { get; set; }
  }
  public class QuantityBeforeOpen
  {
    public decimal? Quantity { get; set; }
  }
  public class PreopenSessionDate
  {
    public DateTime? sessionStart { get; set; }
    public DateTime? sessionEnd { get; set; }
  }


}
