﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class AttendanceVolumeValueRatio
    {
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public bool IsClientOrder { get; set; }
        public byte TransactionType { get; set; }
    } 
    public class AttendanceVolumeValueRatioView {
        public decimal BuyerMemberQuantity { get; set; }
        public decimal BuyerMemberPrice { get; set; }
        public decimal SellerMemberQuantity { get; set; }
        public decimal SellerMemberPrice { get; set; }
        public decimal BuyerClientQuantity { get; set; }
        public decimal BuyerClientPrice { get; set; }
        public decimal SellerClientQuantity { get; set; }
        public decimal SellerClientPrice { get; set; }
        public decimal SellerClientRatio { get; set; }
        public decimal BuyerClientRatio { get; set; }
        public decimal BuyerMemberRatio { get; set; }
        public decimal SellerMemberRatio { get; set; }
    }
    public class AttendanceRatio
    {
        public string Date { get; set; }
        public decimal BuyerSellerRatio { get; set; }
        public decimal VolumeValueRatio { get; set; }

    }
   
}
