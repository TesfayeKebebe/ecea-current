using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class Auction
  {
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public string OrderType { get; set; }
    public string Warehouse { get; set; }
    public string MemberCode { get; set; }
    public string Member { get; set; }
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public Guid? SessionId { get; set; }
    public string RepName { get; set; }
    public string ClientName { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public Guid OrderId { get; set; }
    public Guid? WarehouseId { get; set; }
    public DateTime? OpenStart { get; set; }
    public DateTime? OpenEnd { get; set; }
    public string Symbol { get; set; }
    public string OrderStatus { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public DateTime? CreatedDate { get; set; }
    public DateTime? OnlineStartActualTime { get; set; }
    public Guid? MemberGuid { get; set; }
    public DateTime? SubmitedTimestamp { get; set; }

  }
  public class AuctionVM
  {
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public string OrderType { get; set; }
    public string Warehouse { get; set; }
    public string Member { get; set; }
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public string RepName { get; set; }
    public string ClientName { get; set; }
    public Guid OrderId { get; set; }
    public string Symbol { get; set; }
    public string OrderStatus { get; set; }
    public DateTime? SubmitedTimestamp { get; set; }
    public string HasViolation { get; set; }
    public int SN { get; set; }
    public string Time { get; set; }
    public string Date { get; set; }
    public string Violations { get; set; }
    public bool IsViolation { get; set; }
    public string ViewStatus { get; set; }
    public string SessionId { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public string Start { get; set; }
    public string Ends { get; set; }


  }

}
