using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class AuctionViolationRecord
  {
    [Key]
    public string Id { get; set; }
    public string ViolationId { get; set; }
    public string Type { get; set; }
    public DateTime CreatedDate { get; set; }
    public bool IsViewed { get; set; }
  public string  MemberId { get; set; }
  }
}
