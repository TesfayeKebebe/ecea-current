using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public  class BidOfferRecordVM
  {
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public int? ProductionYear { get; set; }
    public string OrderType { get; set; }
    public string Warehouse { get; set; }
    public string MemberCode { get; set; }
    public string Member { get; set; }
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public DateTime? SubmittedTimestamp { get; set; }
    public Guid? SessionId { get; set; }
    public Guid? MemberId { get; set; }
    public string RepName { get; set; }
    public string ClientName { get; set; }
    public DateTime? PreOpenStart { get; set; }
    public DateTime? PreOpenEnd { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public Guid OrderId { get; set; }
    public Guid? WarehouseId { get; set; }
    public DateTime? OpenStart { get; set; }
    public DateTime? OpenEnd { get; set; }
    public string Symbol { get; set; }
    public string OrderStatus { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }

  }
    public class BidOfferRecordDetail : BidOfferRecordVM
    {
        public bool IsViolation { get; set; }

    }
    public class BacketingViolation
    {
        public Guid? CommodityGradeId { get; set; }
        public int? TotalNoOfTrader { get; set; }
        public string WareHouse { get; set; }
    }

  public class AbnormalTrading
  {
    public string Buyer { get; set; }
    public string Seller { get; set; }
    public decimal? SelerPrice { get; set; }
    public decimal? BuyerPrice { get; set; }
    public decimal? ChangePrice { get; set; }
    public string CommodityGrade { get; set; }
    public int Counter { get; set; }
  }
}
