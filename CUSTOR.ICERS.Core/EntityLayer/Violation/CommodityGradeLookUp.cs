using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class CommodityGradeLookUp
  {
    public Guid Commodity { get; set; }
    public Guid Id { get; set; }
    public string Symbol { get; set; }
  }
}
