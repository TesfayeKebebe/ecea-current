using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class CommodityPriceLimit
  {
    public Guid? SessionId { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public Guid? WarehouseId { get; set; }
    public decimal? UpperPriceLimit { get; set; }
    public decimal? LowerPriceLimit { get; set; }

  }
}
