using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public class CommonViolation
  {
    [Key]
    public Guid? Id { get; set; }
    public Guid? TradeId { get; set; }
    public string Warehouse { get; set; }
    public string MemberCode { get; set; }
    public string ClientCode { get; set; }
    public string CommodityGrade { get; set; }
    public decimal? OrderQuantity { get; set; }
    public decimal? OrderPrice { get; set; }
    public decimal? TradeQuantity { get; set; }
    public decimal? TradePrice { get; set; }
    public DateTime? CreatedTimestamp { get; set; }
    public decimal? MaxAvgPrice { get; set; }
    public decimal? MinAvgPrice { get; set; }
    public string BuyerCode { get; set; }
    public string SellerCode { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }
    public bool HasMarkingToClose { get; set; }
    public bool IsNotRegistered { get; set; }
    public bool HasPriceChange { get; set; }
    public decimal? PriceChange { get; set; }
    public bool HasSisterCompany { get; set; }
    public decimal? PreOpenQuantity { get; set; }
    public DateTime? PreOpenEnd { get; set; }
    public bool HasNewRequestAfterPreOpen { get; set; }
    public bool HasPreArragedTrade { get; set; }
    public decimal? NewQuantityChange { get; set; }

    public decimal? LastPrice { get; set; }
    public DateTime? PreOpenStart { get; set; }
    public bool HasPriceVolumeBeyondLimit { get; set; }
    public decimal MinimumPrice { get; set; }
    public decimal MaximumPrice { get; set; }
    public decimal VolumeLimit { get; set; }
    public bool BuyerNotRegisterd { get; set; }
    public bool SellerNotRegisterd { get; set; }
    public string SellerClientName { get; set; }
    public string BuyerClientName { get; set; }
    public bool HasCancelled { get; set; }
    public bool HasSuspended { get; set; }
    public bool HasNotRenewed { get; set; }
    public bool HasBucketing { get; set; }
    public bool IsMatchTrade { get; set; }
    public bool HasPreArrangedDuringOpen { get; set; }
    public bool HasFrontRunning { get; set; }
    public decimal? CancelledPrice { get; set; }
    public decimal? ChangePrice { get; set; }
    public decimal? FollowerPrice { get; set; }
    public DateTime? CancelledTimestamp { get; set; }
    public string Status { get; set; }
    public string InjuctionBody { get; set; }
    public string Reason { get; set; }
    public DateTime? InjunctionEndDate { get; set; }
    public DateTime? InjunctionStartDate { get; set; }
    public DateTime? IssueDateTime { get; set; }
    public DateTime? ExpireDateTime { get; set; }
    public DateTime? CancellationDate { get; set; }
    public string CancellationType { get; set; }
    public bool HasAbnormalTrading { get; set; }
    public bool HasPriceLimit { get; set; }
    public decimal? UpperPriceLimit { get; set; }
    public decimal? LowerPriceLimit { get; set; }
  }




  public class CommonViolationDetail : CommonViolation
  {
    public int? ProductionYear { get; set; }
    public DateTime? SubmittedTimestamp { get; set; }
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public bool IsVoilation { get; set; } = false;
    public string BuyerRepName { get; set; }
    public string SellerRepName { get; set; }
    public double? Lot { get; set; }
    public double? Quintal { get; set; }
    public double? Bags { get; set; }
    public decimal? VolumeKGUsingSTD { get; set; }
    public string IsOnline { get; set; }
    public string ConsignmentType { get; set; }
    public string IsNewModel { get; set; }
    public DateTime? TradedTimestamp { get; set; }
    public string ViolationStatus { get; set; }
    public List<Violations> Violations { get; set; }
    public List<FrontRunningDetail> FrontRunning { get; set; }
    public List<AbnormalTrading> AbnormalTrading { get; set; }


  }
  public class ExchangeActorCode
  {
    public string Ecxcode { get; set; }
  }
  public class SuspendedExchange 
  {
    public string Ecxcode { get; set; }
    public string DescriptionEng { get; set; }
    public string Reason { get; set; }
    public DateTime? InjunctionEndDate { get; set; }
    public DateTime? InjunctionStartDate { get; set; }
  }
  public class RenewalInformation
  {
    public string Ecxcode { get; set; }
    public DateTime? IssueDateTime { get; set; }
    public DateTime? ExpireDateTime { get; set; }
  }
  public class CancellationInformation
  {
    public string Ecxcode { get; set; }
    public DateTime? CreatedDateTime { get; set; }
    public string CancellationType { get; set; }
  }
  public class Violations
  {
    public string MemberCode { get; set; }
    public string CommodityGrade { get; set; }
    public decimal? OrderQuantity { get; set; }
    public decimal? OrderPrice { get; set; }
    public DateTime? PreOpenEnd { get; set; }
    public DateTime? PreOpenStart { get; set; }
    public DateTime? SubmittedTimestamp { get; set; }

  }
}
