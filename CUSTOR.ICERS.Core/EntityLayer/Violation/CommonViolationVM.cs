using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class CommonViolationVM
  {
    public string CommodityGrade { get; set; }
    public string MemberId { get; set; }
    public string TradeDate { get; set; }
    public string TicketNo { get; set; }
    public int productionYear { get; set; }
    [Column(TypeName = "decimal(18,2)")]
    public decimal tradePrice { get; set; }
   
  }
}
