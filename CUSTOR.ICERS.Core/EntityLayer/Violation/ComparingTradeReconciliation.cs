﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class ComparingTradeReconciliation
    {
        public DateTime TradeDate { get; set; }
        public string SellerOrganizationName { get; set; }
        public string SellerRepresentativeName { get; set; }
       // public string SellerClientFullName { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string BuyerOrganizationName { get; set; }
        //public string BuyerClientFullName { get; set; }
        public string BuyerRepresentativeName { get; set; }
        public string StatusId { get; set; }
    }
    public class SearchParmsQuery
    {
        public byte ReconcilationTypeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string WarehouseId { get; set; }
        public Guid CommodityGradeId { get; set; }
        public double Ratio { get; set; }
        public string CommodityId { get; set; }
        public List<string> WareHouseId { get; set; }
    }
}
