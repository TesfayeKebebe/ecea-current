using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.DataAccessLayer.Violation
{
   public class ComparisonTradeByCommodityType
    {
        public string CommodityType { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }
    public class ComparisonTradeByCommodityTypeView
    {
        public string CommodityType { get; set; }
        public decimal? BeforeQuantity { get; set; }
        public decimal? BeforePrice { get; set; }
        public decimal? CurrentQuantity { get; set; }
        public decimal? CurrentPrice { get; set; }
        public decimal? DiffernceQuantity { get; set; }
        public decimal? DifferncePrice { get; set; }
    }
    public class ComparisonSearchSqlQuery
    {
        public DateTime BeforeFrom { get; set; }
        public DateTime BeforeTo { get; set; }
        public DateTime CurrentFrom { get; set; }
        public DateTime CurrentTo { get; set; }
        public Guid CommodityId { get; set; }
        public int TopValue { get; set; }
        public bool Order { get; set; }
    }
}
