using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public  class ContractMarketShare
  {
    public string Symbol { get; set; }
    public string CommodityGrade { get; set; }
    public decimal Quantity { get; set; }
    public string Commodity { get; set; }
    public string CreatedTimestamp { get; set; }
  }
  public class TotalSalesForMarket
  {
    public decimal? TotalQuantity { get; set; }
  }
  public class MarketShareDetail:ContractMarketShare
  {
    public decimal? TotalQuantity { get; set; }
    public decimal Share { get; set; }
    public string Commodity { get; set; }

  }
  public class WarehouseMarketShare
  {
    //public string Symbol { get; set; }
    //public string CommodityGrade { get; set; }
    public decimal Quantity { get; set; }
    public string Warehouse { get; set; }
    public string CreatedTimestamp { get; set; }
    //public DateTime? PreOpenEnd { get; set; }
  }
  public class CommodityWarehouseMarketShare
  {
    public string Symbol { get; set; }
    public string CommodityGrade { get; set; }
    public decimal Quantity { get; set; }
    public string Warehouse { get; set; }
    public string CreatedTimestamp { get; set; }
    public string Commodity { get; set; }

  }

  public class WarehouseMarketShareDetail : WarehouseMarketShare
  {
    public decimal? TotalQuantity { get; set; }
    public decimal Share { get; set; }
    public string Commodity { get; set; }
  }
  public class MarketShareByCommodity
  {
    public string Symbol { get; set; }
    public string CommodityGrade { get; set; }
    public decimal Quantity { get; set; }
    public string CreatedTimestamp { get; set; }
    public string Commodity { get; set; }

  }

}
