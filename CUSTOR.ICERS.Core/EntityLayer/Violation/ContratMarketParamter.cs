using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public class ContratMarketParamter
  {
   public DateTime From { get; set; }
    public DateTime To { get; set; }
    public bool Order { get; set; }
    public int Top { get; set; }
    public Guid? Commodity { get; set; }
    public string warehouseId { get; set; }
    
  }

}
