using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class DistinctOrderVM
  {
    public Guid? CommodityGradeId { get; set; }
    public decimal? Quantity { get; set; }
    public decimal? LimitPrice { get; set; }
    public Guid? SessionId { get; set; }
  }
}
