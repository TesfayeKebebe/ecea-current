using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public  class EcxAuction
  {
    [Key]
    public Guid OrderId { get; set; }
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public string OrderType { get; set; }
    public string Warehouse { get; set; }
    public string Member { get; set; }
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public string RepName { get; set; }
    public string ClientName { get; set; }
    public string Symbol { get; set; }
    public string OrderStatus { get; set; }
    public DateTime? SubmitedTimestamp { get; set; }
    public bool IsViolation { get; set; }
    public string ViewStatus { get; set; }
    public string MemberId { get; set; }
    public string SessionId { get; set; }
    public DateTime? SessionStart { get; set; }
    public DateTime? SessionEnd { get; set; }

  }
}
