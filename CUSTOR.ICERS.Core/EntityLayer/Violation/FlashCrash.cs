using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class FlashCrash
  {
    public string Buyer_Seller { get; set; }
    public decimal? Price { get; set; }
    public DateTime? TradeDate { get; set; }
  }
  public class FlashCrashDetail
  {
    public string Buyer_Seller { get; set; }
    public decimal? Price { get; set; }
    public string TradeDate { get; set; }
  }
}
