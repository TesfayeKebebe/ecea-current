using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class FlashCrashRepository
  {
    private readonly ECEADbContext _context;
    public FlashCrashRepository(ECEADbContext context)
    {
      _context = context;
    }
    public async Task<List<FlashCrashDetail>>  FlashCrashes(Guid? CommodityGrade, DateTime TradeDate)
    {
      List<FlashCrash> flashCrashes = new List<FlashCrash>();
      flashCrashes = await _context.FlashCrash.FromSqlRaw("exec dbo.pGetFlashCrash {0},{1}", CommodityGrade, TradeDate).OrderBy(x=>x.TradeDate).ToListAsync();
      List<FlashCrashDetail> details = new List<FlashCrashDetail>();
      foreach (var item in flashCrashes)
      {
        FlashCrashDetail d = new FlashCrashDetail();
        d.Buyer_Seller = item?.Buyer_Seller;
        d.TradeDate = item?.TradeDate.Value.ToShortTimeString();
        d.Price = item.Price;
        details.Add(d);
      }
      return details;
    }
   
  }
}
