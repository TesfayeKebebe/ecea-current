using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public class FrontRunning
  {
    public bool IsClientOrder { get; set; }
    public DateTime? TradeDate { get; set; }
    public decimal? TradePrice { get; set; }
    public string Seller { get; set; }
    public string Buyer { get; set; }
    public string ClientName { get; set; }
    public Guid? CommodityGradeId { get; set; }
  }
  public class FrontRunningDetail : FrontRunning
  {
    public string Member { get; set; }
    public DateTime? TransactionDate { get; set; }
    public decimal? Price { get; set; }
    public string ClientName { get; set; }

  }

}
