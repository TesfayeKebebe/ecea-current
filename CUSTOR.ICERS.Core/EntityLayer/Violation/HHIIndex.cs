﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class HHIIndex
    {
        public string MemberName { get; set; }
        public string Commodity { get; set; }
        public decimal Quantity { get; set; }
    }
    public class HHIIndexViewModel
    {
        public string MemberName { get; set; }
        public string Commodity { get; set; }
        public decimal VolumeInLot { get; set; }
        public decimal MerketShare { get; set; }
        public decimal CalculateHHI { get; set; }
    }
    public class HHISearchSqlQuery
    {
        public bool TradeActorTypeId { get; set; }
        public int CommodityTypeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int TopValue { get; set; }
        public bool Order { get; set; }
        public string CommodityId { get; set; }
        public int NoOfFrequency { get; set; }
    }
}
