using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class MarkingTheCloseVM
  {
    public string sessionName { get; set; }
    public string CommodityGrade { get; set; }
    public int? ProductionYear { get; set; }
    public decimal? Quantity { get; set; }
    public decimal? Price { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public string buyer { get; set; }
    public string seller { get; set; }
    public DateTime? CreatedDate { get; set; }
    public Guid? SessionId { get; set; }
  }
  public class MarkingTheCloseDetail:MarkingTheCloseVM
  {
    public decimal? MinimumAveragePrice { get; set; }
    public decimal? MaximumAveragePrice { get; set; }
  }
}
