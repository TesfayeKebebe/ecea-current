﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class MembersTradeExecution
    {
        //public string Member { get; set; }
        public string TradeDate { get; set; }
        public string BuyerMember { get; set; }
        public string BuyerClient { get; set; }
        public string SellerMember { get; set; }
        public string SellerClient { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Quantity { get; set; }
        public string CommodityGrade { get; set; }
    }
}
