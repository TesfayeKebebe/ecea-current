using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class NotRegistered : CommonViolationVM
  {
   
    public string Member { get; set; }
  }
}
