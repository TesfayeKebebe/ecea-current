using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class OpenedSession
  {
    public Guid? SessionId { get; set; }
  }
}
