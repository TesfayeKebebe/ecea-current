﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class OrderAttendanceSellerBuyer
    {
        public Guid MemberId { get; set; }
        public byte TransactionType { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
