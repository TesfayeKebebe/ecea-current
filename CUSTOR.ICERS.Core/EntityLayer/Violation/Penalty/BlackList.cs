﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System; 
using System.ComponentModel.DataAnnotations; 

namespace CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty
{
    public class BlackList : BaseEntity
    {
        [Key]
        public int BlackListId { get; set; } 
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        [MaxLength(150)]
        public string Issue { get; set; }
        [MaxLength(100)]  
        public string BlackListLevel { get; set; }
        [MaxLength(150)]
        public string Source { get; set; }
        [MaxLength(250)]
        public string Remark { get; set; }
    }

    public class BlackListDto : BaseEntity
    {
        public int? PenaltyId { get; set; }
         public int BlackListId { get; set; } 
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        [MaxLength(150)]
        public string Issue { get; set; }
        [MaxLength(100)]  
        public string BlackListLevel { get; set; }
        [MaxLength(50)]
        public string Source { get; set; }
        [MaxLength(250)]
        public string Remark { get; set; }
    }
    public class BlackListViewDto
    { 
        public int BlackListId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public string ExchangeActorName { get; set; }
        public virtual OwnerManager OwnerManager { get; set; } 
        [MaxLength(150)]
        public string Issue { get; set; }
        [MaxLength(100)]
        public string BlackListLevel { get; set; }
        [MaxLength(50)]
        public string Source { get; set; }
        [MaxLength(250)]
        public string Remark { get; set; }
    }
    public class BlackListQueryParameters
    {
        public string BlackListLevel { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
    }
}
