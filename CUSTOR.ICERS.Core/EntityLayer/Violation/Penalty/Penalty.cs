﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation.Penalty
{
    public class Penalty : BaseEntity
    {
        [Key]
        public int PenaltyId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public int? TypeOfPenaltyId { get; set; }
        public Lookup.Lookup TypeOfPenalty { get; set; }
        public DateTime DateOfPenalty { get; set; }
        [MaxLength(250)]
        public string ReasonOfPenalty { get; set; }
        [MaxLength(50)]  
        public string PenaltyLetterNo { get; set; } 
        public Decimal AmountOfCashPenalty { get; set; } 
        public DateTime PenaltyStartDate { get; set; }
        public DateTime PenaltyEndDate { get; set; }
    }

    public class PenaltyDto: BaseEntity
    {
        public int? PenaltyId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public int? TypeOfPenaltyId { get; set; }
        public Lookup.Lookup TypeOfPenalty { get; set; }
        public DateTime? DateOfPenalty { get; set; }
        [MaxLength(250)]
        public string ReasonOfPenalty { get; set; }
        [MaxLength(50)]
        public string PenaltyLetterNo { get; set; }
        public Decimal AmountOfCashPenalty { get; set; }
        public DateTime PenaltyStartDate { get; set; }
        public DateTime PenaltyEndDate { get; set; }
    }
    public class PenaltyViewDto
    { 
        public int PenaltyId { get; set; }
        public Guid ExchangeActorId { get; set; }
        public virtual ExchangeActor ExchangeActor { get; set; }
        public string ExchangeActorName { get; set; }
        public virtual OwnerManager OwnerManager { get; set; }
        public int? TypeOfPenaltyId { get; set; }
        public Lookup.Lookup TypeOfPenalty { get; set; }
        public string TypeOfPenaltyName { get; set; }
        public DateTime DateOfPenalty { get; set; }
        public string DateOfPenaltyAm { get; set; }
        [MaxLength(250)]
        public string ReasonOfPenalty { get; set; }
        [MaxLength(50)]
        public string PenaltyLetterNo { get; set; }
        public Decimal AmountOfCashPenalty { get; set; }
        public DateTime PenaltyStartDate { get; set; }
        public DateTime PenaltyEndDate { get; set; }
    }
    public class PenaltyQueryParameters
    { 
        public int? TypeOfPenalty { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        // For Pagination
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public string Lang { get; set; }
    }
}
