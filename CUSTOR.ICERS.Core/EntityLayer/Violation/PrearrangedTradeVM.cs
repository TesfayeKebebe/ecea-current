using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class PrearrangedTradeVM
  {
    public string CommodityGrade { get; set; }
    public int? ProductionYear { get; set; }
    public string TransactionOwnerName { get; set; }
    public decimal? Price { get; set; }
    public decimal? Quantity { get; set; }
    public DateTime? CreatedDate { get; set; }
    public Guid? SessionId { get; set; }
    public DateTime? SubmittedTimestamp { get; set; }
  }
}
