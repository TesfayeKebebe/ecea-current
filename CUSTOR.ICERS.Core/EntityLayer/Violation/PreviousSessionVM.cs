using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class PreviousSessionVM
  {
    public Guid? LastSessionId { get; set; }
  }
}
