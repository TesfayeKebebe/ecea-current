using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public  class PriceAndQuantitySummury
  {
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public string CommodityGrade { get; set; }
    public decimal TradedQuantity { get; set; }
    public decimal AveragePrice { get; set; }
    public string Symbol { get; set; }
  }
  public class PriceAndQuantitySummuryByClass
  {
    public string CommodityType { get; set; }
    public string CommodityClass { get; set; }
    public decimal TradedQuantity { get; set; }
    public decimal AveragePrice { get; set; }
  }
  public class PriceAndQuantitySummuryByCommodity
  {
    public string CommodityType { get; set; }
    public decimal TradedQuantity { get; set; }
    public decimal AveragePrice { get; set; }
  }
  public class summuryOfQuantityPriceDetail
  {
     public List<PriceAndQuantitySummury> PriceAndQuantitySummuries { get; set; }
    public List<PriceAndQuantitySummuryByClass> PriceAndQuantitySummuryByClass { get; set; }
    public List<PriceAndQuantitySummuryByCommodity> PriceAndQuantitySummuryByCommodity { get; set; }

  }
}
