using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class PriceChangeVM
  {
    public string CommodityGrade { get; set; }
    public Guid? CommodityGradeId { get; set; }
    public string buyerMember { get; set; }
    public string TradeDate { get; set; }
    public string buyTicketNo { get; set; }
    public int productionYear { get; set; }
    public decimal tradePrice { get; set; }
    public decimal tradeQuantity { get; set; }
    public decimal PriceChange { get; set; }
    public Guid? SessionId { get; set; }
  }
  public class PreviousPriceVM
  {
    public decimal? WeightAveragePrice { get; set; }
  }
  public class PriceVolumeLimitDetail : PriceChangeVM
  {
    public decimal MinPrice { get; set; }
    public decimal MaxPrice { get; set; }
  }
}
