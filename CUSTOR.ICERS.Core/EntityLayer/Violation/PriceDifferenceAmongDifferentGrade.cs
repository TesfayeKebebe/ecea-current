﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class PriceDifferenceAmongDifferentGrade
    {
        public DateTime TradeDate { get; set; }
        public int ProductionYear { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string Symbol { get; set; }
        public decimal Price { get; set; }
    }
    public class PriceDifferenceAmongDiffGradeView
    {
        public DateTime TradeDate { get; set; }
        public int ProductionYear { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string HighGrade { get; set; }
        public decimal HighGradePrice { get; set; }
        public string LowGrade { get; set; }
        public decimal LowGradePrice { get; set; }
        public decimal Deviation { get; set; }
    }
    public class PriceDifferenceSqlSearch
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal Deviation { get; set; }
        public Guid HighGradeId { get; set; }
        public Guid LowGradeId { get; set; }
        public Guid WareHouseId { get; set; }
    }
}
