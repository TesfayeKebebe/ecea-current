using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public class PriceDiffernceBetweenWarehouses
    {
        public string WareHouse { get; set; }
        //public string TradeDate { get; set; }
        //public string Grade { get; set; }
        public double? Quantity { get; set; }
         public decimal? TradePrice { get; set; }
     //   public Guid? CommodityGradeId { get; set; }
     public string Symbol { get; set; }
     //public string BuyerClientName { get; set; }
     //public string SellerClientName { get; set; }
    public string BuyerMember { get; set; }
    public string SellerMember { get; set; }
    public long WRNo { get; set; }
    public string Date { get; set; }
  }
    public class WarehouseDetail : PriceDiffernceBetweenWarehouses
    {
       public decimal? Difference { get; set; }
       public string WareHouseTo { get; set; }
        public decimal? TradePrice2 { get; set; }
       public double? QuantityTo { get; set; }

  }
  public class WareHouse
  {
    public string Name { get; set; }
    public Guid? WareHouseId { get; set; }
  }
}
