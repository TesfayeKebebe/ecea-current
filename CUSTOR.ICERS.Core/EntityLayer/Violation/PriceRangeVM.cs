using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class PriceRangeVM
  {
    public decimal MinPrice { get; set; }
    public decimal MaxPrice { get; set; }
  }
}
