﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class ReceiptHoldingSellOderGraph
    {
        public string CommodityGrade { get; set; }
        public double WarehouseQuantity { get; set; }
        public decimal OrderQuantity { get; set; }
    }
}
