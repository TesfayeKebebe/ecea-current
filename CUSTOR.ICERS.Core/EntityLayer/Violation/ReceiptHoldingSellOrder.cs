﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class ReceiptHoldingSellOrder
    {
        public string WareHouse { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public double WarehouseQuantity { get; set; }
        public decimal OrderQuantity { get; set; }
        public double Ratio { get; set; }
        public string WarehouseId { get; set; }
    }
    public class ReceiptHoldingSellOrderView
    {
        public List<ReceiptHoldingSellOrder> ReceiptHoldingSellOrder { get; set; }
        public Totals Totals { get; set; }
    }
    public class Totals
    {
        public decimal? Total { get; set; }
    }
}
