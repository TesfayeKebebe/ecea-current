using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class SalesTradeCommodity
  {
    public string Commodity { get; set; }
    public decimal TradeQuantity { get; set; }
    public decimal SellerQuantity { get; set; }
    public decimal BuyerOrder { get; set; }
    public decimal TradeSalesRatio { get; set; }
  }
}
