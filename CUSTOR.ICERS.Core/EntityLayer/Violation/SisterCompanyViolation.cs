using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class SisterCompanyViolation
  {
    public string CommodityGrade { get; set; }
    public Guid? MemberExchangeActorId { get; set; }
    public string sellerMember { get; set; }
    public string buyerMember { get; set; }
    public string TradeDate { get; set; }
    public string Ecxcode { get; set; }
    public string buyTicketNo { get; set; }
    public int? productionYear { get; set; }
    public decimal? tradePrice { get; set; }
    public decimal? Quantity { get; set; }
  }
}
