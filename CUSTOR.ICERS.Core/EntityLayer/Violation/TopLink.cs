﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TopLink
    {
        public string BuyerMemberId { get; set; }
        public string BuyerMemberName { get; set; }
        public string BuyerCommodity { get; set; }
        public string SellerMemberId { get; set; }
        public string SellerMemberName { get; set; }
        public string SellerCommodity { get; set; }
        public decimal BuyerQuantity { get; set; }
        public decimal SellerQuantity { get; set; }
    }
    public class TopLinkViewModel
    {
        public string BuyerMemberId { get; set; }
        public string BuyerMemberName { get; set; }
        public string BuyerCommodity { get; set; }
        public string SellerMemberId { get; set; }
        public string SellerMemberName { get; set; }
        public string SellerCommodity { get; set; }
        public decimal Quantity { get; set; }
        public int FrequencyOfTrade { get; set; }
        public decimal ShareQuantity { get; set; }
    }
}
