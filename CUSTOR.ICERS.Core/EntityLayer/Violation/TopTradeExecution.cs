﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class TopTradeExecution
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }

    public class TopSearchSQLParms
    {
        public int TopType { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string WarehouseId { get; set; }
        public int TopValue { get; set; }
        public int TopDescription { get; set; }
        public int SellerBuyer { get; set; }
        public string CommodityId { get; set; }
    }
}
