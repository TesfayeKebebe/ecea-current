﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeAttendanceBuyerSeller
    {
        public Guid BuyerId { get; set; }
        public Guid SellerId { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
