﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeAttendanceValueVolumeRatio
    {
        public decimal BuyerMemberQuantity { get; set; }
        public decimal BuyerMemberPrice { get; set; }
        public decimal SellerMemberQuantity { get; set; }
        public decimal SellerMemberPrice { get; set; }
        public decimal BuyerClientQuantity { get; set; }
        public decimal BuyerClientPrice { get; set; }
        public decimal SellerClientQuantity { get; set; }
        public decimal SellerClientPrice { get; set; }
    }
    public class TradeAttendanceBuyerSellerVolumeValueRatio
    {
        public string Commodity { get; set; }
        public decimal BuyerQuantity { get; set; }
        public decimal BuyerPrice { get; set; }
        public bool BuyerIsClient { get; set; }
        public decimal SellerQuantity { get; set; }
        public decimal SellerPrice { get; set; }
        public bool SellerIsClient { get; set; }
        public DateTime TradeDate { get; set; }
    }
}
