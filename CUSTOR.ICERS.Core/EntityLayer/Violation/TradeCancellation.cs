﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeCancellation
    {
        public Guid Id { get; set; }
        public string IssueDescription { get; set; }
        public string RejectionReason { get; set; }
        public string IssueType { get; set; }
    }
    public class TradeCancellationView
    {
        public string IssueDescription { get; set; }
        public string RejectionReason { get; set; }
        public string IssueType { get; set; }

    }
    public class TradeCancellationFigure
    {
        public string IssueDescription { get; set; }
        public int Total { get; set; }

    }
    public class TradeCancellationSqlQuery
    {
        public byte IssueTypeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
