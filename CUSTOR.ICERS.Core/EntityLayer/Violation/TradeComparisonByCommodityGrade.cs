﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public  class TradeComparisonByCommodityGrade
    {
        public string CommodityGrade { get; set; }
        public decimal Quantity { get; set; }
        public string Symbol { get; set; }
    }
    public class TradeComparisonByCommodityGradeView
    {
        public string CommodityGrade { get; set; }
        public string Symbol { get; set; }
        public decimal Quantity { get; set; }
        public decimal ShareInPercent { get; set; }
    }
}
