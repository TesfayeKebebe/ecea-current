﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeExecution
    {
        [Key]
        public int Id { get; set; }
        public Guid TradeId { get; set; }
        public DateTime TradedTimestamp { get; set; }
        public string BuyerOrganizationName { get; set; }
        public string BuyerClientFullName { get; set; }
        public Guid CommodityId { get; set; }
        public Guid CommodityTypeId { get; set; }
        public Guid CommodityGradeId { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string BuyerRepresentativeName { get; set; }
        public Guid BuyOrderTicketId { get; set; }
        public Guid SellOrderTicketId { get; set; }
        public byte StatusId { get; set; }
        public int ProductionYear { get; set; }
        public string Remark { get; set; }
        public byte TradeTypeId { get; set; }
        public string SellerOrganizationName { get; set; }
        public string SellerClientFullName { get; set; }
        public string SellerRepresentativeName { get; set; }
        public Guid BuyerMemberId { get; set; }
        public string BuyerIDNO { get; set; }
        public Guid BuyerRepresentativeId { get; set; }
        public string SellerIDNO { get; set; }
        public Guid SellerMemberId { get; set; }
        public Guid SellerRepresentativeId { get; set; }
    }
    public class TradeExecutionView
    {
        public DateTime TradeDate { get; set; }
        public string SellerOrganizationName { get; set; }
        public string SellerRepresentativeName { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string CommodityGrade { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string BuyerOrganizationName { get; set; }
        public string BuyerRepresentativeName { get; set; }
        public string StatusId { get; set; }
    }
}
