﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeReconcilationFigure
    {
        public byte StatusId { get; set; }
    }
    public class TradeReconcilationFigureViewModel
    {
        public int Accepted { get; set; }
        public int Rejected { get; set; }
        public int AcceptedReconciled { get; set; }
        public int RejectedReconciled { get; set; }
        public int Settled { get; set; }
        public int ExpectedSettled { get; set; }
        public int Difference { get; set; }
        public int ExpectedReconciled { get; set; }
    }
}
