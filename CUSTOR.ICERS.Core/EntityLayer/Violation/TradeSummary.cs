﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
   public class TradeSummary
    {
        public string Commodity { get; set; }
        public string Symbol { get; set; }
       // public string CommodityGrade { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public Guid BuyOrderTicketId { get; set; }
        public bool IsBuyerClient { get; set; }
        public Guid SellOrderTicketId { get; set; }
        public bool IsSellerClient { get; set; }
        public decimal SellerQuantity { get; set; }
    }
    public class TradeSummaryViewModel
    {
        public string Commodity { get; set; }
        public string Symbol { get; set; }        
        public int NoOfBuyerMember { get; set; }
        public int NoOfSellerMember { get; set; }
        public int NoOfBuyerClient { get; set; }
        public int NoOfSellerClient { get; set; }
        public decimal TransactionValue { get; set; }
        public decimal WeightAveragePrice { get; set; }
        public int MemberBuySellRatio { get; set; }
        public int ClientBuySellRatio { get; set; }
        public decimal PerCapitaBuyMember { get; set; }
        public decimal PerCapitaSellMember { get; set; }
        public decimal PerCapitaBuyClient { get; set; }
        public decimal PerCapitaSellClient { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal BuyerMemberHHIIndex { get; set; }
        public decimal SellerMemberHHIIndex { get; set; }
        public decimal BuyerClientHHIIndex { get; set; }
        public decimal SellerClientHHIIndex { get; set; }
        public decimal LowerPrice { get; set; }
        public decimal HigerPrice { get; set; }
        public decimal LowerAmount { get; set; }
        public decimal HigherAmount { get; set; }
    }
}
