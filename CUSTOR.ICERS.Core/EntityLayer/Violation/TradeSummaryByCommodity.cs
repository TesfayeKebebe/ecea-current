using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class TradeSummaryByCommodity
  {
    public string Description { get; set; }
    public decimal? Price { get; set; }
    public double? Quantity { get; set; }
  }
  public class TradeSummaryDetail  {
    public string Description { get; set; }
    public decimal? Price { get; set; }
    public double? Quantity { get; set; }
    public decimal? BeforePrice { get; set; }
    public double? BeforeQuantity { get; set; }
    public double? QuantityChange { get; set; }
    public decimal? PriceChange { get; set; }
  }
  public class BuyerSellerRatio
  {
    public string Description { get; set; }
    public decimal? MemberRatio { get; set; }
    public decimal? ClientRatio { get; set; }
  }
}
