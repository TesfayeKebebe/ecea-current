using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class TradeV‭iolationRecord
  {
    [Key]
    public string Id { get; set; }
    public string ViolationId { get; set; }
    public string Type { get; set; }
    public DateTime CreatedDate { get; set; }
    public bool IsViewed { get; set; }
    public string BuyerMemberId { get; set; }
    public string SellerMemberId { get; set; }
  }
}
