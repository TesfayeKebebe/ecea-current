using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public  class ViolationRecord
    {
        [Key]
        public string Id { get; set; }
        public string ViolationId { get; set; }
        public string Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsViewed { get; set; }
        public string MemberId { get; set; }

  }
  public class ViolationRecordCount
  {
    public string Type { get; set; }
    public int Total { get; set; }

  }

  public class ViolationRecordVm
  {
    public string Id { get; set; }
    public string ViolationId { get; set; }
    public string Type { get; set; }
    public DateTime CreatedDate { get; set; }
    public bool IsViewed { get; set; }
  }
  public class ViolationType
  {
    public bool HasMarkingToClose { get; set; }
    public bool IsNotRegistered { get; set; }
    public bool HasPriceChange { get; set; }
    public bool HasSisterCompany { get; set; }
    public bool HasNewRequestAfterPreOpen { get; set; }
    public bool HasPreArragedTrade { get; set; }
    public bool HasPriceVolumeBeyondLimit { get; set; }
    public bool BuyerNotRegisterd { get; set; }
    public bool SellerNotRegisterd { get; set; }
    public bool HasCancelled { get; set; }
    public bool HasSuspended { get; set; }
    public bool HasNotRenewed { get; set; }
    public bool HasBucketing { get; set; }
    public bool IsMatchTrade { get; set; }
    public bool HasPreArrangedDuringOpen { get; set; }
    public bool HasFrontRunning { get; set; }
    public bool HasAbnormalTrading { get; set; }
    public bool HasPriceLimit { get; set; }
  }
}
