using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
  public class ViolationInvestigation : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    public Guid ViolationId { get; set; }
    public InvestigationType Type { get; set; }
    public string Remark { get; set; }
    public string MemberId { get; set; }
    public string BuyerMemberId { get; set; }
    public string SellerMemberId { get; set; }
  }
  public class ViolationInvestigationDto : BaseEntity
  {
    public Guid Id { get; set; }
    public Guid ViolationId { get; set; }
    public InvestigationType Type { get; set; }
    public string Remark { get; set; }
    public string MemberId { get; set; }
    public string BuyerMemberId { get; set; }
    public string SellerMemberId { get; set; }
  }
  public class ViolationInvestigationDetail : ViolationInvestigationDto
  {
    public string Description { get; set; }

  }
  public enum InvestigationType
  {
    Oral=1,
    Written=2,
    LawInforcement=3
  }
}
