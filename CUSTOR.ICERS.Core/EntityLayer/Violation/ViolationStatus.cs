using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
public  class ViolationStatus
  {
    public string ViolationId { get; set; }
    public string ViolationType { get; set; }
    public string IsPostOrLive { get; set; }
  }
}
