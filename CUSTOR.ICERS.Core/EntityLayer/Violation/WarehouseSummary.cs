﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
    public class WarehouseSummary
    {
        public string BuyerWarehouse { get; set; }
        public string SellerWarehouse { get; set; }
        public string Symbol { get; set; }
        public string Commodity { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public Guid BuyOrderTicketId { get; set; }
        public Guid SellOrderTicketId { get; set; }
        public bool IsBuyerClient { get; set; }
        public bool IsSellerClient { get; set; }
        public decimal SellerQuantity { get; set; }
    }
    public class WarehouseSummaryViewModel
    {
        public string Warehouse { get; set; }
        public string ProductName { get; set; }
        public int NoOfTransaction { get; set; }
        public int NoOfContractsTrasacted { get; set; }
        public int NoOfBMParticipated { get; set; }
        public int NoOfBCParticipated { get; set; }
        public int NoOfSMParticipated { get; set; }
        public int NoOfSCParticipated { get; set; }
        public decimal WeightAveragePrice { get; set; }
        public decimal SumOfLot { get; set; }
        public decimal SumOfPrice { get; set; }
    }
}
