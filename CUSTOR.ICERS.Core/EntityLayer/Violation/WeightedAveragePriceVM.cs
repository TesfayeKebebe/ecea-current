using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Violation
{
 public class WeightedAveragePriceVM
  {
    public decimal? WeightAveragePrice { get; set; }
  }
}
