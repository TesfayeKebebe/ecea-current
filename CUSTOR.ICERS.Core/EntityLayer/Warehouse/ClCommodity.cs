﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ClCommodity
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public Guid? StandardUnitOfMeasureId { get; set; }
        public int? MaxLimit { get; set; }
    }

    public partial class ClCommodityDTO
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
     
    }
}
