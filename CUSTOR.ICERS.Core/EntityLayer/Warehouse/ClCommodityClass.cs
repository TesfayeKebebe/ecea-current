﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class ClCommodityClass
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public Guid CommodityGuid { get; set; }
        public Guid? CommodityTypeGuid { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public string Symbol { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int Auto { get; set; }
    }
}
