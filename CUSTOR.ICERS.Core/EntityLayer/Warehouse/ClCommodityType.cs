﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ClCommodityType
    {
        public Guid Id { get; set; }
        public Guid? CommodityId { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ProcessingGroup { get; set; }
    }
}
