﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ClGinstatus
    {
        public int EnumValue { get; set; }
        public string EnumName { get; set; }
        public int GinstatusId { get; set; }
    }
}
