﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ClWarehouseShed
    {
        public Guid Id { get; set; }
        public Guid WarehouseId { get; set; }
        public string ShedNumber { get; set; }
        public DateTime? RowVersionStamp { get; set; }
    }
}
