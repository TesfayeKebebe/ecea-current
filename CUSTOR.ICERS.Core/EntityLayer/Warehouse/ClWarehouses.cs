﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ClWarehouses
    {
        public Guid Id { get; set; }
        public int? OldId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public int? QueueDate { get; set; }
        public int? GrnqueueNo { get; set; }
        public int? GinqueueNo { get; set; }
        public bool? UsedInOfflineMode { get; set; }
        public string OfflinePath { get; set; }
        public Guid? LocationId { get; set; }
        public decimal? StorageCharge { get; set; }
        public int? StorageChargeAfterNdays { get; set; }
        public int? ActiveStatus { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? RowVersionStamp { get; set; }
    }

    public partial class ClWarehousesDTO2
    {
        public Guid Id { get; set; }
        public int? OldId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public int? QueueDate { get; set; }
        public int? GrnqueueNo { get; set; }
        public int? GinqueueNo { get; set; }
        public bool? UsedInOfflineMode { get; set; }
        public string OfflinePath { get; set; }
        public Guid? LocationId { get; set; }
        public decimal? StorageCharge { get; set; }
        public int? StorageChargeAfterNdays { get; set; }
        public int? ActiveStatus { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? RowVersionStamp { get; set; }
    }

    public partial class ClWarehousesDTO
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public int? QueueDate { get; set; }
        public int? GrnqueueNo { get; set; }
        public int? GinqueueNo { get; set; }
        public string Location { get; set; }
        public decimal? StorageCharge { get; set; }
        public int? StorageChargeAfterNdays { get; set; }
        public string ActiveStatus { get; set; }
    }

}
