﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{

    public class tblArrivals : BaseEntity
    {
        [Key]
        public string ID { get; set; }
        public string TrackingNumber { get; set; }
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string CommodityID { get; set; }
        public string WarehouseID { get; set; }
        public bool IsLocationKnown { get; set; }

        public string WoredaID { get; set; }
        public string SpecificArea { get; set; }
        public string ProcessingCenter { get; set; }
        public string DriverName { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssuedPlace { get; set; }
        public string TruckPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string VoucherNumber { get; set; }
        public string VoucherCertificateNo { get; set; }

        public DateTime Arrivaldate { get; set; }
        public DateTime AssignedtoSamplerDate { get; set; }
        public DateTime DateofSampleDrawn { get; set; }
        public DateTime DateofLabResult { get; set; }
        public DateTime DateofCustomerSignatureResult { get; set; }
        public DateTime UnloadDate { get; set; }
        public int Grade { get; set; }
        public DateTime GRNCreationDate { get; set; }
        public DateTime DateofCustomerSignatureGRN { get; set; }
        public DateTime DateGRNApproved { get; set; }
        public int GRNNumber { get; set; }
        public string Commodity { get; set; }
        public string WashingStation { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public string Labcode { get; set; }
        public int SLA { get; set; }
    }

}
