﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{

    // Warehouse Operations Report –From Arrival to GRN Approval
    public class EcxArrivalToGRN : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int VoucherId { get; set; }
        public string NameofDepositor { get; set; }
        public string Warehouse { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string CommodityType { get; set; }
        public int NoBagsReceived { get; set; }
        public DateTime Arrivaldate { get; set; }
        public DateTime AssignedtoSamplerDate { get; set; }
        public DateTime DateofSampleDrawn { get; set; }
        public DateTime DateofLabResult { get; set; }
        public DateTime DateofCustomerSignatureResult { get; set; }
        public DateTime UnloadDate { get; set; }
        public int Grade { get; set; }
        public DateTime GRNCreationDate { get; set; }
        public DateTime DateofCustomerSignatureGRN { get; set; }
        public DateTime DateGRNApproved { get; set; }
        public int GRNNumber { get; set; }
        public string Commodity { get; set; }
        public string WashingStation { get; set; }
        public string TruckPlateNumber  { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public string SpecificArea { get; set; }
        public string Labcode { get; set; }
        public int SLA { get; set; }
    }
    public partial class EcxArrivalToGRNDTO
    {
       
        public int Id { get; set; }
        public string VoucherId { get; set; }
        public string NameofDepositor { get; set; }
        public string Warehouse { get; set; }
        public string WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string CommodityType { get; set; }
        public int? NoBagsReceived { get; set; }
        public DateTime Arrivaldate { get; set; }
        public DateTime AssignedtoSamplerDate { get; set; }
        public DateTime DateofSampleDrawn { get; set; }
        public DateTime DateofLabResult { get; set; }
        public DateTime DateofCustomerSignatureResult { get; set; }
        public DateTime UnloadDate { get; set; }
        public int Grade { get; set; }
        public DateTime? GRNCreationDate { get; set; }
        public DateTime DateofCustomerSignatureGRN { get; set; }
        public DateTime? DateGRNApproved { get; set; }
        public int GRNNumber { get; set; }
        public string Commodity { get; set; }
        public string WashingStation { get; set; }
        public string TruckPlateNumber { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public string SpecificArea { get; set; }
        public string Labcode { get; set; }
        public int SLA { get; set; }
    }

    public partial class ArrivalDTO
    {
        public ArrivalDTO()
        {
        }
        public int Id { get; set; }
        public string NameofDepositor { get; set; }
        public string Warehouse { get; set; }
        public int? NoBagsReceived { get; set; }
        public DateTime Arrivaldate { get; set; }
        public string Commodity { get; set; }
        public string WashingStation { get; set; }
        public string TruckPlateNumber { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public string SpecificArea { get; set; }
        public string Labcode { get; set; }
        public int SLA { get; set; }
    }
}
