﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    //Warehouse Operations Report –Delivery Notice (DN) Received – Goods Issue Note (GIN) Approval
    public class EcxDeliveryNoticeToGIN : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int No { get; set; }
        public string NameofBuyer { get; set; }
        public int TradeNo { get; set; }
        public int SessionId { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int Grade { get; set; }
        public DateTime DeliveryNoticeReceived { get; set; }
        public DateTime DateProductDelivered { get; set; }
        public float AmountBoughtinKG { get; set; }
        public float AmountDeliveredinKG { get; set; }
        public float ShortfallinKG { get; set; }
        public float PercentwithMoisture { get; set; }
        public string TruckPlateNumber { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }

    }

    public partial class EcxDeliveryNoticeToGINDTO
    {
       
        public int No { get; set; }
        public string NameofBuyer { get; set; }
        public int SessionId { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int Grade { get; set; }
        public DateTime DeliveryNoticeReceived { get; set; }
        public DateTime DateProductDelivered { get; set; }
        public float AmountBoughtinKG { get; set; }
        public float AmountDeliveredinKG { get; set; }
        public float ShortfallinKG { get; set; }
        public float PercentwithMoisture { get; set; }
        public string Woreda { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public int TradeNo { get; set; }

    }
}
