﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{

    //Expired Pickup Notices (PUN) Report
    public class EcxExpiredPickupNoticesReport : BaseEntity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int No { get; set; }
        public int TradeNo { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int CommodityGrade { get; set; }
        public string BuyerName { get; set; }
        public DateTime DateExpired { get; set; }
        public  DateTime TradeDate { get; set; }
        public int Grade { get; set; }
        public float WeightinKG { get; set; }
        public DateTime DateDNProvided { get; set; }
        public virtual EcxDeliveryNoticeToGIN EcxDeliveryNoticeToGIN { get; set; }

    }

    public partial class EcxExpiredPickupNoticesReportDTO
    {
       

        public int No { get; set; }
        public int TradeNo { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int CommodityGrade { get; set; }
        public string BuyerName { get; set; }
        public DateTime DateExpired { get; set; }
        public DateTime TradeDate { get; set; }
        public int Grade { get; set; }
        public float WeightinKG { get; set; }
        public DateTime DateDNProvided { get; set; }

    }
}
