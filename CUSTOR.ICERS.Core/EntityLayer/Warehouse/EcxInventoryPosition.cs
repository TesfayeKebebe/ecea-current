﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    //Warehouse Inventory Position
    public class EcxInventoryPosition : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int No { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int CommodityGrade { get; set; }
        public int CurrentInventoryNoOfbags { get; set; }
        public float CurrentInventoryWeight { get; set; }
        public int SampleLeftoversNoOfbags { get; set; }
        public float SampleLeftoversWeight { get; set; }
        public int ExcessProductsNoOfbags { get; set; }
        public float ExcessProductsWeight { get; set; }
        public int ShortfallProductsNoOfbags { get; set; }
        public float ShortfallProductsWeight { get; set; }
        public virtual EcxWarehouse EcxWarehouse { get; set; }

    }

    public partial class EcxInventoryPositionDTO
    {
        [Key]
        public int No { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseLocation { get; set; }
        public string WarehouseName { get; set; }
        public string CommodityType { get; set; }
        public int CommodityGrade { get; set; }
        public int CurrentInventoryNoOfbags { get; set; }
        public float CurrentInventoryWeight { get; set; }
        public int SampleLeftoversNoOfbags { get; set; }
        public float SampleLeftoversWeight { get; set; }
        public int ExcessProductsNoOfbags { get; set; }
        public float ExcessProductsWeight { get; set; }
        public int ShortfallProductsNoOfbags { get; set; }
        public float ShortfallProductsWeight { get; set; }
    }
}
