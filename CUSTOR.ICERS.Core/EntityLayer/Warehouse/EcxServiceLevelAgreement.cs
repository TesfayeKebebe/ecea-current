﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{

    //Warehouse Service Delivery Level (SLA) Summary Report
    public class EcxServiceLevelAgreement : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int Region { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseName { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string Symbol { get; set; }
        public int NoBagsReceived { get; set; }
        public float percentMoistureCase { get; set; }
        public float percentwithinSLA { get; set; }
        public float percentoutsideSLA { get; set; }
        public DateTime Arrivaldate { get; set; }
        public string NameofDepositor { get; set; }
        public virtual EcxArrivalToGRN EcxArrivalToGRN { get; set; }

    }
    public partial class EcxServiceLevelAgreementDTO
    {
       

        public int Id { get; set; }
        public int Region { get; set; }
        public int WarehouseNo { get; set; }
        public string WarehouseName { get; set; }
        public string Commodity { get; set; }
        public string CommodityType { get; set; }
        public string Symbol { get; set; }
        public int NoBagsReceived { get; set; }
        public float percentMoistureCase { get; set; }
        public float percentwithinSLA { get; set; }
        public float percentoutsideSLA { get; set; }

    }
}
