﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{  //Warehouse Profile
    public class EcxWarehouse : BaseEntity 
    { 
        [Key] 
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WarehouseId { get; set; }
        [StringLength(100, ErrorMessage = "Name can't be longer than 100 characters")]
        public string WarehouseName { get; set; }
        [StringLength(20, ErrorMessage = "Warehouse No can't be longer than 20 characters")]
        public string WarehouseNo { get; set; }
        [StringLength(20, ErrorMessage = "Short Name can't be longer than 20 characters")]
        public string ShortName { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
    }

    public class EcxWarehouseDto
    {  
        public int WarehouseId { get; set; } 
        public string WarehouseName { get; set; } 
        public string WarehouseNo { get; set; } 
        public string ShortName { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
    }
}
