﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class ExcessProduct : BaseEntity
{
    public ExcessProduct()
    {
    }

    [Key]
    public int Id { get; set; }
    public string SeReportNo { get; set; }
    public DateTime Date_registered { get; set; }
    public int WareHouseShedNo { get; set; }
    public string WarehouseReceiptNumber { get; set; }
    public int GradeOfCommodity { get; set; }
    public string ExcessReportNumber { get; set; }
    public int NoOfBagsDelivered { get; set; }

    [Column(TypeName ="decimal(18,4)")]
    public decimal FirstWeight { get; set; }
    [Column(TypeName = "decimal(18,4)")]
    public decimal SecondWeight { get; set; }
    [Column(TypeName = "decimal(18,4)")]
    public decimal NetWeight { get; set; }
    [Column(TypeName = "decimal(18,4)")]
    public decimal PunWeight { get; set; }
    [Column(TypeName = "decimal(18,4)")]
    public decimal GinWeight { get; set; }
    [Column(TypeName = "decimal(18,4)")]
    public decimal ExcessAmount { get; set; }
    public string LeadInventoryController { get; set; }
    public string InventoryController { get; set; }
    public string WareHouseOperationManager { get; set; }
}
