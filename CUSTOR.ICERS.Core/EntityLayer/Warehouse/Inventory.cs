﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class Inventory
    {
        public Guid Id { get; set; }
        public Guid ShedId { get; set; }
        public DateTime InventoryDate { get; set; }
        public Guid LeadInventoryControllerId { get; set; }
        public int InventoryReasonId { get; set; }
        public Guid WarehouseId { get; set; }
        public int? Status { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
    }
}
