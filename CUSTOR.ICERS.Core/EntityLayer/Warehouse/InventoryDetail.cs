﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class InventoryDetail
    {
        public Guid? Id { get; set; }
        public Guid InventoryId { get; set; }
        public Guid StackId { get; set; }
        public double PhysicalCount { get; set; }
        public double SystemCount { get; set; }
        public double? PhysicalWeight { get; set; }
        public double? SystemWeight { get; set; }
        public double AdjustmentCount { get; set; }
        public double AdjustmentWeight { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
        public Guid? ApprovedById { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public int? Status { get; set; }
    }
}
