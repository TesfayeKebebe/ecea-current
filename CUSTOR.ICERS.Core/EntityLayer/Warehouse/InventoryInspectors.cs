﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class InventoryInspectors
    {
        public Guid InventoryId { get; set; }
        public Guid InspectorId { get; set; }
        public string InspectorName { get; set; }
        public string Position { get; set; }
    }
}
