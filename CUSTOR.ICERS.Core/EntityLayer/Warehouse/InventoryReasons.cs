﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class InventoryReasons
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
