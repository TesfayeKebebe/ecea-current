﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public class SampleLeftOver: BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int HandoverNumber { get; set; }
        public DateTime HandoverDate { get; set; }
        public string WarehouseCustodian { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public int BranchId { get; set; }
        public int CommodityGrade { get; set; } 
        public int CommodityType { get; set; }
        public string QullityOperationSuperVisor { get; set; }
        public string WareHouseOperationSuperVisor { get; set; }
        public string LeadInventoryController { get; set; }
        public string Comment { get; set; }
        public DateTime DateSigned { get; set; }

    }
}
