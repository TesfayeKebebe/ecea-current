﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public class ShortFall: BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string BranchLocation { get; set; }
        public int WareHouseNo { get; set; }
        public int PUN { get; set; }
        public int NoticedAgainstPun { get; set; }
        public int ScaleTicketNumber { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal PunWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Short_Fall { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal FirstWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal SecondWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal NetWeight { get; set; }

        public int GradeOfCommodity { get; set; }
        public string InventoryController { get; set; }
        public string OperationSuperVisor { get; set; }
        public string NameofLeadInventory { get; set; }
      
      
    }
    public class ShortFallDTO
    {
        public int Id { get; set; }
        public string BranchLocation { get; set; }
        public int WareHouseNo { get; set; }
        public int PUN { get; set; }
        public int NoticedAgainstPun { get; set; }
        public int ScaleTicketNumber { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal PunWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Short_Fall { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal FirstWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal SecondWeight { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal NetWeight { get; set; }
        public int GradeOfCommodity { get; set; }
        public string InventoryController { get; set; }
        public string OperationSuperVisor { get; set; }
        public string NameofLeadInventory { get; set; }


    }
}
