﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class ShortFallDetails: BaseEntity
    {
        public string StackNumber { get; set; }
        public int CumulativeDepositBeforeShortFall { get; set; }
        public int Bags { get; set; }
        public int Weight { get; set; }
        public int WeightDeduction { get; set; }
        public int LastDeliveryDate { get; set; }
        public DateTime FirstDeposit { get; set; }
        public string Commment { get; set; }
    }
}
