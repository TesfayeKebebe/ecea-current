﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblArrivals
    {
        public Guid Id { get; set; }
        public string TrackingNumber { get; set; }
        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }
        public Guid? CommodityId { get; set; }
        public Guid? WarehouseId { get; set; }
        public bool? IsLocationKnown { get; set; }
        public Guid? WoredaId { get; set; }
        public string SpecificArea { get; set; }
        public string ProcessingCenter { get; set; }
        public int? ProductionYear { get; set; }
        public int? NumberofBags { get; set; }
        public DateTime? DateTimeReceived { get; set; }
        public int? Status { get; set; }
        public bool? IsNonTruck { get; set; }
        public string DriverName { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssuedPlace { get; set; }
        public string TruckPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public bool HasVoucher { get; set; }
        public string VoucherNumber { get; set; }
        public Guid? VoucherCommodityTypeId { get; set; }
        public int? VoucherNumberOfBags { get; set; }
        public int? VoucherNumberOfPlomps { get; set; }
        public int? VoucherNumberOfPlompsTrailer { get; set; }
        public string VoucherCertificateNo { get; set; }
        public decimal? VoucherWeight { get; set; }
        public string Remark { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsBiProduct { get; set; }
        public bool IsTruckInCompound { get; set; }
        public decimal? VehicleSize { get; set; }
        public Guid? WashingStation { get; set; }
        public bool? MoistureDeduct { get; set; }
        public string Sampletype { get; set; }
        public bool? IsDeposit { get; set; }
        public decimal? GrossWeight { get; set; }
        public string ScaleTicketNo { get; set; }
    }

    public partial class TblArrivalsDTO
    {
        public string TrackingNumber { get; set; }
        public string ClientName { get; set; }
        public string Commodity { get; set; }
        public string Warehouse { get; set; }
        public bool? IsLocationKnown { get; set; }
        public string Woreda { get; set; }
        public string SpecificArea { get; set; }
        public string ProcessingCenter { get; set; }
        public int? ProductionYear { get; set; }
        public int? NumberofBags { get; set; }
        public string DateTimeReceived { get; set; }
        public int? Status { get; set; }
        public bool? IsNonTruck { get; set; }
        public string DriverName { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssuedPlace { get; set; }
        public string TruckPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public bool HasVoucher { get; set; }
        public string VoucherNumber { get; set; }
        public Guid? VoucherCommodityTypeId { get; set; }
        public int? VoucherNumberOfBags { get; set; }
        public int? VoucherNumberOfPlomps { get; set; }
        public string WarehouseNo { get; set; }
        public string VoucherCertificateNo { get; set; }
        public decimal? VoucherWeight { get; set; }
        public string Remark { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsBiProduct { get; set; }
        public string IsTruckInCompound { get; set; }
        public decimal? VehicleSize { get; set; }
        public string WashingStation { get; set; }
        public string MoistureDeduct { get; set; }
        public string Sampletype { get; set; }
        public bool? IsDeposit { get; set; }
        public decimal? GrossWeight { get; set; }
        public string ScaleTicketNo { get; set; }

    }

    public partial class TblArrivalsBywarehouseandcommodityDTO
    {

        
        public string Commodity { get; set; }
        public string Warehouse { get; set; }
     
        public string ZoneWoreda { get; set; }
        public int? NumberofBags { get; set; }
        public int? NumberofTruk { get; set; }
        public int? NumberofClient { get; set; }
        public decimal? Weight { get; set; }
     

    }

}
