﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
   public class TblCommodity
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string Symbol { get; set; }
        public string UnitOfMeasure { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
