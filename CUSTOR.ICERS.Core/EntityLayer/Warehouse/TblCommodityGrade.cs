﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class TblCommodityGrade
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public Guid CommodityClassGuid { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public string Symbol { get; set; }
        public double LotSize { get; set; }
        public bool CanWithdraw { get; set; }
        public bool Retradable { get; set; }
        public int LotsizeInBag { get; set; }
        public int WhrexpiryDate { get; set; }
        public bool Exportable { get; set; }
        public double Mla { get; set; }
        public bool Vatable { get; set; }
        public bool DeductVatonnor { get; set; }
        public double PriceCalledPerKg { get; set; }
        public double? DepositPerKg { get; set; }
        public double? IndividualDailyPositionLimit { get; set; }
        public double? TotalDailyPositionLimit { get; set; }
        public double? MaximumOrderSize { get; set; }
        public double DailyPriceLimit { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
