﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class TblExtendExpiryDateStatus
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
