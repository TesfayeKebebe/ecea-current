﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class TblExtendPunexpiryDate
    {
        public Guid Id { get; set; }
        public Guid Punid { get; set; }
        public Guid RequestedBy { get; set; }
        public DateTime DateRequested { get; set; }
        public int NumberOfDays { get; set; }
        public Guid? ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public Guid? RejectedBy { get; set; }
        public DateTime? DateRejected { get; set; }
        public byte Status { get; set; }
    }

    public partial class TblExtendPunexpiryDateDTO
    {
        public string DateRequested { get; set; }
        public int? NumberOfDays { get; set; }
        public string DateApproved { get; set; }
        public string DateRejected { get; set; }
        public string Status { get; set; }
        public string PunprintDateTime { get; set; }
        public string  SellerName { get; set; }
        public string ClientName { get; set; }
        public string MemberName { get; set; }
        public string CommodityName { get; set; }
        public string WarehouseName { get; set; }
        public string Grnno { get; set; }
        public string GinNo { get; set; }
    }
    public partial class TblExtendPunexpiryDateBywarehouseNaDTO
    {
 
        public string ClientName { get; set; }
        public int NumberofPunExtended { get; set; }
        public string CommodityGradeName { get; set; }
        public string WarehouseName { get; set; }
        public double? QuantityInLot { get; set; }

    }

}
