﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblGins
    {
        public Guid Id { get; set; }
        public string Ginnumber { get; set; }
        public int? AutoNumber { get; set; }
        public string TransactionId { get; set; }
        public string DriverName { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssuedBy { get; set; }
        public string PlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public DateTime? TruckRequestTime { get; set; }
        public DateTime? TruckRegisterTime { get; set; }
        public double? IssuedBags { get; set; }
        public double? IssuedWeight { get; set; }
        public Guid? WarehouseId { get; set; }
        public Guid? ShedId { get; set; }
        public Guid? LeadInventoryControllerId { get; set; }
        public string LeadInventoryController { get; set; }
        public int AdjustmentTypeId { get; set; }
        public double? WeightAdjustment { get; set; }
        public double? BagAdjustment { get; set; }
        public int? NoOfRebags { get; set; }
        public DateTime? DateIssued { get; set; }
        public DateTime? DateTimeLoaded { get; set; }
        public DateTime? ClientSignedDate { get; set; }
        public string ClientSignedName { get; set; }
        public DateTime? LicsignedDate { get; set; }
        public string LicsignedName { get; set; }
        public DateTime? ManagerSignedDate { get; set; }
        public string ManagerSignedName { get; set; }
        public int? GinstatusId { get; set; }
        public bool? IsPsa { get; set; }
        public int? DailyLabourersAssociationId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? CreatedTimeStamp { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedTimeStamp { get; set; }
        public DateTime? RowVersionStamp { get; set; }
        public Guid? InventoryControlId { get; set; }
        public Guid? PsastackId { get; set; }
        public string ScaleTicketNumber { get; set; }
        public string LoadUnloadTicketNo { get; set; }
        public string Remark { get; set; }
        public string Psaremark { get; set; }
        public int? WbserviceProviderId { get; set; }
    }

    public partial class TblGinsDTO
    {
        public string Ginnumber { get; set; }
        public int? AutoNumber { get; set; }
        public string TransactionId { get; set; }
        public string DriverName { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssuedBy { get; set; }
        public string PlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string TruckRequestTime { get; set; }
        public DateTime? TruckRegisterTime { get; set; }
        public double? IssuedBags { get; set; }
        public double? IssuedWeight { get; set; }
        public string Warehouse{ get; set; }
        public Guid? ShedId { get; set; }
        public Guid? LeadInventoryControllerId { get; set; }
        public string LeadInventoryController { get; set; }
        public int AdjustmentTypeId { get; set; }
        public double? WeightAdjustment { get; set; }
        public double? BagAdjustment { get; set; }
        public int? NoOfRebags { get; set; }
        public string DateIssued { get; set; }
        public string DateTimeLoaded { get; set; }
        public string ClientSignedDate { get; set; }
        public string CommodityGrade { get; set; }

        public string ClientSignedName { get; set; }
        public string LicsignedDate { get; set; }
        public string LicsignedName { get; set; }
        public string ManagerSignedDate { get; set; }
        public string ManagerSignedName { get; set; }
        public string Ginstatus { get; set; }
        public string IsPsa { get; set; }
        public int? DailyLabourersAssociationId { get; set; }
        public Guid? InventoryControlId { get; set; }
        public Guid? PsastackId { get; set; }
        public string ScaleTicketNumber { get; set; }
        public string LoadUnloadTicketNo { get; set; }
        public string Remark { get; set; }
        public string Psaremark { get; set; }
        public int? WbserviceProviderId { get; set; }
    }

    public partial class TblIssudeGinsDTO
    {
        public double? QuantityInLot { get; set; }
        public string Warehouse { get; set; }
        public int? CanceldGIN { get; set; }
        public int? NumberOfClient { get; set; }
        public string CommodityGrade { get; set; }
        public string ClientName { get; set; }


    }

}
