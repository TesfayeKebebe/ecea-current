﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblGrns
    {
        public long AutoNo { get; set; }
        public Guid Id { get; set; }
        public string Grn_Number { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? CommodityGradeId { get; set; }
        public string Symbol { get; set; }
        public int? ProductionYear { get; set; }
        public Guid? CommodityReceivingId { get; set; }
        public DateTime? DateDeposited { get; set; }
        public int? Status { get; set; }
        public int? TotalNumberOfBags { get; set; }
        public double? GrossWeight { get; set; }
        public double? TruckWeight { get; set; }
        public decimal? NetWeight { get; set; }
        public decimal? Tare { get; set; }
        public double? Quantity { get; set; }
        public Guid? LeadInventoryControllerId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
        public int AutoNumber { get; set; }
        public Guid? WarehouseSupervisorApprovedBy { get; set; }
        public int? WarehouseSupervisorStatus { get; set; }
        public DateTime? WarehouseSupervisorApprovedDateTime { get; set; }
        public DateTime? WarehouseSupervisorApprovedTimeStamp { get; set; }
        public Guid? LicapprovedBy { get; set; }
        public int? Licstatus { get; set; }
        public DateTime? LicapprovedDateTime { get; set; }
        public DateTime? LicapprovedTimeStamp { get; set; }
        public DateTime? GrncreatedDate { get; set; }
        public int? RebagingQuantity { get; set; }
        public string Remark { get; set; }
        public bool? ScaleTicketSigned { get; set; }
        public int? ConsignmentType { get; set; }
        public decimal? CupValue { get; set; }
        public decimal? RawValue { get; set; }
        public decimal? TotalValue { get; set; }
        public string ProcessingCenter { get; set; }
        public string CarPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string Shade { get; set; }
        public Guid? Licname2 { get; set; }
        public Guid? WashingandMillingStation { get; set; }
        public string Quadrant { get; set; }
        public bool? IsTraceable { get; set; }
        public string ClientCert { get; set; }
        public Guid? WoredaId { get; set; }
        public Guid? EditedBy { get; set; }
        public decimal? ScreenSize { get; set; }
    }

    public partial class TblGrnsDTO
    {
        public string GrnNumber { get; set; }
        public string Warehouse { get; set; }
        public int? sla { get; set; }
        public string ClientName { get; set; }
        public string CommodityGrade { get; set; }
        public string CommodityID { get; set; }
        public string Symbol { get; set; }
        public int? ProductionYear { get; set; }
        public string DateDeposited { get; set; }
        public string ArrivalDate { get; set; }
        public string Status { get; set; }
        public int? TotalNumberOfBags { get; set; }
        public double? GrossWeight { get; set; }
        public double? Lot { get; set; }

        public double? TruckWeight { get; set; }
        public double? ExtendedNumberOfDay { get; set; }
        public decimal? NetWeight { get; set; }
        public decimal? Tare { get; set; }
        public double? Quantity { get; set; }
        public string LeadInventoryController { get; set; }
        public DateTime? CreatedTimestamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
        public int? AutoNumber { get; set; }
        public Guid? WarehouseSupervisorApprovedBy { get; set; }
        public int? WarehouseSupervisorStatus { get; set; }
        public DateTime? WarehouseSupervisorApprovedDateTime { get; set; }
        public DateTime? WarehouseSupervisorApprovedTimeStamp { get; set; }
        public Guid? LicapprovedBy { get; set; }
        public int? Licstatus { get; set; }
        public DateTime? LicapprovedDateTime { get; set; }
        public DateTime? LicapprovedTimeStamp { get; set; }
        public string GrncreatedDate { get; set; }
        public int? RebagingQuantity { get; set; }
        public string Remark { get; set; }
        public bool? ScaleTicketSigned { get; set; }
        public int? ConsignmentType { get; set; }
        public decimal? CupValue { get; set; }
        public decimal? RawValue { get; set; }
        public decimal? TotalValue { get; set; }
        public string ProcessingCenter { get; set; }
        public string CarPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string Shade { get; set; }
        public string WashingandMillingStation { get; set; }
        public string Quadrant { get; set; }
        public bool? IsTraceable { get; set; }
        public string ClientCert { get; set; }
        public decimal? ScreenSize { get; set; }
        public int? WarehouseRecieptId { get; set; }
        public string Grnnumber { get; set; }
        public string DateApproved { get; set; }
        public string ExpiredDate { get; set; }
        public byte? WrstatusId { get; set; }
        public double? OriginalQuantity { get; set; }
        public double? CurrentQuantity { get; set; }
        public byte? SourceType { get; set; }
        public double? NetWeightAdjusted { get; set; }
        public double? TempQuantity { get; set; }
        public int? NumberOfBags { get; set; }
        public string RejectionReason { get; set; }
        public string ReciteExpiryDate { get; set; }
        public string TradeDate { get; set; }
        public string TransactionNo { get; set; }
        public string cdGrnstatus { get; set; }
        public string DateClosed { get; set; }
        public bool? SpecFlag { get; set; }
        public string CDRemark { get; set; }
        public double? WeightBeforeMoisture { get; set; }
        public int? TradeNo { get; set; }
        public string ClientCertificate { get; set; }
        public string ManualApprovalDate { get; set; }
    }
    public partial class TblGrnsforslaDTO
    {
        public int? sla { get; set; }
        public string Warehouse { get; set; }
        public string Code { get; set; }

    }

    public partial class TblExtendedGrnsDTO
    {
        public int TotalNumberOfExtendedGRN { get; set; }
        public string Warehouse { get; set; }
        public string ClientName { get; set; }
        public int? NoClientName { get; set; }

        public string CommodityGrade { get; set; }
        public decimal? NetWeight { get; set; }
        public double? ExtendedNumberOfDay { get; set; }


    }

    public partial class TblslaDTO
    {
        public int? A { get; set; }
        public int? A1{ get; set; }
        public int? A2 { get; set; }
        public int? A3 { get; set; }
        public int? A4 { get; set; }
        public int? A5 { get; set; }
        public int? A6 { get; set; }
        public int? A7 { get; set; }
        public int? A8 { get; set; }
        public int? A9 { get; set; }
        public int? A10 { get; set; }
        public int? A11 { get; set; }
        public int? A12 { get; set; }
        public string Warehouse { get; set; }
        public string Code { get; set; }
        public int? NumberofTruckWithInSla { get; set; }
        public int? NumberofTruckOutsideSla { get; set; }
        public double? PercentwithInSla { get; set; }
        public double? PercentOutsideSla { get; set; }


    }

    public partial class TblslaReportBywarehouseDTO
    {
        public int? A { get; set; }
        public int? A1 { get; set; }
        public int? A2 { get; set; }
        public int? A3 { get; set; }
        public int? A4 { get; set; }
        public int? A5 { get; set; }
        public int? A6 { get; set; }

        public string Warehouse { get; set; }
      


    }

}
