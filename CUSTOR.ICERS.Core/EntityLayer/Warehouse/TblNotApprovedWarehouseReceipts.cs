﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblNotApprovedWarehouseReceipts
    {
        public Guid Id { get; set; }
        public Guid WarehouseReceiptId { get; set; }
        public DateTime DateTimeChecked { get; set; }
        public Guid CheckedBy { get; set; }
        public bool StatusInactive { get; set; }
        public bool LotToWeightOutOfRange { get; set; }
        public bool InvalidBagType { get; set; }
        public bool InvalidMembership { get; set; }
        public bool NonCompliant { get; set; }
    }
}
