﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblPickUpNoticeWarehouseReciept
    {
        public int WarehouseRecieptId { get; set; }
        public Guid PickupNoticeId { get; set; }
        public double Quantity { get; set; }
        public double Weight { get; set; }
        public DateTime DnreceivedDateTime { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
