﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblPickupNotices
    {
        public Guid Id { get; set; }
        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientIdno { get; set; }
        public Guid? MemberId { get; set; }
        public string MemberName { get; set; }
        public string MemberIdno { get; set; }
        public Guid? RepId { get; set; }
        public string RepName { get; set; }
        public string RepIdno { get; set; }
        public string AgentName { get; set; }
        public int? IdtypeId { get; set; }
        public string Idtype { get; set; }
        public string AgentIdnumber { get; set; }
        public string AgentTel { get; set; }
        public Guid? CommodityGradeId { get; set; }
        public string CommodityName { get; set; }
        public int? ProductionYear { get; set; }
        public Guid? WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public string ShedName { get; set; }
        public Guid? WarehouseReceiptId { get; set; }
        public int? WarehouseReceiptNo { get; set; }
        public string Grnno { get; set; }
        public double? QuantityInLot { get; set; }
        public decimal? WeightInKg { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? ExpectedPickupDateTime { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public bool? Exported { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimestamp { get; set; }
        public int? OldId { get; set; }
        public DateTime? RowVersionStamp { get; set; }
        public bool? Punprinted { get; set; }
        public DateTime? PunprintDateTime { get; set; }
        public Guid? PunprintedBy { get; set; }
        public string ConsignmentType { get; set; }
        public string Woreda { get; set; }
        public decimal? RawValue { get; set; }
        public decimal? Cupvalue { get; set; }
        public decimal? TotalValue { get; set; }
        public string WashingMillingStation { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string PlateNumber { get; set; }
        public string SellerName { get; set; }
        public string Shade { get; set; }
        public bool? IsTraceable { get; set; }
        public string SustainableCertification { get; set; }
    }

    public partial class TblPickupNoticesDTO
    {
        public string ClientName { get; set; }
        public string ClientIdno { get; set; }
        public string MemberName { get; set; }
        public string MemberIdno { get; set; }
        public string RepName { get; set; }
        public string RepIdno { get; set; }
        public string AgentName { get; set; }
        public string AgentIdnumber { get; set; }
        public string AgentTel { get; set; }
        public string CommodityGradeName { get; set; }
        public string CommodityName { get; set; }
        public int? ProductionYear { get; set; }
        public string WarehouseName { get; set; }
        public string ShedName { get; set; }
        public int? WarehouseReceiptNo { get; set; }
        public string Grnno { get; set; }
        public string GinNo { get; set; }
        public double? QuantityInLot { get; set; }
        public decimal? WeightInKg { get; set; }
        public string ExpirationDate { get; set; }
        public string ExpectedPickupDateTime { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }
        public string Exported { get; set; }
        public bool? Punprinted { get; set; }
        public string PunprintDateTime { get; set; }
        public Guid? PunprintedBy { get; set; }
        public string ConsignmentType { get; set; }
        public string Woreda { get; set; }
        public decimal? RawValue { get; set; }
        public decimal? Cupvalue { get; set; }
        public decimal? TotalValue { get; set; }
        public string WashingMillingStation { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string PlateNumber { get; set; }
        public string SellerName { get; set; }
        public string Shade { get; set; }
        public string IssueDate { get; set; }

        public bool? IsTraceable { get; set; }
        public string SustainableCertification { get; set; }
    }


    public partial class ReconcilationDTO
    {
        public string ClientName { get; set; }
        public string MemberName { get; set; }
        public string CommodityGradeName { get; set; }
        public string WarehouseName { get; set; }
        public string Grnno { get; set; }
        public string GinNo { get; set; }
        public string Psanumber { get; set; }
        public int? BWhr { get; set; }
        public string Symbol { get; set; }
        public double? Weight { get; set; }
        public string CdmanagerCreatedDate { get; set; }
        public string IntiatedDueTo { get; set; }
        public int? CdmanagerApprovalStatus { get; set; }
        public string CdmanagerRemark { get; set; }
        public int? TradeNo { get; set; }
        public string TradeDate { get; set; }
        public double? Punweight { get; set; }
        public double? IssuedWeight { get; set; }
        public double? WeightDiff { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? MemberId { get; set; }
        public int? Swhr { get; set; }
        public string NoteType { get; set; }

        // psa from warehouseapplictation
        public int? AutoNumber { get; set; }
        public string Warehouse { get; set; }
        public Guid? ShedId { get; set; }
        public Guid? Licid { get; set; }
        public string Licname { get; set; }
        public int? AdjustmentTypeId { get; set; }
        public double? WeightAdjustment { get; set; }
        public double? BagAdjustment { get; set; }
        public int? NoOfRebags { get; set; }
        public string DateIssued { get; set; }
        public string DateTimeLoaded { get; set; }
        public string WhsupervisorApprovedDate { get; set; }
        public string WhsupervisorName { get; set; }
        public int? WhsupervisorApprovalStatus { get; set; }
        public string InsertedDueTo { get; set; }
        public string WhsupervisorRemark { get; set; }
        public string WhsupervisorPsadescription { get; set; }
        public string WhbranchManagerCreatedDate { get; set; }
        public string WhbranchManagerName { get; set; }
        public int? WhbranchManagerApprovalStatus { get; set; }
        public string WhbranchManagerRemark { get; set; }
        public string WhmanagerCreatedDate { get; set; }
        public string WhmanagerName { get; set; }
        public int? WhmanagerApprovalStatus { get; set; }
        public string WhmanagerRemark { get; set; }
        public string OccreatedDate { get; set; }
        public string OcapprovedName { get; set; }
        public int? OcapprovalStatus { get; set; }
        public string Ocremark { get; set; }
        public string Psastatus { get; set; }
        public string ClientSignedName { get; set; }
        public string ClientSignedDate { get; set; }
        public string LicsignedDate { get; set; }
        public string LicsignedName { get; set; }
        public int? DailyLabroursAssociationId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string TransactionId { get; set; }
        public string PsamodifiedBy { get; set; }
        public string PsaupdatedTimeStamp { get; set; }
        public string CommodityGrade { get; set; }


    }

    public partial class TblShortFallReconcilationDTO
    {
        public double? Weight { get; set; }
        public int? NoOfClient { get; set; }
        public string Warehouse { get; set; }
        public string CommodityGrade { get; set; }
        public string ClientName { get; set; }
        public string Remark { get; set; }


    }
}
