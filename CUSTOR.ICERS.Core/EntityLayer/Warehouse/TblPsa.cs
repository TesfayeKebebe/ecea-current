﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class TblPsa
    {
        public Guid Psaid { get; set; }
        public string Psanumber { get; set; }
        public int AutoNumber { get; set; }
        public int? BWhr { get; set; }
        public Guid? WarehouseId { get; set; }
        public Guid? ShedId { get; set; }
        public Guid? Licid { get; set; }
        public string Licname { get; set; }
        public int? AdjustmentTypeId { get; set; }
        public double? WeightAdjustment { get; set; }
        public double? BagAdjustment { get; set; }
        public int? NoOfRebags { get; set; }
        public DateTime? DateIssued { get; set; }
        public int? TradeNo { get; set; }
        public DateTime? TradeDate { get; set; }
        public double? Punweight { get; set; }
        public double? IssuedWeight { get; set; }
        public double? WeightDiff { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? MemberId { get; set; }
        public int? Swhr { get; set; }
        public string NoteType { get; set; }
        public string Symbol { get; set; }
        public DateTime? DateTimeLoaded { get; set; }
        public DateTime? WhsupervisorApprovedDate { get; set; }
        public string WhsupervisorName { get; set; }
        public int? WhsupervisorApprovalStatus { get; set; }
        public string InsertedDueTo { get; set; }
        public string WhsupervisorRemark { get; set; }
        public string WhsupervisorPsadescription { get; set; }
        public DateTime? WhbranchManagerCreatedDate { get; set; }
        public string WhbranchManagerName { get; set; }
        public int? WhbranchManagerApprovalStatus { get; set; }
        public string WhbranchManagerRemark { get; set; }
        public string WhmanagerCreatedDate { get; set; }
        public string WhmanagerName { get; set; }
        public int? WhmanagerApprovalStatus { get; set; }
        public string WhmanagerRemark { get; set; }
        public DateTime? OccreatedDate { get; set; }
        public string OcapprovedName { get; set; }
        public int? OcapprovalStatus { get; set; }
        public string Ocremark { get; set; }
        public int? PsastatusId { get; set; }
        public string ClientSignedName { get; set; }
        public DateTime? ClientSignedDate { get; set; }
        public DateTime? LicsignedDate { get; set; }
        public string LicsignedName { get; set; }
        public int? DailyLabroursAssociationId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string TransactionId { get; set; }
        public Guid? PsamodifiedBy { get; set; }
        public DateTime? PsaupdatedTimeStamp { get; set; }
    }
}
