﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.ModelsdbCentralDepository
{
    public partial class TblPsa
    {
        public Guid Id { get; set; }
        public string Psanumber { get; set; }
        public int? BWhr { get; set; }
        public double? Weight { get; set; }
        public DateTime? CdmanagerCreatedDate { get; set; }
        public string IntiatedDueTo { get; set; }
        public Guid? CdmanagerApprovedBy { get; set; }
        public int? CdmanagerApprovalStatus { get; set; }
        public string CdmanagerRemark { get; set; }
        public int? TradeNo { get; set; }
        public DateTime? TradeDate { get; set; }
        public double? Punweight { get; set; }
        public double? IssuedWeight { get; set; }
        public double? WeightDiff { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? MemberId { get; set; }
        public int? Swhr { get; set; }
        public string NoteType { get; set; }
        public string Symbol { get; set; }
    }
}
