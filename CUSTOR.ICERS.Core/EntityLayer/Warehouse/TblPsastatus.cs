﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblPsastatus
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
