﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.EntityLayer.Warehouse
{
    public partial class TblPungin
    {
        public Guid Id { get; set; }
        public Guid? Punid { get; set; }
        public Guid? Ginid { get; set; }
        public double? Quantity { get; set; }
        public double? Weight { get; set; }
        public DateTime? RowVersionStamp { get; set; }
        public DateTime? PunprintDateTime { get; set; }
        public Guid? PunprintedBy { get; set; }
        public bool? IsPsa { get; set; }
    }
}
