﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblPunpsa
    {
        public Guid Id { get; set; }
        public Guid? PunId { get; set; }
        public Guid? Psaid { get; set; }
        public double? Weight { get; set; }
        public DateTime? PunPrintedTime { get; set; }
    }
}
