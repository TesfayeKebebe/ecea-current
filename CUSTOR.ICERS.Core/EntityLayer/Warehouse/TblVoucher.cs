﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblVoucher
    {
        public Guid VoucherId { get; set; }
        public Guid? DepositRequestId { get; set; }
        public string VoucherNo { get; set; }
        public Guid? CoffeeTypeId { get; set; }
        public int? NumberOfBags { get; set; }
        public int? NumberOfPlomps { get; set; }
        public int? NumberOfPlompsTrailer { get; set; }
        public string CertificateNo { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? OldId { get; set; }
        public string SpecificArea { get; set; }
        public int? Status { get; set; }
    }
}
