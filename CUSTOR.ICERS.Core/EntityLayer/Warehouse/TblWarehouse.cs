﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblWarehouse
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Code { get; set; }
        public Guid LocationGuid { get; set; }
        public Guid WarehouseTypeGuid { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
        public decimal? LocationDifferential { get; set; }
        public decimal? SellHandlingCharge { get; set; }
        public decimal? BuyHandlingCharge { get; set; }
        public decimal? StorageCharge { get; set; }
        public int? StorageChargeAfterNdays { get; set; }
        public short Status { get; set; }
        public Guid RegionGuid { get; set; }
        public Guid ZoneGuid { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
