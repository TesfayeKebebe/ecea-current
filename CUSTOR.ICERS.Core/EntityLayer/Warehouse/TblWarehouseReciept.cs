﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.ModelsdbCentralDepository
{
    public partial class TblWarehouseReciept
    {
        public Guid Id { get; set; }
        public int WarehouseRecieptId { get; set; }
        public Guid Grnid { get; set; }
        public string Grnnumber { get; set; }
        public Guid CommodityGradeId { get; set; }
        public Guid WarehouseId { get; set; }
        public Guid BagTypeId { get; set; }
        public Guid? VoucherId { get; set; }
        public Guid UnLoadingId { get; set; }
        public Guid ScalingId { get; set; }
        public Guid GradingId { get; set; }
        public Guid SamplingTicketId { get; set; }
        public DateTime DateDeposited { get; set; }
        public DateTime? DateApproved { get; set; }
        public byte WrstatusId { get; set; }
        public double GrossWeight { get; set; }
        public double NetWeight { get; set; }
        public double OriginalQuantity { get; set; }
        public double CurrentQuantity { get; set; }
        public Guid DepositeTypeId { get; set; }
        public byte SourceType { get; set; }
        public double NetWeightAdjusted { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public Guid? LastModifiedBy { get; set; }
        public DateTime? LastModifiedTimeStamp { get; set; }
        public Guid ClientId { get; set; }
        public Guid? ApprovedBy { get; set; }
        public double TempQuantity { get; set; }
        public int NumberOfBags { get; set; }
        public string RejectionReason { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? TradeDate { get; set; }
        public string TransactionNo { get; set; }
        public int Grnstatus { get; set; }
        public Guid? GrntypeId { get; set; }
        public int ProductionYear { get; set; }
        public Guid? ClosedBy { get; set; }
        public DateTime? DateClosed { get; set; }
        public bool SpecFlag { get; set; }
        public string Remark { get; set; }
        public double? WeightBeforeMoisture { get; set; }
        public Guid? TradeId { get; set; }
        public int? TradeNo { get; set; }
        public int? ConsignmentType { get; set; }
        public decimal? RawValue { get; set; }
        public decimal? CupValue { get; set; }
        public decimal? TotalValue { get; set; }
        public Guid? Woreda { get; set; }
        public string Shade { get; set; }
        public bool? IsTraceable { get; set; }
        public string CarPlateNumber { get; set; }
        public string TrailerPlateNumber { get; set; }
        public string WashingandMillingStation { get; set; }
        public string ClientCertificate { get; set; }
        public decimal? ScreenSize { get; set; }
        public DateTime? ManualApprovalDate { get; set; }
    }
}
