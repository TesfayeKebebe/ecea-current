﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblWarehouseReciept
    {
        public Guid Id { get; set; }
        public int? WarehouseRecieptId { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? Grnid { get; set; }
        public string Grnnumber { get; set; }
        public Guid? CommodityGradeId { get; set; }
        public Guid? WarehouseId { get; set; }
        public Guid? ShedId { get; set; }
        public Guid? BagTypeId { get; set; }
        public Guid? VoucherId { get; set; }
        public Guid? UnLoadingId { get; set; }
        public Guid? ScalingId { get; set; }
        public Guid? GradingId { get; set; }
        public Guid? SamplingTicketId { get; set; }
        public double? Weight { get; set; }
        public double? Quantity { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedTimeStamp { get; set; }
        public Guid? LastedModifiedBy { get; set; }
        public DateTime? LastModifiedTimeStamp { get; set; }
        public int? NumberOfBags { get; set; }
    }
}
