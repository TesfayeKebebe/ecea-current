﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.API.ModelsdbCentralDepository
{
    public partial class TblWarehouseRecieptStatus
    {
        public byte Id { get; set; }
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
