﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblWashingStation
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid? Zone { get; set; }
        public string Warehouse { get; set; }
        public string OwnershipType { get; set; }
        public double? EstablishedYear { get; set; }
        public string WashedorDry { get; set; }
        public DateTime? CreatedTimeStamp { get; set; }
        public string Region { get; set; }
        public string ZoneDesc { get; set; }
    }
}
