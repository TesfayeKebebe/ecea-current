﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblWoreda
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public Guid ZoneGuid { get; set; }
        public string Description { get; set; }
        public short Status { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
