﻿using System;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class TblWoredaCommodityClass
    {
        public Guid Id { get; set; }
        public Guid? WordeaId { get; set; }
        public Guid? CommodityClassId { get; set; }
        public int? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
