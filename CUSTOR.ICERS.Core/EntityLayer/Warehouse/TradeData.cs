﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
    public partial class EcexTradeDataForECEA
    {
        public EcexTradeDataForECEA()
        {

        }
        [Key]
        public int tradeNo { get; set; }
        public string sessionName { get; set; }
        public int? sessionId { get; set; }
        public string buyerClient { get; set; }
        public string commodityGrade { get; set; }
        public string buyerMember { get; set; }
        public string buyerRep { get; set; }
        public string buyTicketNo { get; set; }
        public bool? buyIsClientTrade { get; set; }
        public string sellerClient { get; set; }
        public string sellerMember { get; set; }
        public string sellerRep { get; set; }
        public string sellTicketNo { get; set; }
       // public bool sellIsClientTrade { get; set; }
        public string warehouse { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal tradePrice { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal tradeQuantity { get; set; }
        public int productionYear { get; set; }
        public string tradeStatus { get; set; }
        public DateTime tradedTimestamp { get; set; }
        public DateTime? sessionStart { get; set; }
        public DateTime? sessionEnd { get; set; }
    }
    public partial class TradeData
    {
        public TradeData()
        {

        }
        public int tradeNo { get; set; }
        public string sessionName { get; set; }
        public int SessionId { get; set; }
        public string buyerClient { get; set; }
        public string commodityGrade { get; set; }
        public string buyerMember { get; set; }
        public string buyerRep { get; set; }
        public string buyTicketNo { get; set; }
        public bool buyIsClientTrade { get; set; }
        public string sellerClient { get; set; }
        public string sellerMember { get; set; }
        public string sellerRep { get; set; }
        public string sellTicketNo { get; set; }
        public bool sellIsClientTrade { get; set; }
        public string warehouse { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal tradePrice { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal tradeQuantity { get; set; }
        public int productionYear { get; set; }
        public string tradeStatus { get; set; }
        public DateTime tradedTimestamp { get; set; }
        public DateTime sessionStart { get; set; }
        public DateTime sessionEnd { get; set; }
    }
}
