﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
namespace CUSTOR.ICERS.Core.EntityLayer.Warehouse
{
   public class TransactionStatusTwo: BaseEntity
    {
        public DateTime TraddeDate { get; set; }
        public string BuyerMemberName { get; set; }
        public string BuyerClientName { get; set; }
        public string SellerClientName { get; set; }
        public string SellerMemberName { get; set; }
        public string Symbol { get; set; }
        public string Lot { get; set; }
        public decimal Price { get; set; }
        public string StDVolumeSmall { get; set; }
        public string StDVolumeLarge { get; set; }
        public string StDValueSmall { get; set; }
        public string StDValueLarge { get; set; }
    }
}
