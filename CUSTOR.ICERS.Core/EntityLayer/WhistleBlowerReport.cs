﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public class WhistleblowerReport : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SourceOfInformation { get; set; }
        public int InformationType { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public DateTime Eventdate { get; set; }
        public  DateTime RegisterdDate { get; set; }
        public string Url { get; set; }

    }

    public class WhistleBlowerDTO : BaseEntity
    {
        public int? Id { get; set; }
        public int SourceOfInformation { get; set; }
        public int InformationType { get; set; }
        public string Name { get; set; }
        public string InformationTypeDescription { get; set; }
        public DateTime Eventdate { get; set; }
        public DateTime RegisterdDate { get; set; }
        public string Remark { get; set; }
        public string Url { get; set; }


    }
    public class WhistleBlowerListDTO
    {
        public int Id { get; set; }
        public int SourceOfInformation { get; set; }
        public string SourceOfInformationDescription { get; set; }
        public int InformationType { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string InformationTypeDescription { get; set; }
        public string EventdateDec { get; set; }
        public string RegisterdDateDec { get; set; }
        public DateTime Eventdate { get; set; }
        public DateTime RegisterdDate { get; set; }
        public string Url { get; set; }

    }
    public class SearchSqlBlowersQuery
    {       
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Lang { get; set; }
    }
}
