﻿using CUSTOR.ICERS.Core.EntityLayer.Common;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CUSTOR.ICERS.Core.EntityLayer
{
    public partial class WorkFlow : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkFlowId { get; set; }
        public string ProcessedByName { get; set; }
        public int FinalDec { get; set; }
        public string Comment { get; set; }
        public int? MemberTradeViolationId { get; set; }
        public int? InjunctionId { get; set; }
        public int? MemberClientTradeId { get; set; }
        public Guid? ServiceApplicationId { get; set; }
        public int OffSiteMonitoringReportId { get; set; }
        public virtual ServiceApplication ServiceApplication { get; set; }
        public virtual MemberClientTrade MemberClientTrade { get; set; }
        public virtual MemberTradeViolation MemberTradeViolation { get; set; }
        public virtual Injunction Injunction { get; set; }
        public int BankOversightViolationId { get; set; }
        public virtual BankOversightViolation BankOversightViolation { get; set; }
        public virtual OffSiteMonitoringReport OffSiteMonitoringReport { get; set; }

    }
    public partial class WorkFlowDTO
    {

        public string ProcessedByName { get; set; }
        public int FinalDec { get; set; }
        public string Comment { get; set; }
        public int? MemberTradeViolationId { get; set; }
        public Guid ServiceApplicationId { get; set; }
        public int ServiceType { get; set; }
        public int CustomerTypeId { get; set; }
        public Guid CreatedUserId { get; set; }
        public int? MemberClientTradeId { get; set; }
        public bool ApprovalFinished { get; set; }
        public int InjunctionId { get; set; }
        public int OffSiteMonitoringReportId { get; set; }
        public int BankOversightViolationId { get; set; }
    }
    public class WorkFlowViewModel
    {
        public string ProcessedByName { get; set; }
        public int FinalDec { get; set; }
        public string CreatedDate { get; set; }
        public string Comment { get; set; }
    }
}
