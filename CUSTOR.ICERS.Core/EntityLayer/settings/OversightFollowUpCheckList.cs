﻿using CUSTOR.ICERS.Core.EntityLayer.Common;

namespace CUSTOR.ICERS.Core.EntityLayer.settings
{
    class OversightFollowUpCheckList : BaseEntity
    {
        OversightFollowUpCheckList()
        {
        }
        public string CheckListCodeId { get; set; }
        public int ExchangeActorType { get; set; }
        public string CheckListDescAmh { get; set; }
        public string CheckListDescEng { get; set; }
        public string ConfirmationMethodEng { get; set; }
        public string ConfirmationMethodAmh { get; set; }
    }
    class OversightChecklistDTO
    {
        OversightChecklistDTO()
        {
        }
        public string CheckListCodeId { get; set; }
        public int ExchangeActorType { get; set; }
        public string CheckListDescAmh { get; set; }
        public string CheckListDescEng { get; set; }
        public string ConfirmationMethod { get; set; }
    }
}
