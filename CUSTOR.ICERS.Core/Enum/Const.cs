namespace CUSTOR.ICERS.Core.Enum
{
    public enum StatusCode : int
    {
        UnderInjunction = 1000,
        Cancelled = 1001
    }
    public enum CustomerType : short
    {
        Representative = 63,
        Member = 6, 
        NonMemberTrading = 90,  
        Auditor = 5,
        SeetlementBank = 69,
        NonMemberDirectTraderTradeReprsentative = 120,
        NationalTradeAssociation =194
    }
    public enum ClientType : short
    {
       Individual = 127,
       Plc= 128,
       ShareCompany=129,
       PublicEnterprise = 130,
        CooperativeSociety = 131
    }
    public enum RecognitionServiceApplicaitonSteps : short

    {
        General_Information = 0,
        SisterCompany = 1,
        Owner = 2,
        OwnerManager = 3,
        Prerequisite = 4,
        PaymentOrder = 5,
        Certeficate = 6
    }
    public enum ServiceApplicaitonStatus : short
    {
        Drafted = 17,
        Completed = 70,
        SendForPayment = 28,
        WaitingForApproval = 66,
        Approved = 67,
        Paid = 45
    }

    public enum ManagerType : short
    {
        Owner = 117,
        Delegate = 118,
        Employee = 119
    }
    public enum ExchangeActorMaxReprsentative : short
    {
        Member = 4,
        NonMemberDirectTradder = 2
    }
    public enum CancellationServiceApplicationSteps : short
    {
        General_Information = 0,
        Prerequisite = 1
    }
    public enum RenewalServiceApplicaionSteps : short
    {
        General_Information = 0,
        Prerequisite = 1,
        PaymentOrder = 2,
        Certeficate = 3
    }
    public enum RepresentativeChangeSteps : short
    {
        General_Information = 0,
        Prerequisite = 1,
        Certificate = 3
    }
    // TODO reacator this one
    public enum ExchangeActorStatus : short
    {
        Active = 20,
        Cancelled = 64,
        Under_Injunction = 31,
        Pre_Cancellation_Injunction = 4,
        Injunction_Draft_state = 114, // unapproved/ non-declined injunction state
    }
    public enum CasePartyType : int
    {
        ExchangeActor = 1,
        Sole = 2, 
        Organization = 3
    }
    public enum CasePartyCategory : int
    {
        Suspect = 1,
        Plaintiff = 2,
        Collaborator = 3,
        Witness = 4
    }
    public enum CaseAdministrationUnitType : int
    {
        Application = 1,
        Investigation = 2,
        Case = 3,
        CaseTracking = 4
    }
    public enum ServiceType : short
    {
        Recognition = 1,
        Renewal = 2,
        Cancellation = 3,
        Replacement = 4,
        RepresentativeChange = 5,
        RenewalWithPenality = 6,
        Injunction = 7,
        AddressChange = 8,
        Managerchange = 9,
        CompanyNameChange = 10,
        UploadedDocumentChange = 11,
        OtherDataChange = 12,
        Amendment = 13
    }

    public enum DaysMesurementUnit : short
    {
        Year = 1,
        Month = 2,
        Day = 3
    }
    public enum OversightFollowUpDecisionStatus:int
    {
        incomplete= 170
    }

    public enum MemberCategory: short
    {
        Intermideary = 72,
        Trader = 88
    }

    public enum DataChangeType: short
    {
        OrganizationAddressChange = 37,
        ManagerAddressChange = 78,
        FainacialInsititutionAddressChange = 182
    }

    public enum DataChangeServiceApplicaiton : short
    {
        ChangeInformation = 0,
        ChangePrerequiste = 1,
        Reprint = 2
    }
        
    /// <summary>
    /// represents Months injunction 6 month, expiration 6
    /// </summary>
    public enum GracePeriod : short
    {
        Injunction = 6,
        RenewalGracePeriod = 3
    }
    public enum MonthlyOverSightSummaryStatus
    {
        SubmitedToLeader=1,
        SubmitedToDepartment=2,
        ApprovedByDepartment = 3,
        DeclinedByTeamLeader=4,
        DeclinedByDepartment = 3
    }
    public enum OversightFollowUpDecisions: short
    {
        GOAHEAD = 171,
        ORALWARNING = 35,
        LETTERWARNING = 95,
        SENDTOLAWENFORCEMENT = 122,
        STARTED = 170,
    }
    public enum ViolationType
    {
    HasMarkingToClose, 
   IsNotRegistered ,
   HasPriceChange, 
   HasSisterCompany,
   HasNewRequestAfterPreOpen, 
   HasPreArragedTrade, 
   HasPriceVolumeBeyondLimit, 
   BuyerNotRegisterd,
   SellerNotRegisterd, 
   HasCancelled, 
   HasSuspended, 
   HasNotRenewed, 
   HasBucketing, 
   IsMatchTrade, 
   HasPreArrangedDuringOpen,
   HasFrontRunning 
}
}
