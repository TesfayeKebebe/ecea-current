using AutoMapper;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.Address;
using CUSTOR.ICERS.Core.EntityLayer.DataChanges;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement;
using CUSTOR.ICERS.Core.EntityLayer.LawEnforcement.CaseApplicationDto;
using CUSTOR.ICERS.Core.EntityLayer.Lookup;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUp;
using CUSTOR.ICERS.Core.EntityLayer.OversightFollowUP;
using CUSTOR.ICERS.Core.EntityLayer.Payment;
using CUSTOR.ICERS.Core.EntityLayer.Recognition;
using CUSTOR.ICERS.Core.EntityLayer.Registration;
using CUSTOR.ICERS.Core.EntityLayer.SettlementBank;
using CUSTOR.ICERS.Core.EntityLayer.TradeExcution;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using CUSTOR.ICERS.Core.EntityLayer.Warehouse;
using CUSTOR.ICERS.Core.Security;
using CUSTOR.ICERS.DAL.DTO.ECX;
using CUSTOR.ICERS.DAL.Entity.Ecx;
using CUSTOR.Security;

namespace CUSTOR.ICERS.Core.Helpers
{

  public class ApplicationMappingProfile : Profile
  {
    public ApplicationMappingProfile()
    {
       
            CreateMap<ExchangeActor, ExchangeActorGeneralInfoDTO>().ReverseMap();
            CreateMap<ServiceApplication, ServiceApplicationDTO>().ReverseMap();
            CreateMap<MemberRepresentativeType, int>().ReverseMap();
            CreateMap<ClWarehouses, ClWarehousesDTO2>().ReverseMap();
            CreateMap<WhistleblowerReport, WhistleBlowerDTO>().ReverseMap();


      CreateMap<CustomerPrerequisiteDTO, CustomerPrerequisite>().ReverseMap();
      CreateMap<OwnerManager, OwnerManagerDTO>().ReverseMap();
      CreateMap<PaymentAuthorized, PaymentAuthorizedDTO>().ReverseMap();
      CreateMap<PaymentOrder, PaymentOrderDTO>().ReverseMap();
      CreateMap<Receipt, ReceiptDTO>().ReverseMap();
      CreateMap<Certificate, CertificateDTO>().ReverseMap();
      CreateMap<SisterCompany, SisterCompanyPostDTO>().ReverseMap();
      CreateMap<SisterCompany, SisterCompanyGetDTO>().ReverseMap();
      CreateMap<EcxMemberSisterCompany, EcxMemberSistetCompanyDTO>().ReverseMap();
      CreateMap<Lookup, LookupDTO2>().ReverseMap();
      CreateMap<MemberProduct, MemberProductDTO>().ReverseMap();
      CreateMap<CustomerBussinessFiled, CustomerBusinessFiledDto>().ReverseMap();

      CreateMap<Injunction, InjunctionInfoDTO>().ReverseMap();
      CreateMap<Injunction, InjunctionLiftDTO>().ReverseMap();
      CreateMap<Injunction, InjunctionApprovalDTO>().ReverseMap();
      CreateMap<Injunction, PostInjunctionDTO>().ReverseMap();
      CreateMap<Cancellation, CancellationDTO>().ReverseMap();
      CreateMap<Replacement, ReplacementDTO>().ReverseMap();
      CreateMap<Renewal, RenewalDTO>().ReverseMap();
      CreateMap<MemberRepresentativeType, MemberRepresentativeTypeDTO>().ReverseMap();
      CreateMap<MemberClientTrade, TradeExcutionReportDTO>().ReverseMap();
      CreateMap<MemberClientTradeDetail, MemberClientTradeDetailDTO>().ReverseMap();
      CreateMap<MemberClientTradeDetail, MemberTradeDetailViewModel>().ReverseMap();
      CreateMap<MemberClientInformation, MemberClientInformationDTO>().ReverseMap();
      CreateMap<MemberTradeFinancial, MemberTradeFinancialDTO>().ReverseMap();
      CreateMap<MemberClientInformation, ClientInformationViewModel>().ReverseMap();
      CreateMap<MemberTradeFinancial, MemberTradeFinancialViewModel>().ReverseMap();
      CreateMap<MemberTradeEvidence, MemberTradeEvidenceDTo>().ReverseMap();
      CreateMap<TradeExcutionViolationRecord, TradeExcutionViolationRecordDTO>().ReverseMap();
      CreateMap<Oversight_report_criteria, Oversight_report_criteriaDTO>().ReverseMap();
      CreateMap<OversightFollowUpReport, OversightReportDTO>().ReverseMap();
      CreateMap<OversightReportSetting, OversightReportSettingDTO>().ReverseMap();
      CreateMap<OversightSchedule, OversightSchedule>().ReverseMap();
      CreateMap<MemberFinancialAuditor, MemberFinancialAuditorDTO>().ReverseMap();
      CreateMap<FinancialAuditoredFileUpload, FinancialAuditoredFileUploadDTO>().ReverseMap();
      CreateMap<SettlementBankContact, SettlementBankContactDTO>().ForMember(dest => dest.OwnerManagerId, opt => opt.MapFrom(src => src.Id)).ReverseMap();

      CreateMap<RegionDTO, Region>()
         .ReverseMap();
      CreateMap<ZoneDTO, Zone>()
       .ReverseMap();
      CreateMap<WoredaDTO, Woreda>()
       .ReverseMap();
      CreateMap<KebeleDTO, Kebele>()
       .ReverseMap();
      CreateMap<Region, LookUpDisplayView>().ReverseMap();
      CreateMap<Zone, LookUpDisplayView>().ReverseMap();
      CreateMap<Woreda, LookUpDisplayView>().ReverseMap();
      CreateMap<Kebele, LookUpDisplayView>().ReverseMap();
      CreateMap<Nationality, CountryDisplayView>().ReverseMap();
      CreateMap<Lookup, LookUpDisplayView>().ReverseMap();
      CreateMap<Lookup, LookupDTO4>().ReverseMap();
      CreateMap<LookupType, LookupTypeDTO>().ReverseMap();
      CreateMap<ApplicationUser, UserViewModel3>();

      CreateMap<ApplicationUser, UserViewModel3>()
        .ForMember(d => d.Roles, map => map.Ignore());
      CreateMap<UserViewModel3, ApplicationUser>()
        .ForMember(d => d.IdentityUserRole, map => map.Ignore());

      CreateMap<CaseLookUp, CaseLookUpDTO2>().ReverseMap();
      CreateMap<CaseLookUp, CaseLookupDTO4>().ReverseMap();
      CreateMap<CaseLookUpType, CaseLookupTypeDTO>().ReverseMap();

      CreateMap<CaseLookUp, CaseLookUpDTO2>().ReverseMap();
      CreateMap<CaseLookUp, LookUpDisplayView>().ReverseMap();
      CreateMap<CaseStatusType, LookUpDisplayView>().ReverseMap();
      CreateMap<CaseParty, CasePartyDTO>().ReverseMap();
      CreateMap<CaseParty, CasePartyEditDTO>().ReverseMap();
      CreateMap<ExchangeActor, ExchangeActorCasePartyModel>().BeforeMap((s, d) =>
      {
        d.FullName = $"{s?.OwnerManager?.FirstNameAmh} {s?.OwnerManager?.FatherNameAmh} {s?.OwnerManager?.GrandFatherNameAmh}";
        d.OrganizationName = s?.OrganizationNameAmh;
        d.Tin = s?.Tin;
        d.ExchangeActorId = s?.ExchangeActorId;
        d.Ecxcode = s?.Ecxcode;
      });
      CreateMap<CaseApplication, CaseApplicationPartyDTO>().ReverseMap();
      CreateMap<CaseParty, CaseApplicationPartyDTO>().ReverseMap();
      CreateMap<CaseParty, PartyDTO>().ReverseMap();
      CreateMap<ExchangeActor, ExchangeActorCasePartyModel>().ReverseMap();
      CreateMap<ExchangeActor, ExchangeActorCasePartyDTO>().ReverseMap();
      CreateMap<CaseApplication, CaseApplicationDTO>().ReverseMap();
      CreateMap<CaseApplication, CaseApplicationEditDTO>().ReverseMap();
      CreateMap<CaseInvestigation, CaseInvestigationListModel>()
      .BeforeMap((s, d) =>
      {
        d.ExchangeActorOrganizationName = $"{s?.CaseApplication?.Suspect?.ExchangeActor?.OrganizationNameAmh} ";

              //d.associationId = s?.associationId;
            });
      CreateMap<CaseFeedback, CaseFeedbackDTO>().ReverseMap();
      CreateMap<SampleUser, SampleEmployeeDTO>().ReverseMap();
      CreateMap<CaseInvestigation, CaseInvestigationDTO>().ReverseMap();
      CreateMap<CasePartyInvestigation, CasePartyInvestigationDTO>().ReverseMap();
      CreateMap<CaseParty, CasePartyInvestigationListModel>().ReverseMap();
      CreateMap<CaseDocument, CaseDocumentDTO>().ReverseMap();
      CreateMap<Case, LegalCaseDTO>().ReverseMap();
      CreateMap<CaseTracking, CaseTrackingDTO>().ReverseMap();
      CreateMap<CaseTracking, CaseAppointmentDTO>().ReverseMap();
      CreateMap<CaseTracking, CaseCourtSessionDTO>().ReverseMap();
      CreateMap<CaseAppointment, AppointmentDTO>().ReverseMap();
      CreateMap<CaseDecisionTracking, CaseDecisionTrackingDTO>().ReverseMap();
      CreateMap<CaseDecisionTracking, CaseTrackingDTO>().ReverseMap();
      CreateMap<CasePartyDocument, CasePartyDocumentDto>().ReverseMap();
      CreateMap<ReportPeriod, ReportPeriodViewModel>().ReverseMap();
      CreateMap<ReportType, ReportTypeviewDTO>().ReverseMap();
      CreateMap<ExchangeActorFinanicial, ExchangeActorFinanicialDTO>().ReverseMap();
      CreateMap<CasePartyTracking, CasePartyTrackingDTO>().ReverseMap();
      CreateMap<ClientInformationReminder, ClientInformationReminderDTO>().ReverseMap();
      CreateMap<FinancialReportReminder, FinancialReportReminderDTO>().ReverseMap();
      CreateMap<Commodity, CommodityDTO>().ReverseMap();
      CreateMap<CommodityType, CommodityTypeDTO>().ReverseMap();
      CreateMap<CommodityGrade, CommodityGradeDTO>().ReverseMap();
      CreateMap<WorkFlow, WorkFlowDTO>().ReverseMap();
      CreateMap<MemberTradeViolation, MemberTradeViolationDTO>().ReverseMap();
      CreateMap<RepresentativeChange, RepresentativeChangeDTO>().ReverseMap();

      //tariff
      CreateMap<ServiceTariff, ServicetariffDTO>().ReverseMap();
      CreateMap<ServiceTariff, ServiceForTariffDTO>().ReverseMap();
      CreateMap<ClientProduct, ClientProductDTO>().ReverseMap();
      CreateMap<CaseTribunalFeedBack, CaseTribunalFeedBackDto>().ReverseMap();
      CreateMap<OffSiteMonitoringReport, OffSiteMonitoringReportDTO>().ReverseMap();
      CreateMap<OffSiteMonitoringReportDetail, OffSiteMonitoringReportDetailDTO>().ReverseMap();
      CreateMap<AnnualBudgetCloser, AnnualBudgetCloserDTO>().ReverseMap();

      CreateMap<BankOversightViolation, BankOversightViolationDTO>().ReverseMap();
      CreateMap<BankOverSightViolationDetail, BankOverSightViolationDetailDTO>().ReverseMap();
      // data Change
      CreateMap<DataChange, AddressChangeDTO>().ReverseMap();
      CreateMap<Owner, OwnerDTO>().ReverseMap();
      CreateMap<MonthlyOnsiteFollowupSummary, MonthlyOnsiteFollowupSummary>().ReverseMap();
      CreateMap<CommonViolation, CommonViolationDetail>();
      CreateMap<CommonViolationDetail, CommonViolation>();
      CreateMap<SettlementGuarnatteFund, SettlementGuarnatteFundDTO>().ReverseMap();
      CreateMap<SettlementBankPerformance, SettlementBankPerformanceDTO>().ReverseMap();
      CreateMap<GuaranteeFundUpload, GuaranteeFundUploadDTO>().ReverseMap();
      CreateMap<DelayRegisterPerBank, DelayRegisterPerBankDTO>().ReverseMap();
      CreateMap<ViolationInvestigation, ViolationInvestigationDto>().ReverseMap();

    }
  }

}
