﻿using System;

namespace CUSTOR
{
    public static class CBirrToAmharicString
    {
        public static String ChangeNumericToWords(double numb)
        {
            String num = numb.ToString();

            return ChangeToWords(num, false);
        }

        public static String ChangeCurrencyToWords(String numb)
        {
            return ChangeToWords(numb, true);
        }

        public static String ChangeNumericToWords(String numb)
        {
            return ChangeToWords(numb, false);
        }

        public static String ChangeCurrencyToWords(double numb)
        {
            return ChangeToWords(numb.ToString(), true);
        }

        private static String ChangeToWords(String numb, bool isCurrency)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = (isCurrency) ? ("ብቻ") : ("");

            try
            {

                int decimalPlace = numb.IndexOf(".");

                if (decimalPlace > 0)
                {

                    wholeNo = numb.Substring(0, decimalPlace);

                    points = numb.Substring(decimalPlace + 1);

                    if (Convert.ToInt32(points) > 0)
                    {

                        andStr = (isCurrency) ? ("ብር ከ") : ("ነጥብ");// just to separate whole numbers from points/cents

                        endStr = (isCurrency) ? ("ሳንቲም " + endStr) : ("");

                        //pointStr = TranslateCents(points);
                        pointStr = ChangeNumericToWords(points);

                    }


                }
                else
                {
                    andStr = (isCurrency) ? ("ብር") : ("");
                }

                val = String.Format("{0} {1}{2} {3}", TranslateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);

            }

            catch {; }

            return val;

        }

        private static String TranslateWholeNumber(String number)
        {

            string word = "";

            try
            {

                bool beginsZero = false;//tests for 0XX

                bool isDone = false;//test if already translated

                double dblAmt = (Convert.ToDouble(number));

                //if ((dblAmt > 0) && number.StartsWith("0"))

                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric

                    beginsZero = number.StartsWith("0");

                    int numDigits = number.Length;

                    int pos = 0;//store digit grouping

                    String place = "";//digit grouping name:hundres,thousand,etc...

                    switch (numDigits)
                    {

                        case 1://ones' range

                            word = Ones(number);

                            isDone = true;

                            break;

                        case 2://tens' range

                            word = Tens(number);

                            isDone = true;

                            break;

                        case 3://hundreds' range

                            pos = (numDigits % 3) + 1;

                            if (beginsZero)
                                place = "";
                            else
                                place = " መቶ "; //Hundred

                            break;

                        case 4://thousands' range

                        case 5:

                        case 6:

                            pos = (numDigits % 4) + 1;

                            if (beginsZero)
                                place = "";
                            else
                                place = " ሺ "; //Thousand

                            break;

                        case 7://millions' range

                        case 8:

                        case 9:

                            pos = (numDigits % 7) + 1;

                            if (beginsZero)
                                place = "";
                            else
                                place = " ሚሊየነ "; //Million

                            break;

                        case 10://Billions's range

                            pos = (numDigits % 10) + 1;

                            if (beginsZero)
                                place = "";
                            else
                                place = " ቢሊየን "; // Billion

                            break;

                        //add extra case options for anything above Billion...

                        default:

                            isDone = true;

                            break;

                    }

                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)

                        word = TranslateWholeNumber(number.Substring(0, pos)) + place + TranslateWholeNumber(number.Substring(pos));

                        //check for trailing zeros

                        if (beginsZero) word = " " + word.Trim();

                    }

                    //ignore digit grouping names

                    if (word.Trim().Equals(place.Trim())) word = "";

                }

            }

            catch {; }

            return word.Trim();
        }

        private static String Tens(String digit)
        {

            int digt = Convert.ToInt32(digit);

            String name = null;

            switch (digt)
            {

                case 10:

                    name = "አስር";

                    break;

                case 11:

                    name = "አስራ አንድ";

                    break;

                case 12:

                    name = "አስራ ሁለት";

                    break;

                case 13:

                    name = "አስራ ሶስት";

                    break;

                case 14:

                    name = "አስራ አራት";

                    break;

                case 15:

                    name = "አስራ አምስት";

                    break;

                case 16:

                    name = "አስራ ስድስት";

                    break;

                case 17:

                    name = "አስራ ሰባት";

                    break;

                case 18:

                    name = "አስራ ስምንት";

                    break;

                case 19:

                    name = "አስራ ዘጠኝ";

                    break;

                case 20:

                    name = "ሃያ";

                    break;

                case 30:

                    name = "ሰላሳ";

                    break;

                case 40:

                    name = "አርባ";

                    break;

                case 50:

                    name = "ሀምሳ";

                    break;

                case 60:

                    name = "ስልሳ";

                    break;

                case 70:

                    name = "ሰባ";

                    break;

                case 80:

                    name = "ሰማንያ";

                    break;

                case 90:

                    name = "ዘጠና";

                    break;

                default:

                    if (digt > 0)
                    {

                        name = Tens(digit.Substring(0, 1) + "0") + " " + Ones(digit.Substring(1));

                    }

                    break;

            }

            return name;

        }

        private static String Ones(String digit)
        {

            int digt = Convert.ToInt32(digit);

            String name = "";

            switch (digt)
            {

                case 1:

                    name = "አንድ";

                    break;

                case 2:

                    name = "ሁለት";

                    break;

                case 3:

                    name = "ሶስት";

                    break;

                case 4:

                    name = "አራት";

                    break;

                case 5:

                    name = "አምስት";

                    break;

                case 6:

                    name = "ስድስት";

                    break;

                case 7:

                    name = "ሰባት";

                    break;

                case 8:

                    name = "ስምንት";

                    break;

                case 9:

                    name = "ዘጠኝ";

                    break;

            }

            return name;

        }

        private static String TranslateCents(String cents)
        {

            String cts = "", digit = "", engOne = "";

            for (int i = 0; i < cents.Length; i++)
            {

                digit = cents[i].ToString();

                if (digit.Equals("0"))
                {

                    engOne = "ዜሮ";

                }

                else
                {

                    engOne = Ones(digit);

                }

                cts += " " + engOne;

            }
            return cts;
        }
    }
}
