﻿namespace CUSTOR.ICERS.Core.Helpers
{

    public class NumberToWord
    {
        public static string ConvertNumberToWord(long numberVal)
        {
            string[] powers = new string[] { "THOUSAND ", "MILLION ", "BILLION " };
            string[] ones = new string[] { "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
            string[] tens = new string[] { "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };
            string wordValue = "";

            if (numberVal == 0) return "zero";
            if (numberVal < 0)
            {
                wordValue = "NEGATIVE ";
                numberVal = -numberVal;
            }
            long[] partStack = new long[] { 0, 0, 0, 0 };
            int partNdx = 0;
            while (numberVal > 0)
            {
                partStack[partNdx++] = numberVal % 1000;
                numberVal /= 1000;
            }

            for (int i = 3; i >= 0; i--)
            {
                long part = partStack[i];

                if (part >= 100)
                {
                    wordValue += ones[part / 100 - 1] + " HUNDRAD ";
                    part %= 100;
                }

                if (part >= 20)
                {
                    if ((part % 10) != 0) wordValue += tens[part / 10 - 2] +
                       " " + ones[part % 10 - 1] + " ";
                    else wordValue += tens[part / 10 - 2] + " ";
                }
                else if (part > 0) wordValue += ones[part - 1] + " ";

                if (part != 0 && i > 0) wordValue += powers[i - 1];
            }

            return wordValue;
        }

    }

}