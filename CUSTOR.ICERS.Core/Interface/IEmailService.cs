﻿using CUSTOR.ICERS.Core.EntityLayer.Email;
using System.Collections.Generic;

namespace CUSTOR.ICERS.Core.Interface
{
    public interface IEmailService
    {
        void Send(EmailMessage emailMessage);
        List<EmailMessage> ReceiveEmail(int maxCount = 10);
    }

}
