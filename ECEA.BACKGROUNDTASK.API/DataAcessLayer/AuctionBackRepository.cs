using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
  public class AuctionBackRepository
  {
    private ECEADbContext  _eceacontext;
    private ECXTradeDbContext _context;
    public AuctionBackRepository()
    {

    }
    public bool PostViolation(string type, string violationId, DateTime? submittedTime, string memberCode)
    {
      try
      {
        var old = _eceacontext.EcxAuction.FirstOrDefault(x => x.OrderId.ToString() == violationId);
        if (old != null)
        {
          old.IsViolation = true;
          old.ViewStatus = "Not Viewed";
          _eceacontext.SaveChanges();
        }
        var data = _eceacontext.AuctionViolationRecord.FirstOrDefault(x => x.ViolationId == violationId && x.Type.Trim() == type.Trim());
        if (data == null)
        {
          AuctionViolationRecord violation = new AuctionViolationRecord()
          {
            CreatedDate = Convert.ToDateTime( submittedTime),
            Id = Guid.NewGuid().ToString(),
            Type = type,
            ViolationId = violationId ,
            MemberId =   memberCode
          };
          _eceacontext.AuctionViolationRecord.Add(violation);
          _eceacontext.SaveChanges();
        }
        return true;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public List<Auction> getbidoffers(DateTime From, DateTime To, string connectionString, string ecxTradeConnection)
    {
      string count = "";
      try
      {
        var optionBuilder = new DbContextOptionsBuilder<ECEADbContext>();
        optionBuilder.UseSqlServer(connectionString);
        _eceacontext = new ECEADbContext(optionBuilder.Options);
        var optionBuilderTrade = new DbContextOptionsBuilder<ECXTradeDbContext>();
        optionBuilderTrade.UseSqlServer(ecxTradeConnection);
        _context = new ECXTradeDbContext(optionBuilderTrade.Options);
        List<Auction> bidOfferRecordVMs = new List<Auction>();
       // List<Auction> addedBidOffers = new List<Auction>();
        bidOfferRecordVMs = _context.Auction.FromSqlRaw<Auction>(QueryConfiguration.GetQuery(Constants.GetAuction), From, To).AsNoTracking().AsEnumerable().OrderByDescending(x => x.SubmitedTimestamp).ToList();
       // addedBidOffers = bidOfferRecordVMs.Where(x => x.PreOpenEnd != null && x.PreOpenEnd != null).ToList();
        foreach (var item in bidOfferRecordVMs)
        {
          PostOrder(item);
          //if (item.OpenStart.HasValue && item.OpenEnd.HasValue)
          //{

          //  var cancellation = _context.ArrangedTrade.FromSql(QueryConfiguration.GetQuery(Constants.Constants.PrearrangedDuringOpen), item.OrderId, item.OpenStart, item.OpenEnd).FirstOrDefault();
          //  if (cancellation != null)
          //  {
          //    var followerBuyer = bidOfferRecordVMs.Where(x => x.MemberId != item.MemberId && x.CommodityGradeId == cancellation.CommodityGradeId && x.OpenStart >= item.OpenStart && x.OpenEnd <= item.OpenEnd).OrderByDescending(x => x.SubmitedTimestamp).FirstOrDefault();
          //    if (followerBuyer != null)
          //    {
          //      if (followerBuyer.Price < cancellation.LimitPrice)
          //      {
          //        PostViolation( Constants.HasPreArrangedDuringOpen, item.OrderId.ToString());
          //      }
          //    }

          //  }
          //  if (item.OrderType.Equals("Buyer"))
          //  {
          //    decimal priceChange = 0;
          //    decimal priceChange2 = 0;
          //    var ordersOfBuyers = bidOfferRecordVMs.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.MemberId == item.MemberId && x.OpenStart >= item.OpenStart && x.OpenEnd <= item.OpenEnd).OrderByDescending(x => x.SubmitedTimestamp).ToList();
          //    if (ordersOfBuyers.Count() > 2)
          //    {
          //      priceChange = Convert.ToDecimal(ordersOfBuyers[1].Price - ordersOfBuyers[0].Price);
          //      priceChange2 = Convert.ToDecimal(ordersOfBuyers[2].Price - ordersOfBuyers[1].Price);

          //    }

          //    var ordersOfSeller = bidOfferRecordVMs.Where(x => x.OrderType.Equals("seller") && x.CommodityGradeId == item.CommodityGradeId && x.OpenStart >= item.OpenStart && x.OpenEnd <= item.OpenEnd).ToList();
          //    foreach (var seller in ordersOfSeller.Select(x => x.MemberId).Distinct())
          //    {
          //      var countMemberOrder = ordersOfSeller.Where(x => x.MemberId == seller).OrderByDescending(x => x.SubmitedTimestamp).ToList();
          //      if (countMemberOrder.Count > 2)
          //      {
          //        var SellerChange = Convert.ToDecimal(countMemberOrder[0].Price - countMemberOrder[1].Price);
          //        var SellerChange2 = Convert.ToDecimal(countMemberOrder[1].Price - countMemberOrder[2].Price);
          //        if (SellerChange == priceChange && priceChange2 == SellerChange2)
          //        {
          //          if (priceChange > 0 && priceChange2 > 0)
          //          {
          //            PostViolation("HasAbnormalTrading", item.OrderId.ToString());

          //          }
          //        }
          //      }
          //    }
          //  }

          //}
          //var preopen = bidOfferRecordVMs.Where(x => x.OrderId != item.OrderId && x.MemberId == item.MemberId && item.CommodityGradeId == x.CommodityGradeId).FirstOrDefault();
          //if (preopen != null)
          //{
          //  if (preopen.Quantity != item.Quantity)
          //  {
          //    PostViolation("HasNewRequestAfterPreOpen", item.OrderId.ToString());
          //  }
          //}
          //else
          //{

          //  if (item.SubmittedTimestamp > item.PreOpenEnd)
          //  {
          //    PostViolation("HasNewRequestAfterPreOpen", item.OrderId.ToString());

          //  }
          //}
          if (item.MemberCode != null)
          {
            var exchangeActor = _context.ExchangeActorCode.FromSqlRaw<ExchangeActorCode>(QueryConfiguration.GetQuery(Constants.ExchangeActorCode), item.MemberCode).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (exchangeActor == null)
            {
              PostViolation("IsNotRegistered", item.OrderId.ToString(),item.SubmitedTimestamp, item.MemberCode);


              //}
            }
            var canceledMember = _context.CancellationInformation.FromSqlRaw<CancellationInformation>(QueryConfiguration.GetQuery(Constants.Cancel), item.MemberCode).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (canceledMember != null)
            {

              PostViolation("HasCancelled", item.OrderId.ToString(), item.SubmitedTimestamp,item.MemberCode);

            }
            var suspendedMember = _context.SuspendedExchange.FromSqlRaw<SuspendedExchange>(QueryConfiguration.GetQuery(Constants.SuspendedMember), item.MemberCode).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (suspendedMember != null)
            {

              PostViolation("HasSuspended", item.OrderId.ToString(),item.SubmitedTimestamp,item.MemberCode);
            }
            var notRenewedMember = _context.RenewalInformation.FromSqlRaw<RenewalInformation>(QueryConfiguration.GetQuery(Constants.NotRenewedMember), item.MemberCode, item.SubmitedTimestamp).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (notRenewedMember != null)
            {
              PostViolation("HasNotRenewed", item.OrderId.ToString(),item.SubmitedTimestamp,item.MemberCode);

            }
            //if (item.PreOpenStart.HasValue && item.PreOpenEnd.HasValue && item.OrderType == "seller" && item.CommodityGradeId.HasValue && item.Price.HasValue && item.Quantity.HasValue)
            //{

            //  var prearranged = addedBidOffers.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.PreOpenStart.Value.Date >= item.PreOpenStart.Value.Date && x.PreOpenEnd.Value.Date <= item.PreOpenEnd.Value.Date && x.Price == item.Price && x.Quantity == item.Quantity).ToList();
            //  if (prearranged.Count >= 2)
            //  {
            //    PostViolation("HasPreArragedTrade", item.OrderId.ToString());

            //  }
            //  // && 


            //}
          }
          var backeting = _context.BacketingViolation.FromSqlRaw<BacketingViolation>(QueryConfiguration.GetQuery(Constants.Bucketing), item.WarehouseId, item.CommodityGradeId, From, To).AsNoTracking().AsEnumerable().ToList();
          if (backeting != null)
          {
            PostViolation("HasBucketing", item.OrderId.ToString(),item.SubmitedTimestamp, item.MemberCode);
          }
          var listData = bidOfferRecordVMs.Where(x => x.CommodityGradeId == item.CommodityGradeId).OrderByDescending(x => x.SubmitedTimestamp);
          if (listData.Any())
          {
            var first = listData.FirstOrDefault();
            if (first.OrderId == item.OrderId)
            {
              var forWeigh = listData.Where(x => x.OrderId != first.OrderId);
              if (forWeigh.Any())
              {
                var weightedAvg = forWeigh.Sum(x => x.Price) / forWeigh.Count();
                if (item.CommodityType == "Coffee")
                {
                  var coffee = _eceacontext.Commodity.FirstOrDefault(x => x.DesciptionEng.Contains("Coffee"));
                  var min = weightedAvg - coffee?.WeightedAverage;
                  var max = weightedAvg + coffee?.WeightedAverage;
                  if (item.Price < min || item.Price > max)
                  {
                    PostViolation("HasMarkingToClose", item.OrderId.ToString(),item.SubmitedTimestamp,item.MemberCode);

                  }

                }
              }

            }
          }
          PreviousSessionVM previousSession = new PreviousSessionVM();
          previousSession = _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.LastSession), item.CommodityGradeId, item.SessionId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
          if (previousSession != null)
          {
            PreviousPriceVM previousPrice = new PreviousPriceVM();
            previousPrice = _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.WeightAveragePrice), item.CommodityGradeId, previousSession.LastSessionId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (previousPrice != null)
            {
              var priceChange = Math.Round(Convert.ToDecimal((item.Price - previousPrice.WeightAveragePrice)), 2);
              if (priceChange != 0)
               {
                PostViolation("HasPriceChange", item.OrderId.ToString(),item.SubmitedTimestamp , item.MemberCode);
              }

            }
          }
          CommodityPriceLimit priceLimit = new CommodityPriceLimit();
          priceLimit = _context.CommodityPriceLimit.FromSqlRaw<CommodityPriceLimit>(QueryConfiguration.GetQuery(Constants.PriceLimitForCommodity), item.SessionId, item.CommodityGradeId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
          if (priceLimit != null)
          {
            if (item.Price < priceLimit.LowerPriceLimit || item.Price > priceLimit.UpperPriceLimit)
            {
              PostViolation("HasPriceLimit", item.OrderId.ToString(), item.SubmitedTimestamp, item.MemberCode);
            }
          }

        }
        return bidOfferRecordVMs;
      }
      catch (Exception ex)
      {
        var x = count;
        throw new Exception(ex.Message);
      }
    }
    public bool PostOrder(Auction item)
    {
      var old = _context.EcxAuction.FirstOrDefault(x => x.OrderId == item.OrderId);
      if (old == null)
      {
        EcxAuction ecxOrder = new EcxAuction()
        {
          Price = item.Price,
          OrderType = item.OrderType,
          ClientName = item.ClientName,
          CommodityClass = item.CommodityClass,
          CommodityGrade = item.CommodityGrade,
          CommodityType = item.CommodityType,
          //ProductionYear = item.ProductionYear,
          Quantity = item.Quantity,
          RepName = item.RepName,
          SubmitedTimestamp = item.SubmitedTimestamp,
          Warehouse = item.Warehouse,
          OrderId = item.OrderId,
          Member = item.Member,
          Symbol = item.Symbol,
          OrderStatus = item.OrderStatus,
          ViewStatus = "No Violation",
          MemberId = item.MemberCode ,
           SessionId = item.SessionId.ToString(),
            SessionEnd = item.SessionEnd,
             SessionStart = item.SessionStart
        };
        _context.EcxAuction.Add(ecxOrder);
        _context.SaveChanges();
        return true;
      }
      else
      {
        old.OrderStatus = item.OrderStatus;
        old.MemberId = item.MemberCode;
        old.SessionId = item.SessionId.ToString();
        old.SessionEnd = item.SessionEnd;
        old.SessionStart = item.SessionStart;
        _context.SaveChanges();
        return true;
      }
    }

  }
}
