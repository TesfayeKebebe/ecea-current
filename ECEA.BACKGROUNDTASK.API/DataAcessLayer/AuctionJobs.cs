using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
  [DisallowConcurrentExecution]
  public class AuctionJobs: IJob
  {
    private readonly IServiceProvider _provider;
  private readonly ILogger<AuctionJobs> _logger;
  AuctionBackRepository _auctionBackRepository;
  public IConfiguration _configuration { get; }

  public AuctionJobs(IServiceProvider provider, ILogger<AuctionJobs> logger, IConfiguration configuration)
  {
    _provider = provider;
    _logger = logger;
    _configuration = configuration;

  }
  public Task Execute(IJobExecutionContext context)
  {
    string connectionString = _configuration.GetConnectionString("DefaultConnection");
    string ecxTradeConnection = _configuration.GetConnectionString("ECXTradeConnection");

      _auctionBackRepository = new AuctionBackRepository();
    var from = Convert.ToDateTime("09/01/2020");
     _auctionBackRepository.getbidoffers(from, Convert.ToDateTime("09/30/2020"), connectionString, ecxTradeConnection);
    return Task.CompletedTask;
  }
}
}
