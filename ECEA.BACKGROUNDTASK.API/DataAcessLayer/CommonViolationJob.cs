using CUSTOR.ICERS.Core.DataAccessLayer.Violation;
using ECEA.BACKGROUNDTASK.API.DataAcessLayer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API
{
    [DisallowConcurrentExecution]
    public class CommonViolationJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger<CommonViolationJob> _logger;
        TradeRepository _repository;
        OrderRepository _offerRecordRepository;
        public IConfiguration _configuration { get; }

        public CommonViolationJob(IServiceProvider provider, ILogger<CommonViolationJob> logger, IConfiguration configuration)
        {
            _provider = provider;
            _logger = logger;
            _configuration = configuration;
             
    }
    public Task Execute(IJobExecutionContext context)
        {
            string connectionString = _configuration.GetConnectionString("DefaultConnection");
      string ecxTradeConnection = _configuration.GetConnectionString("ECXTradeConnection");
      _repository = new TradeRepository();
            var from = Convert.ToDateTime("08/26/2020");
      var obj = _repository.getOrdersTrade(from, Convert.ToDateTime("09/30/2020"), 0, 0, "TV", connectionString, ecxTradeConnection); 
      return Task.CompletedTask;
        }
    }
}
