using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
  [DisallowConcurrentExecution]
  public class OrderJob : IJob
  {
    private readonly IServiceProvider _provider;
    private readonly ILogger<OrderJob> _logger;
    OrderRepository _offerRecordRepository;
    public IConfiguration _configuration { get; }

    public OrderJob(IServiceProvider provider, ILogger<OrderJob> logger, IConfiguration configuration)
    {
      _provider = provider;
      _logger = logger;
      _configuration = configuration;

    }
    public Task Execute(IJobExecutionContext context)
    {
      string connectionString = _configuration.GetConnectionString("DefaultConnection");
      string ecxTradeConnection = _configuration.GetConnectionString("ECXTradeConnection");
      _offerRecordRepository = new OrderRepository();
      var from = Convert.ToDateTime("09/01/2020");
      var bid = _offerRecordRepository.getbidoffers(from, Convert.ToDateTime("09/01/2020"), connectionString, ecxTradeConnection);
      return Task.CompletedTask;
    }
  }
}
