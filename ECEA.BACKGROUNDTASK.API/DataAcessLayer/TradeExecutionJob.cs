//using AutoMapper.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
    [DisallowConcurrentExecution]
    public class TradeExecutionJob: IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger<TradeExecutionJob> _logger;
        private  IConfiguration _configuration { get; }
        TradeExecutionRepository _tradeExecutionRepository;
        public TradeExecutionJob(IServiceProvider provider, ILogger<TradeExecutionJob> logger, 
                                   IConfiguration configuration)
        {
            _provider = provider;
            _logger = logger;
            _configuration = configuration;
        }
        public Task Execute(IJobExecutionContext context)
        {
            string connectionString = _configuration.GetConnectionString("DefaultConnection");
      string ecxTradeConnection = _configuration.GetConnectionString("ECXTradeConnection");
      _tradeExecutionRepository = new TradeExecutionRepository();
            var from = Convert.ToDateTime("01/01/2020");
            var to = Convert.ToDateTime(DateTime.Now);
            _tradeExecutionRepository.GetTradeExecutionList(from, to, connectionString, ecxTradeConnection);
            return Task.CompletedTask;
        }
    }
}
