using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
    public class TradeExecutionRepository
    {
    private ECEADbContext _eceacontext;
    private ECXTradeDbContext _context;
    public TradeExecutionRepository()
        {

        }
        public List<TradeExecutionViewModel> GetTradeExecutionList(DateTime From, DateTime To, string connectionString, string ecxTradeConnection)
        {
            try
            {
        var optionBuilder = new DbContextOptionsBuilder<ECEADbContext>();
        optionBuilder.UseSqlServer(connectionString);
        _eceacontext = new ECEADbContext(optionBuilder.Options);
        var optionBuilderTrade = new DbContextOptionsBuilder<ECXTradeDbContext>();
        optionBuilderTrade.UseSqlServer(ecxTradeConnection);
        _context = new ECXTradeDbContext(optionBuilderTrade.Options);
        List<TradeExecutionViewModel> listOfTradeExecution = new List<TradeExecutionViewModel>();
                listOfTradeExecution = _context.TradeExecutionViewModel.FromSqlRaw<TradeExecutionViewModel>("exec dbo.preGetDailyTradeExecution {0},{1}", From, To).ToList();
                foreach (var item in listOfTradeExecution)
                {
                    var traded = _eceacontext.TradeExecution.Where(x => x.TradeId == item.TradeId).FirstOrDefault();
                    if (traded == null)
                    {
                        TradeExecution tradeExecution = new TradeExecution()
                        {
                            TradeId = item.TradeId,
                            TradedTimestamp = item.TradedTimestamp,
                            TradeTypeId = item.TradeTypeId,
                            CommodityId = item.CommodityId,
                            BuyerClientFullName = item.BuyerClientFullName,
                            BuyerIDNO = item.BuyerIDNO,
                            BuyerMemberId = item.BuyerMemberId,
                            BuyerOrganizationName = item.BuyerOrganizationName,
                            BuyerRepresentativeId = item.BuyerRepresentativeId,
                            BuyerRepresentativeName = item.BuyerRepresentativeName,
                            BuyOrderTicketId = item.BuyOrderTicketId,
                            Commodity = item.Commodity,
                            CommodityGrade = item.CommodityGrade,
                            CommodityGradeId = item.CommodityGradeId,
                            CommodityType = item.CommodityType,
                            CommodityTypeId = item.CommodityTypeId,
                            Price = item.Price,
                            ProductionYear = item.ProductionYear,
                            Quantity = item.Quantity,
                            Remark = item.Remark,
                            SellerClientFullName = item.SellerClientFullName,
                            SellerIDNO = item.SellerIDNO,
                            SellerMemberId = item.SellerMemberId,
                            SellerOrganizationName = item.SellerOrganizationName,
                            SellerRepresentativeId = item.SellerRepresentativeId,
                            SellerRepresentativeName = item.SellerRepresentativeName,
                            SellOrderTicketId = item.SellOrderTicketId,
                            StatusId = item.StatusId
                        };
            _eceacontext.Add(tradeExecution);
            _eceacontext.SaveChanges();
                      
                    }
                }
                return listOfTradeExecution;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public bool PostTradeExecution(TradeExecutionViewModel item)
        {
            var traded = _eceacontext.TradeExecution.Where(x => x.TradeId == item.TradeId).FirstOrDefault();
            if (traded != null)
            {
                return false;
            }
            else
            {
                TradeExecution tradeExecution = new TradeExecution()
                {
                    TradeId = item.TradeId,
                    TradedTimestamp = item.TradedTimestamp,
                    TradeTypeId = item.TradeTypeId,
                    CommodityId = item.CommodityId,
                    BuyerClientFullName = item.BuyerClientFullName,
                    BuyerIDNO = item.BuyerIDNO,
                    BuyerMemberId = item.BuyerMemberId,
                    BuyerOrganizationName = item.BuyerOrganizationName,
                    BuyerRepresentativeId = item.BuyerRepresentativeId,
                    BuyerRepresentativeName = item.BuyerRepresentativeName,
                    BuyOrderTicketId = item.BuyOrderTicketId,
                    Commodity = item.Commodity,
                    CommodityGrade = item.CommodityGrade,
                    CommodityGradeId = item.CommodityGradeId,
                    CommodityType = item.CommodityType,
                    CommodityTypeId = item.CommodityTypeId,
                    Price = item.Price,
                    ProductionYear = item.ProductionYear,
                    Quantity = item.Quantity,
                    Remark = item.Remark,
                    SellerClientFullName = item.SellerClientFullName,
                    SellerIDNO = item.SellerIDNO,
                    SellerMemberId = item.SellerMemberId,
                    SellerOrganizationName = item.SellerOrganizationName,
                    SellerRepresentativeId = item.SellerRepresentativeId,
                    SellerRepresentativeName = item.SellerRepresentativeName,
                    SellOrderTicketId = item.SellOrderTicketId,
                    StatusId = item.StatusId
                };
        _eceacontext.Add(tradeExecution);
        _eceacontext.SaveChanges();
                return true;
            }


        }
    }
}
