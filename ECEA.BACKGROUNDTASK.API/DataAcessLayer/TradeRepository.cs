using CUSTOR.ICERS.Core;
using CUSTOR.ICERS.Core.Common;
using CUSTOR.ICERS.Core.EntityLayer;
using CUSTOR.ICERS.Core.EntityLayer.EcxViewModels;
using CUSTOR.ICERS.Core.EntityLayer.Violation;
using Microsoft.AspNetCore.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECEA.BACKGROUNDTASK.API.DataAcessLayer
{
  public class TradeRepository
  {
    private ECEADbContext _eceacontext;
    private ECXTradeDbContext _context;
    public TradeRepository()
    {

    }
    public List<CommonTradeVM> getOrdersTrade(DateTime From, DateTime To, decimal VolumLimit, int Percentage, string Type, string connectionString, string ecxTradeConnection)
    {
      try
      {
        var optionBuilder = new DbContextOptionsBuilder<ECEADbContext>();
        optionBuilder.UseSqlServer(connectionString);
        _eceacontext = new ECEADbContext(optionBuilder.Options);
        var optionBuilderTrade = new DbContextOptionsBuilder<ECXTradeDbContext>();
        optionBuilderTrade.UseSqlServer(ecxTradeConnection);
        _context = new ECXTradeDbContext(optionBuilderTrade.Options);
        List<CommonViolationDetail> addedDetails = new List<CommonViolationDetail>();
        List<CommonTradeVM> addedCommonOrderAndTrade = new List<CommonTradeVM>();
        List<CommonOrderAndTradeVM> Trades = new List<CommonOrderAndTradeVM>();
        addedCommonOrderAndTrade = _context.CommonTradeVM.FromSqlRaw<CommonTradeVM>(QueryConfiguration.GetQuery(Constants.TradeByDate), From, To).AsNoTracking().AsEnumerable().OrderByDescending(x => x.TradedTimestamp).ToList();
        foreach (var item in addedCommonOrderAndTrade)
        {
          GetCommonViolaitons(item);
          var highPrice = addedCommonOrderAndTrade.Where(x => x.SessionId == item.SessionId && item.CommodityGradeId == x.CommodityGradeId).OrderByDescending(x => x.TradePrice).FirstOrDefault();
          if (highPrice != null)
          {
            if (highPrice.tradeId == item.tradeId)
            {
              OpenningClosingPrice openclose = new OpenningClosingPrice();
              openclose = _context.OpenningClosingPrice.FromSqlRaw<OpenningClosingPrice>(QueryConfiguration.GetQuery(Constants.ClosingAndOpenningPrice),item.SessionId,item.CommodityGradeId, item.WarehouseId,item.TradedTimestamp).AsNoTracking().AsEnumerable().FirstOrDefault();
              var lowPrice = addedCommonOrderAndTrade.Where(x => x.SessionId == item.SessionId && item.CommodityGradeId == x.CommodityGradeId).OrderBy(x => x.TradePrice).FirstOrDefault();
              var oldTrande = _eceacontext.TradeTrend.FirstOrDefault(x => x.TradeId == item.tradeId);
              if (oldTrande != null)
              {
                oldTrande.TradeDate = Convert.ToDateTime(item.TradedTimestamp);
                oldTrande.TradeId = item.tradeId;
                oldTrande.Symbol = item.Symbol;
                oldTrande.Volume = Convert.ToDouble(item.Lot);
                oldTrande.High = Convert.ToDouble(highPrice?.TradePrice);
                oldTrande.Low = Convert.ToDouble(lowPrice?.TradePrice);
                oldTrande.CommodityId = openclose.CommodityId;
                oldTrande.CommodityTypeId = openclose.CommodityTypeId;
                _eceacontext.SaveChanges();
              }
              else
              {
                TradeTrend tt = new TradeTrend()
                {
                  Close = Convert.ToDouble( openclose?.ClosingPrice),
                  CommodityGradeId = item.CommodityGradeId,
                  High = Convert.ToDouble(highPrice?.TradePrice),
                  Low = Convert.ToDouble(lowPrice?.TradePrice),
                  Open = Convert.ToDouble(openclose?.OpenningPrice),
                  ProductionYear = Convert.ToInt32(openclose?.ProductionYear),
                  Symbol = item.Symbol,
                  TradeDate = Convert.ToDateTime(item.TradedTimestamp),
                  Warehouse = item.warehouse,
                  WarehouseId = item.WarehouseId.Value,
                  Volume = Convert.ToDouble(item.Lot),
                  TradeId = item.tradeId,
                  ProductType = item.CommodityType,
                 CommodityId = openclose.CommodityId,
              CommodityTypeId = openclose.CommodityTypeId
              };
                _eceacontext.TradeTrend.Add(tt);
                _eceacontext.SaveChanges();
              }
            }

          }

          List<OrderMember> orderMember = new List<OrderMember>();
          if (item.BuyerCode != null)
          {
            orderMember = _context.OrderMember.FromSqlRaw<OrderMember>(QueryConfiguration.GetQuery(Constants.OrderByMemberId), item.BuyerCode).ToList();
            if (orderMember.Any())
            {
              var client = orderMember.FirstOrDefault(x => x.Id == item.SellOrderTicketId);
              if (client != null)
              {
                PostViolation("IsMatchTrade", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);

              }
            }
          }
          PreviousSessionVM previousSession = new PreviousSessionVM();
          previousSession = _context.PreviousSessionVM.FromSqlRaw<PreviousSessionVM>(QueryConfiguration.GetQuery(Constants.LastSession), item.CommodityGradeId, item.SessionId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
          if (previousSession != null)
          {
            PreviousPriceVM previousPrice = new PreviousPriceVM();
            previousPrice = _context.PreviousPriceVM.FromSqlRaw<PreviousPriceVM>(QueryConfiguration.GetQuery(Constants.WeightAveragePrice), item.CommodityGradeId, previousSession.LastSessionId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (previousPrice != null)
            {
              var priceChange = Math.Round(Convert.ToDecimal((item.TradePrice - previousPrice.WeightAveragePrice)), 2);
              if (priceChange != 0)
              {
                PostViolation("HasPriceChange", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);
              }

            }
          }


          SisterCompanyMemberVM sisterCompanyMember = new SisterCompanyMemberVM();
          sisterCompanyMember = _context.SisterCompanyMemberVM.FromSqlRaw<SisterCompanyMemberVM>(QueryConfiguration.GetQuery(Constants.SisterCompanyViolationByCode), item.BuyerCode, item.SellerCode).AsNoTracking().AsEnumerable().FirstOrDefault();
          if (sisterCompanyMember != null)
          {
            PostViolation("HasSisterCompany", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);
          }
          if (Percentage > 0 || VolumLimit > 0)
          {
            PriceRangeVM previousRange = new PriceRangeVM();
            previousRange = _context.PriceRangeVM.FromSqlRaw<PriceRangeVM>(QueryConfiguration.GetQuery(Constants.PriceRange), item.CommodityGradeId, item.SessionId, Percentage).AsNoTracking().AsEnumerable().FirstOrDefault();
            if (previousRange != null)
            {
              if (item.TradePrice < previousRange.MinPrice || item.TradePrice > previousRange.MaxPrice)
              {
                PostViolation("HasPriceVolumeBeyondLimit", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);

              }
              if (item.TradeQuantity > VolumLimit && VolumLimit > 0)
              {
                PostViolation("HasPriceVolumeBeyondLimit", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);

              }
            }
          }

          if (item.SellerCode != null && item.BuyerCode != null)
          {

            List<FrontRunning> frontRunningSeller = new List<FrontRunning>();
            List<FrontRunning> frontRunningBuyer = new List<FrontRunning>();
            frontRunningSeller = _context.FrontRunning.FromSqlRaw<FrontRunning>(QueryConfiguration.GetQuery(Constants.FrontRunningBySeller), item.SellerCode, item.CommodityGradeId, From, To).ToList();
            var sellerFirst = frontRunningSeller.Where(x => !x.IsClientOrder).ToList();
            frontRunningBuyer = _context.FrontRunning.FromSqlRaw<FrontRunning>(QueryConfiguration.GetQuery(Constants.FrontRunningByBuyer), item.BuyerCode, item.CommodityGradeId, From, To).ToList();
            var BuyerFirst = frontRunningBuyer.Where(x => !x.IsClientOrder).ToList();
            foreach (var sell in sellerFirst)
            {
              var viol = frontRunningSeller.FirstOrDefault(x => x.IsClientOrder && x.CommodityGradeId == item.CommodityGradeId && (x.TradePrice < sell.TradePrice || x.TradeDate < sell.TradeDate));
              if (viol != null)
              {
                PostViolation("HasFrontRunning", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);

                break;
              }
              else
              {
                continue;
              }
            }

            foreach (var sell in BuyerFirst)
            {
              var viol = frontRunningBuyer.FirstOrDefault(x => x.IsClientOrder && x.CommodityGradeId == item.CommodityGradeId && (x.TradePrice > sell.TradePrice || x.TradeDate < sell.TradeDate));
              if (viol != null)
              {
                PostViolation("HasFrontRunning", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);
                break;
              }
              else
              {
                continue;
              }
            }

          }
          CommodityPriceLimit priceLimit = new CommodityPriceLimit();
          priceLimit = _context.CommodityPriceLimit.FromSqlRaw<CommodityPriceLimit>(QueryConfiguration.GetQuery(Constants.PriceLimitForCommodity), item.SessionId, item.CommodityGradeId, item.WarehouseId).AsNoTracking().AsEnumerable().FirstOrDefault();
          if (priceLimit != null)
          {
            if (item.TradePrice < priceLimit.LowerPriceLimit || item.TradePrice > priceLimit.UpperPriceLimit)
            {
              PostViolation("HasPriceLimit", item.tradeId.ToString(), item.TradedTimestamp, item.BuyerCode, item.SellerCode);
            }
          }
          //if (item.PreOpenStart.HasValue && item.PreOpenEnd.HasValue && item.TransactionType == 2)
          //{
          //  var prearranged = addedCommonOrderAndTrade.Where(x => x.CommodityGradeId == item.CommodityGradeId && x.PreOpenStart.Value.Date >= item.PreOpenStart.Value.Date && x.PreOpenEnd.Value.Date <= item.PreOpenEnd.Value.Date && x.LimitPrice == item.LimitPrice && x.Quantity == item.Quantity && item.memberCode != x.memberCode).ToList();
          //  if (prearranged.Count >= 2)
          //  {
          //   PostViolation("HasPreArragedTrade", item.tradeId.ToString());
          //  }
          //}



          //CommonViolationDetail commonDetail = new CommonViolationDetail();
          //commonDetail = GetCommonViolaitons(item, common);
        }

        return addedCommonOrderAndTrade;
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }
    public bool PostViolation(string type, string violationId, DateTime? tradeTime, string buyer, string seller)
    {
      var old = _eceacontext.EcxTrade.FirstOrDefault(x => x.TradeId.ToString() == violationId);
      if (old != null)
      {
        old.IsVoilation = true;
        old.ViewStatus = "Not Viewed";
        old.BuyerMemberId = buyer;
        old.SellerMemberId = seller;
        _eceacontext.SaveChanges();
      }
      var data = _eceacontext.TradeV‭iolationRecord.FirstOrDefault(x => x.ViolationId == violationId && x.Type.Trim() == type.Trim());
      if (data == null)
      {
        TradeV‭iolationRecord violation = new TradeV‭iolationRecord()
        {
          CreatedDate = Convert.ToDateTime(tradeTime),
          Id = Guid.NewGuid().ToString(),
          Type = type,
          ViolationId = violationId,
          BuyerMemberId = buyer,
          SellerMemberId = seller
        };
        _eceacontext.TradeV‭iolationRecord.Add(violation);
        _eceacontext.SaveChanges();
      }
      return true;
    }

    public bool GetCommonViolaitons(CommonTradeVM item)
    {
      var old = _eceacontext.EcxTrade.FirstOrDefault(x => x.TradeId == item.tradeId);
      if (old == null)
      {
        EcxTrade common = new EcxTrade()
        {
          CommodityGrade = item.CommodityGrade,
          OrderQuantity = item.Quantity,
          //ProductionYear = item.ProductionYear,
          SessionStart = item.SessionStart,
          SessionEnd = item.SessionEnd,
          OrderPrice = item.LimitPrice,
          Warehouse = item.warehouse,
          TradeId = item.tradeId,
          TradeQuantity = item.TradeQuantity,
          TradePrice = item.TradePrice,
          BuyerMember = item.BuyerName,
          SellerMember = item.SellerName,
          CommodityClass = item.CommodityClass,
          CommodityType = item.CommodityType,
          Bags = item.Bags,
          BuyerRepName = item.BuyerRepName,
          ConsignmentType = item.ConsignmentType,
          Quintal = item.Quintal,
          SellerRepName = item.SellerRepName,
          VolumeKGUsingSTD = item.VolumeKGUsingSTD,
          Lot = item.Lot,
          BuyerClientName = item.BuyerClientName,
          SellerClientName = item.SellerClientName,
          TradedTimestamp = item.TradedTimestamp,
          Symbol = item.Symbol,
          ViewStatus = "No Violation",
          SellerMemberId = item.SellerCode,
          BuyerMemberId = item.BuyerCode

        };
        _eceacontext.EcxTrade.Add(common);
        _eceacontext.SaveChanges();
        return true;
      }
      else
      {
        old.TradedTimestamp = item.TradedTimestamp;
        old.SellerMemberId = item.SellerCode;
        old.BuyerMemberId = item.BuyerCode;
        _eceacontext.SaveChanges();
        return false;
      }



    }
  }
}
