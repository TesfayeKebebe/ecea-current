using CUSTOR.ICERS.Core;
using ECEA.BACKGROUNDTASK.API.DataAcessLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace ECEA.BACKGROUNDTASK.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ECEADbContext>(options =>
                  {

                      options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                      options.EnableSensitiveDataLogging(true);

                  }, ServiceLifetime.Transient);
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder.WithOrigins($"http://localhost:4200", $"http://localhost:4400", $"http://www.eca.com", $"http://192.168.8.6:60330", $"http://localhost:60330", $"http://192.168.8.6:60200", $"http://localhost:60200", $"http://127.0.0.1:60200")
            //            .AllowAnyMethod()
            //            .AllowAnyHeader()
            //            .AllowCredentials());
            //});

            var scheduler = StdSchedulerFactory.GetDefaultScheduler().GetAwaiter().GetResult();
            services.AddSingleton(scheduler);
            services.AddControllers();
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddHostedService<QuartzHostedService>();

      services.AddSingleton<CommonViolationJob>();
      services.AddSingleton<OrderJob>();
      services.AddSingleton<TradeExecutionJob>();
      services.AddSingleton<AuctionJobs>();
      services.AddSingleton(new JobSchedule(jobType: typeof(OrderJob),
      cronExpression: "0/5 * * * * ?"));
      services.AddSingleton(new JobSchedule(
      jobType: typeof(CommonViolationJob),
                     cronExpression: "0/5 * * * * ?"));
      services.AddSingleton(new JobSchedule(
               jobType: typeof(TradeExecutionJob),
               cronExpression: "0/1 * * * * ?"));
      services.AddSingleton(new JobSchedule(
               jobType: typeof(AuctionJobs),
               cronExpression: "0/5 * * * * ?"));


    }
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
      //app.UseEndpoints(endpoints =>
      //{
      //  endpoints.MapControllers();
      //});
    }
    }
}
